# Angular Naming Convention

(Hãy đóng góp tài liệu này)

Chuẩn đặt tên áp dụng cho các project Angular
Tham khảo:

- `https://angular.io/guide/styleguide`

## Quy tắc (nên đọc toàn bộ theo hướng dẫn angular, ở đây chỉ trích một số thứ hay gặp)

- Nên sử dụng lệnh hỗ trợ từ CLI
- Các component có thể sử dụng các prefix đặc trưng, việc này sẽ tránh xung đột các component ở nơi khác. Ví dụ module questionbank có component qb-list-question, trong đó qb gợi ý là questionbank
- pipe, directive: cũng nên có prefix tương tự component. Thuộc tính name của pipe, directive

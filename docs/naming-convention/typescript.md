# Typescript Naming Convention

Chuẩn đặt tên áp dụng cho các project typescript

Tham khảo:

- Handbook `https://www.typescriptlang.org/docs/handbook/intro.html`
- Đóng góp từ cộng đồng `https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines`

## Quy tắc

- Sử dụng PascalCase cho class
- Không sử dung prefix "I" cho interface
- Sử dụng PascalCase cho enum, viết hoa các giá trị của nó (điều này khác với cách viết trên c#)
- Sử dụng camelCase cho function
- Sử dụng camelCase cho tên thuộc tính, biến private
- Không sử dụng prefix "_" cho biến private, tuy nhiên đối với biến private của setter, getter thì có thể dùng "_". [Tài liệu tại đây](`https://www.typescriptlang.org/docs/handbook/classes.html#accessors`)
- Đặt tên rõ ràng nhất có thể, không nên viết tắt
- Tên thư mục viết thường

# CLS Naming Convention

- Chuẩn đặt tên áp dụng cho các project CLS

## Quy tắc

Ngoài các quy tắc ở đây cần áp dụng thêm

- Quy tắc `typescript.md` cho dự án `typescript`
- Quy tắc `angular.md` cho dự án `angular`

### Prefix

- `is`: Mô tả đặc điểm hoặc trạng thái của context (thường là boolean).

```TypeScript
const color = "blue"
const isBlue = color === "blue" // đặc điểm
const isPresent = true // trạng thái

if(isBlue && isPresent) {
    console.log("Blue is present");
}
```

- `has`: Xác định sự tồn tại hoặc trạng thái của context (thường là boolean).

```TypeScript
/* Bad */
const isProductsExist = productCount > 0 // sai ngữ pháp
const areProductsPresent = productCount > 0 // chưa gọn, đọc không xuôi tay lắm

/* Good */
const hasProducts = productCount > 0
```

- `should`: Thể hiện điều kiện sẽ xảy ra của một hành động (thường là boolean).

```TypeScript
function shouldUpdateUrl(url, expectedUrl) {
    return url !== expectedUrl;
}
```

- `min/max`: Mô tả giới hạn của context (thường là numeric).

```TypeScript
function getPosts(posts, minPosts, maxPosts) {
    return posts.slice(0, randomBetween(minPosts, maxPosts));
}
```

- `prev/next`: Thể hiện sự chuyển từ trạng thái cũ sang trạng thái mới.

```TypeScript
function fetchPosts() {
    const prevPosts = this.state.posts;
    const fetchedPosts = fetch(...);
    const nextPosts = concat(prevPosts, fetchedPosts);
    this.setState({ posts: nextPosts });
}
```

- Các prefix khác

| Động từ    | Ý nghĩa                | Giá trị trả về chính                                         |
| ---------- | ---------------------- | ------------------------------------------------------------ |
| Get        | Lấy ra                 | Object                                                       |
| Set        | Đặt giá trị            | void                                                         |
| Update     | Cập nhật               | void( hoặc số lượng đã update)                               |
| Delete     | Xóa bỏ                 | void( hoặc số lượng đã delete)                               |
| Insert     | Chèn vào               | void                                                         |
| Select     | Lựa chọn               | Object                                                       |
| Find       | Lựa chọn               | Object                                                       |
| Add        | Thêm vào               | void                                                         |
| Remove     | Xóa bỏ                 | Object                                                       |
| Clear      | Dọn sạch               | void                                                         |
| Append     | Viết thêm              | void or Object sau khi đã thêm                               |
| Trim       | Chỉnh sửa              | Object                                                       |
| Compare    | So sánh                | Kết quả so sánh(-1,0,1)                                      |
| Concat     | Nối thêm               | Object                                                       |
| Split      | Phân chia              | Object                                                       |
| Enumerate  | Liệt kê                | Array                                                        |
| Move       | Di chuyển              | void                                                         |
| Copy       | Sao chép               | void                                                         |
| Create     | Tạo thành              | Object                                                       |
| Make       | Tạo thành              | Object                                                       |
| Generate   | Tạo thành              | Object                                                       |
| New        | Tạo mới                | Object                                                       |
| Build      | Xây dựng               | Object                                                       |
| Flush      | Flush                  | void                                                         |
| Begin      | Bắt đầu                | void                                                         |
| End        | Kết thúc               | void                                                         |
| Initialize | Khởi tạo               | void                                                         |
| Load       | Load                   | void                                                         |
| Merge      | Ghép                   | Object                                                       |
| Read       | Đọc                    | Object                                                       |
| Write      | Viết                   | void                                                         |
| To         | Biến đổi               | Object                                                       |
| Convert    | Biến đổi               | Object                                                       |
| Accept     | Cho phép               | void                                                         |
| Fill       | Làm đầy, điền          | void                                                         |
| Apply      | Áp dụng                | void                                                         |
| Show       | Hiển thị               | void                                                         |
| Union      | Hợp của tập hợp        | Object                                                       |
| Intersect  | Giao của tập hợp       | Object                                                       |
| Do         | Tiến hành              | void                                                         |
| Run        | Tiến hành              | void                                                         |
| Shutdown   | Tắt                    | void                                                         |
| Save       | Lưu lại                | void                                                         |
| Cancel     | Hủy bỏ                 | void                                                         |
| Refresh    | Làm mới                | void                                                         |
| Execute    | Tiến hành              | bool(Thành công hay thất bại)                                |
| Resolve    | Giải quyết             | Object                                                       |
| Invoke     | Gọi ra                 | Object                                                       |
| Handle     | Xử lý                  | Object                                                       |
| Raise      | Gọi ra                 | void                                                         |
| Is         | Có phải là ... không ? | bool                                                         |
| Contains   | Có chứa ... không ?    | bool                                                         |
| Exists     | Có tồn tại không ?     | bool                                                         |
| Verify     | Xác nhận / Đánh giá    | bool(nếu để là void không được thì trả về ngoại lệ cũng tốt) |
| Validate   | Kiểm chứng             | bool                                                         |
| Try        | Thử                    | bool(nếu để là void không được thì trả về ngoại lệ cũng tốt) |
| Has        | Có ... không           | bool                                                         |
| Equals     | So sánh                | bool                                                         |
| Can        | Có thể không           | bool                                                         |

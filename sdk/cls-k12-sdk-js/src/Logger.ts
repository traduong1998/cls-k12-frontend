export class Logger {

	private static instance: Logger;
	private logger: Console = console;
	private LOG_FNS = [this.logger.log, this.logger.debug, this.logger.info, this.logger.warn, this.logger.error];
	private isProdMode = false;

	private constructor() {}

	static getInstance(): Logger {
		if(!Logger.instance){
			Logger.instance = new Logger();
		}
		return Logger.instance;
	}

	log(...args: any[]){
		if (!this.isProdMode) {
			this.LOG_FNS[0].apply(this.logger, args);
		}
	}

	debug(...args: any[]) {
		if (!this.isProdMode) {
			this.LOG_FNS[1].apply(this.logger, args);
		}
	}

	info(...args: any[]) {
		if (!this.isProdMode) {
			this.LOG_FNS[2].apply(this.logger, args);
		}
	}

	warn(...args: any[]) {
		if (!this.isProdMode) {
			this.LOG_FNS[3].apply(this.logger, args);
		}
	}

	error(...args: any[]) {
		this.LOG_FNS[4].apply(this.logger, args);
	}

	enableProdMode(){
		this.isProdMode = true;
	}
}
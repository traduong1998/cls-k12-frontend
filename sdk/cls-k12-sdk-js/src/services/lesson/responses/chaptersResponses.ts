
export interface ChaptersResponses {
    id: number;
    name: string;
    contentPanelId: number;
}
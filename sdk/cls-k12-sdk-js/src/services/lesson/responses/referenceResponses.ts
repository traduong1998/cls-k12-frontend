export interface ReferenceResponses {
    name: string
    fileName?: string;
    link: string
    fileId?: string
    creationDate: Date
    dateOfUpdate: Date
    lessonId: number
    id: number
    domainEvents: any
}
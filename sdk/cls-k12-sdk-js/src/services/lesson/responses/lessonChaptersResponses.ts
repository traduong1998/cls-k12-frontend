

export interface LessonChaptersResponses {
    id: number;
    name: string;
    chapterId: number;
}
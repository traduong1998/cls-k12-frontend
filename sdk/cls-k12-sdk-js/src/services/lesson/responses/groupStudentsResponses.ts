
export interface GroupStudentsResponses {
    id: number;
    name: string;
    gradeId: number;
    schoolId: number;
}
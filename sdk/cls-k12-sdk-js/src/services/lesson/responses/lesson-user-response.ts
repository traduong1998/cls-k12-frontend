export interface UserLesson {
    id: number,
    lessonId: number,
    userId: number,
    percentageOfCompletion: number
}
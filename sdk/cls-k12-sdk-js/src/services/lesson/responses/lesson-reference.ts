

export interface LessonReference {
    id: number,
    name: string,
    teacherId: number,
    avatarUrl: string
    introduction: string
}
export interface ListStudentRegisterResponse {
    id: number;
    fullName: string;
    userName: string;
    groupStudentId: number;
    groupStudentName: string;
    gradeId: number;
    gradeName: string;
    registrationDate: Date;
    isApproval: boolean;
}
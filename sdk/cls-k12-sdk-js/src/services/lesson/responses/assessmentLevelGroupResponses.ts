
export interface AssessmentLevelGroupResponses {
    id: number;
    name: string;
}
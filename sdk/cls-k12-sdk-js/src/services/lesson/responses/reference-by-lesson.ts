export interface ReferenceByLesson {
    id: number,
    name: string
    field: number,
    creationDate: Date,
    lessonId: number
}
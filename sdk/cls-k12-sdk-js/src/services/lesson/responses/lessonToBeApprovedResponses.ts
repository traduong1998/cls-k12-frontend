export interface LessonToBeApprovedResponses {
    id: number;
    name: string;
    userId: number;
    nameOfUser: string;
    submissionDate: string;
}
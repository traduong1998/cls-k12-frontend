
export interface UnitResponses {
    id: number;
    name: string;
    chapterId: number;
}
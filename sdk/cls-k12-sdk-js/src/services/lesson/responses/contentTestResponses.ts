export interface ContentTestResponse {
    id : number,
    name : string,
    contentGroupId? : number,
    conditionsForCompletion : string,
    scoreAfterFinish?: number;
    order : number,
    status : string,
    testId? : number,
    viewCorrectResultAfterSubmittingTheTest?: boolean ,
    viewResultsAfterSubmitting? : boolean,
    numberOfQuestionOnThePage? : number,
    generateNumberRandomQuetions? : number,
    preserveTime? : boolean,
    intervalWriteLog? : number,
    testTime? : number,
    allowToRedo? : boolean,
    theMaximumNumberToRedo? : number,
    isRandomizeQuestions? : boolean,
    urlJsonTest : string,
    numberOfQuestion : number,
    totalScore: number | undefined;
    totalQuizScore: number | undefined;
    totalEssayScore: number | undefined;
}
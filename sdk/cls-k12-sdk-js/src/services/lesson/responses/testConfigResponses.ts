export interface TestConfigResponses {
    id: number;
    viewCorrectResultAfterSubmittingTheTest: boolean;
    viewResultsAfterSubmitting: boolean;
    numberOfQuestionOnThePage?: number;
    generateNumberRandomQuetions?: number
    preserveTime: boolean;
    intervalWriteLog?: number;
    testTime?: number
    allowToRedo: boolean
    theMaximumNumberToRedo?: number
    isRandomizeQuestions: boolean
    numberOfQuestion: number,
    minimumScore?: number

}

export interface SubjectResponses {
    id: number;
    name: string;
    //CHECK: không có thuộc tính này
    gradeId?: number;
    orderNumber?: number;
}
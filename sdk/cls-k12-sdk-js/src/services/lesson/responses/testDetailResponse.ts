export interface TestDetailResponse {
    id?: number;
    name?: string;
    conditionsForCompletion?: string;
    contentGroupId?: number;
    scoreAfterFinish?: number;
    numberOfQuestion: number;
    totalScore: number | undefined;
    totalQuizScore: number | undefined;
    totalEssayScore: number | undefined;
}
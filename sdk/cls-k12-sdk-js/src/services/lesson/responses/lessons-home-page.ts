export interface LessonHomePage {
    id: number,
    lessonName: string,
    teacherId: number,
    avatarUrl: string,
    createdDate: Date,
    approvedDate: Date,
    endDate: Date,
    learnTime: number,
    interval: number,
    subjectId: number,
    percentageOfCompletion: number
}
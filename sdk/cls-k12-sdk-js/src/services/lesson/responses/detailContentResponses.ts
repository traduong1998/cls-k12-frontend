import { QuestionTest } from './../../question/models/question-test';
import { Content } from "../models/content";
import { Curriculum } from "../models/curriculum";

export class DetailContentResponses implements Content, Curriculum {
    id?: number;
    name?: string | undefined;
    typesOfContent?: string | undefined;
    conditionsForCompletion?: string | undefined;
    timeToComplete?: number;
    lessonId?: number | undefined;
    contentGroupId?: number;
    link?: string;
    linkId?: string;
    fileName?: string;
    fileId?: number;
    detailsOfContent?: string;
    allowToRewindAndFastForward?: boolean;
    testId?: number;
    virtualClassroomId?: number;
    description?: string;
    scoreAfterFinish?: number;
    listQuestion?: QuestionTest[];
    totalScore: number | undefined;
    totalQuizScore: number | undefined;
    totalEssayScore: number | undefined;
    testInfo? : TestInfo;
}

export class TestInfo {
    viewCorrectResultAfterSubmittingTheTest?: boolean;
    numberOfQuestionOnThePage?: number;
    testTime?: number;
    allowToRedo: boolean | undefined;
    theMaximumNumberToRedo?: number;
    isRandomizeQuestions: boolean | undefined;
}
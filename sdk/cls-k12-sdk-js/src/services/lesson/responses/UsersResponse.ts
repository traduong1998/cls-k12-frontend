
export interface UsersResponse {
    id: number;
    name: string;
    divisionId?: number;
    schoolId?: number;
}
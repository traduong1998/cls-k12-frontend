import { ContentResponses } from './contentResponses';

export interface ContentGroupResponses {
    id?: number;
    name: string;
    order?: number;
    step?: number;
    publicationDate? : Date
    contentGroups : ContentGroups[]
}

export interface ContentGroups {
    id?: number;
    name: string;
    order?: number;
    contents: ContentResponses[];
}



export interface content {
    id?: number;
    name: string;
    order?: number;
    contentGroupId: number;
}
export interface ContentGroupForCombobox {
    id?: number;
    name: string;
    order?: number;
}
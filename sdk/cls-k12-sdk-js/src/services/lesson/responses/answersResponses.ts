export interface AnswerResponse {
    id: number;
    questionId: number;
    questionGroupId: number
    content: string
    trueAnswer: number;
    position?: boolean;
    groupOfFillBlank2?: number;
    isShuffler: boolean;
}
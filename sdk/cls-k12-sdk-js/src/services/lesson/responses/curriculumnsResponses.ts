
export interface CurriculumnResponses {
    id: number;
    name: string;
    gradeId: number
    subjectId: number;
}
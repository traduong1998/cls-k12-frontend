

export interface SchoolResponses {
    id: number;
    name: string;
    divisionId?: number;
}
export class LessonInfoResponses {
    id: number | undefined;
    name: string | undefined;
    userId: number | undefined;
    avatar?: string;
    description?: string;
    status: string | undefined;
}
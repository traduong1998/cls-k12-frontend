
export class TeacherZoomResponses {
    userId?:number;
    publicKey?: string;
    privateKey?: string;
    email?: string;
}
export interface LessonUserInfo {
    id: number,
    name: string,

    teacherId: number,
    avatarUrl: string,

    introduction: string

    percentageOfCompletion:number
}
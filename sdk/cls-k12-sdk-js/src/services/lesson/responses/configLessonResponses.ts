export interface ConfigLessonResponses  {
    id: number;
    name: string;
    step: number;
    avatar: string;
    shareSetting: string;
    assessmentLevelGroupId: number;
    formOfLearn: string;
    learnTime: string;
    interval: number;
    startTimeLine: string;
}
export interface ListStudentForAssignedResponse {
    id: number;
    fullName: string;
    username: string;
    groupStudentName: string;
    gradeName: string;
    isApproval: boolean;
} 
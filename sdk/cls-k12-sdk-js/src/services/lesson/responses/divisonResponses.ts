
export interface DivisonResponses {
    id: number;
    name: string;
    code: number
}
export interface ContentResponses {
    id: number;
    name: string;
    order: number;
    typesOfContent:string
    status: string;
    contentGroupId: number;
}
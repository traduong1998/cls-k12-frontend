
export interface SaveFileResponses {
    id?: number;
    urlImage: string;
    urlVideo: string;
}
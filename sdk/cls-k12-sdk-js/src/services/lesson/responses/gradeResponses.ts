

export interface GradeResponses {
    id: number;
    name: string;
    schoolId: number;
}
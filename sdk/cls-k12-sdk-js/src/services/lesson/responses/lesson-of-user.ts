export interface LessonOfUser {
    id: number,
    name: string,
    teacherId: number,
    avatarUrl: string,
    registrationDate: Date,
    createdDate:Date,
    startDate:Date,
    approvedDate:Date,
    startTimeLine:string,
    learnTime: number,
    interval: number,
    subjectId: number,
    percentageOfCompletion: number,
    endDate: Date,
    startDateNull?:boolean
}
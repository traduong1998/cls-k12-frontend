export interface UserLessonResgistry {
    id: number,
    schoolId: number,
    gradeId: number,
    groupStudentId: number,
    lessonId: number,
    studentId: number,
    registrationDate: Date,
    isApproval:boolean;
}
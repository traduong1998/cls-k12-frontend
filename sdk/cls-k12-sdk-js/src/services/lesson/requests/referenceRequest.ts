import { number } from 'ngx-custom-validators/src/app/number/validator';


export interface ReferenceRequest {
    name: string;
    fileName: string
    link: string;
    fileId: string;
    lessonId: number;
    id?: number;
}
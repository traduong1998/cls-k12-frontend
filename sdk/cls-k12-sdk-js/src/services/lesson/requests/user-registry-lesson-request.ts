export interface UserRegistryLesson {
    lessonId: number,
    schoolId: number,
    gradeId: number,
    studentId: number
}
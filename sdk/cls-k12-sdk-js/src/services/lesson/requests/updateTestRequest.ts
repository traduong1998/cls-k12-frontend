import { QuestionTest } from "../../question/models/question-test";

export class UpdateTestRequest {
    id: number | undefined;
    name: string | undefined;
    lessonId?: number;
    contentGroupId?: number;
    conditionsForCompletion: string | undefined;
    viewCorrectResultAfterSubmittingTheTest: boolean | undefined;
    viewResultsAfterSubmitting: boolean | undefined;
    numberOfQuestionOnThePage: number | undefined;
    generateNumberRandomQuetions?: number;
    preserveTime: boolean | undefined;
    intervalWriteLog?: number;
    testTime?: number;
    allowToRedo: boolean | undefined;
    theMaximumNumberToRedo?: number;
    isRandomizeQuestions: boolean | undefined;
    numberOfQuestion: number | undefined;
    scoreAfterFinish?: number;
    objectUpdate: string[] | undefined;
    listQuestion? : QuestionTest[];
    totalScore: number | undefined;
    totalQuizScore: number | undefined;
    totalEssayScore: number | undefined;
    /**
     *
     */
    constructor() {
        this.listQuestion = [];
        this.objectUpdate = [];            
    }
}
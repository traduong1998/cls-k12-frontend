
export interface TroubleshootingFilterRequest {
    lessonId?: number;
    status?: string;
    startDate?: Date;
    endDate?: Date;
    page?: number;
    size?: number;
    getCount?: boolean;
    sortDirection?: string;
    sortField?: string
}
import { Content } from "../models/content";
import { Curriculum } from "../models/curriculum";

export class UpdateInfoContentRequest implements Content{
    id?: number;
    typeOfContent?: number;
    name?:string;
    conditionsForCompletion?: string;
    timeToComplete?: number;
    contentGroupId?: number;
    status?: string;
    curriculumId?: number;
}
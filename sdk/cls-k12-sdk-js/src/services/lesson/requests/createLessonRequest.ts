import { Lesson } from "../models/lesson";

export class CreateLessonRequest implements Lesson {
    id?: number;
    name: string;
    gradeId?: number;
    subjectId?: number;
    contentPanelId?: number;
    chapterId?: number;
    accessRange?: number;
    unitId?: number;
    createdDate?: string;
    userId?: number;
    status?: string;
    imageURL?: string;
    videoURL?: string;
    introductionLesson?:string;
    
    constructor() {
        this.name = '';
        this.createdDate = new Date().toLocaleString();
    }

}
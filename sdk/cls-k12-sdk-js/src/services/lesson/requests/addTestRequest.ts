import { QuestionTest } from "../../question/models/question-test";

export class AddTestRequest {
    id?: number;
    name: string | undefined;
    lessonId?: number;
    contentGroupId?: number;
    conditionsForCompletion: string | undefined;
    scoreAfterFinish?: number;
    viewCorrectResultAfterSubmittingTheTest: boolean | undefined;
    viewResultsAfterSubmitting: boolean | undefined;
    numberOfQuestionOnThePage: number | undefined;
    generateNumberRandomQuetions?: number;
    preserveTime: boolean | undefined;
    intervalWriteLog?: number;
    testTime?: number;
    allowToRedo: boolean | undefined;
    theMaximumNumberToRedo?: number;
    isRandomizeQuestions: boolean | undefined;
    numberOfQuestion: number | undefined;
    objectUpdate?: string[] | undefined;
    listQuestion? : QuestionTest[];
    totalScore: number | undefined;
    totalQuizScore: number | undefined;
    totalEssayScore: number | undefined;
}
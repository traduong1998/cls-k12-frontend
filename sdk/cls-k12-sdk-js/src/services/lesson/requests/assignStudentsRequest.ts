export interface AssignAndApprovalStudentRequest{
    lessonId: number;
    lessonStatus: string;
    listUserLesson: AssignAndApprovalStudentDetai[];
    assignFrom: string;
}
export interface AssignAndApprovalStudentDetai {
    id: number;
    action: string;
}

export interface AssignUserMeeting{
    meetingRoomId: number;
    listUserMeetings: AssignAndApprovalStudentDetai[];
}
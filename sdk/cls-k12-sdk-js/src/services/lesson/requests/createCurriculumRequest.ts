import { Content } from "../models/content";
import { Curriculum } from "../models/curriculum";

export class CreateCurriculumRequest implements Curriculum, Content {
    id?: number;//contentId
    typeOfCurriculum?: number;
    fileId?: number;
    link?: string;
    linkId?: string;
    fileName?: string;
    detailsOfContent?: string;
    allowToRewindAndFastForward?: boolean;
    virtualClassroomId?: number;
    description?: string;
}
import { PaginatorRequest } from "../../../core/api/requests/PaginatorRequest";

export class LessonFilterRequests implements PaginatorRequest {
  divisionId?: number;
  schoolId?: number;
  gradeId?: number;
  subjectId?: number;
  contentPanelId?: number;
  unitId?: number;
  createdDate?: Date;
  endDate?: Date;
  lessonName?: string;

  pageNumber?: number = 1;
  pageSize?: number = 1;
}

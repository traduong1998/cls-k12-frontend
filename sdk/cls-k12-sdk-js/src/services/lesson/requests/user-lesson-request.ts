export interface UserLessonRequest {
    lessonId: number,
    shareSetting: string,
    assignFrom: string,
    schoolId: number,
    gradeId: number
}
export interface ListStudentForAssignedRequest {
    lessonId: number;
    divisionId? : number;
    schoolId?: number;
    gradeId?: number;
    studentGroupId?: number;
}
export interface StudentRegisterRequest {
    lessonId: number;
    schoolId?: number;
    gradeId?: number;
    studentGroupId?: number;
    status: boolean;
}
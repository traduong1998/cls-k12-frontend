export interface LessonConfigRequest {
    id: number;
    shareSetting: string;
    assessmentLevelGroupId: number;
    formOfLearn: string;
    learnTime: string
    interval: number;
    startTimeLine: string;
}
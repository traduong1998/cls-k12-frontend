
export class TeacherZoomRequest {
    publicKey?: string;
    privateKey?: string;
    email?: string;
}
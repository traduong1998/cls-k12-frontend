
export interface OlClassRoomRequest {
    id?: number;
    name: string;
    groupContentId: number;
    startDate: Date;
    endDate: Date;
    decription?: string;
    lessonId: number;
}
import { AnswerResponse } from '../responses/answersResponses';
import { ContentTestResponse } from '../responses/contentTestResponses';
import { AddTestRequest } from '../requests/addTestRequest';
import axios, { AxiosResponse } from 'axios';
import { CLS } from '../../../CLS';
import { Logger } from '../../../Logger';
import { TestAPI } from './testAPI';
import { QuestionTest } from '../../question/models/question-test';

export class TestEndPoint implements TestAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }

    getListQuestionByTestId(testId: number): Promise<QuestionTest[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            let url = CLS.getConfig().apiBaseUrl;
            axios.get(`/assets/fakedata/data-question-list.json`)
                .then((resp: AxiosResponse<QuestionTest[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    addTestLesson(request: AddTestRequest): Promise<any> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/lessons/${request.lessonId}/contents/CreateTest`, request)
                .then((resp: AxiosResponse<any[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    editTestLessonBefore(request: AddTestRequest): Promise<any> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/lessons/${request.lessonId}/contents/UpdateTest-beforepublish`, request)
                .then((resp: AxiosResponse<any[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    editTestLessonAfter(request: AddTestRequest): Promise<any> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/lessons/${request.lessonId}/contents/UpdateTest-afterpublish`, request)
                .then((resp: AxiosResponse<any[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getContentTest(contentId: number, lessonId: number): Promise<ContentTestResponse> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/lessons/${lessonId}/contents/content-test?contentId=${contentId}`)
                .then((resp: AxiosResponse<ContentTestResponse>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getListAnswer(questionId: number): Promise<AnswerResponse[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/lessons/1/contents/list-answer/${questionId}`)
                .then((resp: AxiosResponse<AnswerResponse[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}

import { UsersResponse } from '../responses/UsersResponse';

export interface UsersAPI {

    getUsersByTypeId(typeId?: number, divisionId?: number, schoolId?: number): Promise<UsersResponse[]>
}
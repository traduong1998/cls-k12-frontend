import { QuestionTest } from "../../question/models/question-test";

export interface TestAPI {
    getListQuestionByTestId(testId: number): Promise<QuestionTest[]>;
}
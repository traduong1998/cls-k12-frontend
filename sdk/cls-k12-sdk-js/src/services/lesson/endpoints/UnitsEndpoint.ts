import axios, { AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import { UnitResponses } from "../responses/UnitResponses";
import { UnitsAPI } from "./unitsAPI";

export class UnitsEndpoint implements UnitsAPI {

    logger: Logger = Logger.getInstance();

    private baseUrl: string;

    constructor(config: { baseUrl: string }) {
        this.baseUrl = config.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api/v1';
    }

    getUnitsByChapterId(chapterId: number): Promise<UnitResponses[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/assets/fakedata/list-reference.json`)
                .then((resp: AxiosResponse<UnitResponses[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}
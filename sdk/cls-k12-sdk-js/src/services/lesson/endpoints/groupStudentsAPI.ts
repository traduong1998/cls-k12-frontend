import { GroupStudentsResponses } from '../responses/groupStudentsResponses';

export interface GroupStudentAPI {

    getGroupStudentsByGradeIdAndSchoolId(schoolId?: number, gradeId?: number): Promise<GroupStudentsResponses[]>;
}
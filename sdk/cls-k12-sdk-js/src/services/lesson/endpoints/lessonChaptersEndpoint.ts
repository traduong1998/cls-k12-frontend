import axios, { AxiosResponse } from "axios";
import { Logger } from "../../..";
import { CLS } from "../../../CLS";
import { LessonChaptersResponses } from "../responses/lessonChaptersResponses";
import { ChaptersAPI } from "./chaptersAPI";
import { lessonChaptersAPI } from "./lessonChaptersAPI";

export class lessonChaptersEndpoint implements lessonChaptersAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config: { baseUrl: string }) {
        this.baseUrl = config.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }

    getLessonChaptersByContentPanelId(contentPanelId: number): Promise<LessonChaptersResponses[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/`, { params: contentPanelId })
                .then((resp: AxiosResponse<LessonChaptersResponses[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}
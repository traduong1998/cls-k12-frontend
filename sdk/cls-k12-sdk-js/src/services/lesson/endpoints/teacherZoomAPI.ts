import { TeacherZoomRequest } from "../requests/teacherZoomRequest";
import { TeacherZoomResponses } from "../responses/teacherZoomResponses";

export interface TeacherZoomAPI {
    getInformationZoomAPI(): Promise<TeacherZoomResponses>;
    updateInformationZoomAPI(teacherZoom: TeacherZoomRequest): Promise<boolean>;
    createInformationZoomAPI(teacherZoom: TeacherZoomRequest): Promise<boolean>;
}
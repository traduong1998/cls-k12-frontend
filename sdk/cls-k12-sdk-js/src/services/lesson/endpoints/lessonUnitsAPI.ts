import { LessonChaptersResponses } from "../responses/lessonChaptersResponses";

export interface LessonUnitsAPI {

    getLessonUnitsByUnitId(unitId: number): Promise<LessonChaptersResponses[]>
}
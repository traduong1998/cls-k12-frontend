import { GradeResponses } from '../responses/gradeResponses';
import { CLS } from '../../../CLS';
import { GradeAPI } from './gradeAPI';
import { Logger } from '../../../Logger';
import axios, { AxiosError, AxiosResponse } from "axios";

export interface ListGradeOfSchool {
    listGradeOfSchool: GradeResponses[]
}

export class GradeEndpoint implements GradeAPI {

    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config: { baseUrl: string }) {
        this.baseUrl = config.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    getAllGrades(): Promise<GradeResponses[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Grades/GetGradeOptions`)
                .then((resp: AxiosResponse<GradeResponses[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    getGradeBySchoolId(schoolId?: number): Promise<ListGradeOfSchool> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Schools/GetGradeOfSchool?SchoolId=${schoolId}`)
                .then((resp: AxiosResponse<ListGradeOfSchool>) => {
                    let data: ListGradeOfSchool = resp.data;
                    data.listGradeOfSchool = resp.data.listGradeOfSchool;
                    resolve(data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}
import { UsersAPI } from './UserAPI';
import { UsersResponse } from '../responses/UsersResponse';
import { CLS } from '../../../CLS';
import { Logger } from '../../../Logger';
import axios, { AxiosResponse } from "axios";

export class UsersEndpoint implements UsersAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
    }
    getUsersByTypeId(typeId?: number, divisionId?: number, schoolId?: number): Promise<UsersResponse[]> {
        
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/`, { params: { typeId, divisionId, schoolId } })
                .then((resp: AxiosResponse<UsersResponse[]>) => {
                    console.log(`get from json`, resp);

                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}
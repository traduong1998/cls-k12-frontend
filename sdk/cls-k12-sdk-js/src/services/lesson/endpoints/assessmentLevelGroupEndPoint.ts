import { AssessmentLevelGroupAPI } from './assessmentLevelGroupAPI';
import { AssessmentLevelGroupResponses } from '../responses/assessmentLevelGroupResponses';
import { CLS, Logger } from '../../..';
import axios, { AxiosResponse } from 'axios';

export class AssessmentLevelGroupEndPoint implements AssessmentLevelGroupAPI {
  
    // TODO : Endpoint :
    endpoint = '/v1/learn';

    private baseUrl: string;
    logger: Logger = Logger.getInstance();

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }


    getAll(): Promise<AssessmentLevelGroupResponses[]> {
        this.logger.warn('usertype get3');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/AssessmentLevelGroup/get-list`)
                .then((resp: AxiosResponse<AssessmentLevelGroupResponses[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
import { DivisonResponses } from '../responses/divisonResponses';
import { Logger } from '../../../Logger';
import { CLS } from '../../../CLS';
import { DivisonAPI } from './divison-api';
import axios, { AxiosResponse } from "axios";

export interface Divisions {
    items: DivisonResponses[]
}

export class DivisionEndpoint implements DivisonAPI {
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = config?.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    endpoint = '/v1/learn';

    logger: Logger = Logger.getInstance();

    getAll(): Promise<Divisions> {
        return new Promise((resolve, reject) => {

            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/v1/Divisions/GetDivisionsPaging`)
                .then((resp: AxiosResponse<Divisions>) => {
                    let divisions: Divisions = resp.data;
                    divisions.items = resp.data.items;
                    resolve(divisions);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
import { ContentGroupForCombobox, ContentGroupResponses, ContentGroups } from '../responses/contentGroupResponses';
import { CLS } from '../../../CLS';
import { Logger } from '../../../Logger';
import axios, { AxiosError, AxiosResponse } from 'axios';
import { ContentGroupAPI } from './contentGroupAPI';
export class ContentGroupEndpoint implements ContentGroupAPI {

    endpoint = '/v1/learn';

    private baseUrl: string;
    logger: Logger = Logger.getInstance();

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }

    getListContentGroupByLessonId(lessonId: number): Promise<ContentGroupResponses[]> {
        this.logger.warn('usertype get3');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Lesson/${lessonId}/contents`)
                .then((resp: AxiosResponse<ContentGroupResponses[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }


    getListContentGroupForCombobox(lessonId: number): Promise<ContentGroupForCombobox[]> {
        this.logger.warn('usertype get3');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/lessons/${lessonId}/contentGroups/get-list`)
                .then((resp: AxiosResponse<ContentGroupForCombobox[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    deleteContentGroup(lessonId: number, id: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.delete(`${this.baseUrl}/lessons/${lessonId}/contentGroups/delete/${id}`)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    editContentGroup(lessonId: number, contentGroupId: number, name: string): Promise<ContentGroups> {
        let data = { lessonId: lessonId, id: contentGroupId, name: name }
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/lessons/${lessonId}/contentgroups/update/${contentGroupId}`, data)
                .then((resp: AxiosResponse<ContentGroups>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    addContentGroup(lessonId: number, name: string): Promise<ContentGroupResponses> {
        let data = { 'lessonId': lessonId, 'name': name }

        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/lessons/${lessonId}/ContentGroups/create`, data)
                .then((resp: AxiosResponse<ContentGroupResponses>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
}
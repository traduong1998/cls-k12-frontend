import { OlClassRoom } from "../models/olClassRoom";
import { OlClassRoomRequest } from "../requests/olClassRoomRequest";

export interface OlClassRoomAPI {

    createOlClassRoom(classRoom: OlClassRoomRequest): Promise<OlClassRoom>;
}
import { GradeResponses } from '../responses/gradeResponses';
import { ListGradeOfSchool } from './gradeEndpoint';

export interface GradeAPI {

    getAllGrades() : Promise<GradeResponses[]>;
    getGradeBySchoolId(schoolId?: number): Promise<ListGradeOfSchool>;
}
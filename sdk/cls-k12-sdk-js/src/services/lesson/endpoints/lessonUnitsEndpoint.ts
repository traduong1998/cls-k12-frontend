import { LessonUnitsAPI } from './lessonUnitsAPI';
import { CLS } from '../../../CLS';
import { Logger } from '../../../Logger';
import axios, { AxiosResponse } from "axios";
import { LessonChaptersResponses } from "../responses/lessonChaptersResponses";

export class LessonUnitsEndpoint implements LessonUnitsAPI {

    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config: { baseUrl: string }) {
        this.baseUrl = config.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }

    getLessonUnitsByUnitId(unitId: number): Promise<LessonChaptersResponses[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/`, { params: unitId })
                .then((resp: AxiosResponse<LessonChaptersResponses[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}
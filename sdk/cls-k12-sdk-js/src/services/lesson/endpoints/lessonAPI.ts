import { Lesson } from '../models/lesson';
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { CreateLessonRequest } from "../requests/createLessonRequest";
import { LessonConfigRequest } from "../requests/lessonConfigRequest";
import { ConfigLessonResponses } from "../responses/configLessonResponses";
import { LessonInfoResponses } from "../responses/lessonInfoResponses";
import { LessonOfUser } from '../responses/lesson-of-user';
import { LessonUserInfo } from '../responses/lesson-user-info';
import { ReferenceByLesson } from '../responses/reference-by-lesson';
import { LessonReference } from '../responses/lesson-reference';
import { ContentGroupResponses } from '../responses/contentGroupResponses';
import { UserLesson } from '../responses/lesson-user-response';
import { UserLessonResgistry } from '../responses/user-lesson-resgistry-response';
import { UserRegistryLesson } from '../requests/user-registry-lesson-request';
import { UserLessonRequest } from '../requests/user-lesson-request';
import { LessonHomePage } from '../responses/lessons-home-page';

export interface LessonAPI {
    getLessonByIdAndUserId(lessonid:number,userId:number):Promise<LessonUserInfo>;
    getLessonOfUser(userId: number, gradeId: number, subjectId: number, lessonName: string, sortDirection: string, sortField:string, pageNumber:number, pageSize:number): Promise<PaginatorResponse<LessonOfUser>>;
    getReferenceByLessonId(lessionId:number):Promise<ReferenceByLesson>;
    getLessonInfoById(id: number): Promise<Lesson>;

    getLessonsHomePage(portalId:number,divisionId:number,schoolId:number,gradeId:number, subjectId:number, userId:number,chapterId:number, unitId:number,lessonName:string, pageNumber:number, pageSize:number):Promise<PaginatorResponse<LessonHomePage>>;
    
    getReferenceLesson(gradeId:number,subjectId:number):Promise<LessonReference[]>;
    getLessonContentsById(lessonId: number): Promise<ContentGroupResponses>
    getLessonContentsInfoById(lessonId: number): Promise<ContentGroupResponses>
    getLessonUser(userId:number, lessonId:number):Promise<UserLesson>
    getLessonUserRegistry(userId:number, lessonId:number):Promise<UserLessonResgistry>

    createUserLesson(userLesson:UserLessonRequest):Promise<boolean>
    createUserLessonInLesson(userLesson:UserLessonRequest):Promise<UserLessonRequest>

    createLesson(lesson: CreateLessonRequest): Promise<Lesson>;
    getLessonFilter(informationFilter: any): Promise<PaginatorResponse<Lesson>>;
    
    getLessonConfig(id: number): Promise<ConfigLessonResponses>;
    getLessonById(id: number): Promise<Lesson>;
    getLessonInfo(lessonId: number): Promise<LessonInfoResponses>;
    addLessonConfig(lessonConfig: LessonConfigRequest): Promise<ConfigLessonResponses>;
    approvalLesson(lessonId: number): Promise<Lesson>;
    returnLesson(lessonId: number): Promise<Lesson>;
    submitLesson(lessonId: number): Promise<Lesson>;
    editInfomationLesson(lesson: Lesson): Promise<Lesson>;
    //getListLessonHome(queryParams: any): Promise<PaginatorResponse<Lesson>>;
    // getListLessonHomePage(queryParams: any): Promise<PaginatorResponse<Lesson>>;
    // getListLessonLatestHomePage(): Promise<Lesson[]>;

    getMyLesson(gradeId: number, subjectId: number, lessonName: string, sortDirection: string, sortField:string, pageNumber:number, pageSize:number): Promise<PaginatorResponse<LessonOfUser>>;
}
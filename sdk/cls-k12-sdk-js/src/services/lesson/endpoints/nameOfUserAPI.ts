import { NameOfUserResponses } from "../responses/nameOfUserResponses";

export interface NameOfUserAPI {
    getListNameOfUser(ids: number[]): Promise<NameOfUserResponses[]>;
}
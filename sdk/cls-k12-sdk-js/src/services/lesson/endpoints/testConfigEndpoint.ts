import { TestConfigAPI } from './testConfigAPI';
import { TestConfigResponses } from '../responses/testConfigResponses';
import { CLS } from '../../../CLS';
import { Logger } from '../../../Logger';
import axios, { AxiosResponse } from 'axios';

export class TestConfigEndpoint implements TestConfigAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config: { baseUrl: string }) {
        this.baseUrl = config.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    getTestConfig(contentId: number): Promise<TestConfigResponses> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/assets/fakedata/test-config.json`)
                .then((resp: AxiosResponse<TestConfigResponses>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
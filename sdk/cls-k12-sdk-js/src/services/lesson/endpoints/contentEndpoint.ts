import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { ErrorResponse } from "../../../core/api/responses/ErrorResponse ";
import { Logger } from "../../../Logger";
import { Content } from "../models/content";
import { VirtualClassRoom } from "../models/virtualClassRoom";
import { CreateCurriculumRequest } from "../requests/createCurriculumRequest";
import { DetailContentRequests } from "../requests/detailContentRequests";
import { UpdateInfoContentRequest } from "../requests/updateInfoContentRequest";
import { DetailContentResponses } from "../responses/detailContentResponses";
import { ContentAPI } from "./contentAPI";

type NewType = Logger;

export class ContentEndpoint implements ContentAPI {
    // TODO : Endpoint :
    endpoint = '/v1/lessons';

    private baseUrl: string;
    logger: NewType = Logger.getInstance();

    constructor(config?: { baseUrl: string }) {
        console.log(`ssss:`, CLS.getConfig().apiBaseUrl);
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    updateContent(detailContentRequests: DetailContentRequests): Promise<Content> {
        throw new Error("Method not implemented.");
    }

    createContent(detailContentRequest: DetailContentRequests): Promise<DetailContentRequests> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/lessons/${detailContentRequest.lessonId}/contents/Create`, detailContentRequest)
                .then((resp: AxiosResponse<DetailContentRequests>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        });
    }

    getContent(idLesson: number, idContent: number): Promise<DetailContentResponses> {
        this.logger.warn('usertype get3');
        console.log("idContentidContent", idContent);
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/lessons/${idLesson}/contents/get-content/${idContent}`)
                .then((resp: AxiosResponse<DetailContentResponses>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getVirtualClassRoom(idLesson: number, idContent: number): Promise<VirtualClassRoom> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/lessons/${idLesson}/contents/virtualclassroom/${idContent}`)
                .then((resp: AxiosResponse<VirtualClassRoom>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    updateVirClassroomUnpublish(value: Content): Promise<Content> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/lessons/${value.lessonId}/contents/content/${value.id}/update-virtualclassroom-unreleased`, value)
                .then((resp: AxiosResponse<Content>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        });
    }

    updateVirClassroomPublish(value: Content): Promise<Content> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/lessons/${value.lessonId}/contents/content/${value.id}/update-virtualclassroom-released`, value)
                .then((resp: AxiosResponse<Content>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        });
    }

    updateContentBeforPublish(detailContentRequest: DetailContentRequests): Promise<Content> {
        this.logger.warn('usertype get3');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/lessons/${detailContentRequest.lessonId}/contents/update-content-before-publish`, detailContentRequest)
                .then((resp: AxiosResponse<Content>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    updateContentAfterPublish(detailContentRequest: DetailContentRequests): Promise<Content> {
        this.logger.warn('usertype get3');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/lessons/${detailContentRequest.lessonId}/contents/update-content-after-publish`, detailContentRequest)
                .then((resp: AxiosResponse<Content>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    createCurriculum(idLesson: number, createCurriculumRequest: CreateCurriculumRequest): Promise<Content> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/lessons/${idLesson}/contents/create-curriculum`, createCurriculumRequest)
                .then((resp: AxiosResponse<Content>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    alert('Thêm thất bại');
                    console.error('cccccc', error);
                    reject(error);
                });
        });
    }

    updateInfoContent(idLesson: number, updateInfoContentRequest: UpdateInfoContentRequest): Promise<Content> {
        this.logger.warn('usertype get3');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/lessons/${idLesson}/contents/update-info-content`, updateInfoContentRequest)
                .then((resp: AxiosResponse<Content>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    deleteContent(idLesson: number, idContent: number): Promise<Content> {
        this.logger.warn('usertype get3');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.delete(`${this.baseUrl}/lessons/${idLesson}/contents/delete-content/${idContent}`)
                .then((resp: AxiosResponse<Content>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    alert('xóa thất bại');
                    console.error(error);
                    reject(error);
                });
        });
    }
}
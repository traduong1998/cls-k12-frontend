import { ReferencesAPI } from './referencesAPI';
import { ReferenceResponses } from '../responses/referenceResponses';
import { CLS } from '../../../CLS';
import { Logger } from '../../../Logger';
import axios, { AxiosResponse } from 'axios';
import { References } from '../models/references';
import { ReferenceRequest } from '../requests/referenceRequest';

export class ReferencesEndpoint implements ReferencesAPI {
    /**
     *
     */

    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    addReference(reference: ReferenceRequest): Promise<ReferenceResponses> {
        
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/reference/addreference`, reference)
                .then((resp: AxiosResponse<ReferenceResponses>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    removeReference(lessonId: number, refId: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let data = { id: refId };
            axios.delete(`${this.baseUrl}/reference/remove/${refId}`, { data: data })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    editReference(reference: References): Promise<References> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/reference/edit/${reference.id}`, reference)
                .then((resp: AxiosResponse<References>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getReferenceById(id: number): Promise<References> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/reference/${id}/get-reference`)
                .then((resp: AxiosResponse<References>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getListReferenceByLessonId(lessonId: number): Promise<ReferenceResponses[]> {
        this.logger.warn('usertype get3');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/reference/getreference?LessonId=${lessonId}`)
                .then((resp: AxiosResponse<ReferenceResponses[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getListReferenceByLessonIdLearner(lessonId: number): Promise<ReferenceResponses[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/learners/${lessonId}/references`)
                .then((resp: AxiosResponse<ReferenceResponses[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
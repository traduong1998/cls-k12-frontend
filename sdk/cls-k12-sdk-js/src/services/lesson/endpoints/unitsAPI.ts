import { UnitResponses } from '../responses/UnitResponses';

export interface UnitsAPI {

    getUnitsByChapterId(chapterId: number): Promise<UnitResponses[]>;
}
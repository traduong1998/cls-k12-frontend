import { TestConfigResponses } from '../responses/testConfigResponses';
export interface TestConfigAPI {
    getTestConfig(contentId: number): Promise<TestConfigResponses>;
}
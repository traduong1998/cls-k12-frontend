import axios, { AxiosError, AxiosResponse } from "axios";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { CLS } from "../../../CLS";
import { ErrorResponse } from "../../../core/api/responses/ErrorResponse ";
import { Logger } from "../../../Logger";
import { ZoomMeeting } from "../../learner-lesson/models/zoom-meeting";
import { VirtualClassRoom } from "../models/virtualClassRoom";
import { VirtualClassroomAPI } from "./virtual-classrom-api";


export class VirtualClassRoomEndpoint implements VirtualClassroomAPI {

    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }

    getListOnlineTeachingFilter(request?: any): Promise<PaginatorResponse<VirtualClassRoom>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/VirtualClassrooms`, { params: request })
                .then((resp: AxiosResponse<PaginatorResponse<VirtualClassRoom>>) => {

                    console.log("VirtualClassrooms", resp.data)
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        });
    }

    getMeeting(contentId: number, lesssonId: number, isUpdateDateVirClassroom?: boolean): Promise<ZoomMeeting> {
        return new Promise((resolve, reject) => {
            if (!isUpdateDateVirClassroom) {
                isUpdateDateVirClassroom = false;
            }
            let request = { contentId: contentId, lesssonId: lesssonId, isUpdateDateVirClassroom: isUpdateDateVirClassroom };
            console.log("request get meeting", request)
            axios.get(`${this.baseUrl}/VirtualClassrooms/meeting/${contentId}`, { params: request })
                .then((resp: AxiosResponse<ZoomMeeting>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        });
    }

    createMeeting(contentId: number, lessonId: number): Promise<ZoomMeeting> {
        {
            return new Promise((resolve, reject) => {
                let request = { contentId: contentId, lessonId: lessonId };
                axios.post(`${this.baseUrl}/VirtualClassrooms/meeting`, request)
                    .then((resp: AxiosResponse<ZoomMeeting>) => {
                        resolve(resp.data);
                    })
                    .catch((error: AxiosError<ErrorResponse>) => {
                        reject(error.response?.data);
                    });
            });
        }
    }

    checkMeeting(lessonId: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/VirtualClassrooms/meeting/check-meeting?lessonId=${lessonId}`)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        });
    }
}
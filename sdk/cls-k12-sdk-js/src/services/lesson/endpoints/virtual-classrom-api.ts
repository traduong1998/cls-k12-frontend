import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { ZoomMeeting } from "../../learner-lesson/models/zoom-meeting";
import { VirtualClassRoom } from "../models/virtualClassRoom";


export interface VirtualClassroomAPI {
    getListOnlineTeachingFilter(request?: any):Promise<PaginatorResponse<VirtualClassRoom>>;
    getMeeting(contentId: number, lesssonId: number): Promise<ZoomMeeting>;
    createMeeting(contentId: number, lesssonId: number): Promise<ZoomMeeting>;
    checkMeeting(lessonId: number): Promise<boolean>;
}
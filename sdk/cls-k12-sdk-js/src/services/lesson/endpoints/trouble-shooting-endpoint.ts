import axios, { AxiosResponse } from "axios";
import { param } from "jquery";
import { CLS } from "../../../CLS";
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { Troubleshooting } from "../models/troubleshooting";
import { TroubleshootingFilterRequest } from "../requests/troubleshooting-filter-request";
import { TroubleshootingAPI } from "./trouble-shooting-api";
export interface LessonOption {
    id: number,
    name: string
}

export class TroubleshootingEndpoint implements TroubleshootingAPI {
    private baseUrl: string = '';
    constructor(config?: { baseUrl: string }) {
        console.log(`CLS.getConfig().apiBaseUrl: `, CLS.getConfig().apiBaseUrl);
        this.baseUrl = CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    createFeedbackTroubleshooting(requset: any): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/CommentTeacher/CreateAnswerForTeacher`, requset)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    updateFeedbackTroubleshooting(resquest: any): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/CommentTeacher/UpdateAnswerForTeacher`, resquest)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getListTroubleshootingPaging(request?: TroubleshootingFilterRequest): Promise<PaginatorResponse<Troubleshooting>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/CommentTeacher/GetQuestionAndAnswerForTeacherPaging`,
                {
                    params: request
                })
                .then((resp: AxiosResponse<PaginatorResponse<Troubleshooting>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getLessons(): Promise<LessonOption[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/CommentTeacher/GetLessonCommentTeacher`)
                .then((resp: AxiosResponse<LessonOption[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
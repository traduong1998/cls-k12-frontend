import { SaveFileAPI } from './saveFileAPI';
import axios, { AxiosResponse } from "axios";
import { Logger, CLS } from "../../..";
import { SaveFileResponses } from "../responses/saveFileResponses";

export class SaveFileEndpoint implements SaveFileAPI {
    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();
    
    // TODO : Endpoint :
    endpoint = '/v1/learn';

    constructor(config: { baseUrl: string }) {
        this.baseUrl = config.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    
    saveFileData(formData: FormData): Promise<SaveFileResponses> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}`, formData)
                .then((resp: AxiosResponse<SaveFileResponses>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}
import { CLS } from '../../../CLS';
import { Logger } from '../../../Logger';
import axios, { AxiosResponse } from "axios";
import { ChaptersResponses } from "../responses/chaptersResponses";
import { ChaptersAPI } from "./chaptersAPI";


export class ChaptersEndpoint implements ChaptersAPI {

    logger: Logger = Logger.getInstance();
    
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = config?.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api/v1';
    }

    getChaptersBycurriculumId(contentPanelId: number): Promise<ChaptersResponses[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/`, { params: contentPanelId })
                .then((resp: AxiosResponse<ChaptersResponses[]>) => {
                    console.log(`get from json`, resp);

                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}
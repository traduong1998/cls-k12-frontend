import { ListStudentRegisterResponse } from '../responses/listStudentRegisterResponse';
import axios, { AxiosResponse } from 'axios';
import { CLS } from '../../../CLS';
import { ListStudentForAssignedRequest } from '../requests/listStudentForAssignedRequest';
import { StudentRegisterRequest } from '../requests/listStudentRegisterRequest';
import { ListStudentForAssignedResponse } from '../responses/listStudentForAssignedResponse';
import { Logger } from '../../../Logger';
import { AssignStudentsAPI } from './assignStudentAPI';
import { AssignAndApprovalStudentRequest } from '../requests/assignStudentsRequest';
import { StudentAssignPagingRequest } from '../models/StudentAssignPagingRequest';
import { PaginatorResponse } from '../../../../core/api/responses/PaginatorResponse';
import { StudentRegistrationPagingRequest } from '../models/StudentRegistrationPagingRequest';
export class AssignStudentsEndpoint implements AssignStudentsAPI {
    // TODO : Endpoint :
    endpoint = '/v1/lessons';

    private baseUrl: string;
    logger: Logger = Logger.getInstance();

    constructor(config?: { baseUrl: string }) {
        console.log(`ssss:`, CLS.getConfig().apiBaseUrl);
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    getListStudentForAssigned(request: StudentAssignPagingRequest): Promise<PaginatorResponse<ListStudentForAssignedResponse>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/LessonAssign/GetLessonStudentInfoPaging`, { params: {
                DivisionId: request.DivisionId, SchoolId: request.SchoolId,GradeId: request.GradeId,GroupStudentId: request.GroupStudentId,
                LessonId: request.LessonId, schoolYear: request.schoolYear, SortField: request.SortField,SortDirection: request.SortDirection,
                Page: request.pageNumber,  Size:request.sizeNumber ,GetCount: request.getCount
            } })
                .then((resp: AxiosResponse<PaginatorResponse<ListStudentForAssignedResponse>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getListStudentRegister(request: StudentRegistrationPagingRequest): Promise<PaginatorResponse<ListStudentRegisterResponse>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/LessonAssign/GetLessonStudentRegistrationPaging`, { params: {
                SchoolId: request.SchoolId,GradeId: request.GradeId,GroupStudentId: request.GroupStudentId, Status: request.Status,
                LessonId: request.LessonId, schoolYear: request.schoolYear, SortField: request.SortField,SortDirection: request.SortDirection,
                Page: request.pageNumber,  Size:request.sizeNumber ,GetCount: request.getCount
            } })
                .then((resp: AxiosResponse<PaginatorResponse<ListStudentRegisterResponse>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    
    assignStudentsForLesson(request: AssignAndApprovalStudentRequest): Promise<ListStudentForAssignedResponse[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/LessonAssign/AddListUserLesson`, request)
                .then((resp: AxiosResponse<ListStudentForAssignedResponse[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
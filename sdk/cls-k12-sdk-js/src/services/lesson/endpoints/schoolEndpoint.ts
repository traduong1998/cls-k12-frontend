import { CLS } from '../../../CLS';
import { Logger } from '../../../Logger';
import { SchoolResponses } from '../responses/schoolResponses';
import axios, { AxiosResponse } from "axios";

import { SchoolAPI } from "./schoolAPI";


export class SchoolEnpoint implements SchoolAPI {

    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config: { baseUrl: string }) {
        this.baseUrl = config.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }

    getSchoolsByDivisionId(divisionId?: number): Promise<SchoolResponses[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Schools/GetSchoolOptions`, { params: divisionId })
                .then((resp: AxiosResponse<SchoolResponses[]>) => {
                    console.log(`get from json`, resp);

                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}
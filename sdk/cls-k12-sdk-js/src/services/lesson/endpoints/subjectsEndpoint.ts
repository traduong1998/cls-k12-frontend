import axios, { AxiosError, AxiosResponse } from "axios";
import { Logger, CLS } from "../../..";
import { SubjectOption } from "../../manage/models/subject";
import { SubjectResponses } from "../responses/subjectResponses";
import { SubjectAPI } from "./subjectAPI";


export interface ListSubjectOfGrade {
    listSubjectOfGrade: SubjectResponses[]
}

export class SubjectsEndpoint implements SubjectAPI {

    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    getSubjectByGradeId(gradeId?: number): Promise<ListSubjectOfGrade> {

        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Grades/GetSubjectOfGrade?GradeId=${gradeId}`)
                .then((resp: AxiosResponse<ListSubjectOfGrade>) => {
                    let data: ListSubjectOfGrade = resp.data
                    data.listSubjectOfGrade = resp.data.listSubjectOfGrade;
                    resolve(data);
                    console.log("🚀 ~ file: subjectsEndpoint.ts ~ line 28 ~ SubjectsEndpoint ~ .then ~ data", data)
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getSubjectByGradeIdOptions(gradeId?: number): Promise<SubjectOption[]> {

        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Subjects/GetSubjectOptions`, { params: { gradeId: gradeId } })
                .then((resp: AxiosResponse<SubjectOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
}
import { SchoolResponses } from '../responses/schoolResponses';

export interface SchoolAPI {

    getSchoolsByDivisionId(divisionId?: number): Promise<SchoolResponses[]>;

}
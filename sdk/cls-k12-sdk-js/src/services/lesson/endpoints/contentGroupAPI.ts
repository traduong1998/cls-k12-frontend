import { ContentGroupResponses, ContentGroups } from '../responses/contentGroupResponses';

export interface ContentGroupAPI {
    getListContentGroupByLessonId(lessonId: number): Promise<ContentGroupResponses[]>;

    addContentGroup(lessonId: number, contentGroupName: string): Promise<ContentGroupResponses>;

    deleteContentGroup(lessonId: number, id: number): Promise<boolean>;

    editContentGroup(lessonId: number, contentGroupId: number, name: string): Promise<ContentGroups>
}
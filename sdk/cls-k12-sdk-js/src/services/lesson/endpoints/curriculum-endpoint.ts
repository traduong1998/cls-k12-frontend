import { CurriculumnResponses } from '../responses/curriculumnsResponses';
import { CLS } from '../../../CLS';
import { Logger } from '../../../Logger';
import axios, { AxiosResponse } from "axios";
import { CurriculumsAPI } from './curriculumnsAPI';

export class CurriculumEndpoint implements CurriculumsAPI {

    logger: Logger = Logger.getInstance();

    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = config?.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }

    getCuriculumByGradeIdAndSubjectId(gradeId?: number, subjectId?: number): Promise<CurriculumnResponses[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/`, { params: { gradeId, subjectId } })
                .then((resp: AxiosResponse<CurriculumnResponses[]>) => {

                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
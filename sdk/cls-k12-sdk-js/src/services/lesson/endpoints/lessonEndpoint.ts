import axios, { AxiosResponse } from "axios";
import { LessonAPI } from "../..";
import { CLS } from "../../../CLS";
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { Logger } from "../../../Logger";
import { Lesson } from "../models/lesson";
import { CreateLessonRequest } from "../requests/createLessonRequest";
import { LessonConfigRequest } from "../requests/lessonConfigRequest";
import { UserLessonRequest } from "../requests/user-lesson-request";
import { UserRegistryLesson } from "../requests/user-registry-lesson-request";
import { ConfigLessonResponses } from "../responses/configLessonResponses";
import { ContentGroupResponses } from "../responses/contentGroupResponses";
import { LessonOfUser } from "../responses/lesson-of-user";
import { LessonReference } from "../responses/lesson-reference";
import { LessonUserInfo } from "../responses/lesson-user-info";
import { UserLesson } from "../responses/lesson-user-response";
import { LessonInfoResponses } from "../responses/lessonInfoResponses";
import { LessonHomePage } from "../responses/lessons-home-page";
import { ReferenceByLesson } from "../responses/reference-by-lesson";
import { UserLessonResgistry } from "../responses/user-lesson-resgistry-response";

export class LessonEndpoint implements LessonAPI {

    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();
    constructor(config?: { baseUrl: string }) {
        console.log(`CLS.getConfig().apiBaseUrl: `, CLS.getConfig().apiBaseUrl);
        this.baseUrl = CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    getMyLesson(gradeId: number, subjectId: number, lessonName: string, sortDirection: string, sortField: string, pageNumber: number, pageSize: number): Promise<PaginatorResponse<LessonOfUser>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/learners/GetMyLessonPagging`, { params: { gradeId: gradeId, subjectId: subjectId, lessonName: lessonName, 'Filter.SortDirection': sortDirection, 'Filter.SortField': sortField, Page: pageNumber, Size: pageSize, GetCount: true } })
                // axios.get(`http://localhost:59909/api/v1/learners/GetMyLessonPagging`, { params: { gradeId: gradeId, subjectId: subjectId, lessonName: lessonName, 'Filter.SortDirection': sortDirection, 'Filter.SortField': sortField, Page: pageNumber, Size: pageSize, GetCount: true } })
                .then((resp: AxiosResponse<PaginatorResponse<LessonOfUser>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getLessonsHomePage(portalId: number, divisionId: number, schoolId: number, gradeId: number, subjectId: number, TeacherId: number, chapterId: number, unitId: number, lessonName: string, page: number, Size: number): Promise<PaginatorResponse<LessonHomePage>> {
        return new Promise((resolve, reject) => {

            axios.get(`${this.baseUrl}/learn/global/getlistlessonbydomain`, { params: { portalId: portalId, divisionId: divisionId, schoolId: schoolId, gradeId: gradeId, subjectId: subjectId, TeacherId: TeacherId, chapterId: chapterId, unitId: unitId, lessonName: lessonName, page: page, Size: Size, GetCount: true } })
            // axios.get(`http://localhost:58910/api/v1/learn/global/getlistlessonbydomain`, { params: { portalId: portalId, divisionId: divisionId, schoolId: schoolId, gradeId: gradeId, subjectId: subjectId, TeacherId: TeacherId, chapterId: chapterId, unitId: unitId, lessonName: lessonName, page: page, Size: Size, GetCount: true } })
                .then((resp: AxiosResponse<PaginatorResponse<LessonHomePage>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }


    createUserLessonInLesson(userLesson: UserLessonRequest): Promise<UserLessonRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/Lesson/AddListUserLesson`, userLesson)
                .then((resp: AxiosResponse<UserLessonRequest>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    createUserLesson(userLesson: UserLessonRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/lessonassign/adduserlessonfromhomepage`, userLesson)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getLessonUserRegistry(userId: number, lessonId: number): Promise<UserLessonResgistry> {
        return new Promise((resolve, reject) => {

            axios.get(`${this.baseUrl}/LessonAssign/GetUserLessonRegistry`, { params: { UserId: userId, LessonId: lessonId } })
                .then((resp: AxiosResponse<UserLessonResgistry>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getLessonUser(userId: number, lessonId: number): Promise<UserLesson> {
        return new Promise((resolve, reject) => {

            axios.get(`${this.baseUrl}/LessonAssign/GetUserLesson`, { params: { UserId: userId, LessonId: lessonId } })
                .then((resp: AxiosResponse<UserLesson>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getReferenceLesson(gradeId: number, subjectId: number): Promise<LessonReference[]> {
        return new Promise((resolve, reject) => {

            axios.get(`${this.baseUrl}/learn/global/lessons/references/${gradeId}`, { params: { subjectId: subjectId } })
                .then((resp: AxiosResponse<LessonReference[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getReferenceByLessonId(lessonId: number): Promise<ReferenceByLesson> {
        return new Promise((resolve, reject) => {

            axios.get(`${this.baseUrl}/learn/global/lessons/${lessonId}/reference`)
                .then((resp: AxiosResponse<ReferenceByLesson>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getLessonByIdAndUserId(lessonid: number, userId: number): Promise<LessonUserInfo> {
        return new Promise((resolve, reject) => {

            axios.get(`${this.baseUrl}/Lesson/${lessonid}/users/${userId}`)
                .then((resp: AxiosResponse<LessonUserInfo>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getLessonOfUser(userId: number, gradeId: number, subjectId: number, lessonName: string, sortDirection: string, sortField: string, pageNumber: number, pageSize: number): Promise<PaginatorResponse<LessonOfUser>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/learners/${userId}/lessons`, { params: { gradeId: gradeId, subjectId: subjectId, lessonName: lessonName, sortDirection: sortDirection, sortField: sortField, pageNumber: pageNumber, pageSize: pageSize } })
                .then((resp: AxiosResponse<PaginatorResponse<LessonOfUser>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }




    getLessonInfo(lessonId: number): Promise<LessonInfoResponses> {
        throw new Error("Method not implemented.");
    }
    submitLesson(lessonId: number): Promise<Lesson> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/Lesson/${lessonId}/request-to-approval`)
                .then((resp: AxiosResponse<Lesson>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getLessonFilter(informationFilter: any): Promise<PaginatorResponse<Lesson>> {
        this.logger.warn('usertype get3');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/lesson`, { params: informationFilter })
                .then((resp: AxiosResponse<PaginatorResponse<Lesson>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getLessonContentsById(lessonId: number): Promise<ContentGroupResponses> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Lesson/${lessonId}/contents`)
                .then((resp: AxiosResponse<ContentGroupResponses>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getLessonContentsInfoById(lessonId: number): Promise<ContentGroupResponses> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/learn/global/lessons/${lessonId}/contents`)
                .then((resp: AxiosResponse<ContentGroupResponses>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    editInfomationLesson(lesson: Lesson): Promise<Lesson> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/lesson/update/infor/${lesson.id}`, lesson)
                .then((resp: AxiosResponse<Lesson>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    deleteLessonById(id: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.delete(`${this.baseUrl}/lesson/delete/${id}`)
                .then((resp: AxiosResponse<boolean>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getLessonById(id: number): Promise<Lesson> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Lesson/${id}`)
                .then((resp: AxiosResponse<Lesson>) => {
                    resolve(resp.data);
                    console.log("🚀 ~ file: lessonEndpoint.ts ~ line 89 ~ LessonEndpoint ~ .then ~ resp.data", resp.data)
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getLessonInfoById(id: number): Promise<Lesson> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/learn/global/lessons/${id}`)
                .then((resp: AxiosResponse<Lesson>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getLessonConfig(id: number): Promise<ConfigLessonResponses> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Lesson/config/${id}`)
                .then((resp: AxiosResponse<ConfigLessonResponses>) => {
                    console.log(`Get lesson config from api: `, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    createLesson(lesson: CreateLessonRequest): Promise<Lesson> {
        console.log("🚀 ~ file: lessonEndpoint.ts ~ line 98 ~ LessonEndpoint ~ returnnewPromise ~ lesson", lesson)
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/Lesson/create`, lesson)
                .then((resp: AxiosResponse<Lesson>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    addLessonConfig(lessonConfig: LessonConfigRequest): Promise<ConfigLessonResponses> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/Lesson/config`, lessonConfig)
                .then((resp: AxiosResponse<ConfigLessonResponses>) => {
                    console.log(`set lesson config`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    approvalLesson(lessonId: number): Promise<Lesson> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/Lesson/${lessonId}/approval`)
                .then((resp: AxiosResponse<Lesson>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    returnLesson(lessonId: number): Promise<Lesson> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.put(`${this.baseUrl}/Lesson/${lessonId}/return`)
                .then((resp: AxiosResponse<Lesson>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    // getListLessonHome(queryParams: any): Promise<PaginatorResponse<Lesson>> {
    //     this.logger.warn('usertype get3');
    //     return new Promise((resolve, reject) => {
    //         // TODO : pass URL
    //         axios.get(`${this.baseUrl}/Lesson/get-list-lesson-home/`, { params: queryParams })
    //             .then((resp: AxiosResponse<PaginatorResponse<Lesson>>) => {
    //                 console.log(`get from json`, resp);
    //                 resolve(resp.data);
    //             })
    //             .catch(error => {
    //                 console.error(error);
    //                 reject(error);
    //             });
    //     });
    // }
    // getListLessonHomePage(queryParams: any): Promise<PaginatorResponse<Lesson>> {
    //     this.logger.warn('usertype get3');
    //     return new Promise((resolve, reject) => {
    //         // TODO : pass URL
    //         axios.get(`${this.baseUrl}/Lesson/get-list-lesson-home-page/`, { params: queryParams })
    //             .then((resp: AxiosResponse<PaginatorResponse<Lesson>>) => {
    //                 console.log(`get from json`, resp);
    //                 resolve(resp.data);
    //             })
    //             .catch(error => {
    //                 console.error(error);
    //                 reject(error);
    //             });
    //     });
    // }
    // getListLessonLatestHomePage(queryParams?: any): Promise<Lesson[]> {
    //     this.logger.warn('usertype get3');
    //     return new Promise((resolve, reject) => {
    //         // TODO : pass URL
    //         axios.get(`${this.baseUrl}/Lesson/get-list-lesson-latest-home-page/`, { params: queryParams })
    //             .then((resp: AxiosResponse<Lesson[]>) => {
    //                 console.log(`get from json`, resp);
    //                 resolve(resp.data);
    //             })
    //             .catch(error => {
    //                 console.error(error);
    //                 reject(error);
    //             });
    //     });
    // }

    getLessonByListId(lessonIds: any): Promise<Lesson[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/Lesson/GetLessonByUserId`, { lessonIds })
                .then((resp: AxiosResponse<Lesson[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
import { Divisions } from './division-endpoint';

export interface DivisonAPI {
    
    // Danh sách phòng 
    getAll(): Promise<Divisions>;
}
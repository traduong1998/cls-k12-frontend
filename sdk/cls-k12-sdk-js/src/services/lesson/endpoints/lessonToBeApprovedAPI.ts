import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { LessonToApprovalRequest } from "../requests/lesson-to-approval-request";
import { LessonToBeApprovedResponses } from "../responses/lessonToBeApprovedResponses";

export interface LessonToBeApprovedAPI {
    getListLessonToBeApproved(request: LessonToApprovalRequest):Promise<PaginatorResponse<LessonToBeApprovedResponses>>;
}
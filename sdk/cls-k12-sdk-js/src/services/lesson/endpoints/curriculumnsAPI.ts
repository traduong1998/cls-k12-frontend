import { CurriculumnResponses } from '../responses/curriculumnsResponses';

export interface CurriculumsAPI {

    getCuriculumByGradeIdAndSubjectId(gradeId?: number, subjectId?: number): Promise<CurriculumnResponses[]>;
}
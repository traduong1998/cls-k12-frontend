import { ReferenceRequest } from '../requests/referenceRequest';
import { ReferenceResponses } from '../responses/referenceResponses';

export interface ReferencesAPI {
    getListReferenceByLessonId(lessonId: number): Promise<ReferenceResponses[]>;
    addReference(reference: ReferenceRequest): Promise<ReferenceResponses>
}
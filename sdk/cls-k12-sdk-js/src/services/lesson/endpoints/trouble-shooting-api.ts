import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { Troubleshooting } from "../models/troubleshooting";
import { TroubleshootingFilterRequest } from "../requests/troubleshooting-filter-request";
import { LessonOption } from "./trouble-shooting-endpoint";


export interface TroubleshootingAPI {
    getListTroubleshootingPaging(request?: TroubleshootingFilterRequest): Promise<PaginatorResponse<Troubleshooting>>;
    getLessons(): Promise<LessonOption[]> ;
}
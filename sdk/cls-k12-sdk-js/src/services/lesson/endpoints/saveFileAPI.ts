import { SaveFileResponses } from "../responses/saveFileResponses";

export interface SaveFileAPI {

    saveFileData(formData: FormData): Promise<SaveFileResponses>;
}

import { SubjectOption } from '../../manage/models/subject';
import { ListSubjectOfGrade } from './subjectsEndpoint';

export interface SubjectAPI {

    getSubjectByGradeId(gradeId?: number): Promise<ListSubjectOfGrade>;
    getSubjectByGradeIdOptions(gradeId?: number): Promise<SubjectOption[]>
}
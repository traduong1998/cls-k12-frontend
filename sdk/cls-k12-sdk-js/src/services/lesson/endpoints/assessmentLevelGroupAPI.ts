import { AssessmentLevelGroupResponses } from '../responses/assessmentLevelGroupResponses';

export interface AssessmentLevelGroupAPI {
    getAll(): Promise<AssessmentLevelGroupResponses[]>;
}
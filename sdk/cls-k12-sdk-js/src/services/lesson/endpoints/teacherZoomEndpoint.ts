import axios, { AxiosError, AxiosResponse } from "axios";
import { Logger, CLS } from "../../..";
import { TeacherZoomRequest } from "../requests/teacherZoomRequest";
import { TeacherZoomResponses } from "../responses/teacherZoomResponses";
import { TeacherZoomAPI } from "./teacherZoomAPI";

/**24/08/2021 LAMNV add check information zoomAPI */
export class TeacherZoomEndpoint implements TeacherZoomAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }

    getInformationZoomAPI(): Promise<TeacherZoomResponses> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/ZoomAccount`)
                .then((resp: AxiosResponse<TeacherZoomResponses>) => {

                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    updateInformationZoomAPI(teacherZoom: TeacherZoomRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios
                .put(`${this.baseUrl}/ZoomAccount`, teacherZoom)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    createInformationZoomAPI(teacherZoom: TeacherZoomRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios
                .post(`${this.baseUrl}/ZoomAccount`, teacherZoom)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

}
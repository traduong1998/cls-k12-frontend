import axios, { AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { Logger } from "../../../Logger";
import { LessonToApprovalRequest } from "../requests/lesson-to-approval-request";
import { LessonToBeApprovedResponses } from "../responses/lessonToBeApprovedResponses";
import { LessonToBeApprovedAPI } from "./lessonToBeApprovedAPI";

export class LessonToBeApprovedEndpoint implements LessonToBeApprovedAPI {
    // TODO : Endpoint :
    endpoint = '/v1/learn';

    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }


    getListLessonToBeApproved(request: LessonToApprovalRequest): Promise<PaginatorResponse<LessonToBeApprovedResponses>> {
        return new Promise((resolve, reject) => {
            
            // TODO : pass URL
            axios.get(`${this.baseUrl}/lesson/getlistlessonpaging`,{params: {
                keyWord: request.keyWord,sortField: request.sortField, sortDirection: request.sortDirection,
                page: request.pageNumber, size: request.sizeNumber, getCount: request.getCount
            }} )
                .then((resp: AxiosResponse<PaginatorResponse<LessonToBeApprovedResponses>>) => {
                    console.log(`get from json`, resp);
                    
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}

import { LessonChaptersResponses } from "../responses/lessonChaptersResponses";

export interface lessonChaptersAPI {

    getLessonChaptersByContentPanelId(contentPanelId: number): Promise<LessonChaptersResponses[]>
}
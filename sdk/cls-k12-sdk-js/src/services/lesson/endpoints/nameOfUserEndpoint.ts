import axios, { AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import { NameOfUserResponses } from "../responses/nameOfUserResponses";
import { NameOfUserAPI } from "./nameOfUserAPI";

export class NameOfUserEndpoint implements NameOfUserAPI {
    // TODO : Endpoint :
    endpoint = '/v1/learn';

    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }


    getListNameOfUser(ids: number[]): Promise<NameOfUserResponses[]> {
        this.logger.warn('usertype get3');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/assets/fakedata/name-of-user.json`, { params: ids })
                .then((resp: AxiosResponse<NameOfUserResponses[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
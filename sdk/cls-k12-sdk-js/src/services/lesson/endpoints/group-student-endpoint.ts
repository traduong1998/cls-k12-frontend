import { CLS } from '../../../CLS';
import { GroupStudentsResponses } from '../responses/groupStudentsResponses';
import { Logger } from '../../../Logger';
import axios, { AxiosResponse } from "axios";
import { GroupStudentAPI } from './groupStudentsAPI';

export class GroupStudentEndpoint implements GroupStudentAPI {

    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = config?.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    getGroupStudentsByGradeIdAndSchoolId(schoolId?: number, gradeId?: number): Promise<GroupStudentsResponses[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/`, { params: { schoolId, gradeId } })
                .then((resp: AxiosResponse<GroupStudentsResponses[]>) => {
                    let a: GroupStudentsResponses[] = [
                        { id: 1, name: "lam", gradeId: 1, schoolId: 1 }
                    ]
                    console.log(`get from json`, resp);
                    //resolve(resp.data);
                    resolve(a);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}
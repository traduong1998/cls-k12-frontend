import { ChaptersResponses } from "../responses/chaptersResponses";


export interface ChaptersAPI {

    getChaptersBycurriculumId(contentPanelId: number): Promise<ChaptersResponses[]>;
}
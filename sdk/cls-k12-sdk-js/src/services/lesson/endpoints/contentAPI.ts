import { Content } from "../models/content";
import { VirtualClassRoom } from "../models/virtualClassRoom";
import { CreateCurriculumRequest } from "../requests/createCurriculumRequest";
import { DetailContentRequests } from "../requests/detailContentRequests";
import { UpdateInfoContentRequest } from "../requests/updateInfoContentRequest";
import { DetailContentResponses } from "../responses/detailContentResponses";

export interface ContentAPI {
    createContent(detailContentRequests: DetailContentRequests): Promise<DetailContentRequests>;
    getContent(idLesson:number, idContent: number): Promise<DetailContentResponses>;
    updateContent(detailContentRequests: DetailContentRequests): Promise<Content>;
    deleteContent(idLesson: number, idContent: number): Promise<Content>
    createCurriculum(idLesson:number,createCurriculumRequest: CreateCurriculumRequest): Promise<Content>;
    updateInfoContent(idLesson:number,updateInfoContentRequest: UpdateInfoContentRequest): Promise<Content>
    getVirtualClassRoom(dLesson: number, idContent: number): Promise<VirtualClassRoom>
    updateVirClassroomUnpublish(value : any): Promise<Content>;
    updateContentBeforPublish(detailContentRequest: DetailContentRequests): Promise<Content>
    updateContentAfterPublish(detailContentRequest: DetailContentRequests): Promise<Content>
}
import { ListStudentForAssignedResponse } from '../responses/listStudentForAssignedResponse';
import { StudentAssignPagingRequest } from '../models/StudentAssignPagingRequest';
import { PaginatorResponse } from '../../../../core/api/responses/PaginatorResponse';
import { StudentRegistrationPagingRequest } from '../models/StudentRegistrationPagingRequest';
import { ListStudentRegisterResponse } from '../responses/listStudentRegisterResponse';
export interface AssignStudentsAPI  {
    getListStudentForAssigned(request: StudentAssignPagingRequest): Promise<PaginatorResponse<ListStudentForAssignedResponse>>;
    getListStudentRegister(request: StudentRegistrationPagingRequest): Promise<PaginatorResponse<ListStudentRegisterResponse>>;
}
import axios, { AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import { OlClassRoom } from "../models/olClassRoom";
import { OlClassRoomRequest } from "../requests/olClassRoomRequest";
import { OlClassRoomAPI } from "./olClassRoomAPI";

export class OlClassRoomEndPoint implements OlClassRoomAPI {
    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
    }
    createOlClassRoom(classRoom: OlClassRoomRequest): Promise<OlClassRoom> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/assets/fakedata/list-reference.json`)
                .then((resp: AxiosResponse<OlClassRoom>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}
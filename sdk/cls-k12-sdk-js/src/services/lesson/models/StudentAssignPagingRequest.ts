import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class StudentAssignPagingRequest implements PaginatorRequest {
  DivisionId? : number;
  SchoolId?: number;
  GradeId?: number;
  GroupStudentId?: number;
  LessonId?: number;
  schoolYear?: number;
  SortField?: string;
  SortDirection?: string;
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  getCount?: boolean;
}

export class StudentAssignPagingMeetingRequest implements PaginatorRequest {
  DivisionId? : number;
  SchoolId?: number;
  GradeId?: number;
  GroupStudentId?: number;
  meetingRoomId?: number;
  schoolYear?: number;
  SortField?: string;
  SortDirection?: string;
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  getCount?: boolean;
}

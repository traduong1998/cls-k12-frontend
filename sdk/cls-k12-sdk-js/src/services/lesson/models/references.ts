
export interface References {
    id?: number;
    name?: string;
    fileName?: string;
    link?: string;
    fileId?: string;
    creationDate?: Date;
    dateOfUpdate?: Date;
    lessonId: number;
}
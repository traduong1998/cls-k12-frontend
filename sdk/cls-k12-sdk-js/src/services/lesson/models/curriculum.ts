export interface Curriculum {
    id?: number;
    typeOfCurriculum?: number;
    fileId?: number;
    link?: string;
    detailsOfContent?: string;
    allowToRewindAndFastForward?: boolean;
    virtualClassroomId?: number;
    description?: string;
}


export interface Troubleshooting {
    id: number;
    fbId?: number;
    name?: string;
    lessonId: number;
    parentId?: number;
    lessonName: string;
    senderId: number;
    senderName: string;
    senderAvatar?: string;
    senderEmail?: string;
    content?: string;
    feedbackId?: string;
    feedbackContent?: string
    typeOfContent?: string;
    senderDate?: Date;
    feedbackDate?: Date;
    status: string
}
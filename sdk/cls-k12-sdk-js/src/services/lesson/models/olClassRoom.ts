
export interface OlClassRoom {
    id: number;
    zoomMeetingId: string
    zoomPassword: string
    userId: number;
    zoomUserId: number;
    startDate: Date
    finishDate: Date;
    isDeleted: boolean
    status: string;

}
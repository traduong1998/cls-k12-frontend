import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class StudentRegistrationPagingRequest implements PaginatorRequest {
  GroupStudentId? : number;
  SchoolId?: number;
  GradeId?: number;
  LessonId?: number;
  Status?: boolean;
  schoolYear?: number;
  SortField?: string;
  SortDirection?: string;
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  getCount?: boolean;
}

export interface Lesson {
    id?: number;
    name?: string;
    gradeId?: number;
    subjectId?: number;
    subjectName?: string;
    panelId?: number;
    chapterId?: number;
    accessRange?: number;
    unitId?: number;
    step?: number;
    publicationDate?: Date;
    createdDate?: string;
    userId?: number;
    userName?: string;
    userAvatar?: string;
    status?: string;
    avatar?: string;
    fileAvatarName?: string
    videoURL?: string;
    videoURLId?: string;
    fileVideoName?: string;
    introduction?: string;
    portalId?: number;
    divsionId?: number;
    schoolId?: number;
    contentPanelId?: number;
    shareSetting?: string;
    learnTime?: string;
    interval?: number;
}


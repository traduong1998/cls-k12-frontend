
export interface VirtualClassRoom {
    lessonId: number
    contentId?: number;
    contentName?: string
    lessonName: string
    meetingId: number
    zoomMeetingId: number
    zoomPassword: string
    zoomUserId: number
    startDate: Date
    finishDate: Date
    userId: number
    joinURL: string
    startURL: string
    statusOnline: string
    isOwnerRoom: boolean
    isNeedUpdatekey?: boolean
}
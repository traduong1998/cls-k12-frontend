export enum TestObjectUpdate {
    Information = "INF",
    Config = "CON",
    Questions = "QUE"
}
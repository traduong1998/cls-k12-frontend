export interface TeacherReport{
    teacherId: number,
    username?: string,
    teacherName:string,
    schoolName:string,
    totalLessonPedding: number,
    totalLessonPublic: number
}
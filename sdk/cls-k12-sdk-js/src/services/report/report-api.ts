import { TeacherReport } from "./models/teacher-report";

export interface TeacherReportApi {
    getTeacherReport(divisonId:number, schoolId:number, teacherName:string, page:number, pageSize:number): Promise<TeacherReport[]>
    exportTeacherReport(divisonId:number, schoolId:number, teacherName:string):Promise<string>
}

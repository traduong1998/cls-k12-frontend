import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS } from "../../CLS";
import { Logger } from "../../Logger";
import { TeacherReport } from "./models/teacher-report";
import { TeacherReportApi } from "./report-api";

export class TeacherReportEndpoint implements TeacherReportApi {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        //this.baseUrl = config.baseUrl;
    }
    getTeacherReport(divisonId: number, schoolId: number, teacherName: string, page: number, pageSize: number): Promise<TeacherReport[]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.baseUrl}/api/ReportTeacher/reportTeacher`, { params: { divisionId: divisonId, schoolId: schoolId, teacherName: teacherName, page: page, pageSize: pageSize } })
                .then((resp: AxiosResponse<TeacherReport[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    exportTeacherReport(divisonId: number, schoolId: number, teacherName: string): Promise<string> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL\
            axios
                .get(`${this.baseUrl}/api/ReportTeacher/exportExcelReportTeacher`,
                    { params: { divisionId: divisonId, schoolId: schoolId, teacherName: teacherName }, responseType: 'blob' }
                )
                .then((resp) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
}
export interface StudentCount{
    
    /**Meeting room number */
    id:number;

    /**User count number */
    count:number
}
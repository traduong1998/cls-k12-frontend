import { PaginatorResponse } from "../../../../../core/api/responses/PaginatorResponse";
import { MeetingRoom } from "../models/meeting-room";
import { StudentCount } from "./student-count-response";

export interface GetMeetingRoomsResponse{
    meetingRooms : PaginatorResponse<MeetingRoom>;
    totalStudentsRegisMeeting: StudentCount[];
    studentsJoined:StudentCount[];
}
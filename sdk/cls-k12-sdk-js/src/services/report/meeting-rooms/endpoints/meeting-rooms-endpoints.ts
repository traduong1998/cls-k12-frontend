import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS, Logger } from "../../../..";
import { GetStudentLogsPagingResponse } from "../../../meeting-room/models/meeting-room-student-logs-response";
import { MeetingRoomTeacherLogsModel } from "../../../meeting-room/models/meeting-room-teacher-logs-model";
import { ExcelMeetingRoomStudentLogsRequest } from "../requests/excel-meeting-room-student-logs-request";
import { GetMeetingRoomsRequest } from "../requests/get-meeting-rooms-request";
import { GetStudentLogsPagingRequest } from "../requests/get-student-logs-paging-request";
import { GetMeetingRoomsResponse } from "../responses/get-meeting-rooms-response";

export class ReportMeetingRoomsEndpoint {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl += "/api/reports/ReportMeetingRooms"
        // this.baseUrl = "http://localhost:58915/api/v1/reports/ReportMeetingRooms"
    }

    getAll(request: GetMeetingRoomsRequest): Promise<GetMeetingRoomsResponse> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}`, { params: request })
                .then((resp: AxiosResponse<GetMeetingRoomsResponse>) => {

                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    exportMeetingRoomExcel(request: GetMeetingRoomsRequest): Promise<string> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/actions/export`,
                { params: request, responseType: 'blob' })
                .then((resp: AxiosResponse<string>) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getByMeetingRoomIdAndUserId(meetingRoomId: number): Promise<MeetingRoomTeacherLogsModel> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/${meetingRoomId}/teacher/logs`)
                .then((resp: AxiosResponse<MeetingRoomTeacherLogsModel>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    GetStudentLogs(request: GetStudentLogsPagingRequest): Promise<GetStudentLogsPagingResponse> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/${request.meetingRoomId}/student/logs`, { params: request })
                .then((resp: AxiosResponse<GetStudentLogsPagingResponse>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    exportStudentLogExcel(meetingRoomId: number, request: ExcelMeetingRoomStudentLogsRequest): Promise<string> {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.baseUrl}/${meetingRoomId}/student/logs/actions/export`,
                    { params: request, responseType: 'blob' })
                .then((resp: AxiosResponse<string>) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error: AxiosError) => {
                    console.error(error);
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
}
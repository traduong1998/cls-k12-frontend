export class MeetingRoom {
    id?: number
    name?: string
    meetingId?: number
    passWord?: string
    meetingUserId?: number
    startDate?: Date
    meetingRoomTeacherStartDate?: Date;
    meetingRoomTeacherEndDate?: Date;
    finishDate?: Date
    joinURL?: string
    startURL?: string
    meetingType?: string
    statusOnline?: string
    description?: string
    portalId?: number;
    portalName?: string;
    divisionId?: number;
    divisionName?: string;
    schoolId?: number;
    schoolName?: string;
    totalStudent?: number;
    joinedStudent?: number;

    statusLearning?: string
    unitManage?: string;
}
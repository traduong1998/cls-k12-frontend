export interface GetMeetingRoomsRequest {
    divisionId?: number;
    schoolId?: number;
    meetingType?: number;
    status?: number;
    fromDate?: number;
    toDate?: number;
    page?: number;
    size?: number;
    keyword?: number;
    sortField?: string;
    sortDirection?: string;
    getCount?: boolean
}
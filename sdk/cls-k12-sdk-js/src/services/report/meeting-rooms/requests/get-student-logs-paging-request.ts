export interface GetStudentLogsPagingRequest {
    meetingRoomId?: number
    divisionId?: number
    schoolId?: number
    gradeId?: number
    groupStudentId?: number
    keyword?: string
    getCount?: boolean
    page: number
    size: number
    sortField: string,
    sortDirection: string
}
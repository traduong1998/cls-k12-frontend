
import { ReportMeetingRoomsEndpoint } from './endpoints/meeting-rooms-endpoints'
import { GetMeetingRoomsRequest } from './requests/get-meeting-rooms-request'

export {
    ReportMeetingRoomsEndpoint,
    GetMeetingRoomsRequest
}
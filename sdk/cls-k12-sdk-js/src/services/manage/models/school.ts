export interface School {
    id?: number;
    name?: string;
    code?: string;
    divisionId?: number;
}

export interface SchoolOption {
    id: number;
    name: string;
}



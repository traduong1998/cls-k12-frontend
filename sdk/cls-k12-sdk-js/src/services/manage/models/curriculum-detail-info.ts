export class CurriculumDetailInfo implements CurriculumDetail {
    id: number | undefined;
    curriculumName: string | undefined;
    gradeName: string | undefined;
    subjectName: string | undefined;
    gradeId: number | undefined;
    subjectId: number | undefined;
    parentId: number | undefined;
    leftPointer: number | undefined;
    rightPointer: number | undefined;
    isActived: boolean | undefined;
    getActiveStatus: string | undefined;

}

export interface CurriculumDetail {
    id?: number | undefined;
    curriculumName: string | undefined;
    gradeName?: string | undefined;
    subjectName?: string | undefined;
    gradeId: number | undefined;
    subjectId: number | undefined;
    parentId?: number | undefined;
    leftPointer?: number | undefined;
    rightPointer?: number | undefined;
    isActived?: boolean | undefined;
    getActiveStatus?: string | undefined;

}
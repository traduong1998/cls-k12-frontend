export interface GlobalUnitInfo {
    id: number;
    portalId: number;
    divisionId?: number;
    schoolId?: number;
    unitLevel: string;
    name: string;
    currentSchoolYear: number;
}
import { SchoolOptionModel } from "../../report-option/models/school-option-model";
import { Currculum } from "../models/curriculum";
import { CurriculumDetailInfo } from "../models/curriculum-detail-info";
import { GlobalUnitInfo } from "../models/global-unit-info";
import { GradeOption } from "../models/grade";
import { SchoolOption } from "../models/school";
import { SubjectOption } from "../models/subject";
import { TeacherOption } from "../models/teacher";

export interface GlobalAPI {
    getUnitInfo(request: { domain: string }): Promise<GlobalUnitInfo>;

    GetListUserByIds(userIdArr: number[], portalId: number): Promise<TeacherOption[]>

    getSubjectOptionsByIds(ids: number[], portalId: number): Promise<SubjectOption[]>

    getSubjectOptionsByGrade(portalId: number, gradeId?: number): Promise<SubjectOption[]>

    getCurriculum(domain: string,  divisionId: number, schoolId: number,  gradeId: number, subjectId: number): Promise<Currculum[]>
    getCurriculumDetailInfoByPortalId(portalId: number, curriculumId: number): Promise<CurriculumDetailInfo>
    getCurriculumChildrentNodesByPortalId(portalId: number, curriculumId: number): Promise<CurriculumDetailInfo[]>

    getGradeOptionsByPortalId(portaId: number, schoolId?: number): Promise<GradeOption[]>
    
    getGradeOptionsByDomain(domain:string): Promise<GradeOption[]>

    getSchoolOptionsByPortalId(portaId: number, divisionId?: number): Promise<SchoolOption[]>
    getSchoolOptionsByGradeId(portalId:number,divisionId:number, gradeId:number):Promise<SchoolOptionModel[]>
    getTeacherOptionBySchoolId(shoolId:number):Promise<TeacherOption[]>
}
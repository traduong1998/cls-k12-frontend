import axios, { AxiosError, AxiosResponse } from "axios";
var qs = require('qs');
import { CLS } from "../../..";
import { SchoolOptionModel } from "../../report-option/models/school-option-model";
import { Currculum } from "../models/curriculum";
import { CurriculumDetailInfo } from "../models/curriculum-detail-info";
import { GlobalUnitInfo } from "../models/global-unit-info";
import { GradeOption } from "../models/grade";
import { SchoolOption } from "../models/school";
import { SubjectOption } from "../models/subject";
import { TeacherOption } from "../models/teacher";
import { GlobalAPI } from "./global-api";

export class GlobalEndpoint implements GlobalAPI {
    private baseUrl: string;

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api/system/global';
    }
    getGradeOptionsByDomain(domain: string): Promise<GradeOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/getlistgradebydomain`, { params: { domain: domain } })
                .then((resp: AxiosResponse<GradeOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getTeacherOptionBySchoolId(shoolId: number): Promise<TeacherOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/list-users/${shoolId}`)
                .then((resp: AxiosResponse<TeacherOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getSchoolOptionsByPortalId(portaId: number, divisionId?: number): Promise<SchoolOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/${portaId}/schools`, { params: { divisionId: divisionId } })
                .then((resp: AxiosResponse<SchoolOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getUnitInfo(request: { domain: string }): Promise<GlobalUnitInfo> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/units/${request.domain}`)
                .then((resp: AxiosResponse<GlobalUnitInfo>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    console.error(error);
                    reject(error.response?.data);
                });
        });
    }

    //user
    GetListUserByIds(userIdArr: number[], portalId: number): Promise<TeacherOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/users`, {
                    params: { portalId: portalId, ids: userIdArr }
                    , paramsSerializer: params => {
                        return qs.stringify(params);
                    }
                })
                .then((resp: AxiosResponse<TeacherOption[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }


    //subject 
    getSubjectOptionsByIds(ids: number[], portalId: number): Promise<SubjectOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/${portalId}/subjects/ids`, {
                params: { portalId: portalId, ids: ids }
                , paramsSerializer: params => {
                    return qs.stringify(params);
                }
            })
                .then((resp: AxiosResponse<SubjectOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getSubjectOptionsByGrade(portalId: number, gradeId?: number): Promise<SubjectOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/${portalId}/subjects`, { params: { gradeId: gradeId } })
                .then((resp: AxiosResponse<SubjectOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getSchoolOptionsByGradeId(portalId: number, divisionId: number, gradeId: number): Promise<SchoolOptionModel[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/${portalId}/${gradeId}/schools`, { params: { divisionId: divisionId } })
                .then((resp: AxiosResponse<SchoolOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    //currilumn
    getCurriculum(domain: string, divisionId: number, schoolId: number, gradeId: number, subjectId: number): Promise<Currculum[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/GetCurriculumsInHomePage`, { params: {domain:domain, divisionId: divisionId, schoolId: schoolId, gradeId: gradeId, subjectId: subjectId } })
                .then((resp: AxiosResponse<Currculum[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    getCurriculumDetailInfoByPortalId(portalId: number, curriculumId: number): Promise<CurriculumDetailInfo> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/${portalId}/curriculums/${curriculumId}`, { params: { curriculumId: curriculumId } })
                .then((resp: AxiosResponse<CurriculumDetailInfo>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    getCurriculumChildrentNodesByPortalId(portalId: number, curriculumId: number): Promise<CurriculumDetailInfo[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/${portalId}/curriculums/${curriculumId}/children`, { params: { curriculumId: curriculumId } })
                .then((resp: AxiosResponse<CurriculumDetailInfo[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    //grande
    getGradeOptionsByPortalId(portaId: number, schoolId?: number): Promise<GradeOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/${portaId}/grades`, { params: { schoolId: schoolId } })
                .then((resp: AxiosResponse<GradeOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }



}
import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class TestkitsPagingRequest {
    page: number | undefined;
    size: number | undefined;
    getCount?: boolean;
    gradeId?: number;
    subjectId?: number;
    testKitSelectedId?: number;
}

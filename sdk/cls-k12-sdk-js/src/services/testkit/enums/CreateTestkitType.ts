export enum CreateTestkitType {
    FromQuestionBank = 'QTB',
    FromFile = 'FIL',
    FromMatrix = 'MAT',
}
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { TestKit } from "../models/testkit";
import { TestkitsPagingRequest } from "../requests/testkits-paging-request";
import { UpdateTestKit } from "../requests/update-testkit";

export interface TestKitApi {
    getTestKitPaging(request: TestkitsPagingRequest): Promise<PaginatorResponse<TestKit>>;
    createTestkit(testkit: TestKit): Promise<TestKit>;
    deleteTestKit(testKitId: number): Promise<boolean>;
    getTestKit(id: number): Promise<TestKit>;
    updateTestkit(testkit: UpdateTestKit): Promise<boolean>;
}
import axios, { AxiosError, AxiosResponse } from "axios";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { CLS } from "../../../CLS";
import { ErrorResponse } from "../../../core/api/responses/ErrorResponse ";
import { TestKit } from "../models/testkit";
import { TestkitsPagingRequest } from "../requests/testkits-paging-request";
import { UpdateTestKit } from "../requests/update-testkit";
import { TestKitApi } from "./testkit-api";


export class TestKitEnpoint implements TestKitApi {

    baseUrl: string;
    baseUrlConvertFile?: string;

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
        // this.baseUrl = "http://localhost:58910";
        this.baseUrl = this.baseUrl + '/api';
        this.baseUrlConvertFile = CLS.getConfig().apiBaseUrls.questionConverter;
        console.log(`endpoi`, this.baseUrl);

    }
    deleteTestKit(testKitId: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.delete(`${this.baseUrl}/testkits/` + testKitId).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }
    createTestkit(testkit: TestKit): Promise<TestKit> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/testkits`, testkit).then((res: AxiosResponse<TestKit>) => {
                resolve(res.data);
            })
            .catch((error: AxiosError<ErrorResponse>) => {
                reject(error.response != null ? error.response.data: null);
            });
        })
    }
    getTestKitPaging(request: TestkitsPagingRequest): Promise<PaginatorResponse<TestKit>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/testkits`,
                {
                    params: {
                        gradeId: request.gradeId, subjectId: request.subjectId, testKitSelectedId: request.testKitSelectedId,
                        page: request.page, size: request.size, getCount: request.getCount
                    }
                }).then((res: AxiosResponse<PaginatorResponse<TestKit>>) => {
                    resolve(res.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response != null ? error.response.data: null);
                });
        })
    }
    getTestKit(id: number): Promise<TestKit> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/testkits/` + id)
                .then((res: AxiosResponse<TestKit>) => {
                    resolve(res.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response != null ? error.response.data: null);
                });
        })
    }
    updateTestkit(testkit: UpdateTestKit): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.put(`${this.baseUrl}/testkits`, testkit).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            })
            .catch((error: AxiosError<ErrorResponse>) => {
                reject(error.response != null ? error.response.data: null);
            });
        })
    }
}
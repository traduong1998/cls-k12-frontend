export interface TestKit {
    id:number
    name: string,
    createType: string,
    numberTest: number,
    gradeId: number,
    subjectId: number,
    matrixId?: number,
    used?: number
}

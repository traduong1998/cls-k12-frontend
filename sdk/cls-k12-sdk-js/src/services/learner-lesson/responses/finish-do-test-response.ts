export interface FinishDoTestResponse {
    numberOfQuestion : number; 
    correctNum : number; 
    totalScore : number; 
    totalQuizScore : number; 
    quizScore : number; 
    numberOfRemainingAttempts : number; 
    statusOver : string; 
    isFinishedLesson : boolean;
    endDate: Date;
}
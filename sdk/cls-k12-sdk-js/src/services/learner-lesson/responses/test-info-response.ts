export interface TestInfoResponse {
    id: number;
    name: string;
    testId: number;
    typeOfContent: string;
    scoreAfterFinish: number;
    testTime: number;
    numberOfQuestion: number;
    allowToRedo: boolean;
    numberOfRemainingAttempts?: number;
    isFinished: boolean;
    isCompleted: boolean;
    statusOver: string;
    startDate: Date;
    theMaximumNumberToRedo?: number
    userContentTestId: number;
    correctNum: number;
    totalScore: number;
    quizScore: number;
    realScore: number;
    totalQuizScore: number;
    endDate: Date;
}
export interface ContentGroupsResponse {
    lessonId: number;
    teacherId?: number;
    lessonName: string;
    formOfLearn: string;
    avatar?:string;
    percentageOfCompletion?: number;
    listContentGroupsAndContent: ContentGroupAndContent[];
}
export interface ContentGroupAndContent {
    contentId?: number;
    contentGroupId?: number;
    name: string;
    order: number;
    typesOfContent: string;
    conditionsForCompletion: string;
    timeToComplete?: number;
    isComplete: boolean;
    percentCompleted?: number;
    isActive?: boolean;
    totalContentFinished: number;
    isLearned: boolean;
    isExpanded: boolean;
    listContents: ContentGroupAndContent[];
    percentVideo?:number;
}

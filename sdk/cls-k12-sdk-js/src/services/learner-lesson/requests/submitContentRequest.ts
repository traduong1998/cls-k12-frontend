export interface SubmitContentRequest {
    lessonId: number;
    contentId: number;
    curriculumId: number;
}
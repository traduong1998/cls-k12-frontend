export interface UserLesson{
    id: number,
    userId: number,
    lessonId: number,
    assessmentLevelName?: string,
    result: number,
    percentageOfCompletion: number,
    registrationDate: Date,
    completionDate: Date,
    startDate: Date,
}
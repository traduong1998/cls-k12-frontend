

export interface Reference {
    name: string
    link: string
    creationDate: Date
    dateOfUpdate: Date
    lessonId: number
    id: number
    domainEvents: any
}
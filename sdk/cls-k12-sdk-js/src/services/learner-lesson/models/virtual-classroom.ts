
export interface VirtualClassroom {
    lessonId: number;
    lessonName: string;
    meetingId: number;
    zoomMeetingId: string;
    zoomPassword: string;
    userId: number;
    zoomUserId: string;
    startDate: Date;
    finishDate: Date;
    status: string;
}

export interface ZoomMeeting {
    agenda: string;
    created_at: Date;
    duration: number;
    host_id: string;
    id: number;
    password: string;
    joinUrl: string;
    join_url: string;
    settings: Settings;
    start_time: Date;
    // startUrl: string;
    start_url: string
    status: string;
    timezone: string;
    topic: string;
    type: number;
    uuid: string;
    apiKey: string;
}

export interface GlobalDialInNumber {
    city: string;
    country: string;
    country_name: string;
    number: string;
    type: string;
}

export interface Settings {
    alternative_hosts: string;
    approval_type: number;
    audio: string;
    auto_recording: string;
    close_registration: boolean;
    cn_meeting: boolean;
    enforce_login: boolean;
    enforce_login_domains: string;
    global_dial_in_countries: string[];
    global_dial_in_numbers: GlobalDialInNumber[];
    host_video: boolean;
    in_meeting: boolean;
    join_before_host: boolean;
    mute_upon_entry: boolean;
    participant_video: boolean;
    registrants_confirmation_email: boolean;
    use_pmi: boolean;
    waiting_room: boolean;
    watermark: boolean;
    registrants_email_notification: boolean;
}

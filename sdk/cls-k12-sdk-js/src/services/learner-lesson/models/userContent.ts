export interface UserContent {
    id: number;
    userId: number;
    contentId: number;
    lessonId: number;
    startDate: Date;
}
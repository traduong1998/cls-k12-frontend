
export interface Content {
    id?: number;
    name?: string;
    typesOfContent?: string;
    conditionsForCompletion?: string;
    timeToComplete?: number;
    createDate?: string;
    lessonId?: number;
    contentGroupId?: number;
    description?: string;
    order?: number;
    status?: string;
    curriculumId?: number;
    scoreAfterFinish?: number;
    testId?: number;
    isDelete?: boolean;
    detailsOfContent?: string;
    link?: string;
    fileId?: string;
}
import { VirtualClassRoom } from "../../lesson/models/virtualClassRoom";
import { ZoomMeeting } from "./zoom-meeting";


export interface MeetingDetailandSignature {
    meeting: ZoomMeeting;
    signature: string;
    email: string
}
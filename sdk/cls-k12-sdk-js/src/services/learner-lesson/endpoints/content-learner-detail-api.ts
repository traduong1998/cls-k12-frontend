import { LessonOption } from "../../lesson/endpoints/trouble-shooting-endpoint";
import { MeetingDetailandSignature } from "../models/meeting-detail-and-signature";
import { VirtualClassroom } from "../models/virtual-classroom";
import { TestInfoResponse } from "../responses/test-info-response";
import { LessonSelect } from "./content-learner-detail-endpoint";


export interface ContentLearnerDetailAPI {
    getVirtualClassroomContent(lessonId: number, contentId: number): Promise<VirtualClassroom>;
    getZoomMeetingDetail(lessonId: number, contentId: number, meetingId: string): Promise<MeetingDetailandSignature>;
    getTestInfoForLearner(lessonId: number, contentId: number): Promise<TestInfoResponse>;
    getLessons(lessonIds: number[]): Promise<LessonSelect[]>
}
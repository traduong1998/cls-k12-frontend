import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import { LessonOption } from "../../lesson/endpoints/trouble-shooting-endpoint";
import { MeetingDetailandSignature } from "../models/meeting-detail-and-signature";
import { VirtualClassroom } from "../models/virtual-classroom";
import { TestInfoResponse } from "../responses/test-info-response";
import { ContentLearnerDetailAPI } from "./content-learner-detail-api";

export interface LessonSelect {
    id: number;
    name: string;
}

export class ContentLearnerDetailEndpoint implements ContentLearnerDetailAPI {
    baseUrl: string;
    logger: Logger = Logger.getInstance();
    constructor(config?: { baseUrl: string }) {
        console.log(`ssss:`, CLS.getConfig().apiBaseUrl);
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl += '/api';
    }
    getTestInfoForLearner(contentId: number): Promise<TestInfoResponse> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/dotests/contentinfo?contentId=${contentId}`)
                .then((resp: AxiosResponse<TestInfoResponse>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getZoomMeetingDetail(lessonId: number, contentId: number, meetingId: string): Promise<MeetingDetailandSignature> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/lessons/${lessonId}/contents/${contentId}/zoomMeetings/${meetingId}`)
                .then((resp: AxiosResponse<MeetingDetailandSignature>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getZoomMeetingDetailLearner(lessonId: number, contentId: number, meetingId: string): Promise<MeetingDetailandSignature> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/learners/${lessonId}/contents/${contentId}/zoomMeetings/${meetingId}`)
                .then((resp: AxiosResponse<MeetingDetailandSignature>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    getVirtualClassroomContent(lessonId: number, contentId: number): Promise<VirtualClassroom> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/learners/virtualclassroom/${contentId}`)
                .then((resp: AxiosResponse<VirtualClassroom>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getLessons(lessonIds: number[]): Promise<LessonSelect[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL ${this.baseUrl}/VirtualClassrooms/GetLessonsClassroom 
            axios.post(`${this.baseUrl}/VirtualClassrooms/GetLessonsClassroom`, { lessonIds })
                .then((resp: AxiosResponse<LessonSelect[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
import { ContentGroupsResponse } from '../responses/listContentGroupsResponse';
export interface ContentGroupLearnerAPI {
    getListContentGroupsForLearner(lessonId: number): Promise<ContentGroupsResponse>;
}
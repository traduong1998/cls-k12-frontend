import axios, { AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import { Content } from "../models/content";
import { UserLesson } from "../models/user-lesson";
import { UserContent } from "../models/userContent";
import { SubmitContentRequest } from "../requests/submitContentRequest";
import { ContentLearnerAPI } from "./contentLearnerAPI";

type NewType = Logger;

export class ContentLearnerEndpoint implements ContentLearnerAPI {

    // TODO : Endpoint :
    endpoint = '/v1/lessons';

    private baseUrl: string;
    logger: NewType = Logger.getInstance();

    constructor(config?: { baseUrl: string }) {
        // console.log(`ssss:`, CLS.getConfig().apiBaseUrl);
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    submitContent(request: SubmitContentRequest): Promise<UserLesson> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/learners/compelelesson`, request)
                .then((resp: AxiosResponse<UserLesson>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    addUserContent(contentId: number, lessonId: number): Promise<UserContent> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/learners/assign-user-content`, { contentId: contentId, lessonId: lessonId })
                .then((resp: AxiosResponse<UserContent>) => {
                    // console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getContent(idLesson: number, idContent: number): Promise<Content> {
        // console.log("idContentidContent", idContent);
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/learners/getdetailcontentforcurriculum?LessonId=${idLesson}&ContentId=${idContent}`)
                .then((resp: AxiosResponse<Content>) => {
                    // console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
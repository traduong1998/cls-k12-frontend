import axios, { AxiosResponse } from 'axios';
import { ContentGroupsResponse } from '../responses/listContentGroupsResponse';
import { CLS } from '../../../CLS';
import { Logger } from '../../../Logger';
import { ContentGroupLearnerAPI } from "./contentGroupsLearnerAPI";
import { UserLesson } from '../models/user-lesson';

export class ContentGroupLearnerEndpoint implements ContentGroupLearnerAPI {
    // TODO : Endpoint :
    endpoint = '/v1/lessons';
    private baseUrl: string;
    logger: Logger = Logger.getInstance();
    constructor(config?: { baseUrl: string }) {
        //  console.log(`ssss:`, CLS.getConfig().apiBaseUrl);
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    getListContentGroupsForLearner(lessonId: number): Promise<ContentGroupsResponse> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/learners/getlistcontentoflesson?LessonId=${lessonId}`)
                .then((resp: AxiosResponse<ContentGroupsResponse>) => {
                    // console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    updatePercentCompletion(lessonId: number): Promise<UserLesson> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/learners/UpdatePercentCompletion`, {lessonId: lessonId})
                .then((resp: AxiosResponse<UserLesson>) => {
                    // console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }


}
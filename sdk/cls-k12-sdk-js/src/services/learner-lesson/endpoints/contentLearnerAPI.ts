import { Content } from "../models/content";
import { UserContent } from '../models/userContent';
import { SubmitContentRequest } from "../requests/submitContentRequest";

export interface ContentLearnerAPI {
    getContent(idLesson: number, idContent: number): Promise<Content>;
    addUserContent( contentId: number, lessonId: number): Promise<UserContent>;
    submitContent(request: SubmitContentRequest): Promise<any>;

}
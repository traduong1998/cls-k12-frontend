export interface TeacherMarkingEssay {
    id: number;
    examId:number;
    subjectId:number;
    gradeId:number;
    gradeName:string;
    subjectName:string;
    examName:string;
    status:string;
}

export interface TeacherExam {
    id: number;
    fullName: string;
    email:string;
    workUnit:string;
    dayOfBirth:Date;
    gender:boolean;
    isChoose:boolean;
    action: string;
}
import { ExamScope } from "../enums/exam-scope";
import { ExamType } from "../enums/exam-type";

export interface ExamInfo {
    id: number,
    name: string,
    gradeId: number,
    creatorId: number,
    status: string,
    getStatus: string,
    scope: ExamScope,
    type: ExamType,
    startDate: Date,
    endDate: Date,
    step: number,
    gradeName?: string,
    creatorName?: string,
    checked?: boolean,
    donviName?: string,
    createLevel:string,
    donviId:number
}
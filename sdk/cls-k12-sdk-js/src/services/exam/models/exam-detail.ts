import { ExamScope } from "../enums/exam-scope";
import { ExamType } from "../enums/exam-type";

/**
 * Chi tiết kỳ thi
 */
export interface ExamDetail {
    /**
     mã khối
     */
    gradeId: number

    /*mã kỳ thi*/
    id: number
    /*
    tên kì thi
    */
    name: string
    /*
       thang đánh giá
    */
    assessmentLevelGroupId: number
    /*
    thời gian bắt đầu
    */
    startDate: Date,
    /*
    thời gian kết thúc
    */
    endDate: Date,
    /*
    phạm v truy cập
    */
    scope: ExamScope,
    /*
        Hình thúc thi
    */
    type: ExamType,
    /*
    danh sách môn thi
    */
    examSubjects: ExamSubjectInfo[],

    /*
    Trạng thái kỳ thi
    */
    status: string,
    isPreservingTime: boolean,
    isPublicScore: boolean,
    isPublicAnswer: boolean,
    logTime: number,
}

export interface ExamSubjectInfo {
    /// <summary>
    /// mã kì thi môn thi
    /// </summary>
    subjectId: number,

    testKitName: string,

    testKitId?: number,

    subjectStartDate: Date,

    subjectEndDate: Date,
}

// export interface ExamSubjectInfo {
//     /// <summary>
//     /// mã kì thi môn thi
//     /// </summary>
//     examSubjectId: number,

//     subjectId?: number
//     /// <summary>
//     /// tên môn thi
//     /// </summary>
//     subjectName: string,

//     testkitId?: number,
//     /// <summary>
//     /// Tên bộ đề
//     /// </summary>
//     testKitname: string,
//     /// <summary>
//     /// Ngày bắt đầu
//     /// </summary>
//     startDate: Date,
//     /// <summary>
//     /// Ngày kết thúc
//     /// </summary>
//     endDate: Date
// }
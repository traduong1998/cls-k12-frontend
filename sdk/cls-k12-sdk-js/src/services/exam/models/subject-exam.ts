export interface SubjectExam {
	subjectId: number
	testKitName: string;
	startDate: Date
	endDate: Date
}
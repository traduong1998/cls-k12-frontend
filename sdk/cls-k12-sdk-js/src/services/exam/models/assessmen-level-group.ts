export interface AssessmentLevelGroupOption{
    id?: number;
    name: string;
}
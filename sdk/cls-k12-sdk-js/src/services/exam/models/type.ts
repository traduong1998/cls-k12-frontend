import {ExamType} from '../enums/exam-type'
export interface TypeExam{
    id:ExamType,
    value:string
}
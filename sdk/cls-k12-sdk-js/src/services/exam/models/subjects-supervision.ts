
/**
 * Subjects supervision interface
 */
export class SubjectsSupervision {
    id?: number
    examName?: string
    subjectId?: number;
    organizeExamId?: number;
    subjectName?: string
    status?: string
    startDate?: Date
    endDate?: Date
    examRoom?: string
    shiftId?: number
    examShift?: string
}


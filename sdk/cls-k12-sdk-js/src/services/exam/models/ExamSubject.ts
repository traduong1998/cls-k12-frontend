export interface ExamSubject {
    id: number;
    subjectName: string;
    startDate:Date;
    endDate:Date;
    testTime:number;
    status:string;
    type:string;
}
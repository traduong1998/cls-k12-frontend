/**
 * Môn học của exam
 */
export interface SubjectOfExam {
    examSubjectId: number,
    subjectId: number,
    testkitId: string,
    testKitname: string,
    startDate: Date,
    endDate: Date,
    gradeId: number,
    subjectName?: string
}
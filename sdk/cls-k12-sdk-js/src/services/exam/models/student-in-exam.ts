

export interface StudentInExam {
    id?: number;
    studentName?: string;
    avatar?: string;
}

// model A Đagnw trả về nè a
export interface ExamSupervisionStudent {
    userId?: number;
    fullName?: string;
    avatar?: string;
    groupName?: string;
    examId?: number;
    subjectId?: number;
    shiftId?: number;
    status?: number;
    examSubjectStudentId: number;
    isCollectedTest: boolean;
}
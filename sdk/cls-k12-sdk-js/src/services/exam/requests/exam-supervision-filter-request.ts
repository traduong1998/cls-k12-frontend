

export interface ExamSupervisionFilterReqest {
    shiftId: number;
    keyword: string;
    listUserIds?: number[];
    notListUserIds?: number[];
    page: number;
    size: number
    getCount?: boolean;
}
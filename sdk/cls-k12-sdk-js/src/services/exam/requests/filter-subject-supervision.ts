import { PaginatorRequest } from "../../../core/api/requests/PaginatorRequest";


export class FilterSubjectSupervisionRequests implements PaginatorRequest {
    public startDate?: Date
    public endDate?: Date
    public listExamId?: number[];
    public pageSize?: number;
    public pageNumber?: number;

    public getCount: boolean = true;
}

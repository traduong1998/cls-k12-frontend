export interface SubjectTestkitRequest {
    id: number,
    testKitId: number
}
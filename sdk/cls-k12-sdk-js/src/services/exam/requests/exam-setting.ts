export interface ExamSetting {
    id: number
    isPreservingTime: boolean
    isPublicScore: boolean,
    isPublicAnswer: boolean,
    logTime: number
}
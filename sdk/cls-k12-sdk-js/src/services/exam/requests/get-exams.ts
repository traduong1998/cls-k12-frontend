import { PaginatorRequest } from "../../../core/api/requests/PaginatorRequest";

export class GetExamFilterRequests implements PaginatorRequest {
    pageNumber: number | undefined;
    pageSize: number | undefined;
    filter: any;
    getCount?: boolean;

    divisionId?: number;
    schoolId?: number;
    gradeId?: string;
    subjectId?: string;

    scope?: number;
    type?: number;
    statusEdit?: string;
    statusExam?: string;
    fromDate?: Date;
    toDate?: Date;
    constructor() {
        this.filter = {
            sortField: "CRE",
            sortDirection: "DESC"
        }
    }
}

import { ExamScope } from "../enums/exam-scope";
import { ExamType } from "../enums/exam-type";

export interface CreateExamRequest {
    gradeId: number,
    /*
    tên kì thi
    */
    name: string,
    /*
       thang đánh giá
    */
    assessmentLevelGroupId: number,

    /**phạm vi truy cập */
    scope: ExamScope,
    /*
        Hình thúc thi
    */
    type: ExamType,

    /*
    thời gian bắt đầu
    */
    startDate: Date,
    /*
    thời gian kết thúc
    */
    endDate: Date,
    examSubjects: SubjectCreateRequest[]
}

export interface SubjectCreateRequest {
    subjectId: number,
    startDate: Date,
    endDate: Date
}
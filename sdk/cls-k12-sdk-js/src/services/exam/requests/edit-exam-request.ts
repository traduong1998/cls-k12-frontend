import { ExamScope } from "../enums/exam-scope";
import { ExamType } from "../enums/exam-type";

export interface EditExamRequest {
    id: number,
    /*
    tên kì thi
    */
    name: string,
    /*
       thang đánh giá
    */
    assessmentLevelGroupId: number,

    scope: ExamScope,
    /*
        Hình thúc thi
    */
    type: ExamType,
    /*
    thời gian bắt đầu
    */
    startDate: Date,
    /*
    thời gian kết thúc
    */
    endDate: Date,
    /*
    phạm v truy cập
    */


    examSubjects: SubjectEditRequest[]
}

export interface SubjectEditRequest {
    subjectId: number,
    startDate: Date,
    endDate: Date,
    isDeleted: boolean
}
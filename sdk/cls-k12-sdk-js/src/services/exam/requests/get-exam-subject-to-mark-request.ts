

export interface GetExamSubjectToMarkRequest {
    gradeId?: number;
    subjectId?: number;
    status?:string;
    page: number;
    size: number
}
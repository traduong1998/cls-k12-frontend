export interface ExamInfoRequest {
    id: number
    /*
    tên kì thi
    */
    name: string
    /*
       thang đánh giá
    */
    assessmentLevelGroupId: number
    /*
    thời gian bắt đầu
    */
    startDate: Date,
    /*
    thời gian kết thúc
    */
    endDate: Date,
    /*
    phạm v truy cập
    */
    scope: string,
    /*
        Hình thúc thi
    */
    type: string
}
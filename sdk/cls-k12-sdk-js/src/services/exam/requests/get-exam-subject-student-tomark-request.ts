

export interface GetExamSubjectStudentToMarkRequest {
    organizeExamId: number;
    page: number;
    size: number;
    isFinish?:boolean;
}
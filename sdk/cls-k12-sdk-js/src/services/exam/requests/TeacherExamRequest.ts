
export interface TeacherExamRequest {
    shiftExamInfoId:number;
    keyword:string;
    divisionId?:number;
    schoolId?:number;
    isChecked?:boolean;
}
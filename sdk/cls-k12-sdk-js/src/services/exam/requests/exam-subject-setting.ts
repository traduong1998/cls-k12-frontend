export interface ExamSubjectSetting {
    id: number,
    displayPerPage: number,
    testTime: number,
    numberOfMediaOpening: number,
    isLockTestScreen: boolean,
    isShuffler: boolean
 
}


export interface SubjectsSupervisionSystem {
    examId: number;
    subjectId: number;
    organizeExamId: number;
    status?: string;
    roomName?: string;
    shiftId?: number;
    shiftName?: string;
    startDate?: Date;
    endDate?: Date;
    subjectName?: string;
    isCompleted?: boolean;
}
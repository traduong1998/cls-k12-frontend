export interface ExamSubjectSettingReponse {
    id: number,
    displayPerPage: number,
    testTime: number,
    numberOfMediaOpening: number,
    isLockTestScreen: boolean,
    isShuffler: boolean,
    subjectName?: string,
    maxTestTime:number,
}
export interface ExamSettingReponse{
    id:number,
    isPreservingTime:boolean,
    isPublicScore:boolean,
    isPublicAnswer:boolean,
    logTime:number
}
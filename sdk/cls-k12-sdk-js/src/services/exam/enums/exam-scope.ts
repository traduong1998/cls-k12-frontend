export enum ExamScope {
    Private = 'PRI',
    Public = 'PUB'
}
export enum ExamType {
    Free = 'FRE',
    Concentrated = 'CON'
}
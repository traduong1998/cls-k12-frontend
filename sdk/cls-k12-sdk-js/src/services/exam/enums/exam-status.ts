export enum ExamStatus {
    Editing = 'EDI',
    WaitingApprove = 'WAI',
    Public = "PUB",
    Refuse = "REF"
}
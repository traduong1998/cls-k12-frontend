
import { ExamSubject } from "../models/ExamSubject";
import { PaginatorResponse } from '../../../core/api/responses/PaginatorResponse';
import { TeacherExam } from "../models/TeacherExam";
import { TeacherExamRequest } from "../requests/TeacherExamRequest";

export interface ExamSubjectAPI {
    getListExamSubject(examId: number): Promise<ExamSubject[]>;
    getExamSubject(id:number):Promise<ExamSubject>;
    getListTeacherAssignment(examTeacherRequest:TeacherExamRequest):Promise<PaginatorResponse<TeacherExam> >;
    getListSupervisorExamSubject(examTeacherRequest:TeacherExamRequest):Promise<TeacherExam[]>;
    
}
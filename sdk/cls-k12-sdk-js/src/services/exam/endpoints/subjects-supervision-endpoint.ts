import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { Logger } from "../../../Logger";
import { FilterSubjectSupervisionRequests } from "../requests/filter-subject-supervision";
import { SubjectsSupervisionExam } from "../response/subjects-supervision-exam";
import { SubjectsSupervisionSystem } from "../response/subjects-supervision-system";
import { SubjectsSupervisionAPI } from "./subjects-supervision-api";

export class SubjectsSupervisionEndpoint implements SubjectsSupervisionAPI {

    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
        this.baseUrl += "/api";
    }
    getListExamIdByKeyword(keyword: string): Promise<number[]> {
        return new Promise((resolve, reject) => {
            let url = "exams/getlistexamid"
            axios.get(`${this.baseUrl}/${url}`, {
                params: { keyword: keyword }
            })
                .then((resp: AxiosResponse<number[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getSubjectsSupervisionSystemPaging(request?: FilterSubjectSupervisionRequests): Promise<PaginatorResponse<SubjectsSupervisionSystem>> {
        return new Promise((resolve, reject) => {
            let url = "OrganizeExams/GetListSubjectExamMonitorPaging";
            let listExamId = request?.listExamId?.length == 0 ? null : request?.listExamId;
            axios.post(`${this.baseUrl}/${url}`,
                { startDate: request?.startDate, endDate: request?.endDate, listExamIds: listExamId, page: request?.pageNumber, size: request?.pageSize, getCount: true })
                .then((resp: AxiosResponse<PaginatorResponse<SubjectsSupervisionSystem>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getSubjectsSupervisionExamPaging(request: number[]): Promise<SubjectsSupervisionExam[]> {
        return new Promise((resolve, reject) => {
            let url = "exams/GetExamMonitorById";
            axios.post(`${this.baseUrl}/${url}`, { listExamId: request == [] ? null : request })
                .then((resp: AxiosResponse<SubjectsSupervisionExam[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
}
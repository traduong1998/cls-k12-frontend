import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { ExamSupervisionStudent } from "../models/exam-supervision-student";
import { StudentInExam } from "../models/student-in-exam";
import { ExamSupervisionFilterReqest } from "../requests/exam-supervision-filter-request";
import { ViolatorsResponse } from "../responses/violators-response";


export interface ExamSupervisionAPI {
    //#region Filter
    getExamSupervisionFilter(request: ExamSupervisionFilterReqest): Promise<PaginatorResponse<ExamSupervisionStudent>>;
    getUserStatusByListUserId(listUserId: number[]): Promise<StudentInExam>;
    getExamByKeyword(keyword: string): Promise<number[]>;
    //#endregion


}
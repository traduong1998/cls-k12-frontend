
import axios, { AxiosError, AxiosResponse } from "axios";
import { ExamDetail } from "../models/exam-detail";
import { ExamInfo } from "../models/exam-info";
import { SubjectOfExam } from "../models/subject-of-exam";
import { EditExamRequest } from "../requests/edit-exam-request";
import { CreateExamRequest } from "../requests/create-exam-request";
import { ExamInfoRequest } from "../requests/exam-info-request";
import { ExamSetting } from "../requests/exam-setting";
import { ExamSubjectSetting } from "../requests/exam-subject-setting";
import { ExamApi } from "./exam-api";
import { SubjectTestkitRequest } from "../requests/update-subject-testkit-request";
import { ExamSubjectSettingReponse } from "../response/exam-subject-setting-response";
import { CLS } from "../../../CLS";
import { ExamStep } from "../requests/exam-step-request";
import { ExamSettingReponse } from "../response/exam-setting-reponse";
import { ErrorResponse } from "../../../core/api/responses/ErrorResponse ";
import { GetExamFilterRequests } from "../requests/get-exams";
import { AssessmentLevelGroupOption } from "../models/assessmen-level-group";
import { ViolatorsResponse } from "../responses/violators-response";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { ExamSubjectStudentToMark } from "../models/exam-subject-student-tomark";
import { GetExamSubjectStudentToMarkRequest } from "../requests/get-exam-subject-student-tomark-request";
import { TeacherMarkingEssay } from "../models/teacher-marking-essay";
import { GetExamSubjectToMarkRequest } from "../requests/get-exam-subject-to-mark-request";
import { UnitName } from "../models/unit-name";

export class ExamEnpoint implements ExamApi {
    baseUrl: string;
    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    GetDonViNameExam(listCreateLevel: string[], listDonviId: number[]): Promise<UnitName[]> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/OrganizeExams/GetDonViNameExam`, { listCreateLevel, listDonviId }).then((res: AxiosResponse<UnitName[]>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }
    getSettingExam(examId: number): Promise<ExamSettingReponse> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/exams/` + examId + `/settings`).then((res: AxiosResponse<ExamSettingReponse>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }

    updateExamFinishAndapproval(examId: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/exams/${examId}` + `/finishandapproval`, { id: examId }).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }
    updateExamFinish(examId: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/exams/${examId}` + `/finish`).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }
    updateStepExam(examId: number, examStep: ExamStep): Promise<ExamStep> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/exams/${examId}` + `/actions/step`, examStep, {

            }).then((res: AxiosResponse<ExamStep>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }
    getExamSubjectSetting(examId: number, subjectId: number): Promise<ExamSubjectSettingReponse> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/examsubjects/settings`, {
                params: { examId: examId, subjectId: subjectId }
            }).then((res: AxiosResponse<ExamSubjectSettingReponse>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }

    getSubjectExam(examId: number): Promise<SubjectOfExam[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/exams/` + examId + `/examsubject`).then((res: AxiosResponse<SubjectOfExam[]>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }

    updateExam(id: number, examInfo: EditExamRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/exams/${id}`, examInfo, {

            }).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }

    updateExamSubjectTestKit(subjectId: number, subjectTestkitRequest: SubjectTestkitRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/examsubjects/` + subjectId + `/testkits`, subjectTestkitRequest, {
            }).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            }).catch(err => {
                reject(err);
            })
        })
    }

    deleteExam(id: number): Promise<ExamDetail> {
        return new Promise((resolve, reject) => {
            axios.delete(`${this.baseUrl}/exams/` + id,).then((res: AxiosResponse<ExamDetail>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }

    getExams(request: GetExamFilterRequests): Promise<PaginatorResponse<ExamInfo>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/exams`, {
                params: request
            })
                .then((resp: AxiosResponse<PaginatorResponse<ExamInfo>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }

    updateExamSubjectSetting(subjectId: number, subjectSetting: ExamSubjectSetting): Promise<ExamSubjectSetting> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/examsubjects/` + subjectId + `/settings`, subjectSetting, {
            })
                .then((res: AxiosResponse<ExamSubjectSetting>) => {
                    resolve(res.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }

    updateInfoExam(id: number, examInfo: ExamInfoRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/exams/` + id + `/info`, examInfo, {

            }).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }
    getInfoExam(id: number): Promise<ExamDetail> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/exams/` + id, {

            }).then((res: AxiosResponse<ExamDetail>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }
    createSettingExam(examSetting: ExamSetting, id: number): Promise<ExamSetting> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/exams/` + id + `/settings`, examSetting, {

            }).then((res: AxiosResponse<ExamSetting>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }
    createInfoExam(infoExam: CreateExamRequest): Promise<ExamDetail> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/exams`, infoExam, {

            }).then((resp: AxiosResponse<ExamDetail>) => {
                resolve(resp.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        });
    }
    getAssessmenLevelGroupOptions(): Promise<AssessmentLevelGroupOption[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/assessmentlevelgroups/assessmenlevelgroupoptions`, {

            }).then((res: AxiosResponse<AssessmentLevelGroupOption[]>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }
    getExamStatus(id: number): Promise<string> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/exams/` + id + `/status`, {

            }).then((res: AxiosResponse<string>) => {
                resolve(res.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }
    getExamsApproval(request: GetExamFilterRequests): Promise<PaginatorResponse<ExamInfo>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/exams/approval`, {
                params: request
            })
                .then((resp: AxiosResponse<PaginatorResponse<ExamInfo>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }
    approveExam(examId: number, isApproved: boolean): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/exams/${examId}/approval`,
                {
                    id: examId, isApproval: isApproved
                })
                .then((resp: AxiosResponse<boolean>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }
    deleteExams(ids: number[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.delete(`${this.baseUrl}/exams/delete-exams`, {
                data: {
                    ids: ids
                }
            })
                .then((res: AxiosResponse<boolean>) => {
                    resolve(res.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }

    getContestantIds(examId: number, subjectId: number, organizeExamId: number, filter: { isCompleted?: boolean, isSuspended?: boolean, isFinished?: boolean }): Promise<number[]> {
        return new Promise((resolve, reject) => {
            // resolve([85, 84, 25, 23, 10]);
            axios.get(`${this.baseUrl}/exams/${examId}/subjects/${subjectId}/organizeExams/${organizeExamId}/contestants/ids`, {
                params: filter
            })
                .then((res: AxiosResponse<number[]>) => {
                    resolve(res.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }

    getViolators(examId: number, subjectId: number, shiftId: number): Promise<ViolatorsResponse[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/exams/${examId}/subjects/${subjectId}/shifts/${shiftId}/violators`)
                .then((resp: AxiosResponse<ViolatorsResponse[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getExamSubjectToMarkQuery(request: GetExamSubjectToMarkRequest): Promise<PaginatorResponse<TeacherMarkingEssay>> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/OrganizeExams/GetListExamSubjectToMark`, request)
                .then((resp: AxiosResponse<PaginatorResponse<TeacherMarkingEssay>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getListExamSubjectStudentToMark(request: GetExamSubjectStudentToMarkRequest): Promise<PaginatorResponse<ExamSubjectStudentToMark>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/doexams/getexamsubjectstudenttomarking`, {
                params: request
            })
                .then((resp: AxiosResponse<PaginatorResponse<ExamSubjectStudentToMark>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}
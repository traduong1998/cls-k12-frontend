import axios, { AxiosResponse } from "axios";
var qs = require('qs');
import { CLS } from "../../../CLS";
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { DoExamLog } from "../models/do-exam-log";


export class DoExamLogEndpoint {
    private baseUrl: string;

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
        this.baseUrl += "/api/do-exam-logs";
        // this.baseUrl = `http://localhost:58910/api/v1/do-exam-logs`;
    }

    getLogsOfUsers(request: { examId: number, subjectId: number, shiftId: number, action: string }): Promise<PaginatorResponse<DoExamLog>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}`,
                {
                    params: request,
                    paramsSerializer: params => {
                        return qs.stringify(params);
                    }
                })

                .then((resp: AxiosResponse<PaginatorResponse<DoExamLog>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
import axios, { AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { Logger } from "../../../Logger";
import { ExamSubject } from "../models/ExamSubject";
import { TeacherExam } from "../models/TeacherExam";
import { TeacherExamRequest } from "../requests/TeacherExamRequest";

import { ExamSubjectAPI } from "./ExamSubjectAPI";

export class ExamSubjectEndPoint implements ExamSubjectAPI {

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
    }
    // TODO : Endpoint :
    endpoint = '';

    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();

    getListExamSubject(examId: number): Promise<ExamSubject[]> {
        this.logger.warn('get list ExamSubject');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/`, { params: examId })
                .then((resp: AxiosResponse<ExamSubject[]>) => {
                    console.log(`get from json`, resp);

                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getExamSubject(id: number): Promise<ExamSubject> {
        this.logger.warn('usertype get3');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`/assets/fakedata/exam-subject.json`)
                .then((resp: AxiosResponse<ExamSubject>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getListTeacherAssignment(examTeacherRequest: TeacherExamRequest): Promise<PaginatorResponse<TeacherExam> > {
        this.logger.warn('list teacher');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`/assets/fakedata/teacher-exam.json`)
                .then((resp: AxiosResponse<PaginatorResponse<TeacherExam>>) => {
                    console.log(`get listTeacher`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getListSupervisorExamSubject(examTeacherRequest: TeacherExamRequest): Promise<TeacherExam[] > {
        this.logger.warn('list teacher');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`/assets/fakedata/teacher-exam-subject.json`)
                .then((resp: AxiosResponse<TeacherExam[]>) => {
                    console.log(`get listTeacher`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
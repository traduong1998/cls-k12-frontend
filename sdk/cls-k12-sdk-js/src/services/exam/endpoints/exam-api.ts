import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { ExamDetail } from "../models/exam-detail";
import { ExamInfo } from "../models/exam-info";
import { SubjectOfExam } from "../models/subject-of-exam";
import { EditExamRequest } from "../requests/edit-exam-request";
import { CreateExamRequest } from "../requests/create-exam-request";
import { ExamInfoRequest } from "../requests/exam-info-request";
import { ExamSetting } from "../requests/exam-setting";
import { ExamSubjectSetting } from "../requests/exam-subject-setting";
import { SubjectTestkitRequest } from "../requests/update-subject-testkit-request";
import { ExamSubjectSettingReponse } from "../response/exam-subject-setting-response";
import { ExamStep } from "../requests/exam-step-request";
import { ExamSettingReponse } from "../response/exam-setting-reponse";
import { GetExamFilterRequests } from "../requests/get-exams";
import { AssessmentLevelGroupOption } from "../models/assessmen-level-group";
import { GetExamSubjectToMarkRequest } from "../requests/get-exam-subject-to-mark-request";
import { TeacherMarkingEssay } from "../models/teacher-marking-essay";
import { ExamSubjectStudentToMark } from "../models/exam-subject-student-tomark";
import { GetExamSubjectStudentToMarkRequest } from "../requests/get-exam-subject-student-tomark-request";
import { UnitName } from "../models/unit-name";


export interface ExamApi {
    getInfoExam(id: number): Promise<ExamDetail>;

    getExams(request: GetExamFilterRequests): Promise<PaginatorResponse<ExamInfo>>;

    getSettingExam(examId: number): Promise<ExamSettingReponse>;

    getSubjectExam(examId: number): Promise<SubjectOfExam[]>;

    getExamSubjectSetting(examId: number, subjectId: number): Promise<ExamSubjectSettingReponse>

    updateExam(id: number, examInfo: EditExamRequest): Promise<boolean>;



    createInfoExam(infoExam: CreateExamRequest): Promise<ExamDetail>;

    createSettingExam(examSetting: ExamSetting, id: number): Promise<ExamSetting>;

    updateInfoExam(id: number, examInfo: ExamInfoRequest): Promise<boolean>;

    ///updateSettingExam(id: number, examInfo: ExamSetting): Promise<ExamSetting>;

    updateExamSubjectSetting(subjectId: number, subjectSetting: ExamSubjectSetting): Promise<ExamSubjectSetting>;

    updateExamSubjectTestKit(subjectId: number, subjectTestkitRequest: SubjectTestkitRequest): Promise<boolean>;


    updateStepExam(examId: number, examStep: ExamStep): Promise<ExamStep>;

    updateExamFinish(examId: number): Promise<boolean>;

    updateExamFinishAndapproval(examId: number): Promise<boolean>;

    deleteExam(id: number): Promise<ExamDetail>;

    getAssessmenLevelGroupOptions(): Promise<AssessmentLevelGroupOption[]>;

    getExamStatus(id: number): Promise<string>;

    getExamsApproval(request: GetExamFilterRequests): Promise<PaginatorResponse<ExamInfo>>;

    approveExam(examId: number, isApproved: boolean): Promise<boolean>;
    getExamSubjectToMarkQuery(request: GetExamSubjectToMarkRequest): Promise<PaginatorResponse<TeacherMarkingEssay>>
    getListExamSubjectStudentToMark(request: GetExamSubjectStudentToMarkRequest): Promise<PaginatorResponse<ExamSubjectStudentToMark>>

    GetDonViNameExam(listCreateLevel: string[], listDonviId: number[]): Promise<UnitName[]>
}
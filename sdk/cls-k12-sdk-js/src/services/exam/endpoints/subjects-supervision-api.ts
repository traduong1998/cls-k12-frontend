import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { FilterSubjectSupervisionRequests } from "../requests/filter-subject-supervision";
import { SubjectsSupervisionExam } from "../response/subjects-supervision-exam";
import { SubjectsSupervisionSystem } from "../response/subjects-supervision-system";

export interface SubjectsSupervisionAPI {
    getSubjectsSupervisionSystemPaging(request?: FilterSubjectSupervisionRequests): Promise<PaginatorResponse<SubjectsSupervisionSystem>>;
    getSubjectsSupervisionExamPaging(request: number[]): Promise<SubjectsSupervisionExam[]>;
    getListExamIdByKeyword(keyword: string): Promise<number[]>
}
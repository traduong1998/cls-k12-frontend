import axios, { AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { Logger } from "../../../Logger";
import { ExamSupervisionStudent } from "../models/exam-supervision-student";
import { StudentInExam } from "../models/student-in-exam";
import { ExamSupervisionFilterReqest } from "../requests/exam-supervision-filter-request";
import { ViolatorsResponse } from "../responses/violators-response";
import { ExamSupervisionAPI } from "./exam-supervision-api";
var qs = require('qs');

export class ExamSupervisionEndpoint implements ExamSupervisionAPI {
    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
        this.baseUrl += "/api";
    }
    
    getExamSupervisionFilter(request: ExamSupervisionFilterReqest): Promise<PaginatorResponse<ExamSupervisionStudent>> {
        return new Promise((resolve, reject) => {
            let url = 'OrganizeExams/GetListStudentOfOrganizeExamInfo';
            axios.get(`${this.baseUrl}/${url}`,
                {
                    params: request,
                    paramsSerializer: params => {
                        return qs.stringify(params);
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<ExamSupervisionStudent>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getUserStatusByListUserId(listUserId: number[]): Promise<StudentInExam> {
        throw new Error("Method not implemented.");
    }
    getExamByKeyword(keyword: string): Promise<number[]> {
        throw new Error("Method not implemented.");
    }
}
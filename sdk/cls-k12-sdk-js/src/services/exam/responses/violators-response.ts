

export interface ViolatorsResponse {
    id: number;
    violations: ViolationResponse[];
}

export interface ViolationResponse {
    creatorId: number;
    violationType: string;
    minusScore?: number;
    reason?: string;
}
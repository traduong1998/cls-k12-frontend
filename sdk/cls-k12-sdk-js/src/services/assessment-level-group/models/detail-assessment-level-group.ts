export interface DetailAssessmentLevelGroup {
    id: 1,
    highestScore: 100,
    passedScore: 50,
    listAssessmentLevelDTO: AssessmentLevelGroup[]
}

export interface AssessmentLevelGroup {
    id: number,
    assessmentLevelId: number,
    name: string,
    lowestCoefficient: number
}
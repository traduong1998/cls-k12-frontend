export interface AssessmentLevel {
    id: number,
    name: string,
    lowestCoefficient: number
}
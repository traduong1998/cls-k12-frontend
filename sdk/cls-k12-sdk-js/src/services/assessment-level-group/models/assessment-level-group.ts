export interface AddassessmentLevelGroup {
    name: string,
    highestScore: number,
    passedScore: number,
    isActived: boolean,
    portalId: number,
    divisionId: number,
    schoolId: number,
    isDeleted: boolean,
    id: number,
    domainEvents: string
}
import { AssessmentLevel } from "./models/assessment-level";
import { AddassessmentLevelGroup } from "./models/assessment-level-group";
import { DetailAssessmentLevelGroup } from "./models/detail-assessment-level-group";
import { AssessmentLevelGroupRequest } from "./requests/assessment-level-group-request";
import { AssessmentLevelGroupUpdateRequest } from "./requests/assessment-level-group-update-request";
import { AssessmentLevelRequest } from "./requests/assessment-level-request";
import { AssessmentLevelUpdateRequest } from "./requests/assessment-level-update-request";

export interface AssessmentLevelGroupApi {
    getAssessmentLevelGroup(): Promise<AddassessmentLevelGroup[]>
    getDetailAssessmentLevelGroup(id: number): Promise<DetailAssessmentLevelGroup>


    getAssessmentLevelGroupById(id: number): Promise<AddassessmentLevelGroup>
    getDetailAssessmentLevelGroupById(id: number): Promise<AssessmentLevel>

    addAssessmentLevelGroup(request: AssessmentLevelGroupRequest): Promise<boolean>
    updateAssessmentLevelGroup(request: AssessmentLevelGroupUpdateRequest): Promise<boolean>
    deleteAssessmentLevelGroup(id: number): Promise<boolean>

    addAssessmentLevel(request: AssessmentLevelRequest): Promise<boolean>
    updateAssessmentLevel(request: AssessmentLevelUpdateRequest): Promise<boolean>
    deleteAssessmentLevel(id: number): Promise<boolean>
}


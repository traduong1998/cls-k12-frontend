export interface AssessmentLevelGroupRequest {
    name: string,
    highestScore: number,
    passedScore: number,
    isActived: boolean
}
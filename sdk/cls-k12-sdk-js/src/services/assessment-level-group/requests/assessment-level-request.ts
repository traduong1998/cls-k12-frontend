export interface AssessmentLevelRequest {
    name: string,
    lowestCoefficient: number,
    assessmentLevelGroupId: number
}
export interface AssessmentLevelUpdateRequest {
    id: number,
    name: string,
    lowestCoefficient: number
}
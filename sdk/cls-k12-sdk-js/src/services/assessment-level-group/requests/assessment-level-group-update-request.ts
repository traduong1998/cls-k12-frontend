export interface AssessmentLevelGroupUpdateRequest {
    id: number,
    name: string,
    highestScore: number,
    passedScore: number,
    isActived: boolean
}
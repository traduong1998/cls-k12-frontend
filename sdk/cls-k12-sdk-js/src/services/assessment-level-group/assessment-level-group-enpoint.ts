import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS } from "../../CLS";
import { Logger } from "../../Logger";
import { AssessmentLevelGroupApi } from "./assessment-level-group-api";
import { AssessmentLevel } from "./models/assessment-level";
import { AddassessmentLevelGroup } from "./models/assessment-level-group";
import { DetailAssessmentLevelGroup } from "./models/detail-assessment-level-group";
import { AssessmentLevelGroupRequest } from "./requests/assessment-level-group-request";
import { AssessmentLevelGroupUpdateRequest } from "./requests/assessment-level-group-update-request";
import { AssessmentLevelRequest } from "./requests/assessment-level-request";
import { AssessmentLevelUpdateRequest } from "./requests/assessment-level-update-request";

export class AssessmentLevelGroupEnpoint implements AssessmentLevelGroupApi {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: string) {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
    }
    getAssessmentLevelGroupById(id: number): Promise<AddassessmentLevelGroup> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/assessmentlevelgroup/getassessmentlevelgroupbyId`, { params: { id: id } })
                .then((resp: AxiosResponse<AddassessmentLevelGroup>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    getDetailAssessmentLevelGroupById(id: number): Promise<AssessmentLevel> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/assessmentlevelgroup/getassessmentlevelbyId`, { params: { id: id } })
                .then((resp: AxiosResponse<AssessmentLevel>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    getAssessmentLevelGroup(): Promise<AddassessmentLevelGroup[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/assessmentlevelgroup/getassessmentlevelgroup`)
                .then((resp: AxiosResponse<AddassessmentLevelGroup[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    getDetailAssessmentLevelGroup(id: number): Promise<DetailAssessmentLevelGroup> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/assessmentlevelgroup/getdetailassessmentlevelgroup`, { params: { id: id } })
                .then((resp: AxiosResponse<DetailAssessmentLevelGroup>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    addAssessmentLevelGroup(request: AssessmentLevelGroupRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/api/assessmentlevelgroup/addassessmentlevelgroup`, request)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    updateAssessmentLevelGroup(request: AssessmentLevelGroupUpdateRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/api/assessmentlevelgroup/updateassessmentlevelgroup`, request)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    deleteAssessmentLevelGroup(id: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/api/assessmentlevelgroup/deleteassessmentlevelgroup`, { id: id })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    addAssessmentLevel(request: AssessmentLevelRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/api/assessmentlevelgroup/addassessmentlevel`, request)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    updateAssessmentLevel(request: AssessmentLevelUpdateRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/api/assessmentlevelgroup/updateassessmentlevel`, request)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    deleteAssessmentLevel(id: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/api/assessmentlevelgroup/deleteassessmentlevel`, { id: id })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

}
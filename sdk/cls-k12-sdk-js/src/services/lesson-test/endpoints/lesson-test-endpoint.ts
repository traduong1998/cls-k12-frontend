import { CLS } from './../../../CLS';
import { TestCodeUrlResponse } from '../responses/test-code-url-response';
import { LessonTestAPI } from './lesson-test-api';
import axios, { AxiosResponse } from 'axios';
export class LessonTestEndpoint implements LessonTestAPI {
    baseUrl: string;
    baseUrlConvertFile?: string;
    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrls.testKitBank ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api/v1';
        this.baseUrlConvertFile = CLS.getConfig().apiBaseUrls.questionConverter;
    }
    getTestForStudent(lessonId: number, contentId: number, testId: number, getTestShuffle: boolean): Promise<TestCodeUrlResponse> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/lessontest/student/tests`, { params: { lessonId: lessonId,contentId: contentId, testId: testId, getTestShuffle: getTestShuffle } })
            .then((resp: AxiosResponse<TestCodeUrlResponse>) => {
                resolve(resp.data);
            }).catch(error => {
                reject(error);
            })
        });
    }
    getResultForTest(lessonId: number,contentId: number, testId: number): Promise<TestCodeUrlResponse> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/lessontest/teacher/tests`, { params: { lessonId: lessonId,contentId: contentId, testId: testId } })
            .then((resp: AxiosResponse<TestCodeUrlResponse>) => {
                resolve(resp.data);
            }).catch(error => {
                reject(error);
            })
        });
    }

}
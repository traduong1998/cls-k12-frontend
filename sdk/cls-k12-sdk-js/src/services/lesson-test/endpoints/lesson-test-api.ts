import { TestCodeUrlResponse } from "../responses/test-code-url-response";

export interface LessonTestAPI {
    getTestForStudent(lessonId: number, contentId: number, testId: number, getTestShuffle: boolean): Promise<TestCodeUrlResponse>;
    getResultForTest(lessonId: number,contentId: number, testId: number): Promise<TestCodeUrlResponse>;
}
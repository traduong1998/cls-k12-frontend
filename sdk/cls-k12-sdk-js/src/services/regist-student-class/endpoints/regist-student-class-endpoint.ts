import axios, { AxiosError, AxiosResponse } from "axios";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { CLS } from "../../../CLS";
import { ErrorResponse } from "../../../core/api/responses/ErrorResponse ";
import { ListStudentForAssignedResponse } from "../../lesson/responses/listStudentForAssignedResponse";
import { AssignStudentParams } from "../models/assign-user";
import { ListStudentForAssigned, RegistStudentResponse } from "../models/regist-student";
import { RegistStudentAPI } from "./regist-student-class-api";


export class RegistStudentEndpoint implements RegistStudentAPI {
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
    }

    getListStudentAssign(request: ListStudentForAssigned): Promise<PaginatorResponse<ListStudentForAssignedResponse>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/api/MeetingRooms/GetUserMeetingInfoPaging`, {
                params: {
                    DivisionId: request.DivisionId, SchoolId: request.SchoolId, GradeId: request.GradeId, GroupStudentId: request.GroupStudentId,
                    meetingRoomId: request.meetingRoomId, schoolYear: request.schoolYear, SortField: request.SortField, SortDirection: request.SortDirection,
                    Page: request.pageNumber, Size: request.sizeNumber, GetCount: request.getCount
                }
            })
                .then((resp: AxiosResponse<PaginatorResponse<ListStudentForAssignedResponse>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getListMeetingRoomsUser(informationFilter: any): Promise<PaginatorResponse<RegistStudentResponse>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/api/MeetingRooms/GetMyMeetingRoomsPaging`, { params: informationFilter })
                .then((resp: AxiosResponse<PaginatorResponse<RegistStudentResponse>>) => {
                    resolve(resp.data);
                }).catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    assignUserMeeting(list: AssignStudentParams): Promise<boolean> {
        const link = '/api/MeetingRooms/AssignUserMeeting'.trim();
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}${link}`, list).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            }).catch((error: AxiosError<ErrorResponse>) => {
                reject(error.response?.data);
            });
        });
    }
}

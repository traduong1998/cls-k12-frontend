import { PaginatorResponse } from '../../../../core/api/responses/PaginatorResponse';
import { ListStudentForAssignedResponse } from '../../lesson/responses/listStudentForAssignedResponse';
import { AssignStudentParams } from '../models/assign-user';
import { ListStudentForAssigned, RegistStudentResponse } from '../models/regist-student';

export interface RegistStudentAPI {
    assignUserMeeting(list: AssignStudentParams): Promise<boolean>
    getListStudentAssign(request: ListStudentForAssigned): Promise<PaginatorResponse<ListStudentForAssignedResponse>>
    getListMeetingRoomsUser(informationFilter: any): Promise<PaginatorResponse<RegistStudentResponse>>
}

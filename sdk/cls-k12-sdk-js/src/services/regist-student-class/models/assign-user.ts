
export interface AssignStudentParams {
    meetingRoomId: number;
    listUserMeetings: AssignAndApprovalStudentDetai[];
}

interface AssignAndApprovalStudentDetai {
    id: number;
    action: string;
}

export interface RegistStudentResponse {
    id?: number;
    name?: string;
    type?: number;
    avatarTeacher?: string;
    nameTeacher?: string;
    status?: number;
    startTime?: string;
    endTime?: string;
    linkClass?: string,
    Page?: number;
    Size?: number;
    startTimeConvert?:any,
    endTimeConvert?:any
}
export interface ListStudentForAssigned {
    DivisionId?: number;
    SchoolId?: number;
    GradeId?: number;
    GroupStudentId?: number;
    schoolYear?: number;
    SortField?: string;
    SortDirection?: string;
    pageNumber: number | undefined;
    sizeNumber: number | undefined;
    getCount?: boolean;
    meetingRoomId?: number;
}
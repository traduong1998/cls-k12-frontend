import { MeetingRoomTeacherLogsModel } from "../../meeting-room/models/meeting-room-teacher-logs-model";

export interface MeetingRoomTeacherLogsAPI {
    getByMeetingRoomIdAndUserId(meetingRoomId: number): Promise<MeetingRoomTeacherLogsModel>;
    create(meetingRoomId: number): Promise<MeetingRoomTeacherLogsModel>;
    update(meetingRoomId: number, action?: string): Promise<MeetingRoomTeacherLogsModel>;
}
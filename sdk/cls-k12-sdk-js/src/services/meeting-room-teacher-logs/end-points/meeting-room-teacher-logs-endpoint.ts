import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS, Logger } from "../../..";
import { MeetingRoomTeacherLogsModel } from "../../meeting-room/models/meeting-room-teacher-logs-model";
import { MeetingRoomTeacherLogsAPI } from "./meeting-room-teacher-logs-api";

export class MeetingRoomTeacherLogsEndpoint implements MeetingRoomTeacherLogsAPI {
    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();
    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
        // this.baseUrl = 'http://localhost:65000/api/v1';
        this.baseUrl = this.baseUrl + '/api'
    }

    getByMeetingRoomIdAndUserId(meetingRoomId: number): Promise<MeetingRoomTeacherLogsModel> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/MeetingRooms/${meetingRoomId}/teacher/logs`)
                .then((resp: AxiosResponse<MeetingRoomTeacherLogsModel>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    create(meetingRoomId: number): Promise<MeetingRoomTeacherLogsModel> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/MeetingRooms/teacher/logs`, { meetingRoomId: meetingRoomId })
                .then((resp: AxiosResponse<MeetingRoomTeacherLogsModel>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    update(meetingRoomId: number, action: string): Promise<MeetingRoomTeacherLogsModel> {
        return new Promise((resolve, reject) => {
            let data = { meetingRoomId: meetingRoomId, action: action }
            axios
                .patch(`${this.baseUrl}/MeetingRooms/{meetingRoomId}/teacher/logs`, data)
                .then((resp: AxiosResponse<MeetingRoomTeacherLogsModel>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }


}
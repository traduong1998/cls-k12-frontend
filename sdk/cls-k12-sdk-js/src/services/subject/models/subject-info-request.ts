export interface SubjectInfo{
    id:number,
    name:string,
    orderNumber:number;
}
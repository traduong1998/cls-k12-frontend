export interface Subject {
    id?: number;
    name?: string;
    orderNumber?: number;
    schoolId?: number;
    portalId?: number;
    isDeleted?: number;
    createBy?: string;
    createdDate?: Date;
    icon?: string;
}

export interface SubjectOption {
    id: number;
    name: string;
}

export interface SubjectOfGrade {
    id: number;
}

import { Subject } from "../models/Subject";

export class CreateSubjectRequest implements Subject {
    id?: number | undefined;
    name?: string | undefined;
    orderNumber?: number;
    createdDate?: Date;
    createBy?: string | undefined;
    portalId?: number | undefined;
    isDeleted?: number | undefined;
    icon?: string | undefined;


    constructor() {
        this.createdDate = new Date();
    }

}

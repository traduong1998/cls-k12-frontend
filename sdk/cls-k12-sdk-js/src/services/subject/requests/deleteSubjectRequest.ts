import { Subject } from "../models/Subject";

export class DeleteSubjectRequest implements Subject {
    id?: number | undefined;
}
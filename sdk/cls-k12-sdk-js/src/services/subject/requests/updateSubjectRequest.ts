import { Subject } from "../models/Subject";

export class UpdateSubjectRequest implements Subject {
    id?: number | undefined;
    name?: string | undefined;
    orderNumber?: number | undefined;
    createdDate?: Date;
    createBy?: string | undefined;
    portalId?: number | undefined;
    isDeleted?: number | undefined;
    icon?: string | undefined;

}
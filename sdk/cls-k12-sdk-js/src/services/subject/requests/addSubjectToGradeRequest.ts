export class AddSubjectToGradeRequest{
    gradeId?: number | undefined;
    subjectId?: number[] | undefined;
}
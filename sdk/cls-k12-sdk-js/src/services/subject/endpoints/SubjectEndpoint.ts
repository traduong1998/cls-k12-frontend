import axios, { AxiosError, AxiosResponse } from "axios";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { Logger } from "../../../Logger";
import { DeleteSubjectRequest } from "../requests/deleteSubjectRequest";
import { UpdateSubjectRequest } from "../requests/updateSubjectRequest";
import { CreateSubjectRequest } from "../requests/createSubjectRequest";
import { Subject, SubjectOption, SubjectOfGrade } from "../models/Subject";
import { SubjectFilterRequests } from "../models/SubjectPagingRequest";
import { SubjectAPI } from "./SubjectAPI";
import { SubjectInfo } from "../models/subject-info-request";
import { AddSubjectToGradeRequest } from "../requests/addSubjectToGradeRequest";
import { CLS } from "../../../CLS";

export class SubjectEndpoint implements SubjectAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        //this.baseUrl = config.baseUrl;
    }

    getSubjectById(subjectId: number): Promise<SubjectInfo> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/subjects/getsubject`, { params: { Id: subjectId } })
                .then((resp: AxiosResponse<SubjectInfo>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    getSubjectInfo(subjectId: number): Promise<SubjectInfo> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/Subjects/GetSubject`, { params: { Id: subjectId } })
                .then((resp: AxiosResponse<SubjectInfo>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    getSubjectsPaging(filter: SubjectFilterRequests): Promise<PaginatorResponse<Subject>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/Subjects/GetSubjectsPaging`, {
                    params: {
                        sortField: filter.sortField, sortDirection: filter.sortDirection,
                        page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<Subject>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    createSubject(subject: CreateSubjectRequest): Promise<CreateSubjectRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Subjects/CreateSubject`, subject)
                .then((resp: AxiosResponse<CreateSubjectRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    updateSubject(subject: UpdateSubjectRequest): Promise<UpdateSubjectRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Subjects/UpdateSubject`, subject)
                .then((resp: AxiosResponse<UpdateSubjectRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    deleteSubject(subject: DeleteSubjectRequest): Promise<DeleteSubjectRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Subjects/DeleteSubject`, subject)
                .then((resp: AxiosResponse<DeleteSubjectRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getSubjectOptions(gradeId: number): Promise<SubjectOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/Subjects/GetSubjectOptions`, { params: { gradeId: gradeId } })
                .then((resp: AxiosResponse<SubjectOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getSubjectOfGrade(gradeId?: number): Promise<SubjectOfGrade[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/Grades/GetSubjectOfGradeAsync`, { params: { gradeId: gradeId } })
                .then((resp: AxiosResponse<SubjectOfGrade[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    addSubjectToGrade(grade: AddSubjectToGradeRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Grades/AddSubjectToGrade`, grade)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getSubjectOptionsByIds(ids: number[]): Promise<SubjectOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Subjects/GetListSubjectByIds`, ids)
                .then((resp: AxiosResponse<SubjectOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    checkNameSubject(id: number, name: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/Subjects/CheckSubjectName`, { params: { Id: id, Name: name } })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

}

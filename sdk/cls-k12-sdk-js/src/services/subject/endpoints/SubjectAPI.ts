import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { Subject, SubjectOption, SubjectOfGrade } from "../models/Subject";
import { DeleteSubjectRequest } from "../requests/deleteSubjectRequest";
import { UpdateSubjectRequest } from "../requests/updateSubjectRequest";
import { CreateSubjectRequest } from "../requests/createSubjectRequest";
import { SubjectFilterRequests } from "../models/SubjectPagingRequest";
import { AddSubjectToGradeRequest } from "../requests/addSubjectToGradeRequest";
import { SubjectInfo } from "../models/subject-info-request";

export interface SubjectAPI {

    getSubjectsPaging(filter: SubjectFilterRequests): Promise<PaginatorResponse<Subject>>
    createSubject(division: CreateSubjectRequest): Promise<CreateSubjectRequest>;
    updateSubject(division: UpdateSubjectRequest): Promise<UpdateSubjectRequest>;
    deleteSubject(division: DeleteSubjectRequest): Promise<DeleteSubjectRequest>;
    getSubjectOptions(schoolId?: number): Promise<SubjectOption[]>;
    getSubjectOfGrade(gradeId?: number): Promise<SubjectOfGrade[]>;
    addSubjectToGrade(grade: AddSubjectToGradeRequest): Promise<boolean>
    getSubjectInfo(subjectId:number):Promise<SubjectInfo>
    getSubjectOptionsByIds(ids: number[]): Promise<SubjectOption[]> 
    checkNameSubject(id: number, name: string): Promise<boolean>;
}

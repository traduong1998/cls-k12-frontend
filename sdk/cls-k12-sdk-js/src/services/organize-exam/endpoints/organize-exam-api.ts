
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { TeacherAction } from "../../user-exam/requests/teacher-exam-request";
import { UserAction } from "../../user-exam/requests/user-action-request";
import { ExamSchool } from "../models/ExamSchool";
import { ExamSchoolInfo } from "../models/ExamSchoolInfo";
import { ExamSubjectOrganize,ExamSubjectOrganizeStatus } from "../models/ExamSubjectOrganize";
import { OrganizeExamFinish } from "../models/organize-exam-finish";
import { OrganizeExamDetail } from "../models/OrganizeExamDetail";
import { School } from "../models/School";
import { ShiftInfo } from "../models/ShiftInfo";
import { AssignSchoolRequest } from "../request/assign-school-request";
import { AssignStudentRequest } from "../request/assign-student-request";
import { AssignStudentsToShiftExamsRequest } from "../request/assign-students-shiftexams";
import { AssignSupervisorRequest } from "../request/assign-supervisor-request";
import { AssignTeacherRequest } from "../request/assign-teacher-request";
import { CreateOrganizeExamRequest } from "../request/create-organize-exam-request";
import { AddShiftInfoRequest, ShiftInfoAdd } from "../request/generate-shift-request";
import { GetListExamSchoolPagingRequest } from "../request/get-list-exam-school-paging-request";
import { GetSchoolRequest } from "../request/get-school-request";
import { UpdateOrganizeExamRequest } from "../request/update-organize-exam-request";
import { ShiftInfoUpdateRequest } from "../request/update-shift-request";

export interface OrganizeExamAPI{
   createOrganizeExam(request:CreateOrganizeExamRequest): Promise<number>;
   genarateListOrganizeExamInfo(request:AddShiftInfoRequest):Promise<ShiftInfo[]>;
   updateListOrganizeExamInfo(request:ShiftInfo[]):Promise<boolean>;
   addOrganizeExamInfo(request:AddShiftInfoRequest):Promise<ShiftInfo[]>;
   deleteOrganizeExamInfo(listIds:number[]):Promise<boolean>;
   updateOrganizeExamStep(step:number):Promise<boolean>;
   updateOrganizeExam(request:UpdateOrganizeExamRequest):Promise<boolean>;
   getListOrganizeExamInfo(organizeExamId:number):Promise<ShiftInfo[]>;
   assignExamToSchools(request:AssignSchoolRequest):Promise<boolean>;
   getSchools(request:GetSchoolRequest):Promise<PaginatorResponse<School>>;
   getListExamSchoolInfo():Promise<ExamSchoolInfo[]>;
   getListExamSchoolPaging(request:GetListExamSchoolPagingRequest):Promise<PaginatorResponse<ExamSchool>>;
   getListSubjectOrganize(id:number):Promise<ExamSubjectOrganize[]>
   getOrganizeExamDetail(id:number):Promise<OrganizeExamDetail>
   assignStudentsToExam(assignRequest: AssignStudentRequest): Promise<boolean>;
   assignSupervisorsToExam(assignRequest: AssignSupervisorRequest): Promise<boolean>;
   assignTeachersToExam(assignRequest: AssignTeacherRequest): Promise<boolean>;
   assignStudentsToShiftExams(assignRequest: AssignStudentsToShiftExamsRequest): Promise<boolean>;
   deleteOrganizeExam(id:number):Promise<boolean>;
   getListExamSubjectOrganize(examId:number):Promise<ExamSubjectOrganizeStatus[]>;
   getOrganizeExamFinish(id:number):Promise<OrganizeExamFinish>;
   updateOrganizeExamStatus(id:number,totalSubject:number):Promise<boolean>;
   getOrganizeExamStatus(id:number):Promise<string>;
}
import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS, Logger } from "../../..";
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { TeacherAction } from "../../user-exam/requests/teacher-exam-request";
import { UserAction } from "../../user-exam/requests/user-action-request";
import { ExamSchool } from "../models/ExamSchool";
import { ExamSchoolInfo } from "../models/ExamSchoolInfo";
import { ExamSubjectOrganize, ExamSubjectOrganizeStatus } from "../models/ExamSubjectOrganize";
import { OrganizeExamFinish } from "../models/organize-exam-finish";
import { OrganizeExamDetail } from "../models/OrganizeExamDetail";
import { School } from "../models/School";
import { ShiftInfo } from "../models/ShiftInfo";
import { AssignSchoolRequest } from "../request/assign-school-request";
import { AssignStudentRequest } from "../request/assign-student-request";
import { AssignStudentsToShiftExamsRequest } from "../request/assign-students-shiftexams";
import { AssignSupervisorRequest } from "../request/assign-supervisor-request";
import { AssignTeacherRequest } from "../request/assign-teacher-request";
import { CreateOrganizeExamRequest } from "../request/create-organize-exam-request";
import { AddShiftInfoRequest, ShiftInfoAdd } from "../request/generate-shift-request";
import { GetListExamSchoolPagingRequest } from "../request/get-list-exam-school-paging-request";
import { GetSchoolRequest } from "../request/get-school-request";
import { UpdateOrganizeExamRequest } from "../request/update-organize-exam-request";
import { ShiftInfoUpdateRequest } from "../request/update-shift-request";

import { OrganizeExamAPI } from "./organize-exam-api";
var qs = require('qs');

export class OrganizeExamEndpoint implements OrganizeExamAPI {
  endpoint = '';

  private baseUrl: string | undefined;
  logger: Logger = Logger.getInstance();

  constructor() {
    this.baseUrl = CLS.getConfig().apiBaseUrl;
  }
  updateOrganizeExam(request: UpdateOrganizeExamRequest): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .post(`${this.baseUrl}/api/OrganizeExams/UpdateOrganizeExam`,
          { id: request.id, totalShift: request.totalShift, totalRoom: request.totalRoom, startShiftDate: request.startShiftDate, breakTime: request.breakTime }
        )
        .then((resp: AxiosResponse<boolean>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }

  getOrganizeExamDetail(id: number): Promise<OrganizeExamDetail> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .get(`${this.baseUrl}/api/OrganizeExams/GetOrganizeExamDetail`, {
          params: { id: id }
        })
        .then((resp: AxiosResponse<OrganizeExamDetail>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  getListSubjectOrganize(id: number): Promise<ExamSubjectOrganize[]> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .get(`${this.baseUrl}/api/exams/${id}/examsubjectorganize`)
        .then((resp: AxiosResponse<ExamSubjectOrganize[]>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  getListExamSchoolPaging(request: GetListExamSchoolPagingRequest): Promise<PaginatorResponse<ExamSchool>> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .post(`${this.baseUrl}/api/exams/examschool`,
          {
            keyword: request.keyword, type: request.type, fromDate: request.fromDate, toDate: request.toDate,
            pageNumber: request.pageNumber, pageSize: request.pageSize, sortField: request.sortField, sortDirection: request.sortDirection
            , gradeId: request.gradeId, createLevel: request.createLevel, listIds: request.listIds
          }
        )
        .then((resp: AxiosResponse<PaginatorResponse<ExamSchool>>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  getListExamSchoolInfo(): Promise<ExamSchoolInfo[]> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .get(`${this.baseUrl}/api/OrganizeExams/GetListExamSchoolInfo`)
        .then((resp: AxiosResponse<ExamSchoolInfo[]>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  getSchools(request: GetSchoolRequest): Promise<PaginatorResponse<School>> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .get(`${this.baseUrl}/api/OrganizeExams/GetSchoolOrganizeExam`, {
          params: { divisionId: request.divisionId, examId: request.examId, gradeId: request.gradeId, keyWord: request.keyWord, page: request.page, size: request.size, sortField: request.sortField, sortDirection: request.sortDirection, getCount: true }
        })
        .then((resp: AxiosResponse<PaginatorResponse<School>>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  assignExamToSchools(request: AssignSchoolRequest): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .post(`${this.baseUrl}/api/OrganizeExams/AssignExamToSchools`,
          { examId: request.examId, listSchools: request.listSchools }
        )
        .then((resp: AxiosResponse<boolean>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  updateOrganizeExamStep(step: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .get(`${this.baseUrl}/api/OrganizeExams/UpdateOrganizeExamStep`, {
          params: { step: step }
        })
        .then((resp: AxiosResponse<boolean>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  getListOrganizeExamInfo(organizeExamId: number): Promise<ShiftInfo[]> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .get(`${this.baseUrl}/api/OrganizeExams/GetListOrganizeExamInfo`, {
          params: { organizeExamId: organizeExamId }
        })
        .then((resp: AxiosResponse<ShiftInfo[]>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  deleteOrganizeExamInfo(listIds: number[]): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .post(`${this.baseUrl}/api/OrganizeExams/DeleteOrganizeExamInfo`,
          { listIds: listIds }
        )
        .then((resp: AxiosResponse<boolean>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  addOrganizeExamInfo(request: AddShiftInfoRequest): Promise<ShiftInfo[]> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .post(`${this.baseUrl}/api/OrganizeExams/AddListOrganizeExamInfo`,
          { subjectId: request.subjectId, examId: request.examId, gradeId: request.gradeId, organizeExamId: request.organizeExamId, listOrganizeExamInfo: request.listOrganizeExamInfo }
        )
        .then((resp: AxiosResponse<ShiftInfo[]>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  createOrganizeExam(request: CreateOrganizeExamRequest): Promise<number> {

    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .post(`${this.baseUrl}/api/OrganizeExams/CreateOrganizeExam`,
          { subjectId: request.subjectId, examId: request.examId, gradeId: request.gradeId, examType: request.examType, startDate: request.startDate, endDate: request.endDate }
        )
        .then((resp: AxiosResponse<number>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  genarateListOrganizeExamInfo(request: AddShiftInfoRequest): Promise<ShiftInfo[]> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .post(`${this.baseUrl}/api/OrganizeExams/GenarateListOrganizeExamInfo`,
          { subjectId: request.subjectId, examId: request.examId, gradeId: request.gradeId, OrganizeExamId: request.organizeExamId, listOrganizeExamInfo: request.listOrganizeExamInfo, listOldId: request.listOldId }
        )
        .then((resp: AxiosResponse<ShiftInfo[]>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  updateListOrganizeExamInfo(request: ShiftInfo[]): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .post(`${this.baseUrl}/api/OrganizeExams/UpdateListOrganizeExamInfo`,
          { ListUpdateOrganizeExamInfoRequest: request }
        )
        .then((resp: AxiosResponse<boolean>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }

  assignStudentsToExam(assignRequest: AssignStudentRequest): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .post(`${this.baseUrl}/api/OrganizeExams/AddListExamSubjectStudent`, assignRequest
        )
        .then((resp: AxiosResponse<boolean>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });

  }
  assignSupervisorsToExam(assignRequest: AssignSupervisorRequest): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .post(`${this.baseUrl}/api/OrganizeExams/AddListExamSubjectSupervisor`, assignRequest
        )
        .then((resp: AxiosResponse<boolean>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  assignTeachersToExam(assignRequest: AssignTeacherRequest): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .post(`${this.baseUrl}/api/OrganizeExams/AddListExamSubjectTeacher`, assignRequest
        )
        .then((resp: AxiosResponse<boolean>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  assignStudentsToShiftExams(assignRequest: AssignStudentsToShiftExamsRequest): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .post(`${this.baseUrl}/api/OrganizeExams/AddListStudentToShiftExams`, assignRequest
        )
        .then((resp: AxiosResponse<boolean>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  deleteOrganizeExam(id: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .delete(`${this.baseUrl}/api/OrganizeExams/${id}`
        )
        .then((resp: AxiosResponse<boolean>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  getListExamSubjectOrganize(examId: number): Promise<ExamSubjectOrganizeStatus[]> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .get(`${this.baseUrl}/api/OrganizeExams/GetListExamSubjectOrganize`, {
          params: { examId: examId }
        })
        .then((resp: AxiosResponse<ExamSubjectOrganizeStatus[]>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  getOrganizeExamFinish(id: number): Promise<OrganizeExamFinish> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .get(`${this.baseUrl}/api/OrganizeExams/GetOrganizeDetailFinish`, {
          params: { id: id }
        })
        .then((resp: AxiosResponse<OrganizeExamFinish>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  updateOrganizeExamStatus(id: number, totalSubject: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      axios
        .post(`${this.baseUrl}/api/OrganizeExams/UpdateOrganizeExamStatus`,
          { id: id, totalSubject: totalSubject }
        )
        .then((resp: AxiosResponse<boolean>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  getOrganizeExamStatus(id: number): Promise<string> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL
      axios
        .get(`${this.baseUrl}/api/OrganizeExams/${id}/status`
        )
        .then((resp: AxiosResponse<string>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
}
import { ExamStatus } from "../../exam/enums/exam-status";

export interface ExamSubjectOrganize{
    id?:number;
    examId: number;
    examType: string;
    subjectId: number;
    subjectName: string;
    startDate: Date;
    endDate: Date;
    getStartDate: string;
    getEndDate: string;
    status?:string;
}
export interface ExamSubjectOrganizeStatus{
    id:number;
    subjectId:number;
    status?:string;
}
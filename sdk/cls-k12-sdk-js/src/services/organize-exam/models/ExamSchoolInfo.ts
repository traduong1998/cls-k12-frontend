export interface ExamSchoolInfo {
    examId: number;
    schoolId: number;
    status?: string;
    isDeleted: boolean;
}
export interface OrganizeExamDetail {

  id: number;
  examType: string;
  testTime: number;
  subjectName: string;
  startDate: Date;
  endDate:Date;
  startShiftDate:Date;
  totalRoom: number;
  totalShift: number;
  totalStudent: number;
  breakTime:number;
  status:string;
}
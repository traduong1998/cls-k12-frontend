export interface OrganizeExamFinish {
    totalRoom:number;
    totalShift:number;
    totalStudent:number;
    totalSupervisor:number;
    totalTeacher:number;
    status:string;
  }
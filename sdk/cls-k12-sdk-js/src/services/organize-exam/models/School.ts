export interface School{
    id:number;
    schoolCode:string;
    schoolName:string;
    isChoose:boolean;
    action:string;
}
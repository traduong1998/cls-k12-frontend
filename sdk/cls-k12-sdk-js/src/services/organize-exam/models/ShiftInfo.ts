export interface ShiftInfo {
    id:number;
    shiftName: string;
    roomName: string;
    realStudent:number;
    maximumStudent:number;
    startDate:Date;
    endDate:Date;
    action?:string;
  }
  
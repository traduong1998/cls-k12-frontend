export interface ExamSchool {
    id: number;
    name: string;
    status?: string;
    gradeId:number;
    getStatus?:string;
    type:string;
    gradeName:string;
    startDate:Date;
    endDate:Date;

}
export interface ShiftInfoDTO {
    id?:number;
    organizeExamId:number;
    shiftName: string;
    roomName: string;
    realStudent?:number;
    startDate:Date;
    endDate:Date;
    fromRecord?: number;
    size?:number;
    isFinish?:boolean;
  }
 
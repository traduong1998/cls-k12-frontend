import { TeacherAction } from "../../user-exam/requests/teacher-exam-request";

export interface AssignSupervisorRequest {
    users: TeacherAction[];
    examId: number;
    gradeId: number;
    subjectId: number;
    organizeExamId: number;
}
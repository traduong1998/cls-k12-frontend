
export interface GetSchoolRequest {
    divisionId?: number;
    examId:number;
    gradeId:number;
    keyWord: string;
    page?: number;
    size?: number;
    sortField?: string;
    sortDirection?: string;
}
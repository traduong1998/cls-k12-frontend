import { UserAction } from "../../user-exam/requests/user-action-request";

export interface AssignStudentRequest{
    users: UserAction[];
    examId:number;
    gradeId:number;
    subjectId:number;
    organizeExamId: number;
}
import { TeacherAction } from "../../user-exam/requests/teacher-exam-request";

export interface AssignTeacherRequest {
    listExamSubjectTeacher: TeacherAction[];
    examId: number;
    gradeId: number;
    subjectId: number;
    organizeExamId: number;
}
export interface CreateOrganizeExamRequest{
    subjectId:number;
    examId:number;
    gradeId:number;
    examType:string;
    startDate:Date;
    endDate:Date;
}
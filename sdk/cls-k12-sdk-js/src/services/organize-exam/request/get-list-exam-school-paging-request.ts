export interface GetListExamSchoolPagingRequest {
    keyword: string;
    type: string;
    fromDate?: Date;
    toDate?: Date;
    createLevel: string;
    gradeId?: number;
    pageNumber?: number;
    pageSize?: number;
    sortField?: string;
    sortDirection?: string;
    listIds:number[];

}
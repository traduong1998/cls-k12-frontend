export interface UpdateOrganizeExamRequest{
    id: number;
    breakTime: number;
    totalShift: number;
    totalRoom: number;
    startShiftDate: Date;
}
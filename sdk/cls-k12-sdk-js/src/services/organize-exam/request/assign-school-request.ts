export interface AssignSchoolRequest{
    listSchools:SchoolAction[];
    examId:number;
}
export interface SchoolAction{
    schoolId:number;
    action:string;
}
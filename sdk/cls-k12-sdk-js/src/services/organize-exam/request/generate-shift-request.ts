export interface AddShiftInfoRequest{
    listOldId?:number[];
    organizeExamId:number;
    examId:number;
    subjectId:number;
    gradeId:number;
    listOrganizeExamInfo:ShiftInfoAdd[];
}
export interface ShiftInfoAdd {
    id:number;
    shiftName: string;
    roomName: string;
    realStudent:number;
    maximumStudent:number;
    startDate:Date;
    endDate:Date;
  }
  
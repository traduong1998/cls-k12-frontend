
export interface ShiftInfoUpdateRequest{
    listOrganizeExamInfo: ShiftInfoUpdate[];
}
export interface ShiftInfoUpdate {
    OrganizeExamInfoId: number;
    realStudent:number;
    startDate:Date;
    endDate:Date;
  }
  
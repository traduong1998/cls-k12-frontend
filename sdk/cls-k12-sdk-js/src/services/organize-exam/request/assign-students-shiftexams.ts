export interface AssignStudentsToShiftExamsRequest {
    organizeExamId: number;
    shiftExams: ShiftExamAssignStudentInfo[];
}

export interface ShiftExamAssignStudentInfo {
    index: number;
    organizeExamInfoId: number;
    numberOfStudent: number;
}
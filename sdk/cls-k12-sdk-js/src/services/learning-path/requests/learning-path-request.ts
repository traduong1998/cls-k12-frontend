
export interface LearningPathRequest {
    id?: number;
    name: string;
    type?: string;
    startDate?: Date;
    endDate?: Date;
    action?: string;
    createLearningPathStepCommands?: CreateLearningPathStepCommands[],
    updateLearningPathStepCommands?: CreateLearningPathStepCommands[]
}

export interface CreateLearningPathStepCommands {
    id?: number
    name: string,
    startDate?: Date,
    endDate?: Date,
    action?: string,
    courses?: Courses[]
}

export interface Courses {
    id?: number,
    action?: string,
}
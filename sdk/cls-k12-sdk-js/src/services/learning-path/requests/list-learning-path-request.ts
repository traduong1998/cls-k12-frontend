export interface ListLearningPathRequest {
    keyword?: string;
    page: number;
    size: number;
    sortField: string;
    sortDirection: string;
}
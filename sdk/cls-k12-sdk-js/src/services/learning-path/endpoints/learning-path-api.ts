import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { LearningPathModel } from "../models/learning-path-model";
import { LearningPathRequest } from "../requests/learning-path-request";
import { ListLearningPathRequest } from "../requests/list-learning-path-request";
import { ListLearningPathResponse } from "../responses/list-learning-path-response";


export interface LearningPathAPI {
    getListPagging(request?: ListLearningPathRequest): Promise<PaginatorResponse<ListLearningPathResponse>>;

    getById(id: number): Promise<LearningPathModel[]>;

    create(learningPath: LearningPathRequest): Promise<boolean>;
    update(learningPath: LearningPathRequest): Promise<boolean>;

    removeLearningPath(id: number): Promise<boolean>
}


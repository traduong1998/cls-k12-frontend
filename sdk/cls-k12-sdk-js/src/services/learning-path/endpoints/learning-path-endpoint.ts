import axios, { AxiosResponse } from "axios";
import { CLS, Logger } from "../../..";
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { LearningPathModel } from "../models/learning-path-model";
import { LearningPathRequest } from "../requests/learning-path-request";
import { ListLearningPathRequest } from "../requests/list-learning-path-request";
import { ListLearningPathResponse } from "../responses/list-learning-path-response";
import { LearningPathAPI } from "./learning-path-api";


export class LearningPathEndpoint implements LearningPathAPI {
    // TODO : Endpoint :
    endpoint = '/v1/learn';

    private baseUrl: string;
    logger: Logger = Logger.getInstance();

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    create(learningPath: LearningPathRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL `http://localhost:65000/api/v1/LearningPaths/Create`
            axios.post(`${this.baseUrl}/LearningPaths/Create`, learningPath)
                .then((resp: AxiosResponse<boolean>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getById(id: number): Promise<LearningPathModel[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/LearningPaths/${id}`)
                .then((resp: AxiosResponse<LearningPathModel[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getListPagging(request?: ListLearningPathRequest): Promise<PaginatorResponse<ListLearningPathResponse>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/LearningPaths`, { params: request })
                .then((resp: AxiosResponse<PaginatorResponse<ListLearningPathResponse>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    update(request: LearningPathRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/LearningPaths/Update`, request)
                .then((resp: AxiosResponse<boolean>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    removeLearningPath(id: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/LearningPaths/${id}/Delete`, id)
                .then((resp: AxiosResponse<boolean>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
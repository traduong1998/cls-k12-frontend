

export interface ListLearningPathResponse{
    id:number;
    name:string;
    type:string;
    startDate?:Date;
    endDate?:Date
}
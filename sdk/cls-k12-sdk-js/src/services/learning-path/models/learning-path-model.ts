
export interface LearningPathModel {
    id: number
    name: string
    type: string
    startDate?: Date
    endDate?: Date
    learningPathStepId?: number
    learningPathStepName?: string
    learningPathStepStartDate?: Date
    learningPathStepEndDate?: Date
    learningPathStepLesson_learningPathStepId?: number
    lessonId?: number
}
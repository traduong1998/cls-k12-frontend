import { GetStudentLogsPagingRequest, GetStudentLogsPagingResponse } from "./meeting-room-student-logs-endpoint";
import {MeetingRoomStudentLogsModel} from "./../models/meeting-room-student-logs-model"

export interface MeetingRoomStudentLogsAPI {
    create(meetingRoomId: number): Promise<MeetingRoomStudentLogsModel>;
}
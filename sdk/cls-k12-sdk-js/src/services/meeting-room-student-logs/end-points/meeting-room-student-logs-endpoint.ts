import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS, Logger } from "../../..";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { MeetingRoomStudentLogsModel } from "../models/meeting-room-student-logs-model";
import { MeetingRoomStudentLogsResponse } from "../models/meeting-room-student-logs-response";
import { MeetingRoomStudentLogsAPI } from "./meeting-room-student-logs-api";

export interface GetStudentLogsPagingRequest {
    meetingRoomId?: number
    divisionId?: number
    schoolId?: number
    gradeId?: number
    groupStudentId?: number
    keyword?: string
    getCount?: boolean
    page: number
    size: number
    sortField: string,
    sortDirection: string
}

export interface GetStudentLogsPagingResponse {
    data: PaginatorResponse<MeetingRoomStudentLogsResponse>;
    studentJoinMeetingRoomCount: number;
}
export class MeetingRoomStudentLogsEndpoint implements MeetingRoomStudentLogsAPI {
    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();
    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api'

    }
    create(meetingRoomId: number): Promise<MeetingRoomStudentLogsModel> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/MeetingRooms/${meetingRoomId}/student/logs`, { meetingRoomId: meetingRoomId })
                .then((resp: AxiosResponse<MeetingRoomStudentLogsModel>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getAllPaging(request: GetStudentLogsPagingRequest): Promise<GetStudentLogsPagingResponse> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/MeetingRooms/${request.meetingRoomId}/student/logs`, { params: request })
                .then((resp: AxiosResponse<GetStudentLogsPagingResponse>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    exportExcel(request: GetStudentLogsPagingRequest): Promise<string> {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.baseUrl}/MeetingRooms/${request.meetingRoomId}/student/logs/actions/export`,
                    { params: request, responseType: 'blob' })
                .then((resp: AxiosResponse<string>) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
}
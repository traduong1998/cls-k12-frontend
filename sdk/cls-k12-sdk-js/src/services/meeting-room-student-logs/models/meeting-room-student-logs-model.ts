export interface MeetingRoomStudentLogsModel {
    meetingRoomId: number
    userId: number
    startDate: Date
    endDate?: Date
    joinDate?: string
    portalId: number
    divisionId?: number
    schoolId?: number
    joinDates?: Date[]
    lastJoinDate?: Date
}
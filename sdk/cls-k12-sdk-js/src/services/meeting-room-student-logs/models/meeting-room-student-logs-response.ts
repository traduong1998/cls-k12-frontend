export interface MeetingRoomStudentLogsResponse {
    userId?: number
    userName: string
    lastName?: string
    firstName?: string
    schoolId?: number
    schoolName?: string
    gradeId?: number
    gradeName?: string
    groupStudentId?: number
    groupStudentName?: string
    joinDate?: string
    joinDateLocalTime?: Date[]
    joinDateStudentTitleString?: string
    startDate?: Date
    endDate?: Date
}
export interface ShiftInfo {
    examId: number;
    subjectId: number;
    shiftId: number;
}
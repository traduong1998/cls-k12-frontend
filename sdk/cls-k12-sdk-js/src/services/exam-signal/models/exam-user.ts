import { ShiftInfo } from "./shift-info";

/**Thí sinh */
export interface ExamUser {
    userId: number;
    fullName: string;
    shiftInfo: ShiftInfo;
    groupName: string;
    connectionIds: string[];
    status: string;
    avatar?: string;
    examSubjectStudentId: number;
}
import { DeviceInfo } from "../models/device-info";

export interface RegisterSupervision {
    examId: number;
    subjectId: number;
    shiftId: number;
    device: DeviceInfo;
}
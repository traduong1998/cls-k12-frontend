export interface ProcessViolateRequest {
    contestantId: number;
    violationType: string;
    reason: string;
    minusScore?: number;
}
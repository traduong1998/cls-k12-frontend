// const signalR = require("@microsoft/signalr");
import * as signalR from "@microsoft/signalr";
import { CLS } from "../../../CLS";
import { ExamUser } from "../models/exam-user";
import { ProcessViolateRequest } from "../requests/process-violate-request";
import { RegisterSupervision } from "../requests/register-supervision";

/**[Hub] Giám sát thi */
export class ExamSignalSupervisionHub {
    private baseUrl: string;
    connection: signalR.HubConnection;

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrls.examSignal ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl += '/supervision-exam-hub';

        console.log(`CLS.getConfig().token`, CLS.getConfig().token);
        var token = CLS.getConfig().token;

        this.connection = new signalR.HubConnectionBuilder()
            .withUrl(this.baseUrl, { accessTokenFactory: () => token })
            .withAutomaticReconnect()
            .configureLogging(signalR.LogLevel.Debug)
            .build();

    }

    start(): Promise<void> {
        return this.connection.start();
    }

    dispose() {
        if (this.connection)
            this.connection.stop();
    }

    /**Đăng ký giám sát */
    registerSupervision(request: RegisterSupervision): Promise<void> {
        return this.connection.invoke("registerSupervision", request);
    };

    /**Lấy danh sách thí sinh tham dự */
    getContestantsOnline(): Promise<ExamUser[]> {
        return this.connection.invoke("getContestantsOnline");
    };

    /**Xử lý vi phạm */
    processViolate(request: ProcessViolateRequest): Promise<void> {
        return this.connection.invoke("processViolate", request);
    };

    /**Thu bài thí sinh */
    collectTestOfContestant(request: { contestantId: number }): Promise<void> {
        return this.connection.invoke("collectTestOfContestant", request);
    };
    
    /**Thu bài */
    collectTest(): Promise<void> {
        return this.connection.invoke("collectTest");
    };
}
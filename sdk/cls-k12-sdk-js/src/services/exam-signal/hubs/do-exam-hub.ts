// const signalR = require("@microsoft/signalr");
import * as signalR from "@microsoft/signalr";
import { CLS } from "../../../CLS";
import { DeviceInfo } from "../models/device-info";

/**[Hub] Thực hiện thi */
export class DoExamHub {
    private baseUrl: string;
    connection;

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrls.examSignal ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl += '/participant-exam-hub';

        var token = CLS.getConfig().token;

        this.connection = new signalR.HubConnectionBuilder()
            .withUrl(this.baseUrl, { accessTokenFactory: () => token })
            .withAutomaticReconnect()
            .configureLogging(signalR.LogLevel.Debug)
            .build();

    }

    /**
     * Bắt đầu kết nối đến signal
     * @returns 
     */
    start(): Promise<void> {
        console.log(`hub start, this.connection.state`, this.connection.state);
        return this.connection.start();
    }

    dispose() {
        if (this.connection) {
            console.warn(`stop hub`);
            this.connection.stop();
        }
    }

    /**
     * Vào phòng 
     * */
    joinRoom(request: { examId: number, subjectId: number, shiftId: number, device: DeviceInfo }): Promise<void> {
        return this.connection.invoke("joinRoom", request);
    };

    /**Nộp bài */
    submit() {
        return this.connection.invoke("submit");
    }

}
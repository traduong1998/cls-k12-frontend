/**Hình thức xử lý vi phạm trong lúc thi */
export enum ViolationTypes {
    /**Trừ điểm */
    SubtractScore = 'SUB',

    /**Cảnh cáo */
    Warning = 'WAR',

    /**Đình chỉ */
    Suspend = 'SUS'
}
import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS, Logger } from "../..";
import { ReportLessonModel } from "./models/report-lesson-model";
import { ReportLessonApi } from "./report-lesson-api";
import { ReportLearnPagingRequest } from "./models/ReportLearnPagingRequest";
import { PaginatorResponse } from "../../core/api/responses/PaginatorResponse";

export class ReportLessonEnpoint implements ReportLessonApi {

    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
    }
    exportLessonReport(divisionId: number, schoolId: number, gradeId: number, subjectId: number, teacherId: number, lessonName: string): Promise<string> {
        return new Promise((resolve, reject) => {

            axios.get(`${this.baseUrl}/api/reports/student/exportExcelReportLesson`,
                { params: { divisionId: divisionId, schoolId: schoolId, gradeId: gradeId, subjectId: subjectId, teacherId: teacherId, lessonName: lessonName }, responseType: 'blob' })
                .then((resp) => {
                    debugger;
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error: AxiosError) => {
                    console.log("report URL: ", error);
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getLessonReport(filter: ReportLearnPagingRequest): Promise<PaginatorResponse<ReportLessonModel>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/reports/student/reportLesson`, {
                    params: {
                        divisionId: filter.divisionId,schoolId:filter.schoolId,gradeId:filter.gradeId,teacherId:filter.teacherId,subjectId:filter.subjectId,
                        lessonName: filter.lessonName, sortField: filter.sortField, sortDirection: filter.sortDirection,
                        page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<ReportLessonModel>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

}

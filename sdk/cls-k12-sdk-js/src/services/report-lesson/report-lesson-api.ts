import { PaginatorResponse } from "../../core/api/responses/PaginatorResponse";
import { ReportLessonModel } from "./models/report-lesson-model";
import { ReportLearnPagingRequest } from "./models/ReportLearnPagingRequest";

export interface ReportLessonApi{
    getLessonReport(filter: ReportLearnPagingRequest): Promise<PaginatorResponse<ReportLessonModel>>
    exportLessonReport(divisonId:number, schoolId:number, gradeId:number, subjectId:number,teacherId:number, lessonName:string):Promise<string>

}
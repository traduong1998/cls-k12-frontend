import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class ReportLearnPagingRequest implements PaginatorRequest {
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  divisionId?: number;
  schoolId?: number;
  gradeId?: number;
  teacherId?: number;
  subjectId?: number;
  sortField?: string;
  sortDirection?: string;
  getCount?: boolean;
  lessonName?: string;
}

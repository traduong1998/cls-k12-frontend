import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class ReportStudentPagingRequest implements PaginatorRequest {
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  divisionId?: number;
  schoolId?: number;
  gradeId?: number;
  groupStudentId?: number;
  schoolYear?: number;
  sortField?: string;
  sortDirection?: string;
  getCount?: boolean;
  studentName?: string;
  
}



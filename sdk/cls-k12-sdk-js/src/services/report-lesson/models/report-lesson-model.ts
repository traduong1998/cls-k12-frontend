export interface ReportLessonModel{
    lessonId: number,
    lessonName: string,
    teacherId: number,
    teacherName: string,
    totalStudentAgg: number,
    totalStudentComplete: number
}
import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class ReportStudentLearnPagingRequest implements PaginatorRequest {
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  divisionId?: number;
  schoolId?: number;
  gradeId?: number;
  classId?: number;
  lessonId?: number;
  schoolYear?: number;
  sortField?: string;
  sortDirection?: string;
  getCount?: boolean;
  studentName?: string;
}



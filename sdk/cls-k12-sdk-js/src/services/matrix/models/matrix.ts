export interface Matrix {
    id?: number,
    name: string,
    numberTest: number,
    gradeId: number,
    subjectId: number,
    matrixDetails: MatrixDetail[]
}

export interface MatrixDetail {
    id?: number,
    contentPanelId: number,
    chapterId: number,
    unitId: number,
    formatQuestion: string
    typeOfQuestion: string,
    level: string,
    totalQuestion: number,
    totalScore: number
    matrixId: number,
    action: string
}
import { Matrix } from "../models/matrix";

export interface MatrixAPI {
    getMatrix(id: number): Promise<Matrix>;
    createMatrix(matrix: Matrix): Promise<Matrix>;
    updateMatrix(matrix: Matrix): Promise<Matrix>;
}

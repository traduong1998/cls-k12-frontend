import axios, { AxiosError, AxiosResponse } from "axios";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { CLS } from "../../../CLS";
import { ErrorResponse } from "../../../core/api/responses/ErrorResponse ";
import { Logger } from "../../../Logger";
import { Matrix } from "../models/matrix";
import { MatrixAPI } from "./matrix-api";


export class MatrixEndpoint implements MatrixAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        console.log(`CLS.getConfig().apiBaseUrl `, CLS.getConfig().apiBaseUrl);
        console.log(` config.baseUrl`, config?.baseUrl);
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        //this.baseUrl = config.baseUrl;
    }

    getMatrix(id: number): Promise<Matrix> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/matrices/` + id)
                .then((resp: AxiosResponse<Matrix>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    createMatrix(matrix: Matrix): Promise<Matrix> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/api/matrices`, matrix)
                .then((resp: AxiosResponse<Matrix>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    updateMatrix(matrix: Matrix): Promise<Matrix> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .patch(`${this.baseUrl}/api/matrices`, matrix)
                .then((resp: AxiosResponse<Matrix>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
}

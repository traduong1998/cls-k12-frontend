export interface DataQuestionInfoImport {
    portalId?: number;
    divisionId?: number;
    schoolId?: number;
    gradeId?: number;
    subjectId?: number;
    contentPanelId?: number;
    chapterId?: number;
    unitId?: number;
}
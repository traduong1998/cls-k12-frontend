export interface DataRequestImportQuestions {
    portalId?: number;
    divisionId?: number;
    schoolId?: number;
    gradeId?: number;
    subjectId?: number;
    contentPanelId?: number;
    chapterId?: number;
    unitId?: number;
    questions: QuestionImport[];
    autoRequestApproval: boolean;
}
export interface QuestionImport {
    content?: string;
    type?: string;
    level?: string;
    format?: string;
    questions?: any;
    answers?: Answer[];
}
interface Answer {
    id?: number;
    questionId?: number;
    content?: string;
    trueAnswer?: number;
    position?: boolean;
    groupOfFillBlank2?: number;
    isShuffler?: boolean;
}

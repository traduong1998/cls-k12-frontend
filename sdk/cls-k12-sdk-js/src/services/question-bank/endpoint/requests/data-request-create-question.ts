
export interface DataRequestCreateQuestion {
    content?: string;
    type?: string;
    level?: string;
    format?: string;
    portalId?: number;
    divisionId?: number;
    schoolId?: number;
    gradeId?: number;
    subjectId?: number;
    contentPanelId?: number;
    chapterId?: number;
    unitId?: number;
    isShuffle?: true;
    questions?: any;
    answers?: Answer[];
}

interface Answer {
    id?: number;
    questionId?: number;
    content?: string;
    trueAnswer?: number;
    position?: boolean;
    groupOfFillBlank2?: number;
    isShuffler?: boolean;
}



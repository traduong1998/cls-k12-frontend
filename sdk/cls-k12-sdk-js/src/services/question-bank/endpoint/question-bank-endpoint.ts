import { ErrorResponse } from './../../../core/api/responses/ErrorResponse ';
import { AxiosError } from 'axios';
import { CLS } from '../../../CLS';
import axios, { AxiosResponse } from 'axios';
import { PaginatorResponse } from '../../../core/api/responses/PaginatorResponse';
import { QuestionBankAPI } from './question-bank-api';
import { ListQuestionBankResponse } from './responses/question-bank-response';
import { ListAdminAndTeachers } from './responses/list-admin-and-teachers';
import { DataRequestCreateQuestion } from './requests/data-request-create-question';
import { Matrix } from '../../matrix/models/matrix';
import { DataRequestImportQuestions } from './requests/data-request-import-questions';

export class QuestionBankEndpoint implements QuestionBankAPI {
    private baseUrl: string | undefined;
    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }

    // Get all list question
    getListQuestionFilter(informationFilter: any): Promise<PaginatorResponse<ListQuestionBankResponse>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/questions`, { params: informationFilter })
                .then((resp: AxiosResponse<PaginatorResponse<ListQuestionBankResponse>>) => {
                    resolve(resp.data);
                }).catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getAdminAndTeachers(informationFilter: any): Promise<PaginatorResponse<ListAdminAndTeachers>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/users/getadminandteacher`, { params: informationFilter })
                .then((resp: AxiosResponse<PaginatorResponse<ListAdminAndTeachers>>) => {
                    resolve(resp.data);
                }).catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getQuestionDetail(id: number): Promise<ListQuestionBankResponse> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/questions/${id}`)
                .then((resp: AxiosResponse<ListQuestionBankResponse>) => {
                    resolve(resp.data);
                }).catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    deleteQuestion(id: number[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/questions/delete`, { ids: id }).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            }).catch((error: AxiosError<ErrorResponse>) => {
                reject(error.response?.data);
            });
        });
    }

    requestApproveQuestions(id: number[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/questions/request-approval`, { ids: id }).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            }).catch((error: AxiosError<ErrorResponse>) => {
                reject(error.response?.data);
            });
        });
    }

    approveQuestions(id: number[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/questions/approve`, { approvalQuestions: id }).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            }).catch((error: AxiosError<ErrorResponse>) => {
                reject(error.response?.data);
            });
        });
    }

    refuseQuestion(id: number[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/questions/refuse`, { refuseQuestions: id }).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            }).catch((error: AxiosError<ErrorResponse>) => {
                reject(error.response?.data);
            });
        });
    }

    createQuestion(dataRequest: DataRequestCreateQuestion, isAutoApprove: boolean): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/questions/create`, { question: dataRequest,  autoRequestApproval: isAutoApprove}).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            }).catch((error: AxiosError<ErrorResponse>) => {
                reject(error.response?.data);
            });
        });
    }

    updateQuestion(dataRequest: DataRequestCreateQuestion, isAutoApprove: boolean): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/questions/update`, { question: dataRequest, autoRequestApproval: isAutoApprove }).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            }).catch((error: AxiosError<ErrorResponse>) => {
                reject(error.response?.data);
            });
        });
    }
    getTestsByMatrix(matrix: Matrix): Promise<any[]> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/questions/get-test-by-matrix`, matrix)
                .then((resp: AxiosResponse<any>) => {
                    console.log(resp)
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getQuestionByIds(ids: number[]): Promise<any> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/questions/get-question-by-ids`, {
                ids: ids
            })
                .then((resp: AxiosResponse<any>) => {
                    console.log(resp)
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    importQuestions(dataRequest: DataRequestImportQuestions): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/questions/import-questions`, dataRequest).then((res: AxiosResponse<boolean>) => {
                resolve(res.data);
            }).catch((error: AxiosError<ErrorResponse>) => {
                reject(error.response?.data);
            });
        });
    }
}

import { DataRequestCreateQuestion } from './requests/data-request-create-question';
import { ListAdminAndTeachers } from './responses/list-admin-and-teachers';
import { PaginatorResponse } from '../../../core/api/responses/PaginatorResponse';
import { Matrix } from '../../matrix/models/matrix';
import { ListQuestionBankResponse } from './responses/question-bank-response';
import { DataRequestImportQuestions } from './requests/data-request-import-questions';
export interface QuestionBankAPI {
    getListQuestionFilter(informationFilter: any): Promise<PaginatorResponse<ListQuestionBankResponse>>;
    getTestsByMatrix(matrix: Matrix): Promise<any[]>;
    getAdminAndTeachers(informationFilter: any): Promise<PaginatorResponse<ListAdminAndTeachers>>;
    getQuestionDetail(id: number): Promise<ListQuestionBankResponse>;
    deleteQuestion(id: number[]): Promise<boolean>;
    approveQuestions(requestData: number[]): Promise<boolean>;
    requestApproveQuestions(id: []): Promise<boolean>;
    refuseQuestion(dataRequest: {}): Promise<boolean>;
    createQuestion(dataRequest: DataRequestCreateQuestion, isAutoApprove: boolean): Promise<boolean>
    updateQuestion(dataRequest: DataRequestCreateQuestion, isAutoApprove: boolean): Promise<boolean>;
    getQuestionByIds(ids: number[]): Promise<any[]>;
    importQuestions(dataRequest: DataRequestImportQuestions): Promise<boolean>
}

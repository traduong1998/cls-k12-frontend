
export interface ListQuestionBankResponse {
    id?: number;
    content?: string;
    type?: string;
    level?: string;
    format?: string;
    createDate?: Date;
    approvalDate?: Date;
    userId?: number;
    questionGroupId?: number;
    answers?: Answer[];
    GradeId?: number;
    SubjectId?: number;
    ChapterId?: number;
    UnitId?: number;
    status?: string;
    questions?: {};
    keyword?: string;
}

interface Answer {
    id?: number;
    questionId?: number;
    content?: string;
    trueAnswer?: number;
    position?: boolean;
    groupOfFillBlank2?: number;
    isShuffler?: boolean;
}



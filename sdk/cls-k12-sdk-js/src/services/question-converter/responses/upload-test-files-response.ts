import { QuestionModel } from "../../test/requests/question-model";

export interface UploadTestFilesReponse {
    name: string;
    questions: QuestionModel[];
}
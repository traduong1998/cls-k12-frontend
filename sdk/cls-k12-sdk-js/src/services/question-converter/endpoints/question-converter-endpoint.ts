import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { UploadTestFilesReponse } from "../responses/upload-test-files-response";
import { QuestionConverterApi } from "./question-converter-api";


export class QuestionConverterEndpoint implements QuestionConverterApi {

    baseUrl: string;
    baseUrlConvertFile?: string;

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrls.questionConverter ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api/v1';

    }

    uploadTestFiles(files: File[], config?: { onUploadProgress: any }, moduleName?: string): Promise<UploadTestFilesReponse> {
        let bodyFormData: FormData = new FormData();
        for (let i = 0; i < files.length; i++) {
            bodyFormData.append("files" + i, files[i]);
        }
        if (!moduleName || moduleName == null) {
            moduleName = "learn";
        }

        bodyFormData.append("moduleName",moduleName);
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/question-converter/actions/parse-from-file`, bodyFormData, {
                onUploadProgress: (progressEvent) => {
                    var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                    console.log(percentCompleted);
                    config?.onUploadProgress(percentCompleted);
                }
            })
                .then((res: AxiosResponse<UploadTestFilesReponse>) => {
                    resolve(res.data);
                }).catch(err => {
                    reject(err);
                });
        })
    }
}
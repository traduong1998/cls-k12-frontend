
import { UploadTestFilesReponse } from "../responses/upload-test-files-response";


export interface QuestionConverterApi {
    uploadTestFiles(id: File[]): Promise<UploadTestFilesReponse>;
}


import { GradeOption } from "../../grade/models/Grade";
import { SubjectOption } from "../../subject/models/Subject";

export interface InitData {
    subjects: GradeOption[],
    grades: SubjectOption[],
} 
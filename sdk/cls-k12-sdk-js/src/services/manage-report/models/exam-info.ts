export interface ExamInfoReport {
    examId: number,
    name: string
}
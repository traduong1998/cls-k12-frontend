export interface ExamTime{
    examSubjectStudentId: number,
    startDate: Date,
    endDate: Date
}
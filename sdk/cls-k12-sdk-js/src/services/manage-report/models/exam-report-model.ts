export interface ExamReportModel {
    id: number,
    examId: number,
    gradeId: number,
    subjectId: number,
    roomName: string,
    shiftName: string,
    startDate:Date,
    endDate: Date,
    gradeName:string,
    subjectName: string,
    examType: string,
    status:string,
    isCompleted: boolean,
    infingeTimes:number,
    scoreDecuted:number,
    score: number
}
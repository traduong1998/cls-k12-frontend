export interface ExamSubjectStudent {
    examSubjectStudentId: number,
    shiftName: string,
    roomName: string
}
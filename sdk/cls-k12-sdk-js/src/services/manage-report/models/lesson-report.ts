export interface LessonReport {
    lessonId:number,
      lessonName: string,
      gradeId: number,
      subjectId: number,
      teacherId: number,
      registrationDate: Date,
      startDate: Date,
      completionDate: Date,
      percentageOfCompletion: number,
      result: number,
      assessmentLevelName: string,
}
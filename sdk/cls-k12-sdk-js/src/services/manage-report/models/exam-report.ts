export interface ExamReport {
    examSubjectStudentId: number,
    examName: string,
    subjectId: number,
    subjectName: string,
    gradeId: number,
    gradeName: string,
    startDate: Date,
    endDate: Date,
    score: number,
    shiftName: string,
    roomName: string,
    isCompleted: boolean,
    isFisnished: boolean
}
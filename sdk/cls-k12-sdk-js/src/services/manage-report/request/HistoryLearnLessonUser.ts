import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";
export class HistoryLearnLessonUser implements PaginatorRequest {
    pageNumber?: number;
    sizeNumber?: number;
    gradeId?: number;
    subjectId?: number;
    teacherId?: number;
    sortField?: string;
    sortDirection?: string;
    getCount?: boolean;
}



export interface GetLessonReportFilterRequests {
    pageNumber: number,
    pageSize: number,
    gradeId: number,
    subjectId: number,
    userCreateId : number,
}

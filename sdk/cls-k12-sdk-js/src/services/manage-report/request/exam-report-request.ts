export interface ExamReportRequest {
    gradeId: number,
    subjectId: number,
    listExamIds: [],
    sortField: string,
    sortDirection: string

    page: number,
    size: number,
    getCount: boolean
}
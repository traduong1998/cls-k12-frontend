import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";
export class DetailHistoryLearnLessonUser implements PaginatorRequest {
    pageNumber?: number;
    sizeNumber?: number;
    userId?: number;
    gradeId?: number;
    subjectId?: number;
    teacherId?: number;
    sortField?: string;
    sortDirection?: string;
    getCount?: boolean;
}



export interface GetExamReportFilterRequests {
    pageNumber: number,
    pageSize: number,
    gradeId: number,
    subjectId: number,
    examName: string,
}

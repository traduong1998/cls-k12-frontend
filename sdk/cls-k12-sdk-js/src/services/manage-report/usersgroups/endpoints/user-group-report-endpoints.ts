import axios, { AxiosResponse } from "axios";
import { CLS, Logger } from "../../../..";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { InforSystemLesson } from "../models/infor-system-lesson";
import { LessonReport } from "../models/lesson-report";
import { UserGroup } from "../models/user-group";
import { GetListStudentForAssignRequest } from "../requests/get-list-student-for-assign-request";
import { HistoryLessonUser } from "../requests/history-lesson-user-request";
import { InforSystemHistoryLesson } from "../requests/info-system-history-lesson-request";
import { StudentForAssignResponse } from "../responses/student-for-assign-response";
import { UsersGroupsReportAPI } from "./users-group-report-api";


export class UserGroupReportEndpoints implements UsersGroupsReportAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = config?.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    getListUserGroup(keyWord: string, page: number, size: number): Promise<PaginatorResponse<UserGroup>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/ReportUserGroups/list-usergroups`, { params: { keyWord: keyWord, page: page, size: size } })
                .then((resp: AxiosResponse<PaginatorResponse<UserGroup>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getListStudentForAssign(request: GetListStudentForAssignRequest): Promise<PaginatorResponse<StudentForAssignResponse>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/ReportUserGroups/list-student-assign`, {
                params: {
                    DivisionId: request.divisionId,
                    SchoolId: request.schoolId,
                    GradeId: request.gradeId,
                    GroupStudentId: request.groupStudentId,
                    UserGroupId: request.userGroupId,
                    SortField: request.sortField,
                    SortDirection: request.sortDirection,
                    Page: request.pageNumber,
                    Size: request.sizeNumber,
                    GetCount: request.getCount
                }
            })
                .then((resp: AxiosResponse<PaginatorResponse<StudentForAssignResponse>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getLearnLesson(userId: number, request: HistoryLessonUser): Promise<PaginatorResponse<LessonReport>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL\ http://localhost:58915/api/v1/ReportUserGroups/users/{userId}/ReportLearnUserLesson
            axios
                .get(`${this.baseUrl}/ReportUserGroups/users/${userId}/ReportLearnUserLesson`, { params: request })
                .then((resp: AxiosResponse<PaginatorResponse<LessonReport>>) => {
                    resolve(resp.data);
                })
                .catch((error) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getLearnInfoSystem(request: InforSystemHistoryLesson): Promise<InforSystemLesson[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL\
            axios
                .post(`${this.baseUrl}/ReportUserGroups/ReportUserGroupLearnGetInfoSystem`, request)
                .then((resp: AxiosResponse<InforSystemLesson[]>) => {
                    resolve(resp.data);
                })
                .catch((error) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    exportLessonReportByUser(userId: number, subjectId: number, gradeId: number, teacherId: number): Promise<string> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL\
            axios
                .get(`${this.baseUrl}/ReportUserGroups/exportExcelStudyHistoryByUser/${userId}`, {
                    params: {
                        SubjectId: subjectId,
                        GradeId: gradeId,
                        UserCreateId: teacherId,
                    },
                    responseType: 'blob',
                })
                .then((resp) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    };
}
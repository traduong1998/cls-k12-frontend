import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { InforSystemLesson } from "../models/infor-system-lesson";
import { LessonReport } from "../models/lesson-report";
import { UserGroup } from "../models/user-group";
import { GetListStudentForAssignRequest } from "../requests/get-list-student-for-assign-request";
import { HistoryLessonUser } from "../requests/history-lesson-user-request";
import { InforSystemHistoryLesson } from "../requests/info-system-history-lesson-request";
import { StudentForAssignResponse } from "../responses/student-for-assign-response";


export interface UsersGroupsReportAPI {

    getListUserGroup(keyWord: string, page: number, size: number): Promise<PaginatorResponse<UserGroup>>

    getListStudentForAssign(request: GetListStudentForAssignRequest): Promise<PaginatorResponse<StudentForAssignResponse>>;

    getLearnLesson(userId: number, request: HistoryLessonUser): Promise<PaginatorResponse<LessonReport>>
    getLearnInfoSystem(request: InforSystemHistoryLesson): Promise<InforSystemLesson[]>
    exportLessonReportByUser(userId: number, subjectId: number, gradeId: number, teacherId: number): Promise<string>
}
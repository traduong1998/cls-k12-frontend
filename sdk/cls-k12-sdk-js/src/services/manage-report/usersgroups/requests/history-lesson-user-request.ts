export interface HistoryLessonUser {
    gradeId: number,
    subjectId: number,
    teacherId: number,
    sortField: string,
    sortDirection: string,
    page: number,
    size: number,
    getCount: boolean
}
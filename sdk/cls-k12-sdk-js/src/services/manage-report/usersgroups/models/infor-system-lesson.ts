export interface InforSystemLesson{
    gradeId: number,
    gradeName: string,
    subjectId: number,
    subjectName: string,
    teacherId: number,
    teacherName:  string
}
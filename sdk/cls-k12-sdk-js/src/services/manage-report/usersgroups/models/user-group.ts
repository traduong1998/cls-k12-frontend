export interface UserGroup {
    id: number;
    name: string;
    userGroupCode: string;
    createrId: number;
    createdDate: Date;
    portalId: number;
    divisionId: number;
    schoolId: number;
    count: number;
}
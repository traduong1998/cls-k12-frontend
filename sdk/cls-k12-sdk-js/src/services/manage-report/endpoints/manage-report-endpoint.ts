import axios, { AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import { ManageReportAPI } from "./manage-report-api";
import { GetLessonReportFilterRequests } from "../request/get-lesson-report-filter-request";
import { GetExamReportFilterRequests } from "../request/get-exam-report-filter-requests";
import { ExamReport } from '../models/exam-report';
import { LessonReport } from "../models/lesson-report";
import { ExamReportModel } from "../models/exam-report-model";
import { ExamReportRequest } from "../request/exam-report-request";
import { ExamInfoReport } from "../models/exam-info";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { ExamTime } from "../models/exam-time";
import { HistoryLessonUser } from "../request/history-lesson-user-request";
import { InforSystemLesson } from "../models/infor-system-lesson";
import { InforSystemHistoryLesson } from "../request/info-system-history-lesson-request";
import { HistoryLearnLessonUser } from "../request/HistoryLearnLessonUser";
import { DetailHistoryLearnLessonUser } from "../request/DetailHistoryLearnLessonUser";

export class ManageReportEndpoint implements ManageReportAPI {
  logger: Logger = Logger.getInstance();
  private baseUrl: string;

  constructor() {
    this.baseUrl = CLS.getConfig().apiBaseUrl;
    this.baseUrl += '/api';
  }
  getLearnInfoSystem(request: InforSystemHistoryLesson): Promise<InforSystemLesson[]> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL\
      axios
        .post(`${this.baseUrl}/reportlearn/reportlearngetinfosystem`, request)
        .then((resp: AxiosResponse<InforSystemLesson[]>) => {
          resolve(resp.data);
        })
        .catch((error) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  getLearnLesson(request: HistoryLessonUser): Promise<PaginatorResponse<LessonReport>> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL\
      axios
        .post(`${this.baseUrl}/reportlearn/reportlearnuserlesson`, request)
        .then((resp: AxiosResponse<PaginatorResponse<LessonReport>>) => {
          resolve(resp.data);
        })
        .catch((error) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }

  getHistoryLearnLesson(filter: HistoryLearnLessonUser): Promise<PaginatorResponse<LessonReport>> {
    return new Promise((resolve, reject) => {
      axios
        .post(`${this.baseUrl}/ReportLearn/ReportLearnUserLesson`, {
          gradeId: filter.gradeId, subjectId: filter.subjectId, teacherId: filter.teacherId, sortField: filter.sortField, sortDirection: filter.sortDirection,
          page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
        })
        .then((resp: AxiosResponse<PaginatorResponse<LessonReport>>) => {
          debugger;
          resolve(resp.data);
        })
        .catch((error) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }

  getDetailUserLesson(userId: number, filter: DetailHistoryLearnLessonUser): Promise<PaginatorResponse<LessonReport>> {
    return new Promise((resolve, reject) => {
      axios
        .get(`${this.baseUrl}/reports/student/${userId}/lessons`, {
          params: {
            UserId: filter.userId, GradeId: filter.gradeId, SubjectId: filter.subjectId, TeacherId: filter.teacherId, SortField: filter.sortField, SortDirection: filter.sortDirection,
            Page: filter.pageNumber, Size: filter.sizeNumber, GetCount: filter.getCount
          }
        })
        .then((resp: AxiosResponse<PaginatorResponse<LessonReport>>) => {
          debugger;
          resolve(resp.data);
        })
        .catch((error) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }

  getDateFromExamSubjectStudentId(ids: number[]): Promise<ExamTime[]> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL\
      axios
        .post(`${this.baseUrl}/reports/exams/reportgetdateexamsubjectstudent`, { listExamSubjectStudentId: ids })
        .then((resp: AxiosResponse<ExamTime[]>) => {
          resolve(resp.data);
        })
        .catch((error) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  getListExamIdByName(keyword: string): Promise<[]> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL\
      axios
        .get(`${this.baseUrl}/reports/exams/reportgetlistexamid`, { params: { keyWord: keyword } })
        .then((resp: AxiosResponse<[]>) => {
          resolve(resp.data);
        })
        .catch((error) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  getListExamMonitorById(ids: number[]): Promise<ExamInfoReport[]> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL\
      axios
        .post(`${this.baseUrl}/reports/exams/reportgetexammonitorbyid`, { listExamId: ids })
        .then((resp: AxiosResponse<ExamInfoReport[]>) => {
          resolve(resp.data);
        })
        .catch((error) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }
  getListReportExam(examReportRequest: ExamReportRequest): Promise<PaginatorResponse<ExamReportModel>> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL\
      axios
        .post(`${this.baseUrl}/reports/exams/reportexamsubjectstudentpaging`, examReportRequest)
        .then((resp: AxiosResponse<PaginatorResponse<ExamReportModel>>) => {
          resolve(resp.data);
        })
        .catch((error) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  }

  getLessonReportByUser(getLessonReportFilterRequests: GetLessonReportFilterRequests): Promise<LessonReport[]> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL\
      axios
        .get(`${this.baseUrl}/ReportLearn/reportStudyHistoryByUser`, {
          params: {
            SubjectId: getLessonReportFilterRequests.subjectId,
            GradeId: getLessonReportFilterRequests.gradeId,
            UserCreateId: getLessonReportFilterRequests.userCreateId,
            Page: getLessonReportFilterRequests.pageNumber,
            PageSize: getLessonReportFilterRequests.pageSize
          }
        })
        .then((resp) => {
          console.log("get from json", resp);
          resolve(resp.data);
        })
        .catch((error) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  };

  getExamReportByUser(getExamReportFilterRequests: GetExamReportFilterRequests): Promise<ExamReport[]> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL\
      axios
        .get(`${this.baseUrl}/reports/exams/reportExamHistoryByUser`, {
          params: {
            SubjectId: getExamReportFilterRequests.subjectId,
            GradeId: getExamReportFilterRequests.gradeId,
            Keyword: getExamReportFilterRequests.examName,
            Page: getExamReportFilterRequests.pageNumber,
            PageSize: getExamReportFilterRequests.pageSize,
          }
        })
        .then((resp) => {
          console.log("get from json", resp);
          resolve(resp.data);
        })
        .catch((error) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  };

  exportLessonReportByUser(subjectId: number, gradeId: number, teacherId: number): Promise<string> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL\
      axios
        .get(`${this.baseUrl}/ReportLearn/exportExcelStudyHistoryByUser`, {
          params: {
            SubjectId: subjectId,
            GradeId: gradeId,
            UserCreateId: teacherId,
          },
          responseType: 'blob',
        })
        .then((resp) => {
          const url = window.URL.createObjectURL(resp.data);
          console.log("report URL: ", url);
          resolve(url);
        })
        .catch((error) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  };

  exportLessonReportByUserId(userId: number, subjectId: number, gradeId: number, teacherId: number): Promise<string> {
    return new Promise((resolve, reject) => {
      axios
        .post(`${this.baseUrl}/reports/student/${userId}/lessons/export`, {
          UserId: userId,
          SubjectId: subjectId,
          GradeId: gradeId,
          UserCreateId: teacherId
        },
          {
            responseType: 'blob'
          })
        .then((resp) => {
          const url = window.URL.createObjectURL(resp.data);
          console.log("report URL: ", url);
          resolve(url);
        })
        .catch((error) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  };


  exportExamReportByUser(getExamReportFilterRequests: GetExamReportFilterRequests): Promise<string> {
    return new Promise((resolve, reject) => {
      // TODO : pass URL\
      axios
        .get(`${this.baseUrl}/reports/exams/exportExcelExamHistoryByUser`, {
          params: {
            SubjectId: getExamReportFilterRequests.subjectId,
            GradeId: getExamReportFilterRequests.gradeId,
            Keyword: getExamReportFilterRequests.examName,
          },
          responseType: 'blob',
        })
        .then((resp) => {
          const url = window.URL.createObjectURL(resp.data);
          resolve(url);
        })
        .catch((error) => {
          reject(error.response != null ? error.response.data : null);
        });
    });
  };

}

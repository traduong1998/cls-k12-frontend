import { ExamReport } from '../models/exam-report';
import { GetLessonReportFilterRequests } from "../request/get-lesson-report-filter-request";
import { LessonReport } from "../models/lesson-report";
import { GetExamReportFilterRequests } from "../request/get-exam-report-filter-requests";
import { ExamReportRequest } from '../request/exam-report-request';
import { ExamReportModel } from '../models/exam-report-model';
import { ExamInfoReport } from '../models/exam-info';
import { PaginatorResponse } from '../../../../core/api/responses/PaginatorResponse';
import { ExamTime } from '../models/exam-time';
import { HistoryLessonUser } from '../request/history-lesson-user-request';
import { InforSystemHistoryLesson } from '../request/info-system-history-lesson-request';
import { InforSystemLesson } from '../models/infor-system-lesson';
import { HistoryLearnLessonUser } from '../request/HistoryLearnLessonUser';
import { DetailHistoryLearnLessonUser } from '../request/DetailHistoryLearnLessonUser';

export interface ManageReportAPI {

    getLearnLesson(request: HistoryLessonUser): Promise<PaginatorResponse<LessonReport>>
    getHistoryLearnLesson(request: HistoryLearnLessonUser): Promise<PaginatorResponse<LessonReport>>
    getDetailUserLesson(userId: number, request: DetailHistoryLearnLessonUser): Promise<PaginatorResponse<LessonReport>>
    getLearnInfoSystem(request: InforSystemHistoryLesson): Promise<InforSystemLesson[]>

    //fake
    getLessonReportByUser(request: GetLessonReportFilterRequests): Promise<LessonReport[]>;
    getExamReportByUser(request: GetExamReportFilterRequests): Promise<ExamReport[]>;
    exportLessonReportByUser(subjectId: number, gradeId: number, teacherId: number): Promise<string>;
    exportLessonReportByUserId(userId: number, subjectId: number, gradeId: number, teacherId: number): Promise<string>;
    //report exam
    getListReportExam(examReportRequest: ExamReportRequest): Promise<PaginatorResponse<ExamReportModel>>;
    getListExamIdByName(keyword: string): Promise<[]>;

    getListExamMonitorById(ids: number[]): Promise<ExamInfoReport[]>;

    getDateFromExamSubjectStudentId(ids: number[]): Promise<ExamTime[]>
}

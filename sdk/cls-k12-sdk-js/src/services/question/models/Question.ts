import { Type } from "../enums/Type";
import { Answer } from "./Answer";

export interface Question {
    id?: number;
    content: string;
    type: Type;
    level: string;
    formatQuestion: string;
    createDate?: Date;
    approvalDate?: Date;
    userId?: number;
    questionGroupId?: number;
    answers: Answer[];
    GradeId?: number
    SubjectId?: number
    ChapterId?: number
    UnitId?: number
    
}
export interface QuestionGroup {
    id: number;
    content: string;
    isShuffleChild: boolean;
    questions: Question[];
}

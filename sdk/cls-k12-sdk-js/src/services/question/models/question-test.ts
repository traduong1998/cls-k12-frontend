export interface QuestionTest {
    id?: number;
    content: string | undefined;
    type?: string;
    order?:number;
    level: string | undefined;
    format: string | undefined;
    questionGroupId?: number;
    isChecked: boolean;
    score: number | undefined;
    userId?: number;
    contentPanelId?: number;
    chapterId?: number;
    unitId?: number;
    answers: AnswerTest[];
    questions?: QuestionTest[];
    //xác định câu hỏi đẩy lên API với mục đích gì (thêm, sửa, hay là xóa)
    action?: string | null;
}
export interface AnswerTest {
    id: number;
    questionId : number;
    content : string;
    trueAnswer : number;
    position? : boolean;
    groupOfFillBlank2? : number;
    isShuffler : boolean;
}

export interface Answer {
    id?: number;
    questionId?: number
    content: string;
    trueAnswer?: number;
    position?: boolean
    groupOfFillBlank2?: number;
    isShuffler?: boolean;
}
export class SingleChoiceAnswer implements Answer {
    id?: number;
    content: string;
    trueAnswer: number | undefined;
    /**
     *
     */
    constructor() {
        this.content = "";
    }

}
export class MultiChoiceAnswer implements Answer {
    id?: number;
    content: string;
    trueAnswer: number | undefined;

    constructor() {
        this.content = "";
    }
}

export class TrueFalseClaseAnswer implements Answer {
    id?: number;
    content: string;
    trueAnswer: number | undefined;
    constructor() {
        this.content = "";
    }

}
export class TrueFalseAnswer implements Answer {
    id?: number;
    content: string;
    trueAnswer: number | undefined;
    constructor() {
        this.content = "";
    }
}

export class FillBlankAnswer implements Answer {
    id?: number;
    content: string;
    trueAnswer?: number;
    constructor() {
        this.content = "";
    }
}
export class FillBlank2Answer implements Answer {
    id?: number;
    content: string;
    trueAnswer?: number;
    groupOfFillBlank2: number | undefined;

    constructor() {
        this.content = "";
    }
}
export class UnderLineAnswer implements Answer {
    id?: number;
    content: string;
    trueAnswer?: number;
    stt: number | undefined;
    constructor() {
        this.content = "";
    }
}
export class MatchingAnswer implements Answer {
    id?: number;
    content: string;
    trueAnswer?: number;
    position: boolean | undefined;
    constructor() {
        this.content = "";
    }
}
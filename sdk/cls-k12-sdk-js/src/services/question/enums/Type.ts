export enum Type {
    singlechoice = 'SIG',
    multichoice = 'MTC',
    truefasle = 'TFQ',
    truefaslechause = 'TFC',
    fillblank = 'FB1',
    fillblank2 = 'FB2',
    essay = 'ESS',
    underline = 'UDL',
    matching= 'MCQ'
}
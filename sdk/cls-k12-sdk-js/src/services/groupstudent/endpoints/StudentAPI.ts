import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { Student, StudentOption } from "../models/Student";
import { StudentFilterRequests } from "../models/StudentPagingRequest";
import { UnassignStudentsRequest } from "../requests/deleteStudentRequest";
import { StudentNoAssignGroupStudentPagingRequest } from "../models/StudentNoAssignGroupStudentPagingRequest";
import { AssignStudentToGroupStudentRequest } from "../requests/assignStudentToGroupStudentRequest";

export interface StudentAPI {

    getStudentsPaging(filter: StudentFilterRequests): Promise<PaginatorResponse<Student>>
    unassignStudents(student: UnassignStudentsRequest): Promise<boolean> 
    getStudentNoAssignGroupStudentPagingAsync(filter: StudentNoAssignGroupStudentPagingRequest): Promise<PaginatorResponse<Student>>
    assignStudents(student: AssignStudentToGroupStudentRequest): Promise<boolean> 
}

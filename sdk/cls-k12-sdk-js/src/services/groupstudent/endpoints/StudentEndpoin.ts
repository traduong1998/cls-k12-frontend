import axios, { AxiosResponse, AxiosError } from "axios";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import { Student, StudentOption } from "../models/Student";
import { StudentFilterRequests } from "../models/StudentPagingRequest";
import { StudentAPI } from "./StudentAPI";
import { UnassignStudentsRequest } from "../requests/deleteStudentRequest";
import { StudentNoAssignGroupStudentPagingRequest } from "../models/StudentNoAssignGroupStudentPagingRequest";
import { AssignStudentToGroupStudentRequest } from "../requests/assignStudentToGroupStudentRequest";
import { ExportStudentRequest } from '../requests/ExportStudentRequest';

export class StudentEndpoint implements StudentAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config.baseUrl;
        //this.baseUrl = config.baseUrl;
    }

    getStudentsPaging(filter: StudentFilterRequests): Promise<PaginatorResponse<Student>> {

        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/GroupStudents/GetStudentsByGroupStudentPaging`, {
                    params: {
                        GroupStudentId: filter.groupStudentId, SchoolYear: filter.schoolYear, KeyWord: filter.keyWord,
                        sortField: filter.sortField, sortDirection: filter.sortDirection,
                        page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<Student>>) => {
                    console.log(`get from json`, resp);

                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    unassignStudents(student: UnassignStudentsRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/GroupStudents/UnassignStudentsFromGroupStudent`, student)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    assignStudents(student: AssignStudentToGroupStudentRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/api/GroupStudents/AssignStudentsToGroupStudent`, student)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getStudentNoAssignGroupStudentPagingAsync(filter: StudentNoAssignGroupStudentPagingRequest): Promise<PaginatorResponse<Student>> {
        return new Promise((resolve, reject) => {

            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/Users/GetAvailableUsersAssignToGroupStudentPaging`, {
                    params: {
                        SchoolId: filter.schoolId, SchoolYear: filter.schoolYear, KeyWord: filter.keyWord,
                        sortField: filter.sortField, sortDirection: filter.sortDirection,
                        page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<Student>>) => {
                    console.log(`get from json`, resp);

                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    exportStudentReport(getStudentReportFilterRequests: ExportStudentRequest): Promise<string> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL\
            axios
                .get(`${this.baseUrl}/api/GroupStudents/exportStudentOfGroupStudent`, {
                    params: {
                        GroupStudentId: getStudentReportFilterRequests.groupStudentId,
                        SchoolYear: getStudentReportFilterRequests.schoolYear,
                        KeyWord: getStudentReportFilterRequests.keyWord,
                    },
                    responseType: 'blob',
                })
                .then((resp) => {
                    const url = window.URL.createObjectURL(resp.data);
                    resolve(url);
                })
                .catch((error) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    };

}

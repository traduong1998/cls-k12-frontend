import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { GroupStudent, GroupStudentOption } from "../models/GroupStudent";
import { GroupStudentFilterRequests } from "../models/GroupStudentPagingRequest";
import { CreateGroupStudentRequest } from "../requests/createGroupStudentRequest";
import { UpdateGroupStudentRequest } from "../requests/updateGroupStudentRequest";
import { DeleteGroupStudentRequest } from "../requests/deleteGroupStudentRequest";
import { GetGroupStudentInfo } from "../responses/get-groupstudent-info";
import { TransferGroupStudentRequest } from "../requests/transfer-groupstudent-request";
import { SubjectOfGrade } from "../../grade/../grade/models/subject-of-grade";
import { AssignStudentsToGroupStudentSameGrade } from "../models/AssignStudentsToGroupStudentSameGrade";


export interface GroupStudentAPI {

    getGroupStudentsPaging(filter: GroupStudentFilterRequests): Promise<PaginatorResponse<GroupStudent>>
    getGroupStudentOptions(schoolId: number, gradeId?: number): Promise<GroupStudentOption[]>;
    createGroupStudent(grade: CreateGroupStudentRequest): Promise<CreateGroupStudentRequest>;
    updateGroupStudent(grade: UpdateGroupStudentRequest): Promise<boolean>;
    deleteGroupStudent(grade: DeleteGroupStudentRequest): Promise<DeleteGroupStudentRequest>;
    getGroupStudentDetail(id: number): Promise<GetGroupStudentInfo>;
    transferGroupStudentRequest(request: TransferGroupStudentRequest): Promise<any>;
    assignTeacherToGroupStudent(groupStudentId: number, userId: number, schoolYear: number): Promise<boolean>;
    countStudentOfGroupStudent(groupStudentId: number): Promise<number>;

    assignStudentsToGroupStudentSameGrade(AssignStudentsToGroupStudentSameGrade: AssignStudentsToGroupStudentSameGrade):Promise<boolean>;
}

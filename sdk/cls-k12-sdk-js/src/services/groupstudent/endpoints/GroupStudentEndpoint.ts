import axios, { AxiosResponse, AxiosError } from "axios";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import { GroupStudent, GroupStudentOption } from "../models/GroupStudent";
import { GroupStudentFilterRequests } from "../models/GroupStudentPagingRequest";
import { GroupStudentAPI } from "./GroupStudentAPI";
import { UpdateGroupStudentRequest } from "../requests/updateGroupStudentRequest";
import { DeleteGroupStudentRequest } from "../requests/deleteGroupStudentRequest";
import { CreateGroupStudentRequest } from "../requests/createGroupStudentRequest";
import { GetGroupStudentInfo } from "../responses/get-groupstudent-info";
import { TransferGroupStudentRequest } from "../requests/transfer-groupstudent-request";
import { ErrorResponse } from "../../../core/api/responses/ErrorResponse ";
import { AssignStudentsToGroupStudentSameGrade } from "../models/AssignStudentsToGroupStudentSameGrade";

export class GroupStudentEndpoint implements GroupStudentAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl?: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        //this.baseUrl = "http://localhost:65000";
        this.baseUrl = this.baseUrl + '/api';
    }
    assignStudentsToGroupStudentSameGrade(AssignStudentsToGroupStudentSameGrade: AssignStudentsToGroupStudentSameGrade): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/GroupStudents/AssignStudentsToGroupStudentSameGrade`, AssignStudentsToGroupStudentSameGrade)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getGroupStudentsPaging(filter: GroupStudentFilterRequests): Promise<PaginatorResponse<GroupStudent>> {

        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/GroupStudents/GetGroupStudentsPaging`, {
                    params: {
                        SchoolId: filter.schoolId, DivisionId: filter.divisionId, GradeId: filter.gradeId, SchoolYear: filter.schoolYear,
                        sortField: filter.sortField, sortDirection: filter.sortDirection,
                        page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<GroupStudent>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getGroupStudentOptions(schoolId: number, gradeId?: number): Promise<GroupStudentOption[]> {

        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/GroupStudents/GetGroupStudentOptions`, { params: { schoolId: schoolId, gradeId: (gradeId === 0 ? null : gradeId) } })
                .then((resp: AxiosResponse<GroupStudentOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    createGroupStudent(groupstudent: CreateGroupStudentRequest): Promise<CreateGroupStudentRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/GroupStudents/Create`, groupstudent)
                .then((resp: AxiosResponse<CreateGroupStudentRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    updateGroupStudent(groupstudent: UpdateGroupStudentRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/GroupStudents/Update`, groupstudent)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    deleteGroupStudent(groupstudent: DeleteGroupStudentRequest): Promise<DeleteGroupStudentRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/GroupStudents/Delete`, groupstudent)
                .then((resp: AxiosResponse<DeleteGroupStudentRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getGroupStudentDetail(id: number): Promise<GetGroupStudentInfo> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/GroupStudents/` + id)
                .then((resp: AxiosResponse<GetGroupStudentInfo>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    transferGroupStudentRequest(request: TransferGroupStudentRequest): Promise<any> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/GroupStudents/AssignStudentsToNextGroupStudent`, request)
                .then((resp: AxiosResponse<any>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    assignTeacherToGroupStudent(groupStudentId: number, userId: number, schoolYear: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/GroupStudents/AssignTeacherToGroupStudent`, { groupStudentId, userId, schoolYear })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    countStudentOfGroupStudent(groupStudentId: number): Promise<number> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/GroupStudents/CountStudentOfGroupStudent`, { params: { GroupStudentId: groupStudentId } })
                .then((resp: AxiosResponse<number>) => {

                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });

    }
}

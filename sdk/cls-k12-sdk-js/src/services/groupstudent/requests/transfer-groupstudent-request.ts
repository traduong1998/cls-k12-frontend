export class TransferGroupStudentRequest {
    currentGroupStudentId: number | undefined;
    nextGroupStudentId: number | undefined;
    schoolYearFrom: number | undefined;
    schoolYearTo: number | undefined
}
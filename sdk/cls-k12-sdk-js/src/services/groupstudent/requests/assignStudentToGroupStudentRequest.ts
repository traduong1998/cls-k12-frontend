export class AssignStudentToGroupStudentRequest {
    groupStudentId?: number | undefined;
    schoolYear?: number | undefined;
    userIds?: number[] | undefined;
    schoolId?: number | undefined;
    gradeId?: number | undefined;
    divisionId?: number | undefined;
    
}
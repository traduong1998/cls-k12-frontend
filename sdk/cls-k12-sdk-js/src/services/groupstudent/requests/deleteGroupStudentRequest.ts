import { GroupStudent } from "../models/GroupStudent";

export class DeleteGroupStudentRequest implements GroupStudent {
    id?: number | undefined;
}
import { Student } from "../models/Student";

export class UpdateStudentRequest implements Student {
    id?: number | undefined;
    groupStudentName?: string | undefined;
    orderNumber?: number | undefined;
    schoolId?: number | undefined;
    gradeId?: number | undefined;
    divisionId?: number | undefined;
    createdDate?: Date;
    createBy?: string| undefined;
    portalId?: number| undefined;
    isDeleted?: number| undefined;
    
}
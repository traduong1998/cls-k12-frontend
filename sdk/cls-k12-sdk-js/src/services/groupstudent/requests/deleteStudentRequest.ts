export class UnassignStudentsRequest {
    groupStudentId?: number | undefined;
    schoolYear?: number | undefined;
    userIds?: number[] | undefined;
}
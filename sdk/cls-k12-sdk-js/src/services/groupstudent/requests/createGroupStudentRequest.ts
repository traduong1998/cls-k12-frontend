import { FormGroupStudent } from "../models/GroupStudent";

export class CreateGroupStudentRequest implements FormGroupStudent {
    id?: number | undefined;
    groupStudentName?: string | undefined;
    orderNumber?: number | undefined;
    schoolId?: number | undefined;
    gradeId?: number | undefined;
    divisionId?: number | undefined;
    createdDate?: Date;
    createBy?: string| undefined;
    portalId?: number| undefined;
    isDeleted?: number| undefined;

    
    constructor() {
        this.createdDate = new Date();
    }

}

export class ExportStudentRequest {
    groupStudentId?: number | undefined;
    schoolYear?: number | undefined;
    keyWord?:string;
}
import { FormStudent } from "../models/Student";

export class CreateStudentRequest implements FormStudent {
    id?: number | undefined;
    groupStudentName?: string | undefined;
    orderNumber?: number | undefined;
    schoolId?: number | undefined;
    gradeId?: number | undefined;
    divisionId?: number | undefined;
    createdDate?: Date;
    createBy?: string| undefined;
    portalId?: number| undefined;
    isDeleted?: number| undefined;

    
    constructor() {
        this.createdDate = new Date();
    }

}

import { GroupStudent } from "../models/GroupStudent";

export class UpdateGroupStudentRequest{
    groupStudentId?: number | undefined;
    groupStudentName?: string | undefined;
    orderNumber?: number | undefined;   
    gradeName?: string | undefined;
    schoolName?: string | undefined; 
    divisionName?: string | undefined;
}
import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class GroupStudentFilterRequests implements PaginatorRequest {
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  divisionId?: number| undefined;
  schoolId?: number| undefined;
  schoolYear?: number;
  gradeId?: number| undefined;
  groupStudentId?: number| undefined;
  sortField?: string;
  sortDirection?: string;
  getCount?: boolean;
}

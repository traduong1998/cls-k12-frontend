export interface GroupStudent {
    id?: number;
    groupStudentName?: string;
    schoolId?: number;
    gradeId?: number;
    divisionId?: number;
    orderNumber?: number;
    groupStudentId?: number;
    gradeName?: string;
    schoolName?: string;
    divisionName?: string;
}

export interface FormGroupStudent {
    id?: number;
    groupStudentName?: string;
    orderNumber?: number;
    schoolId?: number;
    gradeId?: number;
    divisionId?: number;
}
export interface DeleteGroupStudent {
    groupStudentId?: number;
    groupStudentName?: string;
}

export interface GroupStudentOption {
    id: number;
    name: string;
}

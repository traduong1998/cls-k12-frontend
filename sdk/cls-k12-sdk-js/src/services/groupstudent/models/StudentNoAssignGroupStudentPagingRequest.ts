import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class StudentNoAssignGroupStudentPagingRequest implements PaginatorRequest {
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  schoolId?: number| undefined;
  schoolYear?: number;
  keyWord?: string;
  sortField?: string;
  sortDirection?: string;
  getCount?: boolean;
  fullName?: string;
}
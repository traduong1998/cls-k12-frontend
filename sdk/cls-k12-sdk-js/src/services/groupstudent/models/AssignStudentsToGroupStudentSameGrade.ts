export interface AssignStudentsToGroupStudentSameGrade {
    userIds: [],
    groupStudentId: number,
    schoolYear: number
}
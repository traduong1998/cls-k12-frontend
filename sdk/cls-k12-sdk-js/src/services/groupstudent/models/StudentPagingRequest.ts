import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class StudentFilterRequests implements PaginatorRequest {
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  groupStudentId?: number| undefined;
  schoolYear?: number;
  keyWord?:string;
  sortField?: string;
  sortDirection?: string;
  getCount?: boolean;
}
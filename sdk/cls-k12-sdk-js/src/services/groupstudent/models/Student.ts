export interface Student {
    id?: number;
    username?: string;
    name?: string;
    schoolId?: number;
    gradeId?: number;
    divisionId?: number;
    orderNumber?: number;
    groupStudentId?: number;
    schoolYear?: number;
    dayOfBirth?: Date;
}

export interface StudentAssign {
    userID?: number;
    username?: string;
    name?: string;
    schoolId?: number;
    gradeId?: number;
    divisionId?: number;
    orderNumber?: number;
    groupStudentId?: number;
    schoolYear?: number;
}

export interface FormStudent {
    id?: number;
    groupStudentName?: string;
    orderNumber?: number;
    schoolId?: number;
    gradeId?: number;
    userId?: number;
    divisionId?: number;
}
export interface DeleteStudent {
    groupStudentId?: number;
    groupStudentName?: string;
    schoolYear?: number;
    userID?: number;
}

export interface StudentOption {
    id: number;
    name: string;
}
export interface UnassignStudentDTO {
    schoolYear?: number;
    userIds?: number[];
}

export interface AssignStudentDTO {
    userIds?: number[];
    GroupStudentId?: number; 
    SchoolId?: number;
    GradeId?: number;
    DivisionId?: number; 
    SchoolYear?: number;
}


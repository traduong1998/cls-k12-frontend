export interface AssignTeacherGroupStudent {
    groupStudentId: number,
    schoolYear: number,
    schoolId: number
}
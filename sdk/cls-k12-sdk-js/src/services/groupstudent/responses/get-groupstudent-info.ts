export interface GetGroupStudentInfo {
    groupStudentId: number,
    groupStudentName: string,
    orderNumber: number,
    teacherName: string,
    teacherId: number,
    gradeName: string,
    gradeId: number,
    schoolName: string,
    schoolId: number

}
export interface UploadFileResponse {
    fileId: string;
    name: string;
    type: string;
    extension: string;
    isPrivate: boolean;
    link: string;
}
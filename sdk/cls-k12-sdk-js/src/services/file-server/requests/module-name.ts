export enum ServiceName {
    LEARN = "learn",
    EXAM = "exam",
    OTHER = "other",
}

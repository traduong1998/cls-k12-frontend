import { ServiceName } from "./module-name";

export interface UploadFileRequest {
    /**Tập tin gửi lên */
    file: File;

    /**True nếu mong muốn bảo mật file này */
    isPrivate: boolean;

    /** Tên service request lên */
    moduleName?: string
}
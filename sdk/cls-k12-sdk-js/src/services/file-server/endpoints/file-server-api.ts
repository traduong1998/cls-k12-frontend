import { UploadFileRequest } from "../../file-server/requests/upload-file-request";
import { UploadFileResponse } from "../../file-server/responses/upload-file-response";

export interface FileServerAPI {
    uploadFile(request: UploadFileRequest, config?: { onUploadProgress: any }): Promise<UploadFileResponse>;

    skipFile(fileId: string): Promise<boolean>;

    getStatusFile(link: string): Promise<string>;
    getLinkIOSLinks(videoId: string): Promise<string>;
}
import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS, Logger } from "../../..";
import { ErrorResponse } from "../../../core/api/responses/ErrorResponse ";
import { UploadFileRequest } from "../requests/upload-file-request";
import { UploadFileResponse } from "../responses/upload-file-response";
import { FileServerAPI } from "./file-server-api";

export class FileServerEndpoint implements FileServerAPI {
    private baseUrl: string;
    private urlServerFile: string;

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrls.fileServerConvert ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl += '/api';
        this.urlServerFile = CLS.getConfig().apiBaseUrls.fileServer ?? CLS.getConfig().apiBaseUrl;
    }

    uploadFile(request: UploadFileRequest, config?: { onUploadProgress: any }): Promise<UploadFileResponse> {
        return new Promise((resolve, reject) => {
            let form: FormData = new FormData();
            form.append('file', request.file);
            form.append('isPrivate', request.isPrivate + '');
            form.append('moduleName', request.moduleName + '');

            let baseUrl = this.baseUrl;
            // file image or scorm thì sẽ up thông qua server file, ko cần qua convert, 2 file này cần xử lý nhanh, scorm thì cơ bản là giải nén ra rất nhiều file nhỏ, nên làm trực tiếp svfile cho nhanh
            if (request.file.type.match(/image\/*/) || request.file.type == 'application/x-zip-compressed') {
                baseUrl = CLS.getConfig().apiBaseUrls.fileServer + '/api';
            }
            axios.post(`${baseUrl}/file/upload`, form, {
                onUploadProgress: (progressEvent) => {
                    var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
                    console.log(percentCompleted);
                    config?.onUploadProgress(percentCompleted);
                }
            })
                .then((resp: AxiosResponse<UploadFileResponse>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    console.error(error);
                    reject(error.response?.data);
                });
        });
    }

    skipFile(fileId: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/file/actions/skip`, { fileId })
                .then((resp: AxiosResponse<boolean>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    console.error(error);
                    reject(error.response?.data);
                });
        });
    }

    getStatusFile(linkId: string): Promise<string> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/file/${linkId}`)
                .then((resp: AxiosResponse<string>) => {
                    // console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    console.error(error);
                    reject(error.response?.data);
                });
        });
    }

    getLinkIOSLinks(videoId: string): Promise<string> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.urlServerFile}/api/File/${videoId}/links`)
                .then((resp: AxiosResponse<string>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    console.error(error);
                    reject(error.response?.data);
                });
        });
    }
}
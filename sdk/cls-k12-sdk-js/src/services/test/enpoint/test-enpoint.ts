import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { ErrorResponse } from "../../../core/api/responses/ErrorResponse ";
import { Test } from "../models/test";
import { TestDetail } from "../models/test-detail";
import { QuestionModel } from "../requests/question-model";
import { TestApi } from "./test-api";

export class TestEnpoint implements TestApi {
    baseUrl: string;
    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
        //this.baseUrl = "http://localhost:58910";
        //this.baseUrl = this.baseUrl + '/api/v1';
        // console.log(`endpoit`, this.baseUrl);

    }
    createTest(test: Test): Promise<Test> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/tests`, test).then((resp: AxiosResponse<Test>) => {
                resolve(resp.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    updateTest(test: Test): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.put(`${this.baseUrl}/tests/` + test.id, test).then((resp: AxiosResponse<boolean>) => {
                resolve(resp.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    // phân quyền xem bộ đề
    getTests(testKitId: number): Promise<TestDetail[]> {
      // console.log("getTests", testKitId);
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/tests/get-by-testKitId/` + testKitId).then((resp: AxiosResponse<TestDetail[]>) => {
                resolve(resp.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    // không phân quyền
    getTestsView(testKitId: number): Promise<TestDetail[]> {
          return new Promise((resolve, reject) => {
              axios.get(`${this.baseUrl}/tests/get-by-testKitId-view/` + testKitId).then((resp: AxiosResponse<TestDetail[]>) => {
                  resolve(resp.data);
              })
                  .catch((error: AxiosError<ErrorResponse>) => {
                      reject(error.response != null ? error.response.data : null);
                  });
          });
      }

    getFileTest(filePath: string): Promise<any> {
        return new Promise((resolve, reject) => {
            axios.get(filePath).then((resp: AxiosResponse<any>) => {
                // console.log(resp)
                resolve(resp.data);
            })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
}

import { Test } from "../models/test";
import { TestDetail } from "../models/test-detail";
import { QuestionModel } from "../requests/question-model";

export interface TestApi {
    createTest(test:Test):Promise<Test>;
    updateTest(test: Test): Promise<boolean>;
    getTests(testKitId: number): Promise<TestDetail[]>;
    getTestsView(testKitId: number): Promise<TestDetail[]>
}
export interface AnswerModel {
    content: string,
    trueAnswer: number,
    position: boolean,
    groupOfFillBlank2: number,
    isShuffler: boolean,
    action?: string
}
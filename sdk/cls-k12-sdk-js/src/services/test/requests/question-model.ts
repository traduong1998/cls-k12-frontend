import { AnswerModel } from "./answer-model";

export interface QuestionModel {
    id?: number,
    order?: number,
    score: number,
    content: string,
    type: string,
    level: string,
    format: string,
    contentPanelId: number,
    chapterId: number,
    unitId: number,
    questions: QuestionModel[],
    answers: AnswerModel[],
    action?: string
}


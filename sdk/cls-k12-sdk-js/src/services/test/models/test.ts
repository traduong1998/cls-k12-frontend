import { QuestionModel } from "../requests/question-model";

export interface Test {
    id?: number,
    name: string,
    gradeId: number,
    subjectId: number,
    testKitId: number,
    examId?: number,
    questions: QuestionModel[]
}
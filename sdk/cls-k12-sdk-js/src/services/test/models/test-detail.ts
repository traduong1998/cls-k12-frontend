export interface TestDetail {
    id: number,
    name: string,
    createLevel: string
    divisionId: number,
    schoolId: number
    gradeId: number
    subjectId: number
    testKitId: number
    urlJsonTest: string
}
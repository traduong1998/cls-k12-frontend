export interface TeacherExam {
    id: number;
    fullName: string;
    userName:string;
    email:string;
    workUnit:string;
    getDateOfBirth: string;
    getGender: string;
    isChecked:boolean;
    action:string;
}
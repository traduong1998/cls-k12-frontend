export interface StudentExam {
    id: number;
    userName: string;
    fullName: string;
    email: string;
    groupStudentName: string;
    getDayOfBirth: string;
    getGender: string;
    isChoose: boolean;
    action: string;
}
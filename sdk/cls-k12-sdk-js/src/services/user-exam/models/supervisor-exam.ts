import { ShiftInfo } from "../../organize-exam/models/ShiftInfo";

export interface SupervisorOrganize {
  organizeExamInfoId: number;
  shiftName: string;
  roomName: string;
  examSubjectSupervisorId: number;
  action?:string;
}
export interface SupervisorShiftExamEdit {
  organizeExamId: number;
  shiftInfo: ShiftInfo;
  listSupervisorShiftExam: SupervisorOrganize[];
}

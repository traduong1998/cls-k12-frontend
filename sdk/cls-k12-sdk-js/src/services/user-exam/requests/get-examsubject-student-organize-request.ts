export class GetExamSubjectStudentOrganizeRequest {
    organizeExamId?: number;
    keyword: string | undefined;
    sortField?: string;
    sortDirection?: string;
    fromRecord?: number;
    size?: number;
}
import { PaginatorRequest } from "../../../core/api/requests/PaginatorRequest";

export class StudentExamRequest implements PaginatorRequest {
    gradeId: number | undefined;
    groupStudentId?: number;
    keyword: string | undefined;
    examId?: number;
    subjectId?: number;
    organizeExamId?: number;
    sortField?: string;
    sortDirection?: string;
    pageNumber?: number;
    pageSize?: number;
}
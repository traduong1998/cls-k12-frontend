import { PaginatorRequest } from "../../../core/api/requests/PaginatorRequest";

export class TeacherExamPagingRequest implements PaginatorRequest{

    divisionId?:number;
    schoolId?:number | undefined;
    keyword:string | undefined;
    examId?:number;
    subjectId?:number;
    organizeExamId?:number;
    sortField?:string ;
    sortDirection?:string ;
    pageNumber?:number;
    pageSize?:number;
}
export interface TeacherAction{
    id:number;
    action:string; 
}
import { UserAction } from "./user-action-request";

export interface ExamSubjectSupervisorOrganize{
    organizeExamInfoId:number;
    examSubjectSupervisorId:number;
    action:string;
}
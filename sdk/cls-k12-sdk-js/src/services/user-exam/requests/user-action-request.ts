export interface UserActionResquest{
    examId:number;
    subjectId:number;
    gradeId?:number;
    users:UserAction[]
}
export interface UserAction{
    id:number;
    action:string; 
}
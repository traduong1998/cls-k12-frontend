import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { TeacherExam } from "../../exam/models/TeacherExam";
import { ShiftInfoDTO } from "../../organize-exam/models/shift-info-dto";

import { StudentExam } from "../models/student-exam";
import { SupervisorOrganize } from "../models/supervisor-exam";
import { GetExamSubjectStudentOrganizeRequest } from "../requests/get-examsubject-student-organize-request";
import { StudentExamRequest } from "../requests/student-exam-request";
import { SupervisorOrganizeRequest } from "../requests/supervisor-organize-request";
import { TeacherExamPagingRequest } from "../requests/teacher-exam-request";


export interface UserExamAPI {
    getStudentExam(studentExamRequest: StudentExamRequest): Promise<PaginatorResponse<StudentExam>>;
    getTeacherExam(teacherExamRequest: TeacherExamPagingRequest): Promise<PaginatorResponse<TeacherExam>>;
    getSupervisorExam(supervisorExamRequest: TeacherExamPagingRequest): Promise<PaginatorResponse<TeacherExam>>;
    getSupervisorExamOrganize(request: SupervisorOrganizeRequest): Promise<TeacherExam[]>;
    getSupervisorOfOrganize(id: number): Promise<SupervisorOrganize[]>;
    getExamSubjectStudentOrganize(request: GetExamSubjectStudentOrganizeRequest): Promise<StudentExam[]>;
    addExamSupervisorOrganize(id: number, request: SupervisorOrganize[]): Promise<boolean>;
    getStudentExamFinish(studentExamRequest: StudentExamRequest): Promise<PaginatorResponse<StudentExam>>;
    getSupervisorExamFinish(supervisorExamRequest: TeacherExamPagingRequest): Promise<PaginatorResponse<TeacherExam>>;
    getTeacherExamFinish(supervisorExamRequest: TeacherExamPagingRequest): Promise<PaginatorResponse<TeacherExam>>;
    exportExcelExamSubjectStudentOrganize(shiftInfo:ShiftInfoDTO,request: GetExamSubjectStudentOrganizeRequest): Promise<string>;
    exportExcelExamSubjectSupervisorOrganize(shiftInfo:ShiftInfoDTO,request: SupervisorOrganizeRequest): Promise<string>;
    exportExcelSupervisorExamFinish(supervisorExamRequest: TeacherExamPagingRequest): Promise<string>;
    exportExcelTeacherExamFinish(request: TeacherExamPagingRequest): Promise<string>;
    exportExcelStudentExamFinish(studentExamRequest: StudentExamRequest): Promise<string>;
}
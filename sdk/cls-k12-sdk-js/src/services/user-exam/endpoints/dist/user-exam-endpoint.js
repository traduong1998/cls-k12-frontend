"use strict";
exports.__esModule = true;
exports.UserExamEndpoint = void 0;
var axios_1 = require("axios");
var __1 = require("../../..");
var UserExamEndpoint = /** @class */ (function () {
    function UserExamEndpoint() {
        this.endpoint = '';
        this.logger = __1.Logger.getInstance();
        this.baseUrl = __1.CLS.getConfig().apiBaseUrl;
    }
    UserExamEndpoint.prototype.addExamSupervisorOrganize = function (id, request) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // TODO : pass URL
            axios_1["default"]
                .post(_this.baseUrl + "/api/OrganizeExams/AddExamSupervisorOrganize", { organizeExamInfoId: id, listExamSubjectSupervisorOrganize: request })
                .then(function (resp) {
                console.log("get from json", resp);
                resolve(resp.data);
            })["catch"](function (error) {
                reject(error.response != null ? error.response.data : null);
            });
        });
    };
    UserExamEndpoint.prototype.getSupervisorOfOrganize = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // TODO : pass URL
            axios_1["default"]
                .get(_this.baseUrl + "/api/OrganizeExams/GetSupervisorOfOrganize", {
                params: {
                    id: id
                }
            })
                .then(function (resp) {
                console.log("get from json", resp);
                resolve(resp.data);
            })["catch"](function (error) {
                reject(error.response != null ? error.response.data : null);
            });
        });
    };
    UserExamEndpoint.prototype.getSupervisorExamOrganize = function (request) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // TODO : pass URL
            axios_1["default"]
                .get(_this.baseUrl + "/api/OrganizeExams/GetExamSubjectSupervisorOrganize", {
                params: {
                    organizeExamId: request.organizeExamId, keyword: request.keyword, organizeExamInfoId: request.organizeExamInfoId
                }
            })
                .then(function (resp) {
                console.log("get from json", resp);
                resolve(resp.data);
            })["catch"](function (error) {
                reject(error.response != null ? error.response.data : null);
            });
        });
    };
    UserExamEndpoint.prototype.getTeacherExam = function (request) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // TODO : pass URL
            axios_1["default"]
                .get(_this.baseUrl + "/api/OrganizeExams/GetExamSubjectTeacherPaging", {
                params: {
                    divisionId: request.divisionId, schoolId: request.schoolId, subjectId: request.subjectId, organizeExamId: request.organizeExamId, examId: request.examId, keyword: request.keyword, sortField: request.sortField,
                    sortDirection: request.sortDirection, pageNumber: request.pageNumber, pageSize: request.pageSize
                }
            })
                .then(function (resp) {
                console.log("get from json", resp);
                resolve(resp.data);
            })["catch"](function (error) {
                reject(error.response != null ? error.response.data : null);
            });
        });
    };
    UserExamEndpoint.prototype.getSupervisorExam = function (request) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // TODO : pass URL
            axios_1["default"]
                .get(_this.baseUrl + "/api/OrganizeExams/GetExamSubjectSupervisorPaging", {
                params: {
                    divisionId: request.divisionId, schoolId: request.schoolId, subjectId: request.subjectId, organizeExamId: request.organizeExamId, examId: request.examId, keyword: request.keyword, sortField: request.sortField,
                    sortDirection: request.sortDirection, pageNumber: request.pageNumber, pageSize: request.pageSize
                }
            })
                .then(function (resp) {
                console.log("get from json", resp);
                resolve(resp.data);
            })["catch"](function (error) {
                reject(error.response != null ? error.response.data : null);
            });
        });
    };
    UserExamEndpoint.prototype.getStudentExam = function (request) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // TODO : pass URL
            axios_1["default"]
                .get(_this.baseUrl + "/api/OrganizeExams/GetExamSubjectStudentPaging", {
                params: {
                    gradeId: request.gradeId, subjectId: request.subjectId, organizeExamId: request.organizeExamId, examId: request.examId, groupStudentId: request.groupStudentId, keyword: request.keyword, sortField: request.sortField,
                    sortDirection: request.sortDirection, pageNumber: request.pageNumber, pageSize: request.pageSize
                }
            })
                .then(function (resp) {
                console.log("get from json", resp);
                resolve(resp.data);
            })["catch"](function (error) {
                reject(error.response != null ? error.response.data : null);
            });
        });
    };
    UserExamEndpoint.prototype.getExamSubjectStudentOrganize = function (request) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            // TODO : pass URL
            axios_1["default"]
                .get(_this.baseUrl + "/api/OrganizeExams/GetExamSubjectStudentOrganize", {
                params: {
                    organizeExamId: request.organizeExamId, keyword: request.keyword, filter: {
                        sortField: request.sortField, sortDirection: request.sortDirection
                    },
                    FromRecord: request.fromRecord, size: request.size
                }
            })
                .then(function (resp) {
                console.log("get from json", resp);
                resolve(resp.data);
            })["catch"](function (error) {
                reject(error.response != null ? error.response.data : null);
            });
        });
    };
    UserExamEndpoint.prototype.getStudentExamFinish = function (request) {
        return new Promise(function (resolve, reject) {
            // TODO : pass URL
            axios_1["default"]
                .get("http://localhost:65000/api/v1/OrganizeExams/GetExamSubjectStudentFinish", {
                params: {
                    gradeId: request.gradeId, subjectId: request.subjectId, organizeExamId: request.organizeExamId, examId: request.examId, groupStudentId: request.groupStudentId, keyword: request.keyword, sortField: request.sortField,
                    sortDirection: request.sortDirection, pageNumber: request.pageNumber, pageSize: request.pageSize
                }
            })
                .then(function (resp) {
                console.log("get from json", resp);
                resolve(resp.data);
            })["catch"](function (error) {
                reject(error.response != null ? error.response.data : null);
            });
        });
    };
    return UserExamEndpoint;
}());
exports.UserExamEndpoint = UserExamEndpoint;

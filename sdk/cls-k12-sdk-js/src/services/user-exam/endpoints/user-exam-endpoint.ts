import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS, Logger } from "../../..";
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { TeacherExam } from "../../exam/models/TeacherExam";
import { ShiftInfoDTO } from "../../organize-exam/models/shift-info-dto";
import { StudentExam } from "../models/student-exam";
import { SupervisorOrganize } from "../models/supervisor-exam";
import { GetExamSubjectStudentOrganizeRequest } from "../requests/get-examsubject-student-organize-request";
import { StudentExamRequest } from "../requests/student-exam-request";
import { SupervisorOrganizeRequest } from "../requests/supervisor-organize-request";
import { TeacherExamPagingRequest } from "../requests/teacher-exam-request";
import { UserExamAPI } from "./user-exam-api";

export class UserExamEndpoint implements UserExamAPI {
    endpoint = '';

    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
    }

    addExamSupervisorOrganize(id: number, request: SupervisorOrganize[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/api/OrganizeExams/AddExamSupervisorOrganize`,
                    { organizeExamInfoId: id, listExamSubjectSupervisorOrganize: request }
                )
                .then((resp: AxiosResponse<boolean>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getSupervisorOfOrganize(id: number): Promise<SupervisorOrganize[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/OrganizeExams/GetSupervisorOfOrganize`, {
                    params: {
                        id: id
                    }
                })
                .then((resp: AxiosResponse<SupervisorOrganize[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getSupervisorExamOrganize(request: SupervisorOrganizeRequest): Promise<TeacherExam[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/OrganizeExams/GetExamSubjectSupervisorOrganize`, {
                    params: {
                        organizeExamId: request.organizeExamId, keyword: request.keyword, organizeExamInfoId: request.organizeExamInfoId
                    }
                })
                .then((resp: AxiosResponse<TeacherExam[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getTeacherExam(request: TeacherExamPagingRequest): Promise<PaginatorResponse<TeacherExam>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/OrganizeExams/GetExamSubjectTeacherPaging`, {
                    params: {
                        divisionId: request.divisionId, schoolId: request.schoolId, subjectId: request.subjectId, organizeExamId: request.organizeExamId, examId: request.examId, keyword: request.keyword, sortField: request.sortField,
                        sortDirection: request.sortDirection, pageNumber: request.pageNumber, pageSize: request.pageSize
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<TeacherExam>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getSupervisorExam(request: TeacherExamPagingRequest): Promise<PaginatorResponse<TeacherExam>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/OrganizeExams/GetExamSubjectSupervisorPaging`, {
                    params: {
                        divisionId: request.divisionId, schoolId: request.schoolId, subjectId: request.subjectId, organizeExamId: request.organizeExamId, examId: request.examId, keyword: request.keyword, sortField: request.sortField,
                        sortDirection: request.sortDirection, pageNumber: request.pageNumber, pageSize: request.pageSize
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<TeacherExam>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getStudentExam(request: StudentExamRequest): Promise<PaginatorResponse<StudentExam>> {

        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/OrganizeExams/GetExamSubjectStudentPaging`, {
                    params: {
                        gradeId: request.gradeId, subjectId: request.subjectId, organizeExamId: request.organizeExamId, examId: request.examId, groupStudentId: request.groupStudentId, keyword: request.keyword, sortField: request.sortField,
                        sortDirection: request.sortDirection, pageNumber: request.pageNumber, pageSize: request.pageSize
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<StudentExam>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });

    }
    getExamSubjectStudentOrganize(request: GetExamSubjectStudentOrganizeRequest): Promise<StudentExam[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/OrganizeExams/GetExamSubjectStudentOrganize`, {
                    params: {
                        organizeExamId: request.organizeExamId, keyword: request.keyword, filter: {
                            sortField: request.sortField, sortDirection: request.sortDirection
                        },
                        FromRecord: request.fromRecord, size: request.size
                    }
                })
                .then((resp: AxiosResponse<StudentExam[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getStudentExamFinish(request: StudentExamRequest): Promise<PaginatorResponse<StudentExam>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/OrganizeExams/GetExamSubjectStudentFinish`, {
                    params: {
                        gradeId: request.gradeId, subjectId: request.subjectId, organizeExamId: request.organizeExamId, examId: request.examId, groupStudentId: request.groupStudentId, keyword: request.keyword, sortField: request.sortField,
                        sortDirection: request.sortDirection, pageNumber: request.pageNumber, pageSize: request.pageSize
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<StudentExam>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getSupervisorExamFinish(request: TeacherExamPagingRequest): Promise<PaginatorResponse<TeacherExam>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/OrganizeExams/GetExamSubjectSupervisorFinish`, {
                    params: {
                        divisionId: request.divisionId, schoolId: request.schoolId, subjectId: request.subjectId, organizeExamId: request.organizeExamId, examId: request.examId, keyword: request.keyword, sortField: request.sortField,
                        sortDirection: request.sortDirection, pageNumber: request.pageNumber, pageSize: request.pageSize
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<TeacherExam>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getTeacherExamFinish(request: TeacherExamPagingRequest): Promise<PaginatorResponse<TeacherExam>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/OrganizeExams/GetExamSubjectTeacherFinish`, {
                    params: {
                        divisionId: request.divisionId, schoolId: request.schoolId, subjectId: request.subjectId, organizeExamId: request.organizeExamId, examId: request.examId, keyword: request.keyword, sortField: request.sortField,
                        sortDirection: request.sortDirection, pageNumber: request.pageNumber, pageSize: request.pageSize
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<TeacherExam>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    exportExcelExamSubjectStudentOrganize(shiftInfo: ShiftInfoDTO, request: GetExamSubjectStudentOrganizeRequest): Promise<string> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL\
            axios
                .post(`${this.baseUrl}/api/OrganizeExams/exportexcel-exam-subject-student-organize`,
                    { ShiftInfoDTO: shiftInfo, Request: request }, { responseType: 'blob' }
                )
                .then((resp) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    exportExcelExamSubjectSupervisorOrganize(shiftInfo: ShiftInfoDTO, request: SupervisorOrganizeRequest): Promise<string> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL\
            axios
                .post(`${this.baseUrl}/api/OrganizeExams/exportexcel-exam-subject-supervisor-organize`,
                    { ShiftInfoDTO: shiftInfo, Request: request }, { responseType: 'blob' }
                )
                .then((resp) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    exportExcelSupervisorExamFinish(supervisorExamRequest: TeacherExamPagingRequest): Promise<string> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL\
            axios
                .post(`${this.baseUrl}/api/OrganizeExams/exportexcel-exam-subject-supervisor-finish`,
                    supervisorExamRequest, { responseType: 'blob' }
                )
                .then((resp) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    exportExcelTeacherExamFinish(supervisorExamRequest: TeacherExamPagingRequest): Promise<string> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL\
            axios
                .post(`${this.baseUrl}/api/OrganizeExams/exportexcel-exam-subject-teacher-finish`,
                    supervisorExamRequest, { responseType: 'blob' }
                )
                .then((resp) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    exportExcelStudentExamFinish(studentExamRequest: StudentExamRequest): Promise<string> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL\
            axios
                .post(`${this.baseUrl}/api/OrganizeExams/exportexcel-exam-subject-student-finish`,
                studentExamRequest, { responseType: 'blob' }
                )
                .then((resp) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
}
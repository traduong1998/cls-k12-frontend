export interface User {
    id: number;
    name: string;
    username: string;
    userTypeName: string;
    schoolName: string;
    getStatus: string;
    getDayOfBirth: string;
    getActivedDate: string;
}
export interface UsersResponse {
    id: number;
    name: string;
    username: string;
    userTypeName: string;
    schoolName: string;
    getStatus: string;
    getDayOfBirth: string;
    getActivedDate: string;
}





export class TeacherFromFile {
    no?: number
    firstName?: string;
    lastName?: string;
    userName?: string;
    email?: string;
    birthday?: string;
    phone?: string;
    gender?: string;
    address?: string;
}
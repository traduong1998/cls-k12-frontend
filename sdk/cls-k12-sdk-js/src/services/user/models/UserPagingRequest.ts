import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class UsersFilterRequests implements PaginatorRequest {
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  sortField?: string;
  sortDirection?: string;
  getCount?: boolean;
  divisionId?: number;
  schoolId?: number;
  gradeId?: number;
  groupStudentId?: number;
  userTypeId?: number;
  fromDate?: string;
  toDate?: string;
  keyFullName?: string;
  keyUsername?: string;
  status?: string;
  schoolYear?: number;
}

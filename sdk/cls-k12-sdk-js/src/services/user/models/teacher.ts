export interface TeacherOption {
    id: number;
    name: string;
    avatar: string;
    emailAddress?: string;
}
import { promises } from "dns";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { TeacherOption } from "../models/teacher";
import { User } from "../models/User";
import { UsersFilterRequests } from "../models/UserPagingRequest";
import { CreateTeachersFromFile } from "../requests/create-teachers-from-file";
import { CreateUserRequest } from "../requests/createUserRequest";
import { EditUserRequest } from "../requests/edit-user-request";
import { CreateTeacherErrorResponse } from "../responses/create-teacher-error-response";
import { GetUserInfoResponse } from "../responses/get-user-info-response";

export interface UsersAPI {
    getUserInfor():Promise<GetUserInfoResponse>;


    getUserInfoById(portalId: number, id: number): Promise<GetUserInfoResponse>
    getUsersByTypeId(typeId?: number, divisionId?: number, schoolId?: number): Promise<User[]>
    getUsersByPaging(filter: UsersFilterRequests): Promise<PaginatorResponse<User>>
    createUser(userCreate: CreateUserRequest): Promise<boolean>
    getUserById(id: number): Promise<GetUserInfoResponse>
    editUser(userEdit: EditUserRequest): Promise<boolean>
    deleteUser(id: number): Promise<boolean>
    getTeacherOfSchoolOptions(schoolId: number): Promise<TeacherOption[]>
    getTeachers(): Promise<TeacherOption[]>;
    createTeacherImportFromFile(request: CreateTeachersFromFile): Promise<CreateTeacherErrorResponse[]>;

    getListTeacherByIds(ids:number[]):Promise<TeacherOption>
    GetUserOfSchool(schoolId : number): Promise<User>

    /**20/09/2021 LAMNV11 Add updateProfile */
    UpdateProfile(schoolId: number): Promise<boolean>;
}

import axios, { AxiosError, AxiosResponse } from "axios";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import { TeacherOption } from "../models/teacher";
import { User, UsersResponse } from "../models/User";
import { UsersFilterRequests } from "../models/UserPagingRequest";
import { CreateStudentsFromFile } from "../requests/create-students-from-file";
import { CreateTeachersFromFile } from "../requests/create-teachers-from-file";
import { CreateUserRequest } from "../requests/createUserRequest";
import { EditUserRequest } from "../requests/edit-user-request";
import { PasswordRequest } from "../requests/password-user-request";
import { UserInforRequest } from "../requests/user-info-request";
import { CreateStudentErrorResponse } from "../responses/create-student-error-response";
import { CreateTeacherErrorResponse } from "../responses/create-teacher-error-response";
import { GetUserInfoResponse } from "../responses/get-user-info-response";
import { UsersAPI } from "./UserAPI";

export class UserEndpoint implements UsersAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        //this.baseUrl = "http://localhost:65000";
        this.baseUrl = this.baseUrl + '/api';
    }
    getListTeacherByIds(ids: number[]): Promise<TeacherOption> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/users/GetUsersByIds`, ids)
                .then((resp: AxiosResponse<TeacherOption>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getUserInfor(): Promise<GetUserInfoResponse> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Users/GetMyUserInfo`)
                .then((resp: AxiosResponse<GetUserInfoResponse>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    updateUserInfo(userInfo: UserInforRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios
                .patch(`${this.baseUrl}/Users/UpdateUserInfo`, userInfo)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    updatePasworUser(passwordRequest: PasswordRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios
                .patch(`${this.baseUrl}/Users/UpdatePassword`, passwordRequest)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    deleteUser(id: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/Users/Delete`, { id: id })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    createUser(userCreate: CreateUserRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/Users/Create`, userCreate)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getUsersByPaging(filter: UsersFilterRequests): Promise<PaginatorResponse<User>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/Users/GetUsersPaging`, {
                    params: {
                        divisionId: filter.divisionId, schoolId: filter.schoolId, gradeId: filter.gradeId, groupStudentId: filter.groupStudentId,
                        fromDate: filter.fromDate, toDate: filter.toDate, userTypeId: filter.userTypeId, status: filter.status, schoolYear: filter.schoolYear,
                        keyFullName: filter.keyFullName, keyUsername: filter.keyUsername, sortField: filter.sortField, sortDirection: filter.sortDirection,
                        page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<User>>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getUsersByTypeId(typeId?: number, divisionId?: number, schoolId?: number): Promise<UsersResponse[]> {

        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/`, { params: { typeId, divisionId, schoolId } })
                .then((resp: AxiosResponse<UsersResponse[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getUserById(id: number): Promise<GetUserInfoResponse> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Users/` + id)
                .then((resp: AxiosResponse<GetUserInfoResponse>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getUserInfoById(portalId: number, id: number): Promise<GetUserInfoResponse> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/system/global/users/${id}`, { params: { portalId: portalId } })
                .then((resp: AxiosResponse<GetUserInfoResponse>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }


    editUser(userEdit: EditUserRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .patch(`${this.baseUrl}/Users/Update`, userEdit)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getTeacherOfSchoolOptions(schoolId: number): Promise<TeacherOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/Users/GetTeacherOfSchool`, {
                    params: {
                        schoolId: schoolId
                    }
                })
                .then((resp: AxiosResponse<TeacherOption[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    getTeachers(): Promise<TeacherOption[]> {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.baseUrl}/Users/GetListTeacher`, {
                })
                .then((resp: AxiosResponse<TeacherOption[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    GetListUserByIds(userIdArr: number[]): Promise<TeacherOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/Users/GetUsersByIds`, userIdArr)
                .then((resp: AxiosResponse<TeacherOption[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    createTeacherImportFromFile(request: CreateTeachersFromFile): Promise<CreateTeacherErrorResponse[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/Users/CreateTeachersFromFile`, request)
                .then((resp: AxiosResponse<any>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    createStudentImportFromFile(request: CreateStudentsFromFile): Promise<CreateStudentErrorResponse[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/Users/CreateStudentFromFile`, request)
                .then((resp: AxiosResponse<any>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    GetUserOfSchool(schoolId: number): Promise<User> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/Users/GetUserOfSchool`, {
                    params: {
                        schoolId: schoolId
                    }
                })
                .then((resp: AxiosResponse<User>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    /**20/09/2021 LAMNV11 add updateProfile */
    UpdateProfile(profile: any): Promise<boolean> {

        return new Promise((resolve, reject) => {
            axios
                .put(`${this.baseUrl}/Users/UpdateProfile`, profile)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
}

export interface UserInforRequest {
    userId: number | undefined;
    username: string | undefined;
    firstName: string | undefined;
    lastName: string | undefined;
    email: string | undefined;
    gender?: boolean | undefined;

    dayOfBirth?: Date | undefined;
    avatar?: any | undefined
    address: string | undefined;
    phoneNumber: string | undefined;
}

export class CreateUserRequest{
    username: string | undefined;
    firstName: string | undefined;
    lastName: string | undefined;
    email: string | undefined;
    password: string | undefined;
    userTypeId: number | undefined;
    gender?: boolean | undefined;
    status: string | undefined;
    divisionId?: number | undefined;
    schoolId?: number | undefined;
    gradeId?: number | undefined;
    groupStudentId?: number | undefined;

    dayOfBirth?: Date | undefined;
    avatar?: any | undefined
    academicTitle: string | undefined;
    degree: string | undefined;
    officeAddress: string | undefined;
    address: string | undefined;
    phone: string | undefined;
    biography: string | undefined;
}
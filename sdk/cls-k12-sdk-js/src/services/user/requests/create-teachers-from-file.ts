import { TeacherFromFile } from "../models/teacher-from-file";


export interface CreateTeachersFromFile {
    userTypeId: number;
    divisionId?: number;
    schoolId?: number;
    teachersData: TeacherFromFile[]
}
export interface EditUserRequest {
    userId: number | undefined;
    username: string | undefined;
    firstName: string | undefined;
    lastName: string | undefined;
    email: string | undefined;
    password: string | undefined;
    gender?: boolean | undefined;
    status: string | undefined;
    
    dayOfBirth?: Date | undefined;
    avatar?: any | undefined
    academicTitle: string | undefined;
    degree: string | undefined;
    officeAddress: string | undefined;
    address: string | undefined;
    phone: string | undefined;
    biography: string | undefined;
}
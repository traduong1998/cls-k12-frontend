import { StudentFromFile } from "../models/student-from-file";

export interface CreateStudentsFromFile {
    userTypeId: number;
    divisionId?: number;
    schoolId?: number;
    gradeId?: number;
    groupStudentId?: number;
    schoolYear: number;
    studentsData: StudentFromFile[]
}
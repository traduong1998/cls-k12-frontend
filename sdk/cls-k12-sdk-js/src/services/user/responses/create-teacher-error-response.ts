import { TeacherFromFile } from "../models/teacher-from-file";

export interface CreateTeacherErrorResponse {
    errorCount: number;
    errorResult: ErrorResult[];
    rowData: TeacherFromFile;

}

export interface ErrorResult {
    columnError: string;
    reasonDetail: string;
    reasonType: string;
    columnIndex: number;
}
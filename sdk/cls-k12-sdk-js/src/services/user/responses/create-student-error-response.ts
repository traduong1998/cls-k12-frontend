import { StudentFromFile } from "../models/student-from-file";


export interface CreateStudentErrorResponse {
    errorCount: number;
    errorResult: ErrorResult[];
    rowData: StudentFromFile;

}

export interface ErrorResult {
    columnError: string;
    reasonDetail: string;
    reasonType: string;
    columnIndex: number;
}

export class GetUserInfoResponse {
    userId: number | undefined;
    username: string | undefined;
    firstName: string | undefined;
    lastName: string | undefined;
    email: string | undefined;
    password: string | undefined;
    gender?: boolean | undefined;
    status: string | undefined;

    dayOfBirth?: Date | undefined;
    avatar?: any | undefined
    academicTitle: string | undefined;
    degree: string | undefined;
    officeAddress: string | undefined;
    address: string | undefined;
    phoneNumber: string | undefined;
    biography: string | undefined;

    userTypeRole: string | undefined;
    userTypeName: string | undefined;
    userTypeLevelManage: string | undefined;
    divisionId: number | undefined;
    divisionName: string | undefined;
    schoolName: string | undefined;
    gradeName: string | undefined;
    groupStudentName: string | undefined;
    teacherName?: string
}
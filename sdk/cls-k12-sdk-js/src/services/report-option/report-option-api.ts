import { ClassOptionModel } from "./models/class-option-model";
import { DivisionOptionModel } from "./models/division-option-model";
import { GradeOptionModel } from "./models/grade-option-model";
import { SchoolOptionModel } from "./models/school-option-model";
import { SubjectOptionModel } from "./models/subject-option-model";
import { TeacherOptionModel } from "./models/teacher-option-model";

export interface ReportOptionApi{
    getDivisonOption():Promise<DivisionOptionModel[]>
    getSchoolOption(divisionId:number):Promise<SchoolOptionModel[]>
    getGradeOption(schoolId:number):Promise<GradeOptionModel[]>
    getClassOption(schoolId:number, gradeId:number):Promise<ClassOptionModel[]>
    getSubjectOption(gradeId:number):Promise<SubjectOptionModel[]>
    getTeacherOption(schoolId:number):Promise<TeacherOptionModel[]>
}
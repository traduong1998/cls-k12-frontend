import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS, Logger } from "../..";
import { ClassOptionModel } from "./models/class-option-model";
import { DivisionOptionModel } from "./models/division-option-model";
import { GradeOptionModel } from "./models/grade-option-model";
import { SchoolOptionModel } from "./models/school-option-model";
import { SubjectOptionModel } from "./models/subject-option-model";
import { TeacherOptionModel } from "./models/teacher-option-model";
import { ReportOptionApi } from "./report-option-api";

export class ReportOptionEnpoint implements ReportOptionApi {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
    }

    getDivisonOption(): Promise<DivisionOptionModel[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/api/report/system/GetDivisionOptions`)
                .then((resp: AxiosResponse<DivisionOptionModel[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getSchoolOption(divisionId: number): Promise<SchoolOptionModel[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/api/report/system/GetSchoolOptions`, { params: { divisionId: divisionId } })
                .then((resp: AxiosResponse<SchoolOptionModel[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getGradeOption(schoolId: number): Promise<GradeOptionModel[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/api/report/system/GetGradeOptions`, { params: { schoolId: schoolId } })
                .then((resp: AxiosResponse<GradeOptionModel[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getClassOption(schoolId: number, gradeId: number): Promise<ClassOptionModel[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/api/report/system/GetGroupStudentOptions`, { params: { schoolId: schoolId, gradeId:gradeId } })
                .then((resp: AxiosResponse<ClassOptionModel[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getSubjectOption(gradeId: number): Promise<SubjectOptionModel[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/api/report/system/GetSubjectOptions`, { params: { gradeId:gradeId } })
                .then((resp: AxiosResponse<SubjectOptionModel[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getTeacherOption(schoolId: number): Promise<TeacherOptionModel[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/api/report/system/GetTeacherOptions`, { params: { schoolId:schoolId } })
                .then((resp: AxiosResponse<TeacherOptionModel[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
}
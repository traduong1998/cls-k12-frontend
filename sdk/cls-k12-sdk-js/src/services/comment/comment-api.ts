import { PaginatorResponse } from '../../../core/api/responses/PaginatorResponse';
import { Comment } from './models/comment'
import { CommentResponse } from './models/comment-response';
import { CommentTeacher } from './models/comment-teacher';
import { CommentUpdate } from './models/comment-update';
import { QuestionOfStudent } from './models/question-of-student';
import { StudentFilter } from './requests/studentFilter';

export interface CommentApi {
    createComment(comment: Comment): Promise<boolean>
    createStudentAskQuestion(comment: QuestionOfStudent): Promise<boolean>
    updateComment(commentUpdate: CommentUpdate): Promise<boolean>

    updateCommentTeacher(commentUpdate: CommentUpdate): Promise<boolean>

    getCommentParent(StudentFilter: StudentFilter): Promise<PaginatorResponse<CommentResponse>>
    getCommentChild(lessonId: number, parentId: number, SortField: string, sortDirection: string, pageNumber: number, sizeNumber: number): Promise<PaginatorResponse<CommentResponse>>

    getCommentTeacher(lessonId: number, SortField: string, sortDirection: string, pageNumber: number, sizeNumber: number): Promise<CommentTeacher[]>
    deleteComment(commentId: number): Promise<boolean>
    deleteCommentTeacher(commentId: number): Promise<boolean>
}
import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class StudentFilter implements PaginatorRequest {
    pageNumber: number | undefined;
    sizeNumber: number | undefined;
    SortField?: string;
    getCount?:boolean;
    sortDirection?: string;
    lessonId: number | undefined;
}
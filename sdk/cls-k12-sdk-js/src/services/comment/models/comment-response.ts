export interface CommentResponse {
    commentStudentId: number,
    userId: number,
    lessonId: number,
    messageContent: string
    parentId: number,
    createdDate: Date
}
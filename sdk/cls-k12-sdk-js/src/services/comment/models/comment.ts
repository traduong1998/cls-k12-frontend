export interface Comment{
    lessonId:number,
    messageContent:string,
    parentId:number;
}
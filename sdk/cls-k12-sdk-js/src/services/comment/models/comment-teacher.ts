export interface CommentTeacher {

    commentTeacherId: number,
    userId: number,
    lessonId: number,
    messageContent: string,
    createdDate: Date,
    listCommentChildrend: ChildrenComment[]
}

export interface ChildrenComment {

    commentTeacherId: number,
    userId: number,
    lessonId: number,
    messageContent: string,
    parentId: number,
    createdDate: Date

}
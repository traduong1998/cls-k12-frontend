export interface QuestionOfStudent {
    lessonId: number,
    messageContent: string,
    parentId: number
}
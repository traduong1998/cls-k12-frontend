import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS, Logger } from "../..";
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { CommentApi } from "./comment-api";
import { Comment } from "./models/comment";
import { CommentResponse } from "./models/comment-response";
import { CommentTeacher } from "./models/comment-teacher";
import { CommentUpdate } from "./models/comment-update";
import { QuestionOfStudent } from "./models/question-of-student";
import { StudentFilter } from "./requests/studentFilter";

export class CommentEndpoint implements CommentApi {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
    }
    updateCommentTeacher(commentUpdate: CommentUpdate): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios
                .post(`${this.baseUrl}/api/commentteacher/updatecommentteacher`, commentUpdate)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    deleteCommentTeacher(commentId: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios
                .post(`${this.baseUrl}/api/commentteacher/deletecommentteacher`, { id: commentId })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    deleteComment(commentId: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios
                .post(`${this.baseUrl}/api/commentstudent/deletecommentstudent`, { id: commentId })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    updateComment(commentUpdate: CommentUpdate): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios
                .post(`${this.baseUrl}/api/commentstudent/updatecommentstudent`, commentUpdate)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    getCommentTeacher(lessonId: number, SortField: string, sortDirection: string, pageNumber: number, sizeNumber: number): Promise<CommentTeacher[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/commentteacher/getlistcommentteacherparent`, {
                    params: {
                        LessonId: lessonId,
                        'Filter.SortField': SortField,
                        'Filter.SortDirection': sortDirection,
                        page: pageNumber,
                        size: sizeNumber
                    }
                })
                .then((resp: AxiosResponse<CommentTeacher[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    getCommentChild(lessonId: number, parentId: number, SortField: string, sortDirection: string, pageNumber: number, sizeNumber: number): Promise<PaginatorResponse<CommentResponse>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/commentstudent/getlistcommentsudentchildrend`, {
                    params: {
                        LessonId: lessonId,
                        parentId: parentId,
                        'Filter.SortField': SortField,
                        'Filter.SortDirection': sortDirection,
                        page: pageNumber,
                        size: sizeNumber
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<CommentResponse>>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    getCommentParent(studentFilter: StudentFilter): Promise<PaginatorResponse<CommentResponse>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/commentstudent/getlistcommentsudentparent`, {
                    params: {
                        LessonId: studentFilter.lessonId,
                        'Filter.SortField': studentFilter.SortField,
                        'Filter.SortDirection': studentFilter.sortDirection,
                        page: studentFilter.pageNumber,
                        size: studentFilter.sizeNumber
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<CommentResponse>>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    createComment(comment: Comment): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios
                .post(`${this.baseUrl}/api/commentstudent/createcommentstudent`, comment)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    createStudentAskQuestion(comment: QuestionOfStudent): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios
                .post(`${this.baseUrl}/api/commentteacher/createcommentteacher`, comment)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }


    // getCurriculumsPaging(filter: CurriculumsFilterRequests): Promise<PaginatorResponse<Curriculum>> {
    //     return new Promise((resolve, reject) => {
    //         // TODO : pass URL
    //         axios
    //             .get(`${this.baseUrl}/api/Curriculums/GetCurriculumsPaging`, {
    //                 params: {
    //                     gradeId: filter.gradeId, subjectId: filter.subjectId, isActive: filter.isActive,
    //                     sortField: filter.sortField, sortDirection: filter.sortDirection,
    //                     page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
    //                 }
    //             })
    //             .then((resp: AxiosResponse<PaginatorResponse<Curriculum>>) => {
    //                 resolve(resp.data);
    //             })
    //             .catch((error: AxiosError) => {
    //                 reject(error.response?.data);
    //             });
    //     });
    // }
    // createCurriculum(curriculumCreate: CreateCurriculumRequest): Promise<boolean> {
    //     return new Promise((resolve, reject) => {
    //         // TODO : pass URL
    //         axios
    //             .post(`${this.baseUrl}/api/Curriculums/Create`, curriculumCreate)
    //             .then((resp: AxiosResponse<boolean>) => {
    //                 resolve(resp.data);
    //             })
    //             .catch((error: AxiosError) => {
    //                 reject(error.response?.data);
    //             });
    //     });
    // }

    // getCurriculumChildrentNodes(id: number): Promise<CurriculumDetailInfo[]> {
    //     return new Promise((resolve, reject) => {
    //         // TODO : pass URL
    //         axios
    //             .get(`${this.baseUrl}/api/Curriculums/GetCurriculumChildrent`, {
    //                 params: {
    //                     curriculumId: id
    //                 }
    //             })
    //             .then((resp: AxiosResponse<CurriculumDetailInfo[]>) => {
    //                 resolve(resp.data);
    //             })
    //             .catch((error: AxiosError) => {
    //                 reject(error.response?.data);
    //             });
    //     });
    // }

    // updateCurriculum(curriculumEdit: EditCurriculumRequest): Promise<boolean> {
    //     return new Promise((resolve, reject) => {
    //         // TODO : pass URL
    //         axios
    //             .post(`${this.baseUrl}/api/Curriculums/Update`, curriculumEdit)
    //             .then((resp: AxiosResponse<boolean>) => {
    //                 resolve(resp.data);
    //             })
    //             .catch((error: AxiosError) => {
    //                 reject(error.response?.data);
    //             });
    //     });
    // }

    // deleteCurriculum(id: number): Promise<boolean> {
    //     return new Promise((resolve, reject) => {
    //         // TODO : pass URL
    //         axios
    //             .post(`${this.baseUrl}/api/Curriculums/Delete`, {
    //                 curriculumId: id
    //             })
    //             .then((resp: AxiosResponse<boolean>) => {
    //                 resolve(resp.data);
    //             })
    //             .catch((error: AxiosError) => {
    //                 reject(error.response?.data);
    //             });
    //     });
    // }

}
export interface Usertype {
    id: number;
    name: string;
}
export interface UsertypeOption {
    id: number;
    name: string;
    levelManage: string | undefined;
}
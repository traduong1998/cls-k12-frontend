import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { UserType } from "../models/UserType";
import { UserTypeFilterRequests } from "../models/userTypePagingRequest";
import { CreateUserTypeRequest } from "../requests/createUserTypeRequest";
import { UpdateUserTypeRequest } from "../requests/updateUserTypeRequest";
import { UsertypeOption } from "../responses/Usertype";

export interface UserTypeAPI {

    getUserTypesPaging(filter: UserTypeFilterRequests): Promise<PaginatorResponse<UserType>>
    createUserType(userType: CreateUserTypeRequest): Promise<boolean>;
    updateUserType(userType: UpdateUserTypeRequest): Promise<boolean>
    getUserTypeOptions(role: string): Promise<UsertypeOption[]>;
    deleteUserType(id: number): Promise<boolean>;
    getUserTypeByLevelManage(levelManage: string): Promise<UsertypeOption[]>
    updateUserTypeAllUser(userTypeId: number, userTypeNewId: number): Promise<boolean> 
}
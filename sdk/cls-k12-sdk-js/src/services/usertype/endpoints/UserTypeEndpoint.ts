import axios, { AxiosError, AxiosResponse } from "axios";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import { UserType } from "../models/UserType";
import { UserTypeFilterRequests } from "../models/userTypePagingRequest";
import { CreateUserTypeRequest } from "../requests/createUserTypeRequest"
import { UpdateUserTypeRequest } from "../requests/updateUserTypeRequest"
import { UserTypeAPI } from "./UserTypeAPI";
import { UsertypeOption } from "../responses/Usertype";


export class UserTypeEndpoint implements UserTypeAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config.baseUrl;
        //this.baseUrl = "http://localhost:65000";
        this.baseUrl = this.baseUrl + '/api';
    }

    getUserTypesPaging(filter: UserTypeFilterRequests): Promise<PaginatorResponse<UserType>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/Usertypes`, {
                    params: {
                        keyWord: filter.keyWord, keyFullName: filter.keyFullName, keyUsername: filter.keyUsername, sortField: filter.sortField, sortDirection: filter.sortDirection,
                        page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<UserType>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    createUserType(userType: CreateUserTypeRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/Usertypes`, userType)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    updateUserType(userType: UpdateUserTypeRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.patch(`${this.baseUrl}/usertypes/${userType.id}`, userType)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getUserTypeOptions(role: string): Promise<import("../responses/Usertype").UsertypeOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Usertypes/GetUsertypeOptions`, { params: { role: role } })
                .then((resp: AxiosResponse<UsertypeOption[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    deleteUserType(id: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.delete(`${this.baseUrl}/usertypes/${id}`)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getUserTypeByLevelManage(levelManage: string): Promise<UsertypeOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Usertypes/getusertypebylevelmanage`, { params: { level: levelManage } })
                .then((resp: AxiosResponse<UsertypeOption[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    updateUserTypeAllUser(userTypeId: number, userTypeNewId: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.patch(`${this.baseUrl}/users/updateusertypealluser`, { userTypeId, userTypeNewId })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}

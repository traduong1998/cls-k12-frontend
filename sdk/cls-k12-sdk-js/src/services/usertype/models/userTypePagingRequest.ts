import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class UserTypeFilterRequests implements PaginatorRequest {
  keyWord?: string;
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  sortField?: string;
  sortDirection?: string;
  getCount?: boolean;
  keyFullName?: string;
  keyUsername?: string;
}

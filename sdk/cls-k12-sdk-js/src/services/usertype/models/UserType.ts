export interface UserType {
    id?: number;
    name: string;
    role?: string;
    levelManage?: string;
    permissionValue?: number;
    createDate?: Date;
    createBy?: string;
    portalId?: number;
    isDeleted?: number;
    donviquanly?: string;
}
export interface UserTypeOption {
    id: number;
    name: string;
    levelManage: string | undefined;
}

import { PermissionModules } from "../models/PermissionModules";
import { UserType } from "../models/UserType";

export class CreateUserTypeRequest{
    name: string| undefined;
    userTypeRole?: string| undefined;
    levelManage?:string| undefined;
    permissionModules?: PermissionModules[]| undefined;
    constructor() {
        this.name = '';
    }
}

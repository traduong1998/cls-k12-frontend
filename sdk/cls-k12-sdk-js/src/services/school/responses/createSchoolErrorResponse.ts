import { SchoolFromFile } from "../models/SchoolFromFile";

export interface CreateSchoolErrorResponse {
    errorCount: number;
    errorResult: ErrorResult[];
    rowData: SchoolFromFile;

}

export interface ErrorResult {
    columnError: string;
    reasonDetail: string;
    reasonType: string;
    columnIndex: number;
}
import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class SchoolFilterRequests implements PaginatorRequest {
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  divisionId?: number;
  sortField?: string;
  sortDirection?: string;
  getCount?: boolean;
  keyWord?: string;
}

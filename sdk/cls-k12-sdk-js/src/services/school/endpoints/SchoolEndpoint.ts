import axios, { AxiosResponse, AxiosError } from "axios";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import { School, SchoolOption } from "../models/School";
import { SchoolFilterRequests } from "../models/SchoolPagingRequest";
import { SchoolAPI } from "./SchoolAPI";
import { DeleteSchoolRequest } from "../requests/deleteSchoolRequest";
import { UpdateSchoolRequest } from "../requests/updateSchoolRequest";
import { CreateSchoolRequest } from "../requests/createSchoolRequest";
import { SchoolOptionModel } from "../../report-option/models/school-option-model";
import { SchoolFromFile } from "../models/SchoolFromFile";
import { CreateSchoolErrorResponse } from "../responses/createSchoolErrorResponse";

export class SchoolEndpoint implements SchoolAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        //this.baseUrl = config.baseUrl;
    }
    getSchoolsPaging(filter: SchoolFilterRequests): Promise<PaginatorResponse<School>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/Schools/GetSchoolsPaging`, {
                    params: {
                        divisionId: filter.divisionId,
                        keyWord: filter.keyWord, sortField: filter.sortField, sortDirection: filter.sortDirection,
                        page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<School>>) => {
                    
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getSchoolOptions(divisionId?: number): Promise<SchoolOption[]> {
        console.log(divisionId);
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/Schools/GetSchoolOptions`, { params: { divisionId: divisionId } })
                .then((resp: AxiosResponse<SchoolOption[]>) => {
                    debugger;
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getSchoolDetail(id?: number): Promise<School> {
        console.log(id);
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/schools/getschool`, { params: { Id: id } })
                .then((resp: AxiosResponse<School>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    createSchool(school: CreateSchoolRequest): Promise<CreateSchoolRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Schools/Create`, school)
                .then((resp: AxiosResponse<CreateSchoolRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    updateSchool(school: UpdateSchoolRequest): Promise<UpdateSchoolRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Schools/UpdateSchool`, school)
                .then((resp: AxiosResponse<UpdateSchoolRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    deleteSchool(school: DeleteSchoolRequest): Promise<DeleteSchoolRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Schools/DeleteSchool`, school)
                .then((resp: AxiosResponse<DeleteSchoolRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    countGroupStudentOfSchool(schoolId: number): Promise<number> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/Schools/CountGroupStudentOfSchool`, {
                    params: {
                        SchoolId: schoolId
                    }
                })
                .then((resp: AxiosResponse<number>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getTemplate(): Promise<string> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL\
            axios
                .get(`${this.baseUrl}/api/Schools/GetTemplate`, {
                    // .get(`http://localhost:65000/api/v1/Schools/GetTemplate`, {
                    params: {},
                    responseType: 'blob',
                })
                .then((resp) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    };

    createSchoolImportFromFile(request: SchoolFromFile[]): Promise<CreateSchoolErrorResponse[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/api/Schools/CreateSchoolFromFile`, { schoolFromFile: request })
                // .post(`http://localhost:65000/api/v1/Schools/CreateSchoolFromFile`, { schoolFromFile: request })
                .then((resp: AxiosResponse<CreateSchoolErrorResponse[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    checkNameSchool(id: number , name: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/Schools/CheckSchoolName`, { params: {Id: id , Name: name } })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data: null);
                });
        }); 
    }
}

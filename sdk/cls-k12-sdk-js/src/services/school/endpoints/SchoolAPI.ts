import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { School, SchoolOption } from "../models/School";
import { SchoolFilterRequests } from "../models/SchoolPagingRequest";
import { CreateSchoolRequest } from "../requests/createSchoolRequest";
import { UpdateSchoolRequest } from "../requests/updateSchoolRequest";
import { DeleteSchoolRequest } from "../requests/deleteSchoolRequest";
import { SchoolOptionModel } from "../../report-option/models/school-option-model";

export interface SchoolAPI {

    getSchoolsPaging(filter: SchoolFilterRequests): Promise<PaginatorResponse<School>>
    getSchoolOptions(schoolId?: number): Promise<SchoolOption[]>;
    createSchool(school: CreateSchoolRequest): Promise<CreateSchoolRequest>;
    updateSchool(school: UpdateSchoolRequest): Promise<UpdateSchoolRequest>;
    deleteSchool(school: DeleteSchoolRequest): Promise<DeleteSchoolRequest>;
    countGroupStudentOfSchool(schoolId: number): Promise<number>;
    getTemplate(): Promise<string>;
    checkNameSchool(id: number, name: string): Promise<boolean>;
    getSchoolDetail(id?: number): Promise<School>;
}

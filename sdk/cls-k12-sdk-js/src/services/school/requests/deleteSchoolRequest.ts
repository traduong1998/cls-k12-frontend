import { School } from "../models/School";

export class DeleteSchoolRequest implements School {
    id?: number | undefined;
}
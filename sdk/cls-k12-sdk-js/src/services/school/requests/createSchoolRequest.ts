import { School } from "../models/School";

export class CreateSchoolRequest implements School {
    id?: number | undefined;
    name?: string | undefined;
    code?: string | undefined;
    divisionId?: number | undefined
    createdDate?: Date;
    createBy?: string| undefined;
    portalId?: number| undefined;
    isDeleted?: number| undefined;
    
    constructor() {
        this.createdDate = new Date();
    }

}

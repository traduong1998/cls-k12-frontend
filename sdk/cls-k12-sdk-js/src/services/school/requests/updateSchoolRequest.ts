import { School } from "../models/School";

export class UpdateSchoolRequest implements School {
    id?: number | undefined;
    name?: string | undefined;
    code?: string | undefined;
    divisionId?: number | undefined
    orderNumber?: number| undefined;
    createdDate?: Date;
    createBy?: string| undefined;
    portalId?: number| undefined;
    isDeleted?: number| undefined;
    
}
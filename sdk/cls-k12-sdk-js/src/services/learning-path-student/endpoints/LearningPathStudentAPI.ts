import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { LessonOfUser } from "../../lesson/responses/lesson-of-user";
import { DetailLearningPathModel } from "../models/DetailLearningPathModel";
import { LearningPathModel } from "../models/LearningPathModel";
import { LearningPathOption } from "../models/LearningPathOption";
import { TeacherLearningPathOption } from "../models/TeacherLearningPathOption";
import { StudentAssignLearningPathRequest } from "../request/StudentAssignLearningPathRequest";
import { ListStudentGetLearningPathResponse } from "../response/ListStudentGetLearningPathResponse";

export interface LearningPathStudentAPI {
    getListLearningPath(request?: any): Promise<PaginatorResponse<LearningPathModel>>
    getLearningPathOption(studentId: number): Promise<LearningPathOption[]>
    getTeacherLearningPathOption(studentId: number): Promise<TeacherLearningPathOption[]>
    getDetailLearningPath(learningPathId: number): Promise<DetailLearningPathModel[]>
    getListLessonIdsOfStep(learningPathStepId: number): Promise<number[]>
    getLessonsByIds(lessonIds:number[],lessonName:string): Promise<PaginatorResponse<LessonOfUser>>
    getListUserAssignLearningPath(request: StudentAssignLearningPathRequest): Promise<PaginatorResponse<ListStudentGetLearningPathResponse>>;
}



import axios, { AxiosResponse } from "axios";
import { CLS, Logger } from "../../..";
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { LessonOfUser } from "../../lesson/responses/lesson-of-user";
import { DetailLearningPathModel } from "../models/DetailLearningPathModel";
import { LearningPathModel } from "../models/LearningPathModel";
import { LearningPathOption } from "../models/LearningPathOption";
import { TeacherLearningPathOption } from "../models/TeacherLearningPathOption";
import { LearningPathStudentAPI } from "./LearningPathStudentAPI";
import { StudentAssignLearningPathRequest } from "../request/StudentAssignLearningPathRequest";
import { ListStudentGetLearningPathResponse } from "../response/ListStudentGetLearningPathResponse";
import { AssignStudentsForLearningPathRequest } from "../request/AssignStudentsForLearningPathRequest";
import { ListStudentForAssignLearningPathResponse } from "../response/ListStudentForAssignLearningPathResponse";


export class LearningPathStudentEndpoint implements LearningPathStudentAPI{
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl?: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        //this.baseUrl = config.baseUrl;
    }
    getListLearningPath(request?: any): Promise<PaginatorResponse<LearningPathModel>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/LearningPaths/GetListLearningPath`, { params: request })
            // axios.get(`http://localhost:65000/api/v1/LearningPaths/GetListLearningPath`, { params: request })
                .then((resp: AxiosResponse<PaginatorResponse<LearningPathModel>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getLearningPathOption(studentId: number): Promise<LearningPathOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/LearningPaths/GetLearningPathOption`, { params: studentId })
            // axios.get(`http://localhost:65000/api/v1/LearningPaths/GetLearningPathOption`, { params: studentId })
                .then((resp: AxiosResponse<LearningPathOption[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getTeacherLearningPathOption(studentId: number): Promise<TeacherLearningPathOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/LearningPaths/GetTeacherLearningPathOption`, { params: studentId })
            // axios.get(`http://localhost:65000/api/v1/LearningPaths/GetTeacherLearningPathOption`, { params: studentId })
                .then((resp: AxiosResponse<TeacherLearningPathOption[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getDetailLearningPath(learningPathId: number): Promise<DetailLearningPathModel[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/LearningPaths/GetDetailLearningPath`, { params: {learningPathId: learningPathId}  })
            // axios.get(`http://localhost:65000/api/v1/LearningPaths/GetDetailLearningPath`, { params: {learningPathId: learningPathId}  })
                .then((resp: AxiosResponse<DetailLearningPathModel[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getListLessonIdsOfStep(learningPathStepId: number): Promise<number[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/LearningPaths/GetListLessonIdsOfStep`, { params: {learningPathStepId: learningPathStepId}  })
            // axios.get(`http://localhost:65000/api/v1/LearningPaths/GetListLessonIdsOfStep`, { params: {learningPathStepId: learningPathStepId}  })
                .then((resp: AxiosResponse<number[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getLessonsByIds(lessonIds:number[],lessonName:string): Promise<PaginatorResponse<LessonOfUser>> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/api/Lesson/GetLessonsByIds`,  {lessonIds, lessonName} )
            // axios.post(`http://localhost:65000/api/v1/Lesson/GetLessonsByIds`,  {lessonIds, lessonName} )
                .then((resp: AxiosResponse<PaginatorResponse<LessonOfUser>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getListUserAssignLearningPath(request: StudentAssignLearningPathRequest): Promise<PaginatorResponse<ListStudentGetLearningPathResponse>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/LearningPaths/GetUserAssignLearningsPathPaging`, { params: {
                DivisionId: request.DivisionId, SchoolId: request.SchoolId,GradeId: request.GradeId,GroupStudentId: request.GroupStudentId,
                LearningPathId: request.LearningPathId, SchoolYear: request.SchoolYear, SortField: request.SortField,SortDirection: request.SortDirection,
                Page: request.pageNumber,  Size:request.sizeNumber ,GetCount: request.GetCount
            } })
                .then((resp: AxiosResponse<PaginatorResponse<ListStudentGetLearningPathResponse>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    assignStudentsForLearningPath(request: AssignStudentsForLearningPathRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/LearningPaths/AddListUserLearningPath`, request)
                .then((resp: AxiosResponse<boolean>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
}
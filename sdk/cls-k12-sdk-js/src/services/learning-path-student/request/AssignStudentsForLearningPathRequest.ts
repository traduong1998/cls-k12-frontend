export interface AssignStudentsForLearningPathRequest{
    learningPathId: number;
    listUserLearningPath: AssignStudentsForLearningPathDetailRequest[];
}
export interface AssignStudentsForLearningPathDetailRequest {
    id: number;
    action: string;
}
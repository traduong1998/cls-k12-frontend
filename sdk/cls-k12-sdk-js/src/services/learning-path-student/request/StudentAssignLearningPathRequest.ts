import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class StudentAssignLearningPathRequest implements PaginatorRequest {
  DivisionId? : number;
  SchoolId?: number;
  GradeId?: number;
  GroupStudentId?: number;
  LearningPathId?: number;
  SchoolYear?: number;
  SortField?: string;
  SortDirection?: string;
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  GetCount?: boolean;
}

export interface ListStudentForAssignLearningPathResponse {
    id: number;
    fullName: string;
    username: string;
    groupStudentName: string;
    isApproval: boolean;
} 
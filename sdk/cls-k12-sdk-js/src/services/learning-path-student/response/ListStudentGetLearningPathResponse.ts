
export interface ListStudentGetLearningPathResponse {
    id: number;
    fullName: string;
    username: string;
    groupStudentName: string;
    isApproval: boolean;
} 
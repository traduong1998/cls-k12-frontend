export interface DetailLearningPathModel {
    id?: number;
    name?: string;
    startDate?: Date;
    endDate?: Date;
    hideDate?:boolean;
    learningPathName?: string;
}
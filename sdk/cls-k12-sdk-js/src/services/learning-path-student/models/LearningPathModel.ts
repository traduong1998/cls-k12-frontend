export interface LearningPathModel{
    learningPathId:number;
    learningPathName:string;
    teacherId:number;
    teacherName:string;
    studentId:number;
    startDate:Date;
    endDate:Date;
    status:string;
}


export interface InforExamSubjectStudent {
    id: number;
    examId: number;
    organizeExamId: number;
    gradeId: number;
    gradeName: string;
    portalName: string;
    subjectId?: number;
    shiftId?: number;
    subjectName?: string;
    startDate?: Date;
    endDate?: Date;
    timeRemaining?: number;
}
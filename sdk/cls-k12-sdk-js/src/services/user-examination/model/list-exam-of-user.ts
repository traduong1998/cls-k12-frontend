export interface ListExamOfUser {
    id: number;
    examId:number;
    subjectId:number
    examName: string;
    roomName: string;
    shiftName: string;
    testTime: number;
    status: string;
    startDate: string;
    endDate: string;
    gradeName: string;
    subjectName: string;
    examType: string;
}

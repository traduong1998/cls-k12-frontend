export interface InforExam {
    id: number;
    name: string;
    testTime?: number;
    isPreservingTime?: boolean;
    isLockTestScreen?: boolean
    isPublicScore?: boolean
    isPublicAnswer?: boolean
}
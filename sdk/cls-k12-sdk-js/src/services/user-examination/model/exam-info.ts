export interface ExamInfoUserExammination {
    examId: number;
    subjectId:number;
    name: string;
    testTime: number;
}

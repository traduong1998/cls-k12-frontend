export interface ExamDetaiInfo {
    /// Đã hoàn thành bài làm
    name: string
    isCompleted: boolean;
    isSuspended: boolean;
    testId: number;
    testKitId: number;
    isShuffler: boolean;
    testCodeURL: string;
    displayPerPage: number;
    testTime: number;
    numberOfMediaOpening: number;
    isPreservingTime: boolean;
    isPublicScore: boolean;
    isPublicAnswer: boolean
    isLockTestScreen: boolean
}
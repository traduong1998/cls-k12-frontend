export interface InfoMarkingEssay {
    testKitId: number;
    examId:number;
    testCodeURL: string;
    testId:number;
    userExercises:DoExamEssay[];
    examName:string;
}

export interface DoExamEssay {
    id: number;
    questionId:number;
    content: string;
    attachments: number;
    score:number;
    teacherComment:string;
    questionContent:string;
    questionScore:number;
    order:number;
}

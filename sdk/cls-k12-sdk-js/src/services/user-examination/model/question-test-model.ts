import { QuestionTest } from "../../question/models/question-test";

export interface QuestionTestModel {
    id: number;
    questions: QuestionTest[];
}

import { SubmitUserExerciseRequest } from './request/submit-userExercise-request';
import { CreateTestUrlRequest } from './request/create-test-url-request';
import { Request } from 'express';
import axios, { AxiosError, AxiosResponse } from 'axios';
import { CLS, Logger } from '../../..';
import { PaginatorResponse } from '../../../../core/api/responses/PaginatorResponse';
import { ExamDetaiInfo } from '../model/exam-detail-info';
import { ExamInfoUserExammination } from '../model/exam-info';
import { InforExam } from '../model/info-exam';
import { InforExamSubjectStudent } from '../model/infor-exam-subject-student';
import { ListExamOfUser } from '../model/list-exam-of-user';
import { QuestionTestModel } from '../model/question-test-model';
import { ExamInfoRequest } from './request/exam-info-request';
import { ListExamOfUserRequest } from './request/list-exam-of-user-request';
import { UpdateUserExerciseRequest, UserExerciseRequest } from './request/update-userExercise-request';
import { UserExaminationAPI } from './user-examination-api';
import { UserExerciseResponse } from './responses/user-exercise-response';
import { ResultDoExamResponse } from './responses/result-do-exam-response';
import { InfoMarkingEssay } from '../model/info-marking-essay';
import { MarkingEssayRequest } from './request/marking-essay-request';

export class UserExaminationEndpoint implements UserExaminationAPI {

  endpoint = '';

  private baseUrl: string | undefined;

  constructor() {
    this.baseUrl = CLS.getConfig().apiBaseUrl;
  }
 
  getListExamInfo(request: ExamInfoRequest[]): Promise<ExamInfoUserExammination[]> {
    return new Promise((resolve, reject) => {
      axios
        .post(`${this.baseUrl}/api/exams/GetInfoExamById`,
          { listExamSubjectId: request }
        )
        .then((resp: AxiosResponse<ExamInfoUserExammination[]>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });

  }

  getListExamIdByKeyword(keyword: string): Promise<number[]> {
    return new Promise((resolve, reject) => {
      axios
        .get(` ${this.baseUrl}/api/exams/getlistexamid?keyWord=${keyword}`, {
        })
        .then((resp: AxiosResponse<number[]>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }

  getListExamOfUser(request: ListExamOfUserRequest, listExamIds?: number[]): Promise<PaginatorResponse<ListExamOfUser>> {
    return new Promise((resolve, reject) => {
      axios
        .post(`${this.baseUrl}/api/OrganizeExams/GetAllSubjectExamByStudentPaging`,
          {
            ExamType: request.examType, startDate: request.startDate, endDate: request.endDate,
            page: request.pageNumber, size: request.pageSize, sortField: request.sortField, sortDirection: request.sortDirection
            , subjectId: request.subjectId, gradeId: request.gradeId, status: request.status, statusCompleted: request.statusCompleted
            , listExamIds: listExamIds, getCount: true
          }
        )
        .then((resp: AxiosResponse<PaginatorResponse<ListExamOfUser>>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }

  getQuestionData(): Promise<QuestionTestModel> {
    return new Promise((resolve, reject) => {
      axios
        .get(`/assets/fakedata/question-test.json`, {

        })
        .then((resp: AxiosResponse<QuestionTestModel>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }
  getRigthAnswerData(): Promise<QuestionTestModel> {
    return new Promise((resolve, reject) => {
      axios
        .get(`/assets/fakedata/data-reponse-exam.json`, {

        })
        .then((resp: AxiosResponse<QuestionTestModel>) => {
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }
  getTimeRemaining(): Promise<number> {
    return new Promise((resolve, reject) => {
      axios
        .get(`/assets/fakedata/data-reponse-exam.json`, {

        })
        .then((resp: AxiosResponse<number>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });

  }

  getInforExamSubjectStudentById(id: number): Promise<InforExamSubjectStudent> {
    return new Promise((resolve, reject) => {
      axios
        .get(`${this.baseUrl}/api/organizeexams/getinfoexamsubjectstudent`, {
          params: { id: id }
        })
        .then((resp: AxiosResponse<InforExamSubjectStudent>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }

  getInforExamById(id: number, subjectId: number): Promise<InforExam> {
    return new Promise((resolve, reject) => {
      axios
        .get(`${this.baseUrl}/api/exams/getinfoexambyexamid`, {
          params: { examId: id, subjectId: subjectId }
        })
        .then((resp: AxiosResponse<InforExam>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }

  getDetailInfoExam(id: number, subjectId: number, examSubjectStudentId: number): Promise<ExamDetaiInfo> {
    return new Promise((resolve, reject) => {
      axios
        .get(`${this.baseUrl}/api/doexams/getinfodoexam`, {
          params: { examId: id, subjectId: subjectId, examSubjectStudentId: examSubjectStudentId }
        })
        .then((resp: AxiosResponse<ExamDetaiInfo>) => {
          console.log(`get from json`, resp);
          resolve(resp.data);
        })
        .catch(error => {
          console.error(error);
          reject(error);
        });
    });
  }
  updateUserExercise(request: UpdateUserExerciseRequest): Promise<boolean> {
    return new Promise((resolve, reject) => {
      axios.put(`${this.baseUrl}/api/doexams/updatedoexam`, request)
        .then((resp: AxiosResponse<boolean>) => {
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }

  createTestUrl(request: CreateTestUrlRequest): Promise<boolean> {
    return new Promise((resolve, reject) => {
      axios.post(`${this.baseUrl}/api/doexams/createtesturl`, request)
        .then((resp: AxiosResponse<boolean>) => {
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }
  getUserExercise(examSubjectStudentId: number): Promise<UserExerciseResponse[]> {
    return new Promise((resolve, reject) => {
      axios.get(`${this.baseUrl}/api/doexams/${examSubjectStudentId}`).then((resp: AxiosResponse<UserExerciseResponse[]>) => {
        resolve(resp.data);
      })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }

  submitUserExercise(request: SubmitUserExerciseRequest): Promise<boolean> {
    return new Promise((resolve, reject) => {
      axios.patch(`${this.baseUrl}/api/doexams/finishdoexam`, request).then((resp: AxiosResponse<boolean>) => {
        resolve(resp.data);
      })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }
  getResultDoExam(examSubjectStudentId: number): Promise<ResultDoExamResponse> {
    return new Promise((resolve, reject) => {
      axios.get(`${this.baseUrl}/api/doexams/${examSubjectStudentId}/result`).then((resp: AxiosResponse<ResultDoExamResponse>) => {
        resolve(resp.data);
      })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }

  violationWarning(request: { contestantId: number, examId: number, subjectId: number, shiftId: number, score: number, reason: string }): Promise<boolean> {
    return new Promise((resolve, reject) => {
      axios.post(`${this.baseUrl}/api/doexams/actions/violation-warning`, request)
        .then((resp: AxiosResponse<boolean>) => {
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }

  minusScore(request: { examSubjectStudentId: number, contestantId: number, examId: number, subjectId: number, shiftId: number, score: number, reason: string }): Promise<boolean> {
    return new Promise((resolve, reject) => {
      axios.patch(`${this.baseUrl}/api/doexams/actions/minusscore`, request)
        .then((resp: AxiosResponse<boolean>) => {
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }

  suspend(request: { examSubjectStudentId: number, contestantId: number, examId: number, subjectId: number, shiftId: number, reason: string }): Promise<boolean> {
    return new Promise((resolve, reject) => {
      axios.patch(`${this.baseUrl}/api/doexams/actions/suspend`, request)
        .then((resp: AxiosResponse<boolean>) => {
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }

  /*****
   * Thu bài
   */
  teacherFinishDoExam(examSubjectStudent: number): Promise<boolean> {
    return new Promise((resolve, reject) => {
      axios.patch(`${this.baseUrl}/api/doexams/teacherfinishdoexam`, null, {
        params: {
          examSubjectStudent: examSubjectStudent
        }
      })
        .then((resp: AxiosResponse<boolean>) => {
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }

  /**
   * Thu bài tất cả học viên
   */
  finishAllDoExam(request: { listIds: number[] }): Promise<boolean> {
    return new Promise((resolve, reject) => {
      axios.patch(`${this.baseUrl}/api/doexams/actions/finishalldoexam`, request)
        .then((resp: AxiosResponse<boolean>) => {
          resolve(resp.data);
        })
        .catch((error: AxiosError) => {
          reject(error.response?.data);
        });
    });
  }
  getEssayDoExam(examSubjectStudentId: number): Promise<InfoMarkingEssay> {
    return new Promise((resolve, reject) => {
      axios.get(`${this.baseUrl}/api/doexams/${examSubjectStudentId}/essay`).then((resp: AxiosResponse<InfoMarkingEssay>) => {
        resolve(resp.data);
      }).catch(error => {
        reject(error);
      })
    });
  }
  markingEssay(request: MarkingEssayRequest): Promise<boolean> {
    return new Promise((resolve, reject) => {
      axios.patch(`${this.baseUrl}/api/doexams/actions/markingessay`, request)
        .then((resp: AxiosResponse<boolean>) => {
          resolve(resp.data);
        }).catch(error => {
          reject(error);
        })
    });
  }

}

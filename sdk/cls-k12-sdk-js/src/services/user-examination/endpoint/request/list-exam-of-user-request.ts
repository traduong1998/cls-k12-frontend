export interface ListExamOfUserRequest {
    startDate: Date;
    endDate: Date;
    gradeId: number;
    subjectId: number;
    status: string;
    statusCompleted:boolean;
    listExamIds: number[];
    examType: string;
    sortField?: string;
    sortDirection?: string;
    pageNumber?: number;
    pageSize?: number;
}
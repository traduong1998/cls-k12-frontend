export interface CreateTestUrlRequest {
    id: number;
    organizeExamId: number;
    testKitId: number;
    examId: number;
    subjectId: number;
    testId: number;
    testCodeUrl: string;
}
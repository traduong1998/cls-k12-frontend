import { UserExerciseRequest } from './update-userExercise-request';
export interface SubmitUserExerciseRequest {
    examSubjectStudentId: number;
    userExercisesUpdate: UserExerciseRequest[];
    userExercisesAdd: UserExerciseRequest[];
    isSuppend: boolean;
}
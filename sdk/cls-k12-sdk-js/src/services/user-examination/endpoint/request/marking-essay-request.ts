export interface MarkingEssayRequest{
    examSubjectStudentId:number;
    teacherMarkings:TeacherMarking[];
    isFinish:boolean;
}
export interface TeacherMarking{
    doExamId:number;
    comment:string;
    score:number;
}
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { ExamDetaiInfo } from "../model/exam-detail-info";
import { ExamInfoUserExammination } from "../model/exam-info";
import { InforExam } from "../model/info-exam";
import { InforExamSubjectStudent } from "../model/infor-exam-subject-student";
import { ListExamOfUser } from "../model/list-exam-of-user";
import { QuestionTestModel } from "../model/question-test-model";
import { ExamInfoRequest } from "./request/exam-info-request";
import { ListExamOfUserRequest } from "./request/list-exam-of-user-request";
import { ResultDoExamResponse } from "./responses/result-do-exam-response";
import { UserExerciseRequest } from "./request/update-userExercise-request";
import { InfoMarkingEssay } from "../model/info-marking-essay";
import { MarkingEssayRequest } from "./request/marking-essay-request";

export interface UserExaminationAPI {
    getQuestionData(): Promise<QuestionTestModel>;
    getRigthAnswerData(): Promise<QuestionTestModel>;
    getListExamOfUser(request: ListExamOfUserRequest): Promise<PaginatorResponse<ListExamOfUser>>;//nếu lít null khỏi gửi return null luôn [1,2]
    getListExamIdByKeyword(keyword: string): Promise<number[]>; // nếu có key word thì gửi lên
    getListExamInfo(request: ExamInfoRequest[]): Promise<ExamInfoUserExammination[]>;
    getInforExamSubjectStudentById(id: number): Promise<InforExamSubjectStudent>;
    getInforExamById(id: number, subjectId: number): Promise<InforExam>;
    getDetailInfoExam(id: number, subjectId: number, examSubjectStudentId: number): Promise<ExamDetaiInfo>;
    getResultDoExam(examSubjectStudentId: number): Promise<ResultDoExamResponse>;
    minusScore(request: { examSubjectStudentId: number, contestantId: number, examId: number, subjectId: number, shiftId: number, score: number, reason: string }): Promise<boolean>;
    suspend(request: { examSubjectStudentId: number }): Promise<boolean>;
    getEssayDoExam(examSubjectStudentId: number):Promise<InfoMarkingEssay>;
    markingEssay(request:MarkingEssayRequest):Promise<boolean>;
}
export interface UserExerciseResponse {
    questionId: number;
    userAnswers: AnswerOfUserResponse[];
    /**Đánh dấu */
    isMark: boolean;
    /**Đã trả lời */
    isAnswered: boolean;
    isEssayQuestion?: boolean;
    /**link file câu tự luận */
    fileUrl?: string;
    /**Nội dung trả lời câu tự luận */
    content?: string;
}
export interface AnswerOfUserResponse {
    answerId: number;
    answer: number;
    fillInPosition?: number;
}
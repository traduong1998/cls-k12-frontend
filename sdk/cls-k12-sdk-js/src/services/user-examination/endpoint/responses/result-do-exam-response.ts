import { UserExerciseResponse } from "./user-exercise-response";

export interface ResultDoExamResponse{
    score: number;
    realScore: number;
    scoreDecuted?: number; 
    userExercises: UserExerciseResponse[];
}
export interface Currculum {
    curriculumId: number,
    curriculumName: string,
    gradeName: string,
    subjectName: string,
    parentId: number,
    isActived: boolean,
    getActiveStatus: string;
}
export class CreateCurriculumRequest{
    curriculumName: string | undefined;    
    gradeId: number | undefined;
    subjectId: number | undefined;
    parentId?: number | undefined;
    isActive?: boolean | undefined;
}
export class EditCurriculumRequest{
    curriculumName: string | undefined;    
    curriculumId: number | undefined;  
    parentId?: number | undefined;
    isActive: boolean | undefined;
}
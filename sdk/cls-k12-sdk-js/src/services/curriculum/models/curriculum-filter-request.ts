import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class CurriculumsFilterRequests implements PaginatorRequest {
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  sortField?: string;
  sortDirection?: string;
  getCount?: boolean;
  gradeId?: number;
  divisionId?: number;
  schoolId?: number;
  subjectId?: number;
  isActive?: boolean;
}

export interface Curriculum {
    curriculumId: number;
    curriculumName : string;
    gradeName: string;
    subjectName : string;
    parentId?: number;
    isActived: boolean;
    getActiveStatus: string;
    levelManageName?: string;
}
export interface CurriculumResponse {
    curriculumId: number;
    curriculumName : string;
    gradeName: string;
    subjectName : string;
    parentId?: number;
    isActived: boolean;
    getActiveStatus: string;
}

import axios, { AxiosError, AxiosResponse } from "axios";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { CLS } from "../../../CLS";
import { ErrorResponse } from "../../../core/api/responses/ErrorResponse ";
import { Logger } from "../../../Logger";
import { Curriculum } from "../models/curriculum";
import { CurriculumsFilterRequests } from "../models/curriculum-filter-request";
import { CreateCurriculumRequest } from "../requests/create-curriculum-request";
import { EditCurriculumRequest } from "../requests/edit-curriculum-request";
import { Currculum } from "../responses/curriculum";
import { CurriculumDetailInfo } from "../responses/curriculum-detail-info";

import { CurriculumAPI } from "./curriculum-api";

export class CurriculumEndpoint implements CurriculumAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        //this.baseUrl = "http://localhost:65000";
        this.baseUrl = this.baseUrl + '/api';
    }

    getCurriculumDetailInfo(id: number): Promise<CurriculumDetailInfo> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/Curriculums/GetCurriculumDetailInfo`, {
                    params: {
                        curriculumId: id
                    }
                })
                .then((resp: AxiosResponse<CurriculumDetailInfo>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    getCurriculumsPaging(filter: CurriculumsFilterRequests): Promise<PaginatorResponse<Curriculum>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/Curriculums/GetCurriculumsPaging`, {
                    params: {
                        divisionId: filter.divisionId, schoolId: filter.schoolId, gradeId: filter.gradeId, subjectId: filter.subjectId, isActive: filter.isActive,
                        sortField: filter.sortField, sortDirection: filter.sortDirection,
                        page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<Curriculum>>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }
    createCurriculum(curriculumCreate: CreateCurriculumRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/Curriculums/Create`, curriculumCreate)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    getCurriculumChildrentNodes(id: number): Promise<CurriculumDetailInfo[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/Curriculums/GetCurriculumChildrent`, {
                    params: {
                        curriculumId: id
                    }
                })
                .then((resp: AxiosResponse<CurriculumDetailInfo[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    updateCurriculum(curriculumEdit: EditCurriculumRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/Curriculums/Update`, curriculumEdit)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    deleteCurriculum(id: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/Curriculums/Delete`, {
                    curriculumId: id
                })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    getCurriculumsBySubjectGrade(subjectId: number, gradeId: number): Promise<Curriculum[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/Curriculums/GetCurriculumsBySubjectGrade`, {
                    params: {
                        gradeId: gradeId, subjectId: subjectId
                    }
                })
                .then((resp: AxiosResponse<Curriculum[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    getComboboxCurriculumFilter(divisionId: number, schoolId: number, gradeId: number, subjectId: number): Promise<Currculum[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/Curriculums/GetComboboxCurriculumnFillter`, { params: { divisionId: divisionId, schoolId: schoolId, subjectId: subjectId, gradeId: gradeId } })
                .then((resp: AxiosResponse<Currculum[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getComboboxCurriculumCreateUpdate(divisionId: number, schoolId: number, gradeId: number, subjectId: number): Promise<Currculum[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/Curriculums/GetComboboxCurriculumnCreateUpdate`, { params: { divisionId: divisionId, schoolId: schoolId, gradeId: gradeId, subjectId: subjectId } })
                .then((resp: AxiosResponse<Currculum[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getCurriculumsFilterQuestionBank(filter: CurriculumsFilterRequests, create: boolean, divisionId: number, schoolId: number): Promise<Curriculum[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            let url = create === true ? 'Curriculums/GetComboboxCurriculumnCreateUpdate' : 'Curriculums/GetComboboxCurriculumnFillter'
            axios
                .get(`${this.baseUrl}/${url}`, {
                    params: {
                        divisionId: divisionId, schoolId: schoolId, gradeId: filter.gradeId, subjectId: filter.subjectId, isActive: filter.isActive,
                        sortField: filter.sortField, sortDirection: filter.sortDirection,
                        page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
                    }
                })
                .then((resp: AxiosResponse<Curriculum[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

}

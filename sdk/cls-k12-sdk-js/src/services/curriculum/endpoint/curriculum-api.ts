import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { Curriculum } from "../models/curriculum";
import { CurriculumsFilterRequests } from "../models/curriculum-filter-request";
import { CreateCurriculumRequest } from "../requests/create-curriculum-request";
import { CurriculumDetailInfo } from "../responses/curriculum-detail-info";


export interface CurriculumAPI {
    getCurriculumsPaging(filter: CurriculumsFilterRequests): Promise<PaginatorResponse<Curriculum>>
    getCurriculumsFilterQuestionBank(filter: CurriculumsFilterRequests, create:boolean, divisionId: number, schoolId: number): Promise<Curriculum[]>
    createCurriculum(curriculumCreate: CreateCurriculumRequest): Promise<boolean>
    getCurriculumDetailInfo(id: number): Promise<CurriculumDetailInfo>
    getCurriculumChildrentNodes(id: number): Promise<CurriculumDetailInfo[]>
    deleteCurriculum(id: number): Promise<boolean>
    getCurriculumsBySubjectGrade(subjectId: number, gradeId: number): Promise<Curriculum[]>
}

import { DivisionFromFile } from "../models/DivisionFromFile";

export interface CreateDivisionErrorResponse {
    errorCount: number;
    errorResult: ErrorResult[];
    rowData: DivisionFromFile;

}

export interface ErrorResult {
    columnError: string;
    reasonDetail: string;
    reasonType: string;
    columnIndex: number;
}
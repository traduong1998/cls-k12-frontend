import { Division } from "../models/Division";

export class UpdateDivisionRequest implements Division {
    id?: number | undefined;
    name: string| undefined;
    code?: string| undefined;
    createdDate?: Date;
    createBy?: string| undefined;
    portalId?: number| undefined;
    isDeleted?: number| undefined;
    
    constructor() {
        this.createdDate = new Date();
    }

}
import { Division } from "../models/Division";

export class DeleteDivisionRequest implements Division {
    id?: number | undefined;
    code?: string| undefined;
    name: string| undefined;
    createdDate?: Date;
    createBy?: string| undefined;
    portalId?: number| undefined;
    isDeleted?: number| undefined;
    
}
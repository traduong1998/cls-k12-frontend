import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { Division, DivisionOption, DivisionDetail } from "../models/Division";
import { DivisionFilterRequests } from "../models/DivisionPagingRequest";
import { CreateDivisionRequest } from "../requests/createDivisionRequest";
import { UpdateDivisionRequest } from "../requests/updateDivisionRequest";
import { DeleteDivisionRequest } from "../requests/deleteDivisionRequest";
import { CreateDivisionErrorResponse } from "../responses/createDivisionErrorResponse";
import { DivisionFromFile } from "../models/DivisionFromFile";


export interface DivisionAPI {

    getDivisionsPaging(filter: DivisionFilterRequests): Promise<PaginatorResponse<Division>>
    createDivision(division: CreateDivisionRequest): Promise<CreateDivisionRequest>;
    updateDivision(division: UpdateDivisionRequest): Promise<UpdateDivisionRequest>;
    deleteDivision(division: DeleteDivisionRequest): Promise<DeleteDivisionRequest>;
    countSchoolOfDivision(divisionId: number): Promise<number>;
    getDivisionOptions(): Promise<DivisionOption[]>
    getDivisionsById(id: number): Promise<DivisionDetail>
    checkNameDivision(id: number, name: string): Promise<boolean>;
    createDivisionImportFromFile(request: DivisionFromFile[]): Promise<CreateDivisionErrorResponse[]>
}

import axios, { AxiosResponse, AxiosError } from "axios";
import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import { Division, DivisionOption, DivisionDetail } from "../models/Division";
import { DivisionFilterRequests } from "../models/DivisionPagingRequest";
import { CreateDivisionRequest } from "../requests/createDivisionRequest"
import { UpdateDivisionRequest } from "../requests/updateDivisionRequest"
import { DivisionAPI } from "./DivisionAPI";
import { DeleteDivisionRequest } from "../requests/deleteDivisionRequest";
import { DivisionFromFile } from "../models/DivisionFromFile";
import { CreateDivisionErrorResponse } from "../responses/createDivisionErrorResponse";
// import { async } from "q";


export class  DivisionEndpoint implements DivisionAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config.baseUrl;
        // this.baseUrl = config.baseUrl;
    }

    getDivisionsPaging(filter: DivisionFilterRequests): Promise<PaginatorResponse<Division>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/Divisions/GetDivisionsPaging`, {
                    params: {
                        keyWord: filter.keyWord, sortField: filter.sortField, sortDirection: filter.sortDirection,
                        page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<Division>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data: null);
                });
        });
    }

    createDivision(division: CreateDivisionRequest): Promise<CreateDivisionRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Divisions/Create`, division)
                .then((resp: AxiosResponse<CreateDivisionRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data: null);
                });
        });
    }

    updateDivision(division: UpdateDivisionRequest): Promise<UpdateDivisionRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Divisions/UpdateDivision`, division)
                .then((resp: AxiosResponse<UpdateDivisionRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data: null);
                });
        });
    }
deleteDivision(division: DeleteDivisionRequest): Promise<DeleteDivisionRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Divisions/DeleteDivision`, division)
                .then((resp: AxiosResponse<DeleteDivisionRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data: null);
                });
        });
    }

    countSchoolOfDivision(divisionId : number): Promise<number>{
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/Divisions/CountSchoolOfDivision`, {
                    params: {
                        DivisionId: divisionId
                    }
                })
                .then((resp: AxiosResponse<number>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data: null);
                });
        });
    }
 getDivisionOptions(): Promise<DivisionOption[]>{
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/Divisions/GetDivisionOptions`)
                .then((resp: AxiosResponse<DivisionOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data: null);
                });
        });
    }

    getDivisionsById(id: number): Promise<DivisionDetail> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/Divisions/GetDivision`, {
                    params: {
                        Id: id
                    }
                })
                .then((resp: AxiosResponse<DivisionDetail>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data: null);
                });
        });
    } 

    checkNameDivision(id: number , name: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/Divisions/CheckDivisionName`, { params: {Id: id , Name: name } })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data: null);
                });
        }); 
    }
    createDivisionImportFromFile(request: DivisionFromFile[]): Promise<CreateDivisionErrorResponse[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/api/Divisions/CreateDivisionFromFile`, { divisionFromFile: request })
                // .post(`http://localhost:65000/api/v1/Divisions/CreateDivisionFromFile`, { divisionFromFile: request })
                .then((resp: AxiosResponse<CreateDivisionErrorResponse[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }
}

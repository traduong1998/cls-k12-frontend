export interface Division  {
    id?: number;
    name?: string;
    code?: string;
    createDate?: Date;
    createBy?: string;
    portalId?: number;
    isDeleted?: number;
}
export interface DivisionOption  {
    id?: number;
    name: string;
}

export interface DivisionDetail{
    id?: number;
    code?: string;
    name?: string;
}



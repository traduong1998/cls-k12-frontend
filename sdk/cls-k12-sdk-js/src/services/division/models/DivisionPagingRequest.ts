import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class DivisionFilterRequests implements PaginatorRequest {
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  keyWord?: string;
  sortField?: string;
  sortDirection?: string;
  getCount?: boolean;
}

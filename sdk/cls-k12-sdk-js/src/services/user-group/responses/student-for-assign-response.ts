export interface StudentForAssignResponse{
    id: number;
    fullName: string;
    username: string;
    groupStudentName: string;
    className: string;
    isAssigned: boolean;
}
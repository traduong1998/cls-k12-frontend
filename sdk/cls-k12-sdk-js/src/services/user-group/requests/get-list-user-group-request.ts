export interface GetListUserGroupRequest {
    schoolId?: number;
    divisionId?: number;
    keyWord?: string;
    page: number;
    size: number;
}
export interface AssignStudentForGroupRequest{
    userGroupId: number;
    listStudents: AssignStudentDetail[];
}
export interface AssignStudentDetail {
    id: number;
    action: string;
}
export interface GetListStudentForAssignRequest {
    divisionId? : number;
    schoolId?: number;
    gradeId?: number;
    groupStudentId?: number;
    userGroupId?: number;
    sortField?: string;
    sortDirection?: string;
    pageNumber: number | undefined;
    sizeNumber: number | undefined;
    getCount?: boolean;
}
export interface CreateUserGroupRequest{
    name: string;
    code: string;
}
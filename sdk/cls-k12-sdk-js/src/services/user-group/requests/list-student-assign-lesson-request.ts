export interface GetListStudentForAssignLessonRequest {
    lessonId: number;
    userGroupId: number;
    sortField: string;
    sortDirection: string;
    page: number;
    size: number;
    getCount: boolean;
}
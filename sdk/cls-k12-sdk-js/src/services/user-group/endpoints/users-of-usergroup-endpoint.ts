import { AssignStudentForGroupRequest } from './../requests/assign-student-for-group-request';
import axios, { AxiosResponse } from 'axios';
import { Logger, CLS } from 'cls-k12-sdk-js/src';
import { PaginatorResponse } from '../../../core/api/responses/PaginatorResponse';
import { GetListStudentForAssignRequest } from '../requests/get-list-student-for-assign-request';
import { StudentForAssignResponse } from '../responses/student-for-assign-response';
import { UsersOfUserGroupAPI } from './user-usergroup-api';
export class UsersOfUserGroupEndpoint implements UsersOfUserGroupAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = config?.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    getListStudentForAssign(request: GetListStudentForAssignRequest): Promise<PaginatorResponse<StudentForAssignResponse>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/UserGroup/list-student-assign`, {
                params: {
                    DivisionId: request.divisionId, 
                    SchoolId: request.schoolId, 
                    GradeId: request.gradeId, 
                    GroupStudentId: request.groupStudentId, 
                    UserGroupId: request.userGroupId, 
                    SortField: request.sortField, 
                    SortDirection: request.sortDirection, 
                    Page: request.pageNumber, 
                    Size: request.sizeNumber, 
                    GetCount: request.getCount
                }
            })
                .then((resp: AxiosResponse<PaginatorResponse<StudentForAssignResponse>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    assignStudents(request: AssignStudentForGroupRequest): Promise<boolean>{
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/UserGroup/assign-students`, request)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}
import axios, { AxiosResponse } from 'axios';
import { CLS, Logger } from '../../..';
import { PaginatorResponse } from '../../../core/api/responses/PaginatorResponse';
import { UserGroup } from '../models/user-group';
import { GetListStudentForAssignLessonRequest } from '../requests/list-student-assign-lesson-request';
import { StudentForAssignResponse } from '../responses/student-for-assign-response';
import { UserGroupAPI } from './user-group-api';
export class UserGroupEndpoint implements UserGroupAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = config?.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }

    getListUserGroup(): Promise<UserGroup[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/UserGroup/list-usergroups`)
                .then((resp: AxiosResponse<UserGroup[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getListUserGroupPaging(divisionId: number, schoolId: number, keyWord: string, page: number, size: number): Promise<PaginatorResponse<UserGroup>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/UserGroup/list-usergroups-paging`, { params: { divisionId: divisionId, schoolId: schoolId, keyWord: keyWord, page: page, size: size } })
                .then((resp: AxiosResponse<PaginatorResponse<UserGroup>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    addUserGroup(name: string, code: string): Promise<UserGroup> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/UserGroup/create`, { name: name, code: code })
                .then((resp: AxiosResponse<UserGroup>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    editUserGroup(userGroup: UserGroup): Promise<UserGroup> {
        
        return new Promise((resolve, reject) => {
            axios.put(`${this.baseUrl}/UserGroup/update`, userGroup)
                .then((resp: AxiosResponse<UserGroup>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    deleteUserGroup(id: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.put(`${this.baseUrl}/UserGroup/${id}/delete`)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    deleteListUserGroup(listUserGroupIdDelete: number[]): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.put(`${this.baseUrl}/UserGroup/delete-list`, { listUserGroupIdDelete: listUserGroupIdDelete })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    getListStudentForAssignLesson(request: GetListStudentForAssignLessonRequest): Promise<PaginatorResponse<StudentForAssignResponse>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/UserGroup/list-student-assign-lesson`, {
                params: {
                    lessonId: request.lessonId, userGroupId: request.userGroupId,
                    sortField: request.sortField,
                    sortDirection: request.sortDirection,
                    page: request.page,
                    size: request.size,
                    getCount: request.getCount
                }
            })
                .then((resp: AxiosResponse<PaginatorResponse<StudentForAssignResponse>>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

}
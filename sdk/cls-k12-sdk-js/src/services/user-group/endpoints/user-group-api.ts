import { UserGroup } from '../models/user-group';
import { PaginatorResponse } from './../../../core/api/responses/PaginatorResponse';
export interface UserGroupAPI {
    getListUserGroupPaging(divisionId: number, schoolId: number, keyWord: string, page: number, size: number): Promise<PaginatorResponse<UserGroup>>;
    getListUserGroup(): Promise<UserGroup[]>;
    addUserGroup(name: string, code: string): Promise<UserGroup>;
    editUserGroup(userGroup: UserGroup): Promise<UserGroup>;
    deleteUserGroup(id: number): Promise<boolean>;
}
import { PaginatorResponse } from '../../../core/api/responses/PaginatorResponse';
import { GetListStudentForAssignRequest } from '../requests/get-list-student-for-assign-request';
import { StudentForAssignResponse } from '../responses/student-for-assign-response';
export interface UsersOfUserGroupAPI {
    getListStudentForAssign(request: GetListStudentForAssignRequest): Promise<PaginatorResponse<StudentForAssignResponse>>
}
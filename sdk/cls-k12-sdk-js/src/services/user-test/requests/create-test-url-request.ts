export interface CreateTestUrlRequest {
    id: number;
    testCodeUrl: string;
}
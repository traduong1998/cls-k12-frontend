export interface UpdateUserExerciseRequest {
    userContentTestId: number;
    testId: number;
    userExercisesUpdate: UserExerciseRequest[];
    userExercisesAdd: UserExerciseRequest[];
    isFinish: boolean;
}
export interface UserExerciseRequest {
    questionId: number;
    userAnswers: AnswerOfUserRequest[];
    /**Đánh dấu */
    isMark: boolean;
    /**Đã trả lời */
    isAnswered: boolean;
    isEssayQuestion?: boolean;
    /**link file câu tự luận */
    fileUrl?: string;
    /**Nội dung trả lời câu tự luận */
    content?: string;
}
export interface AnswerOfUserRequest {
    answerId: number;
    answer: number;
    fillInPosition?: number;
}
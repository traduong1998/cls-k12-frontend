import { UserExerciseRequest } from "./update-user-excercise-request";

export interface SubmitUserExerciseRequest {
    userContentTestId: number;
    curriculumId?: number;
    userExercisesUpdate: UserExerciseRequest[];
    userExercisesAdd: UserExerciseRequest[];
}
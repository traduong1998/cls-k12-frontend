import { CreateTestUrlRequest } from "../requests/create-test-url-request";
import { UpdateUserExerciseRequest } from "../requests/update-user-excercise-request";
import { UserExerciseResponse } from "../responses/user-excercise-response";

export interface UserTestAPI {
    // getInforExamSubjectStudentById(id: number): Promise<InforExamSubjectStudent>;
    // getInforExamById(id: number, subjectId: number): Promise<InforExam>;
    // getDetailInfoExam(id: number, subjectId: number, examSubjectStudentId: number): Promise<ExamDetaiInfo>;
    // getResultDoExam(examSubjectStudentId: number): Promise<ResultDoExamResponse>;
    
    updateUserExercise(request: UpdateUserExerciseRequest): Promise<boolean>;
    createTestUrl(request: CreateTestUrlRequest): Promise<boolean>;
    getUserExercise(examSubjectStudentId: number): Promise<UserExerciseResponse[]>;
}
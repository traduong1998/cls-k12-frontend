import { UserContentTest } from './../models/user-content-test';
import axios, { AxiosResponse } from "axios";
import { CLS, Logger } from "../../..";
import { CreateTestUrlRequest } from "../requests/create-test-url-request";
import { SubmitUserExerciseRequest } from "../requests/submit-user-excercise-request";
import { UpdateUserExerciseRequest } from "../requests/update-user-excercise-request";
import { ResultDoTestResponse } from "../responses/result-do-test-response";
import { UserContentTestInfo } from "../responses/user-content-test-info-response";
import { UserExerciseResponse } from "../responses/user-excercise-response";
import { UserTestAPI } from "./user-test-api";
import { UserLesson } from '../../learner-lesson/models/user-lesson';
import { FinishDoTestResponse } from '../../learner-lesson/responses/finish-do-test-response';

export class UserTestEndpoint implements UserTestAPI {

    endpoint = '';

    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl + '/api';
    }
    updateUserExercise(request: UpdateUserExerciseRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.put(`${this.baseUrl}/dotests/actions/updatetest`, request).then((resp: AxiosResponse<boolean>) => {
                resolve(resp.data);
            }).catch(error => {
                reject(error);
            })
        });
    }

    createTestUrl(request: CreateTestUrlRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/dotests/actions/assigntesturl`, request).then((resp: AxiosResponse<boolean>) => {
                resolve(resp.data);
            }).catch(error => {
                reject(error);
            })
        });
    }
    getUserExercise(userContentTestId: number): Promise<UserExerciseResponse[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/dotests/${userContentTestId}`).then((resp: AxiosResponse<UserExerciseResponse[]>) => {
                resolve(resp.data);
            }).catch(error => {
                reject(error);
            })
        });
    }
    getResultDoTest(examSubjectStudentId: number): Promise<ResultDoTestResponse> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/dotests/${examSubjectStudentId}/result`).then((resp: AxiosResponse<ResultDoTestResponse>) => {
                resolve(resp.data);
            }).catch(error => {
                reject(error);
            })
        });
    }
    submitUserExercise(request: SubmitUserExerciseRequest): Promise<FinishDoTestResponse> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/dotests/actions/finishTest`, request).then((resp: AxiosResponse<FinishDoTestResponse>) => {
                resolve(resp.data);
            }).catch(error => {
                reject(error);
            })
        });
    }
    getDetaiTestByUserContentTestId(userContentTestId: number): Promise<UserContentTestInfo> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/dotests/${userContentTestId}/info`).then((resp: AxiosResponse<UserContentTestInfo>) => {
                resolve(resp.data);
            }).catch(error => {
                reject(error);
            })
        });
    }
    createUserContentTest(lessonId: number, contentId: number): Promise<UserContentTest> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/dotests/actions/createusercontenttest`, {lessonId: lessonId, contentId: contentId}).then((resp: AxiosResponse<UserContentTest>) => {
                resolve(resp.data);
            }).catch(error => {
                reject(error);
            })
        });
    }
    confirmResultTest(curriculumId: number, userContentTestId: number): Promise<UserLesson> {
        return new Promise((resolve, reject) => {
            axios.patch(`${this.baseUrl}/dotests/actions/confirm-result-test`, {curriculumId: curriculumId, userContentTestId: userContentTestId}).then((resp: AxiosResponse<UserLesson>) => {
                resolve(resp.data);
            }).catch(error => {
                reject(error);
            })
        });
    }
}
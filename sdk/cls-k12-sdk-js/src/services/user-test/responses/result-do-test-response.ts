import { UserExerciseResponse } from "./user-excercise-response";

export interface ResultDoTestResponse {
    score: number;
    realScore: number;
    statusOver?: string; 
    userExercises: UserExerciseResponse[];
}
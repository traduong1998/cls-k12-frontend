export interface UserContentTestInfo{
    id: number;
    name: string;
    timeRemaining: number;
    isCompleted: boolean;
    testId: number;
    contentId: number;
    lessonId: number;
    isShuffle: boolean;
    testCodeURL: string;
    displayPerPage: number;
    isPreservingTime: boolean;
    testTime?: number;
    isPublicScore: boolean;
    isPublicAnswer: boolean;
}
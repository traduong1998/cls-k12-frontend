import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS } from "../../CLS";
import { Logger } from "../../Logger";
import { StudentLearnLessonModel } from "./models/student-learn-lesson-model";
import { StudentReport } from "./models/student-report";
import { StudentChartDataReport } from "./models/student-report-chart";
import { StudentReportApi } from "./report-api";
import { ReportStudentLearnPagingRequest } from "../report-lesson/models/ReportStudentLearnPagingRequest";
import { PaginatorResponse } from "../../core/api/responses/PaginatorResponse";
import { ReportStudentPagingRequest } from "../report-lesson/models/ReportStudentPagingRequest";

export class StudentReportEndpoint implements StudentReportApi {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        //this.baseUrl = config.baseUrl;
    }

    getStudentLearnLessonReport(filter: ReportStudentLearnPagingRequest): Promise<PaginatorResponse<StudentLearnLessonModel>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/reports/student/reportStudentLearnLesson`, {
                    params: {
                        divisionId: filter.divisionId,schoolId:filter.schoolId,gradeId:filter.gradeId,classId:filter.classId,lessonId:filter.lessonId,
                        schoolYear: filter.schoolYear, studentName: filter.studentName, sortField: filter.sortField, sortDirection: filter.sortDirection,
                        page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount
                    }
                })
                .then((resp: AxiosResponse<PaginatorResponse<StudentLearnLessonModel>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    exportStudentLearnLessonReport(divisionId: number, schoolId: number, gradeId: number, classId: number, lessonId: number, schoolYear: number, studentName: string): Promise<string> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL\
            axios
                .get(`${this.baseUrl}/api/reports/student/exportExcelReportStudentLearnLesson`,
                    { params: { divisionId: divisionId, schoolId: schoolId, gradeId: gradeId, classId: classId, lessonId: lessonId, schoolYear: schoolYear, studentName: studentName }, responseType: 'blob' }
                )
                .then((resp) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getStudentReport(filter: ReportStudentPagingRequest): Promise<PaginatorResponse<StudentReport>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.baseUrl}/api/reports/student/reportStudent`, { params: { divisionId: filter.divisionId, schoolId: filter.schoolId, gradeId: filter.gradeId, classId: filter.groupStudentId, schoolYear: filter.schoolYear, studentName: filter.studentName, sortField: filter.sortField, sortDirection: filter.sortDirection,
                    page: filter.pageNumber, size: filter.sizeNumber, getCount: filter.getCount } })
                .then((resp: AxiosResponse<PaginatorResponse<StudentReport>>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    exportStudentReport(divisionId: number, schoolId: number, gradeId: number, classId: number, schoolYear: number, studentName: string, columnsDisplay?: string[]): Promise<string> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL\
            axios
                .get(`${this.baseUrl}/api/reports/student/exportExcelReportStudent`,
                //.get(`${this.baseUrl}/api/reports/student/exportExcelReportStudent`,
                    {
                        params: {
                            divisionId: divisionId, schoolId: schoolId, gradeId: gradeId, classId: classId, schoolYear: schoolYear, studentName: studentName,
                            columnsDisplay: JSON.stringify(columnsDisplay)
                        },
                        responseType: 'blob'
                    }
                )
                .then((resp) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getStudentChartDataReport(schoolId: number, divisonId: number, gradeId: number, classId: number, schoolYear: number, studentName: string, page: number, pageSize: number): Promise<StudentChartDataReport> {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.baseUrl}/api/reports/student/reportStudentDataChart`, { params: { schoolId: schoolId, divisionId: divisonId, gradeId: gradeId, classId: classId, schoolYear: schoolYear, studentName: studentName, page: page, pageSize: pageSize } })
                .then((resp: AxiosResponse<StudentChartDataReport>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
}
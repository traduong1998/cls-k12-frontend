import { StudentReport } from "./models/student-report";
import {StudentLearnLessonModel} from './models/student-learn-lesson-model'
import { StudentChartDataReport } from "./models/student-report-chart";
import { ReportStudentLearnPagingRequest } from "../report-lesson/models/ReportStudentLearnPagingRequest";
import { PaginatorResponse } from "../../core/api/responses/PaginatorResponse";
import { ReportStudentPagingRequest } from "../report-lesson/models/ReportStudentPagingRequest";

export interface StudentReportApi {
    getStudentLearnLessonReport(filter: ReportStudentLearnPagingRequest): Promise<PaginatorResponse<StudentLearnLessonModel>>
    getStudentReport(filter: ReportStudentPagingRequest): Promise<PaginatorResponse<StudentReport>>

    exportStudentReport(divisonId:number, schoolId:number,gradeId:number, classId:number, schoolYear:number, studentName:string, columnsDisplay?: string[]):Promise<string>

    exportStudentLearnLessonReport(divisonId:number, schoolId:number,gradeId:number, classId:number,lesonId:number, schoolYear:number, studentName:string):Promise<string>

    getStudentChartDataReport(schoolId: number, divisonId: number, gradeId: number, classId: number, schoolYear: number, studentName: string, page: number, pageSize: number): Promise<StudentChartDataReport>
}

export enum ColumnDisplay {
    UserName = 'UNM',
    FullName = 'FNM',
    RequestCourse = 'RQC',
    RegistedCourse = 'RGC',
    ApprovedCourse = 'APC'
}

export interface StudentReport{
    studentId: number,
    username?: string,
    studentName: string,
    schoolName: string,
    totalLessonAssign: number,
    totalLessonRegister: number,
    totalLessonApproval: number
}
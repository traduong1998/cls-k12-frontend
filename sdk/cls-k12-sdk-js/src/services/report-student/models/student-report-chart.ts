  export interface StudentChartDataReport{
    totalLessonAssign: number,
    totalLessonRegister: number,
    totalLessonApproval: number
}
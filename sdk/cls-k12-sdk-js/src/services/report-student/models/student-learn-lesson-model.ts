export interface StudentLearnLessonModel {
    studentId: number,
    username?: string,
    studentName: string,
    schoolName: string,
    gradeName: string,
    groupStudentName: string,
    percentageOfCompletion: number,
    registrationDate: Date,
    startDate: Date,
    completionDate: Date,
    result: number,
    assessmentLevelName: string,
}
export interface UserLoginRequests {
    /**
     * Tên miền
     */
    domain: string;
    userName: string;
    passWord: string;
}
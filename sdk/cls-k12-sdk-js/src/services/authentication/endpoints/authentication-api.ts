import { UserLoginRequests } from "../requests/userLoginRequests";
import { UserLoginResponse } from "../responses/userLoginResponses";

export interface AuthenticationAPI {
    login(userLoginRequests: UserLoginRequests): Promise<UserLoginResponse>;
}
import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS, Logger } from "../../..";
import { UserLoginRequests } from "../requests/userLoginRequests";
import { UserLoginResponse } from "../responses/userLoginResponses";
import { AuthenticationAPI } from "./authentication-api";

type NewType = Logger;

export class AuthenticationEndpoint implements AuthenticationAPI {
    // TODO : Endpoint :
    endpoint = 'api/v1/auth/login';

    private baseUrl: string;
    logger: NewType = Logger.getInstance();

    constructor(config: { baseUrl: string }) {
        this.baseUrl = config.baseUrl ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }

    login(request: UserLoginRequests): Promise<UserLoginResponse> {
        this.logger.warn('usertype get3');
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/auth/login`, request)
                .then((resp: AxiosResponse<UserLoginResponse>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    console.error(error);
                    reject(error.response?.data);
                });
        });
    }
}
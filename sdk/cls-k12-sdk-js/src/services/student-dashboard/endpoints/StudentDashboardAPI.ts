import { ExamInfo } from "../models/ExamInfo";
import { ExamStudent } from "../models/ExamStudent";
import { LessonInfo } from "../models/LessonInfo";
import { LessonStudent } from "../models/LessonStudent";
import { LessonStudentRegistration } from "../models/LessonStudentRegistration";
import { ExamInfoRequest } from "../requests/ExamInfoRequest";

export interface StudentDashboardAPI{
    getListLessonStudentRegistration(studentId:number):Promise<LessonStudentRegistration[]>
    getListLessonInfoByIds(LessonIdArr: number[]): Promise<LessonInfo[]>
    getListLessonsForStudentRegis(portalId: number, divisionId:number, schoolId:number): Promise<LessonInfo[]>
    getListLessonIdsNotDel(): Promise<Number[]>
    getListLessonStudent(userId: number): Promise<LessonStudent[]>
    getListExamStudent(studentId: number): Promise<ExamStudent[]> 
    getListExamInfo(request: ExamInfoRequest[]): Promise<ExamInfo[]> 
}
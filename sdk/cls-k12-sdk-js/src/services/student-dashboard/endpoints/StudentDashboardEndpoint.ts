import { LessonStudentRegistration } from "../models/LessonStudentRegistration";
import { StudentDashboardAPI } from "./StudentDashboardAPI";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import axios, { AxiosResponse } from "axios";
import { LessonInfo } from "../models/LessonInfo";
import { LessonStudent } from "../models/LessonStudent";
import { ExamStudent } from "../models/ExamStudent";
import { ExamInfoRequest } from "../requests/ExamInfoRequest";
import { ExamInfo } from "../models/ExamInfo";

export class StudentDashboardEndpoint implements StudentDashboardAPI {

    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();

    constructor(config?: { baseUrl: string }) {
        console.log(`CLS.getConfig().apiBaseUrl: `, CLS.getConfig().apiBaseUrl);
        this.baseUrl = CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }
    getListLessonStudentRegistration(studentId: number): Promise<LessonStudentRegistration[]> {
        return new Promise((resolve, reject) => {

            axios.get(`${this.baseUrl}/StudentDashboard/get-list-lesson-student-registration`, { params: { studentId: studentId } })
                // axios.get(`http://localhost:58915/api/v1/StudentDashboard/get-list-lesson-student-registration`, { params: { studentId: studentId } })
                .then((resp: AxiosResponse<LessonStudentRegistration[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getListLessonInfoByIds(lessonIds: number[]): Promise<LessonInfo[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/StudentDashboard/get-list-lesson-info`, lessonIds)
                // axios.post(`http://localhost:58915/api/v1/StudentDashboard/get-list-lesson-info`, lessonIds)
                .then((resp: AxiosResponse<LessonInfo[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    getListLessonsForStudentRegis(portalId: number, divisionId:number, schoolId:number): Promise<LessonInfo[]> {
        return new Promise((resolve, reject) => {

            axios.get(`${this.baseUrl}/StudentDashboard/get-list-lesson-info-for-student-regis`, { params: { portalId:portalId, divisionId:divisionId, schoolId:schoolId} })
                // axios.get(`http://localhost:58915/api/v1/StudentDashboard/get-list-lesson-info-for-student-regis`, { params: { portalId:portalId, divisionId:divisionId, schoolId:schoolId} })
                .then((resp: AxiosResponse<LessonInfo[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getListLessonIdsNotDel(): Promise<Number[]> {
        return new Promise((resolve, reject) => {

            axios.get(`${this.baseUrl}/StudentDashboard/get-list-lesson-ids-not-del`)
                // axios.get(`http://localhost:58915/api/v1/StudentDashboard/get-list-lesson-ids-not-del`)
                .then((resp: AxiosResponse<Number[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getListLessonStudent(userId: number): Promise<LessonStudent[]> {
        return new Promise((resolve, reject) => {

            axios.get(`${this.baseUrl}/StudentDashboard/get-list-lesson-student`, { params: { userId: userId } })
                // axios.get(`http://localhost:58915/api/v1/StudentDashboard/get-list-lesson-student`, { params: { userId: userId } })
                .then((resp: AxiosResponse<LessonStudent[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getListExamStudent(studentId: number): Promise<ExamStudent[]> {
        return new Promise((resolve, reject) => {

            axios.get(`${this.baseUrl}/StudentDashboard/get-list-exam-student`, { params: { studentId: studentId } })
                // axios.get(`http://localhost:58915/api/v1/StudentDashboard/get-list-exam-student`, { params: { studentId: studentId } })
                .then((resp: AxiosResponse<ExamStudent[]>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getListExamInfo(request: ExamInfoRequest[]): Promise<ExamInfo[]> {
        return new Promise((resolve, reject) => {
          axios
            .post(`${this.baseUrl}/StudentDashboard/get-list-exam-info`,
            // .post(`http://localhost:58915/api/v1/StudentDashboard/get-list-exam-info`,
              { listExamSubjectId: request }
            )
            .then((resp: AxiosResponse<ExamInfo[]>) => {
              console.log(`get from json`, resp);
              resolve(resp.data);
            })
            .catch(error => {
              console.error(error);
              reject(error);
            });
        });
    
      }
}
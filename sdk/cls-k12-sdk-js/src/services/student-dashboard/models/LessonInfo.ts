export interface LessonInfo {
    id?: number;
    teacherId?: number;
    name?: string;
    avatar?: string;
    createdDate?: Date;
}
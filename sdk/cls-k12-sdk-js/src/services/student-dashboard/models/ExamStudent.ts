export interface ExamStudent{
    id?: number;
    subjectId?:number;
    subjectName?:string;
    examId?:number;
    examName?:string;
    shiftName?:string;
    roomName?:string;
    examType?:string;
    testTime?:number;
    startDate?:string;
    endDate?:string;
    isCompleted?:boolean;
    status?:string;
}
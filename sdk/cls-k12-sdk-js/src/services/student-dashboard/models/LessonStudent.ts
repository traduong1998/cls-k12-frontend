export interface LessonStudent {
    id?: number;
    userId?: number;
    lessonId?: number;
    lessonName?: string;
    avatar?: string;
    assessmentLevelName?: string;
    result?: string;
    percentageOfCompletion?: number;
    registrationDate?: Date;
    completionDate?: Date;
    startDate?: Date;
    assignFrom?: string;
    isDeleted?: string;
    createdDate?: Date;
    teacherId?: number;
}
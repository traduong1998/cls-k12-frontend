export interface LessonStudentRegistration {
    id?: number;
    lessonId?: number;
    lessonName?: string;
    avatar?: string;
    schoolId?: number;
    gradeId?: number;
    groupStudentId?: number;
    studentId?: number;
    registrationDate?: Date;
    isApproval?: boolean;
    createdDate?: Date;
}

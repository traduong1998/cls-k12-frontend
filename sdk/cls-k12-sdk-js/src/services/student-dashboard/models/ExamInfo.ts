export interface ExamInfo {
    examId: number;
    name: string;
    subjectId:number;
    testTime: number;
}


export interface OnlineClasses {
    id?: number
    name?: string
    meetingId?: number
    passWord?: string
    meetingUserId?: number
    startDate?: Date
    finishDate?: Date
    joinURL?: string
    startURL?: string
    meetingType?: string
    statusOnline?: string
    description?: string
    divisionId?: number;
    schoolId?: number
}
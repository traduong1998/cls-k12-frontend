export interface MeetingRoomTeacherLogsModel {
    meetingRoomId: number
    meetingRoomName: string
    mrEndDate: Date
    mrStartDate: Date
    userId: number
    userName: string
    fullName: string
    startDate: Date
    endDate?: Date
    joinDate?: string
    portalId: number
    divisionId?: number
    divisionName?: string
    schoolId?: number
    schoolName?: string
    joinDates?: Date[]
    lastJoinDate?: Date
}

export class OnlineClassesCreateRequest {
    name?: string
    meetingId?: string
    password?: string
    meetingUserId?: string
    startDate?: Date
    finishDate?: Date
    joinURL?: string
    startUrl?:string
    meetingType?:string
}

export class OnlineClassesFilterRequest {
    fromDate?: Date
    toDate?: Date
    keyWord?: string
    status?: string
    meetingType?: string
    page?: number
    size?: number
    getCount?:boolean
    
}
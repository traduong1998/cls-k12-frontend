import axios, { AxiosError, AxiosResponse } from "axios";
import { Logger, CLS } from "../../..";
import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { OnlineClasses } from "../models/onlineClasses";
import { OnlineClassesAPI } from "./onlineClassesAPI";

export class OnlineClassesEndpoint implements OnlineClassesAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        this.baseUrl = this.baseUrl + '/api';
    }

    GetMeetingRoomsPaging(request: any): Promise<PaginatorResponse<OnlineClasses>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/MeetingRooms`, { params: request })
                .then((resp: AxiosResponse<PaginatorResponse<OnlineClasses>>) => {

                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    GetById(id: number): Promise<OnlineClasses> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/MeetingRooms/${id}`)
                // axios.get(`http://localhost:65000/api/v1/MeetingRooms`, { params: request })
                .then((resp: AxiosResponse<OnlineClasses>) => {

                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    UpdateMeetingRoom(request: any): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios
                .patch(`${this.baseUrl}/MeetingRooms`, request)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    CreateMeetingRoom(request: any): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios
                .post(`${this.baseUrl}/MeetingRooms`, request)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    deleteMeetingRoom(id: number): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/MeetingRooms/actions/delete`, { id: id })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

}
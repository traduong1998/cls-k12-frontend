import { PaginatorResponse } from "../../../core/api/responses/PaginatorResponse";
import { TeacherZoomRequest } from "../../lesson/requests/teacherZoomRequest";
import { TeacherZoomResponses } from "../../lesson/responses/teacherZoomResponses";
import { OnlineClasses } from "../models/onlineClasses";

export interface OnlineClassesAPI {
    GetMeetingRoomsPaging(request: any): Promise<PaginatorResponse<OnlineClasses>>;
    GetById(id: number): Promise<OnlineClasses>;
    UpdateMeetingRoom(request: any): Promise<boolean>
    CreateMeetingRoom(request: any): Promise<boolean>;
    deleteMeetingRoom(id: number): Promise<boolean>
}
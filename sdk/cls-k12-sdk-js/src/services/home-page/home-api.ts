import { InforUnitHome } from "./models/infor-unit-home";


export interface HomeApi {
    getUnitHomeConfig(domain: string): Promise<InforUnitHome>
}
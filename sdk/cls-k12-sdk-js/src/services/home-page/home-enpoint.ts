import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS } from "../../CLS";
import { Logger } from "../../Logger";
import { HomeApi } from "./home-api";
import { InforUnitHome } from "./models/infor-unit-home";

export class HomeEnpoint implements HomeApi {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
    }
    getUnitHomeConfig(domain: string): Promise<InforUnitHome> {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.baseUrl}/api/system/global/getunitconfigurations/${domain}`)
                .then((resp: AxiosResponse<InforUnitHome>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
}
export interface InforUnitHome {
  desktopLogo: string,
  mobileLogo: string,
  favicon: string,
  desktopBanner: string,
  mobileBanner: string,
  portalId: number,
  divisionId: number,
  schoolId: number,
  id: number,
  domainEvents: string
  unitName: string,
  address: string,
  phoneNumber: string,
  email: string,
}
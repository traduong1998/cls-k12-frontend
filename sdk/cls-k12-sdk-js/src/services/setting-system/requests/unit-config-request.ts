export interface UnitConfigRequest {
    unitName: string,
    address: string,
    phoneNumber: string,
    email: string
}
export interface EmailConfigRequest{
    userName: string,
    password: string,
    host: string,
    port: number,
    enableSsl: boolean,
    from: string,
    kichHoat: boolean
}
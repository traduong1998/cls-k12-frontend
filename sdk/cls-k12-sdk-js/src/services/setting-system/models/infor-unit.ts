export interface InforUnit {
  unitName: string,
  address: string,
  phoneNumber: string,
  email: string,
  portalId: number,
  divisionId: number,
  schoolId: number,
  id: number,
  domainEvents: null
}
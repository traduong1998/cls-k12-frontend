export interface ThemeConfig {
    desktopLogo: string,
    mobileLogo: string,
    favicon: string,
    desktopBanner: string,
    mobileBanner: string
}
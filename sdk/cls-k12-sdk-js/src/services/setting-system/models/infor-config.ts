export interface InfoConfig{
    desktopLogo: string,
    mobileLogo:string,
    favicon: string,
    desktopBanner:string,
    mobileBanner: string,
    portalId: number,
    divisionId: number,
    schoolId: number,
    id: number,
    domainEvents: string
}
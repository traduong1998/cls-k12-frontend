export interface InforEmailConfig{
    userName: string,
    password: string,
    host: string,
    port: number,
    enableSsl: boolean,
    from: string,
    kichHoat: boolean,
    portalId: number,
    divisionId: number,
    schoolId: number,
    id: number
}
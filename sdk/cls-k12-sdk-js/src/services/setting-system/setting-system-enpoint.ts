import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS } from "../../CLS";
import { Logger } from "../../Logger";
import { InfoConfig } from "./models/infor-config";
import { InforEmailConfig } from "./models/infor-email-config";
import { InforUnit } from "./models/infor-unit";
import { ThemeConfig } from "./models/theme-config";
import { EmailConfigRequest } from "./requests/email-config-request";
import { UnitConfigRequest } from "./requests/unit-config-request";
import { SettingSystemApi } from "./setting-system-api";

export class SettingSystemEnpoint implements SettingSystemApi {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
    }
    getInforTheme(): Promise<InfoConfig> {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.baseUrl}/api/themeconfiguration/getthemeconfiguration`)
                .then((resp: AxiosResponse<InfoConfig>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    createThemeConfig(themeConfig: ThemeConfig): Promise<boolean> {

        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/api/themeconfiguration/updatethemeconfiguration`, themeConfig)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    sendMail(email: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/api/emailconfiguration/sendemailconfiguration`, { To: email })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    createEmailConfig(emailConfig: EmailConfigRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/api/emailconfiguration/updateemailconfiguration`, emailConfig)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }
    createUnitConfig(unitConfig: UnitConfigRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/api/unitconfiguration/updateunitconfiguration`, unitConfig)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch(error => {
                    console.error(error);
                    reject(error);
                });
        });
    }

    getEmailConfig(): Promise<InforEmailConfig> {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.baseUrl}/api/emailconfiguration/getemailconfiguration`)
                .then((resp: AxiosResponse<InforEmailConfig>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getUnitConfig(): Promise<InforUnit> {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.baseUrl}/api/unitconfiguration/getunitconfiguration`)
                .then((resp: AxiosResponse<InforUnit>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

}
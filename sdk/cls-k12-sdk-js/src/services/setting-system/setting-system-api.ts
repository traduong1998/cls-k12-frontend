import { InforEmailConfig } from "./models/infor-email-config";
import { InforUnit } from "./models/infor-unit";
import { EmailConfigRequest } from "./requests/email-config-request";
import { ThemeConfig } from "./models/theme-config";
import { UnitConfigRequest } from "./requests/unit-config-request";
import { InfoConfig } from "./models/infor-config";

export interface SettingSystemApi {
    getUnitConfig(): Promise<InforUnit>
    getEmailConfig(): Promise<InforEmailConfig>

    getInforTheme(): Promise<InfoConfig>

    createUnitConfig(unitConfig: UnitConfigRequest): Promise<boolean>
    createEmailConfig(emailConfig: EmailConfigRequest): Promise<boolean>

    createThemeConfig(themeConfig: ThemeConfig): Promise<boolean>

    sendMail(email: string): Promise<boolean>
}
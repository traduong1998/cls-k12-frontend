import { QuestionModel } from "../../test/requests/question-model";

export interface TestKitFileUpdateRequest {
    examId: number,
    testKitId: number,
    tests: QuestionModel[]
}
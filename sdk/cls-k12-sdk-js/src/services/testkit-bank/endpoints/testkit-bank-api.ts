import { TestKitFileUpdateRequest } from "../requests/update-files-testkit";
import { TestCodeUrlResponse } from "../responses/test-code-url-response";

export interface TestKitBankApi {
    updateTestKitFile(requestData: TestKitFileUpdateRequest): Promise<boolean>;
    getTestForStudent(examId: number, testKitId: number, getTestShuffle: boolean): Promise<TestCodeUrlResponse>;
    getResultForTest(examId: number, testKitId: number, testId: number): Promise<TestCodeUrlResponse>;
}
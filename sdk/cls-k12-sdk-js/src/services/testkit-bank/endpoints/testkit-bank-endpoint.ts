import axios, { AxiosError, AxiosResponse } from "axios";
import { CLS } from "../../../CLS";
import { ErrorResponse } from "../../../core/api/responses/ErrorResponse ";
import { TestKitFileUpdateRequest } from "../requests/update-files-testkit";
import { TestCodeUrlResponse } from "../responses/test-code-url-response";
import { TestKitBankApi } from "./testkit-bank-api";



export class TestKitBankEnpoint implements TestKitBankApi {

    baseUrl: string;
    baseUrlConvertFile?: string;

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrls.testKitBank ?? CLS.getConfig().apiBaseUrl;
        this.baseUrl = this.baseUrl + '/api/v1';
        this.baseUrlConvertFile = CLS.getConfig().apiBaseUrls.questionConverter;
    }
    updateTestKitFile(requestData: TestKitFileUpdateRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            axios.post(`${this.baseUrl}/testkits-bank/actions/shuffle-test`, requestData).then((resp: AxiosResponse<boolean>) => {
                resolve(resp.data);
            }).catch(error => {
                reject(error);
            })
        });
    }
    getTestForStudent(examId: number, testKitId: number, getTestShuffle: boolean): Promise<TestCodeUrlResponse> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/testkits-bank/student/tests`, { params: { examId: examId, testKitId: testKitId, getTestShuffle: getTestShuffle } }).then((resp: AxiosResponse<TestCodeUrlResponse>) => {
                resolve(resp.data);
            }).catch(error => {
                reject(error);
            })
        });
    }
    getResultForTest(examId: number, testKitId: number, testId: number): Promise<TestCodeUrlResponse> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/testkits-bank/teacher/tests`, { params: { examId: examId, testKitId: testKitId, testId: testId } }).then((resp: AxiosResponse<TestCodeUrlResponse>) => {
                resolve(resp.data);
            }).catch(error => {
                reject(error);
            })
        });
    }

}
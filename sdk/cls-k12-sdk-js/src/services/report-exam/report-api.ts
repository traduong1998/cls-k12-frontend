import { ExamInfo, GetExamFilterRequests } from "../..";
import { PaginatorResponse } from "../../core/api/responses/PaginatorResponse";
import { ExamSchool } from "../organize-exam/models/ExamSchool";
import { ExamSchoolInfo } from "../organize-exam/models/ExamSchoolInfo";
import { ExamSubjectOrganize } from "../organize-exam/models/ExamSubjectOrganize";
import { GetListExamSchoolPagingRequest } from "../organize-exam/request/get-list-exam-school-paging-request";
import { ExamReportInfo } from "./models/exam-report-info";
import { StudentOfExamReportRequest } from "./models/request/report-exam-student-request";
import { StudentExamReport } from "./models/request/student-exam-report";



export interface ExamReportApi {

    getExamReportInfo(examId: number, subjectId: number): Promise<ExamReportInfo>;
    getStudentOfExamPaging(request: StudentOfExamReportRequest): Promise<PaginatorResponse<StudentExamReport>>;
    exportExcelStudentOfExam(request: StudentOfExamReportRequest): Promise<string>
    getExams(request: GetExamFilterRequests): Promise<PaginatorResponse<ExamInfo>>;
    getListExamSchoolPaging(request: GetListExamSchoolPagingRequest): Promise<PaginatorResponse<ExamSchool>>;
    getListSubjectReport(id:number):Promise<ExamSubjectOrganize[]>
    getListExamSchoolInfo():Promise<ExamSchoolInfo[]>;
}

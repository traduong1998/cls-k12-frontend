export interface StudentExamReport{
    fullName?: string;
    schoolName?:string;
    dayOfBirth: Date;
    email?: string;
    userName?: string;
    score: number;
    realScore: number;
    scoreDecuted: number;
    isCompleted: boolean;
    isFisnished: boolean;
    isSuspended: boolean;
    infingeTimes: number;
    getStatus: string;
}
export interface StudentOfExamReportRequest{
    divisonId:number,
    schoolId:number
    keyword: string,
    status:string,
    subjectId:number,
    examId:number,
    page: number,
    size: number,
    sortField: string,
    sortDirection: string,
}
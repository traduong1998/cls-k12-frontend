export interface ExamReportInfo{
   name:string;
   subjectName:string;
   testTime:number;
   total:number;
   totalCompleted:number;
   totalInfringe:number;
   totalSuspended:number;
}
import axios, { AxiosError, AxiosResponse } from "axios";
import { ExamInfo, GetExamFilterRequests } from "../..";
import { CLS } from "../../CLS";
import { ErrorResponse } from "../../core/api/responses/ErrorResponse ";
import { PaginatorResponse } from "../../core/api/responses/PaginatorResponse";
import { Logger } from "../../Logger";
import { ExamSchool } from "../organize-exam/models/ExamSchool";
import { ExamSchoolInfo } from "../organize-exam/models/ExamSchoolInfo";
import { ExamSubjectOrganize } from "../organize-exam/models/ExamSubjectOrganize";
import { GetListExamSchoolPagingRequest } from "../organize-exam/request/get-list-exam-school-paging-request";
import { ExamReportInfo } from "./models/exam-report-info";
import { StudentOfExamReportRequest } from "./models/request/report-exam-student-request";
import { StudentExamReport } from "./models/request/student-exam-report";
import { ExamReportApi } from "./report-api";
var qs = require('qs');
export class ExamReportEndpoint implements ExamReportApi {
    private baseUrl: string | undefined;
    logger: Logger = Logger.getInstance();

    constructor() {
        this.baseUrl = CLS.getConfig().apiBaseUrl;
    }
    exportExcelStudentOfExam(request: StudentOfExamReportRequest): Promise<string> {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.baseUrl}/api/reports/exams/exportexcel-student-of-exam-report`,
                    { params: request, responseType: 'blob' })
                .then((resp: AxiosResponse<string>) => {
                    const url = window.URL.createObjectURL(resp.data);
                    console.log("report URL: ", url);
                    resolve(url);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getStudentOfExamPaging(request: StudentOfExamReportRequest): Promise<PaginatorResponse<StudentExamReport>> {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.baseUrl}/api/reports/exams`,
                    { params: request })
                .then((resp: AxiosResponse<PaginatorResponse<StudentExamReport>>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getExamReportInfo(examId: number, subjectId: number): Promise<ExamReportInfo> {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.baseUrl}/api/reports/exams/exam-report-info`, { params: { examId: examId, subjectId: subjectId } })
                .then((resp: AxiosResponse<ExamReportInfo>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getListExamSchoolPaging(request: GetListExamSchoolPagingRequest): Promise<PaginatorResponse<ExamSchool>> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .post(`${this.baseUrl}/api/reportexams/examschool`,
                    {
                        keyword: request.keyword, type: request.type, fromDate: request.fromDate, toDate: request.toDate,
                        pageNumber: request.pageNumber, pageSize: request.pageSize, sortField: request.sortField, sortDirection: request.sortDirection
                        , gradeId: request.gradeId, createLevel: request.createLevel, listIds: request.listIds
                    }

                )
                .then((resp: AxiosResponse<PaginatorResponse<ExamSchool>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getExams(request: GetExamFilterRequests): Promise<PaginatorResponse<ExamInfo>> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/api/reportexams`, {
                params: request
            })
                .then((resp: AxiosResponse<PaginatorResponse<ExamInfo>>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError<ErrorResponse>) => {
                    reject(error.response?.data);
                });
        })
    }
    getListSubjectReport(id: number): Promise<ExamSubjectOrganize[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/reportexams/${id}/examsubjectreport`)
                .then((resp: AxiosResponse<ExamSubjectOrganize[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
    getListExamSchoolInfo(): Promise<ExamSchoolInfo[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/OrganizeExams/report/GetListExamSchoolInfo`)
                .then((resp: AxiosResponse<ExamSchoolInfo[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }
}
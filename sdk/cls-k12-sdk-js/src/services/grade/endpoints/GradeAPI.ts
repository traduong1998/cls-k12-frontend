import { PaginatorResponse } from "../../../../core/api/responses/PaginatorResponse";
import { Grade, GradeOption, GradeOfSchool } from "../models/Grade";
import { GradeFilterRequests } from "../models/GradePagingRequest";
import { CreateGradeRequest } from "../requests/createGradeRequest";
import { UpdateGradeRequest } from "../requests/updateGradeRequest";
import { DeleteGradeRequest } from "../requests/deleteGradeRequest";
import { AddGradeToSchoolRequest } from "../requests/addGradeToSchoolRequest";
import { GradeInfo } from "../requests/grade-info-request";
import { SubjectOfGrade } from "../models/subject-of-grade";
import { promises } from "dns";



export interface GradeAPI {

    getGradesPaging(): Promise<Grade[]>
    createGrade(grade: CreateGradeRequest): Promise<CreateGradeRequest>;
    updateGrade(grade: UpdateGradeRequest): Promise<UpdateGradeRequest>;
    deleteGrade(grade: DeleteGradeRequest): Promise<DeleteGradeRequest>;
    countGradeOfSchool(gradeId: number): Promise<number>;
    getGradeOptions(schoolId?: number): Promise<GradeOption[]>;
    getGradeForCombobox(userId?: number): Promise<GradeOption[]>
    getGradeOfSchool(schoolId?: number): Promise<GradeOfSchool[]>;
    addGradeToSchool(grade: AddGradeToSchoolRequest): Promise<boolean>
    getGradeInfo(gradeId: number): Promise<GradeInfo>;
    checkNameGrade(id: number, name: string): Promise<boolean>;

    getListSubjectOfGrade(ids: number[]): Promise<SubjectOfGrade[]>
}

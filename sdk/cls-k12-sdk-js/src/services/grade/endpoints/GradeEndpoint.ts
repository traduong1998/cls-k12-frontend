import axios, { AxiosResponse, AxiosError } from "axios";
import { CLS } from "../../../CLS";
import { Logger } from "../../../Logger";
import { Grade, GradeOption, GradeOfSchool } from "../models/Grade";
var qs = require('qs');
import { GradeAPI } from "./GradeAPI";
import { DeleteGradeRequest } from "../requests/deleteGradeRequest";
import { UpdateGradeRequest } from "../requests/updateGradeRequest";
import { CreateGradeRequest } from "../requests/createGradeRequest";
import { AddGradeToSchoolRequest } from "../requests/addGradeToSchoolRequest";
import { GradeInfo } from "../requests/grade-info-request";
import { SubjectOfGrade } from "../models/subject-of-grade";

export class GradeEndpoint implements GradeAPI {
    logger: Logger = Logger.getInstance();
    private baseUrl: string;

    constructor(config?: { baseUrl: string }) {
        this.baseUrl = CLS.getConfig().apiBaseUrl ?? config?.baseUrl;
        //this.baseUrl = config.baseUrl;
    }
    getListSubjectOfGrade(ids: number[]): Promise<SubjectOfGrade[]> {
        return new Promise((resolve, reject) => {
            axios
                .post(`${this.baseUrl}/api/grades/listsubjectofgrade`, { listGradeId: ids })
                .then((resp: AxiosResponse<SubjectOfGrade[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }


    getGradesPaging(): Promise<Grade[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/Grades/GetGradesPaging`, {
                })
                .then((resp: AxiosResponse<Grade[]>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    createGrade(grade: CreateGradeRequest): Promise<CreateGradeRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Grades/CreateGrade`, grade)
                .then((resp: AxiosResponse<CreateGradeRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    updateGrade(grade: UpdateGradeRequest): Promise<UpdateGradeRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Grades/UpdateGrade`, grade)
                .then((resp: AxiosResponse<UpdateGradeRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    deleteGrade(grade: DeleteGradeRequest): Promise<DeleteGradeRequest> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Grades/DeleteGrade`, grade)
                .then((resp: AxiosResponse<DeleteGradeRequest>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    countGradeOfSchool(gradeId: number): Promise<number> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios
                .get(`${this.baseUrl}/api/Schools/CountGradeOfSchool`, {
                    params: {
                        GradeId: gradeId
                    }
                })
                .then((resp: AxiosResponse<number>) => {
                    console.log(`get from json`, resp);
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getGradeOptions(schoolId?: number): Promise<GradeOption[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/Grades/GetGradeOptions`, { params: { schoolId: schoolId } })
                .then((resp: AxiosResponse<GradeOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getGradeForCombobox(userId?: number): Promise<GradeOption[]> {
        return new Promise((resolve, reject) => {
            axios.get(`${this.baseUrl}/api/Grades/getgradeforcombobox`, { params: { userId: userId } })
                .then((resp: AxiosResponse<GradeOption[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getGradeOfSchool(schoolId?: number): Promise<GradeOfSchool[]> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/Schools/GetGradeOfSchoolAsync`, { params: { schoolId: schoolId } })
                .then((resp: AxiosResponse<GradeOfSchool[]>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    addGradeToSchool(grade: AddGradeToSchoolRequest): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.post(`${this.baseUrl}/api/Schools/AddGradeToSchool`, grade)
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }

    getGradeInfo(id: number): Promise<GradeInfo> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/Grades/GetGrade`, { params: { Id: id } })
                .then((resp: AxiosResponse<GradeInfo>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response?.data);
                });
        });
    }

    checkNameGrade(id: number, name: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            // TODO : pass URL
            axios.get(`${this.baseUrl}/api/Grades/CheckGradeName`, { params: { Id: id, Name: name } })
                .then((resp: AxiosResponse<boolean>) => {
                    resolve(resp.data);
                })
                .catch((error: AxiosError) => {
                    reject(error.response != null ? error.response.data : null);
                });
        });
    }


}

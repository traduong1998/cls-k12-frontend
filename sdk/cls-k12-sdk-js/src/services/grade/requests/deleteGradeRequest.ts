import { Grade } from "../models/Grade";

export class DeleteGradeRequest implements Grade {
    id?: number | undefined;
}
export class AddGradeToSchoolRequest {
    schoolId?: number | undefined;
    gradeId?: number[] | undefined;
}

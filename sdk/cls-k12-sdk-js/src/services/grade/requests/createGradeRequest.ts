import { Grade } from "../models/Grade";

export class CreateGradeRequest implements Grade {
    id?: number | undefined;
    name?: string | undefined;
    orderNumber?: number | undefined;
    schoolId?: number | undefined
    createdDate?: Date;
    createBy?: string| undefined;
    portalId?: number| undefined;
    isDeleted?: number| undefined;
    
    constructor() {
        this.createdDate = new Date();
    }

}

import { Grade } from "../models/Grade";

export class UpdateGradeRequest implements Grade {
    id?: number | undefined;
    name?: string | undefined;
    schoolId?: number | undefined
    orderNumber?: number| undefined;
    createdDate?: Date;
    createBy?: string| undefined;
    portalId?: number| undefined;
    isDeleted?: number| undefined;
    
}
export interface GradeInfo{
    gradeId:number,
    gradeName:string,
    orderNumber:number
}
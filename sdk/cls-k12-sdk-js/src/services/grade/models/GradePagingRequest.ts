import { PaginatorRequest } from "../../../../core/api/requests/PaginatorRequest ";

export class GradeFilterRequests implements PaginatorRequest {
  pageNumber: number | undefined;
  sizeNumber: number | undefined;
  schoolId?: number;
  sortField?: string;
  sortDirection?: string;
  getCount?: boolean;
}

export interface Grade {
    id?: number;
    name?: string;
    orderNumber?: number;
    schoolId?: number;
    portalId?: number;
    isDeleted?: number;
    createBy?: string;
    createdDate?: Date;
}

export interface GradeOption {
    id: number;
    name: string;
    orderNumber?:number;
}

export interface GradeOfSchool{
    id: number;
}
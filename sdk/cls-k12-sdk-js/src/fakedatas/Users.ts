import { User } from "../services/user/models/User";

export const Users_DATA: User[] = [
  { id: 1, name: 'Hydrogen', username: 'us11111', userTypeName: 'Học sinh', schoolName: 'Trường THPT Hoàng Diệu', getStatus: 'Đã kích hoạt', getActivedDate: '20/10/2021', getDayOfBirth: '' },
  { id: 2, name: 'Helium', username: 'us11112', userTypeName: 'Học sinh', schoolName: 'Trường THPT Lý Tự Trọng', getStatus: 'Đã kích hoạt', getActivedDate: '20/10/2021', getDayOfBirth: '' },
  { id: 3, name: 'Lithium', username: 'us11113', userTypeName: 'Học sinh', schoolName: 'Trường THPT Lý Tự Trọng', getStatus: 'Đã kích hoạt', getActivedDate: '20/10/2021', getDayOfBirth: '' },
  { id: 4, name: 'Beryllium', username: 'us11114', userTypeName: 'Học sinh', schoolName: 'Trường THPT Lý Tự Trọng', getStatus: 'Đã kích hoạt', getActivedDate: '20/10/2021', getDayOfBirth: '' },
  { id: 5, name: 'Boron', username: 'us11115', userTypeName: 'Học sinh', schoolName: 'Trường THPT Lý Tự Trọng', getStatus: 'Đã kích hoạt', getActivedDate: '20/10/2021', getDayOfBirth: '' },
  { id: 6, name: 'Carbon', username: 'us11116', userTypeName: 'Học sinh', schoolName: 'Trường THPT Lý Tự Trọng', getStatus: 'Đã kích hoạt', getActivedDate: '20/10/2021', getDayOfBirth: '' },
  { id: 7, name: 'Nitrogen', username: 'us11117', userTypeName: 'Học sinh', schoolName: 'Trường THPT Lý Tự Trọng', getStatus: 'Đã kích hoạt', getActivedDate: '20/10/2021', getDayOfBirth: '' },
  { id: 8, name: 'Oxygen', username: 'us11118', userTypeName: 'Quản trị viên', schoolName: 'Trường THPT Lý Tự Trọng', getStatus: 'Đã kích hoạt', getActivedDate: '20/10/2021', getDayOfBirth: '' },
  { id: 9, name: 'Fluorine', username: 'us11119', userTypeName: 'Giáo viên', schoolName: 'Trường THPT Lý Tự Trọng', getStatus: 'Đã kích hoạt', getActivedDate: '20/10/2021', getDayOfBirth: '' },
  { id: 10, name: 'Neon', username: 'us111120', userTypeName: 'Giáo viên', schoolName: 'Trường THPT Lý Tự Trọng', getStatus: 'Đã kích hoạt', getActivedDate: '20/10/2021', getDayOfBirth: '' },
];

export interface ErrorResponse {
    statusCode: number;
    title: string;
    errorDetail: string;
    errorCode: string;
    enstance: string;
}
/**Vai trò kiểu người dùng */
export enum UserTypeRoles {
    SuperAdmin = 'SUPER',
    Admin = 'ADMIN',
    Teacher = 'TEACHER',
    Student = 'STUDENT'
}
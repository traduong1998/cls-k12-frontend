import { Enumeration } from "../../domain/seed-work/enumeration";

/**
* Các module trong hệ thống
*/
export class CLSModules extends Enumeration {
    /**Phòng */
    public static Division = new CLSModules(1, 'DIV');
    /**Trường */
    public static School = new CLSModules(2, 'SCH');
    /**Người dùng */
    public static User = new CLSModules(3, 'USE');
    /**Lớp */
    public static Class = new CLSModules(3, 'CLA');
    /**Khối */
    public static Grade = new CLSModules(4, 'GRA');
    /**Môn */
    public static Subject = new CLSModules(5, 'SUB');
    /**Khung chương trình */
    public static Curriculum = new CLSModules(6, 'CUR');
    /**Báo cáo */
    public static Report = new CLSModules(7, 'REP');
    /**Cài đặt cổng */
    public static SettingPortal = new CLSModules(8, 'SEP');
    /**Bài giảng */
    public static Lesson = new CLSModules(9, 'LES');
    /**Kỳ thi */
    public static Exam = new CLSModules(10, 'EXA');
    public static QuestionBank = new CLSModules(11, 'QEB');
    /**Lộ trình đào tạo */
    public static LearningPath = new CLSModules(12, 'LEP');
    /**Nhóm học sinh */
    public static GroupStudent = new CLSModules(13, 'STG');
    /**Tổ chức thi */
    public static OrganizeExam = new CLSModules(14, 'ORE');
    /**Kiểu người dùng */
    public static UserType = new CLSModules(15, 'UST');
    /**Lớp học tương tác */
    public static MeetingRoom = new CLSModules(16, 'MTR');

    constructor(public id: number, public name: string) {
        super(id, name);
    }
}
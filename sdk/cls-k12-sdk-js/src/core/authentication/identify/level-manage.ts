/**Phân cấp */
export enum LevelManages {
    /**Cấp sở */
    Department = 'DPM',
    /**Cấp phòng */
    Division = 'DVS',
    /**Cấp trường */
    School = 'SCH'
}
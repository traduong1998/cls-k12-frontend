/** Thông tin quyền module */
export interface PermissionModuleInfo {
    /**Key module */
    module: string;

    /**Tổng giá trị quyền */
    value: number;
}
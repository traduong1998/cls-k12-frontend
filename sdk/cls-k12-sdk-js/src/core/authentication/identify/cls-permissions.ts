import { Enumeration } from "../../domain/seed-work/enumeration";

/**
 * Các quyền trong hệ thống
 */
export class CLSPermissions extends Enumeration {
    public value: number;

    public static View = new CLSPermissions(1, 'VIW', 1);
    public static Add = new CLSPermissions(2, 'ADD', 2);
    public static Edit = new CLSPermissions(3, 'EDI', 4);
    public static Delete = new CLSPermissions(4, 'DEL', 8);
    public static Clone = new CLSPermissions(5, 'CLO', 16);
    public static Approved = new CLSPermissions(6, 'APR', 32);
    public static Assign = new CLSPermissions(7, 'ASG', 64);

    constructor(id: number, name: string, value: number) {
        super(id, name);

        this.value = value;
    }

    /**
     * Kiểm tra quyền
     * @param modulePermissionValue tổng giá trị các quyền của module
     * @param permission quyền cần kiểm tra
     * @returns 
     */
    public static hasPermission(modulePermissionValue: number, permission: CLSPermissions) {
        return (modulePermissionValue & permission.value) > 0;
    }
}
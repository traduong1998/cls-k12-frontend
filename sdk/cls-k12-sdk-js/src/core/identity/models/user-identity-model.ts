import { CLSModules } from "../../authentication/identify/cls-modules";
import { CLSPermissions } from "../../authentication/identify/cls-permissions";
import { PermissionModuleInfo } from "../../authentication/identify/permission-module-info";
import { LevelManages } from "../../authentication/identify/level-manage";
import { UserTypeRoles } from "../../authentication/identify/usertype-roles";

export interface InfoIdentity {
    version: number,
    userId: number,
    email: string,
    fullName: string,
    avatar: string,
    portalId: number,
    divisionId?: number,
    schoolId?: number,
    deploymentMethod: string,
    levelManage: LevelManages,
    userTypeRole: UserTypeRoles,
    permissionValue: string,
    getPermissionModules(): PermissionModuleInfo[],
    getPermissionModule(moduleName: string): PermissionModuleInfo | undefined,
    parsingPermission(): PermissionModuleInfo[],
    hasPermission(module: CLSModules, permission: CLSPermissions): boolean
}

export class UserIdentity implements InfoIdentity {
    version!: number;
    userId!: number;
    email!: string;
    fullName!: string;
    avatar!: string;
    portalId!: number;
    divisionId?: number;
    schoolId?: number;
    deploymentMethod!: string;
    levelManage: LevelManages;
    userTypeRole: UserTypeRoles;
    permissionValue!: string;

    constructor() {
        this.userTypeRole = UserTypeRoles.Student;
        this.levelManage = LevelManages.School;
    }

    getPermissionModules(): PermissionModuleInfo[] {
        return this.parsingPermission();
    }

    getPermissionModule(moduleName: string): PermissionModuleInfo | undefined {
        let permissionModules = this.parsingPermission();
        var module = permissionModules.find(x => x.module == moduleName);
        if (!module) {
            return undefined;
        }

        return module;
    }

    parsingPermission(): PermissionModuleInfo[] {
        let result: PermissionModuleInfo[] = JSON.parse(this.permissionValue);
        return result;
    }

    /**
     * Kiểm tra quyền
     * @param modulePermissionValue tổng giá trị các quyền của module
     * @param permission quyền cần kiểm tra
     * @returns 
     */
    public hasPermission(module: CLSModules, permission: CLSPermissions): boolean {
        let moduleInfo = this.getPermissionModule(module.name);
        let moduleValue = moduleInfo ? moduleInfo.value : 0;
        return CLSPermissions.hasPermission(moduleValue, permission);
    }
}

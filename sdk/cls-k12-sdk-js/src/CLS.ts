import axios from "axios";

export interface IOptionInit {
    /**Baseurl api */
    apiBaseUrl: string;
    /**Base url chi tiết cho các endpoint, mặc định sử dụng apiBaseUrl (gateway) */
    apiBaseUrls: {
        fileServer?: string;
        testKitBank?: string;
        questionConverter?: string;
        examSignal?: string;
        fileServerConvert?: string;
    };
    /**api key */
    apiKey: string;
    /**token */
    token: string;
}
export class CLS {
    private static options: IOptionInit = {
        apiBaseUrl: '',
        apiBaseUrls: {
            fileServer: undefined,
            testKitBank: undefined,
            questionConverter: undefined,
            examSignal: undefined,
            fileServerConvert: undefined,
        },
        apiKey: '',
        token: ''
    };

    constructor(options: IOptionInit) {
        CLS.options = this.extendObject([], CLS.options, options);
        console.warn(`clsk12: constructor options`, CLS.options);
    }

    public init() {
        axios.interceptors.request.use((config) => {
            const token = localStorage.getItem('auth');
            if (token) {
                config.headers['Authorization'] = 'Bearer ' + token;
            }
            return config;
        }, (error) => {
            // Do something with request error
            return Promise.reject(error);
        });
    }

    public static getConfig() {
        return CLS.options;
    }

    public setToken(token: string) {
        CLS.options.token = token;
    }

    /**
     * extend object
     * @param target 
     * @param sources 
     */
    private extendObject(target: any, ...sources: any[]) {
        sources.forEach(function (source) {
            for (var prop in source) {
                target[prop] = source[prop];
            }
        });
        return target;
    }
}
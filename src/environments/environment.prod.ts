export const environment = {
  production: true,
  apiBaseUrl: 'http://113.160.225.134',
  apiBaseUrls: {
    fileServer: 'http://113.160.225.134:81',
    fileServerConvert: 'https://convertfiletg.cls.vn:8860',
    testKitBank: 'http://113.160.225.134:82',
    questionConverter: 'http://question-converter.clsk12.vn:8888',
    examSignal: 'http://113.160.225.134:83'
  },
  gaTrackingId: ''
};
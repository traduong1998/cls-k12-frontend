export const environment = {
  production: true,
  apiBaseUrl: 'https://gatewayk12demo.cls.vn:8080',
  apiBaseUrls: {
    fileServer: 'https://fileserverk12demo.cls.vn:8081',
    fileServerConvert: 'https://convertfilek12demo.cls.vn:8082',
    testKitBank: 'https://testkitbankk12demo.cls.vn:8083',
    questionConverter: 'https://convertquestionbank.tiengianglms.edu.vn',//'https://convertquestionbankk12demo.cls.vn:8084',
    examSignal: 'https://signalk12demo.cls.vn:8080'
  },
  gaTrackingId: ''//tạm thời dùng của BP
};
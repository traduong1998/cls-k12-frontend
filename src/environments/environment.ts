// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiBaseUrl: 'https://gatewaytg.cls.vn:7010',
  apiBaseUrls: {
    fileServer: 'https://fileservertg.cls.vn:4000',
    fileServerConvert: 'https://convertfiletg.cls.vn:8860',
    testKitBank: 'https://testkitbanktg.cls.vn:4001',
    questionConverter: 'https://convertquestionbanktg.cls.vn:5556',
    examSignal: 'https://signaltg.cls.vn:8083'
  },
  gaTrackingId: ''
};

// export const environment = {
//   production: false,
//   apiBaseUrl: 'http://113.160.225.134',
//   apiBaseUrls: {
//     fileServer: 'http://113.160.225.134:81',
//     fileServerConvert: 'https://convertfiletg.cls.vn:8860',
//     testKitBank: 'http://113.160.225.134:82',
//     questionConverter: 'http://question-converter.clsk12.vn:8888',
//     examSignal: 'http://113.160.225.134:83'
//   }
// };

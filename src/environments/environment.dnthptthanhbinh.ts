export const environment = {
  production: true,
  apiBaseUrl: 'https://gatewayk12.dongnailms.edu.vn',
  apiBaseUrls: {
    fileServer: 'https://fileserverk12.dongnailms.edu.vn:8081',
    fileServerConvert: 'https://convertfilek12.dongnailms.edu.vn:8082',
    testKitBank: 'https://testkitbankk12.dongnailms.edu.vn',
    questionConverter: 'https://convertquestionbankk12.dongnailms.edu.vn:8084',
    examSignal: 'https://signalk12.dongnailms.edu.vn'
  },
  gaTrackingId: ''
};
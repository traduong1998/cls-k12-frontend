export const environment = {
  production: true,
  apiBaseUrl: 'https://gatewayk12.binhphuoc.edu.vn',
  apiBaseUrls: {
    fileServer: 'https://fileserverk12.binhphuoc.edu.vn',
    fileServerConvert: 'https://convertfilek12.binhphuoc.edu.vn',
    testKitBank: 'https://testkitbankk12.binhphuoc.edu.vn',
    questionConverter: 'https://convertquestionbankk12.binhphuoc.edu.vn',
    examSignal: 'https://signalk12.binhphuoc.edu.vn'
  },
  gaTrackingId: 'G-N88NX1K9J0'
};
export const environment = {
  production: true,
  apiBaseUrl: 'https://gateway.tiengianglms.edu.vn',
  apiBaseUrls: {
    fileServer: 'https://fileserver.tiengianglms.edu.vn',
    fileServerConvert: 'https://convertfile.tiengianglms.edu.vn',
    testKitBank: 'https://testkitbank.tiengianglms.edu.vn',
    questionConverter: 'https://convertquestionbank.tiengianglms.edu.vn',
    examSignal: 'https://signal.tiengianglms.edu.vn'
  },
  gaTrackingId: 'G-2YL901X6HK'
};
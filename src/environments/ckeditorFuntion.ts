export function getSelectionHTML(selection) {
    let range = (document.all ? selection.createRange() : selection.getRangeAt(selection.rangeCount - 1).cloneRange());
    if (document.all) {
        return range.htmlText;
    }
    else {
        let clonedSelection = range.cloneContents();
        let div = document.createElement('div');
        div.appendChild(clonedSelection);
        return div.innerHTML;
    }
  }
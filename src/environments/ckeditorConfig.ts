export const ckConfig = {
  toolbarAnswer: [
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
    { name: 'links', groups: [ 'links' ] },
    { name: 'insert', groups: [ 'insert' ] },
    { name: 'tools', groups: [ 'tools' ] },
    { name: 'others', groups: [ 'others' ] },
    ],
  apiBaseUrl: '',
  toolbarQuestion:[
      { name: "styles", items: ['Format', 'Font', 'FontSize', "-", "TextColor", "BGColor"] },
      { name: "basicstyles", items: ["Bold", "Italic", "Underline", "RemoveFormat"] },
      { name: "justify", items: ["JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock"] },
      { name: "paragraph", items: ["NumberedList", "BulletedList", "-", "Outdent", "Indent", "-", "Blockquote"] },
      { name: "links", items: ["Link", "Unlink"] },
      { name: "insert", items: ["Audio","Video", "Table", "HorizontalRule", "Smiley", "SpecialChar"] },
      { name: "document", items: ["Source"] },
    ],
    // extraPlugins:['insertaudio,insertimage,insertvideo,ckeditor_wiris'],
    extraPlugins:['ckeditor_wiris','divarea'],
    toolbarGroups:[
  { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
  { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
  { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
  { name: 'forms', groups: [ 'forms' ] },
  { name: 'links', groups: [ 'links' ] },
  { name: 'insert', groups: [ 'insert' ] },
  { name: 'tools', groups: [ 'tools' ] },
  '/',
    

  { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
  { name: 'others', groups: [ 'others' ] },
  { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
  { name: 'styles', groups: [ 'styles' ] },
  { name: 'colors', groups: [ 'colors' ] },
  // { name: 'others', groups: [ 'others' ] },
  { name: 'about', groups: [ 'about' ] }
  ],

  removeButtons : 'Paste,Image,Source,Save,Templates,NewPage,ExportPdf,Preview,Print,Find,SelectAll,Replace,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Flash,Smiley,PageBreak,Iframe,CopyFormatting,BidiLtr,BidiRtl,Language,JustifyBlock,JustifyRight,JustifyCenter,JustifyLeft,CreateDiv,Font,FontSize,ShowBlocks,TextColor,BGColor,About,Maximize',
  removeButtonsFillBlank : 'Paste,Image,Source,Save,Templates,NewPage,ExportPdf,Preview,Print,Find,SelectAll,Replace,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Flash,Smiley,PageBreak,Iframe,CopyFormatting,BidiLtr,BidiRtl,Language,JustifyBlock,JustifyRight,JustifyCenter,JustifyLeft,CreateDiv,ShowBlocks,About,Maximize',
  removeButtonsAnswer:'Paste,Source,NewPage,Save,Templates,ExportPdf,Preview,Print,Cut,Undo,Redo,Paste,Copy,PasteFromWord,PasteText,Replace,Find,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Button,Select,HiddenField,ImageButton,CopyFormatting,RemoveFormat,Indent,Outdent,CreateDiv,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,Language,BidiRtl,BidiLtr,Image,Flash,Iframe,PageBreak,Smiley,Styles,Format,Font,FontSize,TextColor,ShowBlocks,Maximize,About,BGColor',
  removeButtonsUnderline : 'Paste,Underline,Image,Source,Save,Templates,NewPage,ExportPdf,Preview,Print,Find,SelectAll,Replace,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Flash,Smiley,PageBreak,Iframe,CopyFormatting,BidiLtr,BidiRtl,Language,JustifyBlock,JustifyRight,JustifyCenter,JustifyLeft,CreateDiv,Font,FontSize,ShowBlocks,TextColor,BGColor,About,Maximize',
  readOnly:true
};

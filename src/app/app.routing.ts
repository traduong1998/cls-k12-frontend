import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardTypes } from './core/enums/dashboard-types';
import { AuthGuard } from './core/guards';
import { ZoomMeetingComponent } from './features/learner-lesson/views/zoom-meeting/zoom-meeting.component';
import { TeachingZoomComponent } from './features/online-teaching/views/teaching-zoom/teaching-zoom.component';
import { AppBlankComponent } from './layouts/blank/blank.component';

export const AppRoutes: Routes = [
    {
        path: '',
        component: AppBlankComponent,
        children: [
            {
                path: '',
                loadChildren: () => import('./layouts/home-layouts/education/education.module').then(m => m.EducationModule)
            },
            {
                path: 'authentication',
                loadChildren: () => import('./features/authentication/authentication.module').then(m => m.AuthenticationModule)
            }
        ]
    },
    {
        path: 'dashboard',
        canActivate: [AuthGuard],
        loadChildren: () => import('./layouts/manage-layouts/manage/manage-layout.module').then(m => m.ManageLayoutModule)
    },
    {
        path: 'learner',
        loadChildren: () => import('./layouts/manage-layouts/manage/manage-layout.module').then(m => m.ManageLayoutModule)
    },
    {
        path: 'learn-online/lessons/:lessonId/contents/:id/zoomMeetings/:meetingId',
        component: ZoomMeetingComponent
    }, {
        path: 'online-teaching/:lessonId/contents/:id/zoomMeetings/:meetingId',
        component: TeachingZoomComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(AppRoutes, {
            initialNavigation: 'enabled'
        })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }

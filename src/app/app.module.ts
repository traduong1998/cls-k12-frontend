import { CLS } from 'cls-k12-sdk-js/src/CLS';

import { APP_INITIALIZER, InjectionToken, NgModule } from '@angular/core';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { CoreModule } from './core/core.module';
import { AppConfigService } from './core/services';
import { SpinnerModule } from './core/modules/spinner/spinner.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { environment } from 'src/environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SpinnerType } from './shared/enums/spinner-type';
import { Spinner } from 'ngx-spinner';
import { DEFAULT_NGXSPINNER } from './shared/modules/spinner/config';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 2,
  wheelPropagation: true
};

const DEFAULT_NGXSPINNERCONFIG: Spinner = {
  type: SpinnerType.ballScaleMultiple,
  size: 'medium',
  bdColor: 'rgb(251, 251, 251)',
  color: '#43a047',
  fullScreen: false
  // template: "<img src='https://media.giphy.com/media/o8igknyuKs6aY/giphy.gif' />"
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    CoreModule,
    PerfectScrollbarModule,
    SpinnerModule,
    NgxPermissionsModule.forRoot(),
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    {
      provide: CLS, useFactory: () =>
        new CLS({
          apiBaseUrl: environment.apiBaseUrl,
          apiBaseUrls: {
            fileServer: environment.apiBaseUrls.fileServer,
            fileServerConvert: environment.apiBaseUrls.fileServerConvert,
            testKitBank: environment.apiBaseUrls.testKitBank,
            questionConverter: environment.apiBaseUrls.questionConverter,
            examSignal: environment.apiBaseUrls.examSignal
          },
          apiKey: '',
          token: ''
        })
    },
    {
      provide: APP_INITIALIZER, useFactory: AppInitializeService, deps: [AppConfigService], multi: true
    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {
      provide: DEFAULT_NGXSPINNER,
      useValue: DEFAULT_NGXSPINNERCONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


/**
 * Service khởi tạo lúc khởi tạo hệ thống
 * @param configService 
 * @returns 
 */
export function AppInitializeService(configService: AppConfigService) {
  return (): Promise<any> => {
    return configService.init();
  }
}
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleConstant } from 'src/app/core/enums/role.constant';
import { AuthGuard } from 'src/app/core/guards';
import { ManageLayoutComponent } from './manage-layout.component';

const routes: Routes = [
  {
    path: '',
    component: ManageLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('../../../features/user-dashboard/user-dashboard.module').then(m => m.UserDashboardModule),
        pathMatch: 'full'
      },
      {
        path: 'interactive-class',
        loadChildren: () => import('../../../features/interactive-class/interactive-class.module').then(m => m.InteractiveClassModule)
      },
      {
        path: 'errors',
        loadChildren: () => import('../../../features/dashboard-error/dashboard-error.module').then(m => m.DashboardErrorModule)
      },
      {
        path: 'questionbanks',
        loadChildren: () => import('../../../features/question-banks/question-banks.module').then(x => x.QuestionBanksModule)
      },
      {
        path: 'learn',
        // loadChildren: () => import('../../../features/question-banks/question-banks.module').then(x => x.QuestionBanksModule)
        loadChildren: () => import('../../../features/learn/learn.module').then(x => x.LearnModule)
      },
      {
        path: 'user-examination',
        loadChildren: () => import('../../../features/user-examination/user-examination.module').then(x => x.UserExaminationModule)
      },
      {
        path: 'learning-path',
        loadChildren: () => import('../../../features/learning-path/learning-path.module').then(x => x.LearningPathModule)
      },
      {
        path: 'user-learning-path',
        loadChildren: () => import('../../../features/user-learning-path/user-learning-path.module').then(x => x.UserLearningPathModule)
      },
      {
        path: 'examination-manager',
        loadChildren: () => import('../../../features/examination-manager/examination-manager.module').then(x => x.ExaminationManagerModule)
      },
      {
        path: 'manage-users',
        loadChildren: () => import('../../../features/manage-users/manage-users.module').then(x => x.ManageUsersModule)
      },
      {
        path: 'manage-usertypes',
        loadChildren: () => import('../../../features/manage-usertypes/manage-usertypes.module').then(x => x.ManageUsertypesModule)
      },
      {
        path: 'manage-divisions',
        loadChildren: () => import('../../../features/manage-divisions/manage-divisions.module').then(x => x.ManageDivisionsModule)
      },
      {
        path: 'manage-schools',
        loadChildren: () => import('../../../features/manage-schools/manage-schools.module').then(x => x.ManageSchoolsModule)
      },
      {
        path: 'manage-grades',
        loadChildren: () => import('../../../features/manage-grades/manage-grades.module').then(x => x.ManageGradesModule)
      },
      {
        path: 'manage-groupstudents',
        loadChildren: () => import('../../../features/manage-groupstudents/manage-groupstudents.module').then(x => x.ManageGroupstudentsModule)
      },
      {
        path: 'manage-subjects',
        loadChildren: () => import('../../../features/manage-subjects/manage-subjects.module').then(x => x.ManageSubjectsModule)
      },
      {
        path: 'manage-curriculums',
        loadChildren: () => import('../../../features/manage-curriculums/manage-curriculums.module').then(x => x.ManageCurriculumsModule)
      },
      {
        path: 'user-learn',
        loadChildren: () => import('../../../features/user-learn/user-learn.module').then(x => x.UserLearnModule)
      },
      {
        path: 'user-infor',
        loadChildren: () => import('../../../features/user-information/user-information.module').then(x => x.UserInformationModule)
      },
      {
        path: 'manage-report',
        loadChildren: () => import('../../../features/manage-report/manager-report.module').then(x => x.ManagerReportModule)
      },
      {
        path: 'question-bank',
        loadChildren: () => import('../../../features/manage-question-bank/manage-question-bank.module').then(x => x.ManageQuestionBankModule)
      },
      {
        path: 'online-teaching',
        loadChildren: () => import('../../../features/online-teaching/online-teaching.module').then(x => x.OnlineTeachingModule)
      },
      {
        path: 'troubleshooting',
        loadChildren: () => import('../../../features/troubleshooting/troubleshooting.module').then(x => x.TroubleshootingModule)
      },
      {
        path: 'setting-system',
        loadChildren: () => import('../../../features/setting-system/setting-system.module').then(x => x.SettingSystemModule)
      },
      {
        path: 'user-group',
        loadChildren: () => import('../../../features/group-user/group-user.module').then(x => x.GroupUserModule)
      }
    ]
  }, {
    path: '',
    component: ManageLayoutComponent,
    children: [
      {
        path: 'lessons',
        loadChildren: () => import('../../../features/learner-lesson/learner-lesson.module').then(m => m.LearnerLessonModule),
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageLayoutRoutingModule { }

import { NgModule } from '@angular/core';
import { ManageLayoutRoutingModule } from './manage-layout-routing.module';
import { AppBlankComponent } from '../../blank/blank.component';
import { AppBreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { ManageLayoutComponent } from './manage-layout.component';
import { HorizontalAppHeaderComponent } from './horizontal-header/horizontal-header.component';
import { HorizontalAppSidebarComponent } from './horizontal-sidebar/horizontal-sidebar.component';
import { VerticalAppHeaderComponent } from './vertical-header/vertical-header.component';
import { VerticalAppSidebarComponent } from './vertical-sidebar/vertical-sidebar.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule } from '@angular/forms';
import { ManageLayoutCoreModule } from 'src/app/core/layouts/manage-layouts/manage-layout-core.module';

@NgModule({
  declarations: [
    ManageLayoutComponent,
    VerticalAppHeaderComponent,
    AppBlankComponent,
    VerticalAppSidebarComponent,
    AppBreadcrumbComponent,
    HorizontalAppHeaderComponent,
    HorizontalAppSidebarComponent,
  ],
  imports: [
    ManageLayoutCoreModule,
    SharedModule,
    ManageLayoutRoutingModule,
    PerfectScrollbarModule,
    FormsModule
  ]
})
export class ManageLayoutModule { }

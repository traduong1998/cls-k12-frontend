import { Component } from '@angular/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { TranslateService } from '@ngx-translate/core';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { Router } from '@angular/router';
import { InfoIdentity } from 'cls-k12-sdk-js/src';
import { DashboardTypes } from 'src/app/core/enums/dashboard-types';

@Component({
  selector: 'app-vertical-header',
  templateUrl: './vertical-header.component.html',
  styleUrls: []
})

export class VerticalAppHeaderComponent {
  public DashboardTypes = DashboardTypes;
  public config: PerfectScrollbarConfigInterface = {};
  public tokenInfo: InfoIdentity;
  public canManage: boolean;

  // This is for Notifications
  notifications: Object[] = [
    {
      round: 'round-danger',
      icon: 'ti-link',
      title: 'Launch Admin',
      subject: 'Just see the my new admin!',
      time: '9:30 AM'
    },
    {
      round: 'round-success',
      icon: 'ti-calendar',
      title: 'Event today',
      subject: 'Just a reminder that you have event',
      time: '9:10 AM'
    },
    {
      round: 'round-info',
      icon: 'ti-settings',
      title: 'Settings',
      subject: 'You can customize this template as you want',
      time: '9:08 AM'
    },
    {
      round: 'round-primary',
      icon: 'ti-user',
      title: 'Pavan kumar',
      subject: 'Just see the my admin!',
      time: '9:00 AM'
    }
  ];

  // This is for Mymessages
  mymessages: Object[] = [
    {
      useravatar: 'assets/images/users/1.jpg',
      status: 'online',
      from: 'Pavan kumar',
      subject: 'Just see the my admin!',
      time: '9:30 AM'
    },
    {
      useravatar: 'assets/images/users/2.jpg',
      status: 'busy',
      from: 'Sonu Nigam',
      subject: 'I have sung a song! See you at',
      time: '9:10 AM'
    },
    {
      useravatar: 'assets/images/users/2.jpg',
      status: 'away',
      from: 'Arijit Sinh',
      subject: 'I am a singer!',
      time: '9:08 AM'
    },
    {
      useravatar: 'assets/images/users/4.jpg',
      status: 'offline',
      from: 'Pavan kumar',
      subject: 'Just see the my admin!',
      time: '9:00 AM'
    }
  ];

  public selectedLanguage: any = {
    language: 'English',
    code: 'en',
    type: 'US',
    icon: 'us'
  }

  public languages: any[] = [{
    language: 'English',
    code: 'en',
    type: 'US',
    icon: 'us'
  },
  {
    language: 'Español',
    code: 'es',
    icon: 'es'
  },
  {
    language: 'Français',
    code: 'fr',
    icon: 'fr'
  },
  {
    language: 'German',
    code: 'de',
    icon: 'de'
  }]


  avartar:string
  constructor(
    private translate: TranslateService,
    private authService: AuthService,
    private configService: AppConfigService,
    private router: Router
  ) {

    this.tokenInfo = authService.getTokenInfo();
    this.canManage = this.tokenInfo.userTypeRole == 'ADMIN'
      || this.tokenInfo.userTypeRole == 'TEACHER' ||
      this.tokenInfo.userTypeRole == 'SUPER'
      if (!this.tokenInfo.avatar) {
        this.avartar = '/assets/images/users/user-avartar.png'
      }
      else {
        this.avartar = this.tokenInfo.avatar;
      }
  }

  changeLanguage(lang: any) {
    console.log(`change language`, lang);
    this.translate.use(lang.code)
    this.selectedLanguage = lang;
  }
  onLogOut() {
    this.authService.logout();
    // localStorage.clear();
    localStorage.removeItem('light_mode');
    this.router.navigate(['/authentication/login']);
  }

  changeDashboardType(type: DashboardTypes) {
    this.configService.changeDashboardType(type);
  }
}

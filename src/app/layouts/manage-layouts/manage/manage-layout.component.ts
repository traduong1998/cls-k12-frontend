import { BreakpointObserver, BreakpointState, MediaMatcher } from '@angular/cdk/layout';
import { ActivatedRoute, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import {
	AfterViewInit,
	ChangeDetectorRef,
	Component,
	ElementRef,
	OnDestroy,
	Renderer2,
	ViewChild
} from '@angular/core';
import { MenuItems } from '../../../core/classes/menu-items/menu-items';


import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { filter } from 'rxjs/operators';
import { DashboardTypes } from 'src/app/core/enums/dashboard-types';
import { WindowScrollService } from 'src/app/shared/helpers/services/window-scroll.service';
import { MatSidenav } from '@angular/material/sidenav';
import { NgxSpinnerService } from 'ngx-spinner';
import { ProgressPlayerService } from '../../../features/learner-lesson/Services/progress-player.service';
import { UserIdentity, SchoolEndpoint, DivisionEndpoint } from 'cls-k12-sdk-js/src';
import { UnitConfig } from 'src/app/core/interfaces/app-config';

/** @title Responsive sidenav */
@Component({
	selector: 'app-manage-layout',
	templateUrl: 'manage-layout.component.html',
	styleUrls: ['./manage-layout.component.scss']
})
export class ManageLayoutComponent implements OnDestroy, AfterViewInit {
	@ViewChild("snav") snav: MatSidenav;
	isShowLoading: boolean;
	mobileQuery: MediaQueryList;
	userIdentity: UserIdentity;
	schoolEndpoint: SchoolEndpoint;
	endpointDivision: DivisionEndpoint;


	dir = 'ltr';
	dark = false;
	minisidebar = false;
	boxed = false;
	horizontal = false;

	green = false;
	blue = false;
	danger = false;
	showHide = false;
	url = '';
	sidebarOpened = false;
	status = false;
	isDarkMode = false;
	isLearner = false;
	public showSearch = false;
	public config: PerfectScrollbarConfigInterface = {};
	// tslint:disable-next-line - Disables all
	private _mobileQueryListener: () => void;

	private listenerScroll;
	unitInfo:UnitConfig;
	idPortalId;
	idDivisionId;
	idSchoolId;
	unitNameDivision;
	unitNameSchool;

	@ViewChild('sidenavContent') sidenavContent;
	@ViewChild('pageContent') pageContent: ElementRef;
	baseApiUrl = 'http://localhost:65000';
	isMobileSize: boolean;

	constructor(
		public router: Router,
		private activatedRoute: ActivatedRoute,
		changeDetectorRef: ChangeDetectorRef,
		media: MediaMatcher,
		public breakpointObserver: BreakpointObserver,
		public menuItems: MenuItems,
		private appConfigService: AppConfigService,
		private windowScrollService: WindowScrollService,
		private renderer2: Renderer2,
		private spinner: NgxSpinnerService,
		private progressService: ProgressPlayerService,
		private _authService: AuthService
	) {
		// console.log(`layout dashboard contructor`);
		this.userIdentity = _authService.getTokenInfo();
		if (this.userIdentity.levelManage == "DPM") {
			this.idPortalId = this.userIdentity.portalId;

		} else if (this.userIdentity.levelManage == "DVS") {
			this.idDivisionId = this.userIdentity.divisionId;

		} else if (this.userIdentity.levelManage == "SCH") {
			this.idSchoolId = this.userIdentity.schoolId;
		}
		console.log("asdadasdasd",this.userIdentity.levelManage)
		this.schoolEndpoint = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
		this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
		this.mobileQuery = media.matchMedia('(min-width: 1023px)');
		this._mobileQueryListener = () => changeDetectorRef.detectChanges();
		// tslint:disable-next-line: deprecation
		this.mobileQuery.addListener(this._mobileQueryListener);
		// this is for dark theme
		// const body = document.getElementsByTagName('body')[0];
		// body.classList.toggle('dark');
		this.dark = false;

		this.router.events.subscribe(
			event => {
				if (event instanceof NavigationStart) {
					// this.isShowLoading = true;
					this.spinner.show('loading-page-content');
				} else if (
					event instanceof NavigationEnd ||
					event instanceof NavigationCancel ||
					event instanceof NavigationError
				) {
					// this.isShowLoading = false;
					this.spinner.hide('loading-page-content', 81);
				}
			},
			() => {
				// this.isShowLoading = false;
				this.spinner.hide('loading-page-content');
			}
		);
	}

	ngAfterViewInit() {
		this.listenerScroll = this.renderer2.listen(this.sidenavContent.elementRef.nativeElement, 'scroll', (e: Event) => {
			const element = (e.target as Element);
			this.windowScrollService.updateScrollY(element.scrollTop, element.clientHeight, this.pageContent.nativeElement.clientHeight);
		});
	}

	ngOnDestroy(): void {
		// tslint:disable-next-line: deprecation
		this.mobileQuery.removeListener(this._mobileQueryListener);
	}

	ngOnInit() {
		this.processChangeDashboardType();
		this.unitInfo = this.appConfigService.getUnitConfig();
		if (this.userIdentity.levelManage == "DVS") {
			this.endpointDivision.getDivisionsById(this.idDivisionId).then(res=>{
				this.unitNameDivision = res.name
			})
		} else if (this.userIdentity.levelManage == "SCH") {
			this.schoolEndpoint.getSchoolDetail(this.idSchoolId).then(res=>{
				this.unitNameSchool =  res.name
			}
		);
		}
		this.initEventfromAppConfig();
		this.initEventProcessDashboard();
		this.progressService.isLightMode$.subscribe(res => this.isDarkMode = res);
		this.progressService.isLearner$.subscribe(res => {
			this.isLearner = res;
		});
	}

	initEventfromAppConfig() {
		this.appConfigService.configUpdate$.subscribe((cf) => {
			// if (cf.event == 'dashboard') {
			// 	this.menuItems.setType(cf.config.dashboard.type);
			// }
		});
	}

	initEventProcessDashboard() {
		this.router.events
			.pipe(filter(event => event instanceof NavigationEnd))
			.subscribe((val) => {
				// see also 
				// console.log(val instanceof NavigationEnd);
				this.processChangeDashboardType();
			});
	}

	/**TODO: kiểm tra dashboard loại chi, reload cho đúng loại */
	processChangeDashboardType() {
		let isManageLink = this.router.url.indexOf('learner') > -1 ? false : true;

		this.appConfigService.changeDashboardType(isManageLink ? DashboardTypes.manage : DashboardTypes.learner);
	}

	clickEvent(): void {
		this.status = !this.status;
	}

	darkClick() {
		// const body = document.getElementsByTagName('body')[0];
		// this.dark = this.dark;
		const body = document.getElementsByTagName('body')[0];
		body.classList.toggle('dark');
		// if (this.dark)
		// else
		// 	body.classList.remove('dark');
		// this.dark = this.dark;
		// body.classList.toggle('dark');
		// this.dark = this.dark;
	}

	onSidebarToggle() {
		//kích thước mobile thì toggle bình thường
		if (!this.mobileQuery.matches) {
			console.log(`mobile size`);
			this.snav.toggle();
		}
		else {
			//kích thước desktop thì nút toggle này sẽ chuyển chế độ sidebar
			// console.log(`desktop`);
			this.minisidebar = !this.minisidebar;
		}
	}

	returnHome(){
		this.router.navigate(['/']);
	}

}

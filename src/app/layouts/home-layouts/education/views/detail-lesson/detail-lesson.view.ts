import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common'
import { ChapterDashboard } from 'src/app/layouts/home-layouts/education/interfaces/chapterDashboard';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { LessonDashboard } from 'src/app/layouts/home-layouts/education/interfaces/lessonDashboard';
import { GradeDashboard } from '../../interfaces/gradeDashboard';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { ContentGroupResponses, ContentGroups } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { ReferenceByLesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/reference-by-lesson';
import { LessonVM } from './view-models/lesson-vm';
import { GlobalEndpoint, UserEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { ReferenceLessonVM } from './view-models/reference-lesson';
import { UserLessonRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/user-lesson-request';
import { UserRegistryLesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/user-registry-lesson-request';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'education-detail-lesson',
  templateUrl: './detail-lesson.view.html',
  styleUrls: ['./detail-lesson.view.scss']
})
export class DetailLessonView implements OnInit {
  lessonName: string = '';
  width: number;
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.width = window.innerWidth;
    // console.log(this.width)
  }
  @ViewChild('scroll') scroll: ElementRef;

  reference: ReferenceByLesson;

  referenceLesson: ReferenceLessonVM[] = []

  listGrades: GradeDashboard[];

  lessonInfo: LessonVM;

  perCom: number = 0;

  lessonDashboard: LessonDashboard

  listChapter: ChapterDashboard[];
  listGradesDashboard: GradeDashboard[];

  userId: number;

  lessonId: number;

  gradeId: number;

  chapterId: number;

  subjectId: number;

  schoolId: number;

  unitId: number;

  openDetailLesson = true;
  authService;
  isAuthenticated: boolean;
  isContinute: boolean;
  isWaiting: boolean;
  isRegister: boolean;
  shareSetting: string;
  percentageOfCompletion: number = -1;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';


  chapperContent: ContentGroupResponses;
  contentGroups: ContentGroups[] = [];

  portalId: number;

  divisonId: number;

  teacherId: number;

  isDisabled = false;
  selected: number;
  gradeDisplayMobile: number;

  private lessonEndpoint: LessonEndpoint;
  private userEndpoint: UserEndpoint;
  private globalEndpoint: GlobalEndpoint;
  private history: string[] = []
  constructor(private route: ActivatedRoute, private router: Router, private _authService: AuthService, private _snackBar: MatSnackBar, private locationed: Location, private configService: AppConfigService) {
    // debugger;
    this.onResize();
    this.listGradesDashboard = [];
    this.portalId = this.configService.getConfig().unit.portalId;
    this.schoolId = this.configService.getConfig().unit.schoolId;
    this.divisonId = this.configService.getConfig().unit.divisionId;

    this.lessonEndpoint = new LessonEndpoint();
    this.userEndpoint = new UserEndpoint();
    this.globalEndpoint = new GlobalEndpoint();
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.history.push(event.urlAfterRedirects)
      }
    })

    //lây danh sách khối
    this.globalEndpoint.getGradeOptionsByDomain(location.hostname).then(res => {
      this.selected = this.gradeDisplayMobile ? this.gradeDisplayMobile : res[0].id;
      res.forEach((grade) => {
        this.listGradesDashboard.push(new GradeDashboard(grade));
      });
    })

    this.route.queryParamMap.subscribe((params) => {
      var myQueryParams = { ...params };
      this.lessonId = +myQueryParams["params"].lesson;
      this.gradeDisplayMobile = +myQueryParams["params"].gradeId;
    }
    );

    //check authen
    this.isAuthenticated = this._authService.isAuthenticated();

    if (this.isAuthenticated == true) {
      this.authService = _authService.getTokenInfo();
      this.userId = +this.authService.userId;
      this.schoolId = +this.authService.schoolId;

      this.lessonEndpoint.getLessonUser(this.userId, this.lessonId).then(res => {
        if (res) {
          this.percentageOfCompletion = +res.percentageOfCompletion
          this.isContinute = true;
        }
        else {
          this.isContinute = false;
        }
      }).then(() => {
        if (this.isContinute == false) {
          this.lessonEndpoint.getLessonUserRegistry(this.userId, this.lessonId).then(res => {
            if (res) {
              if (res.isApproval == false) {
                this.isWaiting = true;
              } else {
                this.isContinute = true;
              }
            }
            else {
              //chưa đăng kí
              if (this.shareSetting == "REG") {
                this.isRegister = false;
              }
              else {
                this.isRegister = true;
              }
            }
          });
        }
      })
    }

    //thông tin khóa học mặc định
    this.lessonEndpoint.getLessonInfoById(this.lessonId)
      .then((res) => {
        this.gradeId = res.gradeId;
        this.subjectId = res.subjectId;
        this.unitId = res.unitId;

        this.teacherId = res.userId;
        if (!res.avatar) {
          res.avatar = '/assets/images/lesson/lesson-default.png'
        }

        this.shareSetting = res.shareSetting;
        this.lessonInfo = LessonVM.From(res, null, null);

        return this.userEndpoint.getUserInfoById(this.portalId, res.userId);
      })
      .then((res) => {

        if (!res.avatar) {
          res.avatar = '/assets/images/users/user-avartar.png'
        }
        this.lessonInfo.setUserInfo(res.firstName, res.lastName, res.avatar);
      })
      .then(() => {
        //danh sách khóa học liên quan
        this.lessonEndpoint.getReferenceLesson(this.gradeId, this.subjectId).then((res) => {
          // this.userEndpoint.GetListUserByIds(res.map((x)=>x.teacherId)).then(()=>{
          // })
          res.forEach((item) => {
            this.userEndpoint.getUserInfoById(this.portalId, item.teacherId).then((teacher) => {

              if (!item.avatarUrl) {
                item.avatarUrl = '/assets/images/lesson/lesson-default.png'
              }
              if (!teacher.avatar) {
                teacher.avatar = '/assets/images/users/user-avartar.png'
              }
              this.referenceLesson.push(ReferenceLessonVM.From(item, teacher.lastName, teacher.firstName, teacher.avatar));
              this.referenceLesson = this.referenceLesson.slice(0, 5);
            })
              .catch(err => {
                console.log("Lỗi");
              })
          })
        })
      })
      .catch((err) => {

      });

    //danh sách nội dung bài
    this.lessonEndpoint.getLessonContentsInfoById(this.lessonId).then(res => {
      this.contentGroups = res.contentGroups;
    }).catch(err => { })

    // daanh ách tài liêu tham khảo
    this.lessonEndpoint.getReferenceByLessonId(this.lessonId).then(res => {
      this.reference = res;
    })
  }

  ngOnInit(): void {
  }

  searchLesson() {
    // debugger;
    this.router.navigate(['list-lessons-home-page'], { queryParams: { gradeId: this.gradeId, lessonName: this.lessonName } })
  }

  gradeNavigateTo(value) {
    this.router.navigate(['/list-lessons-home-page'], { queryParams: { gradeId: value } });
  }
  //Đưa tới login nếu chưa đăng nhập
  register() {
    //nếu chưa đăng nhập
    if (this.isAuthenticated == false) {
      this.router.navigate(['authentication/login'])
    }
    //nếu đang đang đợt duyệt
    if (this.isWaiting) {
      //disable nut đó
    }
    //nếu chưa đăng kí
    if (this.isRegister == true) {
      //đăng kí học ngay
      if (this.shareSetting == "NOA") {
        // debugger
        let userLesson: UserLessonRequest = { lessonId: this.lessonId, shareSetting: 'NOA', assignFrom: 'REG', schoolId: 0, gradeId: 0 }
        // đăng kí khóa học bên system
        Promise.resolve()
          .then(() => {
            this.isDisabled = true;
            this.isContinute = true;
            this.isRegister = false;
            this.lessonEndpoint.createUserLesson(userLesson).then(() => {
              this.isDisabled = false;
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: "Đăng kí thành công",
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            })
          })
      }
      else {

        let userRegistryLesson: UserLessonRequest = { lessonId: this.lessonId, shareSetting: 'APP', assignFrom: 'REG', schoolId: this.schoolId, gradeId: this.gradeId }
        this.lessonEndpoint.createUserLesson(userRegistryLesson)
          .then(res => {

            this.isDisabled = true;
            this.isWaiting = true;
            this.isRegister = false;
          })
          .then(() => {
            this.isDisabled = false;
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: "Đăng kí thành công",
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          })
      }
    }
    //nếu đã đăng kí
    if (this.isContinute == true) {
      //routing tới bài học 
      this.router.navigate([`learner/lessons/${this.lessonId}/contents`]);
    }
  }

  scrollTop() {
    this.scroll.nativeElement.scrollTop = 0;
  }

  choseLesson(lessonId) {
    this.router.navigate(['detail-lesson'], { queryParams: { lesson: lessonId } })
  }
  backPage() {
    this.history.pop()
    if (this.history.length > 0) {
      this.locationed.back()
    } else {
      this.router.navigateByUrl('/')
    }
  }

  backCurriculum() {
    this.router.navigate(['/detail-lesson'], { queryParams: { lesson: this.lessonId } });
  }

}

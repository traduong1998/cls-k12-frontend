import { LessonReference } from "sdk/cls-k12-sdk-js/src/services/lesson/responses/lesson-reference";

export class ReferenceLessonVM implements LessonReference {
    teacherId: number;
    id: number;
    name: string;
    introduction: string
    teacherName: string;
    teacherAvatar: string;
    avatarUrl: string;

    setUserInfo(teacherName, teacherAvatar): void {
        this.teacherName = teacherName;
        this.teacherAvatar = teacherAvatar;
    }

    static From(data: LessonReference, teacherLastName: string, teacherFirstname: string, teacherAvatar: string) {
        let m: ReferenceLessonVM = new ReferenceLessonVM();
        m.id = data.id;
        m.name = data.name;
        m.introduction = data.introduction
        m.avatarUrl = data.avatarUrl;

        if(teacherLastName == null){
          teacherLastName = ''
        }
        if(teacherFirstname == null){
            teacherFirstname = ''
        }
        m.teacherName = (teacherLastName + ' ' + teacherFirstname).trim();
        m.teacherAvatar = teacherAvatar;
        return m;
    }
}
import { Lesson } from "sdk/cls-k12-sdk-js/src/services/lesson/models/lesson";

export class LessonVM implements Lesson {
    id: number;
    name: string;
    introduction: string
    teacherId: number;
    teacherName: string;
    teacherAvatar: string;
    avatarUrl: string;
    percentageOfCompletion: number;

    setUserInfo(firstName, lastname, teacherAvatar): void {
        if(firstName == null){
            firstName = ''
        }
        if(lastname == null){
            lastname = ''
        }
        this.teacherName = (lastname + ' ' + firstName).trim();
        this.teacherAvatar = teacherAvatar;
    }

    static From(data: Lesson, teacherName: string, teacherAvatar: string) {
        let m: LessonVM = new LessonVM();
        m.id = data.id;
        m.name = data.name;
        m.introduction = data.introduction
        m.teacherId = data.userId;
        m.avatarUrl = data.avatar ?? '';
        m.teacherName = teacherName;
        m.teacherAvatar = teacherAvatar;
        m.percentageOfCompletion = null;
        return m;
    }
}
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurriculumView } from './curriculum.view';

describe('CurriculumView', () => {
  let component: CurriculumView;
  let fixture: ComponentFixture<CurriculumView>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurriculumView ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurriculumView);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

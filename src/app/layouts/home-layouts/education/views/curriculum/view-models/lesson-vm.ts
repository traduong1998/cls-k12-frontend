import { LessonHomePage } from "sdk/cls-k12-sdk-js/src/services/lesson/responses/lessons-home-page";

export class LessonsVM implements LessonHomePage {
    
    id: number;
    name: string;
    teacherId: number;
    teacherName: string;
    teacherAvatar: string;
    createdDate: Date;
    avatarUrl: string;
    learnTime: number;
    interval: number;
    subjectId: number;
    subjectName: string
    percentageOfCompletion: number;

    static From(data: LessonHomePage, firstname: string, subjectName: string, teacherAvatar: string) {
        var m: LessonsVM =
        {
            id: data.id,
            name: data.name,
            teacherId: data.teacherId,
            avatarUrl: data.avatarUrl,
            interval: data.interval,
            learnTime: data.learnTime,
            percentageOfCompletion: data.percentageOfCompletion,
            createdDate: data.createdDate,
            subjectId: data.subjectId,
            teacherName: firstname,
            subjectName: subjectName,
            teacherAvatar: teacherAvatar
        };
        return m;
    }
}
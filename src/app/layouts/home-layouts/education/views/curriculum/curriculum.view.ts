import {
  Component,
  HostListener,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { GradeEndpoint } from 'sdk/cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { SubjectEndpoint } from 'cls-k12-sdk-js/src/services/subject/endpoints/SubjectEndpoint';
import { ActivatedRoute, Router } from '@angular/router';
import { ChapterDashboard } from 'src/app/layouts/home-layouts/education/interfaces/chapterDashboard';
import { BehaviorSubject, Subscription } from 'rxjs';
import { SubjectDashboard } from '../../interfaces/subjectDashboard';
import { UnitDashboard } from '../../interfaces/unitDashboard';
import { GradeDashboard } from '../../interfaces/gradeDashboard';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { GlobalEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { CurriculumVM } from './view-models/curriculum.VM';
import { LessonVM } from '../index/view-models/curriculum.VM';
import { Currculum } from 'sdk/cls-k12-sdk-js/src/services/curriculum/responses/curriculum';
@Component({
  selector: 'education-curriculum',
  templateUrl: './curriculum.view.html',
  styleUrls: ['./curriculum.view.scss'],
})
export class CurriculumView implements OnInit {
  lessonName: string = '';
  width: number;
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.width = window.innerWidth;
    // console.log(this.width)
  }

  currculum: Currculum[];
  selected: number;

  //scroll to top
  isShow: boolean;
  topPosToStartShowing = 217;
  @HostListener('window:scroll')
  checkScroll() {
    // window의 scroll top
    // Both window.pageYOffset and document.documentElement.scrollTop returns the same result in all the cases. window.pageYOffset is not supported below IE 9.
    const scrollPosition =
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop ||
      0;
    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }

  // TODO: Cross browsing
  gotoTop() {
    document.getElementsByTagName('mat-drawer-content')[0].scrollTo(0, 0);
    // window.scroll({
    //   top: 0,
    //   left: 0,
    //   behavior: 'smooth'
    // });
  }
  //
  routeSub: Subscription;
  listGradesDashboard: GradeDashboard[];
  listGrades: GradeOption[];
  gradeEndpoint: GradeEndpoint;

  listSubjectsDashboard: SubjectDashboard[];
  listSubjects: SubjectOption[];
  subjectEndpoint: SubjectEndpoint;

  gradeId: number;

  //listSubjects: SubjectOption[];

  listSubject$: BehaviorSubject<SubjectDashboard[]> = new BehaviorSubject<
    SubjectDashboard[]
  >([]);

  listChapter: ChapterDashboard[];

  listUnit: UnitDashboard[];

  subjectId: number;
  subjectName: string;

  isReady: boolean = false;

  panelOpenState = false;

  openDetailLesson = true;

  hiddenCurriculum = false;

  //myQueryParams = {}

  sjActived = false;

  baseApiUrl = 'http://192.169.0.108:8010';
  selectedcurriculum: number;
  portalId: number;
  schoolId: number;
  divisonId: number;
  curriculumId: number = 0;
  curriculumName: string;
  listCurriculum: CurriculumVM[] = [];
  globalEndpoint: GlobalEndpoint;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _authService: AuthService,
    private configService: AppConfigService
  ) {
    this.onResize();
    this.listGradesDashboard = [];
    this.listSubjectsDashboard = [];
    this.gradeEndpoint = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.subjectEndpoint = new SubjectEndpoint({ baseUrl: this.baseApiUrl });

    this.portalId = configService.getConfig().unit.portalId;
    this.schoolId = configService.getConfig().unit.schoolId;
    this.divisonId = configService.getConfig().unit.divisionId;

    //fake
    // this.schoolId = 26;
    // this.divisonId = 11;

    this.globalEndpoint = new GlobalEndpoint();
    // lấy khổi và môn trên query
    this.routeSub = this.route.queryParams.subscribe((params) => {
      this.gradeId = +params['gradeId'];
      this.subjectId = +params['subjectId'];
      this.curriculumId = +params['currculumId'];
    });

    if (this.curriculumId > 0) {
      this.selectedcurriculum = this.curriculumId;
    }

    // //get listGrades từ API
    // lấy khung chương trình qua theo môn đó qua

    if (this.gradeId > 0 || this.subjectId > 0) {
      this.globalEndpoint
        .getSubjectOptionsByGrade(this.portalId, this.gradeId)
        .then((res) => {
          res.forEach((subject) => {
            this.listSubjectsDashboard.push(new SubjectDashboard(subject));
          });
        });
      //lây danh sách khối
      this.globalEndpoint
        .getGradeOptionsByDomain(location.hostname)
        .then((res) => {
          this.selected = res[0].id;
          res.forEach((grade) => {
            this.listGradesDashboard.push(new GradeDashboard(grade));
          });
        });
      Promise.resolve()
        .then(() => {
          return this.globalEndpoint.getCurriculum(
            location.hostname,
            this.divisonId,
            this.schoolId,
            this.gradeId,
            this.subjectId
          );
        })
        .then((res) => {
          if (res) {
            this.currculum = res;
            if (this.curriculumId == 0) {
              this.selectedcurriculum = res[0].curriculumId;
              this.curriculumId = res[0].curriculumId;
            }
            // this.curriculumName = res[0].curriculumName;
          } else {
            this.curriculumName = '';
            this.listCurriculum = [];
          }
        })
        .then(() => {
          if (this.curriculumId) {
            //lấy chương
            this.globalEndpoint
              .getCurriculumChildrentNodesByPortalId(
                this.portalId,
                this.curriculumId
              )
              .then((res) => {
                // lây tiếp danh sách bài học
                res.forEach((item) => {
                  let listLesson: LessonVM[] = [];

                  this.globalEndpoint
                    .getCurriculumChildrentNodesByPortalId(
                      this.portalId,
                      item.id
                    )
                    .then((res) => {
                      res.forEach((i) => {
                        listLesson.push({
                          lessonId: i.id,
                          lessonName: i.curriculumName,
                        });
                      });
                    });

                  this.listCurriculum.push(
                    CurriculumVM.From(item.id, item.curriculumName, listLesson)
                  );
                });
              });
          }
        })

        .catch((err) => {});
    } else {
      // lấy khung chương trình
      Promise.resolve()
        .then(() => {
          return this.globalEndpoint.getGradeOptionsByDomain(location.hostname);
        })
        .then((res) => {
          this.selected = res[0].id;
          this.gradeId = res[0].id;
          res.forEach((grade) => {
            this.listGradesDashboard.push(new GradeDashboard(grade));
          });
          return this.globalEndpoint.getSubjectOptionsByGrade(
            this.portalId,
            this.gradeId
          );
        })
        .then((res) => {
          // debugger
          this.subjectId = res[0].id;
          this.subjectName = res[0].name;
          res.forEach((subject) => {
            this.listSubjectsDashboard.push(new SubjectDashboard(subject));
          });

          return this.globalEndpoint.getCurriculum(
            location.hostname,
            this.divisonId,
            this.schoolId,
            this.gradeId,
            this.subjectId
          );
        })
        .then((res) => {
          if (res) {
            this.currculum = res;
            this.selectedcurriculum = res[0].curriculumId;
            this.curriculumId = res[0].curriculumId;
            this.curriculumName = res[0].curriculumName;
          } else {
            this.curriculumId = undefined;
            this.curriculumName = '';
            this.listCurriculum = [];
          }
        })
        .then(() => {
          if (this.curriculumId) {
            //lấy chương
            this.globalEndpoint
              .getCurriculumChildrentNodesByPortalId(
                this.portalId,
                this.curriculumId
              )
              .then((res) => {
                // lây tiếp danh sách bài học
                res.forEach((item) => {
                  let listLesson: LessonVM[] = [];

                  this.globalEndpoint
                    .getCurriculumChildrentNodesByPortalId(
                      this.portalId,
                      item.id
                    )
                    .then((res) => {
                      res.forEach((i) => {
                        listLesson.push({
                          lessonId: i.id,
                          lessonName: i.curriculumName,
                        });
                      });
                    });
                  this.listCurriculum.push(
                    CurriculumVM.From(item.id, item.curriculumName, listLesson)
                  );
                });
              });
          }
        })

        // .then(() => {
        //   // get grade ID in local storage

        //   this.listGradesDashboard.forEach((item) => {
        //     if (item.id === gradeId) {
        //       item.isActived = true;
        //     }
        //   });
        // })
        .catch((err) => {});
    }
  }
  ngOnInit(): void {}

  gotoGrade() {
    let elemenent = document.getElementById('mat-drawer-content');
    elemenent && elemenent.scrollIntoView({ behavior: 'smooth' });
  }

  onScrollClick(el: HTMLElement) {
    el.scrollIntoView();
  }

  gradeClick(grade) {
    this.gradeId = grade;
    this.router.navigate(['/curriculum'], { queryParams: { gradeId: this.gradeId, lessonName: null } })
    localStorage.setItem('gradeId', JSON.stringify(this.gradeId));

    this.listGradesDashboard.forEach((item) => {
      if (item.id === this.gradeId) {
        item.isActived = true;
      }
      else {
        item.isActived = false}
    });
  }

  mouseEnter(subject) {
    this.listSubjectsDashboard.forEach((sj) => {
      // debugger
      if (sj.id == subject.id) {
        sj.isActived = true;
      } else {
        sj.isActived = false;
      }
    });
  }
  mouseOut(subject) {
    this.listSubjectsDashboard.forEach((sj) => {
      if (sj.id == subject.id) {
        sj.isActived = false;
      }
    });
  }

  // chọn môn lấy khung chương trình
  onSubjectClick(subject: SubjectDashboard) {
    this.subjectName = subject.name;
    this.subjectId = subject.id;
    this.listCurriculum = [];
    this.currculum = [];
    this.selectedcurriculum = 0;
    Promise.resolve()
      .then(() => {
        return this.globalEndpoint.getCurriculum(
          location.hostname,
          this.divisonId,
          this.schoolId,
          this.gradeId,
          subject.id
        );
      })
      .then((res) => {
        if (res) {
          this.currculum = res;
          this.selectedcurriculum = res[0].curriculumId;
          this.curriculumId = res[0].curriculumId;
          this.curriculumName = res[0].curriculumName;
        } else {
          this.curriculumName = '';
          this.listCurriculum = [];
          this.curriculumId = undefined;
        }
      })
      .then(() => {
        if (this.curriculumId) {
          //lấy chương
          this.globalEndpoint
            .getCurriculumChildrentNodesByPortalId(
              this.portalId,
              this.curriculumId
            )
            .then((res) => {
              // lây tiếp danh sách bài học
              res.forEach((item) => {
                let listLesson: LessonVM[] = [];

                this.globalEndpoint
                  .getCurriculumChildrentNodesByPortalId(this.portalId, item.id)
                  .then((res) => {
                    res.forEach((i) => {
                      listLesson.push({
                        lessonId: i.id,
                        lessonName: i.curriculumName,
                      });
                    });

                    this.listCurriculum.push(
                      CurriculumVM.From(
                        item.id,
                        item.curriculumName,
                        listLesson
                      )
                    );
                  });
              });
            });
        }
      })
      .catch((err) => {});
  }

  onSubjectOut(subject: SubjectDashboard) {
    this.listSubjectsDashboard.forEach((sj) => {
      if (sj.id == subject.id) {
        sj.isActived = false;
      }
    });

    //this.listSubject$.next(this.listSubject);
  }

  // chọn khối lấy danh sách môn
  gradeNavigateTo(value: number) {
    this.gradeId = value;
    this.listSubjectsDashboard = [];
    this.listCurriculum = [];
    this.currculum = [];
    this.selectedcurriculum = 0;
    this.router.navigate(['/curriculum']);
    this.gotoGrade();
    this.gradeClick(value);
    //lấy cái khung chương trình
    Promise.resolve()
      .then(() => {
        return this.globalEndpoint.getSubjectOptionsByGrade(
          this.portalId,
          value
        );
      })
      .then((res) => {
        this.subjectId = res[0].id;
        this.subjectName = res[0].name;
        res.forEach((subject) => {
          this.listSubjectsDashboard.push(new SubjectDashboard(subject));
        });
        return this.globalEndpoint.getCurriculum(
          location.hostname,
          this.divisonId,
          this.schoolId,
          this.gradeId,
          this.subjectId
        );
      })
      .then((res) => {
        if (res) {
          this.currculum = res;
          this.selectedcurriculum = res[0].curriculumId;
          this.curriculumId = res[0].curriculumId;
          this.curriculumName = res[0].curriculumName;
        } else {
          this.curriculumName = '';
          this.listCurriculum = [];
          this.curriculumId = undefined;
        }
      })
      .then(() => {
        if (this.curriculumId) {
          //lấy chương
          this.globalEndpoint
            .getCurriculumChildrentNodesByPortalId(
              this.portalId,
              this.curriculumId
            )
            .then((res) => {
              // lây tiếp danh sách bài học
              res.forEach((item) => {
                let listLesson: LessonVM[] = [];

                this.globalEndpoint
                  .getCurriculumChildrentNodesByPortalId(this.portalId, item.id)
                  .then((res) => {
                    res.forEach((i) => {
                      listLesson.push({
                        lessonId: i.id,
                        lessonName: i.curriculumName,
                      });
                    });
                  });

                this.listCurriculum.push(
                  CurriculumVM.From(item.id, item.curriculumName, listLesson)
                );
              });
            });
        }
      })
      .catch((err) => {});
  }

  onClickHiddenCurriculums() {
    this.openDetailLesson = this.openDetailLesson == true ? false : true;
  }

  showListLesson(chapterId: number, unitId: number) {
    this.router.navigate(['list-lesson'], {
      queryParams: {
        gradeId: this.gradeId,
        subjectId: this.subjectId,
        curriculumId: this.curriculumId,
        chapterId: chapterId,
        unitId: unitId,
      },
    });
  }

  curriculumChange(value) {
    this.curriculumId = value;
    this.listCurriculum = [];
    this.globalEndpoint
      .getCurriculumChildrentNodesByPortalId(this.portalId, value)
      .then((res) => {
        // lây tiếp danh sách bài học
        res.forEach((item) => {
          let listLesson: LessonVM[] = [];

          this.globalEndpoint
            .getCurriculumChildrentNodesByPortalId(this.portalId, item.id)
            .then((res) => {
              res.forEach((i) => {
                listLesson.push({
                  lessonId: i.id,
                  lessonName: i.curriculumName,
                });
              });
            });
          this.listCurriculum.push(
            CurriculumVM.From(item.id, item.curriculumName, listLesson)
          );
        });
      });
  }
  searchLesson() {
    this.router.navigate(['list-lessons-home-page'], {
      queryParams: { gradeId: this.gradeId, lessonName: this.lessonName },
    });
  }

  backCurriculum() {
    this.router.navigate(['/curriculum'], {
      queryParams: {
        gradeId: this.gradeId,
        subjectId: this.subjectId,
        currculumId: this.curriculumId,
      },
    });
  }
}

import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, ReplaySubject, Subject, Subscription } from 'rxjs';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { UnitDashboard } from '../../interfaces/unitDashboard';
import { GradeDashboard } from '../../interfaces/gradeDashboard';
import { GradeEndpoint } from 'sdk/cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';
import { SubjectDashboard } from '../../interfaces/subjectDashboard';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { SubjectEndpoint } from 'cls-k12-sdk-js/src/services/subject/endpoints/SubjectEndpoint';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { GlobalEndpoint, SchoolEndpoint, UserEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { MatSelect } from '@angular/material/select';
import { TeacherOption } from 'sdk/cls-k12-sdk-js/src/services/user/models/teacher';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/PaginatorResponse';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { NumberPageDashboard } from '../../interfaces/numberPageDashboard';
import { LessonVM } from '../index/view-models/curriculum.VM';
import { CurriculumVM } from '../curriculum/view-models/curriculum.VM';
import { LessonsVM } from '../index/view-models/lesson-vm';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';


@Component({
  selector: 'education-list-lesson',
  templateUrl: './list-lesson.view.html',
  styleUrls: ['./list-lesson.view.scss']
})
export class ListLessonView implements OnInit {

  lessonName: string = '';
  page: number = 1;
  itemsPerPage = 9
  currentPage = 1;
  totalPage = 10;

  width: number;
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.width = window.innerWidth;
  }

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  routeSub: Subscription;
  listGradesDashboard: GradeDashboard[];
  listGrades: GradeOption[];
  gradeEndpoint: GradeEndpoint;

  listSubjectsDashboard: SubjectDashboard[];
  listSubjects: SubjectOption[];
  subjectEndpoint: SubjectEndpoint;

  listSchools: SchoolOption[] = [];
  schoolEndpoint: SchoolEndpoint;

  listTeachers: TeacherOption[] = [];
  userEndpoint: UserEndpoint;

  listLessons: PaginatorResponse<Lesson>;
  listLesssonDashboard: Lesson[];
  lessonEndpoint: LessonEndpoint;

  listLessonCount: number;

  /* loading sniper */
  isLoadingResults = false;

  queryParamListLessonHome: any;

  //listChapter: ChapterDashboard[];

  @ViewChild("schoolSelect") schoolSelect: MatSelect;
  filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);

  @ViewChild("teacherSelect") teacherSelect: MatSelect;
  filteredTeachers: ReplaySubject<TeacherOption[]> = new ReplaySubject<TeacherOption[]>(1);

  listUnits$: BehaviorSubject<UnitDashboard[]> = new BehaviorSubject<UnitDashboard[]>([]);

  listUnits: UnitDashboard[];


  gradeId: number;
  gradeName: string;

  chapterId: number;

  subjectId: number;

  selectChapter: number;

  unitId: number;

  pageId: number;

  advencedSearchDetailOpen = false;

  listPage: NumberPageDashboard[] = [];
  showButtonNextPage: boolean = false;

  //cbb

  baseApiUrl = 'http://192.169.0.108:8010';


  chooseSchool: number = 0;
  keyWordSchool: string = "";
  chooseTeacher = 0;
  keyWordTeacher: string = "";
  userId: number;
  // public filteredGroupContents: ReplaySubject<ContentGroupResponses[]> = new ReplaySubject<ContentGroupResponses[]>(0);

  listLesson: LessonVM[] = [];
  listChapter: CurriculumVM[] = [];
  portalId: number;
  schoolId: number;
  divisonId: number;
  curriculumId: number;
  listTeacherId: number[];
  listSubjectId: number[];
  lessons: LessonsVM[] = []
  selected: number;

  totalItem: number;

  globalEndpoint: GlobalEndpoint;
  constructor(private route: ActivatedRoute, private router: Router, private _authService: AuthService, private _snackBar: MatSnackBar, private configService: AppConfigService) {
    this.onResize();
    this.listGradesDashboard = [];
    this.listSubjectsDashboard = [];

    this.globalEndpoint = new GlobalEndpoint();
    this.userEndpoint = new UserEndpoint({ baseUrl: this.baseApiUrl });
    this.lessonEndpoint = new LessonEndpoint({ baseUrl: this.baseApiUrl });
    // xác định côngt
    this.portalId = this.configService.getConfig().unit.portalId;
    this.schoolId = this.configService.getConfig().unit.schoolId;
    this.divisonId = this.configService.getConfig().unit.divisionId;

    // lấy giá trị từ query
    this.routeSub = this.route.queryParams.subscribe(params => {
      this.gradeId = +params['gradeId'];
      this.subjectId = +params['subjectId'];

      this.chapterId = +params['chapterId'];
      this.unitId = +params['unitId'];
      this.curriculumId = +params['curriculumId'];
    });

    //this.selectChapter = this.chapterId;
    // lấy danh sách bài học trong cái chương gửi qua
    Promise.resolve()
      .then(() => {
        this.globalEndpoint.getCurriculumChildrentNodesByPortalId(this.portalId, this.chapterId).then(res => {

          res.forEach((i) => {
            this.listLesson.push({ lessonId: i.id, lessonName: i.curriculumName });
          })
        });
      })
      .then(() => {

        if (this.curriculumId) {
          //lấy chương
          this.globalEndpoint.getCurriculumChildrentNodesByPortalId(this.portalId, this.curriculumId).then(res => {

            res.forEach((i) => {
              this.listChapter.push({ curriculumId: i.id, curriculumName: i.curriculumName, lessons: null });
              console.log("listChapter", this.listChapter);
              if (i.id == this.chapterId) {
                this.selectChapter = i.id
              }
            })
          })
        }
      })
      .catch((err) => {

      });

    // lấy danh sách khóa học theo unit và chapter
    // lấy danh sách bài giảng đổ lên dữ view
    this.lessonEndpoint.getLessonsHomePage(this.portalId, this.divisonId, this.schoolId, this.gradeId, this.subjectId, null, this.chapterId, this.unitId, null, 1, 9).then(res => {
      if (res.totalItems > 0) {

        this.totalPage = res.totalItems;
        this.listTeacherId = res.items.map((x) => x.teacherId);
        this.listSubjectId = res.items.map((x) => x.subjectId);

        let listTeacher: TeacherOption[] = [];
        let listSubject: SubjectOption[] = [];

        Promise.all([
          this.globalEndpoint.GetListUserByIds(this.listTeacherId, this.portalId).then(res => {
            listTeacher = res;
          }),
          this.globalEndpoint.getSubjectOptionsByIds(this.listSubjectId, this.portalId).then(res => {
            listSubject = res;
          })
        ]).then((value) => {
          let i = 0;
          res.items.forEach((item) => {
            var teacher = listTeacher.find(x => x.id == item.teacherId);
            var subject = listSubject.find(x => x.id == item.subjectId);
            //item.avatarUrl = '' ? item.avatarUrl : '/assets/images/background/lesson-background.jpg';
            if (!item.avatarUrl) {
              item.avatarUrl = '/assets/images/lesson/lesson-default.png'
            }
            if (!teacher.avatar) {
              teacher.avatar = '/assets/images/users/user-avartar.png'
            }
            this.lessons.push(LessonsVM.From(item, teacher?.name, subject?.name, teacher?.avatar))
            console.log("sss", this.lessons)
          });
        });
      } else {
        this.lessons = [];
      }
    })

    // lấy danh sách khối
    this.globalEndpoint.getGradeOptionsByDomain(location.hostname).then(res => {
      this.selected = this.gradeId ? this.gradeId : res[0].id;
      res.forEach((grade) => {
        this.listGradesDashboard.push(new GradeDashboard(grade));
        if (this.gradeId == grade.id) {
          this.gradeName = grade.name;
        }
      });
    })
  }

  stopPropagation(event) {
    event.stopPropagation();
    // console.log("Clicked!");
  }

  searchLesson() {
    this.router.navigate(['list-lessons-home-page'], { queryParams: { gradeId: this.gradeId, lessonName: this.lessonName } })
  }

  filterSchool() {
    if (!this.listSchools) {
      return;
    }
    // get the search keyword
    let search = this.keyWordSchool;
    if (!search) {
      this.filteredSchools.next(this.listSchools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredSchools.next(
      this.listSchools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  filterTeacher() {
    if (!this.listTeachers) {
      return;
    }
    // get the search keyword
    let search = this.keyWordTeacher;
    if (!search) {
      this.filteredTeachers.next(this.listTeachers.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredTeachers.next(
      this.listTeachers.filter(teacher => teacher.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onMenuOpened() {
    const btn = document.getElementById('revisitSection');
    if (btn) {
      const elems = document.getElementsByClassName('button') as any;
      // for (let i = 0; i < elems.length; i++) {
      //   elems[i].style.width = `${btn.offsetWidth}px`;
      // }
      for (const item of elems) {
        item.style.width = `${btn.offsetWidth}px`;
      }
    }
  }

  ngOnInit(): void {
  }

  // chọn chương thì lòa lại danh sách bài học và những bài giảng của bài đầu tiên
  chapterNavigateTo(value) {
    this.listLesson = [];
    // this.router.navigate(['/list-lesson'], { queryParams: { grade: this.gradeId, subject: this.subjectId, chapter: value, unit: this.unitId } });
    this.globalEndpoint.getCurriculumChildrentNodesByPortalId(this.portalId, value).then(res => {
      res.forEach((i) => {
        this.listLesson.push({ lessonId: i.id, lessonName: i.curriculumName });
      })
    });

  }

  gradeNavigateTo(value) {
    this.gradeId = value;
    this.router.navigate(['/curriculum'], { queryParams: { grade: value } });
  }

  onClickAdvencedSearch() {
    this.advencedSearchDetailOpen = this.advencedSearchDetailOpen == false ? true : false;
  }

  onSelectTeacher(schoolId: number) {
    this.globalEndpoint.getTeacherOptionBySchoolId(this.chooseSchool).then((res) => {
      this.listTeachers = res;
      this.filteredTeachers.next(this.listTeachers);
    })
  }
  onChangeSchool() {
    this.filteredTeachers.next([]);
    this.chooseTeacher = 0;

    this.globalEndpoint.getSchoolOptionsByPortalId(this.portalId, this.divisonId).then(res => {
      this.listSchools = res;
      this.filteredSchools.next(res);
    })
  }


  onClickSearch() {
    this.lessons = [];
    // lấy danh sách bài giảng đổ lên dữ view
    this.lessonEndpoint.getLessonsHomePage(this.portalId, this.divisonId, this.chooseSchool, this.gradeId, this.subjectId, this.chooseTeacher, this.chapterId, this.unitId, null, 1, 9).then(res => {
      if (res) {

        this.totalPage = res.totalItems;
        this.listTeacherId = res.items.map((x) => x.teacherId);
        this.listSubjectId = res.items.map((x) => x.subjectId);

        let listTeacher: TeacherOption[] = [];
        let listSubject: SubjectOption[] = [];

        Promise.all([
          this.globalEndpoint.GetListUserByIds(this.listTeacherId, this.portalId).then(res => {
            listTeacher = res;
          }),
          this.globalEndpoint.getSubjectOptionsByIds(this.listSubjectId, this.portalId).then(res => {
            listSubject = res;
          })
        ]).then((value) => {
          let i = 0;
          res.items.forEach((item) => {
            var teacher = listTeacher.find(x => x.id == item.teacherId);
            var subject = listSubject.find(x => x.id == item.subjectId);
            if (!item.avatarUrl) {
              item.avatarUrl = '/assets/images/lesson/lesson-default.png'
            }
            if (!teacher.avatar) {
              teacher.avatar = '/assets/images/users/user-avartar.png'
            }
            this.lessons.push(LessonsVM.From(item, teacher?.name, subject?.name, teacher?.avatar))
          });
        });
      } else {
        this.lessons = [];
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: "Không tìm thấy bài giảng",
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    })
  }

  onClickPage(pageNum: number) {
    this.listLesssonDashboard = this.listLessons.items.slice((pageNum - 1) * 12, pageNum * 12);
  }

  chosseUnit(unitId) {
    this.unitId = unitId;
    this.lessons = [];
    // lấy danh sách bài giảng đổ lên dữ view
    this.lessonEndpoint.getLessonsHomePage(this.portalId, this.divisonId, this.schoolId, this.gradeId, this.subjectId, null, this.chapterId, unitId, null, 1, 9).then(res => {
      this.totalPage = res.totalItems;

      this.listTeacherId = res.items.map((x) => x.teacherId);
      this.listSubjectId = res.items.map((x) => x.subjectId);

      let listTeacher: TeacherOption[] = [];
      let listSubject: SubjectOption[] = [];

      Promise.all([
        this.globalEndpoint.GetListUserByIds(this.listTeacherId, this.portalId).then(res => {
          listTeacher = res;
        }),
        this.globalEndpoint.getSubjectOptionsByIds(this.listSubjectId, this.portalId).then(res => {
          listSubject = res;
        })
      ]).then((value) => {
        let i = 0;
        res.items.forEach((item) => {
          var teacher = listTeacher.find(x => x.id == item.teacherId);
          var subject = listSubject.find(x => x.id == item.subjectId);
          if (!item.avatarUrl) {
            item.avatarUrl = '/assets/images/lesson/lesson-default.png'
          }
          if (!teacher.avatar) {
            teacher.avatar = '/assets/images/users/user-avartar.png'
          }
          this.lessons.push(LessonsVM.From(item, teacher?.name, subject?.name, teacher?.avatar))
        });
      });

    })

  }
  pageChanged(event) {
    this.page = event;
    this.lessonEndpoint.getLessonsHomePage(this.portalId, this.divisonId, this.chooseSchool, this.gradeId, this.subjectId, this.chooseTeacher, this.chapterId, this.unitId, null, this.page, 9).then(res => {
      if (res) {
        this.totalPage = res.totalItems;
        this.listTeacherId = res.items.map((x) => x.teacherId);
        this.listSubjectId = res.items.map((x) => x.subjectId);

        let listTeacher: TeacherOption[] = [];
        let listSubject: SubjectOption[] = [];

        Promise.all([
          this.globalEndpoint.GetListUserByIds(this.listTeacherId, this.portalId).then(res => {
            listTeacher = res;
          }),
          this.globalEndpoint.getSubjectOptionsByIds(this.listSubjectId, this.portalId).then(res => {
            listSubject = res;
          })
        ]).then((value) => {
          let i = 0;
          res.items.forEach((item) => {
            var teacher = listTeacher.find(x => x.id == item.teacherId);
            var subject = listSubject.find(x => x.id == item.subjectId);
            if (!item.avatarUrl) {
              item.avatarUrl = '/assets/images/lesson/lesson-default.png'
            }
            if (!teacher.avatar) {
              teacher.avatar = '/assets/images/users/user-avartar.png'
            }
            this.lessons.push(LessonsVM.From(item, teacher?.name, subject?.name, teacher?.avatar))
          });
        });
      } else {
        this.lessons = [];
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: "Không tìm thấy bài giảng",
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    })
  }
}

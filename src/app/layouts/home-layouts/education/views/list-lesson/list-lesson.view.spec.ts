import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLessonView } from './list-lesson.view';

describe('ListLessonView', () => {
  let component: ListLessonView;
  let fixture: ComponentFixture<ListLessonView>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListLessonView ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLessonView);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

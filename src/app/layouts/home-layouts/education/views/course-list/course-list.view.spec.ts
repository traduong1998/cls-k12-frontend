import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseListView } from './course-list.view';

describe('CourseListView', () => {
  let component: CourseListView;
  let fixture: ComponentFixture<CourseListView>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseListView ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseListView);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'education-course-list',
  templateUrl: './course-list.view.html',
  styleUrls: ['./course-list.view.scss']
})
export class CourseListView implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

export class CurriculumVM {
    curriculumId: number;
    curriculumName: string;
    lessons: LessonVM[];
    //todo
    static From(curriculumId: number, curriculumName: string, lessons: LessonVM[]) {
        var m: CurriculumVM =
        {
            curriculumId: curriculumId,
            curriculumName: curriculumName,
            lessons: lessons
        }
        return m;
    }
}
export interface LessonVM {
    lessonId: number;
    lessonName: string;
}
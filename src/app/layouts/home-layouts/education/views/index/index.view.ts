import {
  ChangeDetectorRef,
  Component,
  HostListener,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  GlobalEndpoint,
  LessonEndpoint,
  UserEndpoint,
} from 'sdk/cls-k12-sdk-js/src';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { AppConfigService } from 'src/app/core/services';
import { GradeDashboard } from '../../interfaces/gradeDashboard';
import { NewsDashboard } from '../../interfaces/newsDashboard';
import { FeelingStudent } from '../../interfaces/feelingStudent';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { SubjectDashboard } from '../../interfaces/subjectDashboard';
import { NumberPageDashboard } from '../../interfaces/numberPageDashboard';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { TeacherOption } from 'sdk/cls-k12-sdk-js/src/services/user/models/teacher';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CurriculumVM, LessonVM } from './view-models/curriculum.VM';
import { LessonsVM } from '../index/view-models/lesson-vm';
import { interval } from 'rxjs';
import { takeWhile, tap } from 'rxjs/operators';
import { Currculum } from 'sdk/cls-k12-sdk-js/src/services/curriculum/responses/curriculum';
import { UnitConfig } from 'src/app/core/interfaces/app-config';
import { Curriculum } from 'src/app/features/learn/intefaces/curriculum';
import { number } from 'ngx-custom-validators/src/app/number/validator';
import { Console } from 'console';
import { CLIENT_RENEG_LIMIT } from 'tls';

export interface SubjectColor {
  subjectName: string;
  color: string;
}

export const colors = ['#DDD'];

@Component({
  selector: 'education-index',
  templateUrl: './index.view.html',
  styleUrls: ['./index.view.scss'],
})
export class IndexView implements OnInit {
  panelOpenState = false;
  lessonName: string;
  listColor: string[] = [
    '#523FA7',
    '#1C9AEE',
    '#56D02B',
    '#D8DA5D',
    '#FA62D8',
    '#77B5E1',
    '#F89B8C',
    '#64E1B0',
    '#D8DA5D',
    '#FA62D8',
    '#77B5E1',
    '#F89B8C',
    '#64E1B0',
  ];

  itemLastedLesson: number = 4;
  itemLesson: number = 12;
  totalItemLesson: number = 8;
  // proportion: number;
  // proportionBannber: number
  width: number;

  lastedLesson: LessonsVM[] = [];
  lessons: LessonsVM[] = [];
  //resize
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.width = window.innerWidth;
  }
  //model Curriculum
  //fake list to do view
  listCurriculum: CurriculumVM[] = [];

  listGradesDashboard: GradeDashboard[];
  listGrades: GradeOption[];

  listSubjectsDashboard: SubjectDashboard[];
  listSubjects: SubjectOption[];

  listSchools: SchoolOption[] = [];
  currculum: Currculum[];

  listTeachers: TeacherOption[] = [];
  userEndpoint: UserEndpoint;

  listLessons: PaginatorResponse<Lesson>;
  listLessonsLatest: Lesson[];
  listLesssonDashboard: Lesson[];
  lessonEndpoint: LessonEndpoint;

  listLessonCount: number;

  listNews: NewsDashboard[] = [
    {
      avatar:
        'https://images.giaoducthoidai.vn/public/giangntk/2021-04-20/ngay-sach-2.jpg',
      title: 'Học sinh Thủ đô hào hứng trong ngày hội sách',
      summary:
        'GD&TĐ - Sáng 20/4, tại GD&TĐ - Sáng 20/4, tại Trường THCS Ngô Sĩ GD&TĐ - Sáng 20/4, tại Trường THCS Ngô Sĩ Liên tổ chức ngày hội sách với chủ đề “Tự hào truyền thống- Tự tin hội nhậpLiên tổ chức ngày hội sách với chủ đề “Tự hào truyền thống- Tự tin hội nhập Trường THCS Ngô Sĩ Liên tổ chức ngày hội sách với chủ đề “Tự hào truyền thống- Tự tin hội nhập”.',
      date: '20/04/2021',
      link:
        'https://giaoducthoidai.vn/ket-noi/ha-noi-day-manh-van-hoa-doc-trong-truong-hoc-BnmdLQuGg.html',
    },
    {
      avatar:
        'https://images.giaoducthoidai.vn/public/ngandk/2021-04-17/a1-8-(3).jpg',
      title: '“Thuận khuyết tật” tranh tài Olympic Tin học Châu Á',
      summary:
        'GD&TĐ -  GD&TĐ - Sáng 20/4, tại Trường THCS Ngô Sĩ Liên tổ chức ngày hội sách với chủ đề “Tự hào truyền thống- Tự tin hội nhập Thuận bị khuyết tật bẩm sinh. Nhưng bảng thành tích học tập của em không khuyết tật...',
      date: '20/04/2021',
      link:
        'https://giaoducthoidai.vn/tre/thuan-khuyet-tat-tranh-tai-olympic-tin-hoc-chau-a-lKmTLJuGg.html',
    },
    {
      avatar:
        'https://images.giaoducthoidai.vn/public/hanhntd1/2021-04-20/s1.jpg',
      title: 'Lào Cai: Lan toả văn hoá đọc trong Ngày sách Việt Nam',
      summary:
        'GD&TĐ - Sáng 20/4 tại Trường THPT Chuyên Lào Cai (LàGD&TĐ - Sáng 20/4, tại Trường THCS Ngô Sĩ Liên tổ chức ngày hội sách với chủ đề “Tự hào truyền thống- Tự tin hội nhậpo Cai), Sở GD&ĐT Lào Cai phối hợp với Sở TT&TT ...',
      date: '20/04/2021',
      link:
        'https://giaoducthoidai.vn/ket-noi/lao-cai-lan-toa-van-hoa-doc-trong-ngay-sach-viet-nam-6CPlUQXMg.html',
    },
    {
      avatar:
        'https://images.giaoducthoidai.vn/public/thuyvt/2021-04-19/image001.jpg',
      title:
        'Tuyên dương 3 em học sinh nhặt được của rơi trả lại người mất cho người ta cho người ta',
      summary:
        'GD&TĐ - Nhặt GD&TĐ - Sáng 20/4, tại Trường THCS Ngô Sĩ Liên tổ chức ngày hội sách với chủ đề “Tự hào truyền thống- Tự tin hội nhậpđược điên thoại trên đường đi học về, 3 em học sinh lớp 4 ở huyện Chi Lăng, Lạng Sơn...',
      date: '19/04/2021',
      link:
        'https://giaoducthoidai.vn/nhan-ai/tuyen-duong-3-em-hoc-sinh-nhat-duoc-cua-roi-tra-lai-nguoi-mat-B8brPyXGR.html',
    },
  ];

  listFeelingStudent: FeelingStudent[] = [
    {
      avatar: '/assets/images/background/avatar-sudent-1.png',
      name: 'Hoàng Diệu Nhi',
      role: 'Học viên',
      feeling:
        'Cách giảng dạy và tài liệu cũng như các bài giảng tôi cảm thấy rất hay và tính ứng dụng cao vào thực tế. Cách thực hành theo nhóm, mô phỏng công ty, các phòng ban cho tôi cái nhìn rõ hơn về cách quản lý, hoạt động sản xuất hiệu quả của một công ty là như thế nào',
    },
    {
      avatar: '/assets/images/background/avatar-sudent-2.png',
      name: 'Hoàng Bá Tùng',
      role: 'Học viên',
      feeling:
        'Nhiều bài học bổ ích, giao diện tuyệt vời.Hình ảnh sinh động, kiến thức bổ ích. Các bài học sinh động, nội dung rất thú vị.',
    },
    {
      avatar: '/assets/images/background/lesson-avatar-user.png',
      name: 'Hoàng Ngọc Bảo',
      role: 'Học viên',
      feeling:
        'Khoá học đã giúp tôi cải thiện kỹ năng tiếng Anh và tự tin hơn rất nhiều khi đứng đối diện với các bạn nước ngoài, cảm giác tự tin này khiến hiệu quả công việc tăng lên đáng kể, tôi đang theo học tại lớp Inter và sẽ tiếp tục theo lên các khoá cao hơn.',
    },
  ];

  isLoadingResults = false;

  queryParamListLessonHomePage: any;
  queryParamListLessonLatestHomePage: any;

  gradeId: number;

  chapterId: number;

  subjectId: number;

  selectChapter: number;

  unitId: number;

  pageId: number;

  advencedSearchDetailOpen = false;

  totalPage: number;
  listPage: NumberPageDashboard[] = [];
  showButtonNextPage: boolean = false;
  selected: number;

  selectedcurriculum: number;
  currentSubject: number = 0;
  curriculumId: number;
  curriculumName: string;

  //
  listTeacherId: number[];
  listSubjectId: number[];

  portalId: number;
  schoolId: number;
  divisonId: number;
  private globalEndpoint: GlobalEndpoint;
  unitInfor: UnitConfig;

  //scroll to top
  isShow: boolean;
  isReady: boolean = false;
  topPosToStartShowing = 100;
  bannerUrl: string = 'url(';
  // TODO: Cross browsing
  gotoTop() {
    document.getElementsByTagName('mat-drawer-content')[0].scrollTo(0, 0);
  }

  gotoGrade() {
    let elemenent = document.getElementById('mat-drawer-content');
    elemenent && elemenent.scrollIntoView({ behavior: 'smooth' });
  }

  onScrollClick(el: HTMLElement) {
    el.scrollIntoView();
  }

  gradeClick(grade) {
    this.gradeId = grade;
    this.router.navigate(['/index'], { queryParams: { gradeId: this.gradeId, lessonName: null } })
    localStorage.setItem('gradeId', JSON.stringify(this.gradeId));

    this.listGradesDashboard.forEach((item) => {
      if (item.id === this.gradeId) {
        item.isActived = true;
      }
      else {
        item.isActived = false;
      }
    });
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _snackBar: MatSnackBar,
    private configService: AppConfigService,
    private cdRef: ChangeDetectorRef
  ) {
    this.onResize();
    this.listGradesDashboard = [];
    this.listSubjectsDashboard = [];
    this.lessonEndpoint = new LessonEndpoint();
    this.globalEndpoint = new GlobalEndpoint();

    this.portalId = this.configService.getConfig().unit.portalId;
    this.divisonId = this.configService.getConfig().unit.divisionId;
    this.schoolId = this.configService.getConfig().unit.schoolId;
    this.unitInfor = this.configService.getConfig().unit.config;
    this.isReady = true;
    this.bannerUrl += this.unitInfor.desktopBanner + ')';
    // lấy khung chương trình
    Promise.resolve()
      .then(() => {
        return this.globalEndpoint.getGradeOptionsByDomain(location.hostname);
      })
      .then((res) => {
          this.selected = res[0].id;
        if (!JSON.parse(localStorage.getItem('gradeId'))){
          this.gradeId = res[0].id;
        }
        else{
          this.gradeId = JSON.parse(localStorage.getItem('gradeId'));

        }
        res.forEach((grade) => {
          this.listGradesDashboard.push(new GradeDashboard(grade));
        });

        return this.globalEndpoint.getSubjectOptionsByGrade(
          this.portalId,
          this.gradeId
        );
      })
      .then((res) => {
        this.subjectId = res[0].id;
        res.forEach((subject) => {
          this.listSubjectsDashboard.push(new SubjectDashboard(subject));
        });
        return this.globalEndpoint.getCurriculum(
          location.hostname,
          this.divisonId,
          this.schoolId,
          this.gradeId,
          this.subjectId
        );
      })
      .then((res) => {
        if (res.length > 0) {
          this.currculum = res;
          this.selectedcurriculum = res[0].curriculumId;
          this.curriculumId = res[0].curriculumId;

          //this.curriculumName = res[0].curriculumName;
        } else {
          this.curriculumId = undefined;
          this.curriculumName = '';
          this.listCurriculum = [];
        }
      })
      .then(() => {
        if (this.curriculumId) {
          //lấy chương
          this.globalEndpoint
            .getCurriculumChildrentNodesByPortalId(
              this.portalId,
              this.curriculumId
            )
            .then((res) => {
              // lây tiếp danh sách bài học
              let i = 0;

              res.forEach((item) => {
                let listLesson: LessonVM[] = [];
                this.globalEndpoint
                  .getCurriculumChildrentNodesByPortalId(this.portalId, item.id)
                  .then((res) => {
                    res.forEach((i) => {
                      listLesson.push({
                        lessonId: i.id,
                        lessonName: i.curriculumName,
                      });
                    });
                  });
                if (i < 3) {
                  this.listCurriculum.push(
                    CurriculumVM.From(item.id, item.curriculumName, listLesson)
                  );
                  i++;
                }
              });
            });
        }
      })
      .then(() => {
        this.getListLessons();
      })
      .then(() => {
        //lấy danh sách môn học
        this.globalEndpoint
          .getSubjectOptionsByGrade(this.portalId, this.gradeId)
          .then((res) => {
            this.listSubjects = res;
          });
      })
      .catch((err) => {});
  }

  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }

  getListLessons() {
    this.lastedLesson = [];
    this.lessons = [];
    // lấy danh sách bài giảng đổ lên dữ view
    this.lessonEndpoint
      .getLessonsHomePage(
        this.portalId,
        this.divisonId,
        this.schoolId,
        this.gradeId,
        null,
        null,
        null,
        null,
        null,
        0,
        100
      )
      .then((res) => {
        this.listTeacherId = res.items.map((x) => x.teacherId);
        this.listSubjectId = res.items.map((x) => x.subjectId);

        let listTeacher: TeacherOption[] = [];
        let listSubject: SubjectOption[] = [];

        Promise.all([
          this.globalEndpoint
            .GetListUserByIds(this.listTeacherId, this.portalId)
            .then((res) => {
              listTeacher = res;
            }),
          this.globalEndpoint
            .getSubjectOptionsByIds(this.listSubjectId, this.portalId)
            .then((res) => {
              listSubject = res;
            }),
        ]).then((value) => {
          let i = 0;

          res.items.forEach((item) => {
            var teacher = listTeacher.find((x) => x.id == item.teacherId);
            var subject = listSubject.find((x) => x.id == item.subjectId);

            if (!item.avatarUrl) {
              item.avatarUrl = '/assets/images/lesson/lesson-default.png';
            }
            if (!teacher.avatar) {
              teacher.avatar = '/assets/images/users/user-avartar.png';
            }
            if (item.interval > 0) {
              item.endDate = this.addDays(
                new Date(item.createdDate),
                item.interval
              );
              if (new Date(item.endDate) < new Date()) {
                return;
              }
            }
            if (i < this.itemLastedLesson) {
              this.lastedLesson.push(
                LessonsVM.From(
                  item,
                  teacher?.name,
                  subject?.name,
                  teacher?.avatar
                )
              );
              ++i;
            } else {
              if (i < 12) {
                this.lessons.push(
                  LessonsVM.From(
                    item,
                    teacher?.name,
                    subject?.name,
                    teacher?.avatar
                  )
                );
              }
              i++;
            }
          });
        });
      });
  }

  ngOnInit(): void {}
  ngAfterViewInit(): void {
    if (this.listGradesDashboard.find((x) => x.isActived)) {
      // this.gotoGrade()
    }
    this.cdRef.detectChanges();
  }

  scrollLeft(el: Element) {
    const animTimeMs = 400;
    const pixelsToMove = 315;
    const stepArray = [0.001, 0.021, 0.136, 0.341, 0.341, 0.136, 0.021, 0.001];
    interval(animTimeMs / 8)
      .pipe(
        takeWhile((value) => value < 8),
        tap((value) => (el.scrollLeft -= pixelsToMove * stepArray[value]))
      )
      .subscribe();
  }

  scrollRight(el: Element) {
    const animTimeMs = 400;
    const pixelsToMove = 315;
    const stepArray = [0.001, 0.021, 0.136, 0.341, 0.341, 0.136, 0.021, 0.001];
    interval(animTimeMs / 8)
      .pipe(
        takeWhile((value) => value < 8),
        tap((value) => (el.scrollLeft += pixelsToMove * stepArray[value]))
      )
      .subscribe();
  }
  //thay đổi khối => dổi môn => đổi khung
  gradeNavigateTo(value: number) {
    this.selected = value;
    this.gradeId = value;
    this.listSubjectsDashboard = [];
    this.currculum = [];
    this.listCurriculum = [];
    this.listSubjects = [];
    this.router.navigate(['/index']);

    // scroll grades
    this.gotoGrade();
    this.gradeClick(value);

    //lấy cái khung chương trình
    Promise.resolve()
      .then(() => {
        this.globalEndpoint
          .getSubjectOptionsByGrade(this.portalId, this.gradeId)
          .then((res) => {
            this.listSubjects = res;
          });
      })
      .then(() => {
        this.lessons = [];
        this.lastedLesson = [];
        // lấy danh sách bài giảng đổ lên dữ view

        this.getListLessons();
      })
      .then(() => {
        // lây môn
        return this.globalEndpoint.getSubjectOptionsByGrade(
          this.portalId,
          value
        );
      })
      .then((res) => {
        this.currentSubject = 0;
        this.subjectId = res[0].id;
        res.forEach((subject) => {
          this.listSubjectsDashboard.push(new SubjectDashboard(subject));
        });

        return this.globalEndpoint.getCurriculum(
          location.hostname,
          this.divisonId,
          this.schoolId,
          this.gradeId,
          this.subjectId
        );
      })
      .then((res) => {
        if (res) {
          this.currculum = res;
          this.curriculumId = res[0].curriculumId;
          this.selectedcurriculum = res[0].curriculumId;
          this.curriculumName = res[0].curriculumName;
        } else {
          this.curriculumName = '';
          this.listCurriculum = [];
        }
      })
      .then(() => {
        if (this.curriculumId) {
          //lấy chương
          this.globalEndpoint
            .getCurriculumChildrentNodesByPortalId(
              this.portalId,
              this.curriculumId
            )
            .then((res) => {
              // lây tiếp danh sách bài học
              let i = 0;
              res.forEach((item) => {
                let listLesson: LessonVM[] = [];

                this.globalEndpoint
                  .getCurriculumChildrentNodesByPortalId(this.portalId, item.id)
                  .then((res) => {
                    res.forEach((i) => {
                      listLesson.push({
                        lessonId: i.id,
                        lessonName: i.curriculumName,
                      });
                    });
                  });
                if (i < 3) {
                  this.listCurriculum.push(
                    CurriculumVM.From(item.id, item.curriculumName, listLesson)
                  );
                  i++;
                }
              });
            });
        }
      })
      .then(() => {})
      .catch((err) => {
        console.log(err);
      });
  }
  //thay đổi khung chương trình
  curriculumChange(value) {
    this.selectedcurriculum = value;
    this.listCurriculum = [];
    console.log('value change', value);
    // lấy chưuowng
    this.globalEndpoint
      .getCurriculumChildrentNodesByPortalId(this.portalId, value)
      .then((res) => {
        // lây tiếp danh sách bài học

        let i = 0;
        res.forEach((item) => {
          let listLesson: LessonVM[] = [];

          this.globalEndpoint
            .getCurriculumChildrentNodesByPortalId(this.portalId, item.id)
            .then((res) => {
              res.forEach((i) => {
                listLesson.push({
                  lessonId: i.id,
                  lessonName: i.curriculumName,
                });
              });
            });
          if (i < 3) {
            this.listCurriculum.push(
              CurriculumVM.From(item.id, item.curriculumName, listLesson)
            );
            i++;
          }
        });
      });
  }

  choseCurriculum(subject, index) {
    this.subjectId = subject.id;
    this.listCurriculum = [];
    this.curriculumId = undefined;
    this.selectedcurriculum = 0;
    this.currentSubject = index;
    this.currculum = [];
    //lấy cái khung chương trình
    Promise.resolve()
      .then(() => {
        return this.globalEndpoint.getCurriculum(
          location.hostname,
          this.divisonId,
          this.schoolId,
          this.gradeId,
          subject.id
        );
      })
      .then((res) => {
        if (res) {
          this.currculum = res;
          this.selectedcurriculum = res[0].curriculumId;
          this.curriculumId = res[0].curriculumId;
        } else {
          this.listCurriculum = [];
        }
      })
      .then(() => {
        if (this.curriculumId) {
          //lấy chương
          this.globalEndpoint
            .getCurriculumChildrentNodesByPortalId(
              this.portalId,
              this.curriculumId
            )
            .then((res) => {
              // lây tiếp danh sách bài học
              var i = 0;
              res.forEach((item) => {
                let listLesson: LessonVM[] = [];

                this.globalEndpoint
                  .getCurriculumChildrentNodesByPortalId(this.portalId, item.id)
                  .then((res) => {
                    res.forEach((i) => {
                      listLesson.push({
                        lessonId: i.id,
                        lessonName: i.curriculumName,
                      });
                    });
                  });
                if (i < 3) {
                  this.listCurriculum.push(
                    CurriculumVM.From(item.id, item.curriculumName, listLesson)
                  );
                  i++;
                }
              });
            });
        }
      })
      .catch((err) => {});
  }

  openSnarBar(message: string, color?: string) {
    this._snackBar.open(message, 'Ok', {
      duration: 3000,
      panelClass: [color ?? 'green-snackbar'],
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  seeMore() {
    //fake route see more? query params??
    this.router.navigate(['curriculum'], {
      queryParams: {
        gradeId: this.gradeId,
        subjectId: this.subjectId,
        currculumId: this.selectedcurriculum,
      },
    });
  }

  seeAllLesson() {
    this.router.navigate(['list-lessons-home-page'], {
      queryParams: { gradeId: this.gradeId },
    });
  }

  choseSubject(subjectId) {
    this.lessons = [];
    this.subjectId = subjectId;

    this.lessonEndpoint
      .getLessonsHomePage(
        this.portalId,
        this.divisonId,
        this.schoolId,
        this.gradeId,
        this.subjectId,
        null,
        null,
        null,
        null,
        0,
        1000
      )
      .then((res) => {
        this.listTeacherId = res.items.map((x) => x.teacherId);
        this.listSubjectId = res.items.map((x) => x.subjectId);

        let listTeacher: TeacherOption[] = [];
        let listSubject: SubjectOption[] = [];

        Promise.all([
          this.globalEndpoint
            .GetListUserByIds(this.listTeacherId, this.portalId)
            .then((res) => {
              listTeacher = res;
            }),
          this.globalEndpoint
            .getSubjectOptionsByIds(this.listSubjectId, this.portalId)
            .then((res) => {
              listSubject = res;
            }),
        ]).then((value) => {
          let i = 0;

          res.items.forEach((item) => {
            var teacher = listTeacher.find((x) => x.id == item.teacherId);
            var subject = listSubject.find((x) => x.id == item.subjectId);

            if (!item.avatarUrl) {
              item.avatarUrl = '/assets/images/lesson/lesson-default.png';
            }
            if (!teacher.avatar) {
              teacher.avatar = '/assets/images/users/user-avartar.png';
            }
            if (item.interval > 0) {
              item.endDate = this.addDays(
                new Date(item.createdDate),
                item.interval
              );
              if (new Date(item.endDate) < new Date()) {
                return;
              }
            }
            if (i < 8) {
              this.lessons.push(
                LessonsVM.From(
                  item,
                  teacher?.name,
                  subject?.name,
                  teacher?.avatar
                )
              );
              i++;
            }
          });
        });
      });
  }
  searchLesson() {
    this.router.navigate(['list-lessons-home-page'], {
      queryParams: { gradeId: this.gradeId, lessonName: this.lessonName },
    });
  }

  seeLessonOfCurriculum(chapterId, unitId) {
    this.router.navigate(['list-lesson'], {
      queryParams: {
        gradeId: this.gradeId,
        subjectId: this.subjectId,
        curriculumId: this.selectedcurriculum,
        chapterId: chapterId,
        unitId: unitId,
      },
    });
  }
}

import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject, Subscription } from 'rxjs';
import { GlobalEndpoint, GradeEndpoint, LessonEndpoint, SchoolEndpoint, SubjectEndpoint, UserEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { TeacherOption } from 'sdk/cls-k12-sdk-js/src/services/user/models/teacher';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { GradeDashboard } from '../../interfaces/gradeDashboard';
import { LessonsVM } from './view-models/lesson-vm';


@Component({
  selector: 'app-list-lessons-home-page',
  templateUrl: './list-lessons-home-page.view.html',
  styleUrls: ['./list-lessons-home-page.view.scss']
})
export class ListLessonsHomePageView implements OnInit {
  itemsPerPage = 8;
  routeSub: Subscription;
  currentPage = 0;
  totalPage = 11;
  width: number
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.width = window.innerWidth;
  }
  listColor: string[] = ['#523FA7', '#1C9AEE', '#56D02B', '#D8DA5D', '#FA62D8', '#77B5E1', '#F89B8C', '#64E1B0', '#56D02B', '#D8DA5D', '#FA62D8', '#77B5E1', '#F89B8C', '#64E1B0']
  page: number = 1;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  @ViewChild("schoolSelect") schoolSelect: MatSelect;
  filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  chooseSchool: number = 0;
  keyWordSchool: string = "";

  @ViewChild("gradeSelect") gradeSelect: MatSelect;
  filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  chooseGrade: number = 0;
  keyWordGrade: string = "";

  @ViewChild("subjectSelect") subjectSelect: MatSelect;
  filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  chooseSubject: number = 0;
  keyWordSubject: string = "";

  @ViewChild("teacherSelect") teacherSelect: MatSelect;
  filteredTeachers: ReplaySubject<TeacherOption[]> = new ReplaySubject<TeacherOption[]>(1);
  chooseTeacher: number;
  keyWordTeacher: string = "";

  listGrades: GradeOption[];
  gradeEndpoint: GradeEndpoint;

  listSubjects: SubjectOption[];
  subjectEndpoint: SubjectEndpoint;


  listSchools: SchoolOption[] = [];
  schoolEndpoint: SchoolEndpoint;

  listTeachers: TeacherOption[] = [];
  userEndpoint: UserEndpoint;

  lessonEndpoint: LessonEndpoint;

  queryParamListLessonHomePage: any;
  isLoadingResults = false;


  listTeacherId: number[];
  listSubjectId: number[];

  lessons: LessonsVM[] = [];
  listGradesDashboard: GradeDashboard[] = [];

  baseApiUrl = 'http://192.169.0.108:8010';


  portalId: number;
  schoolId: number;
  divisonId: number;

  gradeId: number = 0;
  teacherId: number;
  subjectId: number;


  schoolName: string = "";
  gradeName: string = "";
  subjectName: string = "";
  teacherName: string = "";
  lessonName: string = '';
  selected: number;
  isDomaimHasSchool = false;
  private globalEndpoint: GlobalEndpoint;

  constructor(private route: ActivatedRoute, private router: Router, private _authService: AuthService, private _snackBar: MatSnackBar, private configService: AppConfigService) {
    this.onResize();
    this.schoolEndpoint = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.gradeEndpoint = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.subjectEndpoint = new SubjectEndpoint({ baseUrl: this.baseApiUrl });
    this.userEndpoint = new UserEndpoint({ baseUrl: this.baseApiUrl });
    this.lessonEndpoint = new LessonEndpoint({ baseUrl: this.baseApiUrl });

    this.routeSub = this.route.queryParams.subscribe(params => {
      this.lessonName = params['lessonName'];
      this.gradeId = +params['gradeId'];
    });

    this.globalEndpoint = new GlobalEndpoint();
    this.portalId = this.configService.getConfig().unit.portalId;
    this.schoolId = this.configService.getConfig().unit.schoolId;
    this.divisonId = this.configService.getConfig().unit.divisionId;
    //fake
    if (this.schoolId) {
      this.isDomaimHasSchool = true;
      this.chooseSchool = this.schoolId;
    }

    // lấy danh sách khối
    this.globalEndpoint.getGradeOptionsByDomain(location.hostname)
      .then(res => {
        this.selected = res[0].id;
        if (isNaN(this.gradeId)) {
          this.gradeId = res[0].id;
          this.selected = res[0].id;
        }
        res.forEach((grade) => {
          this.listGradesDashboard.push(new GradeDashboard(grade));
        });
      })
      .then(() => {
        //lấy danh sách bài giảng
        this.getListLesson();
      })
  }

  getListLesson() {
    this.lessonEndpoint.getLessonsHomePage(this.portalId, this.divisonId, this.chooseSchool, this.gradeId, null, null, null, null, this.lessonName, this.currentPage, this.itemsPerPage).then(res => {
      this.totalPage = res.totalItems;
      this.listTeacherId = res.items.map((x) => x.teacherId);
      this.listSubjectId = res.items.map((x) => x.subjectId);

      let listTeacher: TeacherOption[] = [];
      let listSubject: SubjectOption[] = [];

      Promise.all([
        this.globalEndpoint.GetListUserByIds(this.listTeacherId, this.portalId).then(res => {
          listTeacher = res;

        }),
        this.globalEndpoint.getSubjectOptionsByIds(this.listSubjectId, this.portalId).then(res => {
          listSubject = res;
        })
      ]).then((value) => {
        let i = 0;
        res.items.forEach((item) => {
          var teacher = listTeacher.find(x => x.id == item.teacherId);
          var subject = listSubject.find(x => x.id == item.subjectId);

          if (!item.avatarUrl) {
            item.avatarUrl = '/assets/images/lesson/lesson-default.png'
          }
          if (!teacher.avatar) {
            teacher.avatar = '/assets/images/users/user-avartar.png'
          }
          if (item.interval > 0) {
            item.endDate = this.addDays(new Date(item.createdDate), item.interval);
          }
          this.lessons.push(LessonsVM.From(item, teacher?.name, subject?.name, teacher?.avatar));
        });
      });
    });
  }


  ngOnInit(): void {
  }

  stopPropagation(event) {
    event.stopPropagation();
    // console.log("Clicked!");
  }

  selectName() {
    if (this.chooseSchool > 0) {
      this.schoolName = this.listSchools.find(x => x.id == this.chooseSchool).name;
    }

    if (this.chooseGrade > 0) {
      this.gradeName = this.listGrades.find(x => x.id == this.chooseGrade).name;
    }

    if (this.chooseSubject > 0) {
      this.schoolName = this.listSubjects.find(x => x.id == this.chooseSubject).name;
    }

    if (this.chooseTeacher > 0) {
      this.teacherName = this.listTeachers.find(x => x.id == this.chooseTeacher).name;
    }
  }

  onChangeSchool() {
    // gán trường
    this.chooseGrade = 0;
    this.chooseSubject = 0;
    this.filteredTeachers.next([]);
    this.chooseTeacher = 0;
  }

  onSelectSchool() {
    this.globalEndpoint.getSchoolOptionsByGradeId(this.portalId, this.divisonId, this.gradeId).then(res => {
      this.listSchools = res;
      this.filteredSchools.next(res);
    })
  }

  onSelectGrade(schoolId: number) {
    this.globalEndpoint.getGradeOptionsByPortalId(this.portalId, this.chooseSchool).then(res => {
      this.listGrades = res
      this.filteredGrades.next(res);
    })
  }

  onSelectSubject() {
    this.globalEndpoint.getSubjectOptionsByGrade(this.portalId, this.gradeId).then(res => {
      this.listSubjects = res
      this.filteredSubjects.next(res);
    })
  }

  onSelectTeacher(schoolId: number) {
    this.globalEndpoint.getTeacherOptionBySchoolId(this.chooseSchool).then((res) => {
      this.listTeachers = res;
      this.filteredTeachers.next(this.listTeachers);
    })
  }

  filterSchool() {
    if (!this.listSchools) {
      return;
    }
    // get the search keyword
    let search = this.keyWordSchool;
    if (!search) {
      this.filteredSchools.next(this.listSchools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredSchools.next(
      this.listSchools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  filterGrade() {
    if (!this.listGrades) {
      return;
    }
    // get the search keyword
    let search = this.keyWordGrade;
    if (!search) {
      this.filteredGrades.next(this.listGrades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredGrades.next(
      this.listGrades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  filterSubject() {
    if (!this.listSubjects) {
      return;
    }
    // get the search keyword
    let search = this.keyWordSubject;
    if (!search) {
      this.filteredSubjects.next(this.listSubjects.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredSubjects.next(
      this.listSubjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }

  filterTeacher() {
    if (!this.listTeachers) {
      return;
    }
    // get the search keyword
    let search = this.keyWordTeacher;
    if (!search) {
      this.filteredTeachers.next(this.listTeachers.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredTeachers.next(
      this.listTeachers.filter(teacher => teacher.name.toLowerCase().indexOf(search) > -1)
    );
  }

  gotoGrade() {
    let elemenent = document.getElementById('mat-drawer-content');
    elemenent && elemenent.scrollIntoView({ behavior: 'smooth' });
  }

  onScrollClick(el: HTMLElement) {
    el.scrollIntoView()
  }

  onClickSearch() {
    // this.currentPage = 0;

    if (this.width > 1440) {
      this.totalPage = 11;
    } else {
      this.totalPage = 13;
    }

    this.lessons = [];
    if (this.chooseSchool == 0) {
      this.schoolName = '';
    }

    this.lessonEndpoint.getLessonsHomePage(this.portalId, this.divisonId, this.chooseSchool, this.gradeId, this.chooseSubject, this.chooseTeacher, null, null, this.lessonName, this.currentPage, this.itemsPerPage).then(res => {
      if (res.totalItems > 0) {
        this.totalPage = res.totalItems;
        this.listTeacherId = res.items.map((x) => x.teacherId);
        this.listSubjectId = res.items.map((x) => x.subjectId);

        let listTeacher: TeacherOption[] = [];
        let listSubject: SubjectOption[] = [];

        Promise.all([
          this.globalEndpoint.GetListUserByIds(this.listTeacherId, this.portalId).then(res => {
            listTeacher = res;
          }),
          this.globalEndpoint.getSubjectOptionsByIds(this.listSubjectId, this.portalId).then(res => {
            listSubject = res;
          })
        ]).then((value) => {
          res.items.forEach((item) => {
            var teacher = listTeacher.find(x => x.id == item.teacherId);
            var subject = listSubject.find(x => x.id == item.subjectId);

            if (!item.avatarUrl) {
              item.avatarUrl = '/assets/images/lesson/lesson-default.png'
            }
            if (!teacher.avatar) {
              teacher.avatar = '/assets/images/users/user-avartar.png'
            }
            if (item.interval > 0) {
              item.endDate = this.addDays(new Date(item.createdDate), item.interval);
            }
            this.lessons.push(LessonsVM.From(item, teacher?.name, subject?.name, teacher?.avatar))
          });
        });
      } else {
        this.lessons = [];
        this.selectName();
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: "Không tìm thấy bài giảng",
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    })
  }

  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }
  pageChanged(event) {

    this.page = event;
    this.lessons = [];
    //lấy danh sách bài giảng
    this.lessonEndpoint.getLessonsHomePage(this.portalId, this.divisonId, this.chooseSchool, this.gradeId, this.chooseSubject, this.chooseTeacher, null, null, this.lessonName, event, this.itemsPerPage)
      .then(res => {

        this.totalPage = res.totalItems;

        this.listTeacherId = res.items.map((x) => x.teacherId);
        this.listSubjectId = res.items.map((x) => x.subjectId);

        let listTeacher: TeacherOption[] = [];
        let listSubject: SubjectOption[] = [];

        Promise.all([
          this.globalEndpoint.GetListUserByIds(this.listTeacherId, this.portalId).then(res => {
            listTeacher = res;
          }),
          this.globalEndpoint.getSubjectOptionsByIds(this.listSubjectId, this.portalId).then(res => {
            listSubject = res;
          })
        ]).then((value) => {
          let i = 0;
          res.items.forEach((item) => {
            var teacher = listTeacher.find(x => x.id == item.teacherId);
            var subject = listSubject.find(x => x.id == item.subjectId);
            if (!item.avatarUrl) {
              item.avatarUrl = '/assets/images/lesson/lesson-default.png'
            }
            if (!teacher.avatar) {
              teacher.avatar = '/assets/images/users/user-avartar.png'
            }
            if (item.interval > 0) {
              item.endDate = this.addDays(new Date(item.createdDate), item.interval);
            }
            this.lessons.push(LessonsVM.From(item, teacher?.name, subject?.name, teacher?.avatar));
          });
        });
      })
  }

  gradeClick(grade) {
    this.currentPage = 0;
    this.page = 1;
    this.gradeId = grade;
    this.listSchools = [];
    this.lessons = [];
    this.gotoGrade();
    this.lessonName = undefined;
    if (this.width > 1440) {
      this.totalPage = 11;
    } else {
      this.totalPage = 13;
    }

    this.router.navigate(['/list-lessons-home-page'], { queryParams: { gradeId: this.gradeId, lessonName: null } })
    localStorage.setItem('gradeId', JSON.stringify(this.gradeId));

    this.listGradesDashboard.forEach((item) => {
      if (item.id === this.gradeId) {
        item.isActived = true;
      }
      else {
        item.isActived = false;
      }
    });
    // lấy trường
    this.globalEndpoint.getSchoolOptionsByGradeId(this.portalId, this.divisonId, this.gradeId).then(res => {
      this.listSchools = res;
      this.filteredSchools.next(res);
    })

    // lấy bài giảng mới => trường => khối
    this.lessonEndpoint.getLessonsHomePage(this.portalId, this.divisonId, this.chooseSchool, this.gradeId, null, null, null, null, this.lessonName, this.currentPage, this.itemsPerPage).then(res => {


      this.totalPage = res.totalItems;

      this.listTeacherId = res.items.map((x) => x.teacherId);
      this.listSubjectId = res.items.map((x) => x.subjectId);

      let listTeacher: TeacherOption[] = [];
      let listSubject: SubjectOption[] = [];

      Promise.all([
        this.globalEndpoint.GetListUserByIds(this.listTeacherId, this.portalId).then(res => {
          listTeacher = res;

        }),
        this.globalEndpoint.getSubjectOptionsByIds(this.listSubjectId, this.portalId).then(res => {
          listSubject = res;
        })
      ]).then(() => {
        res.items.forEach((item) => {
          var teacher = listTeacher.find(x => x.id == item.teacherId);
          var subject = listSubject.find(x => x.id == item.subjectId);
          if (!item.avatarUrl) {
            item.avatarUrl = '/assets/images/lesson/lesson-default.png'
          }
          if (!teacher.avatar) {
            teacher.avatar = '/assets/images/users/user-avartar.png'
          }
          if (item.interval > 0) {
            item.endDate = this.addDays(new Date(item.createdDate), item.interval);
          }
          this.lessons.push(LessonsVM.From(item, teacher?.name, subject?.name, teacher?.avatar));
        });
      });
    });
  }
}

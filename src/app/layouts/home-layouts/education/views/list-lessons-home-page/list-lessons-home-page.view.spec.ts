import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLessonsHomePageView } from './list-lessons-home-page.view';

describe('ListLessonsHomePageComponent', () => {
  let component: ListLessonsHomePageView;
  let fixture: ComponentFixture<ListLessonsHomePageView>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListLessonsHomePageView ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLessonsHomePageView);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

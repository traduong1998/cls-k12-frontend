import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { EducationRoutingModule } from './education-routing.module';
import { EducationComponent } from './education.component';
import { CourseListView } from './views/course-list/course-list.view';
import { HomeLayoutSharedModule } from '../../../core/layouts/home-layouts/home-layout-shared.module';
import { IndexView } from './views/index/index.view';
import { CurriculumView } from './views/curriculum/curriculum.view';
import { EducationBreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { GradeDashboardComponent } from './components/grade-dashboard/grade-dashboard.component';
import { ListLessonView } from './views/list-lesson/list-lesson.view';
import { DetailLessonView } from './views/detail-lesson/detail-lesson.view';
import { ListLessonsHomePageView } from './views/list-lessons-home-page/list-lessons-home-page.view';

@NgModule({
  declarations: [EducationComponent, IndexView, CourseListView, CurriculumView, EducationBreadcrumbComponent, GradeDashboardComponent, ListLessonView, DetailLessonView, ListLessonsHomePageView],
  imports: [
    HomeLayoutSharedModule,
    EducationRoutingModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class EducationModule { }

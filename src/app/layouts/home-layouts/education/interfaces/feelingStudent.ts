export class FeelingStudent{
    avatar:string;
    name:string;
    role:string;
    feeling:string;
    evaluate?:number;
}
import { GradeOption } from "sdk/cls-k12-sdk-js/src/services/grade/models/Grade";

export class GradeDashboard {
    id: number;
    name: string;
    isActived: boolean;
    
    constructor(grade) {
        this.id = grade.id;
        this.name = grade.gradeName;
        this.isActived = false;
    }
}
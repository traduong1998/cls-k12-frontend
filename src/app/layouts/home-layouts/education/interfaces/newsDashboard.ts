export class NewsDashboard{
    avatar:string;
    title:string;
    summary:string;
    date:string;
    link:string;
} 
import { SubjectOption } from "sdk/cls-k12-sdk-js/src/services/subject/models/Subject";

export class SubjectDashboard {
    id: number;
    icon: string;
    name: string;
    isActived: boolean;

    constructor(subject) {
        this.id = subject.id;
        this.name = subject.name;
        this.isActived = false;
        if (subject.icon) {
            this.icon = subject.icon;
        }
        else {
            this.icon = 'elective';
        }

    }
}
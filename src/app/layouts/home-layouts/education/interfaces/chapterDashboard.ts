import { UnitDashboard } from "./unitDashboard";

export class ChapterDashboard {
    id: number;
    name: string;
    listUnits: UnitDashboard[];
}
export class LessonDashboard {
    id?: number;
    name?: string;
    avatar?: string;
    userId?: number;
    userName?: string;
    userAvatar?: string;
    subjectId?: number;
    subjectName?:string;
    subjectColor?: string;
    subjectBanner?:string;
    publicationDate?:string;
    learnTime?:string;
}
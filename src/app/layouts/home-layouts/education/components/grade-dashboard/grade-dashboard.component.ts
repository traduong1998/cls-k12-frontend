import { Component, OnInit } from '@angular/core';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { GradeEndpoint } from 'sdk/cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';

@Component({
  selector: 'education-grade-dashboard',
  templateUrl: './grade-dashboard.component.html',
  styleUrls: ['./grade-dashboard.component.scss']
})
export class GradeDashboardComponent implements OnInit {

  listGrades: GradeOption[];

  gradeEndpoint: GradeEndpoint;

  constructor() {

    //get listGrades từ API
    
    // this.gradeEndpoint.getListGrade().then((res) => {
    //   this.listGrades = res;
    // })

    //fake data listGrades
    this.listGrades = [
      {
        id: 1,
        name: "KHỐI 1"
      },
      {
        id: 2,
        name: "KHỐI 2"
      },
      {
        id: 3,
        name: "KHỐI 3"
      },
      {
        id: 4,
        name: "KHỐI 4"
      },
      {
        id: 5,
        name: "KHỐI 5"
      },
      {
        id: 6,
        name: "KHỐI 6"
      },
      {
        id: 7,
        name: "KHỐI 7"
      },
      {
        id: 8,
        name: "KHỐI 8"
      },
      {
        id: 9,
        name: "KHỐI 9"
      },
      {
        id: 10,
        name: "KHỐI 10"
      },
      {
        id: 11,
        name: "KHỐI 11"
      },
      {
        id: 12,
        name: "KHỐI 12"
      },
    ]

  }

  ngOnInit(): void {
  }

}

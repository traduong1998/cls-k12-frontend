import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AuthenticationEndpoint, InfoIdentity, UserEndpoint } from 'cls-k12-sdk-js/src';
import { HomeEnpoint } from 'sdk/cls-k12-sdk-js/src/services/home-page/home-enpoint';
import { InforUnitHome } from 'sdk/cls-k12-sdk-js/src/services/home-page/models/infor-unit-home';
import { UnitConfig } from 'src/app/core/interfaces/app-config';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { isNullOrEmpty } from 'src/app/shared/helpers/validation.helper';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {

  isToggle = false;
  authEndpoint = new AuthenticationEndpoint({ baseUrl: environment.apiBaseUrl });

  // This is for Mymessages
  mymessages: Object[] = [
    {
      useravatar: 'assets/images/users/1.jpg',
      status: 'online',
      from: 'Pavan kumar',
      subject: 'Just see the my admin!',
      time: '9:30 AM'
    },
    {
      useravatar: 'assets/images/users/2.jpg',
      status: 'busy',
      from: 'Sonu Nigam',
      subject: 'I have sung a song! See you at',
      time: '9:10 AM'
    },
    {
      useravatar: 'assets/images/users/2.jpg',
      status: 'away',
      from: 'Arijit Sinh',
      subject: 'I am a singer!',
      time: '9:08 AM'
    },
    {
      useravatar: 'assets/images/users/4.jpg',
      status: 'offline',
      from: 'Pavan kumar',
      subject: 'Just see the my admin!',
      time: '9:00 AM'
    }
  ];
  tcActived: boolean = true;
  bgActived: boolean = false;
  ctActived: boolean = false;
  tlActived: boolean = false;
  dtActived: boolean = false;
  ttActived: boolean = false;

  @ViewChild('drawer') drawer: ElementRef;

  currentRoute: string;
  tokenInfo: InfoIdentity;
  canManage: boolean;
  unitInfor: UnitConfig;
  portalId: number;

  avartar: string;

  //userEndpoint:UserEndpoint;
  constructor(private route: ActivatedRoute, private router: Router, private _authService: AuthService, private configService: AppConfigService) {
    document.body.classList.add(`body-scroll-color-transparent`);

    //this.userEndpoint = new UserEndpoint();
    this.tokenInfo = this._authService.getTokenInfo();

    console.log("xzzx", this.tokenInfo);
    if (this.tokenInfo) {
      this.canManage = this.tokenInfo.userTypeRole == 'ADMIN'
        || this.tokenInfo.userTypeRole == 'TEACHER' ||
        this.tokenInfo.userTypeRole == 'SUPER'

      if (!this.tokenInfo.avatar) {
        this.avartar = '/assets/images/users/user-avartar.png'
      }
      else {
        this.avartar = this.tokenInfo.avatar;
      }

    }

    this.portalId = this.configService.getConfig().unit.portalId;
    this.unitInfor = this.configService.getConfig().unit.config;

    if (isNullOrEmpty(this.router.url) || this.router.url === "/index") {
      this.activedTC();
    }
    if (this.router.url === "/list-lessons-home-page") {
      this.activedBG();
    }
    if (this.router.url.includes("/curriculum")) {

      this.activedCT();
    }
  }

  // clickLinkHeader = () => {
  //   let gradeId = JSON.parse(localStorage.getItem('gradeId'));
  //   this.router.navigate(['/index'], { queryParams: { gradeId, lessonName: null } })

  //   // this.listGradesDashboard.forEach((item) => {
  //   //   if (item.id === this.gradeId) {
  //   //     item.isActived = true;
  //   //   }
  //   //   else {
  //   //     item.isActived = false;
  //   //   }
  //   // });
  // }

  ngOnInit(): void {
    this.router.events.subscribe(
      () => {
        if (isNullOrEmpty(this.router.url) || this.router.url === "/index") {
          this.activedTC();
        }
        if (this.router.url.includes("/list-lessons-home-page")) {
          this.activedBG();
        }
        if (this.router.url.includes("/curriculum")) {
          this.activedCT();
        }
      }
    );
  }

  ngOnDestroy(): void {
    document.body.classList.remove(`body-scroll-color-transparent`);
  }

  /**
   * check click tab
   */

  onClickTC(event) {

    let gradeId= +localStorage.getItem('gradeId');
    this.router.navigate(['/index'], { queryParams: { gradeId, lessonName: null } });
    if (event.isTrusted) {
      setTimeout(() => {
      this.activedTC();
      }, 0);
    }
  }

  onClickBG(event) {
    let gradeId= +localStorage.getItem('gradeId');
    this.router.navigate(['/list-lessons-home-page'], { queryParams: { gradeId, lessonName: null } });
    if (event.isTrusted) {
      this.activedBG();
    }
  }
  onClickCT(event) {
    let gradeId= +localStorage.getItem('gradeId');
    this.router.navigate(['/curriculum'], { queryParams: { gradeId, lessonName: null } });
    if (event.isTrusted) {
      this.activedCT();
    }
  }

  onClickTL(event) {
    if (event.isTrusted) {
      this.activedTL();
    }
  }

  onClickDT(event) {
    if (event.isTrusted) {
      this.activedDT();
    }
  }
  onClickTT(event) {
    if (event.isTrusted) {
      this.activedTT();
    }
  }

  activedTC() {
    this.tcActived = true;
    this.bgActived = false;
    this.ctActived = false;
    this.tlActived = false;
    this.dtActived = false;
    this.ttActived = false;
  }
  activedBG() {
    this.tcActived = false;
    this.bgActived = true;
    this.ctActived = false;
    this.tlActived = false;
    this.dtActived = false;
    this.ttActived = false;
  }
  activedCT() {
    this.tcActived = false;
    this.bgActived = false;
    this.ctActived = true;
    this.tlActived = false;
    this.dtActived = false;
    this.ttActived = false;
  }

  activedTL() {
    this.tcActived = false;
    this.bgActived = false;
    this.ctActived = false;
    this.tlActived = true;
    this.dtActived = false;
    this.ttActived = false;
  }

  activedDT() {
    this.tcActived = false;
    this.bgActived = false;
    this.ctActived = false;
    this.tlActived = false;
    this.dtActived = true;
    this.ttActived = false;
  }
  activedTT() {
    this.tcActived = false;
    this.bgActived = false;
    this.ctActived = false;
    this.tlActived = false;
    this.dtActived = false;
    this.ttActived = true;
  }
  onClickLogOut() {
    this._authService.logout();
    this.router.navigate(['/authentication/login']);
  }
}

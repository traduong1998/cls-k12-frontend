import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EducationComponent } from './education.component';
import { CourseListView } from './views/course-list/course-list.view';
import { IndexView } from './views/index/index.view';
import { CurriculumView } from './views/curriculum/curriculum.view';
import { ListLessonView } from './views/list-lesson/list-lesson.view';
import { DetailLessonView } from './views/detail-lesson/detail-lesson.view';
import { ListLessonsHomePageView } from './views/list-lessons-home-page/list-lessons-home-page.view';

const routes: Routes = [
  {
    path: '',
    component: EducationComponent,
    children: [
      { path: '', component: IndexView },
      { path: 'index', component: IndexView },
      { path: 'list-lessons-home-page', component: ListLessonsHomePageView },
      { path: 'curriculum', component: CurriculumView },
      { path: 'list-lesson', component: ListLessonView },
      { path: 'detail-lesson', component: DetailLessonView }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EducationRoutingModule { }

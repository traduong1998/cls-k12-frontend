import { FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { CreateUserGroupRequest } from 'sdk/cls-k12-sdk-js/src/services/user-group/requests/create-user-group-request';
import { isUserName } from 'src/app/shared/helpers/validation.helper';

@Component({
  selector: 'app-create-user-group',
  templateUrl: './create-user-group.component.html',
  styleUrls: ['./create-user-group.component.scss']
})
export class CreateUserGroupComponent implements OnInit {
  codePattern = '^[a-zA-Z0-9]+([_\s\-]?[a-zA-Z0-9])*$';
  nameContentControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(255),
  ]);
  codeContentControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(255),
    Validators.pattern(this.codePattern)
  ])

  public createUserGroupRequest: CreateUserGroupRequest;
  constructor() {
    this.createUserGroupRequest = {
      name: null,
      code: null
    }
  }

  ngOnInit(): void {
  }

}

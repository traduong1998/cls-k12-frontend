import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserGroup } from 'sdk/cls-k12-sdk-js/src/services/user-group/models/user-group';
import { CreateUserGroupRequest } from 'sdk/cls-k12-sdk-js/src/services/user-group/requests/create-user-group-request';

@Component({
  selector: 'app-edit-user-group',
  templateUrl: './edit-user-group.component.html',
  styleUrls: ['./edit-user-group.component.scss']
})
export class EditUserGroupComponent implements OnInit {
  codePattern = '^[a-zA-Z0-9]+([_\s\-]?[a-zA-Z0-9])*$';
  nameContentControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(255),
  ]);
  codeContentControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(255),
    Validators.pattern(this.codePattern)
  ])
  constructor(@Inject(MAT_DIALOG_DATA) public userGroup: UserGroup) {
    console.log("dialog-data", this.userGroup);
  }

  ngOnInit(): void {
  }

}

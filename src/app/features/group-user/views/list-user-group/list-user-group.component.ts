import { GetListUserGroupRequest } from './../../../../../../sdk/cls-k12-sdk-js/src/services/user-group/requests/get-list-user-group-request';
import { Router } from '@angular/router';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { ChangeDetectorRef, Component, OnInit, AfterViewInit, ViewChild, OnDestroy } from '@angular/core';
import { UserGroup } from 'sdk/cls-k12-sdk-js/src/services/user-group/models/user-group';
import { UserGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-group/endpoints/user-group-endpoint';
import { MatDialog } from '@angular/material/dialog';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { SelectionModel } from '@angular/cdk/collections';
import { CreateUserGroupComponent } from '../../components/create-user-group/create-user-group.component';
import { EditUserGroupComponent } from '../../components/edit-user-group/edit-user-group.component';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatPaginator } from '@angular/material/paginator';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { AuthService } from 'src/app/core/services';
import { CLSModules, CLSPermissions, DivisionEndpoint, DivisionOption, InfoIdentity, SchoolEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { pairwise, startWith, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-list-user-group',
  templateUrl: './list-user-group.component.html',
  styleUrls: ['./list-user-group.component.scss']
})
export class ListUserGroupComponent implements OnInit, AfterViewInit, OnDestroy {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  private userGroupEndpoint: UserGroupEndpoint;
  public isLoadingResults: boolean = true;
  public userGroupPagination: PaginatorResponse<UserGroup> = { totalItems: 0, items: [] };
  public displayedColumns: string[] = ['select', 'stt', 'name', 'userGroupCode', 'creater', 'function'];
  public getListUserGroupRequest: GetListUserGroupRequest;
  userIdentity: InfoIdentity;
  hasPermissionAdd: boolean;
  hasPermissionDelete: boolean;
  hasPermissionEdit: boolean;
  hasPermissionAssign: boolean;
  levelManageValue: number;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  selection = new SelectionModel<UserGroup>(true, []);

  filterGroupStudentForm = this.fb.group({
    divisionId: new FormControl(''),
    schoolId: new FormControl(''),
    keyWord: new FormControl(''),
  });

  //* Division */
  protected divisions: DivisionOption[];
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;


  /* School */
  protected schools: SchoolOption[];
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  /* Public property */
  get divisionId() { return this.filterGroupStudentForm.get('divisionId'); }
  isEnableDivision = true;
  isFirstLoadDivision = true;

  get schoolId() { return this.filterGroupStudentForm.get('schoolId'); }
  isDisableSchoolId = false;
  isFirstLoadSchool = true;

  get keyWord() { return this.filterGroupStudentForm.get('keyWord'); }

  baseApiUrl = '';
  protected _onDestroy = new Subject<void>();

  schoolFilter = {
    divisionId: null
  }

  constructor(private _authService: AuthService, private dialog: MatDialog, private fb: FormBuilder, private _snackBar: MatSnackBar, private _router: Router, private spinner: NgxSpinnerService, private changeDetectorRefs: ChangeDetectorRef) {
    this.spinner.show();
    this.userGroupEndpoint = new UserGroupEndpoint();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.getListUserGroupRequest = {
      divisionId: null,
      schoolId: null,
      keyWord: null,
      page: 1,
      size: 10
    }

    this.userIdentity = _authService.getTokenInfo();
    this.hasPermissionAdd = this.userIdentity.hasPermission(CLSModules.GroupStudent, CLSPermissions.Add);
    this.hasPermissionDelete = this.userIdentity.hasPermission(CLSModules.GroupStudent, CLSPermissions.Delete);
    this.hasPermissionEdit = this.userIdentity.hasPermission(CLSModules.GroupStudent, CLSPermissions.Edit);
    this.hasPermissionAssign = this.userIdentity.hasPermission(CLSModules.GroupStudent, CLSPermissions.Assign);

    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
      this.getListUserGroupRequest.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
      // Tài khoản cấp Trường, schoolid fillter mặc định theo id trường của tài khoản hiện tại
      this.filterGroupStudentForm.controls['schoolId'].setValue(this.userIdentity.schoolId);
      this.getListUserGroupRequest.schoolId = this.userIdentity.schoolId;
    }
    this.userGroupEndpoint.getListUserGroupPaging(this.getListUserGroupRequest.divisionId, this.getListUserGroupRequest.schoolId, this.getListUserGroupRequest.keyWord, this.getListUserGroupRequest.page, this.getListUserGroupRequest.size).then((res) => {
      this.userGroupPagination = res;
      this.isLoadingResults = false;
      this.spinner.hide();
    })
  }
  ngOnDestroy(): void {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.getListUserGroupRequest.page = this.paginator.pageIndex + 1;
      this.getListUserGroupRequest.size = this.paginator.pageSize;
      this.getListUserGroup();
    });

    this.divisionId?.valueChanges
      .pipe(startWith(this.divisionId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            // reset change
            this.onResetSchoolSelect();
          }
        }
      )

    this.schoolId?.valueChanges
      .pipe(startWith(this.schoolId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
          }
        }
      )
    this.keyWord?.valueChanges
      .pipe(startWith(this.schoolId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
          }
        }
      )

    // listen for search field value changes 
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        console.log(this.divisionId.value);
        this.filterDivisions();
      });


    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });
  }

  ngOnInit(): void {
  }


  onDivisionSelectClicked() {
    if (this.isFirstLoadDivision) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivision = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {
    }
  }

  onSchoolSelectClicked() {
    debugger;
    var divisionIdSelected = this.divisionId?.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchool) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchool = false;
          this.schools = res;
          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {

    }
  }

  private onResetSchoolSelect() {
    this.filterGroupStudentForm.controls['schoolId'].setValue(0);
    this.isFirstLoadSchool = true;
    // this.filterLessonForm.controls['schoolFilterCtrl'].setValue(0);
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }
  onSubmitFilter(): void {
    this.getListUserGroupRequest.divisionId = this.filterGroupStudentForm.controls['divisionId'].value;
    this.getListUserGroupRequest.schoolId = this.filterGroupStudentForm.controls['schoolId'].value;
    this.getListUserGroupRequest.keyWord = this.filterGroupStudentForm.controls['keyWord'].value;
    this.paginator.pageIndex = 0;
    this.getListUserGroupRequest.page = 1;
    this.getListUserGroup();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.userGroupPagination.items.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }
    this.selection.select(...this.userGroupPagination.items);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: UserGroup, index?: number): string {
    //let index = this.userGroupPagination.items.findIndex(x=>x.id == row.id) + 1;
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${index + 1}`;
  }
  getListUserGroup(): void {
    this.spinner.show(undefined, {
      type: SpinnerType.ballFussion,
      size: 'medium',
      bdColor: 'rgb(255 255 255)',
      color: '#43a047',
      fullScreen: true
    });
    this.userGroupEndpoint.getListUserGroupPaging(this.getListUserGroupRequest.divisionId, this.getListUserGroupRequest.schoolId, this.getListUserGroupRequest.keyWord, this.getListUserGroupRequest.page, this.getListUserGroupRequest.size).then((res) => {
      this.selection.clear();
      this.userGroupPagination = res;
      this.isLoadingResults = false;
      this.spinner.hide();
    })
  }
  createUserGroup(): void {
    let createUserGroupDialog = this.dialog.open(CreateUserGroupComponent, {
      width: '510px', height: 'auto'
    });
    createUserGroupDialog.afterClosed().subscribe((usergroup) => {
      console.log("userGroup:", usergroup);
      if (usergroup) {
        this.userGroupEndpoint.addUserGroup(usergroup.name, usergroup.code).then((res) => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Tạo nhóm học sinh thành công',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
            this.getListUserGroupRequest.page = 1;
            this.getListUserGroup();
            // this.userGroupPagination.items.unshift({
            //   id: res.id,
            //   name: res.name,
            //   userGroupCode: res.userGroupCode,
            //   createrId: res.createrId,
            //   creator: this.userIdentity.fullName,
            //   createdDate: res.createdDate,
            //   portalId: res.portalId,
            //   divisionId: res.divisionId,
            //   schoolId: res.schoolId,
            //   count: res.count
            // });
            // this.userGroupPagination.totalItems++;
            // this.userGroupPagination.items = [...this.userGroupPagination.items];
            // this.changeDetectorRefs.detectChanges();
          }
          else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Có lỗi xảy ra',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          }
        }).catch((err) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.response.data["errorDetail"],
            duration: 3000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });
        })
      }
    })
  }

  editUserGroup(userGroup: UserGroup): void {
    console.log("dataInput", userGroup);
    let editUserGroupDialog = this.dialog.open(EditUserGroupComponent, {
      data: { ...userGroup },
      width: '510px',
      height: 'auto'
    });
    editUserGroupDialog.afterClosed().subscribe((newUserGroup) => {
      console.log("userGroup:", newUserGroup);

      if (newUserGroup && JSON.stringify(newUserGroup) != JSON.stringify(userGroup)) {
        this.userGroupEndpoint.editUserGroup(newUserGroup).then((res) => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Sửa nhóm học sinh thành công',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
            this.userGroupPagination.items.map((usergroup) => {
              if (usergroup.id == res.id) {
                usergroup.name = res.name;
                usergroup.userGroupCode = res.userGroupCode;
              }
            })
          }
          else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Có lỗi xảy ra',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          }
        }).catch((err) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.response.data["errorDetail"],
            duration: 3000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });
        })
      }
      else if (JSON.stringify(newUserGroup) == JSON.stringify(userGroup)) {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: 'Lưu thành công!',
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
      }
    })
  }
  onDeleteUserGroup(data): void {
    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      disableClose: true,
      width: '444px',
      height: 'auto',
      //data: data
      data: {
        title: 'Xóa nhóm học sinh',
        name: data.name,
        message: ''
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
        this.userGroupEndpoint.deleteUserGroup(data.id)
          .then(res => {
            if (res) {
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: 'Xóa thành công',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
              this.getListUserGroup();
              // this.userGroupPagination.items.splice(this.userGroupPagination.items.findIndex(x => x.id == data.id), 1);
              // this.userGroupPagination.items = [...this.userGroupPagination.items];
              // if (this.selection.isSelected(data)) {
              //   this.selection.toggle(data);
              // }
            } else {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Xóa không thành công',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            }
          })
          .catch((err: ErrorResponse) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: err.errorCode,
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
      }
      else {
        if (result != undefined) {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Bạn nhập chưa đúng, vui lòng thử lại',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      }
    });

  }
  deleteSelected(): void {
    let listDelete: number[] = [];
    this.userGroupPagination.items.forEach((userGroup) => {
      if (this.selection.isSelected(userGroup)) {
        listDelete.push(userGroup.id);
      }
    })
    if (listDelete.length > 0) {

      const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
        disableClose: true,
        width: '444px',
        height: 'auto',
        //data: data
        data: {
          title: 'Xóa nhóm học sinh',
          name: 'Nhóm đã chọn',
          message: ''
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result == 'DONGY') {
          this.userGroupEndpoint.deleteListUserGroup(listDelete).then((res) => {
            if (res) {
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: 'Xóa thành công!',
                duration: 3000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });
              this.getListUserGroup();
              // listDelete.forEach((id) => {
              //   this.userGroupPagination.items.splice(this.userGroupPagination.items.findIndex(x => x.id == id), 1);
              // });
              // this.userGroupPagination.items = [...this.userGroupPagination.items];
              // this.selection.clear();
            }
            else {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Có lỗi xảy ra!',
                duration: 3000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });
            }
          })
            .catch((err) => {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Có lỗi xảy ra!',
                duration: 3000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });
            })
        }
        else {
          if (result != undefined) {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Bạn nhập chưa đúng, vui lòng thử lại',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        }
      });
    }
    else {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Bạn chưa chọn nhóm học sinh nào',
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
    }
  }
  assignUser(id: number): void {
    this._router.navigate([`dashboard/user-group/${id}/assign-user`]);
  }

  private openConfirmDialog(value: string): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      data: {
        message: `${value}`,
        buttonText: {
          ok: 'Có',
          cancel: 'Không'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }

}

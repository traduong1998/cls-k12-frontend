import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';
import { AssignStudentForGroupRequest } from './../../../../../../sdk/cls-k12-sdk-js/src/services/user-group/requests/assign-student-for-group-request';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/core/services';
import { DivisionEndpoint, DivisionOption, GradeEndpoint, GroupStudentEndpoint, SchoolEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { StudentForAssignResponse } from 'sdk/cls-k12-sdk-js/src/services/user-group/responses/student-for-assign-response';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { pairwise, startWith, takeUntil } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { UsersOfUserGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-group/endpoints/users-of-usergroup-endpoint';
import { GetListStudentForAssignRequest } from 'sdk/cls-k12-sdk-js/src/services/user-group/requests/get-list-student-for-assign-request';
import { MatPaginator } from '@angular/material/paginator';
import { Action } from 'src/app/features/learn/intefaces/action';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';

@Component({
  selector: 'app-assign-user-group',
  templateUrl: './assign-user-group.component.html',
  styleUrls: ['./assign-user-group.component.scss']
})
export class AssignUserGroupComponent implements OnInit, AfterViewInit {
  baseApiUrl = '';
  levelManageValue: number = 0;
  userGroupId: number;
  isListStudentForAssignReady: boolean = false;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointGroupStudent: GroupStudentEndpoint
  usersOfUserGroupEndpoint: UsersOfUserGroupEndpoint;
  userIdentity: UserIdentity;
  assignStudentRequest: AssignStudentForGroupRequest;
  listOldStudentForAssignId: number[];
  listStudentForAssignDefault: PaginatorResponse<StudentForAssignResponse> = {
    items: [],
    totalItems: 0
  };
  assignDataSource: PaginatorResponse<StudentForAssignResponse> = this.listStudentForAssignDefault;
  assignSelection = new SelectionModel<StudentForAssignResponse>(true, []);
  assignDisplayedColumns: string[] = ['stt', 'fullName', 'userName', 'className', 'select'];
  isShowFilter: boolean = true;
  protected _onDestroy = new Subject<void>();

  //* Division */
  protected divisions: DivisionOption[];
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  /* School */
  protected schools: SchoolOption[];
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;
  protected grades: GradeOption[];
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;
  protected groupStudents: GroupStudentOption[];
  public groupStudentCtrl: FormControl = new FormControl();
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterSignupLecturesForm = this.fb.group({
    divisionId: new FormControl(''),
    schoolId: new FormControl(''),
    gradeId: new FormControl(''),
    groupStudentId: new FormControl(''),
  });

  get divisionId() { return this.filterSignupLecturesForm.get('divisionId'); }
  isEnableDivision = true;
  isFirstLoadDivision = true;

  get schoolId() { return this.filterSignupLecturesForm.get('schoolId'); }
  isDisableSchoolId = false;
  isFirstLoadSchool = true;

  get gradeId() { return this.filterSignupLecturesForm.get('gradeId'); }
  isDisableGradeId = false;
  isFirstLoadGrade = true;

  get groupStudentId() { return this.filterSignupLecturesForm.get('groupStudentId'); }
  isDisableGroupStudentId = true;
  isFirstLoadGroupStudent = true;

  studentsAssignPaginationFilter: GetListStudentForAssignRequest;
  constructor(private fb: FormBuilder, private route: ActivatedRoute, private _authService: AuthService, private _router: Router, private _snackBar: MatSnackBar) {
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
    this.usersOfUserGroupEndpoint = new UsersOfUserGroupEndpoint();
    this.userGroupId = +this.route.snapshot.paramMap.get("id");
    this.listOldStudentForAssignId = [];
    this.studentsAssignPaginationFilter = {
      pageNumber: 1,
      sizeNumber: 10,
      getCount: true,
      userGroupId: this.userGroupId,
      divisionId: 0,
      schoolId: 0,
      gradeId: 0,
      groupStudentId: 0,
      sortDirection: 'DESC',
      sortField: 'CRE'
    }
    this.assignStudentRequest = {
      userGroupId: this.userGroupId,
      listStudents: []
    };
    this.userIdentity = _authService.getTokenInfo();

    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
      // Tài khoản cấp Trường, schoolid fillter mặc định theo id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
      this.filterSignupLecturesForm.controls['schoolId'].setValue(this.userIdentity.schoolId);
      // this.usersPaginationFilter.schoolId = this.userIdentity.schoolId;
    }
    this.getListStudentForAssign();
  }
  ngAfterViewInit(): void {
    // listen for search field value changes 
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        console.log(this.divisionId.value);
        this.filterDivisions();
      });

    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    // listen for search field value changes
    this.groupStudentFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudents();
      });

    this.divisionId?.valueChanges
      .pipe(startWith(this.divisionId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            // reset change
            this.onResetSchoolSelect();
            this.onResetGradeSelect();
            this.onResetGroupStudentSelect();
          }
        }
      )

    this.schoolId?.valueChanges
      .pipe(startWith(this.schoolId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetGradeSelect();
            this.onResetGroupStudentSelect();
            if (value < 1) {
              this.isDisableGroupStudentId = true;
            } else {
              this.isDisableGroupStudentId = false;

            }
          }
        }
      )

    this.gradeId?.valueChanges
      .pipe(startWith(this.gradeId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            this.onResetGroupStudentSelect();
          }
        }
      )

    this.groupStudentId?.valueChanges
      .pipe(startWith(this.gradeId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
          }
        }
      )

    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.studentsAssignPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.studentsAssignPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.getListStudentForAssign();
    });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }


  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  ngOnInit(): void {
    this.isListStudentForAssignReady = true;
  }

  onSubmitSignLectures() {
    //reset lại list học sinh đã chọn và list học sinh đã ghi danh
    this.assignStudentRequest.listStudents = [];
    this.listOldStudentForAssignId = [];
    this.studentsAssignPaginationFilter.userGroupId = this.userGroupId;
    this.studentsAssignPaginationFilter.divisionId = this.filterSignupLecturesForm.controls['divisionId'].value;
    this.studentsAssignPaginationFilter.schoolId = this.filterSignupLecturesForm.controls['schoolId'].value;
    this.studentsAssignPaginationFilter.gradeId = this.filterSignupLecturesForm.controls['gradeId'].value;
    this.studentsAssignPaginationFilter.groupStudentId = this.filterSignupLecturesForm.controls['groupStudentId'].value;
    this.paginator.pageIndex = 0; 
    this.studentsAssignPaginationFilter.pageNumber = 1;
    this.getListStudentForAssign();
  }

  onDivisionSelectClicked() {
    if (this.isFirstLoadDivision) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivision = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {
    }
  }

  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionId?.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchool) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchool = false;
          this.schools = res;
          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {

    }
  }

  /* Get Grade */
  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolId.value == 0 ? null : this.schoolId.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrade || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrade = false;
          this.grades = res;

          if (this.grades && !this.grades.find(x => x.id == 0)) {
            this.grades.unshift({ id: 0, name: 'Chọn khối' });
          }
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());

        })
        .catch(err => { console.log(err) }
        )
    } else {

    }
  }

  /* Get GroupStudent */
  onGroupStudentSelectClicked() {
    var schoolIdSelected = this.schoolId?.value;
    var gradeIdSelected = this.gradeId?.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if ((this.isDisableGroupStudentId == true && this.levelManageValue !== 3) || !this.isFirstLoadGroupStudent) {
      return;
    } else {
      this.endpointGroupStudent.getGroupStudentOptions(schoolIdSelected, gradeIdSelected)
        .then(res => {
          this.isFirstLoadGroupStudent = false;
          this.groupStudents = res;
          this.filteredGroupStudents.next(this.groupStudents.slice());
        })
        .catch(

        )
    }
  }

  onCancelAssignStudentTabClick(): void {
    this._router.navigate(['dashboard/user-group']);
  }

  onSaveAssignStudentClick(): void {
    this.usersOfUserGroupEndpoint.assignStudents(this.assignStudentRequest).then((res) => {
      if (res) {
        //Nếu thành công thì reset lại list request
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: 'Bạn vừa mới thực hiện ghi danh/ gỡ ghi danh thành công',
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
        //Update list oldId
        this.assignStudentRequest.listStudents.forEach((std) => {
          if (std.action == Action.Add) {
            this.listOldStudentForAssignId.push(std.id);
            this.assignDataSource.items.map((x) => {
              if (x.id == std.id) {
                x.isAssigned = true;
              }
            })
          }
          else {
            let index = this.listOldStudentForAssignId.indexOf(std.id);
            this.listOldStudentForAssignId.splice(index, 1);
            this.assignDataSource.items.map((x) => {
              if (x.id == std.id) {
                x.isAssigned = false;
              }
            })
          }
        });
      } else {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Có lỗi xảy ra!',
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
        //Reset lại combobox
        this.assignStudentRequest.listStudents.forEach((std) => {
          let row = this.listStudentForAssignDefault.items.find(x => x.id == std.id);
          this.assignSelection.toggle(row);
        });
      }
      this.assignStudentRequest.listStudents = [];
    }).catch((err) => {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Có lỗi xảy ra!',
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
      //Reset lại combobox
      this.assignStudentRequest.listStudents.forEach((std) => {
        let row = this.listStudentForAssignDefault.items.find(x => x.id == std.id);
        this.assignSelection.toggle(row);
      });
      this.assignStudentRequest.listStudents = [];
    });
  }

  isAssignAllSelected() {
    const numSelected = this.assignSelection.selected.length;
    const numRows = this.assignDataSource.items.length;
    return numSelected === numRows;
  }
  assignCheckboxLabel(row?: StudentForAssignResponse): string {
    if (!row) {
      return `${this.isAssignAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.assignSelection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }
  assignMasterToggle() {
    if (this.isAssignAllSelected()) {
      //Nếu là bỏ chọn hết thì thêm những học sinh đã ghi danh vào model để gỡ ghi danh (action: Delete)
      this.assignDataSource.items.forEach((std) => {
        //Mấy thằng cũ đã được ghi danh thì kiểm tra xem nó đã được thêm vào trước đó với action delete chưa
        if (std.isAssigned) {
          if (this.assignStudentRequest.listStudents.find(x => x.id == std.id && x.action == Action.Delete) == undefined) {
            this.assignStudentRequest.listStudents.push({ id: std.id, action: Action.Delete });
          }
        } else {
          let studentAdd = this.assignStudentRequest.listStudents.find(x => x.id == std.id && x.action == Action.Add);
          if (studentAdd != undefined) {
            let index = this.assignStudentRequest.listStudents.indexOf(studentAdd);
            this.assignStudentRequest.listStudents.splice(index, 1);
          }
        }
      });
      console.log(this.assignStudentRequest.listStudents);
      this.assignSelection.clear();
    } else {
      // Nếu chọn hết thì thêm những học sinh chưa ghi dahnh vào model để gửi lên(action: Add)
      this.assignDataSource.items.forEach((std) => {
        if (!std.isAssigned) {
          if (this.assignStudentRequest.listStudents.find(x => x.id == std.id && x.action == Action.Add) == undefined) {
            this.assignStudentRequest.listStudents.push({ id: std.id, action: Action.Add });
          }
        }
        else {
          let studentDel = this.assignStudentRequest.listStudents.find(x => x.id == std.id && x.action == Action.Delete);
          if (studentDel != undefined) {
            let index = this.assignStudentRequest.listStudents.indexOf(studentDel);
            this.assignStudentRequest.listStudents.splice(index, 1);
          }
        }
      });
      console.log(this.assignStudentRequest.listStudents);
      this.assignDataSource.items.forEach(row => this.assignSelection.select(row));
    }
  }
  onChangeAssignCheckBox(checked, row: StudentForAssignResponse): void {
    this.assignSelection.toggle(row);
    console.log(checked);
    // nếu chọn thì thêm vào list request với action là add
    if (checked) {
      //Kiểm tra xem nếu là chọn thằng cũ thì chắc là nãy nó bị bỏ chọn rồi giờ chọn lại nên remove khỏi list request đi
      if (this.listOldStudentForAssignId.includes(row.id)) {
        let std = this.assignStudentRequest.listStudents.find(x => x.id == row.id);
        let index = this.assignStudentRequest.listStudents.indexOf(std)
        this.assignStudentRequest.listStudents.splice(index, 1);
      }
      else {
        this.assignStudentRequest.listStudents.push({ id: row.id, action: Action.Add });
      }
    }
    // nếu bỏ chọn thì ...
    else {
      //Nếu bỏ chọn thằng đã được ghi danh thì thêm vào request để biết mà gỡ ghi danh
      if (this.listOldStudentForAssignId.includes(row.id)) {
        this.assignStudentRequest.listStudents.push({ id: row.id, action: Action.Delete });
      }
      //Bỏ chọn thằng mới thì gỡ ra luôn khỏi list, ko gửi lên
      else {
        let std = this.assignStudentRequest.listStudents.find(x => x.id == row.id);
        let index = this.assignStudentRequest.listStudents.indexOf(std);
        if (!(index < 0)) {
          this.assignStudentRequest.listStudents.splice(index, 1);
        }
      }
    }
    console.log("new", this.assignStudentRequest.listStudents);
    console.log("old", this.listOldStudentForAssignId);
  }

  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }

  private onResetSchoolSelect() {
    this.filterSignupLecturesForm.controls['schoolId'].setValue(0);
    this.isFirstLoadSchool = true;
  }

  /* reset change Grade */
  private onResetGradeSelect() {
    this.filterSignupLecturesForm.controls['gradeId'].setValue(0);
    this.isFirstLoadGrade = true;
  }

  private onResetGroupStudentSelect() {
    this.filterSignupLecturesForm.controls['groupStudentId'].setValue(null);
    this.isFirstLoadGroupStudent = true;
  }
  private getListStudentForAssign() {
    this.usersOfUserGroupEndpoint.getListStudentForAssign(this.studentsAssignPaginationFilter)
      .then(res => {

        if (res) {
          this.assignSelection.clear();
          this.assignDataSource = res;
          //Gán những học sinh đã được ghi danh vào trong list này
          this.assignDataSource.items.forEach((x) => {
            if (x.isAssigned) {
              this.listOldStudentForAssignId.push(x.id);
              this.assignSelection.select(x);
            } else if (this.assignStudentRequest.listStudents.find(y => y.id == x.id && y.action == 'ADD') != undefined) {
              this.assignSelection.select(x);
            }
          });
          this.isListStudentForAssignReady = true;
        }
      });
  }
}

import { AssignUserGroupComponent } from './views/assign-user-group/assign-user-group.component';
import { ListUserGroupComponent } from './views/list-user-group/list-user-group.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ListUserGroupComponent,
    data: {
      title: 'Danh sách nhóm học sinh',
    }
  },
  {
    path: ':id/assign-user',
    component: AssignUserGroupComponent,
    data: {
      title: 'Ghi danh học sinh vào nhóm',
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupUserRoutingModule { }

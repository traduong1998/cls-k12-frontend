import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupUserRoutingModule } from './group-user-routing.module';
import { GroupUserComponent } from './group-user.component';
import { ListUserGroupComponent } from './views/list-user-group/list-user-group.component';
import { CreateUserGroupComponent } from './components/create-user-group/create-user-group.component';
import { EditUserGroupComponent } from './components/edit-user-group/edit-user-group.component';
import { AssignUserGroupComponent } from './views/assign-user-group/assign-user-group.component';
import { NgxSpinnerModule } from 'ngx-spinner';



@NgModule({
  declarations: [GroupUserComponent, ListUserGroupComponent, CreateUserGroupComponent, EditUserGroupComponent, AssignUserGroupComponent],
  imports: [
    CommonModule,
    GroupUserRoutingModule,
    SharedModule,
    NgxSpinnerModule,
  ]
})
export class GroupUserModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators
} from '@angular/forms';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { AuthenticationEndpoint, CLS, UserTypeRoles } from 'cls-k12-sdk-js/src';
import { environment } from 'src/environments/environment';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { UnitConfig } from 'src/app/core/interfaces/app-config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  public form: FormGroup = Object.create(null);
  authEndpoint = new AuthenticationEndpoint({ baseUrl: environment.apiBaseUrl });

  userNamePattern = '^[a-zA-Z0-9_.-]{1,50}$';

  isChecked: boolean = false;

  dataSetting: any;

  unitConfig: UnitConfig;
  
  constructor(private _snackBar: MatSnackBar, private cls: CLS, private fb: FormBuilder, private router: Router, private _authService: AuthService, private appConfig: AppConfigService) {
    //get pass to localStorage
    this.unitConfig = this.appConfig.getUnitConfig();
  }

  ngOnInit(): void {
    this.getDataSetting();

    this.form = this.fb.group({
      uname: [null, Validators.compose([Validators.required, Validators.maxLength(50), Validators.pattern(this.userNamePattern)])],
      password: [null, Validators.compose([Validators.required, Validators.maxLength(50), Validators.minLength(6)])]
    });

    let userSecurityInfo = JSON.parse(localStorage.getItem('userSecurityInfo'));
    if (userSecurityInfo) {
      this.isChecked = true;
      this.form.get("uname").setValue(userSecurityInfo.userName);
      this.form.get("password").setValue(userSecurityInfo.password);
    }
    else {
      this.isChecked = false;
    }

    if (this._authService.isAuthenticated()) {
      var roleUser = this._authService.getTokenInfo().userTypeRole;
      if (roleUser == UserTypeRoles.SuperAdmin || roleUser == UserTypeRoles.Admin || roleUser == UserTypeRoles.Teacher) {
        this.router.navigateByUrl('/dashboard');
      }
      else {
        this.router.navigateByUrl('/learner');
      }
    }
  }

  getDataSetting() {
    this.dataSetting = JSON.parse(localStorage.getItem('dataSettingUser'));

    console.log("sdsd", this.dataSetting)
  }

  onLoginClick() {
    this.authEndpoint.login({
      domain: location.hostname,
      userName: this.form.get("uname").value,
      passWord: this.form.get("password").value
    })
      .then((res) => {
        //save to local if remmember checked, status checked
        if (this.isChecked) {
          let userSecurityInfo = { userName: this.form.get("uname").value, password: this.form.get("password").value }
          localStorage.setItem('userSecurityInfo', JSON.stringify(userSecurityInfo));
        }
        else {
          //delete => unchecked
          localStorage.removeItem('userSecurityInfo');
        }     

        this._authService.setLoginSucess(res.token);
        this.cls.setToken(res.token);

        var roleUser = this._authService.getTokenInfo().userTypeRole;
        if (roleUser == UserTypeRoles.SuperAdmin || roleUser == UserTypeRoles.Admin || roleUser == UserTypeRoles.Teacher) {
          this.router.navigateByUrl('/dashboard');
        }
        else {
          this.router.navigateByUrl('/learner');
        }
      })
      .catch((err) => {
        err.errorDetail
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: "Bạn nhập sai tên hoặc mật khẩu",
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
  }

  onClickVisible(pw: any): void {
    pw.type = pw.type == 'password' ? 'text' : 'password';
  }

  rememberPasswork(event) {
    if (event.checked) {
      this.isChecked = true;
    }
    else {
      this.isChecked = false;
    }
  }           

  register() {
    this.router.navigateByUrl('/authentication/register');
  }
}


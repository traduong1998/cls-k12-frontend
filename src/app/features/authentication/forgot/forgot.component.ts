import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { CustomValidators } from 'ngx-custom-validators';
import { AppConfigService } from 'src/app/core/services';
import { UnitConfig } from 'src/app/core/interfaces/app-config';


@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {
  unitConfig: UnitConfig;
  public form: FormGroup = Object.create(null);
  constructor(private fb: FormBuilder, private router: Router, private appConfig: AppConfigService) {
    this.unitConfig = this.appConfig.getUnitConfig();
  }

  validating: boolean = true;

  ngOnInit(): void {
    this.form = this.fb.group({
      email: [
        null,
        Validators.compose([Validators.required, CustomValidators.email])
      ]
    });
  }

  onSubmit(): void {
    //nếu API thành công thì set
    this.validating = false;
    // this.router.navigate(['/authentication/login']);
  }
}

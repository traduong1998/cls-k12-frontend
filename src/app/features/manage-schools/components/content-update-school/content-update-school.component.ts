import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { NgForm, FormControl, Validators } from '@angular/forms';
import { UpdateSchoolRequest } from 'cls-k12-sdk-js/src/services/school/requests/updateSchoolRequest';
import { SchoolEndpoint } from 'cls-k12-sdk-js/src/services/school/endpoints/SchoolEndpoint';
import { School } from 'cls-k12-sdk-js/src/services/school/models/School';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition, MatSnackBar } from '@angular/material/snack-bar';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';

@Component({
  selector: 'app-content-update-school',
  templateUrl: './content-update-school.component.html',
  styleUrls: ['./content-update-school.component.scss']
})
export class ContentUpdateSchoolComponent implements OnInit {

  schoolEndpoint: SchoolEndpoint;

  baseApiUrl = 'http://localhost:65000';
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  lblname = '';
  lblcode = '';

  school: UpdateSchoolRequest = {
    name: '',
    code: ''
  };

  codePattern = '^([A-Za-z0-9]+(?:\.[A-Za-z0-9]+)*)$';

  codeUpdateSchoolFormControl = new FormControl('', [
    Validators.pattern(this.codePattern)
  ])

  constructor(private mdDialogRef: MatDialogRef<ContentUpdateSchoolComponent>, @Inject(MAT_DIALOG_DATA) public schoolInfo: School, private _snackBar: MatSnackBar) {
    this.schoolEndpoint = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
  }

  formSchool: School = {
    name: this.schoolInfo.name,
    code: this.schoolInfo.code,
    donviname: this.schoolInfo.donviname
  }

  ngOnInit(): void {
  }

  @ViewChild('regForm') myForm: NgForm;


  resetForm() {
    this.lblname = '';
    this.lblcode = '';
  }

  checkForm() {
    var result = true;

    if (this.formSchool.name === '') {
      this.lblname = 'Bạn chưa nhập tên Trường!';
      result = false;
    }
    if (this.formSchool.code === '') {
      this.lblcode = 'Bạn chưa nhập mã Trường!';
      result = false;
    }

    return result;
  }


  EditSchool(regForm: NgForm) {
    this.resetForm();
    if (this.checkForm()) {

      this.schoolEndpoint.checkNameSchool(this.schoolInfo["id"], this.formSchool.name)
        .then(res => {
          //hàm này đặc thù trả về true, false nên sẽ vô then. nếu hàm bình thường thì thường backend sẽ thow exception nếu k thỏa điều kiện để update hoặc đầu vào bị validate
          if (res) {
            this.lblname = 'Tên Trường đã tồn tại trong hệ thống!';
            return;
          }
          this.school["id"] = this.schoolInfo["id"];
          this.school["name"] = this.formSchool.name;
          this.school["code"] = this.formSchool.code;
          this.schoolEndpoint.updateSchool(this.school)
            .then(res => {
              this.mdDialogRef.close('submit')
              if (res) {
                this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                  data: 'Sửa trường thành công',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              } else {
                this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                  data: 'Sửa trường không thành công',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            })
            .catch((err: ErrorResponse) => {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: err.errorDetail,
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            });
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
    }
  }
}

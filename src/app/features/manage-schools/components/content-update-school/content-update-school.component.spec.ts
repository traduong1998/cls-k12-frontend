import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentUpdateSchoolComponent } from './content-update-school.component';

describe('ContentUpdateSchoolComponent', () => {
  let component: ContentUpdateSchoolComponent;
  let fixture: ComponentFixture<ContentUpdateSchoolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentUpdateSchoolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentUpdateSchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

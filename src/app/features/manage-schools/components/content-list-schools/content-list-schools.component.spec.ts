import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentListSchoolsComponent } from './content-list-schools.component';

describe('ContentListSchoolsComponent', () => {
  let component: ContentListSchoolsComponent;
  let fixture: ComponentFixture<ContentListSchoolsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentListSchoolsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentListSchoolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

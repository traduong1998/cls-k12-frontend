import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ContentCreateSchoolComponent } from '../../components/content-create-school/content-create-school.component';
import { ContentUpdateSchoolComponent } from '../../components/content-update-school/content-update-school.component';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { School } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { SchoolEndpoint } from 'cls-k12-sdk-js/src/services/school/endpoints/SchoolEndpoint';
import { SchoolFilterRequests } from 'cls-k12-sdk-js/src/services/school/models/SchoolPagingRequest';
import { ContentListGradesComponent } from '../content-list-grades/content-list-grades.component';
import { FormControl } from '@angular/forms';
import { Subject, ReplaySubject } from 'rxjs';
import { DivisionOption, DivisionDetail } from 'cls-k12-sdk-js/src/services/division/models/Division';
import { DivisionEndpoint } from 'cls-k12-sdk-js/src/services/division/endpoints/DivisionEndpoint';
import { UserIdentity, CLSModules, CLSPermissions, UserEndpoint } from 'cls-k12-sdk-js/src';
import { AuthService } from 'src/app/core/services';
import { takeUntil } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { DeleteSchoolRequest } from 'sdk/cls-k12-sdk-js/src/services/school/requests/deleteSchoolRequest';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';

const schoolsPaginationDefault: PaginatorResponse<School> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-content-list-schools',
  templateUrl: './content-list-schools.component.html',
  styleUrls: ['./content-list-schools.component.scss']
})
export class ContentListSchoolsComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  endpointUser: UserEndpoint;
  schoolEndpoint: SchoolEndpoint;
  endpointDivision: DivisionEndpoint
  userIdentity: UserIdentity;
  isShowFilter = true;
  levelManageValue: number = 0;
  isFirstLoadDivisions = true;
  public txtFullName: FormControl = new FormControl('');
  /** list of divisions */
  protected divisions: DivisionOption[];

  public divisionCtrl: FormControl = new FormControl();
  /** control for the MatSelect filter keyword */
  public divisionFilterCtrl: FormControl = new FormControl();

  protected _onDestroy = new Subject<void>();

  /* Public property */
  //get divisionId() { return this.filterLessonForm.get('divisionId'); }
  /** list of divisions filtered by search keyword */
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  public divisionNameInputt: FormControl = new FormControl();


  baseApiUrl = 'http://localhost:65000';

  // displayedColumns: string[] = ['select', 'stt', 'code', 'name', 'grade', 'function'];
  displayedColumns: string[] = ['stt', 'name', 'code', 'donviquanly', 'function'];

  schoolsPagination: PaginatorResponse<School> = schoolsPaginationDefault;
  schoolsPaginationFilter: SchoolFilterRequests = {
    pageNumber: 0,
    sizeNumber: 10,
    getCount: true,
    sortDirection: 'ASC',
    sortField: 'CRE',
  }

  schoolsPaginationFilterClone: SchoolFilterRequests;

  selection = new SelectionModel<School>(true, []);
  selectedSchoolIds = [];
  isLoadingResults = true;

  schoolFilter = {
    divisionId: null
  }

  hasAddPermission: boolean;
  hasDeletePermission: boolean;
  hasEditPermission: boolean;

  constructor(private _snackBar: MatSnackBar, private _authService: AuthService, public dialog: MatDialog, private _router: Router, private spinner: NgxSpinnerService, @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.endpointUser = new UserEndpoint({ baseUrl: this.baseApiUrl });
    this.schoolEndpoint = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();

    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.School, CLSPermissions.Add);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.School, CLSPermissions.Edit);
    this.hasDeletePermission = this.userIdentity.hasPermission(CLSModules.School, CLSPermissions.Delete);

    console.log("Tài khoản cấp: ", this.userIdentity.levelManage);
    console.log("Vai trò: ", this.userIdentity.userTypeRole);
    //console.log(this.userIdentity);
    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
      // this.getDataDivisionById(this.schoolFilter.divisionId);
    }
    console.log("configByToken", spinnerConfig);
    this.spinner.show();
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit(): void {
    this.getSchoolsData();
  }


  failureCallback() {
    this.isLoadingResults = false;
    alert("hihi 400");
  }

  getSchoolsData() {
    if (this.levelManageValue == 2) {
      this.schoolsPaginationFilter.divisionId = this.schoolFilter.divisionId;
    }
    this.isLoadingResults = true;
    this.schoolEndpoint.getSchoolsPaging(this.schoolsPaginationFilter)
      .then(res => {
        this.spinner.hide();
        this.isLoadingResults = false;
        this.schoolsPagination = res;
        this.schoolsPaginationFilterClone = { ... this.schoolsPaginationFilter };
      })
      .catch(this.failureCallback);
  }


  onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(

        )
    } else {

    }
  }


  getFilterParam() {
    this.schoolsPaginationFilter.divisionId = this.divisionCtrl.value;

    if (this.userIdentity.levelManage == "DVS") {
      this.schoolsPaginationFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.schoolsPaginationFilter.divisionId = this.userIdentity.divisionId;
    }

    this.schoolsPaginationFilter.keyWord = this.txtFullName.value;
    this.schoolsPaginationFilter.pageNumber = 1;
    this.paginator.pageIndex = 0;
  }

  onSubmitFilter() {
    this.getFilterParam();
    if (!compareTwoObject(this.schoolsPaginationFilter, this.schoolsPaginationFilterClone)) {
      this.getSchoolsData();
    }

  }

  //constructor() { }
  openDialog() {
    const dialogRef = this.dialog.open(ContentCreateSchoolComponent, {
      disableClose: true,
      minWidth: '550px',
      height: 'auto',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getSchoolsData();
        }
      }
    });
  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.schoolsPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.schoolsPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.getSchoolsData();
    });

    // listen for search field value changes 
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {

        //console.log(this.divisionId.value);
        this.filterDivisions();
      });

  }


  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }


  /**Show dialog Update School */
  UpdateSchool(data) {
    this.selection.clear();
    //show dialog
    const dialogRef = this.dialog.open(ContentUpdateSchoolComponent, {
      disableClose: true,
      width: '550px',
      height: 'auto',
      data: data
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getSchoolsData();
        }
      }
    });
  }

  /**Show dialog Update School */
  DeleteSchool(data) {
    this.selection.clear();
    debugger
    return Promise.all([
      this.endpointUser.GetUserOfSchool(data.id),
      this.schoolEndpoint.countGroupStudentOfSchool(data.id)
     ])
      .then(res => {
        if (res[0] != null && res[1] == 0) {
          //show dialog
          const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
            disableClose: true,
            width: '444px',
            height: 'auto',
            //data: data
            data: {
              title: 'Xóa trường',
              name: data.name,
              message: 'Xin lưu ý hành động này không thể được khôi phục trong tương lai.'
            }
          });

          dialogRef.afterClosed().subscribe(result => {
            debugger
            if (result == 'DONGY') {
              let model = new DeleteSchoolRequest();
              model.id = data.id;
              this.schoolEndpoint.deleteSchool(model)
                .then(res => {
                  if (res) {
                    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                      data: 'Xóa trường thành công',
                      duration: 3000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                    });

                    this.getSchoolsData();
                  } else {
                    this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                      data: 'Xóa trường không thành công',
                      duration: 3000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                    });
                  }
                })
                .catch((err: ErrorResponse) => {
                  this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                    data: err.errorDetail,
                    duration: 3000,
                    horizontalPosition: this.horizontalPosition,
                    verticalPosition: this.verticalPosition,
                  });
                });
            }
            else {
              if (result != undefined) {
                this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                  data: 'Bạn nhập chưa đúng, vui lòng thử lại',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }

            }
          });
        } else {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Đang có lớp, quản trị viên, giáo viên hoặc học sinh đang thuộc trường. Không được phép xóa',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      })
  }

  public addSchoolFromFile() {
    this._router.navigate(['/dashboard/manage-schools/add-school-from-file']);
    console.log("🚀 ~ file: box-filter-bottom.component.ts ~ line 46 ~ BoxFilterBottomComponent ~ addLesson ~ _router", this._router)

  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.schoolsPagination.items.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.selectedSchoolIds = [];
    } else {
      this.schoolsPagination.items.forEach(row => {
        this.selection.select(row);
        this.selectedSchoolIds.push(row.id);
      })
    }
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: School): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }

  /**Add Grade to School */
  AddGradeToSchool(data) {
    this.selection.clear();
    const dialogRef = this.dialog.open(ContentListGradesComponent, {
      disableClose: true,
      width: '450px',
      height: 'auto',
      data: data
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getSchoolsData();
        }
      }
    });
  }

  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }
}

import { School } from "sdk/cls-k12-sdk-js/src/services/school/models/School";

export class SchoolDataVM implements School {
    id?: number;
    name?: string;
    code?: string;
    divisionId?: number;
    donviname?: string;

    grades: string;

    static From(school: School, grades: string) {
        var m: SchoolDataVM = {
            id: school.id,
            name: school.name,
            code: school.code,
            divisionId: school.divisionId,
            donviname: school.donviname,

            grades: grades
        }
        return m;
    }
}
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentCreateSchoolComponent } from './content-create-school.component';

describe('ContentCreateSchoolComponent', () => {
  let component: ContentCreateSchoolComponent;
  let fixture: ComponentFixture<ContentCreateSchoolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentCreateSchoolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentCreateSchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

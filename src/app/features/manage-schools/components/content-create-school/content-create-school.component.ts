import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormControl, Validators } from '@angular/forms';
import { CreateSchoolRequest } from 'cls-k12-sdk-js/src/services/school/requests/createSchoolRequest';
import { SchoolEndpoint } from 'cls-k12-sdk-js/src/services/school/endpoints/SchoolEndpoint';
import { SchoolFilterRequests } from 'cls-k12-sdk-js/src/services/school/models/SchoolPagingRequest';
import { School } from 'cls-k12-sdk-js/src/services/school/models/School';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef } from '@angular/material/dialog';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { DivisionEndpoint } from 'cls-k12-sdk-js/src/services/division/endpoints/DivisionEndpoint';
import { DivisionOption } from 'cls-k12-sdk-js/src/services/division/models/Division';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { UserIdentity } from 'cls-k12-sdk-js/src';
import { AuthService } from 'src/app/core/services';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { take, takeUntil } from 'rxjs/operators';

const schoolsPaginationDefault: PaginatorResponse<School> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-content-create-school',
  templateUrl: './content-create-school.component.html',
  styleUrls: ['./content-create-school.component.scss']
})
export class ContentCreateSchoolComponent implements OnInit {

  schoolEndpoint: SchoolEndpoint;
  userIdentity: UserIdentity;
  baseApiUrl = 'http://localhost:65000';
  lblname = '';
  lblcode = '';
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  isFirstLoadDivisions = true;
  endpointDivision: DivisionEndpoint
  levelManageValue: number = 0;
  /**List division */
  protected divisions: DivisionOption[];

  public divisionNameInput: FormControl = new FormControl();

  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  public divisionCtrl: FormControl = new FormControl();
  protected _onDestroy = new Subject<void>();
  /** control for the selected division */
  public divisionFilterCtrl: FormControl = new FormControl();

  school: CreateSchoolRequest = {
    name: '',
    code: '',
  };
  schoolsPagination: PaginatorResponse<School> = schoolsPaginationDefault;
  schoolsPaginationFilter: SchoolFilterRequests = {
    pageNumber: 0,
    sizeNumber: 10,
    getCount: true,
    sortDirection: 'ASC',
    sortField: 'CRE',
  }

  schoolFilter = {
    divisionId: null
  }
  codePattern = '^([A-Za-z0-9]+(?:\.[A-Za-z0-9]+)*)$';

  codeSchoolFormControl = new FormControl('', [
    Validators.pattern(this.codePattern)
  ])

  isShowNotifi = false;
  constructor(private _authService: AuthService, private mdDialogRef: MatDialogRef<ContentCreateSchoolComponent>, private _snackBar: MatSnackBar) {

    this.schoolEndpoint = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();
    console.log("Tài khoản cấp: ", this.userIdentity.levelManage);
    console.log("Tài khoản cấp: ", this.userIdentity.divisionId);
    console.log("Vai trò: ", this.userIdentity.userTypeRole);
    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
      // this.getDataDivisionById(this.schoolFilter.divisionId);
    }
  }

  ngOnInit(): void {
    this.divisionFilterCtrl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterDivison();
    });
  }

  ngOnDestroy(): void {
    this._onDestroy.next();
    this._onDestroy.complete();
  }


  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectDivision != undefined) {
          this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id;
        }
      });
  }

  protected filterDivison() {
    debugger
    if (!this.divisions) {
      return;
    }
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }


  failureCallback() {
    alert("hihi 400");
  }


  onFocus() {
    this.isShowNotifi = true;
  }

  // getDataDivisionById(id : number) {
  //   this.endpointDivision.getDivisionsById(id)
  //     .then(res => {
  //       this.setData(res);
  //     })
  //     .catch(this.failureCallback);
  // }

  // setData(res: DivisionDetail){
  //   this.divisionNameInput.setValue(res.name);
  //   this.divisionNameInput.disable();
  // }

  onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.divisions = res;
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(
        )
    } else {
    }
  }

  resetForm() {
    this.lblcode = '';
    this.lblname = '';
  }

  checkForm() {
    var result = true;
    if (this.school.name === '') {
      this.lblname = 'Bạn chưa nhập tên trường!';
      result = false;
    }
    if (this.school.code === '') {
      this.lblcode = 'Bạn chưa nhập mã trường!';
      result = false;
    }
    return result;
  }


  submitForm(regForm: NgForm) {
    this.resetForm()
    if (this.checkForm()) {
      if (this.levelManageValue == 1) {
        this.school.divisionId = this.divisionCtrl.value;
      }
      if (this.levelManageValue == 2) {
        this.school.divisionId = this.schoolFilter.divisionId;
      }

      this.schoolEndpoint.createSchool(this.school)
        .then(res => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Thêm trường thành công!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Thêm trường không thành công',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
      this.mdDialogRef.close('submit')

    }


  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentListGradesComponent } from './content-list-grades.component';

describe('ContentListGradesComponent', () => {
  let component: ContentListGradesComponent;
  let fixture: ComponentFixture<ContentListGradesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentListGradesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentListGradesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

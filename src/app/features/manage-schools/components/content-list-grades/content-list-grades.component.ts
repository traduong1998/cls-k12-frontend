import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { School } from 'cls-k12-sdk-js/src/services/school/models/School';
import { GradeFilterRequests } from 'cls-k12-sdk-js/src/services/grade/models/GradePagingRequest';
import { Grade, GradeOfSchool } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { GradeEndpoint } from 'cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';
import { GradeOption } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { SelectionModel } from '@angular/cdk/collections';
import { AddGradeToSchoolRequest } from 'cls-k12-sdk-js/src/services/grade/requests/addGradeToSchoolRequest';
import { MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition, MatSnackBar } from '@angular/material/snack-bar';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { UserIdentity, CLSModules, CLSPermissions } from 'cls-k12-sdk-js/src';
import { AuthService } from 'src/app/core/services';


@Component({
  selector: 'app-content-list-grades',
  templateUrl: './content-list-grades.component.html',
  styleUrls: ['./content-list-grades.component.scss']
})
export class ContentListGradesComponent implements OnInit {
  isLoadingResults = true;
  baseApiUrl = 'http://localhost:65000';
  gradeEndpoint: GradeEndpoint;
  gradesPaginationFilter: GradeFilterRequests = {
    pageNumber: 0,
    sizeNumber: 10,
    getCount: true,
    sortDirection: 'ASC',
    sortField: 'CRE',
  }
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  selection = new SelectionModel<Grade>(true, []);
  selectedGradeIds = [];
  gradeListsOfSchool: GradeOfSchool[] = [];
  gradeLists: GradeOption[] = [];
  displayedColumns: string[] = ['id', 'name'];
  hasAddPermission: boolean;
  hasEditPermission: boolean;
  userIdentity: UserIdentity;

  constructor(private mdDialogRef: MatDialogRef<ContentListGradesComponent>, @Inject(MAT_DIALOG_DATA) public schoolInfo: School, private _snackBar: MatSnackBar,private _authService: AuthService) {
    this.gradeEndpoint = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();
    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.School, CLSPermissions.Add);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.School, CLSPermissions.Edit);
  }

  school: School = {
    id: this.schoolInfo.id,
  }

  ngOnInit(): void {
    this.getGradesData();
  }

  failureCallback() {
    this.isLoadingResults = false;
    alert("hihi 400");
  }


  getGradesData() {

    // this.gradeEndpoint.getGradeOptions(null)
    // .then(res => {
    //   this.gradeLists = res;
    //   this.gradeEndpoint.getGradeOfSchool(this.school.id)
    //     .then(mes => {
    //       this.gradeListsOfSchool = mes;
    //       this.gradeLists.forEach((v, i) => {
    //         let item = this.gradeListsOfSchool.find(x => x.id == v.id);

    //         if (item != null) {
    //           this.selection.select(v);
    //           this.selectedGradeIds.push(v.id);
    //         }

    //       });

    //     })
    //   console.log(this.selectedGradeIds);

    // })
    // .catch(this.failureCallback);
    this.isLoadingResults = true;
    this.gradeEndpoint.getGradeOptions(null)
      .then(res => {
        this.gradeLists = res;
        this.gradeEndpoint.getGradeOfSchool(this.school.id)
          .then(mes => {
            this.gradeListsOfSchool = mes;

            this.gradeListsOfSchool.forEach((v, i) => {
              this.gradeLists.forEach((u, j) => {
                if (v.id == u.id) {
                  this.selection.select(u);
                  this.selectedGradeIds.push(u.id);
                }
              })
            });
          })

      })
      .catch(this.failureCallback);
  }

  submitForm() {
    let model = new AddGradeToSchoolRequest();

    model.schoolId = this.school.id;
    model.gradeId = this.selectedGradeIds
    this.gradeEndpoint.addGradeToSchool(model)
      .then(res => {
        if (res) {
          this._snackBar.openFromComponent(SuccessSnackBarComponent, {
            data: 'Gán khối vào trường thành công',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        } else {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Gán khối vào trường không thành công',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      })
      .catch((err: ErrorResponse) => {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: err.errorDetail,
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
    this.mdDialogRef.close('submit')
  }

  changeCheck(event, row) {
    if (event.checked) {
      let index = this.selectedGradeIds.indexOf(row.id);
      if (index == -1) {
        this.selectedGradeIds.push(row.id);
      }
    } else {
      let index = this.selectedGradeIds.indexOf(row.id);
      this.selectedGradeIds.splice(index, 1);
    }
    console.log(this.selectedGradeIds);
  }


}

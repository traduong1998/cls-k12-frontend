import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards';
import { ManageSchoolsComponent } from './manage-schools.component';
import { CreateSchoolComponent } from './views/create-school/create-school.component';
import { UpdateSchoolComponent } from './views/update-school/update-school.component';
import { ListSchoolsComponent } from './views/list-schools/list-schools.component';
import { ListGradesComponent } from './views/list-grades/list-grades.component';
import { AddSchoolFromFileComponent } from './views/add-school-from-file/add-school-from-file.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: ManageSchoolsComponent,
    children: [
      {
        path: '',
        component: ListSchoolsComponent,
        data: {
          title: 'Danh sách trường',
        }
      },
      {
        path: 'create',
        component: CreateSchoolComponent,
        data: {
          title: 'Thêm mới trường',
        }
      },
      {
        path: 'update',
        component: UpdateSchoolComponent,
        data: {
          title: 'Chỉnh sửa trường',
        }
      },
      {
        path: 'listgrade',
        component: ListGradesComponent,
        data: {
          title: 'Danh sách khối thuộc trường',
        }
      },
      {
        path: 'add-school-from-file',
        component: AddSchoolFromFileComponent,
        data: {
          title: 'Thêm trường từ tệp tin',
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageSchoolsRoutingModule { }

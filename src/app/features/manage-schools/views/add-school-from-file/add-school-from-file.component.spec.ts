import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSchoolFromFileComponent } from './add-school-from-file.component';

describe('AddSchoolFromFileComponent', () => {
  let component: AddSchoolFromFileComponent;
  let fixture: ComponentFixture<AddSchoolFromFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSchoolFromFileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSchoolFromFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

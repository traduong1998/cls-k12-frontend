import { NgModule } from '@angular/core';
import { ManageSchoolsRoutingModule } from './manage-schools-routing.module';
import { ManageSchoolsComponent } from './manage-schools.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CreateSchoolComponent } from './views/create-school/create-school.component';
import { UpdateSchoolComponent } from './views/update-school/update-school.component';
import { ListSchoolsComponent } from './views/list-schools/list-schools.component';
import { ContentCreateSchoolComponent } from './components/content-create-school/content-create-school.component';
import { ContentUpdateSchoolComponent } from './components/content-update-school/content-update-school.component';
import { ContentListSchoolsComponent } from './components/content-list-schools/content-list-schools.component';
import { ListGradesComponent } from './views/list-grades/list-grades.component';
import { ContentListGradesComponent } from './components/content-list-grades/content-list-grades.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { AddSchoolFromFileComponent } from './views/add-school-from-file/add-school-from-file.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [ManageSchoolsComponent, CreateSchoolComponent, UpdateSchoolComponent, ListSchoolsComponent, ContentCreateSchoolComponent, ContentUpdateSchoolComponent, ContentListSchoolsComponent, ListGradesComponent, ContentListGradesComponent, AddSchoolFromFileComponent],
  imports: [
    SharedModule,
    ManageSchoolsRoutingModule,
    NgxMatSelectSearchModule,
    NgxSpinnerModule
  ]
})
export class ManageSchoolsModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmTeacherJoinDialogComponent } from './confirm-teacher-join-dialog.component';

describe('ConfirmTeacherJoinDialogComponent', () => {
  let component: ConfirmTeacherJoinDialogComponent;
  let fixture: ComponentFixture<ConfirmTeacherJoinDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirmTeacherJoinDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmTeacherJoinDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

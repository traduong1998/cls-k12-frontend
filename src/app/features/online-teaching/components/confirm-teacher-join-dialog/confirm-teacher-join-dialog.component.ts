import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { now } from 'moment';
import { MeetingRoomTeacherLogsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/meeting-room-teacher-logs/end-points/meeting-room-teacher-logs-endpoint';
import { MeetingRoomTeacherLogsModel } from 'cls-k12-sdk-js/src/services/meeting-room/models/meeting-room-teacher-logs-model';
import { OnlineClasses } from 'sdk/cls-k12-sdk-js/src/services/meeting-room/models/onlineClasses';
import { SETTING_LOG } from 'src/app/shared/configurations';

export interface DialogDataCloseResult {
  lastTimeWriteLog: number,
  isFirstWriteLog: boolean,
  isLoadAPI: boolean,
  priAction: string;
  dataResult: MeetingRoomTeacherLogsModel
}

export interface DataDialog {
  onlineClass: OnlineClasses,
  dataCloseResult: DialogDataCloseResult,
}

@Component({
  selector: 'app-confirm-teacher-join-dialog',
  templateUrl: './confirm-teacher-join-dialog.component.html',
  styleUrls: ['./confirm-teacher-join-dialog.component.scss']
})
export class ConfirmTeacherJoinDialogComponent implements OnInit {
  isFirstLoadData = true;
  timeWriteLog = 0;
  isFirstWriteLog = true;

  dataResult: MeetingRoomTeacherLogsModel
  name: string = 'Lớp học tương tác demo';
  startUrl = '';
  public meetingTeacherLogsStatus: string = 'NOLOGS | MEETINGSTARTED | MEETINGEND'

  private priAction: string = '';
  private meetingRoomTeacherLogsEndpoint: MeetingRoomTeacherLogsEndpoint

  constructor(
    public dialogRef: MatDialogRef<ConfirmTeacherJoinDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DataDialog) {
    this.meetingRoomTeacherLogsEndpoint = new MeetingRoomTeacherLogsEndpoint();
    this.startUrl = this.data.onlineClass.startURL ? this.data.onlineClass.startURL : '';
    this.name = this.data.onlineClass.name;
  }

  ngOnInit(): void {
    console.log(this.data);
    if (this.data.dataCloseResult.isLoadAPI
      && this.data.dataCloseResult.dataResult == null) {
      this.getData();
    } else {
      if (this.data.dataCloseResult.isLoadAPI
        && this.data.dataCloseResult.dataResult
        && this.data.onlineClass.id != this.data.dataCloseResult.dataResult.meetingRoomId) {
        this.getData();
      } else {
        if (!this.data.dataCloseResult.isLoadAPI && !this.data.dataCloseResult.dataResult)
          this.getData();
        else {
          this.timeWriteLog = this.data.dataCloseResult.lastTimeWriteLog;
          this.isFirstWriteLog = this.data.dataCloseResult.isFirstWriteLog;
          this.meetingTeacherLogsStatus = this.readStatus(this.data.dataCloseResult.dataResult);
          this.priAction = this.data.dataCloseResult.priAction;
          this.dataResult = this.data.dataCloseResult.dataResult;
          this.isFirstLoadData = false;
        }
      }
    }
  }

  private getData() {
    this.meetingRoomTeacherLogsEndpoint.getByMeetingRoomIdAndUserId(this.data.onlineClass.id)
      .then((res: MeetingRoomTeacherLogsModel) => {
        console.log("API Called");
        this.meetingTeacherLogsStatus = this.readStatus(res);
        this.dataResult = res;
        this.isFirstWriteLog = true;
        this.isFirstLoadData = false;
        if (this.meetingTeacherLogsStatus != "NOLOGS") {
          this.timeWriteLog = new Date(res.lastJoinDate).getTime();
        }
      });
  }
  ngAfterViewInit() {

  }

  /**Start learing when meeting has no log  NOLOGS*/
  public onStartLearingHandleClick() {
    this.applyJoinMeeting(this.startUrl);
    this.excuteButtonHandleClick(this.data.onlineClass.id, this.meetingTeacherLogsStatus, "START");

  }

  /** Watch room when meeting has no logs NOLOGS*/
  public onStartWatchRoomHandleClick() {

    // this.excuteButtonHandleClick(this.data.id, this.meetingTeacherLogsStatus, "WATCH");
    this.applyJoinMeeting(this.startUrl);
  }

  /** End learning when meeting has log and meeting is not end MEETINGSTARTED */
  public onEndLearningHandleClick() {

    this.excuteButtonHandleClick(this.data.onlineClass.id, this.meetingTeacherLogsStatus, "END");

  }

  /** join learning when meeting has log and meeting is not end MEETINGSTARTED */
  public onJoinRoomHandleClick() {

    this.excuteButtonHandleClick(this.data.onlineClass.id, this.meetingTeacherLogsStatus, "JOINSTARTED");
    this.applyJoinMeeting(this.startUrl);
  }

  /** join learning when meeting has log and meeting end */
  public onJoinWhenMeetingEndHandleClick() {

    this.excuteButtonHandleClick(this.data.onlineClass.id, this.meetingTeacherLogsStatus, "JOINEND");
    this.applyJoinMeeting(this.startUrl);
  }
  closeDialogHandleClick() {
    let dataCloseResult: DialogDataCloseResult = {
      isFirstWriteLog: this.isFirstWriteLog,
      lastTimeWriteLog: this.timeWriteLog,
      isLoadAPI: this.isFirstWriteLog,
      dataResult: this.dataResult,
      priAction: "CLOSE"
    }
    this.dialogRef.close(dataCloseResult);
  }

  /** 28/09/2021 LAMNV11 Add check Logs Meeting */
  private readStatus(val: MeetingRoomTeacherLogsModel): string {
    if (val) {
      if (!val.joinDate)
        return 'NOLOGS';
      else {
        if (!val.endDate) {
          return 'MEETINGSTARTED'
        }
        else {
          return 'MEETINGEND'
        }
      }
    } else {
      return 'NOLOGS';
    }
  }

  /** */

  private applyJoinMeeting(startUrl: string) {
    startUrl = startUrl.startsWith("http://") || startUrl.startsWith("https://") || startUrl.startsWith("ftp://") ?
      startUrl
      : `http://${startUrl}`
    window.open(startUrl, '_blank');
  }

  private excuteButtonHandleClick(meetingRoomId: number, status: string, action: string) {
    debugger;
    // if click in one action
    switch (status) {
      case 'NOLOGS':
        this.noLogsAction(action, meetingRoomId);
        break;

      case 'MEETINGSTARTED':
        if (this.timeWriteLog == 0 || (new Date().getTime() - this.timeWriteLog > SETTING_LOG) || (this.priAction && this.priAction != "CLOSE" && this.priAction != action)) {
          this.meetingStartAction(action, meetingRoomId)
        } else {

          let dataCloseResult: DialogDataCloseResult = {
            isFirstWriteLog: this.isFirstWriteLog,
            lastTimeWriteLog: this.timeWriteLog,
            isLoadAPI: this.isFirstWriteLog,
            dataResult: this.dataResult,
            priAction: action
          }

          this.dialogRef.close(dataCloseResult);
        }
        break;

      case 'MEETINGEND':
        if (this.timeWriteLog == 0 || (new Date().getTime() - this.timeWriteLog > 1000 * 60) || (this.priAction && this.priAction != "CLOSE" && this.priAction != action)) {

          this.meetingEndAction(action, meetingRoomId)
        } else {
          let dataCloseResult: DialogDataCloseResult = {
            isFirstWriteLog: this.isFirstWriteLog,
            lastTimeWriteLog: this.timeWriteLog,
            isLoadAPI: this.isFirstWriteLog,
            dataResult: this.dataResult,
            priAction: action
          }
          this.dialogRef.close(dataCloseResult);
        }
        break;
    }
  }

  private noLogsAction(action: string, meetingRoomId: number) {
    switch (action) {
      // start create log
      case "START":
        console.log(" Action create logs")
        this.meetingRoomTeacherLogsEndpoint.create(meetingRoomId).then((res: MeetingRoomTeacherLogsModel) => {
          if (res) {
            this.dataResult = res;
          }
          console.log(" Action create logs success")
          // let lastTimeLog = res.JoinDate.split(";")[0];
          console.log(this.data);
          this.isFirstWriteLog = false;
          this.timeWriteLog = new Date().getTime();

          let dataCloseResult: DialogDataCloseResult = {
            isFirstWriteLog: this.isFirstWriteLog,
            lastTimeWriteLog: this.timeWriteLog,
            isLoadAPI: this.isFirstWriteLog,
            dataResult: this.dataResult,
            priAction: "NOLOGS"
          }
          console.log("Data after close dialog", dataCloseResult)
          this.dialogRef.close(dataCloseResult);
        });

        break;
      case "WATCH":
        break;

      default:
        break;
    }
  }

  private meetingStartAction(action: string, meetingRoomId: number) {
    let actionRequest = '';
    switch (action) {
      case "END":
        actionRequest = 'END';
        this.meetingRoomTeacherLogsEndpoint.update(meetingRoomId, actionRequest).then((res: MeetingRoomTeacherLogsModel) => {
          if (res) {
            this.dataResult = res;
            this.isFirstWriteLog = false;
            this.timeWriteLog = new Date().getTime();
            let dataCloseResult: DialogDataCloseResult = {
              isFirstWriteLog: this.isFirstWriteLog,
              lastTimeWriteLog: this.timeWriteLog,
              isLoadAPI: this.isFirstWriteLog,
              dataResult: this.dataResult,
              priAction: action
            }
            console.log("Data after close dialog", dataCloseResult)
            this.dialogRef.close(dataCloseResult);
          }
        });
      case "JOINSTARTED":
        if ((new Date().getTime() - this.timeWriteLog > SETTING_LOG)) {
          this.meetingRoomTeacherLogsEndpoint.update(meetingRoomId, actionRequest).then((res: MeetingRoomTeacherLogsModel) => {
            if (res) {
              this.dataResult = res;
              this.isFirstWriteLog = false;
              this.timeWriteLog = new Date().getTime();
              let dataCloseResult: DialogDataCloseResult = {
                isFirstWriteLog: this.isFirstWriteLog,
                lastTimeWriteLog: this.timeWriteLog,
                isLoadAPI: this.isFirstWriteLog,
                dataResult: this.dataResult,
                priAction: action
              }
              console.log("Data after close dialog", dataCloseResult)
              this.dialogRef.close(dataCloseResult);
            }
          });
        }

        break;

      default:
        break;
    }
  }

  private meetingEndAction(action: string, meetingRoomId: number) {
    switch (action) {
      case "JOINEND":
        this.meetingRoomTeacherLogsEndpoint.update(meetingRoomId, action).then((res: MeetingRoomTeacherLogsModel) => {
          if (res) {
            this.dataResult = res;
            this.isFirstWriteLog = false;
            this.timeWriteLog = new Date().getTime();

            let dataCloseResult: DialogDataCloseResult = {
              isFirstWriteLog: this.isFirstWriteLog,
              lastTimeWriteLog: this.timeWriteLog,
              isLoadAPI: this.isFirstWriteLog,
              dataResult: this.dataResult,
              priAction: action
            }
            this.dialogRef.close(dataCloseResult);
          }
        });
        break;
      default:
        break;
    }
  }
}

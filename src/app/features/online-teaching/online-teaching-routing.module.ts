import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OnlineTeachingComponent } from './online-teaching.component';
import { AddOnlineClassesGooogleMeetComponent } from './views/add-online-classes-gooogle-meet/add-online-classes-gooogle-meet.component';
import { ListOnlineClassesComponent } from './views/list-online-classes/list-online-classes.component';
import { RegistStudentClassComponent } from './views/regist-student-class/regist-student-class.component';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { UpdateOnlineClassesGooogleMeetComponent } from './views/update-online-classes-gooogle-meet/update-online-classes-gooogle-meet.component';
import { RollCallStudentComponent } from './views/roll-call-student/roll-call-student.component';


const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: OnlineTeachingComponent,
    data: {
      title: 'Danh sách lớp học tương tác',
    },
    children: [
      {
        path: '',
        component: ListOnlineClassesComponent,
        data: {
          title: 'Danh sách lớp học tương tác',
        }
      },
      {
        path: 'add-online-classes',
        component: AddOnlineClassesGooogleMeetComponent,
        data: {
          title: 'Tạo lớp học tương tác',
        },
      },
      {
        path: 'update-online-classes/:classId',
        component: UpdateOnlineClassesGooogleMeetComponent,
        data: {
          title: 'Cập nhật lớp học tương tác',
        },
      },
    ]
  },
  {
    path: 'regist-student-class/:id',
    component: RegistStudentClassComponent,
    data: {
      title: 'Ghi danh lớp học',
    }
  },
  {
    path: 'roll-call-student/:divisionId/:schoolId/:meetingRoomId',
    component: RollCallStudentComponent,
    data: {
      title: 'Điểm danh',
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OnlineTeachingRoutingModule { }

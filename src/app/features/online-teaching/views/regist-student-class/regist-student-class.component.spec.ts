import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageRegistStudentClassComponent } from './regist-student-class.component';

describe('ManageRegistStudentClassComponent', () => {
  let component: ManageRegistStudentClassComponent;
  let fixture: ComponentFixture<ManageRegistStudentClassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageRegistStudentClassComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageRegistStudentClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

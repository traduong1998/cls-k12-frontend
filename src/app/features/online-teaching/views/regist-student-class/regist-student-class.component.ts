import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserIdentity } from 'cls-k12-sdk-js/src';
import { MatSelect } from '@angular/material/select';
import { FormBuilder, FormControl } from '@angular/forms';
import { AssignUserMeeting } from 'cls-k12-sdk-js/src/services/lesson/requests/assignStudentsRequest';
import { AssignStudentsEndpoint, DivisionEndpoint, DivisionOption, GradeEndpoint, GroupStudentEndpoint, SchoolEndpoint } from 'cls-k12-sdk-js/src';
import { ListStudentRegisterResponse } from 'cls-k12-sdk-js/src/services/lesson/responses/listStudentRegisterResponse';
import { ListStudentForAssignedResponse } from 'cls-k12-sdk-js/src/services/lesson/responses/listStudentForAssignedResponse';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { SelectionModel } from '@angular/cdk/collections';
import { pairwise, startWith, take, takeUntil } from 'rxjs/operators';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { Subject, ReplaySubject } from 'rxjs';
import { PaginatorResponse } from 'cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { StudentAssignPagingMeetingRequest } from 'cls-k12-sdk-js/src/services/lesson/models/StudentAssignPagingRequest';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { StudentRegistrationPagingRequest } from 'cls-k12-sdk-js/src/services/lesson/models/StudentRegistrationPagingRequest';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { AssignFrom } from '../../../learn/intefaces/assignFrom';
import { AuthService } from 'src/app/core/services';
import { Action } from '../../../learn/intefaces/action';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { RegistStudentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/regist-student-class/endpoints/regist-student-class-endpoint';
export interface Status {
  id?: boolean,
  name: string;
}

@Component({
  selector: 'app-regist-student-class',
  templateUrl: './regist-student-class.component.html',
  styleUrls: ['./regist-student-class.component.scss']
})
export class RegistStudentClassComponent implements OnInit {
  baseApiUrl = '';

  listStudentForAssignedDefault: PaginatorResponse<ListStudentForAssignedResponse> = {
    items: [],
    totalItems: 0
  };

  listStudentForRegistrationDefault: PaginatorResponse<ListStudentRegisterResponse> = {
    items: [],
    totalItems: 0
  };

  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointGroupStudent: GroupStudentEndpoint
  userIdentity: UserIdentity;
  levelManageValue: number = 0;
  registStudentEndpoint: RegistStudentEndpoint;

  //Paging List Student Assign
  assignDataSource: PaginatorResponse<ListStudentForAssignedResponse> = this.listStudentForAssignedDefault;
  registrationDataSource: PaginatorResponse<ListStudentRegisterResponse> = this.listStudentForRegistrationDefault;


  @ViewChild('assignPaginator') assignPaginator: MatPaginator;
  @ViewChild('registerPaginator') registerPaginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  /*  //#region FromGroup  */
  filterSignupLecturesForm = this.fb.group({
    divisionId: new FormControl(''),
    schoolId: new FormControl(''),
    gradeId: new FormControl(''),
    groupStudentId: new FormControl(''),
  });

  /* Public property */
  get divisionId() { return this.filterSignupLecturesForm.get('divisionId'); }
  isEnableDivision = true;
  isFirstLoadDivision = true;

  get schoolId() { return this.filterSignupLecturesForm.get('schoolId'); }
  isDisableSchoolId = false;
  isFirstLoadSchool = true;

  get gradeId() { return this.filterSignupLecturesForm.get('gradeId'); }
  isDisableGradeId = false;
  isFirstLoadGrade = true;

  get groupStudentId() { return this.filterSignupLecturesForm.get('groupStudentId'); }
  isDisableGroupStudentId = true;
  isFirstLoadGroupStudent = true;

  isDisableSchoolIdReview = false;
  isFirstLoadSchoolReview = true;

  isDisableGradeIdReview = false;
  isFirstLoadGradeReview = true;

  isDisableGroupStudentIdReview = true;
  isFirstLoadGroupStudentsReview = true;

  isDisableStatusReviewReview = false;
  isFirstLoadStatusReviewReview = true;

  //* Division */
  protected divisions: DivisionOption[];
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  /* School */
  protected schools: SchoolOption[];
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  /* grades */
  protected grades: GradeOption[];
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected groupStudents: GroupStudentOption[];
  public groupStudentCtrl: FormControl = new FormControl();
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;

  /* School */
  protected schoolsReview: SchoolOption[];
  public schoolFilterCtrlReview: FormControl = new FormControl();
  public filteredSchoolsReview: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchoolReview', { static: true }) singleSelectSchoolReview: MatSelect;

  /* grades */
  protected gradesReview: GradeOption[];
  public gradeFilterCtrlReview: FormControl = new FormControl();
  public filteredGradesReview: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGradeReview', { static: true }) singleSelectGradeReview: MatSelect;

  protected groupStudentsReview: GroupStudentOption[];
  public groupStudentCtrlReview: FormControl = new FormControl();
  public groupStudentFilterCtrlReview: FormControl = new FormControl();
  public filteredGroupStudentsReview: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectGroupStudentReview', { static: true }) singleSelectGroupStudentReview: MatSelect;

  public statusReviews: Status[] = [{ id: null, name: 'Chọn trạng thái' }, { id: true, name: 'Đã duyệt' }, { id: false, name: 'Chưa duyệt' }];
  public statusCtrlReview: FormControl = new FormControl();
  public statusFilterCtrlReview: FormControl = new FormControl();
  public filteredstatusReview: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectStatusReview', { static: true }) singleSelectStatusReview: MatSelect;

  protected _onDestroy = new Subject<void>();

  lessonId: number;
  lesson: Lesson;
  signUpforLecture: any;
  reviewStudent: any;
  isReady: boolean = false;
  assignDisplayedColumns: string[] = ['fullName', 'userName', 'className', 'action'];

  studentsAssignPaginationFilter: StudentAssignPagingMeetingRequest = {
    pageNumber: 1,
    sizeNumber: 10,
    getCount: true,
    meetingRoomId: +this.router.snapshot.paramMap.get("id"),
    DivisionId: 0,
    SchoolId: 0,
    GradeId: 0,
    GroupStudentId: 0,
    schoolYear: 0,
    SortDirection: 'DESC',
    SortField: 'CRE'
  }

  studentsRegistrationPaginationFilter: StudentRegistrationPagingRequest = {
    pageNumber: 1,
    sizeNumber: 10,
    getCount: true,
    LessonId: +this.router.snapshot.paramMap.get("id"),
    GroupStudentId: 0,
    SchoolId: 0,
    GradeId: 0,
    Status: null,
    schoolYear: 0,
    SortDirection: 'DESC',
    SortField: 'CRE'
  }


  assignSelection = new SelectionModel<ListStudentForAssignedResponse>(true, []);
  registrationSelection = new SelectionModel<ListStudentRegisterResponse>(true, []);

  registerDisplayedColumns: string[] = ['fullName', 'gradeName', 'className', 'registrationDate', 'status', 'action'];

  assignStudentEndpoint: AssignStudentsEndpoint;
  lessonEndpoint: LessonEndpoint;
  //List gửi lên api để ghi danh học sinh vào bài giảng
  assignStudentRequest: AssignUserMeeting;

  //Danh sách học sinh đã được ghi danh
  listOldStudentForAssignId: number[];

  //Danh sách học sinh đã được duyệt(trạng thái đã duyệt)
  listOldStudentRegisterId: number[];

  isListStudentRegisterReady: boolean = false;
  isListStudentForAssignReady: boolean = false;
  isLessonInfoReady: boolean = false;

  constructor(
    private router: ActivatedRoute,
    private _route: Router,
    private fb: FormBuilder,
    private _authService: AuthService,
    private _snackBar: MatSnackBar,
    private spinner: NgxSpinnerService,
    public dialog: MatDialog) {
    this.lessonEndpoint = new LessonEndpoint();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.registStudentEndpoint = new RegistStudentEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
    this.assignStudentEndpoint = new AssignStudentsEndpoint();
    this.lessonId = +this.router.snapshot.paramMap.get("id");
    // this.lessonId = 842
    this.assignStudentRequest = {
      meetingRoomId: this.lessonId,
      listUserMeetings: []
    };

    this.listOldStudentForAssignId = [];
    this.listOldStudentRegisterId = [];
    this.userIdentity = _authService.getTokenInfo();

    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
      // Tài khoản cấp Trường, schoolid fillter mặc định theo id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
      this.filterSignupLecturesForm.controls['schoolId'].setValue(this.userIdentity.schoolId);
      // this.usersPaginationFilter.schoolId = this.userIdentity.schoolId;
    }

    this.getLessonInfo();
  }

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  ngOnInit(): void {
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private getLessonInfo(): void {
    this.lessonEndpoint.getLessonById(this.lessonId).then((res) => {
      this.lesson = { ...res };
      this.getListStudentForAssign();
      // this.getListStudentRegistration();
      this.isLessonInfoReady = true;
    });
  }
  /* Get division */
  onDivisionSelectClicked() {
    if (this.isFirstLoadDivision) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivision = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {
    }
  }

  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionId?.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchool) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchool = false;
          this.schools = res;
          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {

    }
  }

  /* Get Grade */
  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolId.value == 0 ? null : this.schoolId.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrade || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrade = false;
          this.grades = res;

          if (this.grades && !this.grades.find(x => x.id == 0)) {
            this.grades.unshift({ id: 0, name: 'Chọn khối' });
          }
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());

        })
        .catch(err => { console.log(err) }
        )
    } else {

    }
  }

  /* Get GroupStudent */
  onGroupStudentSelectClicked() {

    var schoolIdSelected = this.schoolId?.value;
    var gradeIdSelected = this.gradeId?.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if ((this.isDisableGroupStudentId == true && this.levelManageValue !== 3) || !this.isFirstLoadGroupStudent) {
      return;
    } else {
      this.endpointGroupStudent.getGroupStudentOptions(schoolIdSelected, gradeIdSelected)
        .then(res => {
          this.isFirstLoadGroupStudent = false;
          this.groupStudents = res;
          this.filteredGroupStudents.next(this.groupStudents.slice());
        })
        .catch(

        )
    }
  }

  ngAfterViewInit() {
    this.assignPaginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.assignPaginator.page.subscribe(() => {
      this.assignSelection.clear();
      this.studentsAssignPaginationFilter.pageNumber = this.assignPaginator.pageIndex + 1;
      this.studentsAssignPaginationFilter.sizeNumber = this.assignPaginator.pageSize;
      this.getListStudentForAssign();
    });

    this.divisionId?.valueChanges
      .pipe(startWith(this.divisionId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            this.onResetSchoolSelect();
            this.onResetGradeSelect();
            this.onResetGroupStudentSelect();
          }
        }
      )

    this.schoolId?.valueChanges
      .pipe(startWith(this.schoolId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            this.onResetGradeSelect();
            this.onResetGroupStudentSelect();
            if (value < 1) {
              this.isDisableGroupStudentId = true;
            } else {
              this.isDisableGroupStudentId = false;

            }
          }
        }
      )

    this.gradeId?.valueChanges
      .pipe(startWith(this.gradeId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            this.onResetGroupStudentSelect();
          }
        }
      )

    this.groupStudentId?.valueChanges
      .pipe(startWith(this.gradeId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
          }
        }
      )
    // listen for search field value changes 
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });


    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    this.groupStudentFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudents();
      });

    this.schoolFilterCtrlReview.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchoolsReview();
      });

    this.gradeFilterCtrlReview.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGradesReview();
      });

    this.groupStudentFilterCtrlReview.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudentsReview();
      });

    this.setInitialValue();
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchoolsReview() {
    if (!this.schoolsReview) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrlReview.value;
    if (!search) {
      this.filteredSchoolsReview.next(this.schoolsReview.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchoolsReview.next(
      this.schoolsReview.filter(schoolReview => schoolReview.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGradesReview() {
    if (!this.gradesReview) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrlReview.value;
    if (!search) {
      this.filteredGradesReview.next(this.gradesReview.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGradesReview.next(
      this.gradesReview.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGroupStudentsReview() {
    if (!this.groupStudentsReview) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrlReview.value;
    if (!search) {
      this.filteredGroupStudentsReview.next(this.groupStudentsReview.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudentsReview.next(
      this.groupStudentsReview.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }

  private onResetSchoolSelect() {
    this.filterSignupLecturesForm.controls['schoolId'].setValue(0);
    this.isFirstLoadSchool = true;
    // this.filterLessonForm.controls['schoolFilterCtrl'].setValue(0);
  }

  /* reset change Grade */
  private onResetGradeSelect() {
    this.filterSignupLecturesForm.controls['gradeId'].setValue(0);
    // this.filterLessonForm.controls['gradeFilterCtrl'].setValue(null);
    this.isFirstLoadGrade = true;
  }

  private onResetGroupStudentSelect() {
    this.filterSignupLecturesForm.controls['groupStudentId'].setValue(null);
    // this.filterLessonForm.controls['gradeFilterCtrl'].setValue(null);
    this.isFirstLoadGroupStudent = true;
  }

  //Lấy danh sách học sinh cho 2 tab
  private getListStudentForAssign() {
    this.spinner.show('spinnerAsign');
    this.registStudentEndpoint.getListStudentAssign(this.studentsAssignPaginationFilter)
      .then(res => {
        this.spinner.hide('spinnerAsign');
        if (res) {
          this.assignDataSource = res;
          //Gán những học sinh đã được ghi danh vào trong list này
          this.assignDataSource.items.forEach((x) => {
            if (x.isApproval) {
              this.listOldStudentForAssignId.push(x.id);
              this.assignSelection.select(x);
            } else if (this.assignStudentRequest.listUserMeetings.find(y => y.id == x.id && y.action == 'ADD') != undefined) {
              this.assignSelection.select(x);
            }
          });
          this.isListStudentForAssignReady = true;
        }
      });
  }
  // private getListStudentRegistration() {
  //   this.spinner.show('spinnerRegis');
  //   this.assignStudentEndpoint.getListStudentRegister(this.studentsRegistrationPaginationFilter)
  //     .then((res) => {
  //       this.spinner.hide('spinnerRegis');
  //       if (res) {
  //         this.registrationDataSource = res;
  //         //Gán những học sinh đã được duyệt vào trong list này
  //         this.registrationDataSource.items.forEach((x) => {
  //           if (x.isApproval) {
  //             this.listOldStudentRegisterId.push(x.id);
  //             this.registrationSelection.select(x);
  //           } else if (this.assignStudentRequest.listUserMeetings.find(y => y.id == x.id && y.action == 'ADD') != undefined) {
  //             this.registrationSelection.select(x);
  //           }
  //         });
  //         this.isListStudentRegisterReady = true;
  //       }
  //     });
  // }
  onCancelAssignStudentTabClick(): void {
    this._route.navigate(['dashboard/online-teaching']);
  }

  onSaveAssignStudentClick(): void {
    this.registStudentEndpoint.assignUserMeeting(this.assignStudentRequest).then((res) => {
      if (res) {
        //Nếu thành công thì reset lại list request
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: 'Bạn vừa mới thực hiện ghi danh/ gỡ ghi danh thành công',
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
        //Update list oldId
        this.assignStudentRequest.listUserMeetings.forEach((std) => {
          if (std.action == Action.Add) {
            this.listOldStudentForAssignId.push(std.id);
            this.assignDataSource.items.map((x) => {
              if (x.id == std.id) {
                x.isApproval = true;
              }
            })
          }
          else {
            let index = this.listOldStudentForAssignId.indexOf(std.id);
            this.listOldStudentForAssignId.splice(index, 1);
            this.assignDataSource.items.map((x) => {
              if (x.id == std.id) {
                x.isApproval = false;
              }
            })
          }
        });
        this.getListStudentForAssign();
      } else {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Có lỗi xảy ra!',
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
        this.assignStudentRequest.listUserMeetings.forEach((std) => {
          let row = this.listStudentForAssignedDefault.items.find(x => x.id == std.id);
          this.assignSelection.toggle(row);
        });
      }
      // this.assignStudentRequest.listUserMeetings = [];
    }, err=>{console.log(err)})

  }

  onChangeAssignCheckBox(checked, row: ListStudentForAssignedResponse): void {
    this.assignSelection.toggle(row);
    // nếu chọn thì thêm vào list request với action là add
    if (checked) {
      //Kiểm tra xem nếu là chọn thằng cũ thì chắc là nãy nó bị bỏ chọn rồi giờ chọn lại nên remove khỏi list request đi
      if (this.listOldStudentForAssignId.includes(row.id)) {
        let std = this.assignStudentRequest.listUserMeetings.find(x => x.id == row.id);
        let index = this.assignStudentRequest.listUserMeetings.indexOf(std)
        this.assignStudentRequest.listUserMeetings.splice(index, 1);
      }
      else {
        this.assignStudentRequest.listUserMeetings.push({ id: row.id, action: Action.Add });
      }
    }
    // nếu bỏ chọn thì ...
    else {
      //Nếu bỏ chọn thằng đã được ghi danh thì thêm vào request để biết mà gỡ ghi danh
      if (this.listOldStudentForAssignId.includes(row.id)) {
        this.assignStudentRequest.listUserMeetings.push({ id: row.id, action: Action.Delete });
      }
      //Bỏ chọn thằng mới thì gỡ ra luôn khỏi list, ko gửi lên
      else {
        let std = this.assignStudentRequest.listUserMeetings.find(x => x.id == row.id);
        let index = this.assignStudentRequest.listUserMeetings.indexOf(std);
        if (!(index < 0)) {
          this.assignStudentRequest.listUserMeetings.splice(index, 1);
        }
      }
    }
  }

  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectDivision && (this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id);
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool && (this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id);
      });

    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGrade && (this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id);
      });

    this.filteredGroupStudents
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGroupStudent && (this.singleSelectGroupStudent.compareWith = (a: GroupStudentOption, b: GroupStudentOption) => a && b && a.id === b.id);
      });
  }

  /**Mấy hàm này danh cho check box bên tab ghi danh */
  /** Whether the number of selected elements matches the total number of rows. */
  isAssignAllSelected() {
    const numSelected = this.assignSelection.selected.length;
    const numRows = this.assignDataSource.items.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  assignMasterToggle() {
    if (this.isAssignAllSelected()) {
      //Nếu là bỏ chọn hết thì thêm những học sinh đã ghi danh vào model để gỡ ghi danh (action: Delete)
      this.assignDataSource.items.forEach((std) => {
        //Mấy thằng cũ đã được ghi danh thì kiểm tra xem nó đã được thêm vào trước đó với action delete chưa
        if (std.isApproval) {
          if (this.assignStudentRequest.listUserMeetings.find(x => x.id == std.id && x.action == Action.Delete) == undefined) {
            this.assignStudentRequest.listUserMeetings.push({ id: std.id, action: Action.Delete });
          }
        } else {
          let studentAdd = this.assignStudentRequest.listUserMeetings.find(x => x.id == std.id && x.action == Action.Add);
          if (studentAdd != undefined) {
            let index = this.assignStudentRequest.listUserMeetings.indexOf(studentAdd);
            this.assignStudentRequest.listUserMeetings.splice(index, 1);
          }
        }
      });
      this.assignSelection.clear();
    } else {

      // Nếu chọn hết thì thêm những học sinh chưa ghi dahnh vào model để gửi lên(action: Add)
      this.assignDataSource.items.forEach((std) => {
        if (!std.isApproval) {
          if (this.assignStudentRequest.listUserMeetings.find(x => x.id == std.id && x.action == Action.Add) == undefined) {
            this.assignStudentRequest.listUserMeetings.push({ id: std.id, action: Action.Add });
          }
        }
        else {
          let studentDel = this.assignStudentRequest.listUserMeetings.find(x => x.id == std.id && x.action == Action.Delete);
          if (studentDel != undefined) {
            let index = this.assignStudentRequest.listUserMeetings.indexOf(studentDel);
            this.assignStudentRequest.listUserMeetings.splice(index, 1);
          }
        }
      });
      this.assignDataSource.items.forEach(row => this.assignSelection.select(row));
    }
  }

  /** The label for the checkbox on the passed row */
  assignCheckboxLabel(row?: ListStudentForAssignedResponse): string {
    if (!row) {
      return `${this.isAssignAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.assignSelection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  /**Mấy hàm này danh cho check box bên tab đăng ký */
  isRegisAllSelected() {
    const numSelected = this.registrationSelection.selected.length;
    const numRows = this.registrationDataSource.items.length;
    return numSelected === numRows;
  }

  regisCheckboxLabel(row?: ListStudentRegisterResponse): string {
    if (!row) {
      return `${this.isRegisAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.registrationSelection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  onSubmitSignLectures() {
    //reset lại list học sinh đã chọn và list học sinh đã ghi danh
    this.assignPaginator.pageIndex = 0;
    this.assignSelection.clear();
    this.listOldStudentForAssignId = [];
    this.studentsAssignPaginationFilter.pageNumber = 1;
    this.studentsAssignPaginationFilter.meetingRoomId = this.lessonId
    this.studentsAssignPaginationFilter.DivisionId = this.filterSignupLecturesForm.controls['divisionId'].value
    this.studentsAssignPaginationFilter.SchoolId = this.filterSignupLecturesForm.controls['schoolId'].value
    this.studentsAssignPaginationFilter.GradeId = this.filterSignupLecturesForm.controls['gradeId'].value
    this.studentsAssignPaginationFilter.GroupStudentId = this.filterSignupLecturesForm.controls['groupStudentId'].value
    this.getListStudentForAssign();
  }

  tabChanged(tab: MatTabChangeEvent) {
    if (tab.index === 1) {
      this.dialog.open(ConfirmationDialogComponent, {
        width: '474px',
        data: {
          message: `Tính năng đang được phát triển`, buttonText: { ok: 'Yes', cancel: 'Đóng' }
        },
      });
    }
  }
}

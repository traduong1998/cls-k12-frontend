import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachingZoomComponent } from './teaching-zoom.component';

describe('TeachingZoomComponent', () => {
  let component: TeachingZoomComponent;
  let fixture: ComponentFixture<TeachingZoomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeachingZoomComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeachingZoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

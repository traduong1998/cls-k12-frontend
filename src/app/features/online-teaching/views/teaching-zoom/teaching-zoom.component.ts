import { Component, HostListener, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { CLS, LevelManages, UserEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { ContentLearnerDetailEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/content-learner-detail-endpoint';
import { MeetingDetailandSignature } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/models/meeting-detail-and-signature';
import { AuthService } from 'src/app/core/services/auth.service';
import { Deprecated } from 'src/app/shared/configurations';
export class ZoomMeeting {
  name: string
  mn: string
  email: string
  pwd: string
  role: string
  lang: string
  signature: string
  china: string
  apiKey: string
}

@Component({
  selector: 'app-teaching-zoom',
  templateUrl: './teaching-zoom.component.html',
  styleUrls: ['./teaching-zoom.component.scss']
})
@Deprecated('The function is used when integrated Zoom SDK')
export class TeachingZoomComponent implements OnInit {
  contentLearnerDetailEndpoint: ContentLearnerDetailEndpoint;
  private lessonId: number;
  private contentId: number;
  private zoomMeetingId: string;
  public baseUrl: string = '';
  linkZoomUrl: string = '';
  zoomMeeting: any;
  endpointUser: UserEndpoint;
  userIdentity: UserIdentity;
  apiKey = 'IcL6-LMxTx-QhTFw2MMkIw'
  meetingNumber = ''
  leaveUrl = 'http://localhost:4200'
  userName = ''
  userEmail = 'lovenco0410@gmail.com'
  passWord = ''
  signature = '';
  data: ZoomMeeting = new ZoomMeeting();
  constructor(private router: ActivatedRoute,
    private _authService: AuthService,
    private sanitizer: DomSanitizer) {
    this.lessonId = +this.router.snapshot.paramMap.get('lessonId');
    this.contentId = +this.router.snapshot.paramMap.get('id');
    this.zoomMeetingId = this.router.snapshot.paramMap.get('meetingId');
    this.contentLearnerDetailEndpoint = new ContentLearnerDetailEndpoint();
    this.baseUrl = CLS.getConfig().apiBaseUrl;
  }

  ngOnInit(): void {
    this.getMeetingDetailAndGenarateSignature();
    this.userIdentity = this._authService.getTokenInfo();
    this.userName = this.userIdentity.fullName;
  }

  @HostListener("window:message", ["$event"])
  SampleFunction($event: MessageEvent) {

    console.log($event.data)
    if ($event.data?.leave) {
      window.close();
    }
  }
  //#region PUBLIC METHOD
  public getLinkZoomResourceURL() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.linkZoomUrl);
  }

  private getMeetingDetailAndGenarateSignature() {
    this.contentLearnerDetailEndpoint
      .getZoomMeetingDetail(this.lessonId, this.contentId, this.zoomMeetingId)
      .then((res: MeetingDetailandSignature) => {
        if (res) {
          this.data.apiKey = res.meeting.apiKey
          this.meetingNumber = res.meeting.id.toString();
          this.data.china = "0";
          this.data.email = res.email;
          this.data.name = this.userIdentity.fullName;
          this.passWord = res.meeting.password;
          this.signature = res.signature;
          this.data.role = '1'; // TODO : Change value in here
          this.linkZoomUrl = `http://localhost:9904/api/v1/lessons/${this.lessonId}/contents/${this.contentId}/ZoomMeetings/join-meeting?name=${this.userName}&mn=${this.meetingNumber}&email=${this.data.email}&pwd=${this.passWord}&role=1&lang=vi-VN&signature=${res.signature}&china=0&apiKey=${this.data.apiKey}&lessonId=${this.lessonId}&contentId=${this.contentId}`
        }
      }).then(() => {
      })
  }
}

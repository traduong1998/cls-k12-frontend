import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ReplaySubject, Subject } from 'rxjs';
import { pairwise, startWith, takeUntil } from 'rxjs/operators';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { ContentLearnerDetailEndpoint, LessonSelect } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/content-learner-detail-endpoint';
import { TeacherZoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/teacherZoomEndpoint';
import { VirtualClassRoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/virtual-classroom-endpoints';
import { OnlineClasses } from 'sdk/cls-k12-sdk-js/src/services/meeting-room/models/onlineClasses';
import { VirtualClassRoom } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/virtualClassRoom';
import { AuthService } from 'src/app/core/services';
import { Configuration } from 'src/app/shared/configurations';
import { VerifyAccountDialogComponent, VerifyAccountType } from 'src/app/shared/dialog/verify-account-dialog/verify-account-dialog.component';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { OnlineClassesEndpoint } from 'sdk/cls-k12-sdk-js/src/services/meeting-room/endpoints/onlineClassesEndpoint';
import { OnlineClassesFilterRequest } from 'sdk/cls-k12-sdk-js/src/services/meeting-room/requests/onlineClassesFilterRequest';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { MessageService } from 'src/app/shared/services/message.service';
import { CLS, CLSModules, CLSPermissions, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { ConfirmTeacherJoinDialogComponent, DataDialog, DialogDataCloseResult } from '../../components/confirm-teacher-join-dialog/confirm-teacher-join-dialog.component';
@Component({
  selector: 'app-list-online-classes',
  templateUrl: './list-online-classes.component.html',
  styleUrls: ['./list-online-classes.component.scss']
})
export class ListOnlineClassesComponent implements OnInit {

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('dpFromDate') dpFromDate;
  @ViewChild('dpToDate') dpToDate;

  @ViewChild('paginatorClasses') paginatorClasses: MatPaginator;
  @ViewChild('dpFromDateClasses') dpFromDateClasses;
  @ViewChild('dpToDateClasses') dpToDateClasses;
  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT
  private virtualClassRoomEndpoint: VirtualClassRoomEndpoint;
  private contentLearnerDetailEndpoint: ContentLearnerDetailEndpoint;
  teacherZoomEndpoint = new TeacherZoomEndpoint();
  virtualClassroomEndpoint = new VirtualClassRoomEndpoint();
  onlineClassesEnpoint: OnlineClassesEndpoint;

  hasAddClassPermission: boolean;
  hasEditClassPermission: boolean;
  hasDeleteClassPermission: boolean;
  hasAssignClassPermission: boolean;
  userIdentity: UserIdentity;

  //#endregion

  //#region PRIVATE PROPERTY
  private dataCloseResult: DialogDataCloseResult = { isLoadAPI: true, isFirstWriteLog: true, lastTimeWriteLog: 0, dataResult: null, priAction: '' };
  private lessonDefault: LessonSelect[] = [];
  private isFirstLoad = true;
  private isFirstLoadClasses = true;
  private isCallAPI: boolean;
  //#endregion

  //#region PUBLIC PROPERTY
  userId: number;
  public listOnlineClassroom: VirtualClassRoom[] = [];
  public listOnlineClasses: OnlineClasses[] = [];
  public count = 0;
  public countClasses = 0;
  isShowFilter = true;
  isFullScreen = true;
  public displayedColumns = ['stt', 'name', 'lessonName', 'startDate', 'endDate', 'status', 'function'];
  public displayedColumnsClasses = ['stt', 'name', 'startDate', 'endDate', 'status', 'function'];
  public filterOnlineTeaching = this.formGroup.group({
    startDate: new FormControl(''),
    endDate: new FormControl(''),
    keyword: new FormControl('', Validators.maxLength(50)),
    status: new FormControl(''),
    lessonId: new FormControl(''),
    page: new FormControl(0),
    size: new FormControl(10),
  })

  public filterOnlineClasses = this.formGroup.group({
    fromDate: new FormControl(''),
    toDate: new FormControl(''),
    keyWord: new FormControl('', Validators.maxLength(50)),
    status: new FormControl(''),
    meetingType: new FormControl(''),
    page: new FormControl(0),
    size: new FormControl(10),
    getCount: true
  })

  protected lessonSelect: LessonSelect[] = [];
  public lessonSelectFilterCtrl: FormControl = new FormControl();
  public filteredLessons: ReplaySubject<LessonSelect[]> = new ReplaySubject<LessonSelect[]>(1);
  @ViewChild('singleSelectLesson', { static: true }) singleSelectDivision: MatSelect;

  public objectRequestClone = null;
  public objectRequestCloneClasses = null;
  public isFirstLoadLesson: boolean = true;
  public request: any = null;
  public requestClasses: OnlineClassesFilterRequest = new OnlineClassesFilterRequest;
  public isDisableFilterSubmit = false;
  public isDisableFilterSubmitClasses = false;

  protected _onDestroy = new Subject<void>();
  get lessonId() { return this.filterOnlineTeaching.get('lessonId'); }
  get startDate() { return this.filterOnlineTeaching.get('startDate'); }
  get endDate() { return this.filterOnlineTeaching.get('endDate'); }
  get keyword() { return this.filterOnlineTeaching.get('keyword'); }
  get status() { return this.filterOnlineTeaching.get('status'); }

  get fromDateClasses() { return this.filterOnlineClasses.get('fromDate'); }
  get toDateClasses() { return this.filterOnlineClasses.get('toDate'); }
  get keywordClasses() { return this.filterOnlineClasses.get('keyWord'); }
  get statusClasses() { return this.filterOnlineClasses.get('status'); }

  baseApiUrl = '';

  //#endregion

  //#region CONSTRUCTOR
  constructor(private formGroup: FormBuilder,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private authenService: AuthService,
    private _router: Router,
    private _messageService: MessageService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.userId = authenService.getTokenInfo().userId;
    this.virtualClassRoomEndpoint = new VirtualClassRoomEndpoint();
    this.contentLearnerDetailEndpoint = new ContentLearnerDetailEndpoint();

    this.onlineClassesEnpoint = new OnlineClassesEndpoint();

    this.userIdentity = authenService.getTokenInfo();
    this.hasAddClassPermission = this.userIdentity.hasPermission(CLSModules.MeetingRoom, CLSPermissions.Add);
    this.hasEditClassPermission = this.userIdentity.hasPermission(CLSModules.MeetingRoom, CLSPermissions.Edit);
    this.hasDeleteClassPermission = this.userIdentity.hasPermission(CLSModules.MeetingRoom, CLSPermissions.Delete);
    this.hasAssignClassPermission = this.userIdentity.hasPermission(CLSModules.MeetingRoom, CLSPermissions.Assign);

    this.baseApiUrl = CLS.getConfig().apiBaseUrl;


  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {
    this.isFullScreen = false;
    this.objectRequestClone = { ...this.request }
    this.loadData(this.isFirstLoad);

    this.objectRequestCloneClasses = { ...this.requestClasses }
    this.loadDataClasses(this.isFirstLoadClasses);

    this.isCallAPI = true;
  }
  pageChange(event?: PageEvent) {
    this.loadData(this.isFirstLoad);
    this.isFirstLoadLesson = true;
  }

  pageChangeClasses(event?: PageEvent) {
    this.loadDataClasses(this.isFirstLoadClasses);
    this.isFirstLoadLesson = true;
  }

  ngAfterViewInit() {
    this.lessonId.valueChanges
      .pipe(startWith(this.lessonId?.value),
        pairwise())
      .subscribe(() => {

      }
      )

    // listen for search field value changes 
    this.lessonSelectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {


        this.filterLessons();
      });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  //#endregion

  //#region PUBLIC METHOD

  public onSubmit() {

    this.isFirstLoad = false;
    this.paginator.pageIndex = 0;
    this.loadData(this.isFirstLoad);
  }

  public onSubmitClasses() {

    this.isFirstLoadClasses = false;
    this.paginatorClasses.pageIndex = 0;
    this.loadDataClasses(this.isFirstLoadClasses);
  }

  openDpFromDate() {
    this.dpFromDate.open();
  }

  openDpFromDateClasses() {
    this.dpFromDateClasses.open();
  }

  openDpToDate() {
    this.dpToDate.open();
  }

  openDpToDateClasses() {
    this.dpToDateClasses.open();
  }

  /** Vào dạy */
  public onOnlineTeachingHandleClick(
    contentId: number,
    lessonId: number,
    isOwnerRoom: boolean,
    id: number
  ) {
    if (!isOwnerRoom) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Bạn không thể vào dạy khi bạn không phải là người tạo lớp học này",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      })
      return;
    }
    this.spinner.show();
    var newTab = window.open('', '_blank');
    this.virtualClassRoomEndpoint.getMeeting(contentId, lessonId, true)
      .then(res => {
        if (res) {
          let findMeeting = this.listOnlineClassroom.find(x => x.zoomMeetingId == +res.id)
          if (findMeeting) {
            findMeeting.isNeedUpdatekey = false;
            findMeeting.joinURL = res.join_url;
            findMeeting.zoomMeetingId = +res.id;
          } else console.error(`Can't found meeting with old id : ${findMeeting.zoomMeetingId}`);
          newTab.location.href = res.start_url;
          this.spinner.hide();
        } else {
          newTab.close();
        }
      })
      .catch((err: ErrorResponse) => {
        let data: VerifyAccountType
          = {
          title: err.errorDetail,
          detail: Configuration.MESSAGE_CHANGE_ZOOM_ROOM_DETAIL,
          buttonText: Configuration.BUTTONLINKTEXT
        }

        if (err.statusCode == 401 || err.statusCode == 404) {
          switch (err.statusCode) {
            case 401:
              data.detail = Configuration.MESSAGE_DONT_ZOOM_ROOM_DETAIL
              data.buttonText = Configuration.BUTTONSETTINGTEXT
              break;
            case 404:
              break;
            default:
              break;
          }
          newTab.close();
          const verifyAccountDialog
            = this.dialog.open(VerifyAccountDialogComponent, {
              data: data
            });
          verifyAccountDialog.afterClosed().subscribe(result => {
            if (result) {
              const catchTab = window.open('', '_blank');
              switch (err.statusCode) {
                case 401:
                  this._router.navigateByUrl(`learner/user-infor?tab=create-zoom-account`);
                  catchTab.close();
                  this.spinner.hide();
                  break;

                case 404:
                  this.virtualClassroomEndpoint.createMeeting(contentId, lessonId)
                    .then(res => {
                      this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                        data: "Tạo liên kết thành công",
                        duration: 3000,
                        horizontalPosition: this.horizontalPosition,
                        verticalPosition: this.verticalPosition,
                      });
                      this.changeLinkMeetingUpdated(id, res);
                      catchTab.location.href = res.start_url;
                    })
                    .catch((err: ErrorResponse) => {
                      console.error(err);
                      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                        data: "Tạo liên kết thất bại",
                        duration: 3000,
                        horizontalPosition: this.horizontalPosition,
                        verticalPosition: this.verticalPosition,
                      });

                      catchTab.close();
                    })
                  this.spinner.hide();
                  break;
                default:
                  newTab.close();
                  break;
              }
            } else {
            }
          })
        }
        this.spinner.hide();
      });
  }

  public onOnlineTeachingHandleClickClasses(element: OnlineClasses) {
    let dataDialog: DataDialog = { onlineClass: element, dataCloseResult: this.dataCloseResult }
    const dialogRef = this.dialog.open(ConfirmTeacherJoinDialogComponent, {
      width: '500px',
      data: dataDialog
    });

    dialogRef.afterClosed().subscribe((result: DialogDataCloseResult) => {
      if (result) {
        this.dataCloseResult = JSON.parse(JSON.stringify(result));
      }
    });
  }

  /* Get division */
  onLessonSelectHandleClick() {
    if (this.isFirstLoadLesson && this.listOnlineClassroom) {
      let lessonId = this.listOnlineClassroom.map(x => { return x.lessonId });
      this.contentLearnerDetailEndpoint.getLessons(lessonId)
        .then(res => {
          this.isFirstLoadLesson = false;
          this.lessonSelect = [];
          this.lessonSelect = res;
          if (this.lessonSelect && !this.lessonSelect.find(x => x.id == 0 || x.id == null)) {
            this.lessonSelect.unshift({ id: null, name: 'Chọn bài giảng' })
          }
          this.filteredLessons.next(this.lessonSelect.slice());
        })
    } else {

    }
  }

  protected filterLessons() {
    if (!this.lessonSelect) {
      return;
    }
    // get the search keyword
    let search = this.lessonSelectFilterCtrl.value;
    if (!search) {
      this.filteredLessons.next(this.lessonSelect.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredLessons.next(
      this.lessonSelect.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }

  clearDate(field: string) {
    if (field === 'from') {
      this.startDate.setValue('');
    } else {
      this.endDate.setValue('');
    }
  }

  clearDateClasses(field: string) {
    if (field === 'from') {
      this.fromDateClasses.setValue('');
    } else {
      this.toDateClasses.setValue('');
    }
  }
  //#endregion

  //#region PRIVATE METHOD
  private loadData(isFirstLoad: boolean) {
    this.spinner.show();
    this.isDisableFilterSubmit = true;
    if (!isFirstLoad) {
      this.request = { ...this.filterOnlineTeaching.value };
      this.request.page = this.paginator.pageIndex;
      this.request.size = this.paginator.pageSize;
      if (this.compareObject(this.objectRequestClone, this.request)) {
        this.spinner.hide();
        this.isDisableFilterSubmit = false;
        return;
      }
    }

    this.virtualClassRoomEndpoint.getListOnlineTeachingFilter(this.request).then(res => {
      if (res) {
        this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
        this.listOnlineClassroom = res.items;
        this.count = res.totalItems
      }
      this.spinner.hide();
    }).catch(err => {
      console.log(err);
      this.spinner.hide();
    })

    this.objectRequestClone = { ...this.request }
    this.isDisableFilterSubmit = false;
    this.isFirstLoad = false;
  }

  private loadDataClasses(isFirstLoadClasses: boolean) {
    this.spinner.show();
    this.isDisableFilterSubmitClasses = true;
    if (!isFirstLoadClasses) {
      this.requestClasses = { ...this.filterOnlineClasses.value };
      this.requestClasses.page = this.paginatorClasses.pageIndex + 1;
      this.requestClasses.size = this.paginatorClasses.pageSize;
      if (this.compareObject(this.objectRequestCloneClasses, this.requestClasses)) {
        this.spinner.hide();
        this.isDisableFilterSubmitClasses = false;
        return;
      }
    }
    else {
      this.requestClasses.getCount = true;
    }

    this.onlineClassesEnpoint.GetMeetingRoomsPaging(this.requestClasses).then(res => {
      if (res) {
        this.paginatorClasses._intl.itemsPerPageLabel = "Số dòng trên trang";
        this.listOnlineClasses = res.items;
        console.log(this.listOnlineClasses);
        this.countClasses = res.totalItems
      }
      this.spinner.hide();
    }).catch(err => {
      console.log(err);
      this.spinner.hide();
    })

    this.objectRequestCloneClasses = { ...this.requestClasses }
    this.isDisableFilterSubmitClasses = false;
    this.isFirstLoadClasses = false;
  }

  private compareObject(objectfisrt: any, objectSecond: any) {
    if (JSON.stringify(objectfisrt) == JSON.stringify(objectSecond)) {
      return true;
    }
    return false;
  }

  public onUpdateLinkZoomHandleClick(contentId: number, lessonId: number, meetingId: number) {
    if (contentId && lessonId) {
      this.spinner.show();
      this.virtualClassRoomEndpoint.getMeeting(contentId, lessonId, true)
        .then(res => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: "Tạo liên kết thành công",
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            this.changeLinkMeetingUpdated(meetingId, res);
            this.spinner.hide();
          } else {

          }
        })
        .catch((err: ErrorResponse) => {
          let data: VerifyAccountType = { title: err.errorDetail, detail: Configuration.MESSAGE_CHANGE_ZOOM_ROOM_DETAIL, buttonText: Configuration.BUTTONLINKTEXT }
          if (err.statusCode == 401 || err.statusCode == 404) {
            switch (err.statusCode) {
              case 401:
                data.detail = Configuration.MESSAGE_DONT_ZOOM_ROOM_DETAIL
                data.buttonText = Configuration.BUTTONSETTINGTEXT
                break;
              case 404:

                break;
              default:
                break;
            }
            const verifyAccountDialog = this.dialog.open(VerifyAccountDialogComponent, {
              data: data
            });
            verifyAccountDialog.afterClosed().subscribe(result => {
              if (result) {
                switch (err.statusCode) {
                  case 401:
                    this._router.navigateByUrl(`learner/user-infor?tab=create-zoom-account`);
                    this.spinner.hide();
                    break;

                  case 404:
                    this.virtualClassroomEndpoint.createMeeting(contentId, lessonId)
                      .then(res => {
                        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                          data: "Tạo liên kết thành công",
                          duration: 3000,
                          horizontalPosition: this.horizontalPosition,
                          verticalPosition: this.verticalPosition,
                        });
                        this.changeLinkMeetingUpdated(meetingId,
                          res);
                      })
                      .catch((err: ErrorResponse) => {
                        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                          data: "Tạo liên kết thất bại",
                          duration: 3000,
                          horizontalPosition: this.horizontalPosition,
                          verticalPosition: this.verticalPosition,
                        });
                      })
                    this.spinner.hide();
                    break;
                  default:
                    break;
                }

              } else {
                // this._router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);
                // return
                //  this.getContent();
              }
            })
          }
          this.spinner.hide();
        });
    }
  }

  public onCopyLinkHandleClick(
    contentId: number,
    lessonId: number,
    meetingId: number,
    zoomMeetingId: number,
    joinURL: string
  ) {
    const create_copy = (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', joinURL);
      e.preventDefault();
    };

    document.addEventListener('copy', create_copy);
    document.execCommand('copy');
    document.removeEventListener('copy', create_copy);

    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
      data: "Đã sao chép liên kết",
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  public onCopyLinkHandleClickClasses(joinURL: string) {
    const create_copy = (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', joinURL);
      e.preventDefault();
    };

    document.addEventListener('copy', create_copy);
    document.execCommand('copy');
    document.removeEventListener('copy', create_copy);

    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
      data: "Đã sao chép liên kết",
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  private changeLinkMeetingUpdated(meetingId: number, res: any) {
    let findMeeting = this.listOnlineClassroom.find(x => x.meetingId == meetingId)
    if (findMeeting) {
      findMeeting.isNeedUpdatekey = false;
      findMeeting.joinURL = res.join_url;
      findMeeting.startURL = res.start_url;
      findMeeting.zoomMeetingId = +res.id;
    } else console.error(`Can't found meeting with old id : ${meetingId}`);
  }

  private onCreateClassGoogleMeetHandleClickClasses() {
    this._router.navigate([`/dashboard/online-teaching/add-online-classes`], { queryParams: { type: 'GMT' } });

  }

  private onCreateClassZoomHandleClickClasses() {
    this._router.navigate([`/dashboard/online-teaching/add-online-classes`], { queryParams: { type: 'ZOM' } });
  }
  private onAssignStudentHandleClickClasses(id: number) {
    this._router.navigateByUrl(`dashboard/online-teaching/regist-student-class/${id}`);
  }

  private onEditClassHandleClickClasses(id: number) {
    this._router.navigateByUrl(`dashboard/online-teaching/update-online-classes/${id}`);
  }

  private onDeleteClassHandleClickClasses(id: number) {
    let meetingRoomDelete = this.listOnlineClasses.find(x => x.id == id);
    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      width: '444px',
      data: { title: 'Xóa lớp học tương tác', message: 'Hành đồng này cũng sẽ gỡ ghi danh học sinh ra khỏi lớp học tương tác này.', name: meetingRoomDelete.name }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
        this.isFullScreen = true;
        this.spinner.show();
        this.onlineClassesEnpoint.deleteMeetingRoom(id)
          .then(res => {
            this._messageService.susccessCallback('Xóa thành công');
            //Load lại danh sách theo bộ lọc hiện tại
            this.loadDataClasses(true);
            this.spinner.hide();
          })
          .catch(() => {
            this.spinner.hide();
            this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
          });
      }
    })
  }

  public getLinkTemplate(cases: string) {
    if (cases === 'ZOM') {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-huong-dan-giao-vien-tao-lop-hoc-truc-tuyen-zoom-tren-he-thong-cls-k12.pdf`;
      // return `http://localhost:58910/learn/public/files/cdn/template-file/tao-lop-hoc-tuong-tac-tu-zoom.pdf`;
    }
    else if (cases === 'GMT') {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-huong-dan-giao-vien-tao-lop-hoc-truc-tuyen-google-meet-tren-he-thong-cls-k12.pdf`;
      // return `http://localhost:58910/learn/public/files/cdn/template-file/tao-lop-hoc-tuong-tac-tu-google-meet.pdf`;
    }
  }

  public onRollCallButtonHandleClick(element: any) {
    console.log(element);
    let divisionId  = element.divisionId ? element.divisionId : null
    let schoolId  = element.schoolId ? element.schoolId : null
    this._router.navigateByUrl(`dashboard/online-teaching/roll-call-student/${divisionId}/${schoolId}/${element.id}`)
  }
  //#endregion

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOnlineClassesComponent } from './list-online-classes.component';

describe('ListOnlineClassesComponent', () => {
  let component: ListOnlineClassesComponent;
  let fixture: ComponentFixture<ListOnlineClassesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListOnlineClassesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOnlineClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

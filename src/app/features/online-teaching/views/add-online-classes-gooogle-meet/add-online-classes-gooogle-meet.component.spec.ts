import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOnlineClassesGooogleMeetComponent } from './add-online-classes-gooogle-meet.component';

describe('AddOnlineClassesGooogleMeetComponent', () => {
  let component: AddOnlineClassesGooogleMeetComponent;
  let fixture: ComponentFixture<AddOnlineClassesGooogleMeetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddOnlineClassesGooogleMeetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOnlineClassesGooogleMeetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/errorResponse';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { ContentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentEndpoint';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { NgxSpinnerService } from 'ngx-spinner';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { InvalidDateTimeValidator } from 'src/app/shared/validations/learning-path-validation';
import { TeacherZoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/teacherZoomEndpoint';
import { STATUS_CODES } from 'http';
import { MatDialog } from '@angular/material/dialog';
import { VerifyAccountDialogComponent, VerifyAccountType } from 'src/app/shared/dialog/verify-account-dialog/verify-account-dialog.component';
import { Configuration } from 'src/app/shared/configurations';
import { VirtualClassRoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/virtual-classroom-endpoints';
import { OnlineClassesEndpoint } from 'sdk/cls-k12-sdk-js/src/services/meeting-room/endpoints/onlineClassesEndpoint';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
@Component({
  selector: 'app-add-online-classes-gooogle-meet',
  templateUrl: './add-online-classes-gooogle-meet.component.html',
  styleUrls: ['./add-online-classes-gooogle-meet.component.scss']
})


export class AddOnlineClassesGooogleMeetComponent implements OnInit {

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  onlineClassesEnpoint: OnlineClassesEndpoint;

  minDate: Date = new Date();
  public disableButtonSubmit = false;
  public isDisableSubmit: boolean;
  protected _onDestroy = new Subject<void>();
  public dateRangeInput: FormControl = new FormControl('', [Validators.required]);

  myreg = "^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$";

  createInteractive = this.classRoomForm.group({
    name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    startDate: new FormControl(new Date(), [Validators.required]),
    finishDate: new FormControl(new Date(), [Validators.required]),
    description: new FormControl(''),
    joinURL: new FormControl('', [Validators.required, Validators.pattern(this.myreg)]),
    startURL: new FormControl(''),
    meetingType: ''
  });

  get name() { return this.createInteractive.get('name'); }
  get startDate() { return this.createInteractive.get('startDate'); }
  get finishDate() { return this.createInteractive.get('finishDate'); }
  get description() { return this.createInteractive.get('description'); }
  get joinURL() { return this.createInteractive.get('joinURL'); }
  lessonId: number;
  lesson: Lesson;
  lessonName: string;
  isReady: boolean = false;
  typeClass: string = '';
  detailTypeClass: string = '';

  constructor(private router: ActivatedRoute,
    public dialog: MatDialog,
    private classRoomForm: FormBuilder,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    let defaultEndDate = new Date();
    defaultEndDate.setHours(defaultEndDate.getHours() + 1);

    //set value form group
    this.createInteractive.controls['finishDate'].setValue(defaultEndDate);

    this.onlineClassesEnpoint = new OnlineClassesEndpoint();

    this.router.queryParams
      .filter(params => params.type)
      .subscribe(params => {
        this.typeClass = params.type;
        if (this.typeClass === "GMT") {
          this.detailTypeClass = "Google Meet"
        }
        else if (this.typeClass === "ZOM") {
          this.detailTypeClass = "Zoom"
        }
        else {
          this._router.navigate([`/dashboard/online-teaching`]);
        }
      }
      );


  }

  ngOnInit(): void {

  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  getDisable() {
    return this.disableButtonSubmit;
  }
  ngAfterViewInit() {
  }


  onSubmit() {
    if (this.name.errors?.required) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Vui lòng nhập tên lớp",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (isNullOrEmpty(this.name.value) || isNullOrWhiteSpace(this.name.value)) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Tên lớp không hợp lệ",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (this.name.errors?.maxlength) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Tên lớp không quá 255 ký tự",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (this.getValidateStartDateErrors()) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Ngày bắt đầu không được lớn hơn ngày kết thúc (*)",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (this.getValidateStartDateRequiredErrors()) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Ngày bắt đầu không được trống (*)",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (this.getValidateEndDateErrors()) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Ngày kết thúc không được nhỏ hơn ngày bắt đầu(*)",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (this.getValidateEndDateRequiredErrors()) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Ngày kết thúc không được để trống (*)",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (isNullOrEmpty(this.joinURL.value) || isNullOrWhiteSpace(this.joinURL.value)) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Đường liên kết không hợp lệ",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (this.joinURL.errors?.required) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Vui lòng nhập đường liên kết",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (this.joinURL.errors?.pattern) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Đường liên kết không hợp lệ",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else {
      this.createInteractive.controls['startURL'].setValue(this.joinURL.value);
      this.createInteractive.controls['name'].setValue(this.name.value.trim());
      if (this.typeClass === "GMT") {
        this.createInteractive.controls['meetingType'].setValue('GMT');
      }
      else if (this.typeClass === "ZOM") {
        this.createInteractive.controls['meetingType'].setValue('ZOM');
      }
      this.onlineClassesEnpoint.CreateMeetingRoom(this.createInteractive.value).then(res => {
        if (res) {
          this._snackBar.openFromComponent(SuccessSnackBarComponent, {
            data: 'Thêm lớp học thành công!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this._router.navigateByUrl(`dashboard/online-teaching`);
        }
        else {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Thêm lớp học không thành công. Vui lòng thử lại!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      })
    }

  }

  onCancelButtonHandleClick() {
    this._router.navigateByUrl(`dashboard/online-teaching`);
  }

  public onDateLearningPathChange(value: any) {
    let startDateControl = this.startDate;
    let endDateControl = this.finishDate;
    this.validateDateControl(startDateControl, endDateControl)
  }
  public getValidateStartDateErrors(): boolean {
    if (this.startDate.errors?.invalidDateFromTo) {
      this.isDisableSubmit = true;
      return true;
    }

    this.isDisableSubmit = false;
    return false;
  }

  public getValidateEndDateErrors(): boolean {
    if (this.finishDate.errors?.invalidDateFromTo) {
      this.isDisableSubmit = true;
      return true;
    }

    this.isDisableSubmit = false;
    return false;
  }

  public getValidateStartDateRequiredErrors() {
    if (this.startDate.errors?.required) {
      this.isDisableSubmit = true;
      return true;
    }

    this.isDisableSubmit = false;
    return false;
  }

  public getValidateEndDateRequiredErrors() {
    if (this.finishDate.errors?.required) {
      this.isDisableSubmit = true;
      return true;
    }

    this.isDisableSubmit = false;
    return false;
  }
  private validateDateControl(startDateControl: AbstractControl, endDateControl: AbstractControl) {
    let startDate = new Date(startDateControl.value?.toString())
    let endDate = new Date(endDateControl.value?.toString())
    if (startDateControl.value && endDateControl.value) {
      if (startDate.getTime() > endDate.getTime()) {
        startDateControl
          .setValidators(
            [
              InvalidDateTimeValidator(new Date(endDateControl.value))
            ]
          )
        this.isDisableSubmit = true;
        startDateControl.updateValueAndValidity();
        startDateControl.markAsTouched();
        startDateControl.markAsDirty();
      } else {
        this.isDisableSubmit = false;
        startDateControl.setErrors(null);
        startDateControl.clearValidators();
        startDateControl.updateValueAndValidity();
      }
    } else {
      startDateControl.setErrors(null);
      startDateControl.clearValidators();
      if (!startDateControl.value) {
        startDateControl
          .setValidators([Validators.required])
        startDateControl.updateValueAndValidity();
        startDateControl.markAsTouched();
        startDateControl.markAsDirty();
        this.isDisableSubmit = true;
      }
      if (!endDateControl.value) {
        endDateControl
          .setValidators([Validators.required])
        endDateControl.updateValueAndValidity();
        endDateControl.markAsTouched();
        endDateControl.markAsDirty();
        this.isDisableSubmit = false;
      }
    }
  }

  onCopyLinkHandleClickClasses() {
    const create_copy = (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', this.joinURL.value);
      e.preventDefault();
    };

    document.addEventListener('copy', create_copy);
    document.execCommand('copy');
    document.removeEventListener('copy', create_copy);

    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
      data: "Đã sao chép liên kết",
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  keyDownFunction($event) {
    console.log(`$event`, $event)
    if ($event.key == "Enter") {
      return false;
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateOnlineClassesGooogleMeetComponent } from './update-online-classes-gooogle-meet.component';

describe('UpdateOnlineClassesGooogleMeetComponent', () => {
  let component: UpdateOnlineClassesGooogleMeetComponent;
  let fixture: ComponentFixture<UpdateOnlineClassesGooogleMeetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateOnlineClassesGooogleMeetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateOnlineClassesGooogleMeetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RollCallStudentComponent } from './roll-call-student.component';

describe('RollCallStudentComponent', () => {
  let component: RollCallStudentComponent;
  let fixture: ComponentFixture<RollCallStudentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RollCallStudentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RollCallStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

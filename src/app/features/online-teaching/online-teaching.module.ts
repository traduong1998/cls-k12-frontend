import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { DateFormat } from 'src/app/shared/helpers/date-format.helper';
import { SharedModule } from 'src/app/shared/shared.module';
import { OnlineTeachingRoutingModule } from './online-teaching-routing.module';
import { OnlineTeachingComponent } from './online-teaching.component';
import { RegistStudentClassComponent } from './views/regist-student-class/regist-student-class.component';
import { TeachingZoomComponent } from './views/teaching-zoom/teaching-zoom.component';
import { ListOnlineClassesComponent } from './views/list-online-classes/list-online-classes.component';
import { AddOnlineClassesGooogleMeetComponent } from './views/add-online-classes-gooogle-meet/add-online-classes-gooogle-meet.component';
import { UpdateOnlineClassesGooogleMeetComponent } from './views/update-online-classes-gooogle-meet/update-online-classes-gooogle-meet.component';
import { RollCallStudentComponent } from './views/roll-call-student/roll-call-student.component';
import { ConfirmTeacherJoinDialogComponent } from './components/confirm-teacher-join-dialog/confirm-teacher-join-dialog.component';



@NgModule({
  declarations: [
    OnlineTeachingComponent,
    TeachingZoomComponent,
    RegistStudentClassComponent,
    ListOnlineClassesComponent,
    AddOnlineClassesGooogleMeetComponent,
    UpdateOnlineClassesGooogleMeetComponent,
    RollCallStudentComponent,
    ConfirmTeacherJoinDialogComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    OnlineTeachingRoutingModule
  ],
  providers: [{ provide: DateAdapter, useClass: DateFormat }]
})
export class OnlineTeachingModule {
  constructor(private dateAdapter: DateAdapter<Date>) {
    dateAdapter.setLocale("vi-VN");
  }
}

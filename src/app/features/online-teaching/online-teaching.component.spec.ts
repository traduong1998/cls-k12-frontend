import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineTeachingComponent } from './online-teaching.component';

describe('OnlineTeachingComponent', () => {
  let component: OnlineTeachingComponent;
  let fixture: ComponentFixture<OnlineTeachingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnlineTeachingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineTeachingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

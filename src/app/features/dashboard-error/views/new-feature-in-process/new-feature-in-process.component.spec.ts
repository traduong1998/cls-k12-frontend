import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFeatureInProcessComponent } from './new-feature-in-process.component';

describe('NewFeatureInProcessComponent', () => {
  let component: NewFeatureInProcessComponent;
  let fixture: ComponentFixture<NewFeatureInProcessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewFeatureInProcessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFeatureInProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewFeatureInProcessComponent } from './views/new-feature-in-process/new-feature-in-process.component';

const routes: Routes = [
  {
    path: '',
    component: NewFeatureInProcessComponent
  },
  {
    path: 'feature-work-in-progress',
    component: NewFeatureInProcessComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardErrorRoutingModule { }

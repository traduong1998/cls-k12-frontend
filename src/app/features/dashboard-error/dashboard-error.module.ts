import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardErrorRoutingModule } from './dashboard-error-routing.module';
import { NewFeatureInProcessComponent } from './views/new-feature-in-process/new-feature-in-process.component';
import { DashboardErrorComponent } from './dashboard-error.component';


@NgModule({
  declarations: [NewFeatureInProcessComponent, DashboardErrorComponent],
  imports: [
    CommonModule,
    DashboardErrorRoutingModule
  ]
})
export class DashboardErrorModule { }

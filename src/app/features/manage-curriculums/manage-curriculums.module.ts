import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageCurriculumsRoutingModule } from './manage-curriculums-routing.module';
import { ManageCurriculumsComponent } from './manage-curriculums.component';
import { CurriculumsComponent } from './views/curriculums/curriculums.component';
import { CreateCurriculumsComponent } from './views/create-curriculums/create-curriculums.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { FormsModule } from '@angular/forms';
import { CreateCurriculumDialogComponent } from './components/create-curriculum-dialog/create-curriculum-dialog.component';
import { SettingCurriculumComponent } from './views/setting-curriculum/setting-curriculum.component';
import { CurriculumItemComponent } from './components/curriculum-item/curriculum-item.component';
import { AddCurriculumChildDialogComponent } from './components/add-curriculum-child-dialog/add-curriculum-child-dialog.component';
import { EditCurriculumDialogComponent } from './components/edit-curriculum-dialog/edit-curriculum-dialog.component';

import { DeleteCurriculumConfirmdialogComponent } from './components/delete-curriculum-confirmdialog/delete-curriculum-confirmdialog.component';


@NgModule({
  declarations: [ManageCurriculumsComponent, CurriculumsComponent, CreateCurriculumsComponent, CreateCurriculumDialogComponent,
    SettingCurriculumComponent, CurriculumItemComponent, AddCurriculumChildDialogComponent, EditCurriculumDialogComponent, DeleteCurriculumConfirmdialogComponent],
  imports: [
    SharedModule,
    ManageCurriculumsRoutingModule,
    NgxMatSelectSearchModule,
    FormsModule
  ]
})
export class ManageCurriculumsModule { }

import { Component, OnInit, Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  MatSnackBar, MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition
} from '@angular/material/snack-bar';
import { CurriculumDetailInfo, UserEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { CurriculumEndpoint } from 'sdk/cls-k12-sdk-js/src/services/curriculum/endpoint/curriculum-endpoint';
import { CurriculumDetail } from 'sdk/cls-k12-sdk-js/src/services/curriculum/responses/curriculum-detail-info';
import { User } from 'sdk/cls-k12-sdk-js/src/services/user/models/User';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'app-delete-curriculum-confirmdialog',
  templateUrl: './delete-curriculum-confirmdialog.component.html',
  styleUrls: ['./delete-curriculum-confirmdialog.component.scss']
})
export class DeleteCurriculumConfirmdialogComponent implements OnInit {

  curriculumEndpoint: CurriculumEndpoint;
  baseApiUrl = 'http://localhost:65000';
  curriculumDelete: CurriculumDetail = {
    curriculumName: "",
    gradeId: 0,
    subjectId: 0
  };
  txtXoa = '';
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  submiting: boolean = false;
  constructor(@Inject(MAT_DIALOG_DATA) public curriculum: CurriculumDetail, private _snackBar: MatSnackBar, private mdDialogRef: MatDialogRef<DeleteCurriculumConfirmdialogComponent>) {
    this.curriculumEndpoint = new CurriculumEndpoint({ baseUrl: this.baseApiUrl });
    this.curriculumDelete.id = this.curriculum.id;
    this.curriculumDelete.parentId = this.curriculum.parentId;
    this.curriculumDelete.curriculumName = this.curriculum.curriculumName;
  }

  ngOnInit(): void {

  }

  submitForm() {
    if (this.txtXoa == "DONGY") {
      this.submiting = true;
      this.curriculumEndpoint.deleteCurriculum(this.curriculumDelete.id)
        .then(res => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Xóa nội dung chương trình thành công!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            this.mdDialogRef.close('submit');
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Xóa nội dung chương trình không thành công. Vui lòng thử lại!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
          this.submiting = false;
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.submiting = false;
        });
    } else {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Mã xác nhận chưa chính xác. Vui lòng kiểm tra lại!',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    }
  }
}


import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteCurriculumConfirmdialogComponent } from './delete-curriculum-confirmdialog.component';

describe('DeleteCurriculumConfirmdialogComponent', () => {
  let component: DeleteCurriculumConfirmdialogComponent;
  let fixture: ComponentFixture<DeleteCurriculumConfirmdialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteCurriculumConfirmdialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteCurriculumConfirmdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

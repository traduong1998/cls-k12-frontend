import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { CreateCurriculumRequest, GradeEndpoint, GroupStudentEndpoint, SubjectEndpoint, UserEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { take, takeUntil } from 'rxjs/operators';
import { CurriculumEndpoint } from 'sdk/cls-k12-sdk-js/src/services/curriculum/endpoint/curriculum-endpoint';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { CurriculumDetail } from 'sdk/cls-k12-sdk-js/src/services/curriculum/responses/curriculum-detail-info';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';


@Component({
  selector: 'app-add-curriculum-child-dialog',
  templateUrl: './add-curriculum-child-dialog.component.html',
  styleUrls: ['./add-curriculum-child-dialog.component.scss']
})
export class AddCurriculumChildDialogComponent implements OnInit {

  baseApiUrl = 'http://localhost:65000';

  endpointCurriculum: CurriculumEndpoint;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  formCreateCurriculum: FormGroup;

  createCurriculumRequest: CurriculumDetail = {
    curriculumName: "",
    gradeId: 0,
    subjectId: 0,
    parentId: null,
    isActived: true
  };
  parentNode: CurriculumDetail = {
    curriculumName: "",
    gradeId: 0,
    subjectId: 0,
    parentId: null,
    isActived: true
  };
  parentName: FormControl;
  submiting: boolean = false;
  constructor(@Inject(MAT_DIALOG_DATA) public data, private _snackBar: MatSnackBar, private mdDialogRef: MatDialogRef<AddCurriculumChildDialogComponent>) {
    this.endpointCurriculum = new CurriculumEndpoint({ baseUrl: this.baseApiUrl });

    this.parentNode = data.parent;

    this.createCurriculumRequest.parentId = this.parentNode.id;
    this.createCurriculumRequest.gradeId = this.parentNode.gradeId;
    this.createCurriculumRequest.subjectId = this.parentNode.subjectId;

    this.formCreateCurriculum = new FormGroup({
      curriculumName: new FormControl('', [Validators.required]),
    });
    this.parentName = new FormControl(this.parentNode.curriculumName);
    this.parentName.disable();
  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {

  }

  submitForm() {
    if (this.formCreateCurriculum.valid) {
      this.createCurriculumRequest.curriculumName = this.formCreateCurriculum.get("curriculumName").value;
      this.submiting = true;
      this.endpointCurriculum.createCurriculum(this.createCurriculumRequest)
        .then(res => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Tạo thành công!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            this.mdDialogRef.close('submit');
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Tạo không thành công',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            // this._snackBar.open("Tạo nội dung chương trình không thành công. Vui lòng thử lại", 'Ok', {
            //   duration: 5000,
            //   panelClass: ['red-snackbar'],
            //   horizontalPosition: this.horizontalPosition,
            //   verticalPosition: this.verticalPosition,
            // });
          }
          this.submiting = false;
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.submiting = false;
        });
    }
  }
  get f() {
    return this.formCreateCurriculum;
  }
}




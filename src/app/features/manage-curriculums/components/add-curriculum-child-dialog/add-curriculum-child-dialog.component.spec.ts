import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCurriculumChildDialogComponent } from './add-curriculum-child-dialog.component';

describe('AddCurriculumChildDialogComponent', () => {
  let component: AddCurriculumChildDialogComponent;
  let fixture: ComponentFixture<AddCurriculumChildDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCurriculumChildDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCurriculumChildDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

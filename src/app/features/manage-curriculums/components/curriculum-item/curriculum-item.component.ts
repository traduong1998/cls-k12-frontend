import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { CurriculumDetailInfo } from 'cls-k12-sdk-js/src';
import { CurriculumEndpoint } from 'cls-k12-sdk-js/src/services/curriculum/endpoint/curriculum-endpoint';
import { CurriculumDetail } from 'cls-k12-sdk-js/src/services/curriculum/responses/curriculum-detail-info';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { StringLiteralType } from 'typescript';
import { TitleDialog } from '../../views/setting-curriculum/view-models/title-dialog';
import { AddCurriculumChildDialogComponent } from '../add-curriculum-child-dialog/add-curriculum-child-dialog.component';
import { DeleteCurriculumConfirmdialogComponent } from '../delete-curriculum-confirmdialog/delete-curriculum-confirmdialog.component';
import { EditCurriculumDialogComponent } from '../edit-curriculum-dialog/edit-curriculum-dialog.component';

@Component({
  selector: 'app-curriculum-item',
  templateUrl: './curriculum-item.component.html',
  styleUrls: ['./curriculum-item.component.scss']
})

export class CurriculumItemComponent implements OnInit {
  @Input() node: CurriculumDetailInfo;
  @Input() allowedAddChild: boolean;
  @Output() onReloadCurriculumChildNodes = new EventEmitter<number>();

  baseApiUrl = 'http://localhost:65000';

  endpointCurriculum: CurriculumEndpoint;
  rootCurriculumNodeId: number;
  childrentNodes: CurriculumDetailInfo[];
  showChilds: boolean = false;
  loadedData: boolean = false;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  titleAdd: string;
  titleUpdate: string;
  titleDelete: string;


  constructor(public dialog: MatDialog,
    private _snackBar: MatSnackBar) {
    this.endpointCurriculum = new CurriculumEndpoint({ baseUrl: this.baseApiUrl });
  }

  ngOnInit(): void {
    if (this.allowedAddChild) {
      this.titleAdd = 'Thêm bài học'
      this.titleUpdate = "Sửa chương học"
      this.titleDelete = "Xóa chương học"
    } else {
      this.titleAdd = ''
      this.titleUpdate = "Sửa bài học"
      this.titleDelete = "Xóa bài học"
    }
  }

  onLoadChildrentClicked(id: number) {
    this.showChilds = !this.showChilds;
    if (!this.loadedData) {
      this.getCurriculumChildrentNodes(id);
      this.loadedData = true;
    }
  }

  failureCallback() {
    alert("hihi 400");
  }

  getCurriculumChildrentNodes(id: number) {
    this.endpointCurriculum.getCurriculumChildrentNodes(id)
      .then(res => {
        console.log("cap2: ", res);
        this.childrentNodes = res;
      })
      .catch(this.failureCallback);
  }

  onOpenCreateCurriculumChildDialogClicked(parent: CurriculumDetail) {
    let titleDialog: TitleDialog = {
      commonTitle: 'Thêm bài học',
      nameTitle: 'Tên bài học',
      contentTitle: 'Chương học'
    }
    const dialogRef = this.dialog.open(AddCurriculumChildDialogComponent, {
      disableClose: true,
      width: '450px',
      height: 'auto',
      data: { parent: parent, titleDialog: titleDialog }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("result ", result);
      if (result !== undefined) {
        if (result === 'submit') {
          this.getCurriculumChildrentNodes(parent.id);
        }
      }
    });
  }

  onOpenEditCurriculumChildDialogClicked(editCurriculum: CurriculumDetail) {
    console.log(editCurriculum);
    let titleDialog: TitleDialog = {
      commonTitle: this.allowedAddChild ? 'Sửa tên chương học' : 'Sửa tên bài học',
      nameTitle: this.allowedAddChild ? 'Tên chương học' : 'Tên bài',
      contentTitle: ''
    }
    const dialogRef = this.dialog.open(EditCurriculumDialogComponent, {
      disableClose: true,
      width: '450px',
      height: 'auto',
      data: { editCurriculum: editCurriculum, titleDialog: titleDialog }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("result ", result);
      if (result !== undefined) {
        if (result === 'submit') {
          //this.getCurriculumDetailInfo();
        }
      }
    });
  }

  onOpenDeleteCurriculumChildDialogClicked(deleteCurriculum: CurriculumDetail) {
    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      disableClose: true,
      width: '396px',
      height: 'auto',
      data: {
        title: this.allowedAddChild ? 'Xóa chương học' : 'Xóa bài học', // xóa cái gì
        name: deleteCurriculum.curriculumName,// tên đối tượng cần xóa
        message: '' //nội dung message chi tiết
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == undefined) {
        //todo
      }
      else if (result == 'DONGY') {
        // gọi api xóa
        this.endpointCurriculum.deleteCurriculum(deleteCurriculum.id)
          .then(res => {
            if (res) {
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: 'Xóa thành công!',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
              this.onReloadCurriculumChildNodes.emit(deleteCurriculum.parentId);
            } else {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Xóa không thành công. Vui lòng thử lại!',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            }
          })
          .catch((err: ErrorResponse) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: err.errorDetail,
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
      }
      else {
        // nhâp sai
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Mã xác nhận chưa chính xác. Vui lòng kiểm tra lại!',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    });
    // const dialogRef = this.dialog.open(DeleteCurriculumConfirmdialogComponent, {
    //   disableClose: true,
    //   width: '650px',
    //   height: 'auto',
    //   data: deleteCurriculum
    // });

    dialogRef.afterClosed().subscribe(result => {
      console.log("result ", result);
      if (result !== undefined) {
        if (result === 'submit') {
          console.log(`deleteCurriculum `, deleteCurriculum);
          if (deleteCurriculum.parentId != null) {
            this.onReloadCurriculumChildNodes.emit(deleteCurriculum.parentId);
          }
        }
      }
    });
  }
}

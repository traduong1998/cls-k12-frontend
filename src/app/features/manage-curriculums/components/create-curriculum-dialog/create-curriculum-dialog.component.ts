import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';
import { MatDialogRef } from '@angular/material/dialog';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { CreateCurriculumRequest, GradeEndpoint, GroupStudentEndpoint, SubjectEndpoint, UserEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { take, takeUntil } from 'rxjs/operators';
import { CurriculumEndpoint } from 'sdk/cls-k12-sdk-js/src/services/curriculum/endpoint/curriculum-endpoint';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { AuthService } from 'src/app/core/services';

@Component({
  selector: 'app-create-curriculum-dialog',
  templateUrl: './create-curriculum-dialog.component.html',
  styleUrls: ['./create-curriculum-dialog.component.scss']
})
export class CreateCurriculumDialogComponent implements OnInit {

  baseApiUrl = 'http://localhost:65000';

  endpointCurriculum: CurriculumEndpoint;
  endpointGradeEndpoint: GradeEndpoint;
  endpointSubjectEndpoint: SubjectEndpoint;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  formCreateCurriculum: FormGroup;
  defaultStatus = true;
  userIdentity: UserIdentity;

  constructor(private _snackBar: MatSnackBar, private mdDialogRef: MatDialogRef<CreateCurriculumDialogComponent>, private _authService: AuthService) {
    this.endpointCurriculum = new CurriculumEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGradeEndpoint = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubjectEndpoint = new SubjectEndpoint({ baseUrl: this.baseApiUrl });

    this.formCreateCurriculum = new FormGroup({
      curriculumName: new FormControl('', [Validators.required]),
    });
    this.userIdentity = _authService.getTokenInfo();
  }

  // get curriculumName() { return this.formCreateCurriculum.get('curriculumName').value }
  isFirstLoadGrades = true;
  isFirstLoadSubjects = true;

  subjectFilter = {
    gradeId: null
  }

  createCurriculumRequest: CreateCurriculumRequest = {
    curriculumName: "",
    gradeId: 0,
    subjectId: 0,
    parentId: null,
    isActive: true
  };
  public statusCtrl: FormControl = new FormControl('1');

  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl(null, Validators.required);
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected subjects: SubjectOption[];
  public subjectCtrl: FormControl = new FormControl(null, Validators.required);
  public subjectFilterCtrl: FormControl = new FormControl();
  public filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  @ViewChild('singleSelectSubject', { static: true }) singleSelectSubject: MatSelect;

  protected _onDestroy = new Subject<void>();
  submiting: boolean = false;

  ngOnInit(): void {
    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    // listen for search field value changes
    this.subjectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSubjects();
      });
  }
  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  protected setInitialValue() {
    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        if (this.singleSelectGrade != undefined) {
          this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
        }
      });

    this.filteredSubjects
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        if (this.singleSelectSubject != undefined) {
          this.singleSelectSubject.compareWith = (a: SubjectOption, b: SubjectOption) => a && b && a.id === b.id;
        }
      });
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterSubjects() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.subjectFilterCtrl.value;
    if (!search) {
      this.filteredSubjects.next(this.subjects.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSubjects.next(
      this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onGradeSelectClicked() {
    if (this.isFirstLoadGrades) {
      this.endpointGradeEndpoint.getGradeOptions(this.userIdentity.schoolId)
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onSubjectSelectClicked() {
    let gradeIdSelected = this.gradeCtrl.value;
    if (this.isFirstLoadSubjects || this.subjectFilter.gradeId != gradeIdSelected) {
      this.endpointSubjectEndpoint.getSubjectOptions(gradeIdSelected)
        .then(res => {
          this.isFirstLoadSubjects = false;
          this.subjects = res;
          this.subjectFilter.gradeId == gradeIdSelected;
          this.filteredSubjects.next(this.subjects.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onChangeGradeSelected(id: any) {
    this.resetSubjectSelecteCtrl();
  }

  resetSubjectSelecteCtrl() {
    this.subjects = [];
    this.isFirstLoadSubjects = true;
    this.subjectCtrl.setValue(undefined);
  }

  submitForm() {
    this.gradeCtrl.markAsTouched();
    this.subjectCtrl.markAsTouched();

    if (this.gradeCtrl.valid && this.subjectCtrl.valid && this.formCreateCurriculum.valid) {
      this.createCurriculumRequest.curriculumName = this.formCreateCurriculum.get("curriculumName").value;
      this.createCurriculumRequest.gradeId = this.gradeCtrl.value;
      this.createCurriculumRequest.subjectId = this.subjectCtrl.value;

      this.submiting = true;
      this.endpointCurriculum.createCurriculum(this.createCurriculumRequest)
        .then(res => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Tạo khung chương trình thành công!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            this.mdDialogRef.close('submit');
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Tạo khung chương trình không thành công. Vui lòng thử lại!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
          this.submiting = false;
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.open(err.errorDetail, 'Ok', {
            duration: 5000,
            panelClass: ['red-snackbar'],
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.submiting = false;
        });
    }
  }

  changeSelectStatus(event) {
    this.createCurriculumRequest.isActive = event.value;
  }

  get f() {
    return this.formCreateCurriculum;
  }
}




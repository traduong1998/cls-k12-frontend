import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCurriculumDialogComponent } from './create-curriculum-dialog.component';

describe('CreateCurriculumDialogComponent', () => {
  let component: CreateCurriculumDialogComponent;
  let fixture: ComponentFixture<CreateCurriculumDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateCurriculumDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCurriculumDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

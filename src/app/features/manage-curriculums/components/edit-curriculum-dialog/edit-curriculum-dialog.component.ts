import { Component, Inject, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { EditCurriculumRequest } from 'sdk/cls-k12-sdk-js/src';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { CurriculumEndpoint } from 'sdk/cls-k12-sdk-js/src/services/curriculum/endpoint/curriculum-endpoint';

import { CurriculumDetail } from 'sdk/cls-k12-sdk-js/src/services/curriculum/responses/curriculum-detail-info';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';


@Component({
  selector: 'app-edit-curriculum-dialog',
  templateUrl: './edit-curriculum-dialog.component.html',
  styleUrls: ['./edit-curriculum-dialog.component.scss']
})
export class EditCurriculumDialogComponent implements OnInit {

  baseApiUrl = 'http://localhost:65000';

  endpointCurriculum: CurriculumEndpoint;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  formEditCurriculum: FormGroup;

  editCurriculum: CurriculumDetail = {
    curriculumName: "",
    gradeId: 0,
    subjectId: 0,
    parentId: null
  };
  editCurriculumRequest: EditCurriculumRequest = {
    curriculumId: 0,
    parentId: null,
    curriculumName: "",
    isActive: true
  };
  parentName: FormControl;
  submiting: boolean = false;


  public _currentCurriculum: CurriculumDetail;

  constructor(@Inject(MAT_DIALOG_DATA) public data, private _snackBar: MatSnackBar, private mdDialogRef: MatDialogRef<EditCurriculumDialogComponent>) {
    this.endpointCurriculum = new CurriculumEndpoint({ baseUrl: this.baseApiUrl });


    this._currentCurriculum = data.editCurriculum;

    console.log(`CurriculumDetail `, this._currentCurriculum);
    this.editCurriculum.parentId = this._currentCurriculum.id;
    this.editCurriculum.gradeId = this._currentCurriculum.gradeId;
    this.editCurriculum.subjectId = this._currentCurriculum.subjectId;
    this.editCurriculum.curriculumName = this._currentCurriculum.curriculumName;

    this.editCurriculumRequest.parentId = this._currentCurriculum.parentId;
    this.editCurriculumRequest.curriculumId = this._currentCurriculum.id;
    this.editCurriculumRequest.curriculumName = this._currentCurriculum.curriculumName;

    this.formEditCurriculum = new FormGroup({
      curriculumName: new FormControl(this.editCurriculum.curriculumName, [Validators.required]),
    });
  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {

  }

  submitForm() {
    if (this.formEditCurriculum.valid) {
      this.editCurriculumRequest.curriculumName = this.formEditCurriculum.get("curriculumName").value;
      this.endpointCurriculum.updateCurriculum(this.editCurriculumRequest)
        .then(res => {
          if (res) {
            this._currentCurriculum.curriculumName = this.editCurriculumRequest.curriculumName;
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Cập nhật thành công',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            this.mdDialogRef.close('submit')
          } else {


            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Cập nhật không thành công',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
          this.submiting = false;
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.submiting = false;
        });
    }
  }

  changeSelectStatus(event){
    this.editCurriculumRequest.isActive = event.value;
  }
  get f() {
    return this.formEditCurriculum;
  }
}




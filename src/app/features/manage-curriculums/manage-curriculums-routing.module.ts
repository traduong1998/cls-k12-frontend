import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards';
import { ManageCurriculumsComponent } from './manage-curriculums.component';
import { CreateCurriculumsComponent } from './views/create-curriculums/create-curriculums.component';
import { CurriculumsComponent } from './views/curriculums/curriculums.component';
import { SettingCurriculumComponent } from './views/setting-curriculum/setting-curriculum.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: ManageCurriculumsComponent,
    children: [
      {
        path: '',
        component: CurriculumsComponent,
        data: {
          title: 'Quản lý khung chương trình',
        }
      },
      {
        path: 'create',
        component: CreateCurriculumsComponent,
        data: {
          title: 'Thêm khung chương trình',
        }
      },
      {
        path: 'setting-curriculum',
        component: SettingCurriculumComponent,
        data: {
          title: 'Cài đặt khung chương trình',
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageCurriculumsRoutingModule { }

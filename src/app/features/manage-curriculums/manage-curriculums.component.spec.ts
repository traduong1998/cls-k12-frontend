import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCurriculumsComponent } from './manage-curriculums.component';

describe('ManageCurriculumsComponent', () => {
  let component: ManageCurriculumsComponent;
  let fixture: ComponentFixture<ManageCurriculumsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageCurriculumsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCurriculumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

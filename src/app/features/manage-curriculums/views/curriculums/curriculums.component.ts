import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { CLSModules, CLSPermissions, DivisionEndpoint, DivisionOption, LevelManages, SchoolEndpoint, SubjectEndpoint, UserIdentity } from 'cls-k12-sdk-js/src';

import { GradeEndpoint } from 'sdk/cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';

import { FormBuilder, FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { ReplaySubject, Subject } from 'rxjs';
import { pairwise, startWith, take, takeUntil } from 'rxjs/operators';
import { AuthService } from 'src/app/core/services';

import { MatDialog } from '@angular/material/dialog';

import { CurriculumsFilterRequests } from 'sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum-filter-request';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { Curriculum } from 'sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum';
import { CurriculumEndpoint } from 'sdk/cls-k12-sdk-js/src/services/curriculum/endpoint/curriculum-endpoint';
import { CreateCurriculumDialogComponent } from '../../components/create-curriculum-dialog/create-curriculum-dialog.component';
import { CurriculumDetail } from 'sdk/cls-k12-sdk-js/src/services/curriculum/responses/curriculum-detail-info';
import { DeleteCurriculumConfirmdialogComponent } from '../../components/delete-curriculum-confirmdialog/delete-curriculum-confirmdialog.component';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { SubjectsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/subjectsEndpoint';

// import { DeleteUserConfirmdialogComponent } from '../../components/delete-user-confirmdialog/delete-user-confirmdialog.component';

const curriculumsPaginationDefault: PaginatorResponse<Curriculum> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-curriculums',
  templateUrl: './curriculums.component.html',
  styleUrls: ['./curriculums.component.scss']
})
export class CurriculumsComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';


  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointSubject: SubjectsEndpoint

  endpointCurriculum: CurriculumEndpoint;
  userIdentity: UserIdentity;
  levelManageValue: number = 0;
  hasAddPermission: boolean;
  hasDeletePermission: boolean;
  hasEditPermission: boolean;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  curriculumFilterClone: any;


  /* Division */
  protected divisions: DivisionOption[];
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  /* School */
  protected schools: SchoolOption[];
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  /* School */
  protected grades: GradeOption[];
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  /* Subject */
  protected subjects: SubjectOption[];
  public subjectFilterCtrl: FormControl = new FormControl();
  public filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  @ViewChild('singleSelectSubject', { static: true }) singleSelectSubject: MatSelect;

  protected _onDestroy = new Subject<void>();

  get divisionId() { return this.filterCurriculumForm.get('divisionId'); }
  isEnableDivision = true;
  isFirstLoadDivision = true;

  get schoolId() { return this.filterCurriculumForm.get('schoolId'); }
  isDisableSchoolId = false;
  isFirstLoadSchool = true;

  get gradeId() { return this.filterCurriculumForm.get('gradeId'); }
  isDisableGradeId = false;
  isFirstLoadGrade = true;

  get subjectId() { return this.filterCurriculumForm.get('subjectId'); }
  isDisableSubjectId = true;
  isFirstLoadSubject = true;

  get isActive() { return this.filterCurriculumForm.get('isActive'); }

  displayedColumns: string[] = ['stt', "curriculumName", "levelManageName", "subjectName", "gradeName", "function"];
  curriculumsPagination: PaginatorResponse<Curriculum> = curriculumsPaginationDefault;
  filterCurriculumForm = this.fb.group({
    divisionId: new FormControl(''),
    schoolId: new FormControl(''),
    gradeId: new FormControl(''),
    subjectId: new FormControl(''),
    isActive: new FormControl(true),
    page: 0,
    size: 10,
    getCount: true,
    sortDirection: 'DESC',
    sortField: 'CRE',
  });

  selection = new SelectionModel<Curriculum>(true, []);
  selectedCurriculumIds = [];
  isLoadingResults = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private _authService: AuthService, public dialog: MatDialog, private _snackBar: MatSnackBar, private fb: FormBuilder) {
    this.endpointCurriculum = new CurriculumEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubject = new SubjectsEndpoint({ baseUrl: this.baseApiUrl })

    this.userIdentity = _authService.getTokenInfo();

    console.log("Tài khoản cấp: ", this.userIdentity.levelManage);
    console.log("Vai trò: ", this.userIdentity.userTypeRole);
    //console.log(this.userIdentity);

    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.Curriculum, CLSPermissions.Add);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.Curriculum, CLSPermissions.Edit);
    this.hasDeletePermission = this.userIdentity.hasPermission(CLSModules.Curriculum, CLSPermissions.Delete);

    if (this.userIdentity.levelManage == LevelManages.Department) {
      this.levelManageValue = 1;
      //this.flexValue = 25;
    } else if (this.userIdentity.levelManage == LevelManages.Division) {
      this.levelManageValue = 2;
      //this.flexValue = 50;
      // Tài khoản cấp Phòng
      this.filterCurriculumForm.controls['divisionId'].setValue(this.userIdentity.divisionId);

      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else {
      this.levelManageValue = 3;
      //this.flexValue = -25;
      // if (this.isShowteacherCombobox) {
      //   this.flexValue = 50;
      // }
      // Tài khoản cấp Trường, schoolid fillter mặc định theo  id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
      this.filterCurriculumForm.controls['schoolId'].setValue(this.userIdentity.schoolId);
      // this.usersPaginationFilter.schoolId = this.userIdentity.schoolId;
    }

  }

  isShowFilter = true;

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  isFirstLoadGrades = true;
  isFirstLoadSubjects = true;

  subjectFilter = {
    gradeId: null
  }



  ngOnInit(): void {
    this.getCurriculumsData();
    this.getFilterParam()
    this.curriculumFilterClone = { ...this.filterCurriculumForm.value };

    this.divisionId.valueChanges
      .pipe(startWith(this.divisionId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            // reset change
            this.onResetSchoolSelect();
            this.onResetGradeSelect();
            this.onResetSubjectSelect();

            this.isDisableSubjectId = true;
          }
        }
      )

    this.schoolId.valueChanges
      .pipe(startWith(this.schoolId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetGradeSelect();
            this.onResetSubjectSelect();
            this.isDisableSubjectId = true;
          }
        }
      )

    this.gradeId.valueChanges
      .pipe(startWith(this.gradeId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetSubjectSelect();
            if (value < 1) {
              this.isDisableSubjectId = true;
            } else {

              this.isDisableSubjectId = false;
            }
          }
        }
      )

    this.subjectId.valueChanges
      .pipe(startWith(this.subjectId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
        });

    this.isActive.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe((value) => {
      this.filterCurriculumForm.setValue(['isActive'], value);
    })

    // listen for search field value changes 
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        console.log(this.divisionId.value);
        this.filterDivisions();
      });


    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    this.subjectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSubjects();
      });

  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.filterCurriculumForm.value.pageNumber = this.paginator.pageIndex + 1;
      this.filterCurriculumForm.value.sizeNumber = this.paginator.pageSize;
      this.getCurriculumsData();
    });
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }

  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectDivision && (this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id);
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool && (this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id);
      });

    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGrade && (this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id);
      });

    this.filteredSubjects
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSubject && (this.singleSelectSubject.compareWith = (a: SubjectOption, b: SubjectOption) => a && b && a.id === b.id);
      });
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSubjects() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.subjectFilterCtrl.value;
    if (!search) {
      this.filteredSubjects.next(this.subjects.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSubjects.next(
      this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }

  /* Get division */
  onDivisionSelectClicked() {
    if (this.isFirstLoadDivision) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivision = false;
          this.divisions = res;
          // if (this.divisions && !this.divisions.find(x => x.id == 0)) {
          //   this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          // }
          console.log('dvsid', this.divisions);
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {

    }
  }

  /* Get school */
  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionId.value;
    if (this.userIdentity.levelManage == LevelManages.Division) {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchool) {
      this.endpointSchool.getSchoolOptions(this.divisionId.value)
        .then(res => {
          this.isFirstLoadSchool = false;
          this.schools = res;
          this.schoolFilter.divisionId = divisionIdSelected;

          // if (this.schools && !this.schools.find(x => x.id == null)) {
          //   this.schools.unshift({ id: null, name: 'Chọn trường' })
          // }
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(

        )
    } else {

    }
  }

  /* Get Grade */
  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolId.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrade || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrade = false;
          this.grades = res;

          // if (this.grades && !this.grades.find(x => x.id == null)) {
          //   this.grades.unshift({ id: null, name: 'Chọn khối' });
          // }
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());

        })
        .catch(err => { console.log(err) }
        )
    } else {

    }
  }

  /* Get Subject */
  onSubjectSelectClicked() { // TODO : system service was not have this API
    if (this.isDisableSubjectId == true || this.gradeId.value == null || !this.isFirstLoadSubject) {
      return;
    } else {
      this.endpointSubject.getSubjectByGradeIdOptions(this.gradeId.value).then(res => {
        this.subjects = res;
        this.isFirstLoadSubject = false;
        // if (this.subjects && !this.subjects.find(x => x.id == 0)) {
        //   this.subjects.unshift({ id: null, name: 'Chọn môn' });
        // }
        this.filteredSubjects.next(this.subjects.slice());
      }).catch(err => {
        // sthis.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
    }
  }

  /* reset change School */
  private onResetSchoolSelect() {
    this.filterCurriculumForm.controls['schoolId'].setValue(undefined);
    this.isFirstLoadSchool = true;
  }

  /* reset change Grade */
  private onResetGradeSelect() {
    this.filterCurriculumForm.controls['gradeId'].setValue(undefined);
    this.isFirstLoadGrade = true;
  }

  /* reset change Subject */
  private onResetSubjectSelect() {
    this.filterCurriculumForm.controls['subjectId'].setValue(undefined);
    this.isFirstLoadSubject = true;
  }

  failureCallback() {
    this.isLoadingResults = false;
    alert("hihi 400");
  }

  getCurriculumsData() {
    this.isLoadingResults = true;
    this.endpointCurriculum.getCurriculumsPaging(this.filterCurriculumForm.value)
      .then(res => {
        this.isLoadingResults = false;
        this.curriculumsPagination = res;
        this.curriculumFilterClone = { ...this.filterCurriculumForm.value }
        console.log(res);
      })
      .catch(this.failureCallback);
  }

  changeCheck(event, row) {
    if (event.checked) {
      let index = this.selectedCurriculumIds.indexOf(row.curriculumId);
      if (index == -1) {
        this.selectedCurriculumIds.push(row.curriculumId);
      }
    } else {
      let index = this.selectedCurriculumIds.indexOf(row.curriculumId);
      this.selectedCurriculumIds = this.selectedCurriculumIds.splice(index, 1);
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.curriculumsPagination.items.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.selectedCurriculumIds = [];
    } else {
      this.curriculumsPagination.items.forEach(row => {
        this.selection.select(row);
        this.selectedCurriculumIds.push(row.curriculumId);
      })
    }
  }

  checkboxLabel(row?: Curriculum): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.curriculumId}`;
  }

  getFilterParam() {
    if (this.userIdentity.levelManage == LevelManages.Division) {
      this.filterCurriculumForm.controls['divisionId'].setValue(this.userIdentity.divisionId);
    } else if (this.userIdentity.levelManage == LevelManages.School) {
      this.filterCurriculumForm.controls['divisionId'].setValue(this.userIdentity.divisionId);
      this.filterCurriculumForm.controls['schoolId'].setValue(this.userIdentity.schoolId);
    }
  }

  onSubmitFilter() {
    this.paginator.pageIndex = 0;
    this.getFilterParam();
    if (!compareTwoObject(this.curriculumFilterClone, this.filterCurriculumForm.value)) {
      this.getCurriculumsData();
    }
  }
  onOpenCreateCurriculumDialogClicked() {
    const dialogRef = this.dialog.open(CreateCurriculumDialogComponent, {
      width: '450px',
      height: 'auto',
      disableClose: true,
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        console.log(`result `, result);
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getCurriculumsData();
        }
      }
    });
  }

  onOpenDeleteCurriculumDialogClicked(curriculum: Curriculum) {
    let deleteCurriculum: CurriculumDetail = {
      id: curriculum.curriculumId,
      curriculumName: curriculum.curriculumName,
      gradeId: 0,
      subjectId: 0
    }


    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      disableClose: true,
      width: '396px',
      height: 'auto',
      data: {
        title: 'Xóa khung chương trình', // xóa cái gì
        name: deleteCurriculum.curriculumName,// tên đối tượng cần xóa
        message: '' //nội dung message chi tiết
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == undefined) {
        //todo
      }
      else if (result == 'DONGY') {
        // gọi api xóa
        this.endpointCurriculum.deleteCurriculum(deleteCurriculum.id)
          .then(res => {
            if (res) {
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: 'Xóa khung chương trình thành công!',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
              this.getCurriculumsData();
            } else {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Xóa khung chương trình không thành công. Vui lòng thử lại!',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            }
          })
          .catch((err: ErrorResponse) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: err.errorDetail,
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
      }
      else {
        // nhâp sai
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Mã xác nhận chưa chính xác. Vui lòng kiểm tra lại!',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    });
  }

}


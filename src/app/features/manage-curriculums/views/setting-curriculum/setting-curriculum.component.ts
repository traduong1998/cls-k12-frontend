import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { CurriculumDetailInfo, SubjectEndpoint, UserIdentity } from 'cls-k12-sdk-js/src';

import { GradeEndpoint } from 'sdk/cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';

import { AuthService } from 'src/app/core/services';

import { MatDialog } from '@angular/material/dialog';

import { Curriculum } from 'sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum';
import { CurriculumEndpoint } from 'sdk/cls-k12-sdk-js/src/services/curriculum/endpoint/curriculum-endpoint';
import { ActivatedRoute, Router } from '@angular/router';
import { CurriculumDetail } from 'sdk/cls-k12-sdk-js/src/services/curriculum/responses/curriculum-detail-info';
import { AddCurriculumChildDialogComponent } from '../../components/add-curriculum-child-dialog/add-curriculum-child-dialog.component';
import { EditCurriculumDialogComponent } from '../../components/edit-curriculum-dialog/edit-curriculum-dialog.component';
import { TitleDialog } from './view-models/title-dialog';

// import { DeleteUserConfirmdialogComponent } from '../../components/delete-user-confirmdialog/delete-user-confirmdialog.component';

const curriculumsPaginationDefault: PaginatorResponse<Curriculum> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-setting-curriculum',
  templateUrl: './setting-curriculum.component.html',
  styleUrls: ['./setting-curriculum.component.scss']
})
export class SettingCurriculumComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';

  endpointCurriculum: CurriculumEndpoint;
  endpointGradeEndpoint: GradeEndpoint;
  endpointSubjectEndpoint: SubjectEndpoint;

  userIdentity: UserIdentity;
  levelManageValue: number = 0;
  rootCurriculumNodeId: number;
  rootCurriculumDetailInfo: CurriculumDetail = {
    curriculumName: '',
    gradeId: 0,
    subjectId: 0
  }
  childrentNodes: CurriculumDetailInfo[];

  constructor(private _authService: AuthService, public dialog: MatDialog, private router: ActivatedRoute, private route: Router) {
    this.endpointCurriculum = new CurriculumEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGradeEndpoint = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubjectEndpoint = new SubjectEndpoint({ baseUrl: this.baseApiUrl });

    this.userIdentity = _authService.getTokenInfo();

    console.log("Tài khoản cấp: ", this.userIdentity.levelManage);
    console.log("Vai trò: ", this.userIdentity.userTypeRole);
    //console.log(this.userIdentity);
  }


  isLoadingResults = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit(): void {
    this.router.queryParams
      .subscribe(params => {
        this.rootCurriculumNodeId = params.id;
      });

    this.getCurriculumDetailInfo();
    this.getCurriculumChildrentNodes();
  }

  ngAfterViewInit() {

  }

  failureCallback() {
    this.isLoadingResults = false;
    alert("hihi 400");
  }

  getCurriculumDetailInfo() {
    this.endpointCurriculum.getCurriculumDetailInfo(this.rootCurriculumNodeId)
      .then(res => {
        this.rootCurriculumDetailInfo = res;
      })
      .catch(this.failureCallback);
  }

  getCurriculumChildrentNodes() {
    this.endpointCurriculum.getCurriculumChildrentNodes(this.rootCurriculumNodeId)
      .then(res => {
        this.childrentNodes = res;
      })
      .catch(this.failureCallback);
  }


  onOpenCreateCurriculumChildDialogClicked(parent: CurriculumDetail) {
    let titleDialog: TitleDialog = {
      commonTitle: 'Thêm chương học',
      nameTitle: 'Tên chương học (*)',
      contentTitle: 'Khung chương trình'
    }

    const dialogRef = this.dialog.open(AddCurriculumChildDialogComponent, {
      disableClose: true,
      width: '450px',
      height: 'auto',
      data: { parent: parent, titleDialog: titleDialog },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("result ", result);
      if (result !== undefined) {
        if (result === 'submit') {
          this.getCurriculumChildrentNodes();
        }
      }
    });
  }

  onOpenEditCurriculumChildDialogClicked(editCurriculum: CurriculumDetail) {
    let titleDialog: TitleDialog = {
      commonTitle: 'Sửa tên khung chương trình',
      nameTitle: 'Tên khung chương trình',
      contentTitle: ''
    }
    const dialogRef = this.dialog.open(EditCurriculumDialogComponent, {
      disableClose: true,
      width: '450px',
      height: 'auto',
      data: { editCurriculum: editCurriculum, titleDialog: titleDialog, isRootNode: true },
      autoFocus: false
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("result ", result);
      if (result !== undefined) {
        if (result === 'submit') {
          this.getCurriculumDetailInfo();
        }
      }
    });
  }

  back() {
    this.route.navigate(['/dashboard/manage-curriculums']);
  }

}


import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingCurriculumComponent } from './setting-curriculum.component';

describe('SettingCurriculumComponent', () => {
  let component: SettingCurriculumComponent;
  let fixture: ComponentFixture<SettingCurriculumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingCurriculumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingCurriculumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export class LessonStudentDashboard {
    id?: number;
    name?: string;
    avatar?: string;
    percentageOfCompletion?: number;
    createdDate?: Date;
}
import { LessonOfUser } from "sdk/cls-k12-sdk-js/src/services/lesson/responses/lesson-of-user"
import { LessonHomePage } from "sdk/cls-k12-sdk-js/src/services/lesson/responses/lessons-home-page";

export class LessonsVMRegis implements LessonHomePage {

    id: number;
    lessonName: string;
    teacherId: number;
    teacherName: string;
    teacherAvatar: string;
    createdDate: Date;
    approvedDate:Date;
    endDate: Date;
    avatarUrl: string;
    learnTime: number;
    interval: number;
    subjectId: number;
    subjectName: string
    percentageOfCompletion: number;

    static From(data: LessonHomePage, firstname: string, subjectName: string, teacherAvatar: string) {
        var m: LessonsVMRegis =
        {
            id: data.id,
            lessonName: data.lessonName,
            teacherId: data.teacherId,
            avatarUrl: data.avatarUrl,
            interval: data.interval,
            learnTime: data.learnTime,
            percentageOfCompletion: data.percentageOfCompletion,
            createdDate: data.createdDate,
            approvedDate:data.approvedDate,
            endDate: data.endDate,
            subjectId: data.subjectId,
            teacherName: firstname,
            subjectName: subjectName,
            teacherAvatar: teacherAvatar
        };
        return m;
    }
}
import { LessonOfUser } from "sdk/cls-k12-sdk-js/src/services/lesson/responses/lesson-of-user"

export class LessonVM implements LessonOfUser {
    id: number;
    name: string;
    teacherId: number;
    teacherName: string;
    teacherAvatar: string;
    avatarUrl: string;
    registrationDate: Date;
    createdDate: Date;
    startDate: Date;
    approvedDate: Date;
    startTimeLine: string;
    endDate: Date;
    learnTime: number;
    interval: number;
    subjectId: number;
    subjectName: string
    percentageOfCompletion: number;

    static From(data: LessonOfUser, firstname?: string, subjectName?: string, teacherAvatar?: string) {
        var m: LessonVM =
        {
            id: data.id,
            name: data.name,
            teacherId: data.teacherId,
            avatarUrl: data.avatarUrl,
            interval: data.interval,
            learnTime: data.learnTime,
            createdDate: data.createdDate,
            startDate: data.startDate,
            approvedDate: data.approvedDate,
            startTimeLine: data.startTimeLine,
            endDate: data.endDate,
            percentageOfCompletion: data.percentageOfCompletion,
            registrationDate: data.registrationDate,
            subjectId: data.subjectId,
            teacherName: firstname,
            subjectName: subjectName,
            teacherAvatar: teacherAvatar
        };
        return m;
    }
}
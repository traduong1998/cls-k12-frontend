import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UserDashboardComponent } from './user-dashboard.component';
import { StarterRoutes } from './user-dashboard.routing';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(StarterRoutes)
  ],
  declarations: [UserDashboardComponent]
})
export class UserDashboardModule { }

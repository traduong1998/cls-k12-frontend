import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LessonStudentDashboard } from './interfaces/lessonStudentDashboard';
import { UpcomingExams } from './interfaces/upcomingExams';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { CLS, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { LessonVM } from './view-models/lesson-vm';
import { StudentDashboardEndpoint } from 'sdk/cls-k12-sdk-js/src/services/student-dashboard/endpoints/StudentDashboardEndpoint';
import { LessonStudentRegistration } from 'sdk/cls-k12-sdk-js/src/services/student-dashboard/models/LessonStudentRegistration';
import { isNullOrEmpty } from 'src/app/shared/helpers/validation.helper';
import { LessonsVMRegis } from './view-models/lesson-vm-regis';
import { UserExaminationEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-examination/endpoint/user-examination.enpoint';
import { ListExamOfUser } from 'sdk/cls-k12-sdk-js/src/services/user-examination/model/list-exam-of-user';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { MessageService } from 'src/app/shared/services/message.service';
import { LessonInfo } from 'sdk/cls-k12-sdk-js/src/services/student-dashboard/models/LessonInfo';
import { LessonStudent } from 'sdk/cls-k12-sdk-js/src/services/student-dashboard/models/LessonStudent';
import { ExamStudent } from 'sdk/cls-k12-sdk-js/src/services/student-dashboard/models/ExamStudent';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { WarningSnackBarComponent } from 'src/app/shared/snack-bar/warning-snack-bar/warning-snack-bar.component';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit {

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  avatarLesson = "/assets/images/lesson/lesson-default.png";
  totalLessonParticipation = "/assets/images/student-dashboard/total-lesson-participation.png";
  totalLessonRegistered = "/assets/images/student-dashboard/total-lesson-registered.png";
  totalLessonRequest = "/assets/images/student-dashboard/total-lesson-request.png";
  totalLessonCompleted = "/assets/images/student-dashboard/total-lesson-completed.png";
  upcomingExams = "/assets/images/student-dashboard/upcoming-exam.png";
  backgroundUpcomingExams = "/assets/images/student-dashboard/background-upcoming-exams.png";
  backgroundCarousel = "/assets/images/student-dashboard/background-carousel-notext.png";

  //#region ENDPOINT
  lessonEndpoint: LessonEndpoint;
  studentDashboardEndpoint: StudentDashboardEndpoint;
  userExaminationEndpoint: UserExaminationEndpoint;
  baseApiUrl = '';
  //#endregion

  authService: UserIdentity;
  userId: number;
  userTypeRole: string;
  portalId: number;
  divisionId: number;
  schoolId: number;
  dataExamPagination: PaginatorResponse<ListExamOfUser> = {
    items: [],
    totalItems: 0
  };
  examFilter = ({
    gradeId: null,
    subjectId: null,
    examType: null,
    startDate: null,
    endDate: null,
    keyword: null,
    status: null,
    listExamIds: null,
    pageNumber: null,
    pageSize: null,
    sortField: 'CRE',
    sortDirection: 'ASC',
  })
  totalLessonCompletedN: number = 0;
  totalLessonRegisteredN: number = 0;
  totalLessonParticipationN: number = 0;
  totalLessonRequestN: number = 0;
  listLessonId: number[];
  listLessonStudentRegistration: LessonStudentRegistration[] = [];
  listLessonsApproved: LessonStudentRegistration[] = [];
  listLessonsWaitApprove: LessonStudentRegistration[] = [];
  listLessonStudent: LessonStudent[] = [];
  listLessonStudentStuding: LessonStudent[] = [];
  listLessonStudentRequest: LessonStudent[] = [];
  listLessonStudentCompleted: LessonStudent[] = [];
  listExamStudent: ExamStudent[] = [];
  listLessonRegis: LessonsVMRegis[] = [];
  listLessonsForStudentRegis: LessonInfo[];
  todayDate: Date = new Date();
  listLessonsStudentDashboard: LessonVM[] = [];
  listLessonFake: LessonStudentDashboard[] = [
    {
      id: 1,
      name: "Bài 1: Sự đồng biến, nghịch biến của hàm số",
      avatar: "/assets/images/lesson/lesson-default.png",
      percentageOfCompletion: 40,
      createdDate: this.todayDate
    },
    {
      id: 2,
      name: "Bài 2: Khảo sát sự biến thiên và vẽ đồ thị của hàm số",
      avatar: "/assets/images/lesson/lesson-default.png",
      percentageOfCompletion: 60,
      createdDate: this.todayDate
    },
    {
      id: 3,
      name: "Bài 3: Giá trị lớn nhất và giá trị nhỏ nhất của hàm số",
      avatar: "/assets/images/lesson/lesson-default.png",
      percentageOfCompletion: 80,
      createdDate: this.todayDate
    }
  ];

  listUpcomingExams: UpcomingExams[] = [
    {
      id: 1,
      examName: "Kì thi học kì 1",
      startDate: this.todayDate
    },
    {
      id: 2,
      examName: "Kì thi học kì 2",
      startDate: this.todayDate
    },
    {
      id: 3,
      examName: "Kì thi học kì 3",
      startDate: this.todayDate
    }
  ]

  hiddenShowAllExams = false;
  hiddenShowAllLessons = false;
  typeAdmin = '';
  constructor(private route: ActivatedRoute,
    private router: Router,
    private _authService: AuthService,
    private configService: AppConfigService,
    private messageService: MessageService,
    private _snackBar: MatSnackBar) {
    this.lessonEndpoint = new LessonEndpoint();
    this.studentDashboardEndpoint = new StudentDashboardEndpoint();
    this.userExaminationEndpoint = new UserExaminationEndpoint();

    //get token to get user id
    this.authService = this._authService.getTokenInfo();
    this.userId = this.authService.userId;
    this.userTypeRole = this.authService.userTypeRole;
    this.portalId = this.configService.getConfig().unit.portalId;
    this.divisionId = this.configService.getConfig().unit.divisionId;
    this.schoolId = this.configService.getConfig().unit.schoolId;

    this.studentDashboardEndpoint.getListLessonStudent(this.userId).then(res => {
      this.studentDashboardEndpoint.getListLessonIdsNotDel().then(res1 => {
        res.forEach(element => {
          if (res1.includes(element.lessonId)) {
            this.listLessonStudent.push(element)
          }
        });
        this.totalLessonParticipationN = this.listLessonStudent.length;
        this.listLessonId = this.listLessonStudent.map(x => x.lessonId);
        this.studentDashboardEndpoint.getListLessonInfoByIds(this.listLessonId).then(res => {
          // debugger
          this.listLessonStudent.map((lesson) => {
            let info = res.find(x => x.id == lesson.lessonId);
            if (info != undefined) {
              lesson.lessonName = info.name;
              if (isNullOrEmpty(info.avatar)) {
                lesson.avatar = '/assets/images/lesson/lesson-default.png';
              }
              else {
                lesson.avatar = info.avatar;
              }
              lesson.createdDate = info.createdDate;
              if (isNullOrEmpty(lesson.percentageOfCompletion)) {
                lesson.percentageOfCompletion = 0;
              }

              lesson.teacherId = info.teacherId;
            }
          });
          this.listLessonStudentStuding = this.listLessonStudent.filter(x => x.percentageOfCompletion > 0 && x.percentageOfCompletion < 100);
          console.log(`this.listLessonStudentStuding`, this.listLessonStudentStuding)
          // debugger
          if (this.listLessonStudentStuding.length > 0 && this.userTypeRole == "STUDENT") {
            this.hiddenShowAllLessons = false;
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Bạn đang có bài giảng cần hoàn thành !',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
          else {
            this.hiddenShowAllLessons = true;

          }
          // debugger
          this.listLessonStudentRequest = this.listLessonStudent.filter(x => x.assignFrom == 'ASS');
          this.totalLessonRequestN = this.listLessonStudentRequest.length;
          this.listLessonStudentCompleted = this.listLessonStudent.filter(x => x.percentageOfCompletion == 100);
          this.totalLessonCompletedN = this.listLessonStudentCompleted.length;
          this.totalLessonRegisteredN = this.totalLessonParticipationN - this.totalLessonRequestN;
        });
      })
    })

    //this.totalLessonRegisteredN = res.length;
    // console.log(`this.listLessonStudent`, res)
    // this.listLessonStudent = [...res];


    this.studentDashboardEndpoint.getListLessonStudentRegistration(this.userId).then(res => {
      this.studentDashboardEndpoint.getListLessonIdsNotDel().then(res1 => {
        // debugger
        res.forEach(element => {
          if (res1.includes(element.lessonId)) {
            this.listLessonStudentRegistration.push(element);

          }
        });
        this.totalLessonRegisteredN = this.listLessonStudentRegistration.length;
        this.listLessonId = this.listLessonStudentRegistration.map(x => x.lessonId);
        this.studentDashboardEndpoint.getListLessonInfoByIds(this.listLessonId).then(res => {
          this.listLessonStudentRegistration.map((lesson) => {
            let info = res.find(x => x.id == lesson.lessonId);
            if (info != undefined) {
              lesson.lessonName = info.name;
              if (isNullOrEmpty(info.avatar)) {
                lesson.avatar = '/assets/images/lesson/lesson-default.png';
              }
              else {
                lesson.avatar = info.avatar;
              }
            }
            lesson.createdDate = info.createdDate;
          });
          this.listLessonsApproved = this.listLessonStudentRegistration.filter(x => x.isApproval);
          this.listLessonsWaitApprove = this.listLessonStudentRegistration.filter(x => !x.isApproval);
        });
      });
    })


    this.studentDashboardEndpoint.getListExamStudent(this.userId).then((res) => {
      res.forEach(exam => {
        if (exam.status == 'UPC' || exam.status == 'STA') {
          this.listExamStudent.push(exam);
        }
      }
      )
      if (this.listExamStudent.length > 0 && this.userTypeRole == "STUDENT") {
        let requestInfo = this.listExamStudent.map(elem => (
          {
            subjectId: elem.subjectId,
            examId: elem.examId
          }
        ));
        /// lấy thông tin kỳ thi
        this.studentDashboardEndpoint.getListExamInfo(requestInfo).then((res) => {
          this.listExamStudent.forEach(x => {
            let currentInfo = res.find(r => r.examId == x.examId && r.subjectId == x.subjectId);
            if (currentInfo) {
              x.examName = currentInfo.name;
              x.testTime = currentInfo.testTime;
            }
          });
        }).catch(() => {
          this.messageService.failureCallback('Có lỗi xảy ra vui lòng thử lại');
          this.dataExamPagination.totalItems = 0;
          this.dataExamPagination.items = [];
        });
        this._snackBar.openFromComponent(WarningSnackBarComponent, {
          data: 'Bạn chuẩn bị có ca thi !',
          duration: 6000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
      else {
        this.hiddenShowAllExams = true;
      }
    }
    );
    this.baseApiUrl = CLS.getConfig().apiBaseUrl;
  }
  ngAfterViewInit(): void {

  }



  ngOnInit(): void {
    this.typeAdmin = this.configService.getConfig().dashboard.type;
    console.log('typeAdmin', this.typeAdmin)
  }

  showAllLessons() {
    this.router.navigate(['learner/user-learn']);
  }
  showAllExams() {
    this.router.navigate(['learner/user-examination']);
  }

  navigate(cases: number): void {
    if (cases === 1) {
      window.open('/dashboard/user-infor?tab=create-zoom-account', '_blank')
      // this.router.navigateByUrl(`/dashboard/user-infor?tab=create-zoom-account`);
    }
    else if (cases === 2) {
      window.open('/dashboard/online-teaching', '_blank')

      // this.router.navigateByUrl(`/dashboard/online-teaching`);
    }

  }

  getLinkTemplate(cases: number) {
    if (cases === 1) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-huong-dan-tao-tai-khoan-lien-ket-zoom.pdf`;
    }
    else if (cases === 2) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-huong-dan-giao-vien-tao-lop-hoc-truc-tuyen-google-meet-tren-he-thong-cls-k12.pdf`;
    }
    else if (cases === 3) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-huong-dan-giao-vien-tao-lop-hoc-truc-tuyen-zoom-tren-he-thong-cls-k12.pdf`;
    }
  }
}

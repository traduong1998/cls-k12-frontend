import { Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards';

import { UserDashboardComponent } from './user-dashboard.component';

export const StarterRoutes: Routes = [
    {
        path: '',
        component: UserDashboardComponent,//admin
        canActivate: [AuthGuard],
        data: {
            title: 'Tổng quan'
        }
    },
    {
        path: 'index',
        component: UserDashboardComponent,//learner
        canActivate: [AuthGuard],
        data: {
            title: 'Tổng quan'
        }
    }
];

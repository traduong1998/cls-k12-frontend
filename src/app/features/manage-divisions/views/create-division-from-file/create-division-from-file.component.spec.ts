import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDivisionFromFileComponent } from './create-division-from-file.component';

describe('CreateDivisionFromFileComponent', () => {
  let component: CreateDivisionFromFileComponent;
  let fixture: ComponentFixture<CreateDivisionFromFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateDivisionFromFileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDivisionFromFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

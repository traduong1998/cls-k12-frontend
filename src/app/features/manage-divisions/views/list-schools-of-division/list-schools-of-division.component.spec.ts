import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSchoolsOfDivisionComponent } from './list-schools-of-division.component';

describe('ListSchoolsOfDivisionComponent', () => {
  let component: ListSchoolsOfDivisionComponent;
  let fixture: ComponentFixture<ListSchoolsOfDivisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListSchoolsOfDivisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSchoolsOfDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

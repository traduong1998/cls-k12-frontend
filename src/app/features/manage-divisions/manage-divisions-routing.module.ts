import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards';
import { ManageDivisionsComponent } from './manage-divisions.component';
import { ListDivisionsComponent } from './views/list-divisions/list-divisions.component';
import { CreateDivisionComponent } from './views/create-division/create-division.component';
import { CreateDivisionFromFileComponent } from './views/create-division-from-file/create-division-from-file.component';
import { UpdateDivisionComponent } from './views/update-division/update-division.component';
import { ListSchoolsOfDivisionComponent } from './views/list-schools-of-division/list-schools-of-division.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: ManageDivisionsComponent,
    children: [
      {
        path: '',
        component: ListDivisionsComponent,
        data: {
          title: 'Danh sách phòng',
        }
      },
      {
        path: 'create',
        component: CreateDivisionComponent,
        data: {
          title: 'Thêm mới phòng',
        }
      },
      {
        path: 'create-from-file',
        component: CreateDivisionFromFileComponent,
        data: {
          title: 'Thêm phòng từ tệp tin',
        }
      },
      {
        path: 'update',
        component: UpdateDivisionComponent,
        data: {
          title: 'Chỉnh sửa phòng',
        }
      },
      {
        path: 'list-school-of-division',
        component: ListSchoolsOfDivisionComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageDivisionsRoutingModule { }

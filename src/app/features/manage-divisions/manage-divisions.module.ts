import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageDivisionsRoutingModule } from './manage-divisions-routing.module';
import { ManageDivisionsComponent } from './manage-divisions.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CreateDivisionComponent } from './views/create-division/create-division.component';
import { ListDivisionsComponent } from './views/list-divisions/list-divisions.component';
import { ContentCreateDivisionComponent } from './components/content-create-division/content-create-division.component';
import { ContentListDivisionsComponent } from './components/content-list-divisions/content-list-divisions.component';
import { CreateDivisionFromFileComponent } from './views/create-division-from-file/create-division-from-file.component';
import { ContentCreateDivisionFromFileComponent } from './components/content-create-division-from-file/content-create-division-from-file.component';
import { UpdateDivisionComponent } from './views/update-division/update-division.component';
import { ContentUpdateDivisionComponent } from './components/content-update-division/content-update-division.component';
import { ListSchoolsOfDivisionComponent } from './views/list-schools-of-division/list-schools-of-division.component';
import { ContentListSchoolsOfDivisionComponent } from './components/content-list-schools-of-division/content-list-schools-of-division.component';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [
    ManageDivisionsComponent,
    CreateDivisionComponent,
    ListDivisionsComponent,
    ContentCreateDivisionComponent,
    ContentListDivisionsComponent,
    CreateDivisionFromFileComponent,
    ContentCreateDivisionFromFileComponent,
    UpdateDivisionComponent,
    ContentUpdateDivisionComponent,
    ListSchoolsOfDivisionComponent,
    ContentListSchoolsOfDivisionComponent],
  imports: [
    CommonModule,
    SharedModule,
    ManageDivisionsRoutingModule,
    NgxSpinnerModule
  ]
})
export class ManageDivisionsModule { }

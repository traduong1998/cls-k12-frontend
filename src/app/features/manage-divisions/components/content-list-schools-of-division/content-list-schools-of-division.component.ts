import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { TranslateCacheSettings } from 'ngx-translate-cache';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { School } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { SchoolEndpoint } from 'cls-k12-sdk-js/src/services/school/endpoints/SchoolEndpoint';
import { SchoolFilterRequests } from 'cls-k12-sdk-js/src/services/school/models/SchoolPagingRequest';
import { FormControl } from '@angular/forms';
import { take, takeUntil } from 'rxjs/operators';
import { Subject, ReplaySubject } from 'rxjs';
import { DivisionOption, Division } from 'cls-k12-sdk-js/src/services/division/models/Division';
import { DivisionEndpoint } from 'cls-k12-sdk-js/src/services/division/endpoints/DivisionEndpoint';
import { UserIdentity } from 'cls-k12-sdk-js/src';
import { AuthService } from 'src/app/core/services';

const schoolsPaginationDefault: PaginatorResponse<School> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-content-list-schools-of-division',
  templateUrl: './content-list-schools-of-division.component.html',
  styleUrls: ['./content-list-schools-of-division.component.scss']
})
export class ContentListSchoolsOfDivisionComponent implements OnInit {

  schoolEndpoint: SchoolEndpoint;
  endpointDivision: DivisionEndpoint
  userIdentity: UserIdentity;
  isShowFilter = true;
  levelManageValue: number = 0;
  isFirstLoadDivisions = true;
  public txtFullName: FormControl = new FormControl('');
  /** list of divisions */

  isLoadingResults = true;

  baseApiUrl = 'http://localhost:65000';

  displayedColumns: string[] = ['stt', 'code', 'name'];

  schoolsPagination: PaginatorResponse<School> = schoolsPaginationDefault;
  schoolsPaginationFilter: SchoolFilterRequests = {
    pageNumber: 0,
    sizeNumber: 10,
    getCount: true,
    sortDirection: 'ASC',
    sortField: 'CRE',
  }

  constructor(private _authService: AuthService, public dialog: MatDialog, private _router: Router, @Inject(MAT_DIALOG_DATA) public divisionInfo: Division) {
    this.schoolEndpoint = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
  }
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit(): void {
    this.getSchoolsData();

  }


  failureCallback() {
    this.isLoadingResults = false;
    alert("hihi 400");
  }


  getSchoolsData() {
    this.isLoadingResults = true;
    this.schoolsPaginationFilter.divisionId = this.divisionInfo.id
    this.schoolEndpoint.getSchoolsPaging(this.schoolsPaginationFilter)
      .then(res => {

        this.isLoadingResults = false;
        this.schoolsPagination = res;
      })
      .catch(this.failureCallback);
  }



  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.schoolsPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.schoolsPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.getSchoolsData();
    });

  }

}

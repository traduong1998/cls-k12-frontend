import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentListSchoolsOfDivisionComponent } from './content-list-schools-of-division.component';

describe('ContentListSchoolsOfDivisionComponent', () => {
  let component: ContentListSchoolsOfDivisionComponent;
  let fixture: ComponentFixture<ContentListSchoolsOfDivisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentListSchoolsOfDivisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentListSchoolsOfDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentCreateDivisionFromFileComponent } from './content-create-division-from-file.component';

describe('ContentCreateDivisionFromFileComponent', () => {
  let component: ContentCreateDivisionFromFileComponent;
  let fixture: ComponentFixture<ContentCreateDivisionFromFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentCreateDivisionFromFileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentCreateDivisionFromFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { _isNumberValue } from '@angular/cdk/coercion';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ReplaySubject, Subject } from 'rxjs';
import { pairwise, startWith, take, takeUntil } from 'rxjs/operators';
import { CLS, DivisionEndpoint, DivisionOption, GroupStudentEndpoint, SchoolEndpoint, UserEndpoint, UserIdentity, UserTypeEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { SchoolFromFile } from 'sdk/cls-k12-sdk-js/src/services/school/models/SchoolFromFile';
import { CreateSchoolErrorResponse } from 'sdk/cls-k12-sdk-js/src/services/school/responses/createSchoolErrorResponse';
// import { TeacherFromFile } from 'sdk/cls-k12-sdk-js/src/services/user/models/teacher-from-file';
// import { CreateTeachersFromFile } from 'sdk/cls-k12-sdk-js/src/services/user/requests/create-teachers-from-file';
import { UserTypeOption } from 'sdk/cls-k12-sdk-js/src/services/usertype/models/UserType';
import { AuthService } from 'src/app/core/services';
import { Grade, GradeOfSchool, GradeOption } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import * as XLSX from 'xlsx';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { MatTableDataSource } from '@angular/material/table';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { DivisionFromFile } from 'sdk/cls-k12-sdk-js/src/services/division/models/DivisionFromFile';
import { CreateDivisionErrorResponse } from 'sdk/cls-k12-sdk-js/src/services/division/responses/createDivisionErrorResponse';
import { Configuration } from 'src/app/shared/configurations';

@Component({
  selector: 'app-content-create-division-from-file',
  templateUrl: './content-create-division-from-file.component.html',
  styleUrls: ['./content-create-division-from-file.component.scss']
})
export class ContentCreateDivisionFromFileComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  @ViewChild('uploader', { static: false }) inputValue: ElementRef
  dataOldFile: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSourceError: CreateSchoolErrorResponse[];
  dataSource: MatTableDataSource<CreateSchoolErrorResponse>;
  gradeLists: GradeOption[] = [];
  listGradeExcel: number[] = [];
  listkeyGradeExcel: string[] = [];

  protected _onDestroy = new Subject<void>();
  config = { baseUrl: '' }
  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT
  private schoolEndpoint: SchoolEndpoint;
  private divisionEndpoint: DivisionEndpoint;
  userIdentity: UserIdentity;

  //#endregion

  //#region PRIVATE PROPERTY
  private oldKeys: string[] = ['STT', 'Mã phòng giáo dục(*)', 'Tên phòng giáo dục(*)'];
  private newKeys: string[] = ['no', 'code', 'name'];

  //#endregion

  //#region PUBLIC PROPERTY
  public file: File;
  public isMaxColumn: boolean = false;
  public errorCount: number = 0;
  public totalUpCount: number = 0;
  public userTypeLevelManage: string;
  public isSchoolLevelUserType: boolean = true;
  public isDivisionLevelUserType: boolean = true;
  public isDepartmentLevelUserType: boolean = true;
  public levelManageValue: number = 0;
  public nameFileUpload: string = '';
  public isHasFile = true;
  public ishasData: boolean = null;
  public isSuccess = false;
  public isLoadTableData = false;
  public isSizeExceeded = false;

  addDivisionFromFile: FormGroup;

  public displayedColumns = ['position', 'column', 'code', 'name', 'infoError'];

  schoolFilter = {
    divisionId: null
  }
  //#endregion

  //#region CONSTRUCTOR
  constructor(private _authService: AuthService, private spinner: NgxSpinnerService, private router: Router, private _snackBar: MatSnackBar) {
    this.addDivisionFromFile = new FormGroup({
    });
    this.baseApiUrl = CLS.getConfig().apiBaseUrl;

    this.userIdentity = _authService.getTokenInfo();
    this.schoolEndpoint = new SchoolEndpoint();
    this.divisionEndpoint = new DivisionEndpoint({ baseUrl: this.baseApiUrl });

  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {

  }

  ngAfterViewInit() {
    if (this.dataSourceError) {
      this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
      this.paginator.page.subscribe(() => {
        this.paginator.pageIndex
        this.paginator.pageSize
        this.dataSource = new MatTableDataSource<CreateSchoolErrorResponse>(this.dataSourceError);
        this.dataSource.paginator = this.paginator;
      });
    }

  }


  //#endregion

  //#region PUBLIC METHOD

  public onChooseFileButtonHandleClick() {
    this.isHasFile = null;
    this.changeFileInput();
  }

  private changeFileInput() {
    this.inputValue.nativeElement.click()
  }

  public uploadFile(event: any) {
    /* wire up file reader */
    //const target: DataTransfer = <DataTransfer>(event.target);
    if (event.addedFiles.length !== 1) {
      throw new Error('Cannot use multiple files');
    }

    this.file = event.addedFiles[0];
    this.nameFileUpload = this.file?.name;
    if (this.file.name.slice(this.file.name.lastIndexOf(".") + 1) != "xlsx") {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'File không đúng định dạng!',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      this.file = undefined;
      return;
    }
    this.isHasFile = true;
  }
  //#endregion

  //#region PRIVATE METHOD


  private changeNameKeyJson(objs: any, oldKeys: string[], newKeys: string[]) {
    return objs.forEach(obj => {
      for (let index = 0; index < oldKeys.length; index++) {
        this.renameKey(obj, oldKeys[index], newKeys[index])
      }
    });
  }

  private renameKey(obj: any, oldKey: string, newKey: string) {
    obj[newKey] = obj[oldKey];
    delete obj[oldKey];
  }


  // getTemplate() {
  //   this.spinner.show(undefined, {
  //     type: SpinnerType.ballSpin,
  //     size: 'medium',
  //     bdColor: 'rgba(0, 0, 0, 0.2)',
  //     color: '#43a047',
  //     fullScreen: false
  //   });
  //   let date = new Date()
  //   let ticks = date.getTime()
  //   let fileName: string;
  //   let objectUrl: string;
  //   const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
  //   this.schoolEndpoint.getTemplate()
  //     .then((urlFile) => {
  //       objectUrl = urlFile;
  //       fileName = 'ImportSchoolsSample-' + this.userIdentity.levelManage + '-' + ticks + '.xlsx';
  //       a.href = objectUrl;
  //       a.download = fileName;
  //       document.body.appendChild(a);
  //       a.click();

  //       document.body.removeChild(a);
  //       URL.revokeObjectURL(objectUrl);
  //       this.spinner.hide();
  //     })
  // }

  public onBackToManagerHandleClick() {
    this.router.navigateByUrl(`dashboard/manage-divisions`)
  }

  public onSubmitHandleClick() {

    if (!this.file) {
      this.isHasFile = false;
      return;
    }

    this.isHasFile = true;



    if (this.file) {

      this.readExcel();
    }

  }

  async readExcel() {
    let reader: FileReader = new FileReader();
    if (this.file.size > 500000000) {
      this.isSizeExceeded = true;
      return;
    }
    await reader.readAsBinaryString(this.file);

    if (reader.error != null) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Có thể dữ liệu file excel đã thay đổi, mời bạn chọn lại file!',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }

    if (this.file && this.file.name.includes('xlsx')) {
      this.nameFileUpload = this.file?.name;
      //read excel file

      //if (reader.onload)
      reader.onload = (e: any) => {
        /* create workbook */
        let binarystr: string = e.target.result;
        let wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

        /* selected the first sheet */
        let wsname: string = wb.SheetNames[0];
        let ws: XLSX.WorkSheet = wb.Sheets[wsname];

        /* save data */
        let data: any = XLSX.utils.sheet_to_json(ws); // to get 2d array pass 2nd parameter as object {header: 1}

        if (data.length == 0) {
          this.ishasData = false;
          this.isMaxColumn = null;
          this.isLoadTableData = false;
          this.isSuccess = false;
          return;
        }
        if (data.length > 200) {
          this.isMaxColumn = true;
          this.ishasData = true;
          this.isLoadTableData = false;
          return;
        }

        this.changeNameKeyJson(data, this.oldKeys, this.newKeys);
        console.log(`dataex`, data); // Data will be logged in array format containing objects

        if (JSON.stringify(this.dataOldFile) == JSON.stringify(data)) {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'File excel không thay đổi!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          return;
        }

        this.dataOldFile = data;
        let importDivisions: DivisionFromFile[] = [];
        //return new Promise(resolve => {
        [...data].forEach(division => {
          // let valueSchool: string[] = Object.values(school);
          // let listkeyGradeExcel: number[] = [];
          // valueSchool.forEach(vs => {
          //   let vsString: string;
          //   if (vs != undefined) {
          //     vsString = vs.toString();
          //     if (vsString.includes('|1') && vsString.includes('grade::')) {
          //       listkeyGradeExcel.push(parseInt(vsString.slice(vsString.lastIndexOf(":") + 1, vsString.indexOf("|"))));
          //     }
          //   }

          // });
          let importDivision = new DivisionFromFile();
          importDivision.no = division.no
          importDivision.code = division.code
          importDivision.name = division.name
          if (importDivision.code == undefined && importDivision.name == undefined) {
            //TODO
          }
          else {
            importDivisions.push(importDivision);
          }
          // if (school.divisionId == "") {
          //   importSchool.divisionId = 0;
          // }
          // else {
          //   importSchool.divisionId = school.divisionId
          // }
          // if (listkeyGradeExcel.length == 0) {
          //   importSchool.listGradeId == null;
          // }
          // else {
          //   importSchool.listGradeId = listkeyGradeExcel
          // }
          // if (importSchool.no != undefined || importSchool.code != undefined || importSchool.name != undefined) {
          //   importSchools.push(importSchool);
          // }
        });
        if ((importDivisions.length == 0)) {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'File excel không có dữ liệu của phòng!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          return;
        }
        this.dataSourceError = [];
        this.divisionEndpoint.createDivisionImportFromFile(importDivisions).then(res => {
          console.log(res)
          this.errorCount = res.length;
          if (this.errorCount > 0) {
            this.dataSourceError = res;
            this.dataSourceError.forEach(item => {
              item.errorResult.forEach(errorResult => {
                var index = this.newKeys
                  .findIndex(x => x.toLocaleLowerCase() == errorResult.columnError.toLocaleLowerCase());
                if (index > -1) {
                  errorResult.columnError = this.oldKeys[index];
                }
              })
              this.dataSource = new MatTableDataSource<CreateDivisionErrorResponse>(this.dataSourceError);
              this.dataSource.paginator = this.paginator;
              this.errorCount = res.length;
              this.totalUpCount = importDivisions.length;
              this.isLoadTableData = true;
            })
          } else {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: "Bạn đã tải lên thành công " + (importDivisions.length - this.errorCount) + " phòng!",
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            this.isSuccess = true;
            this.isLoadTableData = false;
            this.totalUpCount = data.length;
            this.errorCount = 0;
            this.resetValues();
          }
        })
      };
    }
    this.resetValues();
    this.isSuccess = false;

    // this.inputValue.nativeElement.value = '';
    // this.addSchoolFromFile.reset();
  }
  private resetValues() {
    this.addDivisionFromFile.reset();
    this.file = null;
    this.nameFileUpload = '';
    // this.inputValue.nativeElement.value = '';
    this.dataOldFile = undefined;

  }
  onRemove(event) {
    this.file = undefined;
  }
  //#endregion
  public getLinkExcelTemplate() {
    return `${this.baseApiUrl}/system/public/files/cdn/templates/division/${Configuration.FILE_NAME_CREATE_DIVISIONS}`
  }
}

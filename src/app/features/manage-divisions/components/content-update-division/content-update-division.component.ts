import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { NgForm, Validators, FormControl } from '@angular/forms';
import { UpdateDivisionRequest } from 'cls-k12-sdk-js/src/services/division/requests/updateDivisionRequest';
import { DivisionEndpoint } from 'cls-k12-sdk-js/src/services/division/endpoints/DivisionEndpoint';
import { Division } from 'cls-k12-sdk-js/src/services/division/models/Division';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material/snack-bar';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';


@Component({
  selector: 'app-content-update-division',
  templateUrl: './content-update-division.component.html',
  styleUrls: ['./content-update-division.component.scss']
})
export class ContentUpdateDivisionComponent implements OnInit {

  divisionEndpoint: DivisionEndpoint;
  lblname = ''
  lblcode = ''
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  baseApiUrl = 'http://localhost:65000';

  division: UpdateDivisionRequest = {
    name: '',
    code: ''
  };

  codePattern = '^([A-Za-z0-9]+(?:\.[A-Za-z0-9]+)*)$';

  codeupdateFormControl = new FormControl('', [
    Validators.pattern(this.codePattern)
  ])
  
  constructor(private mdDialogRef: MatDialogRef<ContentUpdateDivisionComponent>,
    @Inject(MAT_DIALOG_DATA) public divisionInfo: Division, private _snackBar: MatSnackBar) {
    this.divisionEndpoint = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
  }

  formDivision: Division = {
    name: this.divisionInfo.name,
    code: this.divisionInfo.code
  }

  ngOnInit(): void {
  }

  @ViewChild('regForm') myForm: NgForm;

  resetForm() {
    this.lblname = '';
    this.lblcode = '';
  }

  checkForm() {
    var result = true
    if (this.formDivision.name === '') {
      this.lblname = 'Bạn chưa nhập tên Phòng!';
      result = false;
    }
    if (this.formDivision.code === '') {
      this.lblcode = 'Bạn chưa nhập mã Phòng!';
      result = false;
    }
    return result;
  }

  EditDivision(regForm: NgForm) {
    this.resetForm();

    if (this.checkForm()) {
      this.divisionEndpoint.checkNameDivision(this.divisionInfo["id"], this.formDivision.name)
        .then(res => {
          //hàm này đặc thù trả về true, false nên sẽ vô then. nếu hàm bình thường thì thường backend sẽ thow exception nếu k thỏa điều kiện để update hoặc đầu vào bị validate
          if (res) {
            this.lblname = 'Tên Phòng đã tồn tại trong hệ thống!';
            return;
          }

          this.division["id"] = this.divisionInfo["id"];
          this.division["name"] = this.formDivision.name;
          this.division["code"] = this.formDivision.code;

          this.divisionEndpoint.updateDivision(this.division)
            .then(res => {
              this.mdDialogRef.close('submit')
              if (res) {
                this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                  data: 'Sửa phòng thành công',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              } else {
                this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                  data: 'Sửa phòng không thành công',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            })
            .catch((err: ErrorResponse) => {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: err.errorDetail,
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            });
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });

    }

  }
}
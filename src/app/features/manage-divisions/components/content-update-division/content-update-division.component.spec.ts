import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentUpdateDivisionComponent } from './content-update-division.component';

describe('ContentUpdateDivisionComponent', () => {
  let component: ContentUpdateDivisionComponent;
  let fixture: ComponentFixture<ContentUpdateDivisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentUpdateDivisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentUpdateDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

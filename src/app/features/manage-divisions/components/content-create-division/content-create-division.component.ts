import { Component, OnInit, Injectable } from '@angular/core';
import { NgForm, Validators, FormControl } from '@angular/forms';
import { CreateDivisionRequest } from 'cls-k12-sdk-js/src/services/division/requests/createDivisionRequest';
import { DivisionEndpoint } from 'cls-k12-sdk-js/src/services/division/endpoints/DivisionEndpoint';
import { DivisionFilterRequests } from 'cls-k12-sdk-js/src/services/division/models/DivisionPagingRequest';
import { Division } from 'cls-k12-sdk-js/src/services/division/models/Division';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { MatDialogRef } from '@angular/material/dialog';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { WhitespaceValidator } from '../../helpers/checkWhitespace';

const divisionsPaginationDefault: PaginatorResponse<Division> = {
  items: [],
  totalItems: 0
};
@Injectable({ providedIn: 'root' })
@Component({
  selector: 'app-content-create-division',
  templateUrl: './content-create-division.component.html',
  styleUrls: ['./content-create-division.component.scss']
})
export class ContentCreateDivisionComponent implements OnInit {

  divisionEndpoint: DivisionEndpoint;
  baseApiUrl = 'http://localhost:65000';
  lblname = ''
  lblcode = ''
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  division: CreateDivisionRequest = {
    name: '',
    code: ''
  };
  divisionsPagination: PaginatorResponse<Division> = divisionsPaginationDefault;
  divisionsPaginationFilter: DivisionFilterRequests = {
    pageNumber: 0,
    sizeNumber: 10,
    getCount: true,
    sortDirection: 'ASC',
    sortField: 'CRE',
  }

  codePattern = '^([A-Za-z0-9]+(?:\.[A-Za-z0-9]+)*)$';

  codeFormControl = new FormControl('', [
    Validators.pattern(this.codePattern),Validators.required, WhitespaceValidator.cannotContainSpace
  ])
  constructor(private mdDialogRef: MatDialogRef<ContentCreateDivisionComponent>, private _snackBar: MatSnackBar) {
    this.divisionEndpoint = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
  }

  ngOnInit(): void {
  }

  failureCallback() {
    alert("hihi 400");
  }

  getDivisionsData() {
    this.divisionEndpoint.getDivisionsPaging(this.divisionsPaginationFilter)
      .then(res => {
        this.divisionsPagination = res;
      })
      .catch(this.failureCallback);
  }

  resetMessage() {
    this.lblname = '';
    this.lblcode = '';
  }

  checkForm() {
    var result = true;
    if (this.division.name === '') {
      this.lblname = 'Bạn chưa nhập tên Phòng!'
      result = false;
    }
    if (this.division.code === '') {
      this.lblcode = 'Bạn chưa nhập mã phòng'
      result = false;
    }

    return result;
  }

  submitForm(regForm: NgForm) {
    this.resetMessage();
    if (this.checkForm()) {
      this.divisionEndpoint.createDivision(this.division)
        .then(res => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Thêm phòng thành công!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: "Thêm phòng không thành công. Vui lòng thử lại",
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });

        });
      this.mdDialogRef.close('submit')
    }
  }
}

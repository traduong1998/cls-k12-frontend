import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentCreateDivisionComponent } from './content-create-division.component';

describe('ContentCreateDivisionComponent', () => {
  let component: ContentCreateDivisionComponent;
  let fixture: ComponentFixture<ContentCreateDivisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentCreateDivisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentCreateDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

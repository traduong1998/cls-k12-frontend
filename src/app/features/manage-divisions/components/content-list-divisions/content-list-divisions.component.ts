import { SelectionModel } from '@angular/cdk/collections';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ContentCreateDivisionComponent } from '../../components/content-create-division/content-create-division.component';
import { ContentUpdateDivisionComponent } from '../../components/content-update-division/content-update-division.component';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { Division } from 'sdk/cls-k12-sdk-js/src/services/division/models/Division';
import { DivisionEndpoint } from 'cls-k12-sdk-js/src/services/division/endpoints/DivisionEndpoint';
import { DivisionFilterRequests } from 'cls-k12-sdk-js/src/services/division/models/DivisionPagingRequest';
import { ContentListSchoolsOfDivisionComponent } from '../content-list-schools-of-division/content-list-schools-of-division.component';
import { AuthService } from 'src/app/core/services';
import { UserIdentity, CLSModules, CLSPermissions } from 'cls-k12-sdk-js/src';
import { NgxSpinnerService } from 'ngx-spinner';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { DeleteDivisionRequest } from 'sdk/cls-k12-sdk-js/src/services/division/requests/deleteDivisionRequest';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';


const divisionsPaginationDefault: PaginatorResponse<Division> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-content-list-divisions',
  templateUrl: './content-list-divisions.component.html',
  styleUrls: ['./content-list-divisions.component.scss']
})
export class ContentListDivisionsComponent implements OnInit {
  @ViewChild("ipSearch") ipSearch: ElementRef;

  divisionEndpoint: DivisionEndpoint;

  baseApiUrl = 'http://localhost:65000';

  // displayedColumns: string[] = ['select', 'stt', 'code', 'name', 'school', 'function'];
  displayedColumns: string[] = ['stt', 'name', 'code', 'function'];

  divisionsPagination: PaginatorResponse<Division> = divisionsPaginationDefault;
  divisionsPaginationFilter: DivisionFilterRequests = {
    keyWord: '',
    pageNumber: 0,
    sizeNumber: 10,
    getCount: true,
    sortDirection: 'ASC',
    sortField: 'CRE',
  }
  divisionsPaginationFilterClone: DivisionFilterRequests;
  async applyFilter() {

    const filterValue = this.ipSearch.nativeElement.value;
    this.divisionsPaginationFilter.keyWord = filterValue;
    this.divisionsPaginationFilter.pageNumber = 1;
    this.paginator.pageIndex = 0;
    if (!compareTwoObject(this.divisionsPaginationFilter, this.divisionsPaginationFilterClone)) {
      this.getDivisionsData();
    }
  }

  selection = new SelectionModel<Division>(true, []);
  selectedDivisionIds = [];
  isLoadingResults = true;

  userIdentity: UserIdentity;
  hasAddPermission: boolean;
  hasDeletePermission: boolean;
  hasEditPermission: boolean;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(private _snackBar: MatSnackBar, public dialog: MatDialog, private _router: Router, private _authService: AuthService, private spinner: NgxSpinnerService) {
    this.divisionEndpoint = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();
    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.Division, CLSPermissions.Add);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.Division, CLSPermissions.Edit);
    this.hasDeletePermission = this.userIdentity.hasPermission(CLSModules.Division, CLSPermissions.Delete);


    this.spinner.show();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit(): void {
    this.getDivisionsData();
  }

  failureCallback() {
    this.isLoadingResults = false;
    alert("hihi 400");
  }

  getDivisionsData() {
    this.isLoadingResults = true;
    this.divisionEndpoint.getDivisionsPaging(this.divisionsPaginationFilter)
      .then(res => {
        this.spinner.hide();
        this.isLoadingResults = false;
        this.divisionsPagination = res;
        this.divisionsPaginationFilterClone = { ...this.divisionsPaginationFilter };
      })
      .catch(this.failureCallback);
  }


  //constructor() { }
  openDialog() {
    const dialogRef = this.dialog.open(ContentCreateDivisionComponent, {
      disableClose: true,
      width: '450px',
      height: 'auto',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getDivisionsData()
        }
      }
    });
  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.divisionsPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.divisionsPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.getDivisionsData();
    });

  }

  /**Show dialog Update Division */
  UpdateDivision(data) {
    this.selection.clear();
    //show dialog
    const dialogRef = this.dialog.open(ContentUpdateDivisionComponent, {
      disableClose: true,
      width: '450px',
      height: 'auto',
      data: data
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getDivisionsData()
        }
      }
    });
  }
  /**Show dialog Update Division */
  DeleteDivision(data) {
    Promise.resolve()
      .then(() => {
       return this.divisionEndpoint.countSchoolOfDivision(data.id)
      })
      .then((res) => {
        if(res > 0){
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: "Đang có trường thuộc phòng, không được phép xóa",
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }else{
          const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
            disableClose: true,
            width: '444px',
            height: 'auto',
            //data: data
            data: {
              title: 'Xóa phòng giáo dục',
              name: data.name,
              message: ''
            }
          });
      
          dialogRef.afterClosed().subscribe(result => {
            if (result == 'DONGY') {
              let model = new DeleteDivisionRequest();
              model.id = data.id;
              this.divisionEndpoint.deleteDivision(model)
                .then(res => {
                  if (res) {
                    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                      data: 'Xóa phòng thành công!',
                      duration: 3000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                    });
                    this.getDivisionsData()
                  } else {
      
                    this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                      data: 'Xóa phòng không thành công!',
                      duration: 3000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                    });
                  }
                })
                .catch((err: ErrorResponse) => {
                  this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                    data: err.errorDetail,
                    duration: 3000,
                    horizontalPosition: this.horizontalPosition,
                    verticalPosition: this.verticalPosition,
                  });
                });
            }
            else {
              if (result != undefined)
                this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                  data: 'Bạn nhập chưa đúng, vui lòng thử lại',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
            }
          });
        }
      })

    // this.divisionEndpoint.countSchoolOfDivision(data.id)
    //   .then(res => {

    //     if (res == 0) {
    //       //show dialog

    //     } else {
    //       this._snackBar.openFromComponent(ErrorSnackBarComponent, {
    //         data: 'Đang có Trường thuộc Phòng hiện tại. Không được phép xóa!',
    //         duration: 3000,
    //         horizontalPosition: this.horizontalPosition,
    //         verticalPosition: this.verticalPosition,
    //       });
    //     }
    //   })

    this.selection.clear();
  }
  UpdateSchool(data) {
    this.selection.clear();
    //show dialog
    const dialogRef = this.dialog.open(ContentListSchoolsOfDivisionComponent, {
      disableClose: true,
      width: '550px',
      height: 'auto',
      autoFocus: false,
      data: data
      // data: {name: this.name, animal: this.animal}
    });


    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getDivisionsData()
        }
      }
    });
  }


  public addDivisionFromFile() {
    this._router.navigate(['/dashboard/manage-divisions/create-from-file']);
    console.log("🚀 ~ file: box-filter-bottom.component.ts ~ line 46 ~ BoxFilterBottomComponent ~ addLesson ~ _router", this._router)

  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.divisionsPagination.items.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.selectedDivisionIds = [];
    } else {
      this.divisionsPagination.items.forEach(row => {
        this.selection.select(row);
        this.selectedDivisionIds.push(row.id);
      })
    }
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Division): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }
}
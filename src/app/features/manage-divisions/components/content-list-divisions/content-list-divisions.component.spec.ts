import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentListDivisionsComponent } from './content-list-divisions.component';

describe('ContentListDivisionsComponent', () => {
  let component: ContentListDivisionsComponent;
  let fixture: ComponentFixture<ContentListDivisionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentListDivisionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentListDivisionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomizeReportComponent } from './views/customize-report/customize-report.component';
import { DetailUsersGroupReportComponent } from './views/detail-users-group-report/detail-users-group-report.component';
import { ExamReportManageComponent } from './views/exam-report-manage/exam-report-manage.component';
import { LessonReportViewComponent } from './views/lesson-report-view/lesson-report-view.component';
import { LessonUsersgroupReportComponent } from './views/lesson-usersgroup-report/lesson-usersgroup-report.component';
import { ListExamReportComponent } from './views/list-exam-report/list-exam-report.component';
import { ListExamSubjectReportComponent } from './views/list-exam-subject-report/list-exam-subject-report.component';
import { OnlineClassesReportComponent } from './views/online-classes-report/online-classes-report.component';
import { RollCardClassroomReportComponent } from './views/roll-card-classroom-report/roll-card-classroom-report.component';
import { StudentLearnLessonReportComponent } from './views/student-learn-lesson-report/student-learn-lesson-report.component';
import { StudentReportComponent } from './views/student-report/student-report.component';
import { StudyHistoryReportComponent } from './views/study-history-report/study-history-report.view';
import { TeacherReportComponent } from './views/teacher-report/teacher-report.component';
import { UsersgroupReportComponent } from './views/usersgroup-report/usersgroup-report.component';
import { HistoryUserLessonComponent } from './views/history-user-lesson/history-user-lesson.component';

const routes: Routes = [
  {
    path: '',
    component: StudyHistoryReportComponent,
    data: {
      title: 'Lịch sử học tập'
    }
  },
  {
    path: 'teacher',
    component: TeacherReportComponent,
    data: {
      title: 'Báo cáo giáo viên'
    }
  },
  {
    path: 'student',
    component: StudentReportComponent,
    data: {
      title: 'Báo cáo học sinh'
    }
  },
  {
    path: 'grade',
    component: StudentReportComponent,
    data: {
      title: 'Báo cáo khối, lớp học'
    }
  },
  {
    path: 'lesson',
    component: LessonReportViewComponent,
    data: {
      title: 'Báo cáo bài giảng'
    }
  },
  {
    path: 'lessons/:id',
    component: StudentLearnLessonReportComponent,
    data: {
      title: 'Thống kê học sinh học bài giảng'
    }
  },
  {
    path: 'customize',
    component: CustomizeReportComponent,
    data: {
      title: 'Báo cáo tùy biến'
    }
  },
  {
    path: 'exam',
    component: ListExamReportComponent,
    data: {
      title: 'Báo cáo kỳ thi'
    }
  },
  {
    path: 'exam/:id/grade/:gradeId/examsubject',
    component: ListExamSubjectReportComponent,
    data: {
      title: 'Báo cáo kỳ thi'
    }
  },
  {
    path: 'exam/:id/grade/:gradeId/examsubject/:subjectId',
    component: ExamReportManageComponent,
    data: {
      title: 'Báo cáo kỳ thi'
    }
  }, {
    path: 'users-groups-report',
    component: UsersgroupReportComponent,
    data: {
      title: 'Thống kê học sinh học bài giảng'
    }
  },
  {
    path: ':id/detail-users-groups-report',
    component: DetailUsersGroupReportComponent,

    data: {
      title: 'Thống kê học sinh học bài giảng'
    }
  },
  {
    path: ':id/detail-users-groups-report/:userId/learn-report',
    component: LessonUsersgroupReportComponent,
    data: {
      title: 'Thống kê học sinh học bài giảng'
    }
  },
  {
    path: 'online-classes',
    component: OnlineClassesReportComponent,
    data: {
      title: 'Thống kê lớp học tương tác'
    }
  },
  {
    path: ':meetingRoomId/roll-call-report',
    component: RollCardClassroomReportComponent,
    data: {
      title: 'Thống kê chi tiết kết quả dạy và học'
    }
  },{
    path: 'history-user-lesson/:id',
    component: HistoryUserLessonComponent,
    data: {
      title: 'Lịch sử học bài giảng'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagerReportRoutingModule { }

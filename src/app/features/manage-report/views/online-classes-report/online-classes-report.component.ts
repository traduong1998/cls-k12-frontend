import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ReplaySubject, Subject } from 'rxjs';
import { pairwise, startWith, take, takeUntil } from 'rxjs/operators';
import { CLS, CLSModules, CLSPermissions, DivisionEndpoint, DivisionOption, LevelManages, SchoolEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { ContentLearnerDetailEndpoint, LessonSelect } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/content-learner-detail-endpoint';
import { TeacherZoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/teacherZoomEndpoint';
import { VirtualClassRoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/virtual-classroom-endpoints';
import { VirtualClassRoom } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/virtualClassRoom';
import { OnlineClasses } from 'sdk/cls-k12-sdk-js/src/services/meeting-room/models/onlineClasses';
import { GetMeetingRoomsRequest, ReportMeetingRoomsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/report/meeting-rooms';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { AuthService } from 'src/app/core/services';
import { ConfirmTeacherJoinDialogComponent, DataDialog, DialogDataCloseResult } from 'src/app/features/online-teaching/components/confirm-teacher-join-dialog/confirm-teacher-join-dialog.component';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { MessageService } from 'src/app/shared/services/message.service';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'app-online-classes-report',
  templateUrl: './online-classes-report.component.html',
  styleUrls: ['./online-classes-report.component.scss']
})
export class OnlineClassesReportComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('dpFromDate') dpFromDate; clea
  @ViewChild('dpToDate') dpToDate;

  @ViewChild('paginatorClasses') paginatorClasses: MatPaginator;
  @ViewChild('dpFromDateClasses') dpFromDateClasses;
  @ViewChild('dpToDateClasses') dpToDateClasses;

  /* Division */
  protected divisions: DivisionOption[];
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  /* School */
  protected schools: SchoolOption[];
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;


  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT
  private virtualClassRoomEndpoint: VirtualClassRoomEndpoint;
  private contentLearnerDetailEndpoint: ContentLearnerDetailEndpoint;
  teacherZoomEndpoint = new TeacherZoomEndpoint();
  virtualClassroomEndpoint = new VirtualClassRoomEndpoint();
  reportMeetingRoomEndpoint = new ReportMeetingRoomsEndpoint();
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  hasAddClassPermission: boolean;
  hasEditClassPermission: boolean;
  hasDeleteClassPermission: boolean;
  hasAssignClassPermission: boolean;
  userIdentity: UserIdentity;

  //#endregion

  //#region PRIVATE PROPERTY
  private dataCloseResult: DialogDataCloseResult = { isLoadAPI: true, isFirstWriteLog: true, lastTimeWriteLog: 0, dataResult: null, priAction: '' };
  private lessonDefault: LessonSelect[] = [];
  private isFirstLoad = true;
  private isFirstLoadClasses = true;
  private isCallAPI: boolean;
  //#endregion

  //#region PUBLIC PROPERTY
  levelManageValue: number = 0;
  userId: number;
  public listOnlineClassroom: VirtualClassRoom[] = [];
  public listOnlineClasses = [];
  public count = 0;
  public countClasses = 0;
  isShowFilter = true;
  isFullScreen = true;
  public displayedColumns = ['stt', 'name', 'lessonName', 'startDate', 'endDate', 'status', 'function'];
  public displayedColumnsClasses = ['stt', 'nameclasses', 'unitname', 'startDate', 'endDate', 'statuslearning', 'tottalstudent', 'joinedstudent', 'missingstudent', 'function'];
  public filterOnlineTeaching = this.formGroup.group({
    startDate: new FormControl(''),
    endDate: new FormControl(''),
    keyword: new FormControl('', Validators.maxLength(50)),
    status: new FormControl(''),
    lessonId: new FormControl(''),
    page: new FormControl(0),
    size: new FormControl(10),
  })

  public filterOnlineClasses = this.formGroup.group({
    divisionId: new FormControl(''),
    schoolId: new FormControl(''),
    fromDate: new FormControl(''),
    toDate: new FormControl(''),
    keyWord: new FormControl('', Validators.maxLength(50)),
    status: new FormControl(''),
    meetingType: new FormControl(''),
    page: new FormControl(0),
    size: new FormControl(10),
    getCount: true
  })


  /* Public property */
  get divisionId() { return this.filterOnlineClasses.get('divisionId'); }
  isEnableDivision = true;
  isFirstLoadDivision = true;

  get schoolId() { return this.filterOnlineClasses.get('schoolId'); }
  isDisableSchoolId = false;
  isFirstLoadSchool = true;

  protected lessonSelect: LessonSelect[] = [];
  public lessonSelectFilterCtrl: FormControl = new FormControl();
  public filteredLessons: ReplaySubject<LessonSelect[]> = new ReplaySubject<LessonSelect[]>(1);

  public objectRequestClone = null;
  public objectRequestCloneClasses = null;
  public isFirstLoadLesson: boolean = true;
  public request: any = null;
  public requestClasses: GetMeetingRoomsRequest;
  public isDisableFilterSubmit = false;
  public isDisableFilterSubmitClasses = false;

  protected _onDestroy = new Subject<void>();
  get lessonId() { return this.filterOnlineTeaching.get('lessonId'); }
  get startDate() { return this.filterOnlineTeaching.get('startDate'); }
  get endDate() { return this.filterOnlineTeaching.get('endDate'); }
  get keyword() { return this.filterOnlineTeaching.get('keyword'); }
  get status() { return this.filterOnlineTeaching.get('status'); }

  get fromDateClasses() { return this.filterOnlineClasses.get('fromDate'); }
  get toDateClasses() { return this.filterOnlineClasses.get('toDate'); }
  get keywordClasses() { return this.filterOnlineClasses.get('keyWord'); }
  get statusClasses() { return this.filterOnlineClasses.get('status'); }

  baseApiUrl = '';
  schoolFilter = {
    divisionId: null
  }
  //#endregion

  //#region CONSTRUCTOR
  constructor(private formGroup: FormBuilder,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    private spinner: NgxSpinnerService,
    private authenService: AuthService,
    private _router: Router,
    private _messageService: MessageService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.userId = authenService.getTokenInfo().userId;
    this.virtualClassRoomEndpoint = new VirtualClassRoomEndpoint();
    this.contentLearnerDetailEndpoint = new ContentLearnerDetailEndpoint();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = authenService.getTokenInfo();
    this.hasAddClassPermission = this.userIdentity.hasPermission(CLSModules.MeetingRoom, CLSPermissions.Add);
    this.hasEditClassPermission = this.userIdentity.hasPermission(CLSModules.MeetingRoom, CLSPermissions.Edit);
    this.hasDeleteClassPermission = this.userIdentity.hasPermission(CLSModules.MeetingRoom, CLSPermissions.Delete);
    this.hasAssignClassPermission = this.userIdentity.hasPermission(CLSModules.MeetingRoom, CLSPermissions.Assign);

    this.baseApiUrl = CLS.getConfig().apiBaseUrl;


  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {
    this.setLevelManage()
    this.requestClasses = {
      divisionId: null,
      schoolId: null,
      fromDate: null,
      toDate: null,
      keyword: null,
      meetingType: null,
      page: 0,
      size: 10,
      sortField: 'CRE',
      sortDirection: 'DESC',
      status: null,
      getCount: true
    }
    this.isFullScreen = false;
    this.objectRequestClone = { ...this.request }
    //this.loadData(this.isFirstLoad);

    this.objectRequestCloneClasses = { ...this.requestClasses }
    this.loadDataClasses(this.isFirstLoadClasses);

    this.isCallAPI = true;
  }

  pageChangeClasses(event?: PageEvent) {
    this.loadDataClasses(this.isFirstLoadClasses);
    this.isFirstLoadLesson = true;
  }

  ngAfterViewInit() {

    this.setInitialValue();
    this.valueSelectSearchChange();
    this.valueControlSelectChange();
    this.lessonId.valueChanges
      .pipe(startWith(this.lessonId?.value),
        pairwise())
      .subscribe(() => {

      }
      )

    // listen for search field value changes 
    this.lessonSelectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {


        this.filterLessons();
      });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  //#endregion

  //#region PUBLIC METHOD
  public onSubmitClasses() {
    this.isFirstLoadClasses = false;
    this.paginatorClasses.pageIndex = 0;
    this.loadDataClasses(this.isFirstLoadClasses);
  }

  openDpFromDate() {
    this.dpFromDate.open();
  }

  openDpFromDateClasses() {
    this.dpFromDateClasses.open();
  }

  openDpToDate() {
    this.dpToDate.open();
  }

  openDpToDateClasses() {
    this.dpToDateClasses.open();
  }

  public onOnlineTeachingHandleClickClasses(element: OnlineClasses) {
    let dataDialog: DataDialog = { onlineClass: element, dataCloseResult: this.dataCloseResult }
    const dialogRef = this.dialog.open(ConfirmTeacherJoinDialogComponent, {
      width: '500px',
      data: dataDialog
    });

    dialogRef.afterClosed().subscribe((result: DialogDataCloseResult) => {
      if (result) {
        this.dataCloseResult = JSON.parse(JSON.stringify(result));
      }
    });
  }

  /* Get division */
  onLessonSelectHandleClick() {
    if (this.isFirstLoadLesson && this.listOnlineClassroom) {
      let lessonId = this.listOnlineClassroom.map(x => { return x.lessonId });
      this.contentLearnerDetailEndpoint.getLessons(lessonId)
        .then(res => {
          this.isFirstLoadLesson = false;
          this.lessonSelect = [];
          this.lessonSelect = res;
          if (this.lessonSelect && !this.lessonSelect.find(x => x.id == 0 || x.id == null)) {
            this.lessonSelect.unshift({ id: null, name: 'Chọn bài giảng' })
          }
          this.filteredLessons.next(this.lessonSelect.slice());
        })
    } else {

    }
  }

  protected filterLessons() {
    if (!this.lessonSelect) {
      return;
    }
    // get the search keyword
    let search = this.lessonSelectFilterCtrl.value;
    if (!search) {
      this.filteredLessons.next(this.lessonSelect.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredLessons.next(
      this.lessonSelect.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }

  clearDate(field: string) {
    if (field === 'from') {
      this.startDate.setValue('');
    } else {
      this.endDate.setValue('');
    }
  }

  clearDateClasses(field: string) {
    if (field === 'from') {
      this.fromDateClasses.setValue('');
    } else {
      this.toDateClasses.setValue('');
    }
  }
  //#endregion

  //#region PRIVATE METHOD
  private loadDataClasses(isFirstLoadClasses: boolean) {
    this.spinner.show();
    this.isDisableFilterSubmitClasses = true;
    if (!isFirstLoadClasses) {
      this.requestClasses = { ...this.filterOnlineClasses.value };
      this.requestClasses.page = this.paginatorClasses.pageIndex + 1;
      this.requestClasses.size = this.paginatorClasses.pageSize;
      if (this.compareObject(this.objectRequestCloneClasses, this.requestClasses)) {
        this.spinner.hide();
        this.isDisableFilterSubmitClasses = false;
        return;
      }
    }
    else {
      this.requestClasses.getCount = true;
    }

    this.reportMeetingRoomEndpoint.getAll(this.requestClasses).then(res => {
      let timeCurentDate = new Date().getTime();
      if (res) {
        res.meetingRooms.items.forEach(element => {
          // NOTLEARN : Chưa dạy - GOINGLEARN : Đang dạy - LEARNED : Đã dạy - DONTLEARN : Không day
          // Column status learning
          // kiểm tra nếu trạng thái ngày bắt đầu lớn hơn ngày hiện tại.
          // statusLearning = chưa dạy
          // if (new Date(element.startDate).getTime() - timeCurentDate > 0) {
          //   element.statusLearning = 'NOTLEARN'
          // } else {
          // trường hợp ngày bắt đầu nhỏ hơn ngày hiện tại và ngày kết thúc lớn hơn ngày hiện tại
          // trường hợp có teacher startDate và teacher EndDate Status learning ='Đã dạy'
          if (element.meetingRoomTeacherStartDate && element.meetingRoomTeacherEndDate) {
            element.statusLearning = 'LEARNED'
          } else {
            if (element.meetingRoomTeacherStartDate) {
              element.statusLearning = 'GOINGLEARN'
            } else {
              if (new Date(element.finishDate).getTime() - timeCurentDate > 0) {
                element.statusLearning = 'NOTLEARN'
              } else {
                element.statusLearning = 'DONTLEARN'
              }
            }
          }
          // }

          // column unit manage
          if (element.schoolId) {
            element.unitManage = element.schoolName;
          } else {
            if (element.divisionId) {
              element.unitManage = element.divisionName
            } else {
              element.unitManage = element.portalName
            }
          }

          let totalStudent = res.totalStudentsRegisMeeting.find(x => x.id == element.id);
          if (totalStudent) {
            element.totalStudent = totalStudent.count;
          } else {
            element.totalStudent = 0;
          }

          let joinedStudent = res.studentsJoined.find(x => x.id == element.id);
          if (joinedStudent) {
            element.joinedStudent = joinedStudent.count;
          } else {
            element.joinedStudent = 0;
          }
        });
        console.log(res);
        this.paginatorClasses._intl.itemsPerPageLabel = "Số dòng trên trang";
        this.listOnlineClasses = res.meetingRooms.items;
        this.countClasses = res.meetingRooms.totalItems
      }
      this.spinner.hide();
    }).catch(err => {
      console.log(err);
      this.spinner.hide();
    })

    this.objectRequestCloneClasses = { ...this.requestClasses }
    this.isDisableFilterSubmitClasses = false;
    this.isFirstLoadClasses = false;
  }

  private compareObject(objectfisrt: any, objectSecond: any) {
    if (JSON.stringify(objectfisrt) == JSON.stringify(objectSecond)) {
      return true;
    }
    return false;
  }

  public onCopyLinkHandleClickClasses(joinURL: string) {
    const create_copy = (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', joinURL);
      e.preventDefault();
    };

    document.addEventListener('copy', create_copy);
    document.execCommand('copy');
    document.removeEventListener('copy', create_copy);

    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
      data: "Đã sao chép liên kết",
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  public onInforRollCallHandleClick(element) {
    this._router.navigateByUrl(`dashboard/manage-report/${element.id}/roll-call-report`)
  }
  public getLinkTemplate(cases: string) {
    if (cases === 'ZOM') {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-huong-dan-giao-vien-tao-lop-hoc-truc-tuyen-zoom-tren-he-thong-cls-k12.pdf`;
      // return `http://localhost:58910/learn/public/files/cdn/template-file/tao-lop-hoc-tuong-tac-tu-zoom.pdf`;
    }
    else if (cases === 'GMT') {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-huong-dan-giao-vien-tao-lop-hoc-truc-tuyen-google-meet-tren-he-thong-cls-k12.pdf`;
      // return `http://localhost:58910/learn/public/files/cdn/template-file/tao-lop-hoc-tuong-tac-tu-google-meet.pdf`;
    }
  }

  public exportExcelHandleClick() {
    if (!this.listOnlineClasses.length) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Không có lớp học để xuất dữ liệu",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    this.spinner.show();
    console.log(this.request);
    const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
    this.reportMeetingRoomEndpoint.exportMeetingRoomExcel(this.requestClasses).then((urlFile) => {
      a.href = urlFile;
      a.download = 'Bao-cao-thong-ke-lop-hoc-tuong-tac.xlsx';
      document.body.appendChild(a);
      a.click();

      document.body.removeChild(a);
      URL.revokeObjectURL(urlFile);
      this.spinner.hide();
    }).catch((err: ErrorResponse) => {
      console.error(err)
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Không có lớp học để xuất dữ liệu",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });

      this.spinner.hide();
    });
  }

  /* Get division */
  onDivisionSelectClicked() {
    if (this.isFirstLoadDivision) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivision = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          console.log('dvsid', this.divisions);
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {

    }
  }

  /* Get school */
  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionId.value;
    if (!this.divisionId.value || this.divisionId.value == 0) {
      divisionIdSelected == null;
    }
    if (this.userIdentity.levelManage == LevelManages.Division) {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchool) {
      this.endpointSchool.getSchoolOptions(this.divisionId.value)
        .then(res => {
          this.isFirstLoadSchool = false;
          this.schools = res;
          this.schoolFilter.divisionId = divisionIdSelected;

          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(

        )
    } else {

    }
  }

  private valueSelectSearchChange() {
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        console.log(this.divisionId.value);
        this.filterDivisions();
      });


    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected setInitialValue() {

    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectDivision && (this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id);
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool && (this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id);
      });
  }

  private setLevelManage() {
    if (this.userIdentity.levelManage == LevelManages.Department) {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == LevelManages.Division) {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else {
      this.levelManageValue = 3;
    }
  }

  private valueControlSelectChange() {
    this.divisionId.valueChanges
      .pipe(startWith(this.divisionId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetSchoolSelect();
          }
        }
      )

    this.schoolId.valueChanges
      .pipe(startWith(this.schoolId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            if (value < 1) {
            } else {

            }
          }
        }
      )
  }

  private onResetSchoolSelect() {
    this.filterOnlineClasses.controls['schoolId'].setValue(0);
    this.isFirstLoadSchool = true;
  }
}

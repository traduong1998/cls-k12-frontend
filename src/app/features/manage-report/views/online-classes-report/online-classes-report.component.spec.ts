import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineClassesReportComponent } from './online-classes-report.component';

describe('OnlineClassesReportComponent', () => {
  let component: OnlineClassesReportComponent;
  let fixture: ComponentFixture<OnlineClassesReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnlineClassesReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineClassesReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { DivisionEndpoint, DivisionOption, ExamEnpoint, GetExamFilterRequests, GradeEndpoint, OrganizeExamEndpoint, SchoolEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/manage/models/grade';
import { ExamSchool } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/ExamSchool';
import { ExamSchoolInfo } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/ExamSchoolInfo';
import { GetListExamSchoolPagingRequest } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/request/get-list-exam-school-paging-request';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { AuthService } from 'src/app/core/services';
import { MessageService } from 'src/app/shared/services/message.service';
import { ExamReportEndpoint } from 'sdk/cls-k12-sdk-js/src/services/report-exam/enpoints';

@Component({
  selector: 'app-list-exam-report',
  templateUrl: './list-exam-report.component.html',
  styleUrls: ['./list-exam-report.component.scss']
})
export class ListExamReportComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'name', 'grade', 'type', 'startTime', 'endTime', 'function'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public statusCtrl: FormControl = new FormControl();
  public examTypeCtrl: FormControl = new FormControl();
  protected divisions: DivisionOption[];
  public divisionCtrl: FormControl = new FormControl();
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  protected schools: SchoolOption[];
  public schoolCtrl: FormControl = new FormControl();
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected _onDestroy = new Subject<void>();
  baseApiUrl: string = "";
  examSchoolPaginationFilter: GetListExamSchoolPagingRequest = {
    keyword: "",
    type: "",
    fromDate: null,
    toDate: null,
    createLevel: "",
    gradeId: null,
    pageNumber: 1,
    pageSize: 10,
    sortField: "NAME",
    sortDirection: "DESC",
    listIds: []
  };

  isFirstLoadDivisions: boolean = true;
  isFirstLoadSchools: boolean = true;
  isFirstLoadAllGrades: boolean = true;
  allGrades: GradeOption[] = []

  userIdentity: UserIdentity;
  organizeExamEndpoint: OrganizeExamEndpoint;
  dataExamReponse: ExamSchool[] = [];
  schoolInfo: ExamSchoolInfo[];
  listExamChoose: number[] = [];
  isFirstLoadGrades: boolean = true;
  getExamFilterRequest: GetExamFilterRequests = {
    pageNumber: 1,
    pageSize: 10,
    divisionId: null,
    schoolId: null,
    gradeId: null,
    subjectId: null,

    fromDate: null,
    toDate: null,
    scope: null,
    type: null,
    statusEdit: 'PUB',
    getCount: true,

    filter: {
      sortDirection: 'DESC',
      sortField: 'CRE',
    },
  }
  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  subjectFilter = {
    gradeId: null
  }

  levelManageValue: number = 0;

  totalItems = 0;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;

  private examEndpoint: ExamEnpoint;
  private reportEndpoint: ExamReportEndpoint;
  private endpointGrade: GradeEndpoint;
  isShowFilter = true;
  isFullScreen = true;
  constructor(private router: Router, private dialog: MatDialog, private _snackBar: MatSnackBar, private _messageService: MessageService, private _authService: AuthService, private spinner: NgxSpinnerService) {
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.examEndpoint = new ExamEnpoint();
    this.reportEndpoint = new ExamReportEndpoint();
    this.organizeExamEndpoint = new OrganizeExamEndpoint();
    this.endpointGrade = new GradeEndpoint();
    this.userIdentity = _authService.getTokenInfo();
    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    }
    else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.getExamFilterRequest.divisionId = this.userIdentity.divisionId;
    }
    else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;

    }

  }

  ngOnInit(): void {
    this.initData();
    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });
  }
  ngAfterViewInit() {
    this.setInitialValue();
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.examSchoolPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.examSchoolPaginationFilter.pageSize = this.paginator.pageSize;
      this.getExamFilterRequest.pageNumber = this.paginator.pageIndex + 1;
      this.getExamFilterRequest.pageSize = this.paginator.pageSize;

      this.initData();
    });
  }
  initData() {
    if (this.levelManageValue == 3) {
      this.getListExamApi();
    } else {
      this.getListExamData();
    }
  }
  getListExamApi() {
    this.isFullScreen = false;
    this.spinner.show();

    this.organizeExamEndpoint.getListExamSchoolInfo().then((res) => {
      this.schoolInfo = res;
      let listIds = this.schoolInfo.map(x => x.examId);
      this.examSchoolPaginationFilter.listIds = listIds;
      if (res.length > 0) {
        this.reportEndpoint.getListExamSchoolPaging(this.examSchoolPaginationFilter)
          .then((res) => {
            if (res) {
              this.dataExamReponse = res.items;
              this.totalItems = res.totalItems;
              if (this.isFirstLoadAllGrades) {
                this.endpointGrade.getGradeOptions(null)
                  .then(res => {
                    this.isFirstLoadAllGrades = false;
                    this.allGrades = res;

                    this.dataExamReponse = this.dataExamReponse.map((exam) => {
                      let grade = this.allGrades.find(x => x.id == exam.gradeId);
                      if (grade != undefined) {
                        exam.gradeName = grade.name;
                      }
                      let school = this.schoolInfo.find(x => x.examId == exam.id);
                      exam.status = school.status;

                      return exam;
                    });

                  })
                  .catch();
              }
              else {
                this.dataExamReponse = this.dataExamReponse.map((exam) => {
                  let grade = this.allGrades.find(x => x.id == exam.gradeId);
                  if (grade != undefined) {
                    exam.gradeName = grade.name;
                  }

                  let school = this.schoolInfo.find(x => x.examId == exam.id);
                  exam.status = school.status;
                  return exam;
                });
              }

              this.loadData(this.dataExamReponse);
            }
            else {
              //to do
            }
          });


      }
      this.spinner.hide();
    }).catch(() => {
      this.spinner.hide();
      this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
    });

  }
  getListExamData() {
    this.isFullScreen = false;
    this.spinner.show();

    this.reportEndpoint.getExams(this.getExamFilterRequest)
      .then((res) => {
        this.dataExamReponse = [];
        res.items.forEach(x => {
          let item: ExamSchool = { id: x.id, name: x.name, type: x.type, status: "", startDate: x.startDate, endDate: x.endDate, gradeId: x.gradeId, gradeName: x.gradeName }
          this.dataExamReponse.push(item);
        });
        this.totalItems = res.totalItems;
        if (this.isFirstLoadAllGrades) {

          // let gradeAll = this.endpointGrade.getGradeOptions(null);
          // let users = this.
          this.endpointGrade.getGradeOptions(null)
            .then(res => {
              this.isFirstLoadAllGrades = false;
              this.allGrades = res;

              this.dataExamReponse = this.dataExamReponse.map((exam) => {
                let grade = res.find(x => x.id == exam.gradeId);
                if (grade != undefined) {
                  exam.gradeName = grade.name;
                }
                return exam;

              });

            })
            .catch();

        }
        else {
          this.dataExamReponse = this.dataExamReponse.map((exam) => {
            let grade = this.allGrades.find(x => x.id == exam.gradeId);
            if (grade != undefined) {
              exam.gradeName = grade.name;
            }
            return exam;
          });
        }
        this.spinner.hide();
      })
      .catch(() => {
        this.spinner.hide();
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
      })
  }

  loadData(data) {
    this.dataExamReponse = []
    this.dataExamReponse = [...this.dataExamReponse, ...data]
  }



  report(examId, gradeId) {
    this.router.navigate(['dashboard/manage-report/exam/', examId, 'grade', gradeId, 'examsubject']);
  }


  chooseExam(event, examId) {
    if (event.checked) {
      this.listExamChoose.push(examId);
    } else {
      let index = this.listExamChoose.findIndex(element => element == examId);
      if (index != -1) {
        this.listExamChoose.splice(index, 1);
      }
    }
  }

  onGradeSelectClicked() {
    let schoolIdSelected;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrades) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch()
    }
  }

  protected setInitialValue() {
    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectGrade != undefined) {
          this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
        }
      });
  }
  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onSubmitFilter() {
    this.getFilterParam();
    this.initData();
  }

  getFilterParam() {
    this.examSchoolPaginationFilter.gradeId = this.gradeCtrl.value;
    this.examSchoolPaginationFilter.type = this.examTypeCtrl.value;
    this.getExamFilterRequest.gradeId = this.gradeCtrl.value;
    this.getExamFilterRequest.type = this.examTypeCtrl.value;
  }
  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }


}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListExamReportComponent } from './list-exam-report.component';

describe('ListExamReportComponent', () => {
  let component: ListExamReportComponent;
  let fixture: ComponentFixture<ListExamReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListExamReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListExamReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

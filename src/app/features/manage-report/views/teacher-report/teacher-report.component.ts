import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { ReplaySubject, Subject as RxSubject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { SchoolOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/school-option-model';
import { UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { DivisionOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/division-option-model';
import { ReportOptionEnpoint } from 'sdk/cls-k12-sdk-js/src/services/report-option/report-option-enpoint';
import { TeacherReportEndpoint } from 'sdk/cls-k12-sdk-js/src/services/report/enpoints';
import { TeacherReport } from 'sdk/cls-k12-sdk-js/src/services/report/models/teacher-report';
import { AuthService } from 'src/app/core/services';


@Component({
  selector: 'app-teacher-report',
  templateUrl: './teacher-report.component.html',
  styleUrls: ['./teacher-report.component.scss']
})
export class TeacherReportComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  teacherName: string;

  divisions: DivisionOptionModel[];
  schools: SchoolOptionModel[];

  public divisionCtrl: FormControl = new FormControl();
  public divisonFilterCtrl: FormControl = new FormControl();
  public filteredDivison: ReplaySubject<DivisionOptionModel[]> = new ReplaySubject<DivisionOptionModel[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  public schoolCtrl: FormControl = new FormControl();
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchool: ReplaySubject<SchoolOptionModel[]> = new ReplaySubject<SchoolOptionModel[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  protected _onDestroy = new RxSubject<void>();


  isFirstLoadDivison: boolean = true;
  isFirstLoadSchools: boolean = true;

  dataTeacher: TeacherReport[];

  displayedColumns: string[] = ['stt', 'TeacherName', 'ID', 'School', 'TotalPendding', 'TotalPublic'];

  teacherReportEndpoint: TeacherReportEndpoint;
  reportOptionEnpoint: ReportOptionEnpoint;
  userIdentity: UserIdentity;
  pageIndex: number;
  totalItem: number;
  // disable
  isDisableDivison = false;
  isDisableSchool = false;
  //selected
  selectedValue: number;

  constructor(private _authService: AuthService) {
    this.reportOptionEnpoint = new ReportOptionEnpoint();
    this.teacherReportEndpoint = new TeacherReportEndpoint();
    this.userIdentity = this._authService.getTokenInfo();
    console.log(" tes", this.userIdentity);
    // phân vùng dữ liêu
    // nếu là ông sở
    if (this.userIdentity.levelManage == "DPM") {
      this.isDisableDivison = false;
      this.isDisableSchool = false;
    }

    // nếu là ông phòng
    if (this.userIdentity.levelManage == "DVS") {
      this.isDisableDivison = true;
      this.divisionCtrl.setValue(this.userIdentity.divisionId);
    }

    // nếu là trương
    if (this.userIdentity.levelManage == "SCH") {
      this.isDisableDivison = true;
      this.isDisableSchool = true;
      this.divisionCtrl.setValue(this.divisionCtrl.value);
      this.schoolCtrl.setValue(this.userIdentity.schoolId);
    }

    this.teacherReportEndpoint.getTeacherReport(this.divisionCtrl.value, this.schoolCtrl.value, null, 1, 10).then(res => {
      this.dataTeacher = res;
      this.totalItem = res.length + 1;
    })
  }

  ngOnInit(): void {
    // listen for search field value changes
    this.divisonFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivision();
      });
    // // listen for search field value changes
    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchool();
      });

  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    // this.paginator.page.subscribe(() => {
    //   this.schoolsPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
    //   this.schoolsPaginationFilter.sizeNumber = this.paginator.pageSize;
    //   this.getSchoolsData();
    // });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected setInitialValue() {
    this.filteredDivison
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectDivision != undefined) {
          this.singleSelectDivision.compareWith = (a: DivisionOptionModel, b: DivisionOptionModel) => a && b && a.id === b.id;
        }
      });

    this.filteredSchool
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectSchool != undefined) {
          this.singleSelectSchool.compareWith = (a: SchoolOptionModel, b: SchoolOptionModel) => a && b && a.id === b.id;
        }
      });
  }

  protected filterDivision() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisonFilterCtrl.value;
    if (!search) {
      this.filteredDivison.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divison
    this.filteredDivison.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchool() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchool.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredSchool.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onDivisonSelectClicked() {
    if (this.isFirstLoadDivison) {
      this.reportOptionEnpoint.getDivisonOption().then((res) => {
        this.isFirstLoadDivison = false;
        this.divisions = res;
        this.filteredDivison.next(this.divisions.slice());
      })
    }
  }

  onSchoolSelectClicked() {
    let divisonIdSelected = this.divisionCtrl.value;
    if (this.isFirstLoadSchools) {
      this.reportOptionEnpoint.getSchoolOption(divisonIdSelected).then(res => {
        this.isFirstLoadSchools = false;
        this.schools = res;
        this.filteredSchool.next(this.schools.slice());
      })
    }
  }

  onChangeSchoolSelected(id: number) {
    this.schoolCtrl.setValue(id);
  }

  onChangeDivisonSelected(id: any) {
    this.divisionCtrl.setValue(id);
  }

  filterReport() {
    this.dataTeacher = [];
    this.teacherReportEndpoint.getTeacherReport(this.divisionCtrl.value, this.schoolCtrl.value, this.teacherName, 1, 10).then(res => {
      this.dataTeacher = res;
    })
  }

  exportExcel() {
    this.teacherReportEndpoint.exportTeacherReport(this.divisionCtrl.value, this.schoolCtrl.value, this.teacherName)
      .then((urlFile) => {
        window.open(urlFile);
      })
  }
  handlePageEvent(page) {
    this.pageIndex = page.pageIndex + 1;
    this.teacherReportEndpoint.getTeacherReport(this.divisionCtrl.value, this.schoolCtrl.value, this.teacherName, this.pageIndex, page.pageSize).then(res => {
      this.dataTeacher = res;

      if (page.pageIndex > page.previousPageIndex) {
        if (res.length >= page.pageSize) {
          this.totalItem = this.totalItem + page.pageSize;
        }
      }
      else {
        if (page.previousPageIndex != 1) {
          this.totalItem = (this.totalItem - page.pageSize) + 1;
        }
      }
    })
  }
}

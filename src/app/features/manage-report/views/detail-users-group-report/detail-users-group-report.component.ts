import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { startWith, pairwise } from 'rxjs/operators';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { DivisionEndpoint, SchoolEndpoint, GradeEndpoint, GroupStudentEndpoint, UserIdentity, DivisionOption } from 'sdk/cls-k12-sdk-js/src';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { UserGroupReportEndpoints } from 'sdk/cls-k12-sdk-js/src/services/manage-report/usersgroups/endpoints/user-group-report-endpoints';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { AssignStudentForGroupRequest } from 'sdk/cls-k12-sdk-js/src/services/user-group/requests/assign-student-for-group-request';
import { GetListStudentForAssignRequest } from 'sdk/cls-k12-sdk-js/src/services/user-group/requests/get-list-student-for-assign-request';
import { StudentForAssignResponse } from 'sdk/cls-k12-sdk-js/src/services/user-group/responses/student-for-assign-response';
import { AuthService } from 'src/app/core/services';

@Component({
  selector: 'app-detail-users-group-report',
  templateUrl: './detail-users-group-report.component.html',
  styleUrls: ['./detail-users-group-report.component.scss']
})
export class DetailUsersGroupReportComponent implements OnInit {
  baseApiUrl = '';
  levelManageValue: number = 0;
  userGroupId: number;
  isListStudentForAssignReady: boolean = false;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointGroupStudent: GroupStudentEndpoint
  usersOfUserGroupEndpoint: UserGroupReportEndpoints;
  userIdentity: UserIdentity;
  assignStudentRequest: AssignStudentForGroupRequest;
  listOldStudentForAssignId: number[];
  isShowFilter = true;
  listStudentForAssignDefault: PaginatorResponse<StudentForAssignResponse> = {
    items: [],
    totalItems: 0
  };
  assignDataSource: PaginatorResponse<StudentForAssignResponse> = this.listStudentForAssignDefault;
  assignSelection = new SelectionModel<StudentForAssignResponse>(true, []);
  assignDisplayedColumns: string[] = ['fullName', 'userName', 'className', 'action'];

  //* Division */
  protected divisions: DivisionOption[];
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  /* School */
  protected schools: SchoolOption[];
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;
  protected grades: GradeOption[];
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;
  protected groupStudents: GroupStudentOption[];
  public groupStudentCtrl: FormControl = new FormControl();
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  filterSignupLecturesForm = this.fb.group({
    divisionId: new FormControl(''),
    schoolId: new FormControl(''),
    gradeId: new FormControl(''),
    groupStudentId: new FormControl(''),
  });

  get divisionId() { return this.filterSignupLecturesForm.get('divisionId'); }
  isEnableDivision = true;
  isFirstLoadDivision = true;

  get schoolId() { return this.filterSignupLecturesForm.get('schoolId'); }
  isDisableSchoolId = false;
  isFirstLoadSchool = true;

  get gradeId() { return this.filterSignupLecturesForm.get('gradeId'); }
  isDisableGradeId = false;
  isFirstLoadGrade = true;

  get groupStudentId() { return this.filterSignupLecturesForm.get('groupStudentId'); }
  isDisableGroupStudentId = true;
  isFirstLoadGroupStudent = true;

  studentsAssignPaginationFilter: GetListStudentForAssignRequest;
  constructor(private fb: FormBuilder, private route: ActivatedRoute, private _authService: AuthService, private _router: Router, private _snackBar: MatSnackBar) {
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
    this.usersOfUserGroupEndpoint = new UserGroupReportEndpoints();
    this.userGroupId = +this.route.snapshot.paramMap.get("id");
    this.listOldStudentForAssignId = [];
    this.studentsAssignPaginationFilter = {
      pageNumber: 1,
      sizeNumber: 10,
      getCount: true,
      userGroupId: this.userGroupId,
      divisionId: 0,
      schoolId: 0,
      gradeId: 0,
      groupStudentId: 0,
      sortDirection: 'DESC',
      sortField: 'CRE'
    }
    this.assignStudentRequest = {
      userGroupId: this.userGroupId,
      listStudents: []
    };
    this.userIdentity = _authService.getTokenInfo();

    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
      // Tài khoản cấp Trường, schoolid fillter mặc định theo id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
      this.filterSignupLecturesForm.controls['schoolId'].setValue(this.userIdentity.schoolId);
      // this.usersPaginationFilter.schoolId = this.userIdentity.schoolId;
    }
    this.getListStudentForAssign();
  }
  ngAfterViewInit(): void {
    this.divisionId?.valueChanges
      .pipe(startWith(this.divisionId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            // reset change
            this.onResetSchoolSelect();
            this.onResetGradeSelect();
            this.onResetGroupStudentSelect();
          }
        }
      )

    this.schoolId?.valueChanges
      .pipe(startWith(this.schoolId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetGradeSelect();
            this.onResetGroupStudentSelect();
            if (value < 1) {
              this.isDisableGroupStudentId = true;
            } else {
              this.isDisableGroupStudentId = false;

            }
          }
        }
      )

    this.gradeId?.valueChanges
      .pipe(startWith(this.gradeId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            this.onResetGroupStudentSelect();
          }
        }
      )

    this.groupStudentId?.valueChanges
      .pipe(startWith(this.gradeId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
          }
        }
      )

    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.studentsAssignPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.studentsAssignPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.getListStudentForAssign();
    });
  }


  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  ngOnInit(): void {
    this.isListStudentForAssignReady = true;
  }

  onSubmitSignLectures() {
    //reset lại list học sinh đã chọn và list học sinh đã ghi danh
    this.assignStudentRequest.listStudents = [];
    this.listOldStudentForAssignId = [];
    this.studentsAssignPaginationFilter.userGroupId = this.userGroupId;
    this.studentsAssignPaginationFilter.divisionId = this.filterSignupLecturesForm.controls['divisionId'].value;
    this.studentsAssignPaginationFilter.schoolId = this.filterSignupLecturesForm.controls['schoolId'].value;
    this.studentsAssignPaginationFilter.gradeId = this.filterSignupLecturesForm.controls['gradeId'].value;
    this.studentsAssignPaginationFilter.groupStudentId = this.filterSignupLecturesForm.controls['groupStudentId'].value;

    this.getListStudentForAssign();
  }

  onDivisionSelectClicked() {
    if (this.isFirstLoadDivision) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivision = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {
    }
  }

  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionId?.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchool) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchool = false;
          this.schools = res;
          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {

    }
  }

  /* Get Grade */
  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolId.value == 0 ? null : this.schoolId.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrade || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrade = false;
          this.grades = res;

          if (this.grades && !this.grades.find(x => x.id == 0)) {
            this.grades.unshift({ id: 0, name: 'Chọn khối' });
          }
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());

        })
        .catch(err => { console.log(err) }
        )
    } else {

    }
  }

  /* Get GroupStudent */
  onGroupStudentSelectClicked() {
    var schoolIdSelected = this.schoolId?.value;
    var gradeIdSelected = this.gradeId?.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if ((this.isDisableGroupStudentId == true && this.levelManageValue !== 3) || !this.isFirstLoadGroupStudent) {
      return;
    } else {
      this.endpointGroupStudent.getGroupStudentOptions(schoolIdSelected, gradeIdSelected)
        .then(res => {
          this.isFirstLoadGroupStudent = false;
          this.groupStudents = res;
          this.filteredGroupStudents.next(this.groupStudents.slice());
        })
        .catch(

        )
    }
  }

  onReportLearnUserButtonHandleClick(id: number) {
    this._router.navigate([`dashboard/manage-report/${this.userGroupId}/detail-users-groups-report/${id}/learn-report`]);
  }
  onCancelAssignStudentTabClick(): void {
    this._router.navigate(['dashboard/user-group']);
  }
  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }
  private onResetSchoolSelect() {
    this.filterSignupLecturesForm.controls['schoolId'].setValue(0);
    this.isFirstLoadSchool = true;
  }

  /* reset change Grade */
  private onResetGradeSelect() {
    this.filterSignupLecturesForm.controls['gradeId'].setValue(0);
    this.isFirstLoadGrade = true;
  }

  private onResetGroupStudentSelect() {
    this.filterSignupLecturesForm.controls['groupStudentId'].setValue(null);
    this.isFirstLoadGroupStudent = true;
  }
  private getListStudentForAssign() {
    this.usersOfUserGroupEndpoint.getListStudentForAssign(this.studentsAssignPaginationFilter)
      .then(res => {

        if (res) {
          this.assignDataSource = res;
        }
      });
  }
}

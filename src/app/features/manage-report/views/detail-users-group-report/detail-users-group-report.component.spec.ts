import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailUsersGroupReportComponent } from './detail-users-group-report.component';

describe('DetailUsersGroupReportComponent', () => {
  let component: DetailUsersGroupReportComponent;
  let fixture: ComponentFixture<DetailUsersGroupReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailUsersGroupReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailUsersGroupReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

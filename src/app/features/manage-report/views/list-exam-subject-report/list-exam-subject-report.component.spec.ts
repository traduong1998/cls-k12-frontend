import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListExamSubjectReportComponent } from './list-exam-subject-report.component';

describe('ListExamSubjectReportComponent', () => {
  let component: ListExamSubjectReportComponent;
  let fixture: ComponentFixture<ListExamSubjectReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListExamSubjectReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListExamSubjectReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

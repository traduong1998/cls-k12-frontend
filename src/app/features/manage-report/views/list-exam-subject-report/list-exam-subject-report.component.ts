import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ExamDetail, ExamEnpoint, OrganizeExamEndpoint, SubjectEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { ExamSubjectOrganize } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/ExamSubjectOrganize';
import { MessageService } from 'src/app/shared/services/message.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { ExamReportEndpoint } from 'sdk/cls-k12-sdk-js/src/services/report-exam/enpoints';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/manage/models/subject';


@Component({
  selector: 'app-list-exam-subject-report',
  templateUrl: './list-exam-subject-report.component.html',
  styleUrls: ['./list-exam-subject-report.component.scss']
})
export class ListExamSubjectReportComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'subject', 'startDate', 'endDate', 'testTime', 'function'];
  reportEndPoint: ExamReportEndpoint;
  data: ExamSubjectOrganize[] = [];
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  examId;
  gradeId;
  routeSub: Subscription;
  isFirstLoadAllGrades: boolean = true;
  allSubject: SubjectOption[] = [];
  examInfo: ExamDetail;
  today = new Date();
  private endpointSubject: SubjectEndpoint;
  private examEndpoint: ExamEnpoint;
  isFullScreen = true;

  // tslint:disable-next-line - Disables all
  constructor(private router: Router, private route: ActivatedRoute, public dialog: MatDialog, private _messageService: MessageService, private spinner: NgxSpinnerService) {
    this.reportEndPoint = new ExamReportEndpoint();
    this.endpointSubject = new SubjectEndpoint();
    this.examEndpoint = new ExamEnpoint();
  }
  ngOnInit(): void {
    console.log('Load danh sách môn thi')
    this.isLoadingResults = true;
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = params['id'];
      this.gradeId = params['gradeId'];
    });

    this.isFullScreen = true;
    this.spinner.show();

    this.examEndpoint.getInfoExam(this.examId)
      .then(res => {
        this.examInfo = res;
      })

    this.reportEndPoint.getListSubjectReport(this.examId).then((res) => {
      this.isLoadingResults = false;
      this.data = res;
      if (this.isFirstLoadAllGrades) {
        this.endpointSubject.getSubjectOptions(null)
          .then(res => {
            this.isFirstLoadAllGrades = false;
            this.allSubject = res;

            this.data = this.data.map((exam) => {
              let subject = this.allSubject.find(x => x.id == exam.subjectId);
              if (subject != undefined) {
                exam.subjectName = subject.name;
              }

              return exam;
            });
          })
          .catch();
      }
      else {
        this.data = this.data.map((exam) => {
          let subject = this.allSubject.find(x => x.id == exam.subjectId);
          if (subject != undefined) {
            exam.subjectName = subject.name;
          }
          return exam;
        });
      }
      this.spinner.hide();
    })
      .catch(() => {
        this.spinner.hide();
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
      });
  }

  ngAfterViewInit(): void {

  }
  getDate(date: any) {
    return new Date(date);
  }
  report(subjectId) {
    this.router.navigate(['dashboard/manage-report/exam/', this.examId, 'grade', this.gradeId, 'examsubject', subjectId])
  }
  previousPage() {
    this.router.navigate(['dashboard/manage-report/exam']);
  }

}

import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject, Subject, Subscription } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { DivisionEndpoint, DivisionOption, SchoolEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { ExamReportEndpoint } from 'sdk/cls-k12-sdk-js/src/services/report-exam/enpoints';
import { ExamReportInfo } from 'sdk/cls-k12-sdk-js/src/services/report-exam/models/exam-report-info';
import { StudentExamReport } from 'sdk/cls-k12-sdk-js/src/services/report-exam/models/request/student-exam-report';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { AuthService } from 'src/app/core/services';
import { MessageService } from 'src/app/shared/services/message.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-exam-report-manage',
  templateUrl: './exam-report-manage.component.html',
  styleUrls: ['./exam-report-manage.component.scss']
})
export class ExamReportManageComponent implements OnInit, AfterViewInit {
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;


  examReportEndpoint: ExamReportEndpoint;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  protected _onDestroy = new Subject<void>();

  examId: number;
  gradeId: number;
  subjectId: number;
  routeSub: Subscription;
  examReportFilter = this.fb.group({
    divisonId: new FormControl(''),
    schoolId: new FormControl(''),
    keyword: new FormControl(''),
    status: new FormControl(''),
    subjectId: 0,
    examId: 0,
    page: new FormControl(1),
    size: new FormControl(10),
    sortField: 'ALP',
    sortDirection: 'ASC',
  })
  layout
  examReportInfo: ExamReportInfo = {
    name: "",
    subjectName: "",
    total: 0,
    totalCompleted: 0,
    totalInfringe: 0,
    totalSuspended: 0,
    testTime: 0,
  };
  studentExams: StudentExamReport[] = [];
  totalItems = 0
  public statusCtrl: FormControl = new FormControl();

  levelManageValue: number = 0;
  protected divisions: DivisionOption[];
  protected schools: SchoolOption[];

  /** control for the selected division */
  public divisionCtrl: FormControl = new FormControl();
  public divisionFilterCtrl: FormControl = new FormControl();

  public schoolCtrl: FormControl = new FormControl();
  public schoolFilterCtrl: FormControl = new FormControl();

  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);

  isFirstLoadDivisions = true;
  isFirstLoadSchools = true;

  schoolFilter = {
    divisionId: null
  }
  userIdentity: UserIdentity;
  displayedColumns: string[] = ['stt', 'fullname', 'email', 'schoolName', 'status', 'infingeTimes', 'scoreDecuted', 'realScore'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  isShowFilter = true;
  isFullScreen = true;

  constructor(private router: Router, private route: ActivatedRoute, private fb: FormBuilder, private messageService: MessageService, private _authService: AuthService, private spinner: NgxSpinnerService) {
    this.examReportEndpoint = new ExamReportEndpoint();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: null });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: null });

    this.userIdentity = _authService.getTokenInfo();
    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      if (this.userIdentity.divisionId != null) {
        this.schoolFilter.divisionId = this.userIdentity.divisionId;
      }
      this.levelManageValue = 3;
    }
  }
  ngOnInit(): void {

    this.routeSub = this.route.params.subscribe(params => {
      this.examId = +params['id'];
      this.subjectId = +params['subjectId'];
      this.gradeId = +params['gradeId'];
      this.examReportFilter.controls['subjectId'].setValue(this.subjectId);
      this.examReportFilter.controls['examId'].setValue(this.examId);
    });

    this.examReportFilter.controls['page'].setValue(1);
    this.examReportFilter.controls['size'].setValue(10);
    this.getData();

    // listen for search field value changes
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });

    // listen for search field value changes
    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    this.setInitialValue();
    this.examReportEndpoint.getExamReportInfo(this.examId, this.subjectId)
      .then((res) => {
        this.examReportInfo = res;
      })
      .catch(() => {
        this.messageService.failureCallback("có lỗi xảy ra lúc lấy thông tin kì thi");
      })
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.examReportFilter.controls['page'].setValue(this.paginator.pageIndex + 1);
      this.examReportFilter.controls['size'].setValue(this.paginator.pageSize);
      this.getData();

    });

  }

  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        // triggers initializing the selection according to the initial value of
        // the form control (i.e. _initializeSelection())
        // this needs to be done after the filteredDivisions are loaded initially
        // and after the mat-option elements are available
        this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id;
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id;
      });
  }

  onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionCtrl.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    console.log(divisionIdSelected);
    if (this.isFirstLoadSchools || this.schoolFilter.divisionId != divisionIdSelected) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchools = false;
          this.schools = res;
          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.schoolFilter.divisionId = divisionIdSelected;
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onChangeDivisionSelected(id: any) {
    this.resetSchoolSelectCtrl();
    this.examReportFilter.controls['divisonId'].setValue(id);
  }
  onChangeSchoolSelected(id: any) {
    this.examReportFilter.controls['schoolId'].setValue(id);
  }
  resetSchoolSelectCtrl() {
    this.schools = [];
    this.isFirstLoadSchools = true;
    this.schoolCtrl.setValue(undefined);
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice())
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  exportExcel() {
    this.isFullScreen = true;
    this.spinner.show();
    this.examReportEndpoint.exportExcelStudentOfExam(this.examReportFilter.value)
      .then((urlFile) => {
        window.open(urlFile);
        this.spinner.hide();

      })
      .catch((ex) => {
        this.spinner.hide();
        this.messageService.failureCallback("Có lỗi xảy ra trong quá trình tải file vui lòng thử lại!")
      });
  }
  onSubmit() {
    this.getData();
  }
  getData() {
    this.isFullScreen = false;
    this.spinner.show();
    this.examReportEndpoint.getStudentOfExamPaging(this.examReportFilter.value)
      .then((res) => {
        this.studentExams = res.items;
        this.totalItems = res.totalItems;
        this.spinner.hide();
      }).catch(() => {
        this.spinner.hide();
        this.messageService.failureCallback('Có lỗi xảy ra lúc lấy danh sách thí sinh');
      });
  }
  previousPage() {
    this.router.navigate(['dashboard/manage-report/exam/', this.examId, 'grade', this.gradeId, 'examsubject'])
  }
  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }
}

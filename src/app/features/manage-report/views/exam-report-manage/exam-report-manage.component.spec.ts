import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamReportManageComponent } from './exam-report-manage.component';

describe('ExamReportManageComponent', () => {
  let component: ExamReportManageComponent;
  let fixture: ComponentFixture<ExamReportManageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExamReportManageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamReportManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-study-history-report',
  templateUrl: './study-history-report.view.html',
  styleUrls: ['./study-history-report.view.scss']
})
export class StudyHistoryReportComponent implements OnInit {

  public currentTab: string = 'lesson';
  constructor() {
  }

  ngOnInit(): void {
  }

  changeTab(tabNumber) {
    switch (tabNumber) {
      case 0:
        this.currentTab = 'lesson';
        break;
      default:
        this.currentTab = 'exam';
        break;
    }
  }
}

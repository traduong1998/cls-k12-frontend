import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { ActivatedRoute } from '@angular/router';
import { ReplaySubject, Subject } from 'rxjs';
import { pairwise, startWith, take, takeUntil } from 'rxjs/operators';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { DivisionEndpoint, DivisionOption, GradeEndpoint, GroupStudentEndpoint, LevelManages, SchoolEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';
import { ReportMeetingRoomsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/report/meeting-rooms';
import { GetStudentLogsPagingResponse, MeetingRoomStudentLogsResponse } from 'sdk/cls-k12-sdk-js/src/services/meeting-room/models/meeting-room-student-logs-response';
import { GetStudentLogsPagingRequest } from 'sdk/cls-k12-sdk-js/src/services/report/meeting-rooms/requests/get-student-logs-paging-request';
import { MeetingRoomTeacherLogsModel } from 'sdk/cls-k12-sdk-js/src/services/meeting-room/models/meeting-room-teacher-logs-model';
import { isNullOrEmpty } from 'src/app/shared/helpers/validation.helper';
import { NgxSpinnerService } from 'ngx-spinner';
import { ExcelMeetingRoomStudentLogsRequest } from 'sdk/cls-k12-sdk-js/src/services/report/meeting-rooms/requests/excel-meeting-room-student-logs-request';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';

const dataTableDefaultValue: PaginatorResponse<MeetingRoomStudentLogsResponse> = {
  totalItems: 0,
  items: []
}

@Component({
  selector: 'app-roll-card-classroom-report',
  templateUrl: './roll-card-classroom-report.component.html',
  styleUrls: ['./roll-card-classroom-report.component.scss']
})
export class RollCardClassroomReportComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  displayedColumns: string[] = ['stt', 'name', 'userName', 'schoolName', 'gradeName', 'groupStudentName', 'joinDate'];
  baseApiUrl = '';
  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT
  unitManage: string = ""
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointGroupStudent: GroupStudentEndpoint
  userIdentity: UserIdentity;

  reportMeetingRoomLogsEndpoint: ReportMeetingRoomsEndpoint
  //#endregion

  //#region PRIVATE PROPERTY
  request: GetStudentLogsPagingRequest = {
    divisionId: null,
    schoolId: null,
    gradeId: null,
    groupStudentId: null,
    keyword: '',
    getCount: true,
    page: 0,
    size: 10,
    sortField: 'CRE',
    sortDirection: 'DESC'
  }

  excelRequest: ExcelMeetingRoomStudentLogsRequest = {
    meetingRoomId: null,
    divisionId: null,
    schoolId: null,
    gradeId: null,
    groupStudentId: null,
    keyword: '',
    getCount: true,
    page: 0,
    size: 10,
    sortField: 'CRE',
    sortDirection: 'DESC'
  }
  //#endregion

  //#region PUBLIC PROPERTY
  joinDateTeacherTitleString: string = '';
  joinDatesTitleString: string = '';
  isFirstOnInit: boolean = true;
  totalStudent: number = 0;
  jointedStudent: number = 0;
  meetingTeacherLogsDetail: MeetingRoomTeacherLogsModel
  routeDivisionId: number;
  routeSchoolId: number;
  meetingRoomId: number
  isLoadData: boolean = false;
  studentLogs: PaginatorResponse<MeetingRoomStudentLogsResponse> = dataTableDefaultValue
  flexValue: number = 0;
  /* Division */
  protected divisions: DivisionOption[];
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  /* School */
  protected schools: SchoolOption[];
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  /* Grade */
  protected grades: GradeOption[];
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  /* GroupStudent */
  protected groupStudents: GroupStudentOption[];
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;
  protected _onDestroy = new Subject<void>();

  filterStudentLogsForm: FormGroup

  filterStudentLogsFormClonce;
  public isShowFilter = true;
  levelManageValue: number = 0;

  /* Public property */
  get divisionId() { return this.filterStudentLogsForm.get('divisionId'); }
  isEnableDivision = true;
  isFirstLoadDivision = true;

  get schoolId() { return this.filterStudentLogsForm.get('schoolId'); }
  isDisableSchoolId = false;
  isFirstLoadSchool = true;

  get gradeId() { return this.filterStudentLogsForm.get('gradeId'); }
  isDisableGradeId = false;
  isFirstLoadGrade = true;

  get groupStudentId() { return this.filterStudentLogsForm.get('groupStudentId'); }
  isDisableGroupStudentId = true;
  isFirstLoadGroupStudents = true;

  get keyword() { return this.filterStudentLogsForm.get('keyword'); }
  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  groupStudentFilter = {
    schoolId: null,
    gradeId: null
  }
  //#endregion



  //#region CONSTRUCTOR
  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private _authService: AuthService,
    private configService: AppConfigService,
    private spinner: NgxSpinnerService,
    private _snackBar: MatSnackBar
  ) {
    this.filterStudentLogsForm = this.fb.group({
      divisionId: new FormControl(''),
      schoolId: new FormControl(''),
      gradeId: new FormControl(''),
      groupStudentId: new FormControl(''),
      keyword: new FormControl(''),
      page: 0,
      size: 10,
      getCount: true,
      sortDirection: 'DESC',
      sortField: 'CRE',
    });
  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {
    this.getValueRoute();
    this.setValueRequest()
    this.initialEndpoint();
    this.setLevelManage();
    this.getTeacherLogsData();
    this.getStudentLogsData();
  }
  ngAfterViewInit() {

    this.setInitialValue();

    this.valueControlSelectChange();

    this.valueSelectSearchChange();
    this.filterStudentLogsFormClonce
      = JSON.parse(JSON.stringify(this.filterStudentLogsForm.value));

    this.setDefaultValueSelect()
  }
  //#endregion

  //#region PUBLIC METHOD

  /** Submit filter */
  public onSubmit() {
    this.paginator.pageIndex = 0;
    this.paginator.pageSize = 10;
    if (this.filterStudentLogsForm.valid &&
      !compareTwoObject(this.filterStudentLogsFormClonce, this.filterStudentLogsForm.value)) {
      this.request = {
        meetingRoomId: this.meetingRoomId,
        divisionId: this.divisionId.value,
        schoolId: this.schoolId.value,
        gradeId: this.gradeId.value,
        groupStudentId: this.groupStudentId.value,
        keyword: this.keyword.value,
        page: 0,
        size: 10,
        sortField: 'CRE',
        sortDirection: 'DESC'
      }
      this.getStudentLogsData();
    }
  }

  public onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }

  pageChange(event?: PageEvent) {
    this.request.page = this.paginator.pageIndex;
    this.request.size = this.paginator.pageSize;
    this.studentLogs.items = []
    this.studentLogs.totalItems = 0
    this.isLoadData = false;
    this.getStudentLogsData();
  }
  //#region SELECTSCLICK
  /* Get division */
  onDivisionSelectClicked() {
    if (this.isFirstLoadDivision) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivision = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          console.log('dvsid', this.divisions);
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {

    }
  }

  /* Get school */
  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionId.value;
    if (!this.divisionId.value || this.divisionId.value == 0) {
      divisionIdSelected == null;
    }
    if (this.userIdentity.levelManage == LevelManages.Division) {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchool) {
      this.endpointSchool.getSchoolOptions(this.divisionId.value)
        .then(res => {
          this.isFirstLoadSchool = false;
          this.schools = res;
          this.schoolFilter.divisionId = divisionIdSelected;

          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(

        )
    } else {

    }
  }
  /* Get Grade */
  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolId.value;
    if (!this.schoolId.value || this.schoolId.value == 0) {
      schoolIdSelected = null
    }
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrade || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrade = false;
          this.grades = res;

          if (this.grades && !this.grades.find(x => x.id == 0)) {
            this.grades.unshift({ id: 0, name: 'Chọn khối' });
          }
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());

        })
        .catch(err => { console.log(err) }
        )
    } else {

    }
  }

  onGroupStudentSelectClicked() {
    var schoolIdSelected = this.schoolId?.value;
    var gradeIdSelected = this.gradeId?.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if ((this.isDisableGroupStudentId == true && this.levelManageValue !== 3) || !this.isFirstLoadGroupStudents) {
      return;
    } else {
      this.endpointGroupStudent.getGroupStudentOptions(schoolIdSelected, gradeIdSelected)
        .then(res => {
          this.isFirstLoadGroupStudents = false;
          this.groupStudents = res;
          if (this.groupStudents && !this.groupStudents.find(x => x.id == 0)) {
            this.groupStudents.unshift({ id: 0, name: 'Chọn lớp' });
          }
          this.filteredGroupStudents.next(this.groupStudents.slice());
        })
        .catch(

        )
    }
  }
  //#endregion

  //#region combobox
  protected setInitialValue() {

    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectDivision && (this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id);
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool && (this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id);
      });

    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGrade && (this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id);
      });
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }


  //#endregion
  //#endregion

  //#region PRIVATE METHOD

  /* reset change School */
  private onResetSchoolSelect() {
    this.filterStudentLogsForm.controls['schoolId'].setValue(0);
    this.isFirstLoadSchool = true;
  }

  /* reset change Grade */
  private onResetGradeSelect() {
    this.filterStudentLogsForm.controls['gradeId'].setValue(0);
    this.isFirstLoadGrade = true;
  }
  private onResetGroupStudentSelect() {
    this.filterStudentLogsForm.controls['groupStudentId'].setValue(null);
    this.isFirstLoadGroupStudents = true;
  }

  /**Value select search change */
  private valueSelectSearchChange() {
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        console.log(this.divisionId.value);
        this.filterDivisions();
      });


    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });
  }

  /**Value control select  change */
  private valueControlSelectChange() {
    this.divisionId.valueChanges
      .pipe(startWith(this.divisionId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetSchoolSelect();
            this.onResetGradeSelect();
          }
        }
      )

    this.schoolId.valueChanges
      .pipe(startWith(this.schoolId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetGradeSelect();
            this.onResetGroupStudentSelect();
            if (value < 1) {
              this.isDisableGroupStudentId = true;
            } else {
              this.isDisableGroupStudentId = false;

            }
          }
        }
      )

    this.gradeId.valueChanges
      .pipe(startWith(this.gradeId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            if (value < 1) {
            } else {
              this.onResetGroupStudentSelect()
            }
          }
        }
      )

    this.groupStudentFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudents();
      });
  }

  private getValueRoute() {
    this.routeDivisionId = +this.route.snapshot.paramMap.get("divisionId") ? +this.route.snapshot.paramMap.get("divisionId") : null;
    this.routeSchoolId = +this.route.snapshot.paramMap.get("schoolId") ? +this.route.snapshot.paramMap.get("schoolId") : null;
    this.meetingRoomId = +this.route.snapshot.paramMap.get("meetingRoomId");
    this.divisionId.setValue(this.routeDivisionId);
    this.schoolId.setValue(this.routeSchoolId);
    console.log(this.routeDivisionId);
    console.log(this.routeSchoolId);

  }
  private initialEndpoint() {
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = this._authService.getTokenInfo();

    this.reportMeetingRoomLogsEndpoint = new ReportMeetingRoomsEndpoint();

  }

  private setLevelManage() {
    if (this.userIdentity.levelManage == LevelManages.Department) {
      this.levelManageValue = 1;
      this.flexValue = 25;
    } else if (this.userIdentity.levelManage == LevelManages.Division) {
      this.levelManageValue = 2;
      this.flexValue = 50;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else {
      this.levelManageValue = 3;
      this.flexValue = -25;

      // Tài khoản cấp Trường, schoolid fillter mặc định theo  id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
      this.filterStudentLogsForm.controls['schoolId'].setValue(this.userIdentity.schoolId);
      // this.usersPaginationFilter.schoolId = this.userIdentity.schoolId;
    }
  }

  private getTeacherLogsData() {
    this.joinDateTeacherTitleString = '';
    this.reportMeetingRoomLogsEndpoint.getByMeetingRoomIdAndUserId(this.meetingRoomId)
      .then((res: MeetingRoomTeacherLogsModel) => {
        if (res.divisionId) {
          if (res.schoolId) {
            this.unitManage = res.schoolName
          } else {
            this.unitManage = res.divisionName
          }
        } else {
          if (res.schoolId) {
            this.unitManage = res.schoolName
          } else {
            this.unitManage = this.configService.getConfig().unit.name
          }
        }
        let date = []
        let joinDate = res.joinDate?.split(';');
        if (joinDate) {
          let indexBreakLine = 1;
          for (let index = 0; index < joinDate.length; index++) {
            if (joinDate[index]) {
              let dateConverted = new Date((joinDate[index].split('/').join('-') + 'Z'));
              joinDate[index] && date.push(dateConverted)
              this.joinDateTeacherTitleString += (this.getDateToString(dateConverted))
              indexBreakLine++;
              if (indexBreakLine > 3) {
                this.joinDateTeacherTitleString.trimRight();
                this.joinDateTeacherTitleString += '\r\n'
                indexBreakLine = 1;
              }
            }
          }
          res.joinDates = date;
          console.log('this.joinDateTeacherTitleString', this.joinDateTeacherTitleString);
        }
        this.meetingTeacherLogsDetail = res;

      });
  }

  private getStudentLogsData() {
    this.joinDatesTitleString = ''
    this.request.meetingRoomId = this.meetingRoomId
    this.reportMeetingRoomLogsEndpoint
      .GetStudentLogs(this.request)
      .then((res: GetStudentLogsPagingResponse) => {
        this.studentLogs = res.data;
        let dateAdds: Date[] = [];
        let studentTitleString = ''
        this.studentLogs.items.forEach(element => {
          if (element.joinDate) {
            let dates = element.joinDate.split(';');
            let indexBreakLine = 1;
            for (let index = 0; index < dates.length; index++) {
              if (dates[index]) {
                debugger;
                let dateReplace = dates[index].split('/').join('-') + 'Z';
                let date = new Date(dateReplace);
                dateAdds.push(date);
                if (!isNullOrEmpty(studentTitleString)) {
                  studentTitleString = studentTitleString + ` `;
                }
                studentTitleString += this.getDateToString(date);
                indexBreakLine++;
                if (indexBreakLine > 3) {
                  studentTitleString += '\r\n'
                  indexBreakLine = 1;
                }
              }
            }
            element.joinDateStudentTitleString = studentTitleString;
            element.joinDateLocalTime = dateAdds;
            dateAdds = [];
            studentTitleString = '';
            console.log("element.joinDateLocalTime ", element.joinDateLocalTime);
            this.excelRequest = { ...this.request };

          }
        });
        if (this.isFirstOnInit) {
          this.totalStudent = res.data.totalItems;
          this.jointedStudent = res.studentJoinMeetingRoomCount ? res.studentJoinMeetingRoomCount : 0;
          this.isFirstOnInit = false;
          this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
        }
        this.asignBeforeRequestToCurrentRequest();
        this.isLoadData = true;
      });
  }


  private asignBeforeRequestToCurrentRequest() {
    this.filterStudentLogsFormClonce = JSON.parse(JSON.stringify(this.filterStudentLogsForm.value))
  }

  private setDefaultValueSelect() {
    let user = this._authService.getTokenInfo();
    if (user.levelManage == LevelManages.Department) {
      if (this.routeDivisionId) {
        console.log(this.divisionId.value)
        this.filterStudentLogsForm.controls['divisionId'].patchValue(this.routeDivisionId);
        this.onDivisionSelectClicked();
      }

      if (this.routeSchoolId) {
        this.filterStudentLogsForm.controls['schoolId'].patchValue(this.routeSchoolId);
        this.onSchoolSelectClicked()
      }
    }
    if (user.levelManage == LevelManages.Division) {
      if (this.routeSchoolId) {
        this.schoolId.setValue(this.routeSchoolId)
        this.onSchoolSelectClicked()
        console.log(this.divisionId.value)
      }
    }
    this.routeDivisionId
    this.routeSchoolId
  }

  private setValueRequest() {
    this.request.divisionId = this.routeDivisionId
    this.request.schoolId = this.routeSchoolId
  }

  private getDateToString(date: Date) {
    let hour = this.checkZero(date.getHours());
    let minute = this.checkZero(date.getMinutes());
    let year = this.checkZero(date.getFullYear());
    let month = this.checkZero(date.getMonth() + 1);
    let day = this.checkZero(date.getDate());
    return ` ` + hour + `:` + minute + ` ` + day + `/` + month + '/' + year + ';';

  }

  checkZero(data) {
    if (data < 10) {
      data = "0" + data;
    }
    return data;
  }
  public exportExcelHandleClick() {
    if (!this.studentLogs.items.length) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Không có học sinh để xuất dữ liệu",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    this.spinner.show();
    const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
    console.log(this.request);
    this.reportMeetingRoomLogsEndpoint.exportStudentLogExcel(this.meetingRoomId, this.request).then((urlFile) => {
      a.href = urlFile;
      a.download = 'Bao-cao-thong-ke-lop-hoc-tuong-tac.xlsx';
      document.body.appendChild(a);
      a.click();

      document.body.removeChild(a);
      URL.revokeObjectURL(urlFile);
      this.spinner.hide();
    }).catch((err: ErrorResponse) => {
      console.error(err)
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Không có học sinh để xuất dữ liệu",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });

      this.spinner.hide();
    });
  }
}

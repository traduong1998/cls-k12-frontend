import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RollCardClassroomReportComponent } from './roll-card-classroom-report.component';

describe('RollCardClassroomReportComponent', () => {
  let component: RollCardClassroomReportComponent;
  let fixture: ComponentFixture<RollCardClassroomReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RollCardClassroomReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RollCardClassroomReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

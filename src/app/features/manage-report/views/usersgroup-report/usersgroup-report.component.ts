import { SelectionModel } from '@angular/cdk/collections';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, Subject } from 'rxjs';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { UserGroupReportEndpoints } from 'sdk/cls-k12-sdk-js/src/services/manage-report/usersgroups/endpoints/user-group-report-endpoints';
import { UserGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-group/endpoints/user-group-endpoint';
import { UserGroup } from 'sdk/cls-k12-sdk-js/src/services/user-group/models/user-group';
import { GetListUserGroupRequest } from 'sdk/cls-k12-sdk-js/src/services/user-group/requests/get-list-user-group-request';
import { CreateUserGroupComponent } from 'src/app/features/group-user/components/create-user-group/create-user-group.component';
import { EditUserGroupComponent } from 'src/app/features/group-user/components/edit-user-group/edit-user-group.component';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-usersgroup-report',
  templateUrl: './usersgroup-report.component.html',
  styleUrls: ['./usersgroup-report.component.scss']
})
export class UsersgroupReportComponent implements OnInit {

  private userGroupEndpoint: UserGroupReportEndpoints;
  public isLoadingResults: boolean = true;
  public userGroupPagination: PaginatorResponse<UserGroup> = { totalItems: 0, items: [] };
  public displayedColumns: string[] = ['stt', 'name', 'userGroupCode', 'countStudent', 'function'];
  public getListUserGroupRequest: GetListUserGroupRequest;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  selection = new SelectionModel<UserGroup>(true, []);

  constructor(private dialog: MatDialog, private _snackBar: MatSnackBar, private _router: Router, private spinner: NgxSpinnerService, private changeDetectorRefs: ChangeDetectorRef) {
    this.spinner.show()
    this.userGroupEndpoint = new UserGroupReportEndpoints();
    this.getListUserGroupRequest = {
      keyWord: null,
      page: 1,
      size: 10
    }
    this.userGroupEndpoint.getListUserGroup(
      this.getListUserGroupRequest.keyWord,
      this.getListUserGroupRequest.page,
      this.getListUserGroupRequest.size)
      .then((res) => {
        this.userGroupPagination = res;
        console.log(res.items)
        this.isLoadingResults = false;
        this.spinner.hide();
      })
  }
  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.getListUserGroupRequest.page = this.paginator.pageIndex + 1;
      this.getListUserGroupRequest.size = this.paginator.pageSize;
      this.getListUserGroup();
    });
  }

  ngOnInit(): void {
  }

  getListUserGroup(): void {
    this.spinner.show();
    this.userGroupEndpoint.getListUserGroup(
      this.getListUserGroupRequest.keyWord,
      this.getListUserGroupRequest.page,
      this.getListUserGroupRequest.size)
      .then((res) => {
        this.userGroupPagination = res;
        this.isLoadingResults = false;
        this.spinner.hide();
      })
  }

  assignUser(id: number): void {
    this._router.navigate([`dashboard/manage-report/${id}/detail-users-groups-report`]);
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersgroupReportComponent } from './usersgroup-report.component';

describe('UsersgroupReportComponent', () => {
  let component: UsersgroupReportComponent;
  let fixture: ComponentFixture<UsersgroupReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsersgroupReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersgroupReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

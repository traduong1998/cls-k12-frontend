import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject, Subject as RxSubject } from 'rxjs';
import { take, takeUntil, startWith, pairwise } from 'rxjs/operators';
import { SchoolOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/school-option-model';
import { TeacherOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/teacher-option-model';
import { UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { DivisionOption, GradeEndpoint, SchoolEndpoint, GroupStudentEndpoint } from 'cls-k12-sdk-js/src';
import { ReportLessonModel } from 'sdk/cls-k12-sdk-js/src/services/report-lesson/models/report-lesson-model';
import { ReportLessonEnpoint } from 'sdk/cls-k12-sdk-js/src/services/report-lesson/report-lesson-enpoint';
import { ClassOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/class-option-model';
import { DivisionOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/division-option-model';
import { GradeOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/grade-option-model';
import { SubjectOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/subject-option-model';
import { ReportOptionEnpoint } from 'sdk/cls-k12-sdk-js/src/services/report-option/report-option-enpoint';
import { StudentReportEndpoint } from 'sdk/cls-k12-sdk-js/src/services/report-student/enpoints';
import { StudentLearnLessonModel } from 'sdk/cls-k12-sdk-js/src/services/report-student/models/student-learn-lesson-model';
import { StudentReport } from 'sdk/cls-k12-sdk-js/src/services/report-student/models/student-report';
import { TeacherReport } from 'sdk/cls-k12-sdk-js/src/services/report/models/teacher-report';
import { AuthService, AppConfigService } from 'src/app/core/services';
import { PaginatorResponse } from 'cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { ReportStudentLearnPagingRequest } from 'cls-k12-sdk-js/src/services/report-lesson/models/ReportStudentLearnPagingRequest';
import { DivisionEndpoint } from 'cls-k12-sdk-js/src';
import { SchoolOption } from 'cls-k12-sdk-js/src/services/school/models/School';
import { GradeOption } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { GroupStudentOption } from 'cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { NgxSpinnerService } from 'ngx-spinner';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';

const reportStudentLearnPaginationDefault: PaginatorResponse<StudentLearnLessonModel> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-student-learn-lesson-report',
  templateUrl: './student-learn-lesson-report.component.html',
  styleUrls: ['./student-learn-lesson-report.component.scss']
})
export class StudentLearnLessonReportComponent implements OnInit {
  baseApiUrl = '';
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointGroupStudent: GroupStudentEndpoint

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  filterSignupLecturesForm = this.fb.group({
    divisionId: new FormControl(0),
    schoolId: new FormControl(0),
    gradeId: new FormControl(0),
    groupStudentId: new FormControl(0),
  });

  public studentName: FormControl = new FormControl('');

  /* Public property */
get divisionId() { return this.filterSignupLecturesForm.get('divisionId'); }
isEnableDivision = true;
isFirstLoadDivision = true;

get schoolId() { return this.filterSignupLecturesForm.get('schoolId'); }
isDisableSchoolId = false;
isFirstLoadSchool = true;

get gradeId() { return this.filterSignupLecturesForm.get('gradeId'); }
isDisableGradeId = false;
isFirstLoadGrade = true;

get groupStudentId() { return this.filterSignupLecturesForm.get('groupStudentId'); }
isDisableGroupStudentId = true;
isFirstLoadGroupStudent = true;

public schoolYearCtrl: FormControl = new FormControl('2021');

//* Division */
protected divisions: DivisionOption[];
public divisionFilterCtrl: FormControl = new FormControl();
public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
@ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

/* School */
protected schools: SchoolOption[];
public schoolFilterCtrl: FormControl = new FormControl();
public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
@ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

/* grades */
protected grades: GradeOption[];
public gradeFilterCtrl: FormControl = new FormControl();
public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
@ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

protected groupStudents: GroupStudentOption[];
public groupStudentCtrl: FormControl = new FormControl();
public groupStudentFilterCtrl: FormControl = new FormControl();
public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
@ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;

protected _onDestroy = new RxSubject < void> ();

  displayedColumns: string[] = ['stt','StudentName', 'Id','SchoolName', 'Grade', 'Class','registerDate','startTime','endTime', "Status", "function",'AssessmentLevelName'];
  reportStudentLearnPagination: PaginatorResponse<StudentLearnLessonModel> = reportStudentLearnPaginationDefault;
  reportStudentLearnPaginationFilter: ReportStudentLearnPagingRequest = {
    pageNumber: 1,
    sizeNumber: 10,
    getCount: true,
    sortDirection: 'DESC',
    sortField: 'CRE',
  }


  dataLesson: ReportLessonModel[];
  dataStudentLearnLesson: StudentLearnLessonModel[];

  reportLessonEnpoint: ReportLessonEnpoint;
  reportOptionEnpoint: ReportOptionEnpoint;

  studentReportEndpoint: StudentReportEndpoint;
  levelManageValue: number = 0;
  isDisableDivison = false;
  isDisableSchool = false;
  userIdentity: UserIdentity;
  lessonId: number;
  currentSchoolYear: number;
  constructor(private router: Router, private spinner: NgxSpinnerService, private route: ActivatedRoute, private fb: FormBuilder, private _authService: AuthService,private configService: AppConfigService) {
    //get params leson
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });

    this.route.params.subscribe(params => {
      this.lessonId = +params['id'];
      console.log("sss", this.lessonId);
    })
    this.reportOptionEnpoint = new ReportOptionEnpoint();
    this.userIdentity = this._authService.getTokenInfo();
    this.reportLessonEnpoint = new ReportLessonEnpoint();

    this.studentReportEndpoint = new StudentReportEndpoint();
    // phân vùng dữ liệu
    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
      // Tài khoản cấp Trường, schoolid fillter mặc định theo id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
      this.filterSignupLecturesForm.controls['schoolId'].setValue(this.userIdentity.schoolId);
      // this.usersPaginationFilter.schoolId = this.userIdentity.schoolId;
    }
    
    this.currentSchoolYear = this.configService.getConfig().unit.currentSchoolYear;
  }

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }

  ngOnInit(): void {
      this.loadReport();
      this.schoolYearCtrl.setValue(this.currentSchoolYear + '');
  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.reportStudentLearnPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.reportStudentLearnPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.loadReport();
    });

    this.divisionId?.valueChanges
      .pipe(startWith(this.divisionId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            // reset change
            this.onResetSchoolSelect();
            this.onResetGradeSelect();
            this.onResetGroupStudentSelect();
          }
        }
      )

    this.schoolId?.valueChanges
      .pipe(startWith(this.schoolId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetGradeSelect();
            this.onResetGroupStudentSelect();
            if (value < 1) {
              this.isDisableGroupStudentId = true;
            } else {
              this.isDisableGroupStudentId = false;

            }
          }
        }
      )

    this.gradeId?.valueChanges
      .pipe(startWith(this.gradeId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            this.onResetGroupStudentSelect();
          }
        }
      )

    this.groupStudentId?.valueChanges
      .pipe(startWith(this.gradeId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
          }
        }
      )

// listen for search field value changes 
this.divisionFilterCtrl.valueChanges
.pipe(takeUntil(this._onDestroy))
.subscribe(() => {
  console.log(this.divisionId.value);
  this.filterDivisions();
});


this.schoolFilterCtrl.valueChanges
.pipe(takeUntil(this._onDestroy))
.subscribe(() => {
  this.filterSchools();
});

this.gradeFilterCtrl.valueChanges
.pipe(takeUntil(this._onDestroy))
.subscribe(() => {
  this.filterGrades();
});

this.groupStudentFilterCtrl.valueChanges
.pipe(takeUntil(this._onDestroy))
.subscribe(() => {
  this.filterGroupStudents();
});

this.setInitialValue();
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }
  
  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }

  private onResetSchoolSelect() {
    this.filterSignupLecturesForm.controls['schoolId'].setValue(0);
    this.isFirstLoadSchool = true;
    // this.filterLessonForm.controls['schoolFilterCtrl'].setValue(0);
  }

  /* reset change Grade */
  private onResetGradeSelect() {
    this.filterSignupLecturesForm.controls['gradeId'].setValue(0);
    // this.filterLessonForm.controls['gradeFilterCtrl'].setValue(null);
    this.isFirstLoadGrade = true;
  }

  private onResetGroupStudentSelect() {
    this.filterSignupLecturesForm.controls['groupStudentId'].setValue(null);
    // this.filterLessonForm.controls['gradeFilterCtrl'].setValue(null);
    this.isFirstLoadGroupStudent = true;
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectDivision && (this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id);
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool && (this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id);
      });

    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGrade && (this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id);
      });

    this.filteredGroupStudents
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGroupStudent && (this.singleSelectGroupStudent.compareWith = (a: GroupStudentOption, b: GroupStudentOption) => a && b && a.id === b.id);
      });

  }

  /* Get division */
  onDivisionSelectClicked() {
    if (this.isFirstLoadDivision) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivision = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {
    }
  }

  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionId.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchool) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchool = false;
          this.schools = res;
          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {

    }
  }

  /* Get Grade */
  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolId.value == 0 ? null : this.schoolId.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrade || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrade = false;
          this.grades = res;

          if (this.grades && !this.grades.find(x => x.id == 0)) {
            this.grades.unshift({ id: 0, name: 'Chọn khối' });
          }
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());

        })
        .catch(err => { console.log(err) }
        )
    } else {

    }
  }

  /* Get GroupStudent */
  onGroupStudentSelectClicked() {

    var schoolIdSelected = this.schoolId.value;
    var gradeIdSelected = this.gradeId.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if ((this.isDisableGroupStudentId == true && this.levelManageValue !== 3) || !this.isFirstLoadGroupStudent) {
      return;
    } else {
      this.endpointGroupStudent.getGroupStudentOptions(schoolIdSelected, gradeIdSelected)
        .then(res => {
          this.isFirstLoadGroupStudent = false;
          this.groupStudents = res;
          if (this.groupStudents && !this.groupStudents.find(x => x.id == 0)) {
            this.groupStudents.unshift({ id: 0, name: 'Chọn lớp' });
          }
          this.filteredGroupStudents.next(this.groupStudents.slice());
        })
        .catch(

        )
    }
  }

  filterReport() {
    this.reportStudentLearnPaginationFilter.divisionId = this.divisionId.value;
    this.reportStudentLearnPaginationFilter.schoolId = this.schoolId.value;
    this.reportStudentLearnPaginationFilter.gradeId = this.gradeId.value;
    this.reportStudentLearnPaginationFilter.classId = this.groupStudentId.value;
    this.reportStudentLearnPaginationFilter.schoolYear = this.schoolYearCtrl.value;
    this.reportStudentLearnPaginationFilter.studentName = this.studentName.value;
    this.reportStudentLearnPaginationFilter.lessonId = this.lessonId;
    this.reportStudentLearnPaginationFilter.pageNumber = 1;
    this.paginator.pageIndex = 0;
    this.studentReportEndpoint.getStudentLearnLessonReport(this.reportStudentLearnPaginationFilter).then(res => {
      this.reportStudentLearnPagination = res;
      res.items.forEach((item) => {
        if (new Date(item.registrationDate).getFullYear() == 1) {
          item.registrationDate = null;
        }

        if (new Date(item.startDate).getFullYear() == 1) {
          item.startDate = null;
        }

        if (new Date(item.completionDate).getFullYear() == 1) {
          item.completionDate = null;
        }
      })
    })
  }

  loadReport() {
    this.reportStudentLearnPaginationFilter.lessonId = this.lessonId;
    this.studentReportEndpoint.getStudentLearnLessonReport(this.reportStudentLearnPaginationFilter).then(res => {
      this.reportStudentLearnPagination = res;
      res.items.forEach((item) => {
        debugger;
        if (new Date(item.registrationDate).getFullYear() == 1) {
          item.registrationDate = null;
        }

        if (new Date(item.startDate).getFullYear() == 1) {
          item.startDate = null;
        }

        if (new Date(item.completionDate).getFullYear() == 1) {
          item.completionDate = null;
        }
      })
    })
  }

  exportExcel() {
    this.spinner.show(undefined, {
      type: SpinnerType.ballSpin,
      size: 'medium',
      bdColor: 'rgba(0, 0, 0, 0.2)',
      color: '#43a047',
      fullScreen: false
    });
    let date = new Date()
    let ticks = date.getTime()
    let fileName: string;
    let objectUrl: string;
    const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
    this.studentReportEndpoint.exportStudentLearnLessonReport(this.divisionId.value, this.schoolId.value, this.gradeId.value, this.groupStudentId.value, this.lessonId, this.schoolYearCtrl.value, this.studentName.value)
      .then((urlFile) => {
        objectUrl = urlFile;
      fileName = 'Bao-cao-hoc-sinh-hoc-bai-giang.xlsx';
      a.href = objectUrl;
      a.download = fileName;
      document.body.appendChild(a);
      a.click();
  
      document.body.removeChild(a);
      URL.revokeObjectURL(objectUrl);
      this.spinner.hide();
      })
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentLearnLessonReportComponent } from './student-learn-lesson-report.component';

describe('StudentLearnLessonReportComponent', () => {
  let component: StudentLearnLessonReportComponent;
  let fixture: ComponentFixture<StudentLearnLessonReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentLearnLessonReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentLearnLessonReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

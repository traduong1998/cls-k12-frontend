import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { ReplaySubject, Subject as RxSubject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { StudentReportEndpoint } from 'sdk/cls-k12-sdk-js/src/services/report-student/enpoints';
import { StudentReport } from 'sdk/cls-k12-sdk-js/src/services/report-student/models/student-report';
import { ReportOptionEnpoint } from 'sdk/cls-k12-sdk-js/src/services/report-option/report-option-enpoint';
import { DivisionOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/division-option-model';
import { SchoolOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/school-option-model';
import { GradeOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/grade-option-model';
import { ClassOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/class-option-model';
import { MatPaginator } from '@angular/material/paginator';
import { ColumnDisplay } from 'sdk/cls-k12-sdk-js/src/services/report-student/enums/column-display';
import { ApexAxisChartSeries, ApexChart, ApexNonAxisChartSeries, ApexTitleSubtitle, ApexXAxis } from 'ng-apexcharts';
import { PaginatorResponse } from 'cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { ReportStudentPagingRequest } from 'cls-k12-sdk-js/src/services/report-lesson/models/ReportStudentPagingRequest';

const reportStudentPaginationDefault: PaginatorResponse<StudentReport> = {
  items: [],
  totalItems: 0
};
@Component({
  selector: 'app-customize-report',
  templateUrl: './customize-report.component.html',
  styleUrls: ['./customize-report.component.scss']
})
export class CustomizeReportComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  studentName: string;

  divisions: DivisionOptionModel[];
  schools: SchoolOptionModel[];
  grades: GradeOptionModel[];
  classes: ClassOptionModel[];

  public schoolYearCtrl: FormControl = new FormControl('2020');

  public divisionCtrl: FormControl = new FormControl();
  public divisonFilterCtrl: FormControl = new FormControl();
  public filteredDivison: ReplaySubject<DivisionOptionModel[]> = new ReplaySubject < DivisionOptionModel[] > (1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  public schoolCtrl: FormControl = new FormControl();
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchool: ReplaySubject<SchoolOptionModel[]> = new ReplaySubject < SchoolOptionModel[] > (1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;


  public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOptionModel[]> = new ReplaySubject < GradeOptionModel[] > (1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;


  public classCtrl: FormControl = new FormControl();
  public classFilterCtrl: FormControl = new FormControl();
  public filteredClass: ReplaySubject<ClassOptionModel[]> = new ReplaySubject < ClassOptionModel[] > (1);
  @ViewChild('singleSelectClass', { static: true }) singleSelectClass: MatSelect;

  protected _onDestroy = new RxSubject < void> ();

  isFirstLoadDivison: boolean = true;
  isFirstLoadSchools: boolean = true;
  isFirstLoadGrades: boolean = true;
  isFirstLoadClass: boolean = true;

  dataStudent: StudentReport[];

  displayedColumns: string[] = ['stt', 'StudentName', 'ID', 'TotalLessonAgg', 'TotalLessonReg', 'TotalLessonApp'];
  reportStudentPagination: PaginatorResponse<StudentReport> = reportStudentPaginationDefault;
  reportStudentPaginationFilter: ReportStudentPagingRequest = {
    pageNumber: 1,
    sizeNumber: 10,
    getCount: true,
    sortDirection: 'DESC',
    sortField: 'CRE',
  }
  studentReportEndpoint: StudentReportEndpoint;
  reportOptionEnpoint: ReportOptionEnpoint;

  userIdentity: UserIdentity;
  pageIndex: number
  totalItem: number;
  isDisableDivison = false;
  isDisableSchool = false;

  public cbUsername: FormControl = new FormControl();
  public cbFullName: FormControl = new FormControl();
  public cbTotalCourseRequest: FormControl = new FormControl();
  public cbTotalCourseRegisted: FormControl = new FormControl();
  public cbTotalCourseApproved: FormControl = new FormControl();

  public showUsername: boolean = true;
  public showFullName: boolean = true;
  public showTotalCourseRequest: boolean = true;
  public showTotalCourseRegisted: boolean = true;
  public showTotalCourseApproved: boolean = true;

  public reportTypeCtrl: FormControl = new FormControl();
  reportTypeValue: number = 1;

  series: ApexAxisChartSeries | ApexNonAxisChartSeries
  chart: ApexChart;
  title: ApexTitleSubtitle;
  xaxis: ApexXAxis;
  currentSchoolYear: number;
  constructor(private fb: FormBuilder, private _authService: AuthService, private configService: AppConfigService) {

    this.userIdentity = this._authService.getTokenInfo();
    this.studentReportEndpoint = new StudentReportEndpoint();
    this.reportOptionEnpoint = new ReportOptionEnpoint();
    // phân vùng dữ liêu
    // nếu là ông sở
    if (this.userIdentity.levelManage == "DPM") {
      this.isDisableDivison = false;
      this.isDisableSchool = false;
    }

    //this.userIdentity.levelManage='DVS'
    // nếu là ông phòng

    // this.isDisableDivison = true;
    if (this.userIdentity.levelManage == "DVS") {
      this.isDisableDivison = true;
      this.divisionCtrl.setValue(this.userIdentity.divisionId);
    }

    // nếu là trương
    if (this.userIdentity.levelManage == "SCH") {
      this.isDisableDivison = true;
      this.isDisableSchool = true;
      this.divisionCtrl.setValue(this.divisionCtrl.value);
      this.schoolCtrl.setValue(this.userIdentity.schoolId);
    }

    
    this.cbUsername.setValue(true);
    this.cbFullName.setValue(true);
    this.cbTotalCourseRequest.setValue(true);
    this.cbTotalCourseRegisted.setValue(true);
    this.cbTotalCourseApproved.setValue(true);
    this.reportTypeCtrl.setValue(1 + '');
    this.currentSchoolYear = this.configService.getConfig().unit.currentSchoolYear;
  }

  ngOnInit(): void {
    // listen for search field value changes
    this.divisonFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivision();
      });
    // // listen for search field value changes
    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchool();
      });

    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrade();
      });

    this.series = [
      {
        name: "Tổng số",
        data: []
      }
    ]
    this.chart = {
      height: 350,
      type: "bar"
    }
    this.title = {
      text: ""
    }
    this.xaxis = {
      categories: ["Tổng bài giảng yêu cầu học", "Tổng số bài giảng đăng ký", "Tổng số bài giảng được duyệt"]
    }
    this.loadReport();
    this.schoolYearCtrl.setValue(this.currentSchoolYear + '');
  }
  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.reportStudentPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.reportStudentPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.loadReport();

    });
  }
  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected setInitialValue() {
    this.filteredDivison
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        if (this.singleSelectDivision != undefined) {
          this.singleSelectDivision.compareWith = (a: DivisionOptionModel, b: DivisionOptionModel) => a && b && a.id === b.id;
        }
      });

    this.filteredSchool
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        if (this.singleSelectSchool != undefined) {
          this.singleSelectSchool.compareWith = (a: SchoolOptionModel, b: SchoolOptionModel) => a && b && a.id === b.id;
        }
      });
    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        if (this.singleSelectGrade != undefined) {
          this.singleSelectGrade.compareWith = (a: GradeOptionModel, b: GradeOptionModel) => a && b && a.id === b.id;
        }
      });
  }

  protected filterDivision() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisonFilterCtrl.value;
    if (!search) {
      this.filteredDivison.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divison
    this.filteredDivison.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchool() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchool.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredSchool.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGrade() {

    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onDivisonSelectClicked() {
    if (this.isFirstLoadDivison) {
      this.reportOptionEnpoint.getDivisonOption().then(res => {
        this.isFirstLoadDivison = false;
        this.divisions = res;
        this.filteredDivison.next(this.divisions.slice());
      })
    }
  }

  onSchoolSelectClicked() {
    let divisonIdSelected = this.divisionCtrl.value;
    if (this.isFirstLoadSchools) {
      this.reportOptionEnpoint.getSchoolOption(divisonIdSelected).then(res => {
        this.isFirstLoadSchools = false;
        this.schools = res;
        this.filteredSchool.next(this.schools.slice());
      })
    }
  }

  onGradeSelectClicked() {
    let schoolId = this.schoolCtrl.value;
    if (this.isFirstLoadGrades) {
      this.reportOptionEnpoint.getGradeOption(schoolId)
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch()
    }
  }

  onClassSelectClicked() {
    let schoolId = this.schoolCtrl.value;
    let gradeId = this.gradeCtrl.value;
    if (this.isFirstLoadClass) {
      this.reportOptionEnpoint.getClassOption(schoolId, gradeId)
        .then(res => {
          this.isFirstLoadClass = false;
          this.classes = res;
          this.filteredClass.next(this.classes.slice());
        })
        .catch()
    }
  }
  onClassSchoolSelected(id) {

  }

  onChangeGradeSelected(id: any) {
  }
  onChangeSchoolSelected(id: number) {
    this.schoolCtrl.setValue(id);
  }

  onChangeDivisonSelected(id: any) {
    this.divisionCtrl.setValue(id);
  }

  resetGradeSelectCtrl() {
    this.grades = [];
    this.isFirstLoadDivison = true;
    this.divisionCtrl.setValue(undefined);
  }

  setColumnsDisplay() {
    this.showUsername = this.cbUsername.value;
    this.showFullName = this.cbFullName.value;
    this.showTotalCourseRequest = this.cbTotalCourseRequest.value;
    this.showTotalCourseRegisted = this.cbTotalCourseRegisted.value;
    this.showTotalCourseApproved = this.cbTotalCourseApproved.value;
    //displayedColumns: string[] = ['stt', 'ID', 'StudentName', 'TotalLessonAgg', 'TotalLessonReg', 'TotalLessonApp'];
    // set lại danh sách column hiển thị
    this.displayedColumns = ['stt']
    if (this.showUsername) {
      this.displayedColumns.push('ID');
    }
    if (this.showFullName) {
      this.displayedColumns.push('StudentName');
    }
    if (this.showTotalCourseRequest) {
      this.displayedColumns.push('TotalLessonAgg');
    }
    if (this.showTotalCourseRegisted) {
      this.displayedColumns.push('TotalLessonReg');
    }
    if (this.showTotalCourseApproved) {
      this.displayedColumns.push('TotalLessonApp');
    }
  }

  filterReport() {
    this.setColumnsDisplay();
    // Lấy hết ra để vẽ biểu đồ, sau này bỏ luôn cái này, làm tạm để demo
    this.studentReportEndpoint.getStudentChartDataReport(this.schoolCtrl.value, this.divisionCtrl.value, this.gradeCtrl.value, this.classCtrl.value, this.schoolYearCtrl.value, this.studentName, 1, 10000).then(res => {
      this.series = [
        {
          name: "Tổng số",
          data: [res.totalLessonAssign, res.totalLessonRegister, res.totalLessonApproval]
        }
      ]
    })

    this.studentReportEndpoint.getStudentReport(this.reportStudentPaginationFilter).then(res => {
      this.reportStudentPagination = res;
    })

  }

  loadReport() {
    this.studentReportEndpoint.getStudentReport(this.reportStudentPaginationFilter).then(res => {
      this.reportStudentPagination = res;
    })

  }


  getColumnsDisplay() {
    let columnsDisplay: string[] = [];

    if (this.cbUsername.value) {
      columnsDisplay.push(ColumnDisplay.UserName);
    }
    if (this.cbFullName.value) {
      columnsDisplay.push(ColumnDisplay.FullName);
    }
    if (this.cbTotalCourseRequest.value) {
      columnsDisplay.push(ColumnDisplay.RequestCourse);
    }
    if (this.cbTotalCourseRegisted.value) {
      columnsDisplay.push(ColumnDisplay.RegistedCourse);
    }
    if (this.cbTotalCourseApproved.value) {
      columnsDisplay.push(ColumnDisplay.ApprovedCourse);
    }

    return columnsDisplay;
  }

  exportExcel() {
    let columnsDisplay = this.getColumnsDisplay();
    this.studentReportEndpoint.exportStudentReport(this.schoolCtrl.value, this.divisionCtrl.value, this.gradeCtrl.value, this.classCtrl.value, this.schoolYearCtrl.value, this.studentName, columnsDisplay)
      .then((urlFile) => {
        window.open(urlFile);
      })
  }
}

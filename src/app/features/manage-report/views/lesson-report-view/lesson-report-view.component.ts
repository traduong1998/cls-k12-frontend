import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { ReplaySubject, Subject as RxSubject } from 'rxjs';
import { take, takeUntil, startWith, pairwise } from 'rxjs/operators';
import { SchoolOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/school-option-model';
import { TeacherOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/teacher-option-model';
import { UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { ReportLessonModel } from 'sdk/cls-k12-sdk-js/src/services/report-lesson/models/report-lesson-model';
import { ReportLessonEnpoint } from 'sdk/cls-k12-sdk-js/src/services/report-lesson/report-lesson-enpoint';
import { DivisionOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/division-option-model';
import { GradeOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/grade-option-model';
import { SubjectOptionModel } from 'sdk/cls-k12-sdk-js/src/services/report-option/models/subject-option-model';
import { ReportOptionEnpoint } from 'sdk/cls-k12-sdk-js/src/services/report-option/report-option-enpoint';
import { StudentReportEndpoint } from 'sdk/cls-k12-sdk-js/src/services/report-student/enpoints';
import { StudentReport } from 'sdk/cls-k12-sdk-js/src/services/report-student/models/student-report';
import { TeacherReportEndpoint } from 'sdk/cls-k12-sdk-js/src/services/report/enpoints';
import { TeacherReport } from 'sdk/cls-k12-sdk-js/src/services/report/models/teacher-report';
import { AuthService } from 'src/app/core/services';
import { PaginatorResponse } from 'cls-k12-sdk-js/src/core/api/responses/PaginatorResponse';
import { ReportLearnPagingRequest } from 'cls-k12-sdk-js/src/services/report-lesson/models/ReportLearnPagingRequest';
import { GradeEndpoint } from 'sdk/cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';
import { ChaptersEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/chaptersEndpoint';
import { DivisionOption } from 'sdk/cls-k12-sdk-js/src/services/division/models/Division';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { DivisionEndpoint, SchoolEndpoint, UserEndpoint, LevelManages } from 'cls-k12-sdk-js/src';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { SubjectsEndpoint } from 'cls-k12-sdk-js/src/services/lesson/endpoints/subjectsEndpoint';
import { Teacher } from 'src/app/features/learn/intefaces/teacher';
import { NgxSpinnerService } from 'ngx-spinner';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';


const reportLearnPaginationDefault: PaginatorResponse<ReportLessonModel> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-lesson-report-view',
  templateUrl: './lesson-report-view.component.html',
  styleUrls: ['./lesson-report-view.component.scss']
})
export class LessonReportViewComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /*  //#region FromGroup  */
  filterLessonForm = this.fb.group({
    divisionId: new FormControl(0),
    schoolId: new FormControl(0),
    gradeId: new FormControl(0),
    subjectId: new FormControl(0),
    userId: new FormControl(),
    lessonName: new FormControl(),
  });

  levelManageValue: number = 0;
  public schoolYearCtrl: FormControl = new FormControl('2021');

  /* Division */
  protected divisions: DivisionOption[];
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  /* School */
  protected schools: SchoolOption[];
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  /* School */
  protected grades: GradeOption[];
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  /* Subject */
  protected subjects: SubjectOption[];
  public subjectFilterCtrl: FormControl = new FormControl();
  public filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  @ViewChild('singleSelectSubject', { static: true }) singleSelectSubject: MatSelect;

  /* Teacher */
  protected teachers: Teacher[];
  public teacherFilterCtrl: FormControl = new FormControl();
  public filteredTeachers: ReplaySubject<Teacher[]> = new ReplaySubject<Teacher[]>(1);
  @ViewChild('singleSelectTeacher', { static: true }) singleSelectTeacher: MatSelect;

  protected _onDestroy = new RxSubject<void>();
  /* Public property */
  get divisionId() { return this.filterLessonForm.get('divisionId'); }
  isEnableDivision = true;
  isFirstLoadDivision = true;

  get schoolId() { return this.filterLessonForm.get('schoolId'); }
  isDisableSchoolId = false;
  isFirstLoadSchool = true;

  get gradeId() { return this.filterLessonForm.get('gradeId'); }
  isDisableGradeId = false;
  isFirstLoadGrade = true;

  get subjectId() { return this.filterLessonForm.get('subjectId'); }
  isDisableSubjectId = true;
  isFirstLoadSubject = true;

  get userId() { return this.filterLessonForm.get('userId'); }
  isDisableUserId = true;

  get lessonName() { return this.filterLessonForm.get('lessonName'); }

  endpointUser: UserEndpoint;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointSubject: SubjectsEndpoint;

  /* load teacher commbox */
  isShowteacherCombobox = false;

  displayedColumns: string[] = ['stt', 'LessonName', 'TeacherName', 'TotalStudentRegister', 'TotalStudentComplete', "function"];
  reportLearnPagination: PaginatorResponse<ReportLessonModel> = reportLearnPaginationDefault;
  reportLearnPaginationFilter: ReportLearnPagingRequest = {
    pageNumber: 0,
    sizeNumber: 10,
    getCount: true,
    sortDirection: 'DESC',
    sortField: 'CRE',
  }


  teacherReportEndpoint: TeacherReportEndpoint;
  //fakse
  studentReportEndpoint: StudentReportEndpoint;
  //real
  reportLessonEnpoint: ReportLessonEnpoint;
  reportOptionEnpoint: ReportOptionEnpoint;

  isDisableDivison = false;
  isDisableSchool = false;
  userIdentity: UserIdentity;

  constructor(private route: Router, private spinner: NgxSpinnerService, private fb: FormBuilder, private _authService: AuthService) {
    this.teacherReportEndpoint = new TeacherReportEndpoint();
    this.reportOptionEnpoint = new ReportOptionEnpoint();
    this.userIdentity = this._authService.getTokenInfo();
    this.studentReportEndpoint = new StudentReportEndpoint()
    this.reportLessonEnpoint = new ReportLessonEnpoint();
    this.endpointUser = new UserEndpoint();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: null });
    this.endpointSchool = new SchoolEndpoint();
    this.endpointGrade = new GradeEndpoint();
    this.endpointSubject = new SubjectsEndpoint()
    // phân vùng dữ liệu
    // nếu là ông sở
    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
      this.isDisableUserId = false;
      // Tài khoản cấp Trường, schoolid fillter mặc định theo id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
    }
  }

  isShowFilter = true;

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  groupStudentFilter = {
    schoolId: null,
    gradeId: null
  }

  ngOnInit(): void {
    this.loadReport();
  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.reportLearnPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.reportLearnPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.loadReport();
    });

    this.divisionId.valueChanges
      .pipe(startWith(this.divisionId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            // reset change
            this.onResetSchoolSelect();
            this.onResetGradeSelect();
            this.onResetSubjectSelect();
            this.onReseTeacherSelect();

            this.isDisableSubjectId = true;
            this.isDisableUserId = true;
          }
        }
      )

    this.schoolId.valueChanges
      .pipe(startWith(this.schoolId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetGradeSelect();
            this.onResetSubjectSelect();
            this.onReseTeacherSelect();

            this.isDisableSubjectId = true;
            if (value < 1) {
              this.isDisableUserId = true;
            } else {
              this.isDisableUserId = false;
            }
          }
        }
      )

    this.gradeId.valueChanges
      .pipe(startWith(this.gradeId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
            this.onResetSubjectSelect();
            this.onReseTeacherSelect();

            if (value < 1) {
              this.isDisableSubjectId = true;
            } else {
              this.isDisableSubjectId = false;
            }
          }
        }
      )

    this.subjectId.valueChanges
      .pipe(startWith(this.subjectId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
            this.onReseTeacherSelect();
            if (value < 1) {
            } else {
            }
          }
        });

    // listen for search field value changes 
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        console.log(this.divisionId.value);
        this.filterDivisions();
      });


    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    this.subjectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSubjects();
      });

    this.teacherFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterTeachers();
      });

    this.setInitialValue();

    
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectDivision && (this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id);
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool && (this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id);
      });

    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGrade && (this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id);
      });

    this.filteredSubjects
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSubject && (this.singleSelectSubject.compareWith = (a: SubjectOption, b: SubjectOption) => a && b && a.id === b.id);
      });
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchools() {
    debugger;
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSubjects() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.subjectFilterCtrl.value;
    if (!search) {
      this.filteredSubjects.next(this.subjects.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSubjects.next(
      this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterTeachers() {
    
    // get the search keyword
    let search = this.teacherFilterCtrl.value;
    if (!search) {
      this.teachers && this.filteredTeachers.next(this.teachers.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredTeachers.next(
      this.teachers.filter(teacher => teacher.name.toLowerCase().indexOf(search) > -1)
    );
  }

  /* Get division */
  onDivisionSelectClicked() {
    if (this.isFirstLoadDivision) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivision = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          console.log('dvsid', this.divisions);
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {

    }
  }

  /* Get school */
  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionId.value;
    if (this.userIdentity.levelManage == LevelManages.Division) {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchool) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchool = false;
          this.schools = res;
          this.schoolFilter.divisionId = divisionIdSelected;

          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(

        )
    } else {

    }
  }

  /* Get Grade */
  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolId.value == 0 ? null : this.schoolId.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrade || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrade = false;
          this.grades = res;

          if (this.grades && !this.grades.find(x => x.id == 0)) {
            this.grades.unshift({ id: 0, name: 'Chọn khối' });
          }
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());

        })
        .catch(err => { console.log(err) }
        )
    } else {

    }
  }

  /* Get Subject */
  onSubjectSelectClicked() { // TODO : system service was not have this API
    if (this.isDisableSubjectId == true || this.gradeId.value == null || this.gradeId.value == 0 || !this.isFirstLoadSubject) {
      return;
    } else {
      this.endpointSubject.getSubjectByGradeIdOptions(this.gradeId.value).then(res => {
        this.subjects = res;
        this.isFirstLoadSubject = false;
        if (this.subjects && !this.subjects.find(x => x.id == 0)) {
          this.subjects.unshift({ id: 0, name: 'Chọn môn' });
        }
        this.filteredSubjects.next(this.subjects.slice());
      }).catch(err => {
        // sthis.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
    }
  }

  /* Get Teacher */
  onTeacherSelectClicked() { // TODO : system service was not have this API
    var sschoolId = 0;
    if (this.schoolId.value || this.gradeFilter.schoolId != 0) {
      if(this.gradeFilter.schoolId != 0 && this.gradeFilter.schoolId != null){
        sschoolId = this.gradeFilter.schoolId;
      }else{
        sschoolId = this.schoolId.value;
      }
      this.endpointUser
        .getTeacherOfSchoolOptions(sschoolId)
        .then(res => {
          this.teachers = res;
          if (this.teachers && !this.teachers.find(x => x.id == 0)) {
            this.teachers.unshift({ id: 0, name: 'Chọn giáo viên' });
          }
          this.filteredTeachers.next(this.teachers.slice())
        })
        .catch(err => {
          // s  this.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
        });
    }
  }

  /* reset change School */
  private onResetSchoolSelect() {
    this.filterLessonForm.controls['schoolId'].setValue(0);
    this.isFirstLoadSchool = true;
  }

  /* reset change Grade */
  private onResetGradeSelect() {
    this.filterLessonForm.controls['gradeId'].setValue(0);
    this.isFirstLoadGrade = true;
  }

  /* reset change Subject */
  private onResetSubjectSelect() {
    this.filterLessonForm.controls['subjectId'].setValue(0);
    this.isFirstLoadSubject = true;
  }

  private onReseTeacherSelect() {
    this.filterLessonForm.controls['userId'].setValue(0);
  }


  filterReport() {
    this.reportLearnPaginationFilter.divisionId = this.divisionId.value;
    this.reportLearnPaginationFilter.schoolId = this.schoolId.value;
    this.reportLearnPaginationFilter.gradeId = this.gradeId.value;
    this.reportLearnPaginationFilter.subjectId = this.subjectId.value;
    this.reportLearnPaginationFilter.teacherId = this.userId.value;
    this.reportLearnPaginationFilter.lessonName = this.lessonName.value;
    this.reportLearnPaginationFilter.pageNumber = 1;
    this.paginator.pageIndex = 0;
    this.reportLessonEnpoint.getLessonReport(this.reportLearnPaginationFilter).then(res => {
      this.reportLearnPagination = res;
    })
  }

  loadReport() {
    this.reportLessonEnpoint.getLessonReport(this.reportLearnPaginationFilter).then(res => {
      this.reportLearnPagination = res;
    })
  }

  exportExcel() {
    this.spinner.show(undefined, {
      type: SpinnerType.ballSpin,
      size: 'medium',
      bdColor: 'rgba(0, 0, 0, 0.2)',
      color: '#43a047',
      fullScreen: false
    });
    let date = new Date()
    let ticks = date.getTime()
    let fileName: string;
    let objectUrl: string;
    const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
    this.reportLessonEnpoint.exportLessonReport(this.divisionId.value, this.schoolId.value, this.gradeId.value, this.subjectId.value, this.userId.value, this.lessonName.value)
      .then((urlFile) => {
        objectUrl = urlFile;
        fileName = 'Bao-cao-thong-ke-bai-giang.xlsx';
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();
    
        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
        this.spinner.hide();
      })
  }

  showDetail(lessonId) {
    this.route.navigate(["dashboard/manage-report/lessons/", lessonId])
  }
}

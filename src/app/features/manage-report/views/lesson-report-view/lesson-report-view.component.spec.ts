import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonReportViewComponent } from './lesson-report-view.component';

describe('LessonReportViewComponent', () => {
  let component: LessonReportViewComponent;
  let fixture: ComponentFixture<LessonReportViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LessonReportViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonReportViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

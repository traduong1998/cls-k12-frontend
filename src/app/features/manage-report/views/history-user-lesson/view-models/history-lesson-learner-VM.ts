
import { LessonReport } from "sdk/cls-k12-sdk-js/src/services/manage-report/models/lesson-report";

export class HistoryLessonLearnVM implements LessonReport {
    lessonId: number;
    lessonName: string;
    gradeId: number;
    subjectId: number;
    teacherId: number;
    registrationDate: Date;
    startDate: Date;
    completionDate: Date;
    percentageOfCompletion: number;
    result: number;
    assessmentLevelName: string;

    subjectName: string;
    gradeName: string;
    teacherName: string;

    static From(data: LessonReport, subjectName: string, gradeName: string, teacherName: string) {
        var m: HistoryLessonLearnVM = new HistoryLessonLearnVM();
        debugger;
        m.lessonId= data.lessonId;
        m.lessonName= data.lessonName;
        m.gradeId= data.gradeId;
        m.subjectId = data.subjectId;
        m.teacherId= data.teacherId;
        m.registrationDate= data.registrationDate;
        m.startDate=data.startDate;
        m.completionDate= data.completionDate;
        m.percentageOfCompletion= data.percentageOfCompletion;
        m.result= data.result;
        m.assessmentLevelName= data.assessmentLevelName;
        if(!subjectName){
            subjectName = '';
        }
        if(!gradeName){
            gradeName = '';
        }
        if(!teacherName){
            teacherName = '';
        }
        m.subjectName= subjectName;
        m.gradeName= gradeName;
        m.teacherName= teacherName;

        return m;
    }
}


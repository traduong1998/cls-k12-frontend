import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryUserLessonComponent } from './history-user-lesson.component';

describe('HistoryUserLessonComponent', () => {
  let component: HistoryUserLessonComponent;
  let fixture: ComponentFixture<HistoryUserLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryUserLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryUserLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

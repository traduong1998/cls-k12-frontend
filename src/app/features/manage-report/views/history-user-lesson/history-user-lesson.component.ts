import { DivisionEndpoint, GradeEndpoint, SchoolEndpoint, SubjectEndpoint, UserEndpoint, UserIdentity, ManagerReportEndpoint } from 'cls-k12-sdk-js/src';
import { AuthService } from '../../../../core/services/auth.service';
import { ExamEnpoint } from 'cls-k12-sdk-js/src/services/exam/endpoints/exam-enpoint';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { ReplaySubject, Subject as RxSubject } from 'rxjs';
import { GradeOption, Grade } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { SubjectOption } from 'cls-k12-sdk-js/src/services/subject/models/Subject';
import { take, takeUntil } from 'rxjs/operators';
import { TeacherOption } from 'cls-k12-sdk-js/src/services/user/models/teacher';
import { GetLessonReportFilterRequests } from 'cls-k12-sdk-js/src/services/manage-report/request/get-lesson-report-filter-request';
import { LessonReport } from 'cls-k12-sdk-js/src/services/manage-report/models/lesson-report';
import { WindowScrollService } from 'src/app/shared/helpers/services/window-scroll.service';
import { MatPaginator } from '@angular/material/paginator';
import { HistoryLessonUser } from 'sdk/cls-k12-sdk-js/src/services/manage-report/request/history-lesson-user-request';
import { Console } from 'console';
import { InforSystemHistoryLesson } from 'sdk/cls-k12-sdk-js/src/services/manage-report/request/info-system-history-lesson-request';
import { InforSystemLesson } from 'sdk/cls-k12-sdk-js/src/services/manage-report/models/infor-system-lesson';
import { HistoryLessonLearnVM } from './view-models/history-lesson-learner-VM';
import { PaginatorResponse } from 'cls-k12-sdk-js/src/core/api/responses/PaginatorResponse';
import { DetailHistoryLearnLessonUser } from 'cls-k12-sdk-js/src/services/manage-report/request/DetailHistoryLearnLessonUser';
import { NgxSpinnerService } from 'ngx-spinner';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { ActivatedRoute } from '@angular/router';
import { GlobalEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { AppConfigService } from 'src/app/core/services';

const reportStudentPaginationDefault: PaginatorResponse<LessonReport> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-history-user-lesson',
  templateUrl: './history-user-lesson.component.html',
  styleUrls: ['./history-user-lesson.component.scss']
})
export class HistoryUserLessonComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('dpFromDate') dpFromDate;
  @ViewChild('dpToDate') dpToDate;
  

  startDate: FormControl = new FormControl('');

  baseApiUrl: string = "";
  protected allGrades: Grade[];
  protected allSubjects: SubjectOption[];

  protected grades: Grade[];
  public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<Grade[]> = new ReplaySubject<Grade[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;


  protected subjects: SubjectOption[];
  public subjectCtrl: FormControl = new FormControl();
  public subjectFilterCtrl: FormControl = new FormControl();
  public filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  @ViewChild('singleSelectSubject', { static: true }) singleSelectSubject: MatSelect;


  protected teachers: TeacherOption[];
  public teacherCtrl: FormControl = new FormControl();
  public teacherFilterCtrl: FormControl = new FormControl();
  public filteredTeachers: ReplaySubject<TeacherOption[]> = new ReplaySubject<TeacherOption[]>(1);
  @ViewChild('singleSelectTeachers', { static: true }) singleSelectTeachers: MatSelect;

  protected _onDestroy = new RxSubject<void>();

  public statusCtrl: FormControl = new FormControl();
  public examTypeCtrl: FormControl = new FormControl();
  public fromDateCtrl: FormControl = new FormControl('');
  public toDateCtrl: FormControl = new FormControl('');

  startDateFilter: Date = null;
  endDateFilter: Date = null;

  filterForm = this.fb.group({
    fromDate: new FormControl(),
    toDate: '',
    search: new FormControl(''),
    gradeId: new FormControl(null, [Validators.required])
  });

  isFirstLoadAllGrades: boolean = true;
  isFirstLoadGrades: boolean = true;
  isFirstLoadSubjects: boolean = true;
  isFirstLoadTeacher: boolean = true;

  levelManageValue: number = 0;

  selectedCurrentGrade: number;

  isLoadFullData: boolean = false;
  isLoadingLessonReport: boolean = false;
  dataLessonReportPagination: PaginatorResponse<LessonReport> = reportStudentPaginationDefault;
  getLessonReportFilterRequest: DetailHistoryLearnLessonUser = {
    pageNumber: 1,
    sizeNumber: 10,
    getCount: true,
    gradeId: null,
    subjectId: null,
    teacherId: null,
    sortField: 'CRE',
    sortDirection: 'DESC',
  }

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  subjectFilter = {
    gradeId: null
  }
  teacherFilter = {
    teacherId: null
  }
  displayedColumns: string[] = ['STT', 'name', 'subject', 'grade', 'creator', 'registerDate', 'startTime', 'endTime', 'scope', 'result', 'assessmentLevelName'];

  private globalEndpoint: GlobalEndpoint;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  private endpointGrade: GradeEndpoint;
  private examEndpoint: ExamEnpoint;
  private endpointSubject: SubjectEndpoint;
  private endpointUser: UserEndpoint;
  private endpointReportManager: ManagerReportEndpoint;
  userId: number;
  userIdentity: UserIdentity;
  portalId: number;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private _authService: AuthService,
    private windowScrollService: WindowScrollService,
    private configService: AppConfigService
  ) {
    this.route.params.subscribe(params => {
      this.userId = +params['id'];
      console.log("sss", this.userId);
    })

    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubject = new SubjectEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUser = new UserEndpoint({ baseUrl: this.baseApiUrl });

    this.endpointReportManager = new ManagerReportEndpoint()
    this.globalEndpoint = new GlobalEndpoint();
    this.examEndpoint = new ExamEnpoint();
    this.userIdentity = _authService.getTokenInfo();
    this.portalId = this.configService.getConfig().unit.portalId;

    console.log("tdfdf", this.userIdentity)
    this.spinner.show();
  }

  ngOnInit(): void {
        // Promise.all([
        //   this.globalEndpoint.getGradeOptionsByPortalId(this.portalId,null)
        //     .then(res => {
  
        //       if (res.length > 0) {
        //         this.gradeCtrl.setValue(res[0].id)
        //         this.getLessonReportFilterRequest.gradeId = this.gradeCtrl.value;
        //         this.grades = res;
        //         this.filteredGrades.next(this.grades.slice());
        //       }
        //     })
        //     .catch((er) => {
        //       console.log("err")
        //     })
        // ]).then(() => {
  
        //   this.getListLessonReport();
        // })    

    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });
    // listen for search field value changes
    this.subjectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSubjects();
      });

    // listen for search field value changes
    this.teacherFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterTeachers();
      });

    this.getListLessonReport();
  }


  ngAfterViewInit() {
    debugger;
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.getLessonReportFilterRequest.pageNumber = this.paginator.pageIndex + 1;
      this.getLessonReportFilterRequest.sizeNumber = this.paginator.pageSize;
      this.getListLessonReport();
    });
  }


  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  currentPage = 1;
  totalPage;
  request: InforSystemHistoryLesson;
  inforSystemLesson: InforSystemLesson[] = [];
  historyLessonLearnVM: HistoryLessonLearnVM[] = []
  getListLessonReport() {
    this.historyLessonLearnVM = [];
    this.spinner.show();
    Promise.resolve()
      .then(() => {
        debugger;
        this.getLessonReportFilterRequest.userId = this.userId;
        return this.endpointReportManager.getDetailUserLesson(this.userId, this.getLessonReportFilterRequest)
      }).then((res) => {
        this.dataLessonReportPagination = res;
        console.log(res);

        this.totalPage = res.totalItems;
        let listGradeId = res.items.map(x => x.gradeId);
        let listSubjectId = res.items.map(x => x.subjectId);
        let listTeacherId = res.items.map(x => x.teacherId);


        this.request = { listGradeId: listGradeId, listSubjectId: listSubjectId, listTeacherId: listTeacherId }
        Promise.all([
          this.endpointReportManager.getLearnInfoSystem(this.request)
            .then((res) => {
              this.inforSystemLesson = res;
            })
        ]).then(() => {
          var temp = [];
          res.items.forEach((item) => {
            let infoSubject = this.inforSystemLesson.find(x => (x.subjectId == item.subjectId));
            let infoGrade = this.inforSystemLesson.find(x => (x.gradeId == item.gradeId));
            let infoTeacher = this.inforSystemLesson.find(x => (x.teacherId == item.teacherId));
            var ssubjectName;
            if (!infoSubject) {
              ssubjectName = '';
            } else {
              ssubjectName = infoSubject.subjectName;
            }

            var sgradeName;
            if (!infoGrade) {
              sgradeName = '';
            } else {
              sgradeName = infoGrade.gradeName;
            }
            var steacherName;
            if (!infoTeacher) {
              steacherName = '';
            } else {
              steacherName = infoTeacher.teacherName;
            }
            if (new Date(item.completionDate).getFullYear() == 1) {
              item.completionDate = null;
            }

            if (new Date(item.startDate).getFullYear() == 1) {
              item.startDate = null;
            }

            // this.historyLessonLearnVM.push(HistoryLessonLearnVM.From(item, ssubjectName, sgradeName, steacherName));
            temp.push(HistoryLessonLearnVM.From(item, ssubjectName, sgradeName, steacherName));
          })
          this.historyLessonLearnVM = [...temp];
          console.log("sdsd", this.historyLessonLearnVM)
          this.spinner.hide();
          //return;
        })
      })
  }

  protected setInitialValue() {
    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectGrade != undefined) {
          this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
        }
      });

    this.filteredSubjects
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectSubject != undefined) {
          this.singleSelectSubject.compareWith = (a: SubjectOption, b: SubjectOption) => a && b && a.id === b.id;
        }
      });

    this.filteredTeachers
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectTeachers != undefined) {
          this.singleSelectTeachers.compareWith = (a: SubjectOption, b: SubjectOption) => a && b && a.id === b.id;
        }
      });
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSubjects() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.subjectFilterCtrl.value;
    if (!search) {
      this.filteredSubjects.next(this.subjects.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the subject
    this.filteredSubjects.next(
      this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterTeachers() {
    if (!this.teachers) {
      return;
    }
    // get the search keyword
    let search = this.teacherFilterCtrl.value;
    if (!search) {
      this.filteredTeachers.next(this.teachers.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredTeachers.next(
      this.teachers.filter(teacher => teacher.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onGradeSelectClicked() {
    this.globalEndpoint.getGradeOptionsByPortalId(this.portalId,null)
      .then(res => {
        this.grades = res;
        this.filteredGrades.next(this.grades.slice());
      })
      .catch()
  }

  onSubjectSelectClicked() {
    let gradeIdSelected = this.gradeCtrl.value;
    if (this.isFirstLoadSubjects || this.subjectFilter.gradeId != gradeIdSelected) {
      this.endpointSubject.getSubjectOptions(gradeIdSelected)
        .then(res => {
          this.isFirstLoadSubjects = false;
          this.subjects = res;
          this.subjectFilter.gradeId = gradeIdSelected;
          this.filteredSubjects.next(this.subjects.slice());
        })
        .catch()
    }
  }

  onTeacherSelectClicked() {
    let teacherIdSelected = this.teacherCtrl.value;
    if (this.isFirstLoadTeacher || this.teacherFilter.teacherId != teacherIdSelected) {
      this.endpointUser.getTeachers()
        .then(res => {
          this.isFirstLoadTeacher = false;
          this.teachers = res;
          this.teacherFilter.teacherId = teacherIdSelected;
          this.filteredTeachers.next(this.teachers.slice());
        })
        .catch()
    }
  }

  onChangeGradeSelected(id: any) {
    this.subjects = [];
    this.isFirstLoadSubjects = true;
    this.subjectCtrl.setValue(undefined);
    this.gradeCtrl.setValue(id);
  }

  resetGradeSelectCtrl() {
    this.grades = [];
    this.isFirstLoadGrades = true;
    this.gradeCtrl.setValue(undefined);
  }
  openDpFromDate() {
    this.dpFromDate.open();
  }
  openDpToDate() {
    this.dpToDate.open();
  }

  getFilterParam() {
    this.getLessonReportFilterRequest.gradeId = this.gradeCtrl.value;
    this.getLessonReportFilterRequest.subjectId = this.subjectCtrl.value;
    this.getLessonReportFilterRequest.teacherId = this.teacherCtrl.value;

    //this.getLessonReportFilterRequest.page = 1;
  }

  filterLessonReport() {
    this.getFilterParam();
    this.getLessonReportFilterRequest.pageNumber = 1;
    this.paginator.pageIndex = 0;
    this.getListLessonReport();
  }

  exportExcel() {
    this.getFilterParam();
    this.spinner.show(undefined, {
      type: SpinnerType.ballSpin,
      size: 'medium',
      bdColor: 'rgba(0, 0, 0, 0.2)',
      color: '#43a047',
      fullScreen: false
    });
    let date = new Date()
    let ticks = date.getTime()
    let fileName: string;
    let objectUrl: string;
    const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
    this.endpointReportManager.exportLessonReportByUserId(this.userId, this.subjectCtrl.value, this.gradeCtrl.value, this.teacherCtrl.value)
      .then((urlFile) => {
        objectUrl = urlFile;
        fileName = 'Lich-su-hoc-bai-giang.xlsx';
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
        this.spinner.hide();
      })
  }

}

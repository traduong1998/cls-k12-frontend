import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonUsersgroupReportComponent } from './lesson-usersgroup-report.component';

describe('LessonUsersgroupReportComponent', () => {
  let component: LessonUsersgroupReportComponent;
  let fixture: ComponentFixture<LessonUsersgroupReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LessonUsersgroupReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonUsersgroupReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

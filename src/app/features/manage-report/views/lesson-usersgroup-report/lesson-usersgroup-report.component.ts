import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { ReplaySubject, Subject as RxSubject } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { DivisionEndpoint, SchoolEndpoint, GradeEndpoint, ExamEnpoint, SubjectEndpoint, UserEndpoint, ManagerReportEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { InforSystemLesson } from 'sdk/cls-k12-sdk-js/src/services/manage-report/models/infor-system-lesson';
import { LessonReport } from 'sdk/cls-k12-sdk-js/src/services/manage-report/models/lesson-report';
import { HistoryLessonUser } from 'sdk/cls-k12-sdk-js/src/services/manage-report/request/history-lesson-user-request';
import { InforSystemHistoryLesson } from 'sdk/cls-k12-sdk-js/src/services/manage-report/request/info-system-history-lesson-request';
import { UserGroupReportEndpoints } from 'sdk/cls-k12-sdk-js/src/services/manage-report/usersgroups/endpoints/user-group-report-endpoints';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/manage/models/subject';
import { TeacherOption } from 'sdk/cls-k12-sdk-js/src/services/user/models/teacher';
import { AuthService } from 'src/app/core/services';
import { WindowScrollService } from 'src/app/shared/helpers/services/window-scroll.service';
import { HistoryLessonLearnVM } from '../../components/lesson-report/view-models/history-lesson-learner-VM';

@Component({
  selector: 'app-lesson-usersgroup-report',
  templateUrl: './lesson-usersgroup-report.component.html',
  styleUrls: ['./lesson-usersgroup-report.component.scss']
})
export class LessonUsersgroupReportComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('dpFromDate') dpFromDate;
  @ViewChild('dpToDate') dpToDate;

  startDate: FormControl = new FormControl('');

  baseApiUrl: string = "";
  protected allGrades: GradeOption[];
  protected allSubjects: SubjectOption[];

  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;


  protected subjects: SubjectOption[];
  public subjectCtrl: FormControl = new FormControl();
  public subjectFilterCtrl: FormControl = new FormControl();
  public filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  @ViewChild('singleSelectSubject', { static: true }) singleSelectSubject: MatSelect;


  protected teachers: TeacherOption[];
  public teacherCtrl: FormControl = new FormControl();
  public teacherFilterCtrl: FormControl = new FormControl();
  public filteredTeachers: ReplaySubject<TeacherOption[]> = new ReplaySubject<TeacherOption[]>(1);
  @ViewChild('singleSelectTeachers', { static: true }) singleSelectTeachers: MatSelect;

  protected _onDestroy = new RxSubject<void>();

  public statusCtrl: FormControl = new FormControl();
  public examTypeCtrl: FormControl = new FormControl();
  public fromDateCtrl: FormControl = new FormControl('');
  public toDateCtrl: FormControl = new FormControl('');

  startDateFilter: Date = null;
  endDateFilter: Date = null;

  filterForm = this.fb.group({
    fromDate: new FormControl(),
    toDate: '',
    search: new FormControl(''),
    gradeId: new FormControl(null, [Validators.required])
  });

  isFirstLoadAllGrades: boolean = true;
  isFirstLoadGrades: boolean = true;
  isFirstLoadSubjects: boolean = true;
  isFirstLoadTeacher: boolean = true;
  isShowFilter = true;
  levelManageValue: number = 0;

  page = 1;
  isLoadFullData: boolean = false;
  isLoadingLessonReport: boolean = false;
  dataLessonReportPagination: LessonReport[] = [];
  getLessonReportFilterRequest: HistoryLessonUser = {
    gradeId: null,
    subjectId: null,
    teacherId: null,
    sortField: 'CRE',
    sortDirection: 'DESC',
    page: 1,
    size: 10,
    getCount: true
  }

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  subjectFilter = {
    gradeId: null
  }
  teacherFilter = {
    teacherId: null
  }
  displayedColumns: string[] = ['STT', 'name', 'subject', 'grade', 'creator', 'registerDate', 'startTime', 'endTime', 'scope', 'result'];
  userIdQueryString: number;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  private endpointGrade: GradeEndpoint;
  private endpointSubject: SubjectEndpoint;
  private endpointUser: UserEndpoint;
  private endpointReportManager: UserGroupReportEndpoints;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  userIdentity: UserIdentity;

  constructor(
    private fb: FormBuilder,
    private _router: ActivatedRoute,
    private _authService: AuthService,
    private windowScrollService: WindowScrollService,
  ) {
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubject = new SubjectEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUser = new UserEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdQueryString = +this._router.snapshot.paramMap.get('userId');
    console.log(this.userIdQueryString);
    this.endpointReportManager = new UserGroupReportEndpoints()
    // this.examEndpoint = new ExamEnpoint();
    this.userIdentity = _authService.getTokenInfo();
  }

  ngOnInit(): void {
    debugger;
    this.getListLessonReport();

  }

  ngAfterViewInit(): void {
    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });
    // listen for search field value changes
    this.subjectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSubjects();
      });

    // listen for search field value changes
    this.teacherFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSubjects();
      });

    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  isReady;
  totalPage;
  request: InforSystemHistoryLesson;
  inforSystemLesson: InforSystemLesson[] = [];
  historyLessonLearnVM: HistoryLessonLearnVM[] = []

  getListLessonReport() {
    this.historyLessonLearnVM = [];
    Promise.resolve()
      .then(() => {
        this.isReady = false;
        return this.endpointReportManager.getLearnLesson(this.userIdQueryString, this.getLessonReportFilterRequest)

      }).then((res) => {

        console.log(res);

        this.totalPage = res.totalItems;
        let listGradeId = res.items.map(x => x.gradeId);
        let listSubjectId = res.items.map(x => x.subjectId);
        let listTeacherId = res.items.map(x => x.teacherId);

        // let listExamSubjectStudentId = res.items.map(x => x.id);

        this.request = { listGradeId: listGradeId, listSubjectId: listSubjectId, listTeacherId: listTeacherId }
        Promise.all([
          this.endpointReportManager.getLearnInfoSystem(this.request)
            .then((res) => {
              this.inforSystemLesson = res;
            })
        ]).then(() => {
          res.items.forEach((item) => {
            let info = this.inforSystemLesson.find(x => x.subjectId == item.subjectId);
            if (new Date(item.completionDate).getFullYear() == 1) {
              item.completionDate = null;
            }

            if (new Date(item.startDate).getFullYear() == 1) {
              item.startDate = null;
            }

            this.historyLessonLearnVM.push(HistoryLessonLearnVM.From(item, info.subjectName, info.gradeName, info.teacherName));

            console.log(this.historyLessonLearnVM)
          })
          this.isReady = true;
        })
      })
  }

  protected setInitialValue() {
    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectGrade != undefined) {
          this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
        }
      });

    this.filteredSubjects
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectSubject != undefined) {
          this.singleSelectSubject.compareWith = (a: SubjectOption, b: SubjectOption) => a && b && a.id === b.id;
        }
      });

    this.filteredTeachers
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectTeachers != undefined) {
          this.singleSelectTeachers.compareWith = (a: SubjectOption, b: SubjectOption) => a && b && a.id === b.id;
        }
      });
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSubjects() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.subjectFilterCtrl.value;
    if (!search) {
      this.filteredSubjects.next(this.subjects.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the subject
    this.filteredSubjects.next(
      this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterTeachers() {
    if (!this.teachers) {
      return;
    }
    // get the search keyword
    let search = this.teacherFilterCtrl.value;
    if (!search) {
      this.filteredTeachers.next(this.teachers.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredTeachers.next(
      this.teachers.filter(teacher => teacher.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onGradeSelectClicked() {
    if (this.isFirstLoadGrades) {
      this.endpointGrade.getGradeOptions()
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch()
    }
  }

  onSubjectSelectClicked() {
    let gradeIdSelected = this.gradeCtrl.value;
    if (this.isFirstLoadSubjects || this.subjectFilter.gradeId != gradeIdSelected) {
      this.endpointSubject.getSubjectOptions(gradeIdSelected)
        .then(res => {
          this.isFirstLoadSubjects = false;
          this.subjects = res;
          this.subjectFilter.gradeId = gradeIdSelected;
          this.filteredSubjects.next(this.subjects.slice());
        })
        .catch()
    }
  }

  onTeacherSelectClicked() {
    let teacherIdSelected = this.teacherCtrl.value;
    if (this.isFirstLoadTeacher || this.teacherFilter.teacherId != teacherIdSelected) {
      this.endpointUser.getTeachers()
        .then(res => {
          this.isFirstLoadTeacher = false;
          this.teachers = res;
          this.teacherFilter.teacherId = teacherIdSelected;
          this.filteredTeachers.next(this.teachers.slice());
        })
        .catch()
    }
  }

  onChangeGradeSelected(id: any) {
    this.subjects = [];
    this.isFirstLoadSubjects = true;
    this.subjectCtrl.setValue(undefined);
  }

  resetGradeSelectCtrl() {
    this.grades = [];
    this.isFirstLoadGrades = true;
    this.gradeCtrl.setValue(undefined);
  }
  openDpFromDate() {
    this.dpFromDate.open();
  }
  openDpToDate() {
    this.dpToDate.open();
  }

  getFilterParam() {
    this.getLessonReportFilterRequest.gradeId = this.gradeCtrl.value;
    this.getLessonReportFilterRequest.subjectId = this.subjectCtrl.value;
    this.getLessonReportFilterRequest.teacherId = this.teacherCtrl.value;

    this.getLessonReportFilterRequest.page = 1;
  }

  filterLessonReport() {
    this.getFilterParam();
    this.getListLessonReport();
  }
  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }
  exportExcel() {
    this.getFilterParam();
    this.endpointReportManager.exportLessonReportByUser(this.userIdQueryString, this.subjectCtrl.value, this.gradeCtrl.value, this.teacherCtrl.value)
      .then((urlFile) => {
        window.open(urlFile);
      })
  }

  handlePageEvent(event) {
    if (Number.isInteger(event)) {
      this.getLessonReportFilterRequest.page = event;
      this.page = event;
    } else {
      this.getLessonReportFilterRequest.page = event.pageIndex + 1;
    }
    this.getListLessonReport();
  }
}

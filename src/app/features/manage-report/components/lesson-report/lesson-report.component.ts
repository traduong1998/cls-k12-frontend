import { DivisionEndpoint, GradeEndpoint, SchoolEndpoint, SubjectEndpoint, UserEndpoint, UserIdentity, ManagerReportEndpoint } from 'cls-k12-sdk-js/src';
import { AuthService } from '../../../../core/services/auth.service';
import { ExamEnpoint } from 'cls-k12-sdk-js/src/services/exam/endpoints/exam-enpoint';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { ReplaySubject, Subject as RxSubject } from 'rxjs';
import { GradeOption } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { SubjectOption } from 'cls-k12-sdk-js/src/services/subject/models/Subject';
import { take, takeUntil } from 'rxjs/operators';
import { TeacherOption } from 'cls-k12-sdk-js/src/services/user/models/teacher';
import { GetLessonReportFilterRequests } from 'cls-k12-sdk-js/src/services/manage-report/request/get-lesson-report-filter-request';
import { LessonReport } from 'cls-k12-sdk-js/src/services/manage-report/models/lesson-report';
import { WindowScrollService } from 'src/app/shared/helpers/services/window-scroll.service';
import { MatPaginator } from '@angular/material/paginator';
import { HistoryLessonUser } from 'sdk/cls-k12-sdk-js/src/services/manage-report/request/history-lesson-user-request';
import { Console } from 'console';
import { InforSystemHistoryLesson } from 'sdk/cls-k12-sdk-js/src/services/manage-report/request/info-system-history-lesson-request';
import { InforSystemLesson } from 'sdk/cls-k12-sdk-js/src/services/manage-report/models/infor-system-lesson';
import { HistoryLessonLearnVM } from './view-models/history-lesson-learner-VM';
import { PaginatorResponse } from 'cls-k12-sdk-js/src/core/api/responses/PaginatorResponse';
import { HistoryLearnLessonUser } from 'cls-k12-sdk-js/src/services/manage-report/request/HistoryLearnLessonUser';
import { NgxSpinnerService } from 'ngx-spinner';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';

const reportStudentPaginationDefault: PaginatorResponse<LessonReport> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-lesson-report',
  templateUrl: './lesson-report.component.html',
  styleUrls: ['./lesson-report.component.scss']
})
export class LessonReportComponent implements OnInit {
  @Input() _currentTab: string;

  @Input() set currentTab(value: string) {
    this._currentTab = value;
    if (this._currentTab === 'lesson') {

      Promise.all([
        this.endpointGrade.getGradeForCombobox()
          .then(res => {

            if (res.length > 0) {
              this.gradeCtrl.setValue(res[0].id)
              this.getLessonReportFilterRequest.gradeId = this.gradeCtrl.value;
              this.grades = res;
              this.filteredGrades.next(this.grades.slice());
            }
          })
          .catch((er) => {
            console.log("err")
          })
      ]).then(() => {

        this.getListLessonReport();
      })
    }
  };

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('dpFromDate') dpFromDate;
  @ViewChild('dpToDate') dpToDate;

  startDate: FormControl = new FormControl('');

  baseApiUrl: string = "";
  protected allGrades: GradeOption[];
  protected allSubjects: SubjectOption[];

  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;


  protected subjects: SubjectOption[];
  public subjectCtrl: FormControl = new FormControl();
  public subjectFilterCtrl: FormControl = new FormControl();
  public filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  @ViewChild('singleSelectSubject', { static: true }) singleSelectSubject: MatSelect;


  protected teachers: TeacherOption[];
  public teacherCtrl: FormControl = new FormControl();
  public teacherFilterCtrl: FormControl = new FormControl();
  public filteredTeachers: ReplaySubject<TeacherOption[]> = new ReplaySubject<TeacherOption[]>(1);
  @ViewChild('singleSelectTeachers', { static: true }) singleSelectTeachers: MatSelect;

  protected _onDestroy = new RxSubject<void>();

  public statusCtrl: FormControl = new FormControl();
  public examTypeCtrl: FormControl = new FormControl();
  public fromDateCtrl: FormControl = new FormControl('');
  public toDateCtrl: FormControl = new FormControl('');

  startDateFilter: Date = null;
  endDateFilter: Date = null;

  filterForm = this.fb.group({
    fromDate: new FormControl(),
    toDate: '',
    search: new FormControl(''),
    gradeId: new FormControl(null, [Validators.required])
  });

  isFirstLoadAllGrades: boolean = true;
  isFirstLoadGrades: boolean = true;
  isFirstLoadSubjects: boolean = true;
  isFirstLoadTeacher: boolean = true;

  levelManageValue: number = 0;

  selectedCurrentGrade: number;

  isLoadFullData: boolean = false;
  isLoadingLessonReport: boolean = false;
  dataLessonReportPagination: PaginatorResponse<LessonReport> = reportStudentPaginationDefault;
  getLessonReportFilterRequest: HistoryLearnLessonUser = {
    pageNumber: 1,
    sizeNumber: 10,
    getCount: true,
    gradeId: null,
    subjectId: null,
    teacherId: null,
    sortField: 'CRE',
    sortDirection: 'DESC',
  }

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  subjectFilter = {
    gradeId: null
  }
  teacherFilter = {
    teacherId: null
  }
  displayedColumns: string[] = ['STT', 'name', 'subject', 'grade', 'creator', 'registerDate', 'startTime', 'endTime', 'scope', 'result', 'assessmentLevelName'];

  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  private endpointGrade: GradeEndpoint;
  private examEndpoint: ExamEnpoint;
  private endpointSubject: SubjectEndpoint;
  private endpointUser: UserEndpoint;
  private endpointReportManager: ManagerReportEndpoint;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  userIdentity: UserIdentity;

  constructor(
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private _authService: AuthService,
    private windowScrollService: WindowScrollService,
  ) {
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubject = new SubjectEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUser = new UserEndpoint({ baseUrl: this.baseApiUrl });

    this.endpointReportManager = new ManagerReportEndpoint()
    this.examEndpoint = new ExamEnpoint();
    this.userIdentity = _authService.getTokenInfo();

    console.log("tdfdf", this.userIdentity)
  }

  ngOnInit(): void {
    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });
    // listen for search field value changes
    this.subjectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSubjects();
      });

    // listen for search field value changes
    this.teacherFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterTeachers();
      });
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.getLessonReportFilterRequest.pageNumber = this.paginator.pageIndex + 1;
      this.getLessonReportFilterRequest.sizeNumber = this.paginator.pageSize;
      this.getListLessonReport();
    });
  }


  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  isReady = false;
  currentPage = 1;
  totalPage;
  request: InforSystemHistoryLesson;
  inforSystemLesson: InforSystemLesson[] = [];
  historyLessonLearnVM: HistoryLessonLearnVM[] = []
  getListLessonReport() {

    debugger;
    this.historyLessonLearnVM = [];
    Promise.resolve()
      .then(() => {
        this.isReady = false;

        return this.endpointReportManager.getHistoryLearnLesson(this.getLessonReportFilterRequest)
      }).then((res) => {
        this.dataLessonReportPagination = res;
        console.log(res);

        this.totalPage = res.totalItems;
        let listGradeId = res.items.map(x => x.gradeId);
        let listSubjectId = res.items.map(x => x.subjectId);
        let listTeacherId = res.items.map(x => x.teacherId);


        this.request = { listGradeId: listGradeId, listSubjectId: listSubjectId, listTeacherId: listTeacherId }
        Promise.all([
          this.endpointReportManager.getLearnInfoSystem(this.request)
            .then((res) => {
              this.inforSystemLesson = res;
            })
        ]).then(() => {
          res.items.forEach((item) => {
            debugger;
            let infoSubject = this.inforSystemLesson.find(x => (x.subjectId == item.subjectId));
            let infoGrade = this.inforSystemLesson.find(x => (x.gradeId == item.gradeId));
            let infoTeacher = this.inforSystemLesson.find(x => (x.teacherId == item.teacherId));
            var ssubjectName;
            if(!infoSubject){
              ssubjectName = '';
            }else{
              ssubjectName = infoSubject.subjectName;
            }

            var sgradeName;
            if(!infoGrade){
              sgradeName = '';
            }else{
              sgradeName = infoGrade.gradeName;
            }
            var steacherName;
            if(!infoTeacher){
              steacherName = '';
            }else{
              steacherName = infoTeacher.teacherName;
            }
            if (new Date(item.completionDate).getFullYear() == 1) {
              item.completionDate = null;
            }

            if (new Date(item.startDate).getFullYear() == 1) {
              item.startDate = null;
            }
         
            this.historyLessonLearnVM.push(HistoryLessonLearnVM.From(item, ssubjectName, sgradeName, steacherName));
            console.log("sdsd", this.historyLessonLearnVM)
          })
          this.isReady = true;
        })
      })
  }

  protected setInitialValue() {
    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectGrade != undefined) {
          this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
        }
      });

    this.filteredSubjects
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectSubject != undefined) {
          this.singleSelectSubject.compareWith = (a: SubjectOption, b: SubjectOption) => a && b && a.id === b.id;
        }
      });

    this.filteredTeachers
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectTeachers != undefined) {
          this.singleSelectTeachers.compareWith = (a: SubjectOption, b: SubjectOption) => a && b && a.id === b.id;
        }
      });
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSubjects() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.subjectFilterCtrl.value;
    if (!search) {
      this.filteredSubjects.next(this.subjects.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the subject
    this.filteredSubjects.next(
      this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterTeachers() {
    if (!this.teachers) {
      return;
    }
    // get the search keyword
    let search = this.teacherFilterCtrl.value;
    if (!search) {
      this.filteredTeachers.next(this.teachers.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredTeachers.next(
      this.teachers.filter(teacher => teacher.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onGradeSelectClicked() {
    this.endpointGrade.getGradeForCombobox()
      .then(res => {
        // this.gradeCtrl.setValue(res[0].id)
        // this.getLessonReportFilterRequest.gradeId = this.gradeCtrl.value;
        //this.selectedCurrentGrade = res[0].id;
        // this.isFirstLoadGrades = false;
        this.grades = res;
        this.filteredGrades.next(this.grades.slice());
      })
      .catch()
  }

  onSubjectSelectClicked() {
    let gradeIdSelected = this.gradeCtrl.value;
    if (this.isFirstLoadSubjects || this.subjectFilter.gradeId != gradeIdSelected) {
      this.endpointSubject.getSubjectOptions(gradeIdSelected)
        .then(res => {
          this.isFirstLoadSubjects = false;
          this.subjects = res;
          this.subjectFilter.gradeId = gradeIdSelected;
          this.filteredSubjects.next(this.subjects.slice());
        })
        .catch()
    }
  }

  onTeacherSelectClicked() {
    let teacherIdSelected = this.teacherCtrl.value;
    if (this.isFirstLoadTeacher || this.teacherFilter.teacherId != teacherIdSelected) {
      this.endpointUser.getTeachers()
        .then(res => {
          this.isFirstLoadTeacher = false;
          this.teachers = res;
          this.teacherFilter.teacherId = teacherIdSelected;
          this.filteredTeachers.next(this.teachers.slice());
        })
        .catch()
    }
  }

  onChangeGradeSelected(id: any) {
    this.subjects = [];
    this.isFirstLoadSubjects = true;
    this.subjectCtrl.setValue(undefined);
    this.gradeCtrl.setValue(id);
  }

  resetGradeSelectCtrl() {
    this.grades = [];
    this.isFirstLoadGrades = true;
    this.gradeCtrl.setValue(undefined);
  }
  openDpFromDate() {
    this.dpFromDate.open();
  }
  openDpToDate() {
    this.dpToDate.open();
  }

  getFilterParam() {
    this.getLessonReportFilterRequest.gradeId = this.gradeCtrl.value;
    this.getLessonReportFilterRequest.subjectId = this.subjectCtrl.value;
    this.getLessonReportFilterRequest.teacherId = this.teacherCtrl.value;

    //this.getLessonReportFilterRequest.page = 1;
  }

  filterLessonReport() {
    this.getFilterParam();
    this.getLessonReportFilterRequest.pageNumber = 1;
    this.paginator.pageIndex = 0;
    this.getListLessonReport();
  }

  exportExcel() {
    this.getFilterParam();
    this.spinner.show(undefined, {
      type: SpinnerType.ballSpin,
      size: 'medium',
      bdColor: 'rgba(0, 0, 0, 0.2)',
      color: '#43a047',
      fullScreen: false
    });
    let date = new Date()
    let ticks = date.getTime()
    let fileName: string;
    let objectUrl: string;
    const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
    this.endpointReportManager.exportLessonReportByUser(this.subjectCtrl.value, this.gradeCtrl.value, this.teacherCtrl.value)
      .then((urlFile) => {
        objectUrl = urlFile;
        fileName = 'Lich-su-hoc-bai-giang.xlsx';
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
        this.spinner.hide();
      })
  }

  handlePageEvent(event) {
    if (Number.isInteger(event)) {
      this.getLessonReportFilterRequest.pageNumber = event
      this.currentPage = event;
    } else {
      this.getLessonReportFilterRequest.pageNumber = event.pageIndex + 1;
    }

    this.getListLessonReport();
  }
}

import { ExamReportModel } from "sdk/cls-k12-sdk-js/src/services/manage-report/models/exam-report-model";

export class ExamReportVM implements ExamReportModel {
    status: string;
    id: number;
    examId: number;
    gradeId: number;
    subjectId: number;
    roomName: string;
    shiftName: string;
    startDate: Date;
    endDate: Date;
    gradeName: string;
    subjectName: string;
    examType: string;
    isCompleted: boolean;
    score: number;
    examName: string;
    startTime?:Date;
    endTime?:Date;
    infingeTimes:number;
    scoreDecuted:number;
    static From(data: ExamReportModel, examName: string,startTime?:Date, endTime?:Date) {
        var m: ExamReportVM = {
            id: data.id,
            examId: data.examId,
            gradeId: data.gradeId,
            subjectId: data.subjectId,
            roomName: data.roomName,
            shiftName: data.shiftName,
            startDate: data.startDate,
            endDate: data.endDate,
            gradeName: data.gradeName,
            subjectName: data.subjectName,
            examType: data.examType,
            isCompleted: data.isCompleted,
            score: data.score,
            examName: examName,
            status:data.status,
            infingeTimes:data.infingeTimes,
            scoreDecuted:data.scoreDecuted,
            startTime:startTime,
            endTime:endTime
        }
        return m;
    }
}


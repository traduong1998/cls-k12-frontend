import { ExamReport } from 'cls-k12-sdk-js/src/services/manage-report/models/exam-report';
import { DivisionEndpoint, GradeEndpoint, SchoolEndpoint, SubjectEndpoint, UserIdentity, ManagerReportEndpoint } from 'cls-k12-sdk-js/src';
import { AuthService } from '../../../../core/services/auth.service';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { MatSort } from '@angular/material/sort';
import { BehaviorSubject, ReplaySubject, Subject as RxSubject } from 'rxjs';
import { GradeOption } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { SubjectOption } from 'cls-k12-sdk-js/src/services/subject/models/Subject';
import { take, takeUntil } from 'rxjs/operators';
import { WindowScrollService } from 'src/app/shared/helpers/services/window-scroll.service';
import { GetExamReportFilterRequests } from 'cls-k12-sdk-js/src/services/manage-report/request/get-exam-report-filter-requests';
import { MatPaginator } from '@angular/material/paginator';
import { ExamReportRequest } from 'sdk/cls-k12-sdk-js/src/services/manage-report/request/exam-report-request';
import { ExamReportModel } from 'sdk/cls-k12-sdk-js/src/services/manage-report/models/exam-report-model';
import { ExamReportVM } from './view-model/exam-report-VM';
import { ExamInfoReport } from 'sdk/cls-k12-sdk-js/src/services/manage-report/models/exam-info';
import { ExamTime } from 'sdk/cls-k12-sdk-js/src/services/manage-report/models/exam-time';

@Component({
  selector: 'app-exam-report',
  templateUrl: './exam-report.component.html',
  styleUrls: ['./exam-report.component.scss']
})
export class ExamReportComponent implements OnInit {
  private _currentTab: string;
  @Input() set currentTab(value: string) {
    this._currentTab = value;
    if (this._currentTab === 'exam' && this.isFirstTimeLoad) {
      Promise.all([
        this.endpointGrade.getGradeForCombobox()
          .then(res => {
            if(res.length> 0){
              this.gradeCtrl.setValue(res[0].id);
              this.examReportRequest.gradeId = this.gradeCtrl.value;
              this.grades = res;
              this.filteredGrades.next(this.grades.slice());
            }
          })
          .catch()
      ]).then(() => {
        this.getListExamReport();
      })


    }
  };


  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('dpFromDate') dpFromDate;
  @ViewChild('dpToDate') dpToDate;

  startDate: FormControl = new FormControl('');

  baseApiUrl: string = "";
  protected allGrades: GradeOption[];
  protected allSubjects: SubjectOption[];

  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;


  protected subjects: SubjectOption[];
  public subjectCtrl: FormControl = new FormControl();
  public subjectFilterCtrl: FormControl = new FormControl();
  public filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  @ViewChild('singleSelectSubject', { static: true }) singleSelectSubject: MatSelect;

  public examNameCtrl: FormControl = new FormControl();

  protected _onDestroy = new RxSubject<void>();

  public statusCtrl: FormControl = new FormControl();
  public examTypeCtrl: FormControl = new FormControl();
  public fromDateCtrl: FormControl = new FormControl('');
  public toDateCtrl: FormControl = new FormControl('');

  startDateFilter: Date = null;
  endDateFilter: Date = null;

  filterForm = this.fb.group({
    fromDate: new FormControl(),
    toDate: '',
    search: new FormControl(''),
    gradeId: new FormControl(null, [Validators.required]),
    examName: ''
  });

  isFirstLoadAllGrades: boolean = true;
  isFirstLoadGrades: boolean = true;
  isFirstLoadSubjects: boolean = true;
  isFirstLoadExam: boolean = true;

  levelManageValue: number = 0;

  isLoadFullData: boolean = false;
  isFirstTimeLoad: boolean = true;
  isLoadingExamReport: boolean = false;

  examReportModel: ExamReportVM[] = [];

  examInfoReport: ExamInfoReport[] = [];
  examTime: ExamTime[];

  dataExamReportPagination: ExamReport[] = [];
  // getExamReportFilterRequest: GetExamReportFilterRequests = {
  //   pageNumber: 1,
  //   pageSize: 10,
  //   gradeId: null,
  //   subjectId: null,
  //   examName: '',
  // }

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  subjectFilter = {
    gradeId: null
  }

  isReady = false;
  examReportRequest: ExamReportRequest =
    {
      gradeId: this.gradeCtrl.value,
      subjectId: this.subjectCtrl.value,
      listExamIds: null,
      sortField: "CRE",
      sortDirection: "DESC",
      page: 1, 
      size: 10,
      getCount: true
    };
  // displayedColumns: string[] = ['STT', 'name', 'shift', 'roomName', 'grade', 'subject', 'startTime', 'endTime', 'status', 'score','infingeTimes','scoreDecuted'];
  displayedColumns: string[] = ['STT', 'name', 'shift', 'roomName', 'grade', 'subject','status','infingeTimes','scoreDecuted', 'score'];
  currentPage: number = 1;
  totalPage = 1;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  private endpointGrade: GradeEndpoint;
  private endpointSubject: SubjectEndpoint;
  private endpointReportManager: ManagerReportEndpoint;

  userIdentity: UserIdentity;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private fb: FormBuilder,
    private _authService: AuthService,
    private windowScrollService: WindowScrollService,
  ) {
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubject = new SubjectEndpoint({ baseUrl: this.baseApiUrl });

    this.endpointReportManager = new ManagerReportEndpoint()
    this.userIdentity = _authService.getTokenInfo();
  }

  ngOnInit(): void {
    //this.getListExamReport();
    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });
    // listen for search field value changes
    this.subjectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSubjects();
      });
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.examReportRequest.page = this.paginator.pageIndex + 1;
      this.examReportRequest.size = this.paginator.pageSize;
      this.getListExamReport();

    });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  getListExamReport() {
    this.examReportModel = [];
    //real

    Promise.resolve()
      .then(() => {
        this.isReady = false;
        
        return this.endpointReportManager.getListReportExam(this.examReportRequest);

      }).then((res) => {
        this.totalPage = res.totalItems;
        let listExamId = res.items.map(x => x.examId);
        let listExamSubjectStudentId = res.items.map(x => x.id);

        Promise.all([
          this.endpointReportManager.getListExamMonitorById(listExamId)
            .then((res) => {
              this.examInfoReport = res;
            }),

          this.endpointReportManager.getDateFromExamSubjectStudentId(listExamSubjectStudentId)
            .then((res) => {
              this.examTime = res;
            })
        ]).then(() => {
          res.items.forEach((item) => {
            let exam = this.examInfoReport.find(x => x.examId == item.examId);

            if (new Date(item.endDate).getFullYear() == 1) {
              item.endDate = null;
            }


            let examTime = this.examTime.find(x => x.examSubjectStudentId == item.id);
            this.examReportModel.push(ExamReportVM.From(item, exam.name, examTime?.startDate, examTime?.endDate));

            console.log("sdasd", this.examReportModel)
          })
          this.isReady = true;
        })
      })
  }

  protected setInitialValue() {
    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectGrade != undefined) {
          this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
        }
      });
    this.filteredSubjects
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectSubject != undefined) {
          this.singleSelectSubject.compareWith = (a: SubjectOption, b: SubjectOption) => a && b && a.id === b.id;
        }
      });
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSubjects() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.subjectFilterCtrl.value;
    if (!search) {
      this.filteredSubjects.next(this.subjects.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the subject
    this.filteredSubjects.next(
      this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onGradeSelectClicked() {
    if (this.isFirstLoadGrades) {
      this.endpointGrade.getGradeForCombobox()
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch()
    }
  }

  onSubjectSelectClicked() {
    let gradeIdSelected = this.gradeCtrl.value;
    if (this.isFirstLoadSubjects || this.subjectFilter.gradeId != gradeIdSelected) {
      this.endpointSubject.getSubjectOptions(gradeIdSelected)
        .then(res => {
          this.isFirstLoadSubjects = false;
          this.subjects = res;
          this.subjectFilter.gradeId = gradeIdSelected;
          this.filteredSubjects.next(this.subjects.slice());
        })
        .catch()
    }
  }

  onChangeGradeSelected(id: any) {
    this.subjects = [];
    this.isFirstLoadSubjects = true;
    this.subjectCtrl.setValue(undefined);
  }

  resetGradeSelectCtrl() {
    this.grades = [];
    this.isFirstLoadGrades = true;
    this.gradeCtrl.setValue(undefined);
  }
  openDpFromDate() {
    this.dpFromDate.open();
  }
  openDpToDate() {
    this.dpToDate.open();
  }

  filterExamReport() {
    Promise.resolve()
      .then(() => {
        if (this.examNameCtrl.value) {
          return this.endpointReportManager.getListExamIdByName(this.examNameCtrl.value)
        }
      })
      .then((res) => {
        if (res) {
          this.examReportRequest.listExamIds = res;
        } else {
          this.examReportRequest.listExamIds = null;
        }
        this.examReportRequest.gradeId = this.gradeCtrl.value;
        this.examReportRequest.subjectId = this.subjectCtrl.value;
        this.examReportRequest.page = 1;
      })
      .then(() => {
        this.getListExamReport();
      })
  }

  exportExcel() {
    let getExamReportFilterRequests: GetExamReportFilterRequests =
    {
      gradeId: this.gradeCtrl.value,
      subjectId: this.subjectCtrl.value,
      examName: this.examNameCtrl.value,
      pageSize: null,
      pageNumber: 1
    }

    this.endpointReportManager.exportExamReportByUser(getExamReportFilterRequests)
      .then((urlFile) => {
        window.open(urlFile);
      })
  }

  handlePageEvent(event) {
    if (Number.isInteger(event)) {
      this.examReportRequest.page = event
      this.currentPage = event;
    } else {
      this.examReportRequest.page = event.pageIndex + 1;
    }

    this.getListExamReport();
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LessonReportComponent } from './components/lesson-report/lesson-report.component';
import { ExamReportComponent } from './components/exam-report/exam-report.component';
import { StudyHistoryReportComponent } from './views/study-history-report/study-history-report.view';
import { MathModule } from 'src/app/shared/math/math.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { ManagerReportRoutingModule } from './manager-report-routing.module';
import { TeacherReportComponent } from './views/teacher-report/teacher-report.component';
import { StudentReportComponent } from './views/student-report/student-report.component';
import { LessonReportViewComponent } from './views/lesson-report-view/lesson-report-view.component';
import { StudentLearnLessonReportComponent } from './views/student-learn-lesson-report/student-learn-lesson-report.component';
import { CircleProgressOptions, NgCircleProgressModule } from 'ng-circle-progress'
import { NgxPaginationModule } from 'ngx-pagination';
import { UsersgroupReportComponent } from './views/usersgroup-report/usersgroup-report.component';
import { DetailUsersGroupReportComponent } from './views/detail-users-group-report/detail-users-group-report.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { CustomizeReportComponent } from './views/customize-report/customize-report.component';
import { LessonUsersgroupReportComponent } from './views/lesson-usersgroup-report/lesson-usersgroup-report.component';
import { ListExamReportComponent } from './views/list-exam-report/list-exam-report.component';
import { ListExamSubjectReportComponent } from './views/list-exam-subject-report/list-exam-subject-report.component';
import { ExamReportManageComponent } from './views/exam-report-manage/exam-report-manage.component';

import { NgApexchartsModule } from 'ng-apexcharts';
import { OnlineClassesReportComponent } from './views/online-classes-report/online-classes-report.component';
import { RollCardClassroomReportComponent } from './views/roll-card-classroom-report/roll-card-classroom-report.component';
import { HistoryUserLessonComponent } from './views/history-user-lesson/history-user-lesson.component';

@NgModule({
  declarations: [
    LessonReportComponent,
    ExamReportComponent,
    StudyHistoryReportComponent,
    TeacherReportComponent,
    StudentReportComponent,
    LessonReportViewComponent,
    StudentLearnLessonReportComponent,
    UsersgroupReportComponent,
    DetailUsersGroupReportComponent,
    LessonUsersgroupReportComponent,
    CustomizeReportComponent,
    ListExamReportComponent,
    ListExamSubjectReportComponent,
    ExamReportManageComponent,
    OnlineClassesReportComponent,
    RollCardClassroomReportComponent,
    HistoryUserLessonComponent
  ],
  imports: [
    MathModule,
    SharedModule,
    ManagerReportRoutingModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    NgxMatSelectSearchModule,
    NgCircleProgressModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    NgApexchartsModule
  ],
  providers: [
    CircleProgressOptions
  ]
})
export class ManagerReportModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageUsertypesRoutingModule } from './manage-usertypes-routing.module';
import { ManageUsertypesComponent } from './manage-usertypes.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ContentListUsertypesComponent } from './components/content-list-usertypes/content-list-usertypes.component';
import { DeleteUsertypeComponent } from './views/delete-usertype/delete-usertype.component';
import { CreateUsertypeComponent } from './views/create-usertype/create-usertype.component';
import { UpdateUsertypeComponent } from './views/update-usertype/update-usertype.component';
import { ListUsertypesComponent } from './views/list-usertypes/list-usertypes.component';
import { ContentCreateUsertypeComponent } from './components/content-create-usertype/content-create-usertype.component';
import { ContentUpdateUsertypeComponent } from './components/content-update-usertype/content-update-usertype.component';
import { ContentDeleteUsertypeComponent } from './components/content-delete-usertype/content-delete-usertype.component';


@NgModule({
  declarations: [ManageUsertypesComponent, ContentListUsertypesComponent, ContentCreateUsertypeComponent, ContentUpdateUsertypeComponent, ContentDeleteUsertypeComponent, DeleteUsertypeComponent, CreateUsertypeComponent, UpdateUsertypeComponent, ListUsertypesComponent],
  imports: [
    SharedModule,
    ManageUsertypesRoutingModule
  ]
})
export class ManageUsertypesModule { }

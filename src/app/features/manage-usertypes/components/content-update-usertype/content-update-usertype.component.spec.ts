import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentUpdateUsertypeComponent } from './content-update-usertype.component';

describe('ContentUpdateUsertypesComponent', () => {
  let component: ContentUpdateUsertypeComponent;
  let fixture: ComponentFixture<ContentUpdateUsertypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentUpdateUsertypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentUpdateUsertypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, NgForm, FormControl, Validators } from '@angular/forms';
import { NgxPermissionsModule } from 'ngx-permissions';
import { UserTypeEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { PermissionModules } from 'sdk/cls-k12-sdk-js/src/services/usertype/models/PermissionModules';
import { CheckboxManager } from './interface/CheckboxManager';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { UserType } from 'sdk/cls-k12-sdk-js/src/services/usertype/models/UserType';
import { CLSPermissions } from 'cls-k12-sdk-js/src/core/authentication/identify/cls-permissions';
import { CLSModules } from 'cls-k12-sdk-js/src/core/authentication/identify/cls-modules';
import { UpdateUserTypeRequest } from 'sdk/cls-k12-sdk-js/src/services/usertype/requests/updateUserTypeRequest';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'app-content-update-usertypes',
  templateUrl: './content-update-usertype.component.html',
  styleUrls: ['./content-update-usertype.component.scss']
})
export class ContentUpdateUsertypeComponent implements OnInit {
  userTypeEndpoint: UserTypeEndpoint;

  baseApiUrl = 'http://localhost:65000';

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  public divisionCtrl: FormControl = new FormControl();
  lblname = '';
  lblPemissionModule = '';
  public selectRoleCtrl: FormControl = new FormControl('ADMIN');
  public selectLevelCtrl: FormControl = new FormControl('DPM');
  public inputFormControl: FormControl = new FormControl('', [
    Validators.required
  ]);
  //public valueSelectRoleCtrl:string='ADMIN';
  //public valueSelectLevelCtrl:string='DPM';

  updateUserTypeRequest: UpdateUserTypeRequest = {
    id: 0,
    name: '',
    userTypeRole: 'ADMIN',
    levelManage: 'DPM',
    permissionModules: []
  };

  levelManageOld: string;

  checkboxUserType: CheckboxManager[];
  checkboxManagerDivision: CheckboxManager[];
  checkboxManagerSchool: CheckboxManager[];
  checkboxManagerUser: CheckboxManager[];
  checkboxManagerClass: CheckboxManager[];
  checkboxManagerGrade: CheckboxManager[];
  checkboxManagerSubject: CheckboxManager[];
  checkboxManagerCurriculum: CheckboxManager[];
  checkboxManagerGroupStudent: CheckboxManager[];
  checkboxManagerLearningPath: CheckboxManager[];
  checkboxManagerMeetingRoom: CheckboxManager[];
  checkboxManagerQuestionBank: CheckboxManager[];
  checkboxManagerReport: CheckboxManager[];
  checkboxManagerSystem: CheckboxManager[];
  checkboxOrganizeExam: CheckboxManager[];
  checkboxManagerLesson: CheckboxManager[];
  checkboxManagerExam: CheckboxManager[];
  // checkboxManagerReportTeacher: CheckboxManager[];

  initialData() {
    this.checkboxUserType = [
      {
        name: "Xem",
        checked: false,
        module: "UST",
        value: 1,
      },
      {
        name: "Thêm",
        checked: false,
        module: "UST",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "UST",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "UST",
        value: 8
      }
    ]

    this.checkboxManagerDivision = [
      {
        name: "Xem",
        checked: false,
        module: "DIV",
        value: 1,
      },
      {
        name: "Thêm",
        checked: false,
        module: "DIV",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "DIV",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "DIV",
        value: 8
      }
    ]

    this.checkboxManagerSchool = [
      {
        name: "Xem",
        checked: false,
        module: "SCH",
        value: 1
      },
      {
        name: "Thêm",
        checked: false,
        module: "SCH",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "SCH",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "SCH",
        value: 8
      }
    ]

    this.checkboxManagerUser = [
      {
        name: "Xem",
        checked: false,
        module: "USE",
        value: 1
      },
      {
        name: "Thêm",
        checked: false,
        module: "USE",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "USE",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "USE",
        value: 8
      }
    ]

    this.checkboxManagerClass = [
      {
        name: "Xem",
        checked: false,
        module: "CLA",
        value: 1
      },
      {
        name: "Thêm",
        checked: false,
        module: "CLA",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "CLA",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "CLA",
        value: 8
      },
      {
        name: "Ghi danh",
        checked: false,
        module: "CLA",
        value: 64
      }
    ]

    this.checkboxManagerGrade = [
      {
        name: "Xem",
        checked: false,
        module: "GRA",
        value: 1
      },
      {
        name: "Thêm",
        checked: false,
        module: "GRA",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "GRA",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "GRA",
        value: 8
      }
    ]

    this.checkboxManagerSubject = [
      {
        name: "Xem",
        checked: false,
        module: "SUB",
        value: 1
      },
      {
        name: "Thêm",
        checked: false,
        module: "SUB",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "SUB",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "SUB",
        value: 8
      }
    ]

    this.checkboxManagerCurriculum = [
      {
        name: "Xem",
        checked: false,
        module: "CUR",
        value: 1
      },
      {
        name: "Thêm",
        checked: false,
        module: "CUR",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "CUR",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "CUR",
        value: 8
      }
    ]

    this.checkboxManagerGroupStudent = [
      {
        name: "Xem",
        checked: false,
        module: "STG",
        value: 1
      },
      {
        name: "Thêm",
        checked: false,
        module: "STG",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "STG",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "STG",
        value: 8
      },
      {
        name: "Ghi danh",
        checked: false,
        module: "STG",
        value: 64
      }
    ]

    this.checkboxManagerLearningPath = [
      {
        name: "Xem",
        checked: false,
        module: "LEP",
        value: 1
      },
      {
        name: "Thêm",
        checked: false,
        module: "LEP",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "LEP",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "LEP",
        value: 8
      },
      {
        name: "Ghi danh",
        checked: false,
        module: "LEP",
        value: 64
      }
    ]

    this.checkboxManagerMeetingRoom = [
      {
        name: "Xem",
        checked: false,
        module: "MTR",
        value: 1
      },
      {
        name: "Thêm",
        checked: false,
        module: "MTR",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "MTR",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "MTR",
        value: 8
      },
      {
        name: "Ghi danh",
        checked: false,
        module: "MTR",
        value: 64
      }
    ]

    this.checkboxManagerQuestionBank = [
      {
        name: "Xem",
        checked: false,
        module: "QEB",
        value: 1
      },
      {
        name: "Thêm",
        checked: false,
        module: "QEB",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "QEB",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "QEB",
        value: 8
      },
      {
        name: "Duyệt",
        checked: false,
        module: "QEB",
        value: 32
      }
    ]

    this.checkboxManagerReport = [
      {
        name: "Xem",
        checked: false,
        module: "REP",
        value: 1
      }
    ]

    this.checkboxManagerSystem = [
      {
        name: "Thiết lập",
        checked: false,
        module: "SEP",
        value: 1
      }
    ]

    this.checkboxOrganizeExam = [
      {
        name: "Xem",
        checked: false,
        module: "ORE",
        value: 1
      },
      {
        name: "Tổ chức",
        checked: false,
        module: "ORE",
        value: 2
      }
    ]

    this.checkboxManagerLesson = [
      {
        name: "Xem",
        checked: false,
        module: "LES",
        value: 1
      },
      {
        name: "Thêm",
        checked: false,
        module: "LES",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "LES",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "LES",
        value: 8
      },
      {
        name: "Sao chép",
        checked: false,
        module: "LES",
        value: 16
      },
      {
        name: "Duyệt",
        checked: false,
        module: "LES",
        value: 32
      },
      {
        name: "Ghi danh",
        checked: false,
        module: "LES",
        value: 64
      }
    ]

    this.checkboxManagerExam = [
      {
        name: "Xem",
        checked: false,
        module: "EXA",
        value: 1
      },
      {
        name: "Thêm",
        checked: false,
        module: "EXA",
        value: 2
      },
      {
        name: "Sửa",
        checked: false,
        module: "EXA",
        value: 4
      },
      {
        name: "Xóa",
        checked: false,
        module: "EXA",
        value: 8
      },
      {
        name: "Sao chép",
        checked: false,
        module: "EXA",
        value: 16
      },
      {
        name: "Duyệt",
        checked: false,
        module: "EXA",
        value: 32
      },
      {
        name: "Ghi danh",
        checked: false,
        module: "EXA",
        value: 64
      }
    ]

    // this.checkboxManagerReportTeacher = [
    //   {
    //     name: "Xem",
    //     checked: false,
    //     module: "REP",
    //     value: 1
    //   }
    // ]
    JSON.parse(this.userTypeInfo.permissionValue).forEach(element => {
      if (element.module == "UST") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxUserType.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxUserType.find(x => x.name == "Thêm").checked = true;
          this.checkboxUserType.find(x => x.name == "Xem").checked = true;
          this.checkboxUserType.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxUserType.find(x => x.name == "Sửa").checked = true;
          this.checkboxUserType.find(x => x.name == "Xem").checked = true;
          this.checkboxUserType.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxUserType.find(x => x.name == "Xóa").checked = true;
          this.checkboxUserType.find(x => x.name == "Xem").checked = true;
          this.checkboxUserType.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "DIV") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerDivision.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxManagerDivision.find(x => x.name == "Thêm").checked = true;
          this.checkboxManagerDivision.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerDivision.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxManagerDivision.find(x => x.name == "Sửa").checked = true;
          this.checkboxManagerDivision.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerDivision.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxManagerDivision.find(x => x.name == "Xóa").checked = true;
          this.checkboxManagerDivision.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerDivision.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "SCH") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerSchool.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxManagerSchool.find(x => x.name == "Thêm").checked = true;
          this.checkboxManagerSchool.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerSchool.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxManagerSchool.find(x => x.name == "Sửa").checked = true;
          this.checkboxManagerSchool.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerSchool.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxManagerSchool.find(x => x.name == "Xóa").checked = true;
          this.checkboxManagerSchool.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerSchool.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "USE") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerUser.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxManagerUser.find(x => x.name == "Thêm").checked = true;
          this.checkboxManagerUser.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerUser.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxManagerUser.find(x => x.name == "Sửa").checked = true;
          this.checkboxManagerUser.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerUser.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxManagerUser.find(x => x.name == "Xóa").checked = true;
          this.checkboxManagerUser.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerUser.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "CLA") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerClass.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxManagerClass.find(x => x.name == "Thêm").checked = true;
          this.checkboxManagerClass.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerClass.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxManagerClass.find(x => x.name == "Sửa").checked = true;
          this.checkboxManagerClass.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerClass.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxManagerClass.find(x => x.name == "Xóa").checked = true;
          this.checkboxManagerClass.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerClass.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Assign)) {
          this.checkboxManagerClass.find(x => x.name == "Ghi danh").checked = true;
          this.checkboxManagerClass.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerClass.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "GRA") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerGrade.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxManagerGrade.find(x => x.name == "Thêm").checked = true;
          this.checkboxManagerGrade.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerGrade.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxManagerGrade.find(x => x.name == "Sửa").checked = true;
          this.checkboxManagerGrade.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerGrade.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxManagerGrade.find(x => x.name == "Xóa").checked = true;
          this.checkboxManagerGrade.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerGrade.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "SUB") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerSubject.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxManagerSubject.find(x => x.name == "Thêm").checked = true;
          this.checkboxManagerSubject.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerSubject.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxManagerSubject.find(x => x.name == "Sửa").checked = true;
          this.checkboxManagerSubject.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerSubject.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxManagerSubject.find(x => x.name == "Xóa").checked = true;
          this.checkboxManagerSubject.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerSubject.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "CUR") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerCurriculum.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxManagerCurriculum.find(x => x.name == "Thêm").checked = true;
          this.checkboxManagerCurriculum.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerCurriculum.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxManagerCurriculum.find(x => x.name == "Sửa").checked = true;
          this.checkboxManagerCurriculum.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerCurriculum.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxManagerCurriculum.find(x => x.name == "Xóa").checked = true;
          this.checkboxManagerCurriculum.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerCurriculum.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "STG") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerGroupStudent.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxManagerGroupStudent.find(x => x.name == "Thêm").checked = true;
          this.checkboxManagerGroupStudent.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerGroupStudent.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxManagerGroupStudent.find(x => x.name == "Sửa").checked = true;
          this.checkboxManagerGroupStudent.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerGroupStudent.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxManagerGroupStudent.find(x => x.name == "Xóa").checked = true;
          this.checkboxManagerGroupStudent.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerGroupStudent.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Assign)) {
          this.checkboxManagerGroupStudent.find(x => x.name == "Ghi danh").checked = true;
          this.checkboxManagerGroupStudent.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerGroupStudent.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "LEP") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerLearningPath.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxManagerLearningPath.find(x => x.name == "Thêm").checked = true;
          this.checkboxManagerLearningPath.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerLearningPath.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxManagerLearningPath.find(x => x.name == "Sửa").checked = true;
          this.checkboxManagerLearningPath.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerLearningPath.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxManagerLearningPath.find(x => x.name == "Xóa").checked = true;
          this.checkboxManagerLearningPath.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerLearningPath.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Assign)) {
          this.checkboxManagerLearningPath.find(x => x.name == "Ghi danh").checked = true;
          this.checkboxManagerLearningPath.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerLearningPath.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "MTR") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerMeetingRoom.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxManagerMeetingRoom.find(x => x.name == "Thêm").checked = true;
          this.checkboxManagerMeetingRoom.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerMeetingRoom.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxManagerMeetingRoom.find(x => x.name == "Sửa").checked = true;
          this.checkboxManagerMeetingRoom.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerMeetingRoom.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxManagerMeetingRoom.find(x => x.name == "Xóa").checked = true;
          this.checkboxManagerMeetingRoom.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerMeetingRoom.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Assign)) {
          this.checkboxManagerMeetingRoom.find(x => x.name == "Ghi danh").checked = true;
          this.checkboxManagerMeetingRoom.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerMeetingRoom.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "QEB") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerQuestionBank.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxManagerQuestionBank.find(x => x.name == "Thêm").checked = true;
          this.checkboxManagerQuestionBank.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerQuestionBank.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxManagerQuestionBank.find(x => x.name == "Sửa").checked = true;
          this.checkboxManagerQuestionBank.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerQuestionBank.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxManagerQuestionBank.find(x => x.name == "Xóa").checked = true;
          this.checkboxManagerQuestionBank.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerQuestionBank.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Approved)) {
          this.checkboxManagerQuestionBank.find(x => x.name == "Duyệt").checked = true;
          this.checkboxManagerQuestionBank.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerQuestionBank.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "REP") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerReport.find(x => x.name == "Xem").checked = true;
        }
      }

      if (element.module == "SEP") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerSystem.find(x => x.name == "Thiết lập").checked = true;
        }
      }

      if (element.module == "ORE") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxOrganizeExam.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxOrganizeExam.find(x => x.name == "Tổ chức").checked = true;
          this.checkboxOrganizeExam.find(x => x.name == "Xem").checked = true;
          this.checkboxOrganizeExam.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "LES") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerLesson.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxManagerLesson.find(x => x.name == "Thêm").checked = true;
          this.checkboxManagerLesson.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerLesson.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxManagerLesson.find(x => x.name == "Sửa").checked = true;
          this.checkboxManagerLesson.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerLesson.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxManagerLesson.find(x => x.name == "Xóa").checked = true;
          this.checkboxManagerLesson.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerLesson.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Clone)) {
          this.checkboxManagerLesson.find(x => x.name == "Sao chép").checked = true;
          this.checkboxManagerLesson.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerLesson.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Approved)) {
          this.checkboxManagerLesson.find(x => x.name == "Duyệt").checked = true;
          this.checkboxManagerLesson.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerLesson.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Assign)) {
          this.checkboxManagerLesson.find(x => x.name == "Ghi danh").checked = true;
          this.checkboxManagerLesson.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerLesson.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      if (element.module == "EXA") {
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
          this.checkboxManagerExam.find(x => x.name == "Xem").checked = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Add)) {
          this.checkboxManagerExam.find(x => x.name == "Thêm").checked = true;
          this.checkboxManagerExam.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerExam.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Edit)) {
          this.checkboxManagerExam.find(x => x.name == "Sửa").checked = true;
          this.checkboxManagerExam.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerExam.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Delete)) {
          this.checkboxManagerExam.find(x => x.name == "Xóa").checked = true;
          this.checkboxManagerExam.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerExam.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Clone)) {
          this.checkboxManagerExam.find(x => x.name == "Sao chép").checked = true;
          this.checkboxManagerExam.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerExam.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Approved)) {
          this.checkboxManagerExam.find(x => x.name == "Duyệt").checked = true;
          this.checkboxManagerExam.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerExam.find(x => x.name == "Xem").isDisabled = true;
        }
        if (CLSPermissions.hasPermission(element.value, CLSPermissions.Assign)) {
          this.checkboxManagerExam.find(x => x.name == "Ghi danh").checked = true;
          this.checkboxManagerExam.find(x => x.name == "Xem").checked = true;
          this.checkboxManagerExam.find(x => x.name == "Xem").isDisabled = true;
        }
      }

      // if (element.module == "REP") {
      //   if (CLSPermissions.hasPermission(element.value, CLSPermissions.View)) {
      //     this.checkboxManagerReportTeacher.find(x => x.name == "Xem").checked = true;
      //   }
      // }
    });
    console.log(`this.checkboxManagerDivisionbbb`, this.checkboxManagerDivision);
  }

  isReady: boolean = false;
  constructor(@Inject(MAT_DIALOG_DATA) public userTypeInfo: any, private mdDialogRef: MatDialogRef<ContentUpdateUsertypeComponent>, private _snackBar: MatSnackBar) {
    this.initialData();

    this.userTypeEndpoint = new UserTypeEndpoint({ baseUrl: this.baseApiUrl });
    this.updateUserTypeRequest.name = userTypeInfo.name;
    this.updateUserTypeRequest.id = userTypeInfo.id;
    this.updateUserTypeRequest.userTypeRole = userTypeInfo.userTypeRole;
    this.updateUserTypeRequest.levelManage = userTypeInfo.levelManage;
    this.updateUserTypeRequest.permissionModules = JSON.parse(userTypeInfo.permissionValue);
    if (this.updateUserTypeRequest.levelManage == 'DVS') {
      let indexDIV = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'DIV');
      if (indexDIV != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexDIV, 1);
      }
      let indexGRA = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'GRA');
      if (indexGRA != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexGRA, 1);
      }
      let indexSUB = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'SUB');
      if (indexSUB != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexSUB, 1);
      }
    }
    if (this.updateUserTypeRequest.levelManage == 'SCH') {
      let indexDIV = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'DIV');
      if (indexDIV != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexDIV, 1);
      }
      let indexSCH = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'SCH');
      if (indexSCH != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexSCH, 1);
      }
      let indexGRA = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'GRA');
      if (indexGRA != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexGRA, 1);
      }
      let indexSUB = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'SUB');
      if (indexSUB != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexSUB, 1);
      }
    }
    this.isReady = true;
    console.log(`userTypeInfo`, this.userTypeInfo);
    console.log(`userTypeInfo`, this.updateUserTypeRequest);
    this.levelManageOld = userTypeInfo.levelManage;;
  }

  ngOnInit(): void {
  }


  checkForm() {
    var result = true;
    this.getCheckboxes();
    if (this.updateUserTypeRequest.levelManage == 'DVS') {
      let indexDIV = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'DIV');
      if (indexDIV != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexDIV, 1);
      }
      let indexGRA = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'GRA');
      if (indexGRA != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexGRA, 1);
      }
      let indexSUB = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'SUB');
      if (indexSUB != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexSUB, 1);
      }
    }
    if (this.updateUserTypeRequest.levelManage == 'SCH') {
      let indexDIV = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'DIV');
      if (indexDIV != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexDIV, 1);
      }
      let indexSCH = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'SCH');
      if (indexSCH != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexSCH, 1);
      }
      let indexGRA = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'GRA');
      if (indexGRA != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexGRA, 1);
      }
      let indexSUB = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'SUB');
      if (indexSUB != -1) {
        this.updateUserTypeRequest.permissionModules.splice(indexSUB, 1);
      }
    }
    if (this.updateUserTypeRequest.name === '') {
      this.lblname = 'Bạn chưa nhập tên kiểu người dùng!';
      result = false;
    }
    if (this.updateUserTypeRequest.permissionModules.length == 0) {
      this.lblPemissionModule = 'Bạn chưa chọn quyền cho người dùng!';
      result = false;
    }
    if (this.updateUserTypeRequest.name == this.userTypeInfo.name &&
      this.updateUserTypeRequest.userTypeRole == this.userTypeInfo.userTypeRole &&
      this.updateUserTypeRequest.levelManage == this.userTypeInfo.levelManage &&
      JSON.stringify(this.updateUserTypeRequest.permissionModules) == this.userTypeInfo.permissionValue
    ) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Không có gì thay đổi!',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      result = false;
    }
    return result;
  }

  submitForm(regForm: NgForm) {
    if (this.checkForm()) {
      this.userTypeEndpoint.updateUserType(this.updateUserTypeRequest)
        .then(res => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Lưu kiểu người dùng thành công!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Lưu kiểu người dùng không thành công. Vui lòng thử lại!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        })
        .catch((err) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.response.data.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
      this.mdDialogRef.close('submit')
    }
  }

  getCheckboxes() {
    let checkedUserType: CheckboxManager[] = [];
    this.checkboxUserType.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa") {
        this.checkboxUserType.find(x => x.name == "Xem").checked = true;
        this.checkboxUserType.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxUserType.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxUserType.filter(x => x.checked === true).forEach(e => {
      checkedUserType.push(e);
    });
    let valueCheckedUserType: number = 0;
    if (checkedUserType.length > 0) {
      checkedUserType.forEach(element => {
        valueCheckedUserType += element.value;
      });
      let userTypeModule: PermissionModules = {
        module: "UST",
        value: valueCheckedUserType
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'UST');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(userTypeModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'UST');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerDivision: CheckboxManager[] = [];
    this.checkboxManagerDivision.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa") {
        this.checkboxManagerDivision.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerDivision.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerDivision.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerDivision.filter(x => x.checked === true).forEach(e => {
      checkedManagerDivision.push(e);
    });
    let valueCheckedManagerDivision: number = 0;
    if (checkedManagerDivision.length > 0) {
      checkedManagerDivision.forEach(element => {
        valueCheckedManagerDivision += element.value;
      });
      let divisionModule: PermissionModules = {
        module: "DIV",
        value: valueCheckedManagerDivision
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'DIV');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(divisionModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'DIV');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerSchool: CheckboxManager[] = [];
    this.checkboxManagerSchool.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa") {
        this.checkboxManagerSchool.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerSchool.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerSchool.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerSchool.filter(x => x.checked === true).forEach(e => {
      checkedManagerSchool.push(e);
    });
    let valueCheckedManagerSchool: number = 0;
    if (checkedManagerSchool.length > 0) {
      checkedManagerSchool.forEach(element => {
        valueCheckedManagerSchool += element.value;
      });
      let schoolModule: PermissionModules = {
        module: "SCH",
        value: valueCheckedManagerSchool
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'SCH');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(schoolModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'SCH');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerUser: CheckboxManager[] = [];
    this.checkboxManagerUser.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa") {
        this.checkboxManagerUser.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerUser.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerUser.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerUser.filter(x => x.checked === true).forEach(e => {
      checkedManagerUser.push(e);
    });
    let valueCheckedManagerUser: number = 0;
    if (checkedManagerUser.length > 0) {
      checkedManagerUser.forEach(element => {
        valueCheckedManagerUser += element.value;
      });
      let userModule: PermissionModules = {
        module: "USE",
        value: valueCheckedManagerUser
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'USE');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(userModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'USE');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerClass: CheckboxManager[] = [];
    this.checkboxManagerClass.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa" || e.name == "Ghi danh") {
        this.checkboxManagerClass.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerClass.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerClass.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerClass.filter(x => x.checked === true).forEach(e => {
      checkedManagerClass.push(e);
    });
    let valueCheckedManagerClass: number = 0;
    if (checkedManagerClass.length > 0) {
      checkedManagerClass.forEach(element => {
        valueCheckedManagerClass += element.value;
      });
      let classModule: PermissionModules = {
        module: "CLA",
        value: valueCheckedManagerClass
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'CLA');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(classModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'CLA');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerGrade: CheckboxManager[] = [];
    this.checkboxManagerGrade.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa") {
        this.checkboxManagerGrade.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerGrade.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerGrade.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerGrade.filter(x => x.checked === true).forEach(e => {
      checkedManagerGrade.push(e);
    });
    let valueCheckedManagerGrade: number = 0;
    if (checkedManagerGrade.length > 0) {
      checkedManagerGrade.forEach(element => {
        valueCheckedManagerGrade += element.value;
      });
      let gradeModule: PermissionModules = {
        module: "GRA",
        value: valueCheckedManagerGrade
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'GRA');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(gradeModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'GRA');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerSubject: CheckboxManager[] = [];
    this.checkboxManagerSubject.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa") {
        this.checkboxManagerSubject.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerSubject.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerSubject.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerSubject.filter(x => x.checked === true).forEach(e => {
      checkedManagerSubject.push(e);
    });
    let valueCheckedManagerSubject: number = 0;
    if (checkedManagerSubject.length > 0) {
      checkedManagerSubject.forEach(element => {
        valueCheckedManagerSubject += element.value;
      });
      let subjectModule: PermissionModules = {
        module: "SUB",
        value: valueCheckedManagerSubject
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'SUB');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(subjectModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'SUB');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerCurriculum: CheckboxManager[] = [];
    this.checkboxManagerCurriculum.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa") {
        this.checkboxManagerCurriculum.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerCurriculum.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerCurriculum.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerCurriculum.filter(x => x.checked === true).forEach(e => {
      checkedManagerCurriculum.push(e);
    });
    let valueCheckedManagerCurriculum: number = 0;
    if (checkedManagerCurriculum.length > 0) {
      checkedManagerCurriculum.forEach(element => {
        valueCheckedManagerCurriculum += element.value;
      });
      let curriculumModule: PermissionModules = {
        module: "CUR",
        value: valueCheckedManagerCurriculum
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'CUR');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(curriculumModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'CUR');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerGroupStudent: CheckboxManager[] = [];
    this.checkboxManagerGroupStudent.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa" || e.name == "Ghi danh") {
        this.checkboxManagerGroupStudent.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerGroupStudent.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerGroupStudent.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerGroupStudent.filter(x => x.checked === true).forEach(e => {
      checkedManagerGroupStudent.push(e);
    });
    let valueCheckedManagerGroupStudent: number = 0;
    if (checkedManagerGroupStudent.length > 0) {
      checkedManagerGroupStudent.forEach(element => {
        valueCheckedManagerGroupStudent += element.value;
      });
      let groupStudentModule: PermissionModules = {
        module: "STG",
        value: valueCheckedManagerGroupStudent
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'STG');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(groupStudentModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'STG');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerLearningPath: CheckboxManager[] = [];
    this.checkboxManagerLearningPath.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa" || e.name == "Ghi danh") {
        this.checkboxManagerLearningPath.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerLearningPath.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerLearningPath.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerLearningPath.filter(x => x.checked === true).forEach(e => {
      checkedManagerLearningPath.push(e);
    });
    let valueCheckedManagerLearningPath: number = 0;
    if (checkedManagerLearningPath.length > 0) {
      checkedManagerLearningPath.forEach(element => {
        valueCheckedManagerLearningPath += element.value;
      });
      let learningPathModule: PermissionModules = {
        module: "LEP",
        value: valueCheckedManagerLearningPath
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'LEP');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(learningPathModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'LEP');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerMeetingRoom: CheckboxManager[] = [];
    this.checkboxManagerMeetingRoom.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa" || e.name == "Ghi danh") {
        this.checkboxManagerMeetingRoom.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerMeetingRoom.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerMeetingRoom.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerMeetingRoom.filter(x => x.checked === true).forEach(e => {
      checkedManagerMeetingRoom.push(e);
    });
    let valueCheckedManagerMeetingRoom: number = 0;
    if (checkedManagerMeetingRoom.length > 0) {
      checkedManagerMeetingRoom.forEach(element => {
        valueCheckedManagerMeetingRoom += element.value;
      });
      let meetingRoomModule: PermissionModules = {
        module: "MTR",
        value: valueCheckedManagerMeetingRoom
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'MTR');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(meetingRoomModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'MTR');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerQuestionBank: CheckboxManager[] = [];
    this.checkboxManagerQuestionBank.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa" || e.name == "Duyệt") {
        this.checkboxManagerQuestionBank.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerQuestionBank.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerQuestionBank.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerQuestionBank.filter(x => x.checked === true).forEach(e => {
      checkedManagerQuestionBank.push(e);
    });
    let valueCheckedManagerQuestionBank: number = 0;
    if (checkedManagerQuestionBank.length > 0) {
      checkedManagerQuestionBank.forEach(element => {
        valueCheckedManagerQuestionBank += element.value;
      });
      let questionBankModule: PermissionModules = {
        module: "QEB",
        value: valueCheckedManagerQuestionBank
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'QEB');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(questionBankModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'QEB');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerReport: CheckboxManager[] = [];
    this.checkboxManagerReport.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa") {
        this.checkboxManagerReport.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerReport.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerReport.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerReport.filter(x => x.checked === true).forEach(e => {
      checkedManagerReport.push(e);
    });
    let valueCheckedManagerReport: number = 0;
    if (checkedManagerReport.length > 0) {
      checkedManagerReport.forEach(element => {
        valueCheckedManagerReport += element.value;
      });
      let reportModule: PermissionModules = {
        module: "REP",
        value: valueCheckedManagerReport
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'REP');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(reportModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'REP');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerSystem: CheckboxManager[] = [];
    this.checkboxManagerSystem.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa") {
        this.checkboxManagerSystem.find(x => x.name == "Thiết lập").checked = true;
        this.checkboxManagerSystem.find(x => x.name == "Thiết lập").isDisabled = true;
      }
      else {
        this.checkboxManagerSystem.find(x => x.name == "Thiết lập").isDisabled = false;
      }
    });
    this.checkboxManagerSystem.filter(x => x.checked === true).forEach(e => {
      checkedManagerSystem.push(e);
    });
    let valueCheckedManagerSystem: number = 0;
    if (checkedManagerSystem.length > 0) {
      checkedManagerSystem.forEach(element => {
        valueCheckedManagerSystem += element.value;
      });
      let systemModule: PermissionModules = {
        module: "SEP",
        value: valueCheckedManagerSystem
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'SEP');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(systemModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'SEP');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedOrganizeExam: CheckboxManager[] = [];
    this.checkboxOrganizeExam.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Tổ chức") {
        this.checkboxOrganizeExam.find(x => x.name == "Xem").checked = true;
        this.checkboxOrganizeExam.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxOrganizeExam.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxOrganizeExam.filter(x => x.checked === true).forEach(e => {
      checkedOrganizeExam.push(e);
    });
    let valueCheckedOrganizeExam: number = 0;
    if (checkedOrganizeExam.length > 0) {
      checkedOrganizeExam.forEach(element => {
        valueCheckedOrganizeExam += element.value;
      });
      let organizeExamModule: PermissionModules = {
        module: "ORE",
        value: valueCheckedOrganizeExam
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'ORE');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(organizeExamModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'ORE');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerLesson: CheckboxManager[] = [];
    this.checkboxManagerLesson.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa" || e.name == "Sao chép" || e.name == "Duyệt" || e.name == "Ghi danh") {
        this.checkboxManagerLesson.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerLesson.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerLesson.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerLesson.filter(x => x.checked === true).forEach(e => {
      checkedManagerLesson.push(e);
    });
    let valueCheckedManagerLesson: number = 0;
    if (checkedManagerLesson.length > 0) {
      checkedManagerLesson.forEach(element => {
        valueCheckedManagerLesson += element.value;
      });
      let lessonModule: PermissionModules = {
        module: "LES",
        value: valueCheckedManagerLesson
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'LES');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(lessonModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'LES');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    let checkedManagerExam: CheckboxManager[] = [];
    this.checkboxManagerExam.filter(x => x.checked === true).forEach(e => {
      if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa" || e.name == "Sao chép" || e.name == "Duyệt" || e.name == "Ghi danh") {
        this.checkboxManagerExam.find(x => x.name == "Xem").checked = true;
        this.checkboxManagerExam.find(x => x.name == "Xem").isDisabled = true;
      }
      else {
        this.checkboxManagerExam.find(x => x.name == "Xem").isDisabled = false;
      }
    });
    this.checkboxManagerExam.filter(x => x.checked === true).forEach(e => {
      checkedManagerExam.push(e);
    });
    let valueCheckedManagerExam: number = 0;
    if (checkedManagerExam.length > 0) {
      checkedManagerExam.forEach(element => {
        valueCheckedManagerExam += element.value;
      });
      let examModule: PermissionModules = {
        module: "EXA",
        value: valueCheckedManagerExam
      }
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'EXA');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
      this.updateUserTypeRequest.permissionModules.push(examModule)
    }
    else {
      let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'EXA');
      if (index != -1) {
        this.updateUserTypeRequest.permissionModules.splice(index, 1);
      }
    }

    // let checkedManagerReportTeacher: CheckboxManager[] = [];
    // this.checkboxManagerReportTeacher.filter(x => x.checked === true).forEach(e => {
    //   if (e.name == "Thêm" || e.name == "Sửa" || e.name == "Xóa" || e.name == "Sao chép" || e.name == "Duyệt") {
    //     this.checkboxManagerReportTeacher.find(x => x.name == "Xem").checked = true;
    //     this.checkboxManagerReportTeacher.find(x => x.name == "Xem").isDisabled = true;
    //   }
    //   else {
    //     this.checkboxManagerReportTeacher.find(x => x.name == "Xem").isDisabled = false;
    //   }
    // });
    // this.checkboxManagerReportTeacher.filter(x => x.checked === true).forEach(e => {
    //   checkedManagerReportTeacher.push(e);
    // });
    // let valueCheckedManagerReportTeacher: number = 0;
    // if (checkedManagerReportTeacher.length > 0) {
    //   checkedManagerReportTeacher.forEach(element => {
    //     valueCheckedManagerReportTeacher += element.value;
    //   });
    //   let reportTeacherModule: PermissionModules = {
    //     module: "REP",
    //     value: valueCheckedManagerReportTeacher
    //   }
    //   let index = this.updateUserTypeRequest.permissionModules.findIndex(x => x.module == 'REP');
    //   if (index != -1) {
    //     this.updateUserTypeRequest.permissionModules.splice(index, 1);
    //   }
    //   this.updateUserTypeRequest.permissionModules.push(reportTeacherModule)
    // }







    console.log(`updateUserTypeRequest`, this.updateUserTypeRequest.permissionModules);


    console.log(`checkboxManagerDivision`, this.checkboxManagerDivision.filter(x => x.checked === true).map(x => x.name));
    console.log(`checkboxManagerSchool`, this.checkboxManagerSchool.filter(x => x.checked === true).map(x => x.name));
    console.log(`checkboxManagerUser`, this.checkboxManagerUser.filter(x => x.checked === true).map(x => x.name));
    console.log(`checkboxManagerClass`, this.checkboxManagerClass.filter(x => x.checked === true).map(x => x.name));
    console.log(`checkboxManagerGrade`, this.checkboxManagerGrade.filter(x => x.checked === true).map(x => x.name));
    console.log(`checkboxManagerSubject`, this.checkboxManagerSubject.filter(x => x.checked === true).map(x => x.name));
    console.log(`checkboxManagerCurriculum`, this.checkboxManagerCurriculum.filter(x => x.checked === true).map(x => x.name));
    console.log(`checkboxManagerGroupStudent`, this.checkboxManagerGroupStudent.filter(x => x.checked === true).map(x => x.name));
    console.log(`checkboxManagerQuestionBank`, this.checkboxManagerQuestionBank.filter(x => x.checked === true).map(x => x.name));
    console.log(`checkboxManagerReport`, this.checkboxManagerReport.filter(x => x.checked === true).map(x => x.name));
    console.log(`checkboxManagerSystem`, this.checkboxManagerSystem.filter(x => x.checked === true).map(x => x.name));
    console.log(`checkboxManagerLesson`, this.checkboxManagerLesson.filter(x => x.checked === true).map(x => x.name));
    console.log(`checkboxManagerExam`, this.checkboxManagerExam.filter(x => x.checked === true).map(x => x.name));
    // console.log(`checkboxManagerReportTeacher`, this.checkboxManagerReportTeacher.filter(x => x.checked === true).map(x => x.name));

  }

  changeSelectLevelCtrl(event: Event) {
    if (this.updateUserTypeRequest.levelManage != this.levelManageOld) {
      this.checkboxUserType.forEach(userType => {
        userType.checked = false;
      });
      this.checkboxManagerDivision.forEach(division => {
        division.checked = false;
      });
      this.checkboxManagerSchool.forEach(school => {
        school.checked = false;
      });
      this.checkboxManagerUser.forEach(user => {
        user.checked = false;
      });
      this.checkboxManagerClass.forEach(clas => {
        clas.checked = false;
      });
      this.checkboxManagerGrade.forEach(grade => {
        grade.checked = false;
      });
      this.checkboxManagerSubject.forEach(subject => {
        subject.checked = false;
      });
      this.checkboxManagerCurriculum.forEach(curriculum => {
        curriculum.checked = false;
      });
      this.checkboxManagerGroupStudent.forEach(groupStudent => {
        groupStudent.checked = false;
      });
      this.checkboxManagerLearningPath.forEach(learningPath => {
        learningPath.checked = false;
      });
      this.checkboxManagerMeetingRoom.forEach(meetingRoom => {
        meetingRoom.checked = false;
      });
      this.checkboxManagerQuestionBank.forEach(questionBank => {
        questionBank.checked = false;
      });
      this.checkboxManagerReport.forEach(report => {
        report.checked = false;
      });
      this.checkboxManagerSystem.forEach(system => {
        system.checked = false;
      });
      this.checkboxOrganizeExam.forEach(organizeExam => {
        organizeExam.checked = false;
      });
      this.checkboxManagerLesson.forEach(lesson => {
        lesson.checked = false;
      });
      this.checkboxManagerExam.forEach(exam => {
        exam.checked = false;
      });
      // this.checkboxManagerReportTeacher.forEach(reportTeacher => {
      //   reportTeacher.checked = false;
      // });
      this.updateUserTypeRequest.permissionModules = [];
    }
    this.levelManageOld = this.updateUserTypeRequest.levelManage;



    // if(this.updateUserTypeRequest.levelManage=='DVS' || this.updateUserTypeRequest.levelManage=='SCH'){
    //   this.checkboxManagerDivision.forEach(division => {
    //     division.checked=false;
    //     this.updateUserTypeRequest.permissionModules=[];
    //   });
    //   this.checkboxManagerGrade.forEach(grade => {
    //     grade.checked=false;
    //   this.updateUserTypeRequest.permissionModules=[];
    // });
    //   this.checkboxManagerSubject.forEach(subject=>{
    //     subject.checked=false;
    //   this.updateUserTypeRequest.permissionModules=[];
    // });
    //   this.checkboxManagerCurriculum.forEach(curriculum=>{
    //     curriculum.checked=false;
    //   this.updateUserTypeRequest.permissionModules=[];
    // })
    // }
    // if(this.updateUserTypeRequest.levelManage=='SCH'){
    //   this.checkboxManagerSchool.forEach(school=>{
    //     school.checked=false;
    //   this.updateUserTypeRequest.permissionModules=[];
    // })
    // }
  }
}

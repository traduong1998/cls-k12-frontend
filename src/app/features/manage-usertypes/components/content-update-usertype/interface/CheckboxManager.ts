export class CheckboxManager{
  name?:string;
  checked?:boolean;
  module?:string;
  value?:number;
  isDisabled?:boolean;
}
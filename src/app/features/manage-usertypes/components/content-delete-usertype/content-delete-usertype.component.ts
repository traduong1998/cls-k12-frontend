import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, NgForm, FormControl, Validators } from '@angular/forms';
import { NgxPermissionsModule } from 'ngx-permissions';
import { UserTypeEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { PermissionModules } from 'sdk/cls-k12-sdk-js/src/services/usertype/models/PermissionModules';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { UserType } from 'sdk/cls-k12-sdk-js/src/services/usertype/models/UserType';
import { CLSPermissions } from 'cls-k12-sdk-js/src/core/authentication/identify/cls-permissions';
import { CLSModules } from 'cls-k12-sdk-js/src/core/authentication/identify/cls-modules';
import { UpdateUserTypeRequest } from 'sdk/cls-k12-sdk-js/src/services/usertype/requests/updateUserTypeRequest';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { UsertypeOption } from 'sdk/cls-k12-sdk-js/src/services/usertype/responses/Usertype';
import { ReplaySubject, Subject } from 'rxjs';
import { pairwise, startWith, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-content-delete-usertype',
  templateUrl: './content-delete-usertype.component.html',
  styleUrls: ['./content-delete-usertype.component.scss']
})
export class ContentDeleteUsertypeComponent implements OnInit {
  userTypeEndpoint: UserTypeEndpoint;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  baseApiUrl = 'http://localhost:65000';
  protected userTypeSelect: UsertypeOption[] = [];
  public filteredUserType: ReplaySubject<UsertypeOption[]> = new ReplaySubject<UsertypeOption[]>(1);
  public userTypeSelectFilterCtrl: FormControl = new FormControl();
  protected _onDestroy = new Subject<void>();
  public filterLearningPath = this.formGroup.group({
    userTypeId: new FormControl('', Validators.required)
  })
  get userTypeId() { return this.filterLearningPath.get('userTypeId'); }

  constructor(@Inject(MAT_DIALOG_DATA) public userTypeInfo: any, private formGroup: FormBuilder, private mdDialogRef: MatDialogRef<ContentDeleteUsertypeComponent>, private _snackBar: MatSnackBar) {

    this.userTypeEndpoint = new UserTypeEndpoint({ baseUrl: this.baseApiUrl });

  }

  ngOnInit(): void {
  }
  ngAfterViewInit() {
    this.userTypeId.valueChanges
      .pipe(startWith(this.userTypeId?.value),
        pairwise())
      .subscribe(() => {
      }
      )
    // listen for search field value changes 
    this.userTypeSelectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {

        this.filterUserTypes();
      });
  }
  submitForm(regForm: NgForm) {
    debugger
    if (this.userTypeId.value == null || this.userTypeId.value == "") {
      return;
    }
    this.userTypeEndpoint.deleteUserType(this.userTypeInfo.id)
      .then(res => {
        if (res) {

          this.userTypeEndpoint.updateUserTypeAllUser(this.userTypeInfo.id, this.userTypeId.value).then(res => {
            if (res) {
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: 'Xóa kiểu người dùng thành công!',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            }
            else {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Xóa kiểu người dùng không thành công. Vui lòng thử lại!',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            }
          })
        } else {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Xóa kiểu người dùng không thành công. Vui lòng thử lại!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      })
      .catch((err: ErrorResponse) => {
        this._snackBar.open(err.errorDetail, 'Ok', {
          duration: 5000,
          panelClass: ['red-snackbar'],
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
    this.mdDialogRef.close('submit')
  }

  onUserTypeSelectHandleClick() {

    this.userTypeEndpoint.getUserTypeByLevelManage(this.userTypeInfo.levelManage)
      .then(res => {
        this.userTypeSelect = [];
        res.forEach(ut => {
          if (ut.id != this.userTypeInfo.id) {
            this.userTypeSelect.push(ut)
          }
        });
        if (this.userTypeSelect && !this.userTypeSelect.find(x => x.id == 0 || x.id == null)) {
          this.userTypeSelect.unshift({ id: null, name: 'Kiểu người dùng', levelManage: null })
        }
        this.filteredUserType.next(this.userTypeSelect.slice());
      })
  }
  protected filterUserTypes() {
    if (!this.userTypeSelect) {
      return;
    }
    // get the search keyword
    let search = this.userTypeSelectFilterCtrl.value;
    if (!search) {
      this.filteredUserType.next(this.userTypeSelect.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredUserType.next(
      this.userTypeSelect.filter(userType => userType.name.toLowerCase().indexOf(search) > -1)
    );
  }
}

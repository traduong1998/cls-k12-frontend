import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentDeleteUsertypeComponent } from './content-delete-usertype.component';

describe('ContentDeleteUsertypeComponent', () => {
  let component: ContentDeleteUsertypeComponent;
  let fixture: ComponentFixture<ContentDeleteUsertypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentDeleteUsertypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentDeleteUsertypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

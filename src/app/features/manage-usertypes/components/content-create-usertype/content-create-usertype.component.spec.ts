import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentCreateUsertypeComponent } from './content-create-usertype.component';

describe('ContentCreateUsertypeComponent', () => {
  let component: ContentCreateUsertypeComponent;
  let fixture: ComponentFixture<ContentCreateUsertypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentCreateUsertypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentCreateUsertypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

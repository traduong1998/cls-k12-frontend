import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentListUsertypesComponent } from './content-list-usertypes.component';

describe('ContentListUsertypesComponent', () => {
  let component: ContentListUsertypesComponent;
  let fixture: ComponentFixture<ContentListUsertypesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentListUsertypesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentListUsertypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

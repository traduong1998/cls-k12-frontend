import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ContentCreateUsertypeComponent } from '../../components/content-create-usertype/content-create-usertype.component';
import { ContentUpdateUsertypeComponent } from '../../components/content-update-usertype/content-update-usertype.component';
import { ContentDeleteUsertypeComponent } from '../../components/content-delete-usertype/content-delete-usertype.component';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { UserType } from 'sdk/cls-k12-sdk-js/src/services/usertype/models/UserType';
import { UserTypeEndpoint } from 'cls-k12-sdk-js/src/services/usertype/endpoints/UserTypeEndpoint';
import { UserTypeFilterRequests } from 'cls-k12-sdk-js/src/services/usertype/models/userTypePagingRequest';
import { FormControl } from '@angular/forms';

const userTypesPaginationDefault: PaginatorResponse<UserType> = {
  items: [],
  totalItems: 0
};


@Component({
  selector: 'app-content-list-usertypes',
  templateUrl: './content-list-usertypes.component.html',
  styleUrls: ['./content-list-usertypes.component.scss']
})
export class ContentListUsertypesComponent implements OnInit {
  isReady = false;
  keyWord: '';
  stringEmpty = "";
  userTypeEndpoint: UserTypeEndpoint;

  baseApiUrl = 'http://localhost:65000';

  // displayedColumns: string[] = ['select', 'stt', 'name', 'role', 'level', 'function'];
  displayedColumns: string[] = ['stt', 'name', 'donviquanly', 'role', 'level', 'function'];

  userTypesPagination: PaginatorResponse<UserType> = userTypesPaginationDefault;
  userTypesPaginationFilter: UserTypeFilterRequests = {
    pageNumber: 0,
    sizeNumber: 10,
    getCount: true,
    keyFullName: null,
    keyUsername: null,
    sortDirection: 'DESC',
    sortField: 'CRE',
  }

  selection = new SelectionModel<UserType>(true, []);
  selectedUserTypeIds = [];
  isLoadingResults = true;

  constructor(public dialog: MatDialog, private _router: Router) {
    this.userTypeEndpoint = new UserTypeEndpoint({ baseUrl: this.baseApiUrl });
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit(): void {
    this.getUserTypesData();
  }

  onEnter() {
    this.userTypesPaginationFilter = {
      keyWord: this.keyWord,
      pageNumber: 0,
      sizeNumber: 10,
      getCount: true,
      keyFullName: null,
      keyUsername: null,
      sortDirection: 'DESC',
      sortField: 'CRE',
    }
    this.getUserTypesData();
  }

  failureCallback() {
    this.isLoadingResults = false;
    alert("hihi 400");
  }

  getUserTypesData() {
    this.isLoadingResults = true;
    this.userTypeEndpoint.getUserTypesPaging(this.userTypesPaginationFilter)
      .then(res => {
        this.isLoadingResults = false;
        this.userTypesPagination = res;
        this.isReady = true;
      })
      .catch(this.failureCallback);
  }


  //constructor() { }
  openDialog() {
    const dialogRef = this.dialog.open(ContentCreateUsertypeComponent, {
      disableClose: true,
      width: '830px',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getUserTypesData()
        }
      }
    });
  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.userTypesPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.userTypesPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.getUserTypesData();
    });

  }

  /**Show dialog Update Division */
  UpdateUserType(data) {
    //show dialog
    const dialogRef = this.dialog.open(ContentUpdateUsertypeComponent, {
      disableClose: true,
      width: '830px',
      data: data
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getUserTypesData()
        }
      }
    });
  }

  /**Show dialog Update Division */
  DeleteUserType(data) {
    //show dialog
    const dialogRef = this.dialog.open(ContentDeleteUsertypeComponent, {
      disableClose: true,
      width: '555px',
      height: 'auto',
      data: data
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getUserTypesData()
        }
      }
    });
  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.userTypesPagination.items.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.selectedUserTypeIds = [];
    } else {
      this.userTypesPagination.items.forEach(row => {
        this.selection.select(row);
        this.selectedUserTypeIds.push(row.id);
      })
    }
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: UserType): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards';
import { ManageUsertypesComponent } from './manage-usertypes.component';
import { DeleteUsertypeComponent } from './views/delete-usertype/delete-usertype.component';
import { CreateUsertypeComponent } from './views/create-usertype/create-usertype.component';
import { UpdateUsertypeComponent } from './views/update-usertype/update-usertype.component';
import { ListUsertypesComponent } from './views/list-usertypes/list-usertypes.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: ManageUsertypesComponent,
    children: [
      {
        path: '',
        component: ListUsertypesComponent,
        data:{
          title: "Quản lý kiểu người dùng"
        }
      },
      {
        path: 'create',
        component: CreateUsertypeComponent
      },
      {
        path: 'update',
        component: UpdateUsertypeComponent
      },
      {
        path: 'delete',
        component: DeleteUsertypeComponent
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageUsertypesRoutingModule { }

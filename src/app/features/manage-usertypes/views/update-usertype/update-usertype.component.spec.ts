import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUsertypeComponent } from './update-usertype.component';

describe('UpdateUsertypeComponent', () => {
  let component: UpdateUsertypeComponent;
  let fixture: ComponentFixture<UpdateUsertypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateUsertypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUsertypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

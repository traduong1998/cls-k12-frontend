import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUsertypesComponent } from './list-usertypes.component';

describe('ListUsertypesComponent', () => {
  let component: ListUsertypesComponent;
  let fixture: ComponentFixture<ListUsertypesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListUsertypesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUsertypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

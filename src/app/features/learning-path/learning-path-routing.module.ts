import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddLearningPathComponent } from './views/add-learning-path/add-learning-path.component';
import { SettingLearningPathComponent } from './views/setting-learning-path/setting-learning-path.component';
import { LearningPathComponent } from './learning-path.component';
import { DetailLearningPathStepComponent } from './views/detail-learning-path-step/detail-learning-path-step.component';
import { DetailLearningPathComponent } from './views/detail-learning-path/detail-learning-path.component';
import { ListLearningPathComponent } from './views/list-learning-path/list-learning-path.component';
import { AssignStudentForLearningPathComponent } from './views/assign-student-for-learningpath/assign-student-for-learningpath.component';

const routes: Routes = [
  {
    path: '',
    component: LearningPathComponent,
    children: [
      {
        path: '',
        component: ListLearningPathComponent,
        data: {
          title: "Danh sách quy trình đào tạo"
        }
      },
      {
        path: 'assign-student-for-learningpath/:id',
        component: AssignStudentForLearningPathComponent,
        data: {
          title: "Ghi danh"
        }
      }, {
        path: 'create',
        component: AddLearningPathComponent,
        data: {
          title: "Tạo quy trình đào tạo"
        }
      }, {
        path: ':id/setting',
        component: SettingLearningPathComponent,
        data: {
          title: "Chi tiết & Chỉnh sửa quy trinh"
        }
      }
    ]
  },
  {
    path: ':learningPathId/detail-learning-path/:learningPathStepId',
    component: DetailLearningPathStepComponent
  },
  {
    path: ':learningPathId',
    component: DetailLearningPathComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LearningPathRoutingModule { }

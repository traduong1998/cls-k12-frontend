import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistCoursesDialogComponent } from './regist-courses-dialog.component';

describe('RegistCoursesDialogComponent', () => {
  let component: RegistCoursesDialogComponent;
  let fixture: ComponentFixture<RegistCoursesDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistCoursesDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistCoursesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

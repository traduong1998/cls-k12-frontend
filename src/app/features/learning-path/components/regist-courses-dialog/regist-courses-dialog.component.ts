import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { LessonEndpoint, UserEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { AuthService } from 'src/app/core/services';

const lessonsPaginationDefault: PaginatorResponse<Lesson> = {
  items: [],
  totalItems: 0
};

export interface LessonIdsResponse {
  id: number;
  name: string;
  action: string;
}

@Component({
  selector: 'app-regist-courses-dialog',
  templateUrl: './regist-courses-dialog.component.html',
  styleUrls: ['./regist-courses-dialog.component.scss']
})
export class RegistCoursesDialogComponent implements OnInit {
  request = {
    divisionId: null,
    schoolId: null,
    gradeId: null,
    subjectId: null,
    contentPanelId: null,
    chapterId: null,
    unitId: null,
    type: 'PUB',
    userId: null,
    createdDate: null,
    endDate: null,
    lessonName: null,
    page: 0,
    size: 10,
    getCount: true,
    sortDirection: 'DESC',
    sortField: 'CRE',
  };
  // displayedColumns: string[] = ['select', 'position', 'name', 'teacher', 'createdDate'];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  isLoading = true;
  pageSize = 10;
  pagingLength = 0;
  pageEvent: PageEvent;
  endpointLesson: LessonEndpoint;
  userIdentity: UserIdentity;
  endpointUser: UserEndpoint;
  lessonIdsResponses: LessonIdsResponse[] = []
  selection = new SelectionModel<Lesson>(true, []);
  lessonsPagination: PaginatorResponse<Lesson> = lessonsPaginationDefault;
  displayedColumns: string[] = ['select', 'stt', 'name', 'createdBy', 'date'];
  constructor(private _authenService: AuthService, public dialogRef: MatDialogRef<RegistCoursesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    dialogRef.disableClose = true;
    this.endpointLesson = new LessonEndpoint();
    this.endpointUser = new UserEndpoint();
    this.userIdentity = _authenService.getTokenInfo();
  }

  ngOnInit(): void {
    this.lessonIdsResponses = [];
    this.selection.clear();
    this.isLoading = true;
    this.getData()
  }

  ngAfterViewInit() {
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.lessonsPagination.items.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.lessonsPagination.items.forEach(element => {
        let data = this.data?.find(x => x.lessonId == element.id);
        if (data) {
          this.lessonIdsResponses.push({ id: element.id, name: element.name, action: 'DEL' })
          // this.lessonIdsResponses.filter(x => x.action == 'ADD')
        } else {
          let index = this.lessonIdsResponses.findIndex(x => x.id == element.id && x.action == 'ADD');
          if (index >= 0) {
            this.lessonIdsResponses.splice(index, 1);
          }
        }
      });
      this.selection.clear();
      console.log(this.lessonIdsResponses);
    } else {
      this.lessonsPagination.items.forEach(element => {
        let data = this.data?.find(x => x.lessonId == element.id);
        if (data == undefined) {
          this.lessonIdsResponses.push({ id: element.id, name: element.name, action: 'ADD' })
          // this.lessonIdsResponses.filter(x => x.action == 'DEL')
        } else {
          let index = this.lessonIdsResponses.findIndex(x => x.id == element.id && x.action == 'DEL');
          if (index >= 0) {
            this.lessonIdsResponses.splice(index, 1);
          }
        }
      });

      this.selection.select(...this.lessonsPagination.items);
      console.log(this.lessonIdsResponses);
    }
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Lesson): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    let index = this.lessonsPagination.items.findIndex(x => x.id == row.id);
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${index + 1}`;
  }
  pageChange(event?: PageEvent) {
    this.request.page = this.paginator.pageIndex + 1;
    this.request.size = this.paginator.pageSize;
    this.getData();
  }
  public onChangeSelection(eventChecked, row: Lesson) {
    if (eventChecked == null) {
      debugger;
      return;
    }
    let oldRows = this.data && [...this.data];
    this.selection.toggle(row);
    // nếu chọn thì thêm vào list request với action là add
    if (eventChecked) {
      // kiểm tra xem danh sách trả về đã tồn tại hay chưa.
      let findOld = oldRows?.find(x => x.lessonId == row.id);
      if (findOld == undefined) {
        this.lessonIdsResponses.push({ id: row.id, name: row.name, action: 'ADD' })
        // this.lessonIdsResponses.filter(x => x.action == 'DEL')
      } else {
        let index = this.lessonIdsResponses.findIndex(x => x.id == row.id && x.action == 'DEL');
        if (index >= 0) {
          this.lessonIdsResponses.splice(index, 1);
        }
      }
      console.log(this.lessonIdsResponses);
    }
    // nếu bỏ chọn thì ...
    else {

      let data = this.data?.find(x => x.lessonId == row.id);
      if (data) {
        this.lessonIdsResponses.push({ id: row.id, name: row.name, action: 'DEL' })
        // this.lessonIdsResponses.filter(x => x.action == 'ADD')
      } else {
        let index = this.lessonIdsResponses.findIndex(x => x.id == row.id && x.action == 'ADD');
        if (index >= 0) {
          this.lessonIdsResponses.splice(index, 1);
        }
      }
      console.log(this.lessonIdsResponses);
    }
  }

  public getDataSelected() {
    this.dialogRef.close(this.lessonIdsResponses);

  }
  public onCancelDialog() {
    this.dialogRef.close(null);
  }
  private getData() {
    this.endpointLesson.getLessonFilter(this.request)
      .then((res) => {
        this.selection.clear();
        this.lessonsPagination = res;
        this.lessonsPagination.items.forEach((value, index) => {
          // Tìm trong lessonIdsResponses đã có action DEL hay chưa
          // kiểm tra có danh sách trả về hay chưa 
          if (this.lessonIdsResponses) {
            let findIdResponse = this.lessonIdsResponses?.find(x => x.id == value.id && x.action == 'ADD');
            if (findIdResponse != undefined) {
              this.selection.select(value);
            }

            let findIdResponseDel = this.lessonIdsResponses.find(x => x.id == value.id && x.action == 'DEL')
            if (findIdResponseDel != undefined) {
              //ktra list response

            } else {
              //ktra list data
              let findOld = this.data && [...this.data]?.find(x => x.lessonId == value.id)
              if (findOld != undefined) {
                this.selection.select(value);
              }
            }
          } else {
            //ktra list data
            let findOld = this.data && [...this.data]?.find(x => x.lessonId == value.id)
            if (findOld != undefined) {
              this.selection.select(value);
            }
          }

        })

        this.pagingLength = res.totalItems;
        if (this.userIdentity.userTypeRole != 'SUPER') {
          this.lessonsPagination.items.forEach((value, index) => {
            value.userName = this.userIdentity.fullName;
          })
        } else {
          let userIdArr: Array<number> = [];
          this.lessonsPagination.items.forEach((value, index) => {
            userIdArr.push(value.userId);
          });
          // distinct arr user
          userIdArr = [...new Set(userIdArr)];

          // mapping name into list lesson
          this.endpointUser.GetListUserByIds(userIdArr).then(user => {
            this.lessonsPagination.items.forEach((value, index) => {
              return value.userName = user.find(x => x.id == value.userId).name;
            })
            this.isLoading = false;
          }).catch(err => {
            console.error(err)
          })
        }
        this.isLoading = false;
         this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
      })
      .catch((err) => {
        console.error(err);
      });
  }
}

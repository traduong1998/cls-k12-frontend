export interface LearningPath {
  id?: number;
  name: string;
  startDate: Date;
  endDate: Date;
  type: number;
  action: string;
  learningPathStep: LearningPathStep[];
}
export interface LearningPathStep {
  learningPathStepId?: number,
  learningPathStepName: string,
  learningPathStepStartDate: Date,
  learningPathStepEndDate: Date,
  stepType: boolean;
  action?: string;
  learningPathStepLesson?: LearningPathStepLesson[];
}

export interface LearningPathStepLesson {
  lessonId?: number;
  learningPahtStepId: number;
  action: string;
  stepLessonType?: string;
  stepLessonName?: string
}
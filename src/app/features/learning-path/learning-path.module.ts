import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { LearningPathRoutingModule } from './learning-path-routing.module';
import { LearningPathComponent } from './learning-path.component';
import { ListLearningPathComponent } from './views/list-learning-path/list-learning-path.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { SettingLearningPathComponent } from './views/setting-learning-path/setting-learning-path.component';
import { AddLearningPathComponent } from './views/add-learning-path/add-learning-path.component';
import { RegistCoursesDialogComponent } from './components/regist-courses-dialog/regist-courses-dialog.component';
import { DetailLearningPathComponent } from './views/detail-learning-path/detail-learning-path.component';
import { DetailLearningPathStepComponent } from './views/detail-learning-path-step/detail-learning-path-step.component';
import { MathModule } from 'src/app/shared/math/math.module';
import { CKEditorModule } from 'ckeditor4-angular';
import { UserLearnRoutingModule } from '../user-learn/user-learn-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContextMenuModule } from 'ngx-contextmenu';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxPaginationModule } from 'ngx-pagination';
import { AssignStudentForLearningPathComponent } from './views/assign-student-for-learningpath/assign-student-for-learningpath.component';
import { LearnMessageService } from '../learn/Services/learn-message-service.service';


@NgModule({
  declarations: [
    LearningPathComponent,
    ListLearningPathComponent,
    DetailLearningPathComponent,
    DetailLearningPathStepComponent,
    AssignStudentForLearningPathComponent,
    SettingLearningPathComponent,
    RegistCoursesDialogComponent,
    AddLearningPathComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    LearningPathRoutingModule,
    MathModule,
    CKEditorModule,
    UserLearnRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ContextMenuModule.forRoot(),
    NgxMatSelectSearchModule,
    NgxPaginationModule
  ],
  providers: [
    DatePipe,
    LearnMessageService,
  ]
})
export class LearningPathModule { }

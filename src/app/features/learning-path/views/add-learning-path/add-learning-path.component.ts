import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray, AbstractControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatRadioChange } from '@angular/material/radio';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, Subject } from 'rxjs';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { LearningPathEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learning-path/endpoints/learning-path-endpoint';
import { LearningPathModel } from 'sdk/cls-k12-sdk-js/src/services/learning-path/models/learning-path-model';
import { Courses, CreateLearningPathStepCommands, LearningPathRequest } from 'sdk/cls-k12-sdk-js/src/services/learning-path/requests/learning-path-request';
import { compareTwoDate } from 'src/app/shared/helpers/cls.helper';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { InValidDateLearningPahtValidator, InvalidDateTimeValidator } from 'src/app/shared/validations/learning-path-validation';
import { LessonIdsResponse, RegistCoursesDialogComponent } from '../../components/regist-courses-dialog/regist-courses-dialog.component';
import { LearningPath, LearningPathStepLesson } from '../../interfaces/learning-path';

@Component({
  selector: 'app-add-learning-path',
  templateUrl: './add-learning-path.component.html',
  styleUrls: ['./add-learning-path.component.scss']
})
export class AddLearningPathComponent implements OnInit {
  @ViewChild('dpFromDate') dpFromDate;
  @ViewChild('dpToDate') dpToDate;
  @ViewChild('dpFromDateStep') dpFromDateStep;
  @ViewChild('dpToDateStep') dpToDateStep;
  @ViewChild('scrollBottom') scrollBottom: ElementRef;
  learningPathClone: LearningPath;
  learningPath: LearningPath = {
    name: '',
    startDate: null,
    endDate: null,
    action: 'ADD',
    type: null,
    learningPathStep: [{
      learningPathStepName: '',
      learningPathStepEndDate: null,
      learningPathStepStartDate: null,
      stepType: null,
      learningPathStepLesson: []
    }],
  };
  learningPaths: LearningPathModel[] = [];
  formSetting: FormGroup;
  public isSave = false;
  public isHiddenDateTime = false;
  public isHiddenDateTimeStep = false;
  public id: number;
  public isChecked: boolean = true;
  today = new Date();
  learningPathRequest: LearningPath = {
    name: '',

    startDate: new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0),
    endDate: new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 59),

    learningPathStep: [],
    type: 2,
    action: "ADD",
  }

  learningPathEndpoint: LearningPathEndpoint;
  lessonEndpoint: LessonEndpoint;
  constructor(private router: ActivatedRoute,
    private fb: FormBuilder,
    private _router: Router,
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.id = +this.router.snapshot.paramMap.get('id');
    this.learningPathEndpoint = new LearningPathEndpoint();
    this.lessonEndpoint = new LessonEndpoint();
    this.formSetting = this.fb.group({
      name: new FormControl('', Validators.required),
      type: new FormControl(2),
      startDate: new FormControl(new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0)),
      endDate: new FormControl(new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 59)),
      point: new FormControl(1),
      learningPathStep: this.fb.array([
      ])
    })
  }

  ngOnInit(): void {
    let initLearnigPathStep = this.formSetting.get('learningPathStep') as FormArray;
    initLearnigPathStep.push(this.learningPathStep);
    initLearnigPathStep.at(initLearnigPathStep.length - 1).get('stepType').setValue(true);
    initLearnigPathStep.at(initLearnigPathStep.length - 1).get('firstControl').setValue(true);
    initLearnigPathStep.at(initLearnigPathStep.length - 1).get('learningPathStepStartDate').setValue(new Date());
    initLearnigPathStep.at(initLearnigPathStep.length - 1).get('learningPathStepEndDate').setValue(new Date());
    this.formSetting.patchValue(this.learningPathRequest);

    this.learningPathClone = JSON.parse(JSON.stringify(this.learningPath));
    
    this.isSave = true;

  }

  ngAfterViewInit() {

  }

  get learningPathStep(): FormGroup {
    return this.fb.group({
      learningPathStepId: [''],
      learningPathStepName: ['', Validators.required],
      learningPathStepStartDate: [''],
      stepType: [''],
      firstControl: [false],
      action: ['ADD'],
      learningPathStepEndDate: [''],
      learningPathStepLesson: this.fb.array([
      ])
    })
  }

  get type() {
    return (this.formSetting.get('type').value);
  }

  get learningPathStepLesson(): FormGroup {
    return this.fb.group({
      lessonId: [''],
      learningPathStepId: [''],
      stepLessonName: [''],
      action: ['ADD']
    })
  }
  public onCancelButtonHandleClick() {
    this._router.navigateByUrl(`dashboard/learning-path`)
  }
  public onSlideToggleChange(isEnable: boolean, index: number) {
    let allLearningPathStep = (this.formSetting.get('learningPathStep') as FormArray).at(index) as FormControl;
    if (isEnable) {
      allLearningPathStep.get('learningPathStepStartDate').enable();
      allLearningPathStep.get('learningPathStepEndDate').enable();

      let learningPathStartDateControl = this.formSetting.get('startDate');
      let learningPathEndDateControl = this.formSetting.get('endDate');
      let startDateControl = (this.pathStepAt(index) as FormGroup).controls["learningPathStepStartDate"]
      let endDateControl = (this.pathStepAt(index) as FormGroup).controls["learningPathStepEndDate"]

      this.validateDateControl(startDateControl, endDateControl);
      this.validateStartDateControlWithLearningPath(startDateControl, learningPathStartDateControl, learningPathEndDateControl);
      this.validateStartDateControlWithLearningPath(endDateControl, learningPathStartDateControl, learningPathEndDateControl);

    } else {
      allLearningPathStep.get('learningPathStepStartDate').disable();
      allLearningPathStep.get('learningPathStepEndDate').disable();
    }
  }
  public radioTypeChange($event: MatRadioChange) {
    let learningPathStep = this.getLearningPathStepControls();

    if ($event.value === 1) {
      this.isHiddenDateTime = true;

      for (let index = 0; index < learningPathStep.length; index++) {
        const element = learningPathStep[index] as FormGroup;
        element.get('stepType').setValue(false);
        element.get('stepType').disable();
      }
    } else {
      this.isHiddenDateTime = false;

      for (let index = 0; index < learningPathStep.length; index++) {
        const element = learningPathStep[index] as FormGroup;
        if (element) {

          element.get('stepType').setValue(true);
          element.get('stepType').enable();
        }
      }
    }
  }
  public addLessonsInPath(index: number) {
    let allLearningPathStep = this.formSetting.get('learningPathStep') as FormArray;
    const addLessonInPath = allLearningPathStep.at(index).get('learningPathStepLesson') as FormArray;
    addLessonInPath.push(this.learningPathStepLesson)
  }

  public getLearningPathStepLessonControls() {
    // return (this.formSetting.get('learningPathStepLesson') as FormArray).controls;
  }
  public getLearningPathStepControls() {
    return (this.formSetting.get('learningPathStep') as FormArray).controls;
  }

  public onRemovePathHandleClick(index: number) {
    const remove = this.formSetting.get('learningPathStep') as FormArray;
    if (remove.length <= 1 && index == 0) {
      return;
    }
    this.openDialog().subscribe(res => {
      if (res) {
        remove.removeAt(index)
      }
    })
  }

  public onAddPathHandleClick() {
    const add = this.formSetting.get('learningPathStep') as FormArray;
    add.push(this.learningPathStep);
    let learningStepLessons = add.at(add.length - 1).get('learningPathStepLesson') as FormArray;
    this.learningPath.learningPathStep.push({
      learningPathStepId: null,
      learningPathStepName: '',
      action: 'ADD',
      learningPathStepStartDate: null,
      learningPathStepEndDate: null,
      stepType: false
    })

    let type = this.formSetting.get('type').value
    if (type == 1) {
      this.isHiddenDateTime = true;

      const element = add.at(add.length - 1) as FormGroup;
      element.get('stepType').setValue(false);
      element.get('stepType').disable();

    } else {

      this.isHiddenDateTime = false;
      const element = add.at(add.length - 1) as FormGroup;
      if (element) {
        element.get('stepType').setValue(true);
        element.get('stepType').enable();
      }
    }
    setTimeout(() => {
      this.scrollToElement();
    }, 300);
  }

  public onSubmit() {
  }

  public learningPathStepControl() {
    return this.formSetting.get('learningPathStep') as FormArray
  }

  public submitButtonHandleClick() {
    this.spinner.show();
    this.isSave = true;
    let learningPathArr = this.learningPathStepControl();

    if (learningPathArr.invalid) {
      learningPathArr.markAllAsTouched();
      learningPathArr.markAsTouched();
      this.isSave = false;
      this.failToast(' Hãy nhập đầy đủ thông tin !');
      this.spinner.hide();
      return;
    }
    if (this.formSetting.get('type').value == 2) {
      if ((!this.formSetting.get('startDate').value || !this.formSetting.get('endDate').value)) {
        this.failToast(" Chọn ngày bắt đầu và ngày kết thúc cho kiểu không thường xuyên ");
        this.isSave = false;
        this.spinner.hide();
        return;

      }
      if (compareTwoDate(new Date(this.formSetting.get('startDate').value), new Date(this.formSetting.get('endDate').value))) {
        this.failToast(" Ngày bắt đầu phải nhỏ hơn ngày kết thúc ");
        this.isSave = false;
        this.spinner.hide();
        return;
      }
    }

    if (!this.formSetting.get('name')) {
      this.failToast("Tên không được để trống ! ")
      this.isSave = false;
      this.spinner.hide();
      return;
    }

    let stepArr: CreateLearningPathStepCommands[] = [];

    let stepArrControl = this.learningPathStepControl();
    for (let index = 0; index < stepArrControl.length; index++) {
      const element = stepArrControl.at(index) as FormGroup;

      let coursesArrControl = element.get('learningPathStepLesson') as FormArray;
      if (!coursesArrControl.controls.find(x => x.get('lessonId').value > 0)) {
        this.failToast(" Hãy nhập khóa học !");

        this.isSave = false;
        this.spinner.hide();
        return;
      }

      let step: CreateLearningPathStepCommands = {
        name: element.get('learningPathStepName').value,
      };

      step.id = element.get('learningPathStepId')?.value;
      if (step.id) {
        step.action
          = this.learningPath.learningPathStep.find(x => x.learningPathStepId == step.id)?.action;
      } else {
        step.action = element.get('action')?.value
      }
      if (element.get('stepType').value) {
        let learningPathStartDate = element.get('learningPathStepStartDate').value;
        let learningPathStepEndDate = element.get('learningPathStepEndDate').value;

        if (compareTwoDate(new Date(learningPathStartDate), new Date(learningPathStepEndDate))) {
          this.failToast(" Ngày bắt đầu phải nhỏ hơn ngày kết thúc !");

          this.isSave = false;
          this.spinner.hide();
          return;
        }

        step.startDate = new Date(new Date(learningPathStartDate).getFullYear(), new Date(learningPathStartDate).getMonth(), new Date(learningPathStartDate).getDate(), 0, 0, 0);
        step.endDate = new Date(new Date(learningPathStepEndDate).getFullYear(), new Date(learningPathStepEndDate).getMonth(), new Date(learningPathStepEndDate).getDate(), 23, 59, 59);
      } else {
        step.startDate = null;
        step.endDate = null;
      }

      let courseArr: Courses[] = [...this.learningPath.learningPathStep[index]?.learningPathStepLesson];
      step.courses = courseArr && [...courseArr.filter(x => x.id === 0 || x.id == null)];

      if (this.formSetting.invalid || stepArrControl.invalid) {
        this.formSetting.markAllAsTouched();
        this.failToast(' Hãy nhập đầy đủ thông tin !');
        this.isSave = false;
        this.spinner.hide();
        return;
      }

      stepArr.push(step);
    }

    let request: LearningPathRequest = {
      id: this.learningPath.id,
      name: this.formSetting.get('name').value,
      type: this.formSetting.get('type').value == 1 ? "ALW" : "NAL",
      startDate: new Date(new Date(this.formSetting.get('startDate').value).getFullYear(), new Date(this.formSetting.get('startDate').value).getMonth(), new Date(this.formSetting.get('startDate').value).getDate(), 0, 0, 0),
      endDate: new Date(new Date(this.formSetting.get('endDate').value).getFullYear(), new Date(this.formSetting.get('endDate').value).getMonth(), new Date(this.formSetting.get('endDate').value).getDate(), 23, 59, 59),
      createLearningPathStepCommands: [...stepArr]
    }

    if (JSON.stringify(this.learningPathClone) == JSON.stringify(this.learningPath)) {
      this.spinner.hide();
      this.failToast("Dữ liệu không thay đổi !");
    }
    this.learningPathEndpoint.create(request).then(res => {
      if (res) {
        this.isSave = true
        this.spinner.hide();
        this.successToast(" Tạo thành công !");
        this._router.navigateByUrl('dashboard/learning-path');
      } else {
        this.isSave = true
        this.spinner.hide();
        this.failToast("Tạo thất bại !")
      }
    })
      .catch(res => {
        console.error(res);
        this.spinner.hide();
        this.failToast(" Có lỗi xảy ra ! ")
      });
  }

  public openDpFromDate() {
    this.dpFromDate.open();
  }
  public openDpToDate() {
    this.dpToDate.open();
  }
  public openDpFromDateStep(i) {
    debugger;
    document.getElementById(`dpFromDateStep_${i}`).click()
  }
  public openDpToDateStep() {
    // this.dpToDateStep.open();
  }

  public pathStepAt(index: number) {
    return (<FormArray>this.formSetting.get('learningPathStep')).at(index);
  }
  public path() {
    return (<FormArray>this.formSetting.get('name'));;
  }

  public removeCourseClick(i: number, pathLessonGroup: number, lessonId: number) {
    let stepControl = this.pathStepAt(i) as FormGroup;
    (stepControl.get('learningPathStepLesson') as FormArray).removeAt(pathLessonGroup);
    this.learningPath.learningPathStep[i].learningPathStepLesson.splice(pathLessonGroup, 1);
    this.formSetting.patchValue(this.learningPath);
  }

  public registerCoursesDiaglog(index: number) {
    let courseData = this.learningPath.learningPathStep[index]?.learningPathStepLesson?.filter(x => x.lessonId && x.action != 'DEL');

    const registerCourse = this.dialog.open(RegistCoursesDialogComponent, {
      minWidth: '996px',
      minHeight: '600px',
      data: courseData
    });

    registerCourse.afterClosed().subscribe((results: LessonIdsResponse[]) => {
      if (results) {
        let learningPathStepForm = this.pathStepAt(index) as FormGroup;
        let learningPathStepLessonFormArr = learningPathStepForm.get('learningPathStepLesson') as FormArray;
        let learningPathStepIndex = this.learningPath.learningPathStep[index];
        if (!learningPathStepIndex.learningPathStepLesson) {
          learningPathStepIndex.learningPathStepLesson = [];
        }

        this.learningPath.name = this.formSetting.controls['name'].value;
        this.learningPath.startDate = this.formSetting.controls['startDate'].value;
        this.learningPath.endDate = this.formSetting.controls['endDate'].value;
        this.learningPath.type = this.formSetting.controls['type'].value;

        learningPathStepIndex.learningPathStepName = learningPathStepForm.controls['learningPathStepName'].value;
        learningPathStepIndex.learningPathStepStartDate = learningPathStepForm.controls['learningPathStepStartDate'].value;
        learningPathStepIndex.learningPathStepEndDate = learningPathStepForm.controls['learningPathStepEndDate'].value;
        learningPathStepIndex.stepType = learningPathStepForm.controls['stepType'].value;

        results.forEach(res => {
          switch (res.action) {
            case 'ADD':
              let find = learningPathStepIndex?.learningPathStepLesson?.find(x => x.lessonId == res.id);

              if (!find) {
                learningPathStepLessonFormArr.push(this.learningPathStepLesson);
                learningPathStepIndex.learningPathStepLesson.push({
                  lessonId: res.id,
                  action: res.action,
                  stepLessonName: res.name,
                  learningPahtStepId: learningPathStepIndex.learningPathStepId,
                })
              }
              break;
            case 'DEL':
              let indexForm = learningPathStepLessonFormArr.controls.findIndex(x => x.get('lessonId').value == res.id);
              if (indexForm > -1) {
                learningPathStepLessonFormArr.removeAt(indexForm);
              }
              let indexValue = learningPathStepIndex.learningPathStepLesson.findIndex(x => x.lessonId == res.id);
              if (indexValue > -1) {
                learningPathStepIndex.learningPathStepLesson.splice(indexValue, 1)
              }
              break;
            default:
              break;
          }
        })

        // this.formSetting.patchValue(this.learningPath);
        learningPathStepForm.patchValue(this.learningPath.learningPathStep[index]);

      }
    });
  }

  public getValidateStartDateErrors(): boolean {
    if (this.formSetting.controls['startDate'].errors?.invalidDateFromTo) {
      return true;
    }
    return false;
  }

  public getValidateStartDateRequiredErrors() {
    if (this.formSetting.controls['startDate'].errors?.required) {
      return true;
    }
    return false;
  }

  public getValidateEndDateRequiredErros() {
    if (this.formSetting.controls['endDate'].errors?.required) {
      return true;
    }
    return false;
  }

  public getNamePathStepValidation(index) {
    if ((this.pathStepAt(index) as FormGroup).controls['learningPathStepName'].errors?.required) {
      return true;
    }
    return false;
  }

  public getstepTypeControl(index: number): boolean {
    let pathStep = this.pathStepAt(index) as FormGroup;
    return pathStep.get('stepType').value
  }

  public getValidatorPathStepControl(index: number): boolean {
    let control = this.pathStepAt(index).get('learningPathStepStartDate') as FormControl;
    if (control.errors?.invalidDateFromTo) {
      return true;
    }

    return false;
  }

  public getValidatorPathStepEndDateControl(index: number): boolean {
    let control = this.pathStepAt(index).get('learningPathStepEndDate') as FormControl;
    if (control.errors?.invalidDateFromTo) {
      return true;
    }

    return false;
  }

  public getValidateStartDatePathStepRequiredErrors(index) {
    if ((this.pathStepAt(index) as FormGroup).controls['learningPathStepStartDate'].errors?.required) {
      return true;
    }
    return false;
  }

  public getValidateEndDatePathStepRequiredErrors(index) {
    if ((this.pathStepAt(index) as FormGroup).controls['learningPathStepEndDate'].errors?.required) {
      return true;
    }
    return false;
  }

  public valueInputStepChange(index: number) {
    this.learningPath.learningPathStep[index].action = "ADD";
  }

  public onDatelearnPathStepChange(index: number) {
    let learningPathStartDateControl = this.formSetting.get('startDate');
    let learningPathEndDateControl = this.formSetting.get('endDate');
    let startDateControl = (this.pathStepAt(index) as FormGroup).controls["learningPathStepStartDate"]
    let endDateControl = (this.pathStepAt(index) as FormGroup).controls["learningPathStepEndDate"]
    let stepType = this.pathStepAt(index).get("stepType").value;
    if (stepType) {
      this.validateDateControl(startDateControl, endDateControl);
      this.validateStartDateControlWithLearningPath(startDateControl, learningPathStartDateControl, learningPathEndDateControl);
      this.validateStartDateControlWithLearningPath(endDateControl, learningPathStartDateControl, learningPathEndDateControl);
    }
    this.learningPath.learningPathStep[index].action == "ADD" ? '' : this.learningPath.learningPathStep[index].action == "EDI";
  }
  private convertDate(value: string, option: number): Date {
    let year = new Date(value).getFullYear();
    let month = new Date(value).getMonth();
    let day = new Date(value).getDate();
    if (option == 1) {
      return new Date(year, month, day, 0, 0, 0)
    } else {
      return new Date(year, month, day, 23, 59, 59)
    }
  }

  private validateStartDateControlWithLearningPath(dateControlCompare: AbstractControl, startDateControl: AbstractControl, endDateControl: AbstractControl) {
    if (dateControlCompare.value && startDateControl.value as Date && endDateControl.value as Date) {
      let dateCompare = new Date(dateControlCompare.value).getTime();
      let startDateCompareDate = this.convertDate(startDateControl.value, 1);
      let startDateCompare = startDateCompareDate.getTime();
      let endDateCompareDate = this.convertDate(endDateControl.value, 0);
      let endDateDateCompare = endDateCompareDate.getTime();
      if ((dateCompare < startDateCompare)
        || (dateCompare > endDateDateCompare)) {
        dateControlCompare
          .setValidators(
            [
              InValidDateLearningPahtValidator(startDateCompareDate, endDateCompareDate)
            ]
          )
        dateControlCompare.updateValueAndValidity();
        dateControlCompare.markAsTouched();
        dateControlCompare.markAsDirty();

      }
      else {
        debugger;
        if (dateControlCompare.hasError('invalidDateLearningPath')) {
          // const { invalidDateLearningPath, ...errors } = dateControlCompare.errors;
          dateControlCompare.setErrors(null);
          dateControlCompare.clearValidators();
          dateControlCompare.updateValueAndValidity();
          // console.log('errorToRemove', invalidDateLearningPath)
        }
      }
    } else {
      // dateControlCompare.setErrors({ 'invalidDateLearningPath': null });
      dateControlCompare.clearValidators();
      if (!dateControlCompare.value) {
        dateControlCompare
          .setValidators([Validators.required])
        dateControlCompare.updateValueAndValidity();
        dateControlCompare.markAsTouched();
        dateControlCompare.markAsDirty();
      }
      if (!endDateControl.value) {
        endDateControl
          .setValidators([Validators.required])
        endDateControl.updateValueAndValidity();
        endDateControl.markAsTouched();
        endDateControl.markAsDirty();
      }
    }
  }

  private validateDateControl(startDateControl: AbstractControl, endDateControl: AbstractControl) {

    if (new Date(startDateControl.value) && new Date(endDateControl.value)) {
      let startDate = this.convertDate(startDateControl.value, 1);
      let endDate = this.convertDate(endDateControl.value, 0);
      if (startDate.getTime() > endDate.getTime()) {
        startDateControl
          .setValidators(
            [
              InvalidDateTimeValidator(new Date(endDateControl.value))
            ]
          )
        startDateControl.updateValueAndValidity();
        startDateControl.markAsTouched();
        startDateControl.markAsDirty();
      } else {
        startDateControl.setErrors(null);
        startDateControl.clearValidators();
        startDateControl.updateValueAndValidity();
      }
    } else {
      startDateControl.setErrors(null);
      startDateControl.clearValidators();
      if (!startDateControl.value) {
        startDateControl
          .setValidators([Validators.required])
        startDateControl.updateValueAndValidity();
        startDateControl.markAsTouched();
        startDateControl.markAsDirty();
      }
      if (!endDateControl.value) {
        endDateControl
          .setValidators([Validators.required])
        endDateControl.updateValueAndValidity();
        endDateControl.markAsTouched();
        endDateControl.markAsDirty();
      }
    }
  }
  public onDateLearningPathChange(value: any) {
    debugger;
    let startDateControl = this.formSetting.controls["startDate"];
    let endDateControl = this.formSetting.controls["endDate"];
    let type: number = this.formSetting.controls['type'].value
    if (type == 2) {
      this.validateDateControl(startDateControl, endDateControl);
    }

    let stepLearningPath = this.formSetting.get('learningPathStep') as FormArray;
    stepLearningPath && stepLearningPath.controls.forEach((element, index) => {
      this.onDatelearnPathStepChange(index);
    });
  }

  public getActionPathStepLessons(i: number, index: number) {
    let pathStep = this.pathStepAt(i) as FormGroup;
    let pathStepLessons = pathStep.get('learningPathStepLesson') as FormArray;
    let formGroup = pathStepLessons.at(index) as FormGroup;
    return (formGroup.get('action') as FormControl).value !== 'DEL';
  }

  public getValidatorPathStepWithStartDateLearningPathControl(index: number): boolean {
    if (this.pathStepAt(index).get('learningPathStepStartDate').errors?.invalidDateLearningPath) {
      return true;
    }

    return false;
  }

  public getValidatorPathStepWithEndDateLearningPathControl(index: number): boolean {
    if (this.pathStepAt(index).get('learningPathStepEndDate').errors?.invalidDateLearningPath) {
      return true;
    }

    return false;
  }

  public getValidatorCourses(index): boolean {
    let stepArrControl = this.learningPathStepControl();
    const element = stepArrControl.at(index) as FormGroup;

    let coursesArrControl = element.get('learningPathStepLesson') as FormArray;
    if (!coursesArrControl.controls.find(x => x.get('lessonId').value > 0)) {
      return true;
    }

    return false;
  }

  public successToast(message?: string) {
    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
      data: message ?? 'Thêm thành công!',
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  public failToast(message?: string) {
    this._snackBar.openFromComponent(ErrorSnackBarComponent, {
      data: message ?? "Thêm thất bại",
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    })
  }

  openDialog(): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '303px',
      data: {
        message: ' Bạn có chắc chắn muốn xóa?',
        buttonText: {
          ok: 'Xóa',
          cancel: 'Hủy',
          colorButtonSubmit: 'warn'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }

  scrollToElement(): void {
    this.scrollBottom.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }
}
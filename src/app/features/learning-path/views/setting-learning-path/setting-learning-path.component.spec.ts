import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingLearningPathComponent } from './setting-learning-path.component';

describe('SettingLearningPathComponent', () => {
  let component: SettingLearningPathComponent;
  let fixture: ComponentFixture<SettingLearningPathComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingLearningPathComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingLearningPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

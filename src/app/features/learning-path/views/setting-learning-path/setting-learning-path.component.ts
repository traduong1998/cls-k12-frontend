import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatRadioChange } from '@angular/material/radio';
import { ActivatedRoute, Router } from '@angular/router';
import { LearningPathEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learning-path/endpoints/learning-path-endpoint';
import { LearningPathModel } from 'sdk/cls-k12-sdk-js/src/services/learning-path/models/learning-path-model';
import { Courses, CreateLearningPathStepCommands, LearningPathRequest } from 'sdk/cls-k12-sdk-js/src/services/learning-path/requests/learning-path-request';
import { LearningPath, LearningPathStep, LearningPathStepLesson } from '../../interfaces/learning-path';
import 'rxjs/add/operator/map'
import { MatSnackBar } from '@angular/material/snack-bar';
import { compareTwoDate } from 'src/app/shared/helpers/cls.helper';
import { InValidDateLearningPahtValidator, InvalidDateTimeValidator } from 'src/app/shared/validations/learning-path-validation';
import { MatDialog } from '@angular/material/dialog';
import { LessonIdsResponse, RegistCoursesDialogComponent } from '../../components/regist-courses-dialog/regist-courses-dialog.component';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { Observable, Subject } from 'rxjs';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';

@Component({
  selector: 'app-setting-learning-path',
  templateUrl: './setting-learning-path.component.html',
  styleUrls: ['./setting-learning-path.component.scss']
})
export class SettingLearningPathComponent implements OnInit {
  @ViewChild('dpFromDate') dpFromDate;
  @ViewChild('dpToDate') dpToDate;
  @ViewChild('dpFromDateStep') dpFromDateStep;
  @ViewChild('dpToDateStep') dpToDateStep;
  @ViewChild('scrollBottom') scrollBottom: ElementRef;
  learningPath: LearningPath;
  learningPathClone: LearningPathRequest;
  learningPaths: LearningPathModel[] = [];
  formSetting: FormGroup;
  removeListStepId: number[] = [];
  public point = 1;
  public learningPathStepDels: LearningPathStep[] = [];
  public learningPathStepLessonDels: LearningPathStepLesson[] = []
  public minDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 0, 0, 0, 0);
  public isHiddenDateTime = false;
  public isHiddenDateTimeStep = false;
  public id: number;
  public isChecked: boolean = true;
  public isSave = false;
  learningPathEndpoint: LearningPathEndpoint;
  lessonEndpoint: LessonEndpoint;
  constructor(
    private router: ActivatedRoute,
    private fb: FormBuilder,
    private _router: Router,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.id = +this.router.snapshot.paramMap.get('id');
    this.learningPathEndpoint = new LearningPathEndpoint();
    this.lessonEndpoint = new LessonEndpoint();
    this.formSetting = this.fb.group({
      name: new FormControl('', Validators.required),
      type: new FormControl(),
      startDate: new FormControl(),
      point: new FormControl(1),
      endDate: new FormControl(),
      learningPathStep: this.fb.array([
      ])
    })
  }

  ngOnInit(): void {
    this.isSave = true;
  }

  ngAfterViewInit() {
    this.learningPathEndpoint.getById(this.id).then(res => {
      this.learningPaths = [...res];
      let listLessonIds: number[] = []
      res.map(value => {
        if (value.lessonId) {
          listLessonIds.push(value.lessonId);
        }
      })

      listLessonIds.filter(x => x != undefined);
      this.lessonEndpoint.getLessonByListId(listLessonIds).then(res => {
        let learningPathStepJsons: LearningPathStep[] = [];
        let learningPathStepLessons: LearningPathStepLesson[]
        let privious = 0;
        this.learningPaths.map((value, index) => {
          learningPathStepLessons = []
          let type = (value.learningPathStepStartDate && value.learningPathStepEndDate) ? true : false
          if (!(value.learningPathStepId == privious)) {
            let learningPathStepJson: LearningPathStep =
            {
              learningPathStepId: value.learningPathStepId,
              learningPathStepName: value.learningPathStepName,
              learningPathStepStartDate: value.learningPathStepStartDate,
              learningPathStepEndDate: value.learningPathStepEndDate,
              action: "NON",
              stepType: type
            }
            this.learningPaths.map(stepLesson => {
              if (stepLesson.learningPathStepLesson_learningPathStepId == value.learningPathStepId) {
                learningPathStepLessons.push(
                  {
                    learningPahtStepId: stepLesson.learningPathStepLesson_learningPathStepId,
                    lessonId: stepLesson.lessonId,
                    stepLessonName: res.find(x => x.id === stepLesson.lessonId)?.name,
                    stepLessonType: "Get",
                    action: "NON"
                  }
                )
              }
            })
            if (learningPathStepLessons.length === 0) {
              let learningPathStepLessonDefault: LearningPathStepLesson = {
                learningPahtStepId: 0,
                lessonId: 0,
                stepLessonType: "Get",
                action: "NON",
                stepLessonName: 'chưa có bài giảng nào '
              }
              learningPathStepLessons.push(learningPathStepLessonDefault);
            }
            learningPathStepJson.learningPathStepLesson = [...learningPathStepLessons]

            learningPathStepJsons.push(learningPathStepJson);

            privious = value.learningPathStepId;
          }

        })
        let learningPath = this.learningPaths[0];
        let type = learningPath.type.includes("ALW") ? 1 : 2
        this.learningPath = {
          id: learningPath.id,
          name: learningPath.name,
          startDate: learningPath.startDate,
          endDate: learningPath.endDate,
          action: "NON",
          type: type,
          learningPathStep: [...learningPathStepJsons]
        }


        for (let line = 0; line < this.learningPath.learningPathStep.length; line++) {
          let learningPathStepFormArr = this.formSetting.get('learningPathStep') as FormArray;
          learningPathStepFormArr.push(this.learningPathStep);
          if (this.learningPath.learningPathStep) {
            let learningPathStepLessons = this.learningPath.learningPathStep[line]?.learningPathStepLesson.length;
            for (let j = 0; j < learningPathStepLessons; j++) {
              let learningPathStepLessonFromArr = learningPathStepFormArr.at(line).get('learningPathStepLesson') as FormArray;
              learningPathStepLessonFromArr.push(this.learningPathStepLesson);
            }
          }
        }

        this.formSetting.patchValue(this.learningPath);

        if (this.type === 1) {
          this.isHiddenDateTime = true;
          let learningPathStep = this.formSetting.get('learningPathStep') as FormArray;
          for (let index = 0; index < learningPathStep.length; index++) {
            const element = learningPathStep.at(index) as FormGroup;
            element.get('stepType').setValue(false);
            element.get('stepType').disable();
          }

        } else {
          this.isHiddenDateTime = false;
        }
        this.learningPathClone = JSON.parse(JSON.stringify(this.learningPath));
      });

    });

  }

  get learningPathStep(): FormGroup {
    return this.fb.group({
      learningPathStepId: [],
      learningPathStepName: ['', Validators.required],
      learningPathStepStartDate: [],
      stepType: [],
      action: "ADD",
      learningPathStepEndDate: [],
      learningPathStepLesson: this.fb.array([
      ])
    })
  }

  get type() {
    return (this.formSetting.get('type').value);
  }

  get learningPathStepLesson(): FormGroup {
    return this.fb.group({
      lessonId: [0],
      learningPathStepId: [],
      stepLessonName: [],
      action: "ADD"
    })
  }

  public getActionPathStepLessons(i: number, index: number) {
    let pathStep = this.pathStepAt(i) as FormGroup;
    let pathStepLessons = pathStep.get('learningPathStepLesson') as FormArray;
    let formGroup = pathStepLessons.at(index) as FormGroup;
    return (formGroup.get('action') as FormControl).value !== 'DEL';
  }
  public onCancelButtonHandleClick() {
    this._router.navigateByUrl(`dashboard/learning-path`)
  }
  public onSlideToggleChange(isEnable: boolean, index: number) {
    let allLearningPathStep = (this.formSetting.get('learningPathStep') as FormArray).at(index) as FormControl;
    if (isEnable) {
      let learningPathStartDateControl = this.formSetting.get('startDate');
      let learningPathEndDateControl = this.formSetting.get('endDate');

      let startDateControl = (this.pathStepAt(index) as FormGroup).controls["learningPathStepStartDate"]
      let endDateControl = (this.pathStepAt(index) as FormGroup).controls["learningPathStepEndDate"]

      startDateControl.enable();
      startDateControl.setValidators([Validators.required]);
      startDateControl.updateValueAndValidity();

      endDateControl.enable();
      endDateControl.setValidators([Validators.required]);
      endDateControl.updateValueAndValidity();



      this.validateDateControl(startDateControl, endDateControl);
      this.validateStartDateControlWithLearningPath(startDateControl, learningPathStartDateControl, learningPathEndDateControl);
      this.validateStartDateControlWithLearningPath(endDateControl, learningPathStartDateControl, learningPathEndDateControl);

    } else {
      allLearningPathStep.get('learningPathStepStartDate').disable();
      allLearningPathStep.get('learningPathStepEndDate').disable();
    }
  }

  public valueInputStepChange(index: number) {
    debugger;
    this.learningPath.learningPathStep[index].action == 'ADD' ? '' : this.learningPath.learningPathStep[index].action = 'EDI';
  }

  public radioTypeChange($event: MatRadioChange) {
    let learningPathStep = this.getLearningPathStepControls();

    if ($event.value === 1) {
      this.isHiddenDateTime = true;

      for (let index = 0; index < learningPathStep.length; index++) {
        const element = learningPathStep[index] as FormGroup;
        element.get('stepType').setValue(false);
        element.get('stepType').disable();

        // set validator cho trường hợp không thường xuyên
        let learningPathStartDateControl = this.formSetting.get('startDate');
        learningPathStartDateControl.setValidators(null);
        learningPathStartDateControl.updateValueAndValidity();

        let learningPathEndDateControl = this.formSetting.get('endDate');
        learningPathEndDateControl.setValidators(null);
        learningPathEndDateControl.updateValueAndValidity();

        let startDateControl = (this.pathStepAt(index) as FormGroup).controls["learningPathStepStartDate"]
        let endDateControl = (this.pathStepAt(index) as FormGroup).controls["learningPathStepEndDate"]

        startDateControl.setValidators(null);
        startDateControl.updateValueAndValidity();

        endDateControl.setValidators(null);
        endDateControl.updateValueAndValidity();
        element.markAllAsTouched();
      }
    } else {
      this.isHiddenDateTime = false;

      for (let index = 0; index < learningPathStep.length; index++) {
        const element = learningPathStep[index] as FormGroup;
        (element.get('stepType') as FormControl)?.setValue(true);
        (element.get('stepType') as FormControl)?.enable();

        // set validator cho trường hợp không thường xuyên
        let learningPathStartDateControl = this.formSetting.get('startDate');
        learningPathStartDateControl.setValidators([Validators.required]);
        learningPathStartDateControl.updateValueAndValidity();

        let learningPathEndDateControl = this.formSetting.get('endDate');
        learningPathEndDateControl.setValidators([Validators.required]);
        learningPathEndDateControl.updateValueAndValidity();

        let startDateControl = (this.pathStepAt(index) as FormGroup).controls["learningPathStepStartDate"]
        let endDateControl = (this.pathStepAt(index) as FormGroup).controls["learningPathStepEndDate"]

        startDateControl.enable();
        startDateControl.setValidators([Validators.required]);
        startDateControl.updateValueAndValidity();

        endDateControl.enable();
        endDateControl.setValidators([Validators.required]);
        endDateControl.updateValueAndValidity();
        element.markAllAsTouched();
      }
    }
  }

  public addLessonsInPath(index: number) {
    let allLearningPathStep = this.formSetting.get('learningPathStep') as FormArray;
    const addLessonInPath = allLearningPathStep.at(index).get('learningPathStepLesson') as FormArray;
    addLessonInPath.push(this.learningPathStepLesson)
  }

  public getValidatorCourses(index): boolean {
    if (this.pathStepAt(index).get('learningPathStepLesson').errors?.required) {
      return true;
    }

    return false;
  }

  public getLearningPathStepLessonControls(i) {
    return ((this.pathStepAt(i).get('learningPathStepLesson') as FormArray).controls.filter(x => x.get('action').value != 'DEL'));
  }

  public getLearningPathStepControls() {
    return (this.formSetting.get('learningPathStep') as FormArray).controls;
  }

  public onRemovePathHandleClick(index: number) {
    console.log("111")
    if (index == 0 && this.getLearningPathStepControls().length == 1) {
      return;
    }

    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      disableClose: true,
      width: '444px',
      height: 'auto',
      //data: data
      data: {
        title: 'Xóa bước',
        name: `Bước ${index + 1}`,
        message: ''
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
        const remove = this.formSetting.get('learningPathStep') as FormArray;
        let element = remove.at(index) as FormGroup;
        let id = +element.controls['learningPathStepId'].value;

        // case : add and not save yet
        if (!id) {
          remove.removeAt(index);
          return;
        }

        // case saved and remove
        let objectRemove = this.learningPath.learningPathStep.find(x => x.learningPathStepId === id);
        let objectRemoveIndex = this.learningPath.learningPathStep.findIndex(x => x.learningPathStepId === id);
        if (!objectRemove) {
          return;
        } else {
          objectRemove.action = "DEL";
          this.learningPathStepDels.push(objectRemove);
          this.learningPath.learningPathStep.splice(objectRemoveIndex, 1);
        }
        remove.removeAt(index);
      }
      else {
        this.isSave = false;
      }
    });
    this.formSetting.patchValue(this.learningPath);
  }

  public onAddPathHandleClick() {
    let add = this.formSetting.get('learningPathStep') as FormArray;
    add.push(this.learningPathStep);
    // let learningStepLessons = add.at(add.length - 1).get('learningPathStepLesson') as FormArray;
    this.learningPath.learningPathStep.push({
      learningPathStepId: null,
      learningPathStepName: '',
      action: 'ADD',
      learningPathStepStartDate: null,
      learningPathStepEndDate: null,
      stepType: false
    })

    let type = this.formSetting.get('type').value
    const element = add.at(add.length - 1) as FormGroup;

    if (type == 1) {
      this.isHiddenDateTime = true;

      element.get('stepType').setValue(false);
      element.get('stepType').disable();

    } else {

      this.isHiddenDateTime = false;
      element.get('stepType').setValue(true);
      element.get('stepType').enable();

      element.get('learningPathStepStartDate').setValue(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 0, 0, 0, 0))
      element.get('learningPathStepEndDate').setValue(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 23, 59, 59, 0))
    }
    setTimeout(() => {
      this.scrollToElement();
    }, 300);
    this.isSave = false;
    this.formSetting.patchValue(this.learningPath);
  }

  public onSubmit() {

  }

  public learningPathStepControl() {
    return this.formSetting.get('learningPathStep') as FormArray
  }

  public submitButtonHandleClick() {
    
    this.isSave = true;
    let learningPathArr = this.learningPathStepControl();

    if (learningPathArr.invalid) {
      learningPathArr.markAllAsTouched();
      learningPathArr.markAsTouched();
      this.isSave = false;
    }
    if (this.formSetting.get('type').value == 2) {
      if ((!this.formSetting.get('startDate').value || !this.formSetting.get('endDate').value)) {
        this.failToast(" Chọn ngày bắt đầu và ngày kết thúc cho kiểu không thường xuyên ");
        this.isSave = false;
      }
      if (compareTwoDate(new Date(this.formSetting.get('startDate').value), new Date(this.formSetting.get('endDate').value))) {
        this.failToast(" Ngày bắt đầu phải nhỏ hơn ngày kết thúc ");
        this.isSave = false;
      }
    }

    if (!this.formSetting.get('name')) {
      this.failToast("Tên không được để trống ! ")
      this.isSave = false;
    }

    let stepArr: CreateLearningPathStepCommands[] = [];
    // find value with delete action  LearningPathStep

    let stepArrControl = this.learningPathStepControl();
    for (let index = 0; index < stepArrControl.length; index++) {
      const element = stepArrControl.at(index) as FormGroup;

      let coursesArrControl = element.get('learningPathStepLesson') as FormArray;
      if (!coursesArrControl.controls.find(x => x.get('lessonId').value > 0)) {
        this.failToast(' Hãy nhập đầy đủ thông tin !');
        this.isSave = false;
        return;
      }
      let step: CreateLearningPathStepCommands = {
        name: element.get('learningPathStepName').value,
      };

      step.id = element.get('learningPathStepId')?.value;
      if (step.id) {
        step.action
          = this.learningPath.learningPathStep.find(x => x.learningPathStepId == step.id)?.action;
      } else {
        step.action = element.get('action')?.value
      }
      if (element.get('stepType').value) {
        let learningPathStartDate = element.get('learningPathStepStartDate').value;
        let learningPathStepEndDate = element.get('learningPathStepEndDate').value;

        if (compareTwoDate(new Date(learningPathStartDate), new Date(learningPathStepEndDate))) {
          this.failToast(" Ngày bắt đầu phải nhỏ hơn ngày kết thúc !");

          this.isSave = false;
        }

        step.startDate = new Date(new Date(learningPathStartDate).getFullYear(), new Date(learningPathStartDate).getMonth(), new Date(learningPathStartDate).getDate(), 0, 0, 0);
        step.endDate = new Date(new Date(learningPathStepEndDate).getFullYear(), new Date(learningPathStepEndDate).getMonth(), new Date(learningPathStepEndDate).getDate(), 23, 59, 59);
      } else {
        step.startDate = null;
        step.endDate = null;
      }

      if (this.formSetting.invalid || stepArrControl.invalid) {
        this.formSetting.markAllAsTouched();
        this.failToast(' Hãy nhập đầy đủ thông tin !');
        this.isSave = false;
        return;
      }

      let courseArr: Courses[] = [...this.learningPath.learningPathStep[index].learningPathStepLesson.filter(x => x.action != 'DEL')];
      step.courses = [...courseArr.filter(x => x.id === 0 || x.id == null)];
      let courseDels = this.learningPathStepLessonDels.filter(x => x.learningPahtStepId == this.learningPath.learningPathStep[index].learningPathStepId);

      courseDels.map(del => {
        step.courses.push(del);
      });

      stepArr.push(step);
    }

    // lấy những step del
    let stepDels = this.learningPathStepDels;
    stepDels && stepDels.map(stepDel => {
      stepArr.push({
        id: stepDel.learningPathStepId,
        action: stepDel.action == 'DEL' ? stepDel.action : 'DEL',
        name: stepDel.learningPathStepName,
        courses: stepDel.learningPathStepLesson
      });
    });

    let request: LearningPathRequest = {
      id: this.learningPath.id,
      name: this.formSetting.get('name').value,
      type: this.formSetting.get('type').value == 1 ? "ALW" : "NAL",
      startDate: new Date(new Date(this.formSetting.get('startDate').value).getFullYear(), new Date(this.formSetting.get('startDate').value).getMonth(), new Date(this.formSetting.get('startDate').value).getDate(), 0, 0, 0),
      endDate: new Date(new Date(this.formSetting.get('endDate').value).getFullYear(), new Date(this.formSetting.get('endDate').value).getMonth(), new Date(this.formSetting.get('endDate').value).getDate(), 23, 59, 59),
      updateLearningPathStepCommands: [...stepArr]
    }

    if (JSON.stringify(this.learningPathClone) == JSON.stringify(request)) {
      this.failToast(' Dữ liệu không thay đổi! ');
      return;
    }
    this.spinner.show();
    this.learningPathEndpoint.update(request).then(res => {
      if (res) {
        this.isSave = true
        this.successToast("Đã cập nhật thành công !");
        this.learningPathClone = JSON.parse(JSON.stringify(request));
      } else {
        this.isSave = false
        this.failToast("Cập nhật thất bại !")
      }

      this.spinner.hide();
    })
      .catch(res => {
        console.error(res);
        this.spinner.hide();
        this.failToast(" Có lỗi xảy ra ! ")
      });
  }
  public openDpFromDate() {
    //this.dpFromDate.open();
  }
  public openDpToDate() {
    //this.dpToDate.open();
  }
  public openDpFromDateStep() {
    //this.dpFromDateStep.open();
  }
  public openDpToDateStep() {
    //this.dpToDateStep.open();
  }
  public pathStepAt(index: number) {
    return (<FormArray>this.formSetting.get('learningPathStep')).at(index);
  }

  public path() {
    return (<FormArray>this.formSetting.get('name'));;
  }

  public getLearningStepLessons(i: number) {
    return this.learningPath.learningPathStep[i].learningPathStepLesson;
  }
  public getstepTypeControl(index: number): boolean {
    let pathStep = this.pathStepAt(index) as FormGroup;
    return pathStep.get('stepType').value
  }
  public getValidatorPathStepEndDateControl(index: number): boolean {
    let control = this.pathStepAt(index).get('learningPathStepEndDate') as FormControl;
    if (control.errors?.invalidDateFromTo) {
      return true;
    }

    return false;
  }
  public onDateLearningPathChange(value: any) {
    debugger;
    let startDateControl = this.formSetting.controls["startDate"];
    let endDateControl = this.formSetting.controls["endDate"];
    let type: number = this.formSetting.controls['type'].value
    if (type == 2) {
      this.validateDateControl(startDateControl, endDateControl);
    }

    let stepLearningPath = this.formSetting.get('learningPathStep') as FormArray;
    stepLearningPath && stepLearningPath.controls.forEach((element, index) => {
      this.checkValiDateStepControl(index);
    });
  }

  public onDatelearnPathStepChange(index: number) {

    this.checkValiDateStepControl(index);
    this.learningPath.learningPathStep[index].action == "ADD" ? '' : this.learningPath.learningPathStep[index].action = "EDI";
  }

  private checkValiDateStepControl(index: number) {
    let learningPathStartDateControl = this.formSetting.get('startDate');
    let learningPathEndDateControl = this.formSetting.get('endDate');
    debugger;
    let startDateControl = (this.pathStepAt(index) as FormGroup).controls["learningPathStepStartDate"]
    let endDateControl = (this.pathStepAt(index) as FormGroup).controls["learningPathStepEndDate"]
    let stepType = this.pathStepAt(index).get("stepType").value;
    if (stepType) {
      this.validateDateControl(startDateControl, endDateControl);
      // // check with startDate 
      this.validateStartDateControlWithLearningPath(startDateControl, learningPathStartDateControl, learningPathEndDateControl);
      this.validateStartDateControlWithLearningPath(endDateControl, learningPathStartDateControl, learningPathEndDateControl);
    }
  }

  private convertDate(value: string, option: number): Date {
    let year = new Date(value).getFullYear();
    let month = new Date(value).getMonth();
    let day = new Date(value).getDate();
    if (option == 1) {
      return new Date(year, month, day, 0, 0, 0)
    } else {
      return new Date(year, month, day, 23, 59, 59)
    }
  }

  private validateDateControl(startDateControl: AbstractControl, endDateControl: AbstractControl) {
    if (new Date(startDateControl.value) && new Date(endDateControl.value)) {
      let startDate = this.convertDate(startDateControl.value, 1);
      let endDate = this.convertDate(endDateControl.value, 0);
      if (startDate.getTime() > endDate.getTime()) {
        startDateControl
          .setValidators(
            [
              InvalidDateTimeValidator(new Date(endDateControl.value))
            ]
          )
        startDateControl.updateValueAndValidity();
        startDateControl.markAsTouched();
        startDateControl.markAsDirty();
      } else {
        startDateControl.setErrors(null);
        startDateControl.clearValidators();
        startDateControl.updateValueAndValidity();
      }
    } else {
      startDateControl.setErrors(null);
      startDateControl.clearValidators();
      if (!startDateControl.value) {
        startDateControl
          .setValidators([Validators.required])
        startDateControl.updateValueAndValidity();
        startDateControl.markAsTouched();
        startDateControl.markAsDirty();
      }
      if (!endDateControl.value) {
        endDateControl
          .setValidators([Validators.required])
        endDateControl.updateValueAndValidity();
        endDateControl.markAsTouched();
        endDateControl.markAsDirty();
      }
    }
  }

  private validateStartDateControlWithLearningPath(dateControlCompare: AbstractControl, startDateControl: AbstractControl, endDateControl: AbstractControl) {
    debugger;
    if (dateControlCompare.value && startDateControl.value as Date && endDateControl.value as Date) {
      let dateCompare = new Date(dateControlCompare.value).getTime();
      let startDateCompareDate = this.convertDate(startDateControl.value, 1);
      let startDateCompare = startDateCompareDate.getTime();
      let endDateCompareDate = this.convertDate(endDateControl.value, 0);
      let endDateDateCompare = endDateCompareDate.getTime();
      if ((dateCompare < startDateCompare)
        || (dateCompare > endDateDateCompare)) {
        dateControlCompare
          .setValidators(
            [
              InValidDateLearningPahtValidator(startDateCompareDate, endDateCompareDate)
            ]
          )
        dateControlCompare.updateValueAndValidity();
        dateControlCompare.markAsTouched();
        dateControlCompare.markAsDirty();

      }
      else {
        debugger;
        if (dateControlCompare.hasError('invalidDateLearningPath')) {
          // const { invalidDateLearningPath, ...errors } = dateControlCompare.errors;
          dateControlCompare.setErrors(null);
          dateControlCompare.clearValidators();
          dateControlCompare.updateValueAndValidity();
          // console.log('errorToRemove', invalidDateLearningPath)
        }
      }
    } else {
      // dateControlCompare.setErrors({ 'invalidDateLearningPath': null });
      dateControlCompare.clearValidators();
      if (!dateControlCompare.value) {
        dateControlCompare
          .setValidators([Validators.required])
        dateControlCompare.updateValueAndValidity();
        dateControlCompare.markAsTouched();
        dateControlCompare.markAsDirty();
      }
      if (!endDateControl.value) {
        endDateControl
          .setValidators([Validators.required])
        endDateControl.updateValueAndValidity();
        endDateControl.markAsTouched();
        endDateControl.markAsDirty();
      }
    }
  }

  public getValidateStartDateErrors(): boolean {
    if (this.formSetting.controls['startDate'].errors?.invalidDateFromTo) {
      return true;
    }
    return false;
  }

  public getNamePathStepValidation(index) {
    if ((this.pathStepAt(index) as FormGroup).controls['learningPathStepName'].errors?.required) {
      return true;
    }
    return false;
  }
  public getValidatorPathStepControl(index: number): boolean {
    if (this.pathStepAt(index).get('learningPathStepStartDate').errors?.invalidDateFromTo) {
      return true;
    }

    return false;
  }

  public getValidateEndDatePathStepRequiredErrors(index) {
    if ((this.pathStepAt(index) as FormGroup).controls['learningPathStepEndDate'].errors?.required) {
      return true;
    }
    return false;
  }

  public getValidatorPathStepWithStartDateLearningPathControl(index: number): boolean {
    if (this.pathStepAt(index).get('learningPathStepStartDate').errors?.invalidDateLearningPath) {
      return true;
    }

    return false;
  }

  public getValidatorPathStepWithEndDateLearningPathControl(index: number): boolean {
    if (this.pathStepAt(index).get('learningPathStepEndDate').errors?.invalidDateLearningPath) {
      return true;
    }

    return false;
  }

  public getValidateStartDateRequiredErrors() {
    if (this.formSetting.controls['startDate'].errors?.required) {
      return true;
    }
    return false;
  }

  public getValidateEndDateRequiredErros() {
    if (this.formSetting.controls['endDate'].errors?.required) {
      return true;
    }
    return false;
  }

  public getValidateStartDatePathStepRequiredErrors(index) {
    if ((this.pathStepAt(index) as FormGroup).controls['learningPathStepStartDate'].errors?.required) {
      return true;
    }

    return false;
  }
  public getStepLessons(i: number) {
    return this.learningPath.learningPathStep[i].learningPathStepLesson?.filter(x => x.action !== 'DEL')
  }
  public registerCoursesDiaglog(index: number) {
    let courseData = this.learningPath.learningPathStep[index]?.learningPathStepLesson?.filter(x => x.lessonId && x.action != 'DEL');
    const registerCourse = this.dialog.open(RegistCoursesDialogComponent, {
      minWidth: '996px',
      data: courseData
    });

    registerCourse.afterClosed().subscribe((results: LessonIdsResponse[]) => {
      if (results) {
        let learningPathStepForm = this.pathStepAt(index) as FormGroup;
        let learningPathStepLessonFormArr = learningPathStepForm.get('learningPathStepLesson') as FormArray;
        let learningPathStepIndex = this.learningPath.learningPathStep[index];
        if (!learningPathStepIndex.learningPathStepLesson) {
          learningPathStepIndex.learningPathStepLesson = [];
        }


        this.learningPath.name = this.formSetting.controls['name'].value;
        this.learningPath.startDate = this.formSetting.controls['startDate'].value;
        this.learningPath.endDate = this.formSetting.controls['endDate'].value;
        this.learningPath.type = this.formSetting.controls['type'].value;

        learningPathStepIndex.learningPathStepName = learningPathStepForm.controls['learningPathStepName'].value;
        learningPathStepIndex.learningPathStepStartDate = learningPathStepForm.controls['learningPathStepStartDate'].value;
        learningPathStepIndex.learningPathStepEndDate = learningPathStepForm.controls['learningPathStepEndDate'].value;
        learningPathStepIndex.stepType = learningPathStepForm.controls['stepType'].value;

        if (learningPathStepIndex?.action !== 'ADD') {
          learningPathStepIndex.action = "EDI";
          results.forEach(res => {
            let pathLesson = learningPathStepIndex.learningPathStepLesson.find(x => x.lessonId === res.id);
            let pathLessonIndex = learningPathStepIndex.learningPathStepLesson.findIndex(x => x.lessonId === res.id);
            let learningPathStepLessonContrlIndex = learningPathStepLessonFormArr?.controls.findIndex(x => x.get('lessonId').value == res.id);
            switch (res.action) {
              case 'ADD':
                if (!pathLesson) {
                  learningPathStepLessonFormArr.push(this.learningPathStepLesson);
                  learningPathStepIndex.learningPathStepLesson.push({
                    lessonId: res.id,
                    action: res.action,
                    stepLessonName: res.name,
                    stepLessonType: 'NEW',
                    learningPahtStepId: learningPathStepIndex.learningPathStepId,
                  })
                } else {
                  if (pathLesson.action = 'DEL') {
                    learningPathStepLessonFormArr.push(this.learningPathStepLesson);
                    let indexDel = this.learningPathStepLessonDels.findIndex(x => x.lessonId == pathLesson.lessonId);
                    this.learningPathStepLessonDels.splice(indexDel, 1);
                    pathLesson.stepLessonName = res.name;
                    pathLesson.action = 'NON';
                  } else {
                    pathLesson.action = res.action
                  }

                }
                break;
              case 'DEL':
                if (!pathLesson) {
                  // learningPathStepIndex.learningPathStepLesson?.splice(pathLessonIndex, 1);
                } else {
                  if (pathLesson.stepLessonType && pathLesson.stepLessonType == 'NEW') {
                  } else {
                    pathLesson.action = res.action;
                    this.learningPathStepLessonDels.push(pathLesson);
                  }
                  learningPathStepIndex.learningPathStepLesson?.splice(pathLessonIndex, 1);
                  learningPathStepLessonFormArr.removeAt(learningPathStepLessonContrlIndex);
                }
                break;
              default:
                break;
            }

          })
        } else {
          if (!learningPathStepIndex.learningPathStepLesson) {
            learningPathStepIndex.learningPathStepLesson = [];
          }
          results.forEach(res => {
            switch (res.action) {
              case 'ADD':
                let find = learningPathStepIndex?.learningPathStepLesson?.find(x => x.lessonId == res.id);

                if (!find) {
                  learningPathStepLessonFormArr.push(this.learningPathStepLesson);
                  learningPathStepIndex.learningPathStepLesson.push({
                    lessonId: res.id,
                    action: res.action,
                    stepLessonName: res.name,
                    learningPahtStepId: learningPathStepIndex.learningPathStepId,
                  })
                }
                break;
              case 'DEL':
                let indexForm = learningPathStepLessonFormArr.controls.findIndex(x => x.get('lessonId').value == res.id);
                if (indexForm > -1) {
                  learningPathStepLessonFormArr.removeAt(indexForm);
                }
                let indexValue = learningPathStepIndex.learningPathStepLesson.findIndex(x => x.lessonId == res.id);
                if (indexValue > -1) {
                  learningPathStepIndex.learningPathStepLesson.splice(indexValue, 1)
                }
                break;
              default:
                break;
            }
          })
        }
        if (learningPathStepIndex.learningPathStepLesson) {
          let arr = learningPathStepIndex.learningPathStepLesson;
          for (let index = arr.length - 1; index >= 0; index--) {
            if (arr[index].action == 'DEL') {
              learningPathStepIndex.learningPathStepLesson?.splice(index, 1)
            }
          }
        }

        learningPathStepForm.patchValue(this.learningPath.learningPathStep[index]);
        this.learningPathStepLessonDels && [...new Set(this.learningPathStepLessonDels)].forEach(element => {
          learningPathStepIndex.learningPathStepLesson.push(
            {
              learningPahtStepId: element.learningPahtStepId,
              action: element.action,
              lessonId: element.lessonId
            }
          )
        });
      }
    });

    this.isSave = false;
  }
  public removeCourseClick(i: number, pathLessonGroup: number) {
    let stepControl = this.pathStepAt(i) as FormGroup;
    this.learningPath.learningPathStep[i].learningPathStepLesson[pathLessonGroup].action = 'DEL';
    (stepControl.get('learningPathStepLesson') as FormArray).removeAt(pathLessonGroup);
    this.learningPathStepLessonDels.push(this.learningPath.learningPathStep[i].learningPathStepLesson[pathLessonGroup])
    this.learningPath.learningPathStep[i].learningPathStepLesson.splice(pathLessonGroup, 1);

    this.isSave = false;
  }
  public getControlLearningpath(control: string): FormControl {
    return this.formSetting.get('control') as FormControl;
  }

  public successToast(message?: string) {
    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
      data: message ?? 'Thêm thành công!',
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  public failToast(message?: string) {
    this._snackBar.openFromComponent(ErrorSnackBarComponent, {
      data: message ?? "Thêm thất bại",
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    })
  }

  scrollToElement(): void {
    this.scrollBottom.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }

  openDialog(): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '303px',
      data: {
        message: ' Bạn có chắc chắn muốn xóa?',
        buttonText: {
          ok: 'Xóa',
          cancel: 'Hủy',
          colorButtonSubmit: 'warn'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }
}

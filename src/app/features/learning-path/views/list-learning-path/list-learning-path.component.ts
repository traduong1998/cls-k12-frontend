import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, Subject } from 'rxjs';
import { CLSModules, CLSPermissions, LessonEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { LearningPathEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learning-path/endpoints/learning-path-endpoint';
import { ListLearningPathRequest } from 'sdk/cls-k12-sdk-js/src/services/learning-path/requests/list-learning-path-request';
import { ListLearningPathResponse } from 'sdk/cls-k12-sdk-js/src/services/learning-path/responses/list-learning-path-response';
import { AuthService } from 'src/app/core/services';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'app-list-learning-path',
  templateUrl: './list-learning-path.component.html',
  styleUrls: ['./list-learning-path.component.scss']
})

export class ListLearningPathComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  displayedColumns: string[] = ['stt', 'name', 'type', 'startDate', 'endDate', 'function'];
  isLoading = true;
  pageSize = 10;
  pageEvent: PageEvent;
  public pagingLength: number = 0;
  public dataSource: any;
  learningPaths: ListLearningPathResponse[] = [];
  request: ListLearningPathRequest = {
    page: 0,
    size: this.pageSize,
    sortDirection: 'DESC',
    sortField: 'CRE',
    keyword: null,
  }
  requestClone: ListLearningPathRequest;
  learningPathEndpoint: LearningPathEndpoint
  lessonEndPoint: LessonEndpoint;
  userIdentity: UserIdentity;
  levelManageValue: number = 0;
  hasAddPermission: boolean;
  hasAsignPermission: boolean;
  hasEditPermission: boolean;
  hasDeletePermission: boolean;

  constructor(private router: Router,
    private _snackBar: MatSnackBar,
    private dialog: MatDialog,
    private _authService: AuthService,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.learningPathEndpoint = new LearningPathEndpoint();
    this.lessonEndPoint = new LessonEndpoint();
    this.dataSource = new MatTableDataSource(this.learningPaths);
    this.dataSource.paginator = this.paginator;

    this.userIdentity = _authService.getTokenInfo();
    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.LearningPath, CLSPermissions.Add);
    this.hasAsignPermission = this.userIdentity.hasPermission(CLSModules.LearningPath, CLSPermissions.Assign);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.LearningPath, CLSPermissions.Edit);
    this.hasDeletePermission = this.userIdentity.hasPermission(CLSModules.LearningPath, CLSPermissions.Delete);
  }

  ngOnInit(): void {
    this.getData();

    this.requestClone = JSON.parse(JSON.stringify(this.request));
  }

  pageChange(event?: PageEvent) {
    this.request.page = this.paginator.pageIndex;
    this.request.size = this.paginator.pageSize;
    this.getData();
  }

  public onAsignStudentHandleClick(id: number) {
    this.router.navigateByUrl(`dashboard/learning-path/assign-student-for-learningpath/${id}`)
  }
  public onFilterLearningPath() {
    if (compareTwoObject(this.request, this.requestClone)) {
      console.warn(' Does not change request filter ');
    } else {

      this.getData();
    }
  }
  public onAddLearningPathHandleClick() {
    if (this.hasAddPermission) {
      this.router.navigateByUrl(`dashboard/learning-path/create`)
    } else {

      console.error(' CLS : Does not add permission');
    }
  }

  public settinglearningPath(id: number) {
    if (this.hasEditPermission) {

      this.router.navigateByUrl(`dashboard/learning-path/${id}/setting`)
    } else {

      console.error(' CLS : Does not edit permission');
    }
  }
  public removeLearningPath(data) {
    if (!this.hasDeletePermission) {
      console.error(' CLS : Does not delete permission');
      return;
    }

    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      disableClose: true,
      width: '444px',
      height: 'auto',
      data: {
        title: 'Xóa quy trình đào tạo',
        name: data.name,
        message: ''
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
        this.learningPathEndpoint.removeLearningPath(data.id).then((res) => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Xóa quy trình thành công',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
            this.getData();
          }
          else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Xóa thất bại',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          }
        })
          .catch((err) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Xóa thất bại',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          })
      }
      else {
        if (result != undefined) {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Bạn nhập chưa đúng, vui lòng thử lại',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      }
    });
  }
  private getData() {
    this.spinner.show();
    this.learningPathEndpoint.getListPagging(this.request)
      .then(res => {
        this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
        this.learningPaths = res.items;
        this.pagingLength = res.totalItems;
        this.isLoading = false;
        this.spinner.hide();
        this.requestClone = JSON.parse(JSON.stringify(this.request));
      }).catch(err => {
        this.spinner.hide();
      })
  }
}

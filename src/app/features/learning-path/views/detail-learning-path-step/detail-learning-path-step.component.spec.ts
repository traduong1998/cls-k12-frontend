import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailLearningPathStepComponent } from './detail-learning-path-step.component';

describe('DetailLearningPathStepComponent', () => {
  let component: DetailLearningPathStepComponent;
  let fixture: ComponentFixture<DetailLearningPathStepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailLearningPathStepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailLearningPathStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { LearningPathStudentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learning-path-student/endpoints/LearningPathStudentEndpoint';
import { DetailLearningPathModel } from 'sdk/cls-k12-sdk-js/src/services/learning-path-student/models/DetailLearningPathModel';

@Component({
  selector: 'app-detail-learning-path',
  templateUrl: './detail-learning-path.component.html',
  styleUrls: ['./detail-learning-path.component.scss']
})
export class DetailLearningPathComponent implements OnInit {


  dateNow = new Date();
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  listDetailLearningPath: DetailLearningPathModel[] = [];
  private learningPathStudentEndpoint: LearningPathStudentEndpoint;
  learningPathId: number = 0;
  isReady = false;
  isNotStep = false;
  title: string;
  hideDate = false;
  checkDateNull = new Date('0001-01-02T01:01:01.01');

  constructor(private _snackBar: MatSnackBar, private activatedRoute: ActivatedRoute, private router: Router) {
    this.activatedRoute.params.subscribe(params => { this.learningPathId = params.learningPathId });
    this.learningPathStudentEndpoint = new LearningPathStudentEndpoint();
    this.learningPathStudentEndpoint.getDetailLearningPath(this.learningPathId).then(res => {
      res.forEach(e => {
        e.startDate = new Date(e.startDate);
        e.endDate = new Date(e.endDate);
        if (e.startDate < new Date('0001-01-02T01:01:01.01') && e.endDate < new Date('0001-01-02T01:01:01.01')) {
          e.hideDate = true;
        }
        else {
          e.hideDate = false;
        }
        this.listDetailLearningPath.push(e);
      });
      if (this.listDetailLearningPath.length == 0) {
        this.isNotStep = true;
        this.title = "Lộ trình học tập";
      }
      else {
        this.title = this.listDetailLearningPath[0].learningPathName;
      }
      this.isReady = true;
    })
  }

  ngOnInit(): void {
  }
  onlick(detailLearningPath: DetailLearningPathModel) {
    
    if (detailLearningPath.startDate > new Date('0001-01-02T01:01:01.01') || detailLearningPath.endDate > new Date('0001-01-02T01:01:01.01')) {
      if (detailLearningPath.startDate > this.dateNow) {
        this._snackBar.open("Chưa tới ngày bắt đầu!", 'Ok', {
          duration: 5000,
          panelClass: ['red-snackbar'],
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
      else if (detailLearningPath.endDate < this.dateNow) {
        this._snackBar.open("Đã quá ngày học!", 'Ok', {
          duration: 5000,
          panelClass: ['red-snackbar'],
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
      else {
        this.router.navigateByUrl(`/learner/learning-path/${this.learningPathId}/detail-learning-path/${detailLearningPath.id}`);
      }
    }
    else{
      this.router.navigateByUrl(`/learner/learning-path/${this.learningPathId}/detail-learning-path/${detailLearningPath.id}`);
    }
  }


}

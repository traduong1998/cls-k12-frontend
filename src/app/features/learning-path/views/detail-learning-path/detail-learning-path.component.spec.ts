import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailLearningPathComponent } from './detail-learning-path.component';

describe('DetailLearningPathComponent', () => {
  let component: DetailLearningPathComponent;
  let fixture: ComponentFixture<DetailLearningPathComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailLearningPathComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailLearningPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

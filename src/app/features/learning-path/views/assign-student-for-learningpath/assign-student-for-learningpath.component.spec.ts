import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignStudentForLearningPathComponent } from './assign-student-for-learningpath.component';

describe('SignupForLecturesAndReviewStudentsComponent', () => {
  let component: AssignStudentForLearningPathComponent;
  let fixture: ComponentFixture<AssignStudentForLearningPathComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignStudentForLearningPathComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignStudentForLearningPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

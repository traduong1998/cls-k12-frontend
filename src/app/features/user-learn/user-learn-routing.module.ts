import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "src/app/core/guards";
import { UserLearnComponent } from "./user-learn.component";
import { ListLessonUserComponent } from "./views/list-lesson-user/list-lesson-user.component";

const routes: Routes = [{
    path: '', component: UserLearnComponent,
    canActivate: [AuthGuard],
    children: [{
        path: '',
        component: ListLessonUserComponent,
        data: {
            title: 'Bài giảng của tôi'
        }
    }]
}]


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserLearnRoutingModule { }
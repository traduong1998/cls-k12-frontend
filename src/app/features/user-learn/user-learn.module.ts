import { CommonModule, DatePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CKEditorModule } from "ckeditor4-angular";
import { ContextMenuModule } from "ngx-contextmenu";
import { NgxMatSelectSearchModule } from "ngx-mat-select-search";
import { MathModule } from "src/app/shared/math/math.module";
import { SharedModule } from "src/app/shared/shared.module";
import { UserLearnRoutingModule } from "./user-learn-routing.module";
import { UserLearnComponent } from "./user-learn.component";
import {NgxPaginationModule} from 'ngx-pagination';
import { ListLessonUserComponent } from './views/list-lesson-user/list-lesson-user.component';

@NgModule({
    declarations: [
        UserLearnComponent, 
        ListLessonUserComponent
    ],
    imports: [
        MathModule,
        SharedModule,
        CKEditorModule,
        UserLearnRoutingModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        ContextMenuModule.forRoot(),
        NgxMatSelectSearchModule,
        NgxPaginationModule
    ],
    providers:[DatePipe]
})

export class UserLearnModule { }
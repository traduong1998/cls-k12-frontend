import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLessonUserComponent } from './list-lesson-user.component';

describe('ListLessonUserComponent', () => {
  let component: ListLessonUserComponent;
  let fixture: ComponentFixture<ListLessonUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListLessonUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLessonUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { DatePipe } from '@angular/common';
import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Observable, of, ReplaySubject, Subject as RxSubject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { GradeEndpoint, SubjectEndpoint, UserEndpoint, UserIdentity } from 'cls-k12-sdk-js/src';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { ListSubjectOfGrade } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/subjectsEndpoint';
import { LessonOfUser } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/lesson-of-user';
import { SubjectOption } from 'cls-k12-sdk-js/src/services/subject/models/Subject';
import { TeacherOption } from 'cls-k12-sdk-js/src/services/user/models/teacher';
import { AuthService } from 'src/app/core/services';
import { LessonVM } from './list-lesson-vm/lesson-vm';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { ProgressDialogComponent } from 'src/app/features/learn/components/progress-dialog/progress-dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { Router } from '@angular/router';
import { FinishLessonDialogComponent } from 'src/app/features/learner-lesson/components/finish-lesson-dialog/finish-lesson-dialog.component';
import { ProgressPlayerService } from 'src/app/features/learner-lesson/Services/progress-player.service';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';
import { StartTimeLine } from 'src/app/features/learn/intefaces/startTimeLine';

@Component({
  selector: 'app-list-lesson-user',
  templateUrl: './list-lesson-user.component.html',
  styleUrls: ['./list-lesson-user.component.scss'],
})
export class ListLessonUserComponent implements OnInit {

  authService: UserIdentity;
  isSortNew: boolean = true;
  itemsPerPage: number = 10;
  isSort = false;
  //resize
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    // this.width = window.innerWidth;
    // //this.screenWidth = window.innerWidth;
    if (window.innerWidth > 1440) {
      this.itemsPerPage = 10
    } else {
      this.itemsPerPage = 10
    }
  }
  //gradeId: number;
  // subjectId: number;
  lessonSearch: string;
  isClicked: boolean = false;
  //sortBy: string = 'LU.RegistrationDate DESC';
  sortDirection: string = "DESC";
  sortField: string = "CRE";
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>
  listOptionFilter = [{ id: 1, sortDirection: 'DESC', sortField: 'CRE', name: 'Bài giảng mới nhất' }, { id: 2, sortDirection: 'ASC', sortField: 'ALP', name: 'Tên bài giảng từ A-Z' }, { id: 3, sortDirection: 'DESC', sortField: 'ALP', name: 'Tên bài giảng từ Z-A' }]
  selected = 1;
  //mã người dùng đăng nhập
  userId: number;
  // mã trường của người đăng nhập
  schoolId: number;
  isFirstLoadGrade: boolean = true;
  isFirstLoadSubject: boolean = true;
  //danh sách bài giảng
  lessons: LessonVM[] = []
  grades: GradeOption[] = [];
  subject: any;
  isFilter: boolean = false;

  listColor: string[] = ['#523FA7', '#1C9AEE', '#56D02B', '#D8DA5D', '#FA62D8', '#77B5E1', '#F89B8C', '#64E1B0', '#523FA7', '#1C9AEE', '#56D02B', '#D8DA5D']

  public lessonOfUser: LessonOfUser[] = [];
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  public filteredSubjects: ReplaySubject<ListSubjectOfGrade[]> = new ReplaySubject<ListSubjectOfGrade[]>(1);

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  public gradeFilterCtrl: FormControl = new FormControl();
  public subjectFilterCtrl: FormControl = new FormControl();
  gradeId: FormControl = new FormControl();
  subjectId: FormControl = new FormControl();

  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected _onDestroy = new RxSubject<void>();

  listPage: number[] = [1, 2, 3];

  listLesssonDashboard;

  //pagingnation
  totalLength: number = 0;
  page: number = 1;

  subjectName: string = '';
  gradeName: string = '';

  private lessonEndpoint: LessonEndpoint;
  private gradeEndpoint: GradeEndpoint;
  private subjectsEndpoint: SubjectEndpoint;
  private userEndpoint: UserEndpoint;
  fileServer: FileServerEndpoint;
  imageUrl: any = null;
  progressData: Observable<any>;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }

  listTeacherId: number[] = [];
  listSubjectId: number[] = [];

  constructor(private _authService: AuthService, public datepipe: DatePipe, private _snackBar: MatSnackBar, private dialog: MatDialog, private route: Router,
    private progressService: ProgressPlayerService) {
    this.onResize();
    this.subjectsEndpoint = new SubjectEndpoint();
    this.gradeEndpoint = new GradeEndpoint();
    this.lessonEndpoint = new LessonEndpoint();
    this.fileServer = new FileServerEndpoint();
    this.userEndpoint = new UserEndpoint();
    //get token to get user id
    this.authService = this._authService.getTokenInfo();
    this.userId = this.authService.userId;
    this.schoolId = this.authService.schoolId;
    Promise.all([
      this.gradeEndpoint.getGradeForCombobox()
        .then(res => {
          this.gradeId.setValue(res[0].id);
          this.grades = res;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch(err => {
          console.log("errr");
        })
    ])
      .then(() => {
        this.getLessonOfUser();
      })
  }

  addDays(date: Date, days: number): Date {
    date.setDate(date.getDate() + days);
    return date;
  }

  checkDateNull(date: Date): boolean {
    if (date < new Date('0001-01-02T01:01:01.01')) {
      return true;
    }
    return false;
  }

  getLessonOfUser() {
    this.lessonEndpoint.getMyLesson(this.gradeId.value, this.subjectId.value, this.lessonSearch, this.sortDirection, this.sortField, this.page, this.itemsPerPage).then(res => {
      this.totalLength = res.totalItems;
      this.listTeacherId = res.items.map((x) => x.teacherId);
      this.listSubjectId = res.items.map((x) => x.subjectId);
      let listTeacher: TeacherOption[] = [];
      let listSubject: SubjectOption[] = [];
      Promise.all([
        this.userEndpoint.GetListUserByIds(this.listTeacherId).then(res => {
          listTeacher = res;
        }),
        this.subjectsEndpoint.getSubjectOptionsByIds(this.listSubjectId).then(res => {
          listSubject = res;
        })
      ]).then((value) => {
        this.lessons = [];
        res.items.forEach((item) => {
          let teacher = listTeacher.find(x => x.id == item.teacherId);
          let subject = listSubject.find(x => x.id == item.subjectId);
          if (!item.avatarUrl) {
            item.avatarUrl = '/assets/images/lesson/lesson-default.png';
          }
          if (teacher != undefined) {
            if (!teacher.avatar) {
              teacher.avatar = null
            }
          }
          if (item.startTimeLine == StartTimeLine.start) {
            if (this.checkDateNull(new Date(item.startDate))) {
              item.startDateNull = true;
            }
            else {
              item.endDate = this.addDays(new Date(item.startDate), item.interval);
            }
          }
          else if (item.startTimeLine == StartTimeLine.publish) {
            item.endDate = this.addDays(new Date(item.approvedDate), item.interval);
          }
          else if (item.startTimeLine == StartTimeLine.register) {
            item.endDate = this.addDays(new Date(item.registrationDate), item.interval);
          }
          this.lessons.push(LessonVM.From(item, teacher?.name, subject?.name, teacher?.avatar))
        });
      });
    })
  }

  ngOnInit(): void {
    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrade();
      });
  }
  ngAfterViewInit(): void {
    this.setInitialGradeValue();
  }

  protected setInitialGradeValue() {
    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        if (this.singleSelectGrade != undefined) {
          this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
        }
      });
  }

  protected filterGrade() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onGradeSelectClicked() {
    if (this.isFirstLoadGrade) {
      this.gradeEndpoint.getGradeForCombobox()
        .then(res => {
          this.isFirstLoadGrade = false;
          this.grades = res;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch(err => {
          console.log("errr");
        })
    }
  }

  onChangeGradeSelected(id: number) {
    this.subjectsEndpoint.getSubjectOptions(id).then(res => {
      this.subject = res;
      this.filteredSubjects.next(this.subject.slice());
    })
    if (id == null)
      this.gradeName = '';
    else
      this.gradeName = this.grades.find(x => x.id == id).name;
    this.isFilter = true;
  }

  //subject
  onSubjectSelectClicked() {
    if (this.gradeId.value) {
      this.subjectsEndpoint.getSubjectOptions(this.gradeId.value).then(res => {
        this.subject = res;
        this.filteredSubjects.next(this.subject.slice());
      }).catch((err) => {
        console.log("lỗi")
      })
    }
  }

  onChangeSubjectSelected(subjectId) {
    this.isFilter = true;
    if (subjectId != null) {
      this.subjectName = this.subject.find(x => x.id == subjectId).name;
    }
    else {
      this.subjectName = ''
    }
  }

  selectedFilterChange(item) {
    this.isFilter = true;
    let sort = this.listOptionFilter.find(x => x.id == item);
    this.sortDirection = sort.sortDirection;
    this.sortField = sort.sortField;
  }

  filter() {
    this.page = 1;
    if (this.isFilter == true || this.lessonSearch != '') {
      this.lessonEndpoint.getMyLesson(this.gradeId.value, this.subjectId.value, this.lessonSearch, this.sortDirection, this.sortField, 1, this.itemsPerPage).then(res => {
        if (res.totalItems > 0) {
          this.totalLength = res.totalItems;
          this.listTeacherId = res.items.map((x) => x.teacherId);
          this.listSubjectId = res.items.map((x) => x.subjectId);
          let listTeacher: TeacherOption[] = [];
          let listSubject: SubjectOption[] = [];
          Promise.all([
            this.userEndpoint.GetListUserByIds(this.listTeacherId).then(res => {
              listTeacher = res;
            }),
            this.subjectsEndpoint.getSubjectOptionsByIds(this.listSubjectId).then(res => {
              listSubject = res;
            })
          ]).then((value) => {
            this.lessons = [];
            res.items.forEach((item) => {
              let teacher = listTeacher.find(x => x.id == item.teacherId);
              let subject = listSubject.find(x => x.id == item.subjectId);
              if (!item.avatarUrl) {
                item.avatarUrl = '/assets/images/lesson/lesson-default.png';
              }
              if (teacher != undefined) {
                if (!teacher.avatar) {
                  teacher.avatar = null
                }
              }
              if (item.startTimeLine == StartTimeLine.start) {
                if (this.checkDateNull(new Date(item.startDate))) {
                  item.startDateNull = true;
                }
                else {
                  item.endDate = this.addDays(new Date(item.startDate), item.interval);
                }
              }
              else if (item.startTimeLine == StartTimeLine.publish) {
                item.endDate = this.addDays(new Date(item.approvedDate), item.interval);
              }
              else if (item.startTimeLine == StartTimeLine.register) {
                item.endDate = this.addDays(new Date(item.registrationDate), item.interval);
              }

              this.lessons.push(LessonVM.From(item, teacher?.name, subject?.name, teacher?.avatar))
            });
          });
        } else {
          this.lessons = [];
          this.totalLength = this.lessons.length;
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Không tìm thấy bài giảng',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      })
      this.isFilter = false;
    }
  }

  orderByDate() {
    this.isSort = !this.isSort;
    if (this.isSortNew == true) {
      this.page = 1;
      this.lessonEndpoint.getMyLesson(this.gradeId.value, this.subjectId.value, this.lessonSearch, "ASC", 'CRE', 1, this.itemsPerPage).then(res => {
        if (res.totalItems > 0) {
          this.totalLength = res.totalItems;
          this.listTeacherId = res.items.map((x) => x.teacherId);
          this.listSubjectId = res.items.map((x) => x.subjectId);
          let listTeacher: TeacherOption[] = [];
          let listSubject: SubjectOption[] = [];
          Promise.all([
            this.userEndpoint.GetListUserByIds(this.listTeacherId).then(res => {
              listTeacher = res;
            }),
            this.subjectsEndpoint.getSubjectOptionsByIds(this.listSubjectId).then(res => {
              listSubject = res;
            })
          ]).then((value) => {
            this.lessons = [];
            res.items.forEach((item) => {
              let teacher = listTeacher.find(x => x.id == item.teacherId);
              let subject = listSubject.find(x => x.id == item.subjectId);
              if (!item.avatarUrl) {
                item.avatarUrl = '/assets/images/lesson/lesson-default.png';
              }
              if (teacher != undefined) {
                if (!teacher.avatar) {
                  teacher.avatar = null
                }
              }
              if (item.startTimeLine == StartTimeLine.start) {
                if (this.checkDateNull(new Date(item.startDate))) {
                  item.startDateNull = true;
                }
                else {
                  item.endDate = this.addDays(new Date(item.startDate), item.interval);
                }
              }
              else if (item.startTimeLine == StartTimeLine.publish) {
                item.endDate = this.addDays(new Date(item.approvedDate), item.interval);
              }
              else if (item.startTimeLine == StartTimeLine.register) {
                item.endDate = this.addDays(new Date(item.registrationDate), item.interval);
              }
              this.lessons.push(LessonVM.From(item, teacher?.name, subject?.name, teacher?.avatar))
            });
            // console.log("tesst", this.lessons);
          });
        } else {
          this.lessons = [];
          this.totalLength = this.lessons.length;
        }
      })
      this.isSortNew = false
    } else {
      this.page = 1;
      this.lessonEndpoint.getMyLesson(this.gradeId.value, this.subjectId.value, this.lessonSearch, this.sortDirection, this.sortField, this.page, this.itemsPerPage).then(res => {
        if (res.totalItems > 0) {
          this.totalLength = res.totalItems;
          this.listTeacherId = res.items.map((x) => x.teacherId);
          this.listSubjectId = res.items.map((x) => x.subjectId);
          let listTeacher: TeacherOption[] = [];
          let listSubject: SubjectOption[] = [];
          Promise.all([
            this.userEndpoint.GetListUserByIds(this.listTeacherId).then(res => {
              listTeacher = res;
            }),
            this.subjectsEndpoint.getSubjectOptionsByIds(this.listSubjectId).then(res => {
              listSubject = res;
            })
          ]).then((value) => {
            this.lessons = [];
            res.items.forEach((item) => {
              let teacher = listTeacher.find(x => x.id == item.teacherId);
              let subject = listSubject.find(x => x.id == item.subjectId);
              if (!item.avatarUrl) {
                item.avatarUrl = '/assets/images/lesson/lesson-default.png';
              }
              if (teacher != undefined) {
                if (!teacher.avatar) {
                  teacher.avatar = null
                }
              }
              if (item.startTimeLine == StartTimeLine.start) {
                if (this.checkDateNull(new Date(item.startDate))) {
                  item.startDateNull = true;
                }
                else {
                  item.endDate = this.addDays(new Date(item.startDate), item.interval);
                }
              }
              else if (item.startTimeLine == StartTimeLine.publish) {
                item.endDate = this.addDays(new Date(item.approvedDate), item.interval);
              }
              else if (item.startTimeLine == StartTimeLine.register) {
                item.endDate = this.addDays(new Date(item.registrationDate), item.interval);
              }
              this.lessons.push(LessonVM.From(item, teacher?.name, subject?.name, teacher?.avatar))
            });
            // console.log("tesst", this.lessons);
          });
        } else {
          this.lessons = [];
          this.totalLength = this.lessons.length;
        }
      })
      this.isSortNew = true
    }
  }
  filterMobile() {
    this.page = 1;
    this.lessonEndpoint.getMyLesson(this.gradeId.value, this.subjectId.value, this.lessonSearch, this.sortDirection, this.sortField, 1, this.itemsPerPage).then(res => {
      if (res.totalItems > 0) {
        this.totalLength = res.totalItems;
        this.listTeacherId = res.items.map((x) => x.teacherId);
        this.listSubjectId = res.items.map((x) => x.subjectId);
        let listTeacher: TeacherOption[] = [];
        let listSubject: SubjectOption[] = [];
        Promise.all([
          this.userEndpoint.GetListUserByIds(this.listTeacherId).then(res => {
            listTeacher = res;
          }),
          this.subjectsEndpoint.getSubjectOptionsByIds(this.listSubjectId).then(res => {
            listSubject = res;
          })
        ]).then((value) => {
          this.lessons = [];
          res.items.forEach((item) => {
            let teacher = listTeacher.find(x => x.id == item.teacherId);
            let subject = listSubject.find(x => x.id == item.subjectId);
            if (!item.avatarUrl) {
              item.avatarUrl = '/assets/images/lesson/lesson-default.png';
            }
            if (teacher != undefined) {
              if (!teacher.avatar) {
                teacher.avatar = null
              }
            }
            if (item.startTimeLine == StartTimeLine.start) {
              if (this.checkDateNull(new Date(item.startDate))) {
                item.startDateNull = true;
              }
              else {
                item.endDate = this.addDays(new Date(item.startDate), item.interval);
              }
            }
            else if (item.startTimeLine == StartTimeLine.publish) {
              item.endDate = this.addDays(new Date(item.approvedDate), item.interval);
            }
            else if (item.startTimeLine == StartTimeLine.register) {
              item.endDate = this.addDays(new Date(item.registrationDate), item.interval);
            }
            this.lessons.push(LessonVM.From(item, teacher?.name, subject?.name, teacher?.avatar))
          });
          console.log("tesst", this.lessons);
        });
      } else {
        this.lessons = [];
        this.totalLength = this.lessons.length;
      }
    })
  }

  //upload imgae to server file
  onChangeImageInput(event): void {
    var reader = new FileReader();
    let file: File = null;
    if (event.target.files && event.target.files[0]) {
      file = event.target.files[0];
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      Promise.resolve()
        .then(() => {
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.fileServer.uploadFile({
            file: file,
            isPrivate: false,
            moduleName: ServiceName.LEARN
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          this.imageUrl = res ? res.link : null;
          // this.fileImageName = res?.name;
          // this.createLessonForm.controls['imageURL'].setValue(this.imageUrl);
          // this.createLessonForm.controls['fileImageURLName'].setValue(res?.name);
        })
        .then((res) => {
        })
        .then(() => {
          // console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }

  inputChange(event) {
    this.getLessonOfUser();
  }

  goToLesson(id) {
    //kiểm tra hoàn thành bài giảng hay chưa
    let lesson = this.lessons.find(x => x.id == id);
    // debugger
    if (new Date(lesson.endDate) < new Date()) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Bài giảng đã quá hạn',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    }
    else {
      if (lesson.percentageOfCompletion == 100) {
        // hiện dialog
        this.dialog.open(FinishLessonDialogComponent, {
          data: {
            id: id,
            name: lesson.name
          },
          disableClose: true
        });
      }
      else {
        this.route.navigate([`learner/lessons/${id}/contents`]);
        this.progressService.isLearner.next(true);
      }
    }
  }

  pageChange(e) {
    this.page = e;
    this.lessonEndpoint.getMyLesson(this.gradeId.value, this.subjectId.value, this.lessonSearch, this.sortDirection, this.sortField, this.page, this.itemsPerPage).then(res => {
      this.totalLength = res.totalItems;
      // console.log(res.items);
      this.listTeacherId = res.items.map((x) => x.teacherId);
      this.listSubjectId = res.items.map((x) => x.subjectId);
      let listTeacher: TeacherOption[] = [];
      let listSubject: SubjectOption[] = [];
      Promise.all([
        this.userEndpoint.GetListUserByIds(this.listTeacherId).then(res => {
          listTeacher = res;
        }),
        this.subjectsEndpoint.getSubjectOptionsByIds(this.listSubjectId).then(res => {
          listSubject = res;
        })
      ]).then((value) => {
        this.lessons = [];
        res.items.forEach((item) => {
          let teacher = listTeacher.find(x => x.id == item.teacherId);
          let subject = listSubject.find(x => x.id == item.subjectId);
          if (!item.avatarUrl) {
            item.avatarUrl = '/assets/images/lesson/lesson-default.png';
          }
          if (teacher != undefined) {
            if (!teacher.avatar) {
              teacher.avatar = null
            }
          }
          if (item.startTimeLine == StartTimeLine.start) {
            if (this.checkDateNull(new Date(item.startDate))) {
              item.startDateNull = true;
            }
            else {
              item.endDate = this.addDays(new Date(item.startDate), item.interval);
            }
          }
          else if (item.startTimeLine == StartTimeLine.publish) {
            item.endDate = this.addDays(new Date(item.approvedDate), item.interval);
          }
          else if (item.startTimeLine == StartTimeLine.register) {
            item.endDate = this.addDays(new Date(item.registrationDate), item.interval);
          }
          this.lessons.push(LessonVM.From(item, teacher?.name, subject?.name, teacher?.avatar))
        });
      });
    })
    if (window.innerWidth <= 525) {
      var elmnt = document.getElementById("content-mobile");
      elmnt.scrollIntoView({ behavior: 'smooth' });
    }
  }

  clickItemMobile(id: number) {
    this.route.navigate([`learner/lessons/${id}/contents`]);
  }

  getFirstName(fullName: string): string {
    fullName = fullName.trim();
    let lastIndex = fullName.lastIndexOf(' ');
    if (lastIndex != -1) {
      return fullName[lastIndex + 1];
    }
    return fullName[0].toUpperCase();
  }
}

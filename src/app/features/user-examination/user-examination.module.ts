import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { UserExaminationComponent } from './user-examination.component'
import { UserExaminationRoutingModule } from './user-examination-routing.module'
import { SingleChoiceComponent } from './Components/single-choice/single-choice.component';
//material
import { TableQuestionComponent } from './Components/table-question/table-question.component';
import { TableQuestionMoblieComponent } from './Components/table-question-moblie/table-question-moblie.component';
import { TrueFalseClauseComponent } from './Components/true-false-clause/true-false-clause.component';
import { MissingWorld1Component } from './Components/missing-world1/missing-world1.component';
import { MissingWorld2Component } from './Components/missing-world2/missing-world2.component';
import { MultipleChoiceComponent } from './Components/multiple-choice/multiple-choice.component';
import { TrueFalseComponent } from './Components/true-false/true-false.component';
import { UnderlineComponent } from './Components/underline/underline.component';
import { MatchingComponent } from './Components/matching/matching.component';
import { ContentExamComponent } from './Components/content-exam/content-exam.component'
import { EssayComponent } from './Components/essay/essay.component';
import { GroupQuestionComponent } from './Components/group-question/group-question.component';
import { CKEditorModule } from 'ckeditor4-angular';
import { SharedModule } from 'src/app/shared/shared.module';
import { StartExamComponent } from './Views/start-test/start-exam.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { LoadingScreenComponent } from '../loading-screen/loading-screen.component';
import { MathModule } from 'src/app/shared/math/math.module';
import { CallSupportComponent } from './Components/call-support/call-support.component';
import { ExamSupervisionComponent } from './Views/exam-supervision/exam-supervision.component';
import { ContentSupervisionComponent } from './Components/content-supervision/content-supervision.component';
import { UserExamInforComponent } from './Components/user-exam-infor/user-exam-infor.component';
import { InforUserDetailComponent } from './Components/infor-user-detail/infor-user-detail.component';
import { HandlingViolationsComponent } from './Components/handling-violations/handling-violations.component';
import { DoExamComponent } from './Views/do-exam/do-exam.component';
import { BeforeDoExamComponent } from './Components/before-do-exam/before-do-exam.component';
import { MatchingMobileComponent } from './Components/matching-mobile/matching-mobile.component';
import { UserExamInfoSmallComponent } from './Components/user-exam-info-small/user-exam-info-small.component';
import { ContextMenuModule } from 'ngx-contextmenu';
import { ResultExamComponent } from './Components/result-exam/result-exam.component';
import { NotficationComponent } from './Components/notfication/notfication.component';
import { ShowResultDoExamComponent } from './Components/show-result-do-exam/show-result-do-exam.component';
import { EndTimeDialogComponent } from './Components/end-time-dialog/end-time-dialog.component';
import { AngularCountdownTimerModule } from 'angular8-countdown-timer';
import { ExamRegulationDialogComponent } from './Components/exam-regulation-dialog/exam-regulation-dialog.component';
import { HandlingViolationsDialogComponent } from './Components/handling-violations-dialog/handling-violations-dialog.component';
import { SwalAlertUserExaminationComponent } from './Components/swal-alert-user-examination/swal-alert-user-examination.component';
import { ListSubjectSupervisionComponent } from './Views/list-subject-supervision/list-subject-supervision.component';
import { ListExamComponent } from './Views/list-exam/list-exam.component';
import { ExamRuleDialogComponent } from './Components/exam-rule-dialog/exam-rule-dialog.component';
import { UserExaminationService } from './Services/user-examination.service';
import { TableQuestionResultComponent } from './Components/table-question-result/table-question-result.component';
import { TableQuestionResultMobileComponent } from './Components/table-question-result-mobile/table-question-result-mobile.component';
import { ViolationStudentTestDialogComponent } from './Components/violation-student-test-dialog/violation-student-test-dialog.component';
import { ListSubjectExamComponent } from './Views/list-subject-exam/list-subject-exam.component';
import { ListUnitsExamComponent } from './Views/list-units-exam/list-units-exam.component';
import { CheckScoreDoExamComponent } from './Views/check-score-do-exam/check-score-do-exam.component';
import { CollectTestOfContestantDialog } from './Components/collect-test-of-contestant-dialog/collect-test-of-contestant-dialog.component';

@NgModule({
  declarations: [
    UserExaminationComponent,
    SingleChoiceComponent,
    TableQuestionComponent,
    TableQuestionMoblieComponent,
    TrueFalseClauseComponent,
    MissingWorld1Component,
    MissingWorld2Component,
    MultipleChoiceComponent,
    TrueFalseComponent,
    UnderlineComponent,
    MatchingComponent,
    EssayComponent,
    ContentExamComponent,
    GroupQuestionComponent,
    StartExamComponent,
    CallSupportComponent,
    ExamSupervisionComponent,
    DoExamComponent,
    ContentSupervisionComponent,
    UserExamInforComponent,
    InforUserDetailComponent,
    HandlingViolationsComponent,
    BeforeDoExamComponent,
    MatchingMobileComponent,
    UserExamInfoSmallComponent,
    ResultExamComponent,
    NotficationComponent,
    ShowResultDoExamComponent,
    EndTimeDialogComponent,
    ExamRegulationDialogComponent,
    HandlingViolationsDialogComponent,
    SwalAlertUserExaminationComponent,
    ListSubjectSupervisionComponent,

    //LoadingScreenComponent,
    ListExamComponent,
    ExamRuleDialogComponent,
    TableQuestionResultComponent,
    TableQuestionResultMobileComponent,
    ViolationStudentTestDialogComponent,
    ListSubjectExamComponent,
    ListUnitsExamComponent,
    CheckScoreDoExamComponent,
    CollectTestOfContestantDialog
  ],
  imports: [
    SharedModule,
    UserExaminationRoutingModule,
    CKEditorModule,
    FormsModule,
    NgxPaginationModule,
    MathModule,
    AngularCountdownTimerModule,
    ContextMenuModule.forRoot()
  ],
  providers: [
    UserExaminationService
  ],
})
export class UserExaminationModule { }

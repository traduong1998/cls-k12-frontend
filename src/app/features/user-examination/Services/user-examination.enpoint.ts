import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { QuestionResponse } from 'src/app/shared/modules/userexam/QuestionResponse';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserExaminationEndpoint {
  private _baseUrl;

  constructor(private http: HttpClient) {
    this._baseUrl = `/assets/fakedata`;
  }
  getQuestionData(): Observable<QuestionResponse[]> {
    return this.http
      .get<any>(`${this._baseUrl}/data.json`)
      .pipe(
        delay(2000), // Simulate request delay
        map(res => {
          return <QuestionResponse[]>res.dataQuestion;
        }),
        map(data => { return data; })
      )
  }
  getAnswerData(): Observable<QuestionResponse[]> {
    return this.http
      .get<any>(`${this._baseUrl}/data.json`)
      .pipe(
        delay(2000), // Simulate request delay
        map(res => {
          return <QuestionResponse[]>res.dataAnswer;
        }),
        map(data => { return data; })
      )
  }

  getRigthAnswerData(): Observable<QuestionResponse[]> {
    return this.http
      .get<any>(`${this._baseUrl}/data-reponse-exam.json`)
      .pipe(
        delay(2000), // Simulate request delay
        map(res => {
          console.log("ress", res)
          return <QuestionResponse[]>res.dataQuestion;
        }),
        map(data => { return data; })
      )
  }
  getTimeRemaining(): Observable<number> {
    return this.http
      .get<any>(`${this._baseUrl}/data.json`)
      .pipe(
        delay(2000), // Simulate request delay
        map(res => {
          return <number>res.timeRemaining;
        }),
        map(data => { return data; })
      )
  }
}

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MarkServiceService {
  private _isMark= new Subject<boolean>();
  
  type$ = this._isMark.asObservable();
  constructor() { }
  getMarkStatus(ismark:boolean){
    this._isMark.next(ismark);
  }
}

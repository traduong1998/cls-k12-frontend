import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { TestKitBankEnpoint } from 'sdk/cls-k12-sdk-js/src/services/testkit-bank/endpoints/testkit-bank-endpoint';
import { isNullOrEmpty } from 'src/app/shared/helpers/validation.helper';
import { BehaviorSubject, Observable } from 'rxjs';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { UserExaminationEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-examination/endpoint/user-examination.enpoint';
import { SupportMessage } from 'src/app/shared/modules/userexam/exam-supervision/SupportMessage';
import { AnswerOfUser, UserExercise } from 'src/app/shared/modules/userexam/UserAnswer';
import { ExamSubjectStudentInfo } from '../Interfaces/exam-subject-student-infor';
import { LocalStorageService } from './local-storage.service';
import { HttpClient } from '@angular/common/http';
import { TestCodeUrlResponse } from 'sdk/cls-k12-sdk-js/src/services/testkit-bank/responses/test-code-url-response';
import { UpdateUserExerciseRequest } from 'sdk/cls-k12-sdk-js/src/services/user-examination/endpoint/request/update-userExercise-request';
import { SubmitUserExerciseRequest } from 'sdk/cls-k12-sdk-js/src/services/user-examination/endpoint/request/submit-userExercise-request';
import { Injectable } from '@angular/core';
import { ResultDoExamResponse } from 'sdk/cls-k12-sdk-js/src/services/user-examination/endpoint/responses/result-do-exam-response';

@Injectable()
export class UserExaminationService {
  private static userExcerCisesStorageKey = 'excercise';
  /**Thời gian ghi log (phút) */
  public static readonly timeToWriteLog = 1;
  private Questions: QuestionTest[] = [];
  // mảng realtime chỉ có câu đã tương tác
  private userExercises: UserExercise[] = [];
  // mảng dữ liệu mới nhất trên server
  private latedUserAnswers: UserExercise[] = [];
  //mảng sẽ cập nhật
  private userUpdateAnswers: UserExercise[] = [];
  /** mảng sẽ add*/
  private userInsertAnswers: UserExercise[] = [];
  private userUpdateAnswer: BehaviorSubject<UserExercise[]> = new BehaviorSubject<UserExercise[]>([]);
  private userAnswersSubject: BehaviorSubject<UserExercise[]> = new BehaviorSubject<UserExercise[]>([]);
  private testKitBankEndpoint: TestKitBankEnpoint;
  public resultDoExam: ResultDoExamResponse;
  userExaminationEndpoint: UserExaminationEndpoint;
  Questions$: BehaviorSubject<QuestionTest[]> = new BehaviorSubject<QuestionTest[]>([]);
  userUpdateAnswer$ = this.userUpdateAnswer.asObservable();
  userAnswer$ = this.userAnswersSubject.asObservable();
  isReady: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isReadyResult: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  infoExam: ExamSubjectStudentInfo;
  constructor(private storageService: LocalStorageService, private httpClient: HttpClient) {
    this.userExaminationEndpoint = new UserExaminationEndpoint();
    this.testKitBankEndpoint = new TestKitBankEnpoint();
  }

  initData(examSubjectStudentId: number) {
    let userTestCodeUrl;
    this.resetData();
    return Promise.resolve()
      .then(() => {
        //Lấy thông tin ca thi bên system
        return this.userExaminationEndpoint.getInforExamSubjectStudentById(examSubjectStudentId);
      })
      .then((info) => {
        this.infoExam = {
          id: info.id,
          organizeExamId: info.organizeExamId,
          examId: info.examId,
          gradeId: info.gradeId,
          gradeName: info.gradeName,
          portalName: info.portalName,
          subjectId: info.subjectId,
          shiftId: info.shiftId,
          subjectName: info.subjectName,
          startDate: info.startDate,
          endDate: info.endDate,
          timeRemaining: info.timeRemaining,
          isCompleted: false,
          isSuspended: false,
          testId: null,
          testKitId: null,
          isShuffle: null,
          testCodeURL: "",
          displayPerPage: null,
          numberOfMediaOpening: null,
          isPreservingTime: false,
          examName: "",
          testTime: null,
          isLockTestScreen: false,
          isPublicScore: false,
          isPublicAnswer: false,
        };
        // Lấy thông tin đề thi bên Exam
        return this.userExaminationEndpoint.getDetailInfoExam(info.examId, info.subjectId, examSubjectStudentId);
      })
      .then((info) => {
        this.infoExam.isCompleted = info.isCompleted,
          this.infoExam.isSuspended = info.isSuspended,
          this.infoExam.testId = info.testId,
          this.infoExam.testKitId = info.testKitId,
          this.infoExam.isShuffle = info.isShuffler,
          this.infoExam.testCodeURL = info.testCodeURL,
          this.infoExam.displayPerPage = info.displayPerPage,
          this.infoExam.numberOfMediaOpening = info.numberOfMediaOpening,
          this.infoExam.isPreservingTime = info.isPreservingTime,
          this.infoExam.examName = info.name;
        this.infoExam.testTime = info.testTime;
        this.infoExam.isLockTestScreen = info.isLockTestScreen;
        this.infoExam.isPublicAnswer = info.isPublicAnswer;
        this.infoExam.isPublicScore = info.isPublicScore;
        UserExaminationService.userExcerCisesStorageKey = JSON.stringify(examSubjectStudentId);
        if (info.isSuspended) throw new Error("Suspended");
        if (info.isCompleted) throw new Error("Completed");
      })
      .then(() => {
        //lay cau hoi json
        if (this.infoExam.testId == null || isNullOrEmpty(this.infoExam.testCodeURL)) {
          //Lấy đề cho học sinh nếu chưa thi trước đó
          return this.testKitBankEndpoint.getTestForStudent(this.infoExam.examId, this.infoExam.testKitId, this.infoExam.isShuffle);
        }
        else {
          let testCodeUrlResponse: TestCodeUrlResponse = { testLink: this.infoExam.testCodeURL }
          return testCodeUrlResponse;
        }
      })
      .then((testCodeUrl) => {
        userTestCodeUrl = testCodeUrl["testLink"];
        return this.GetListQuestionFromUrl(testCodeUrl["testLink"]);
      })
      .then((data$) => {
        data$.subscribe((data) => {
          if (this.infoExam.testId == null) {
            this.userExaminationEndpoint.createTestUrl({
              id: examSubjectStudentId,
              organizeExamId: this.infoExam.organizeExamId,
              testKitId: this.infoExam.testKitId,
              examId: this.infoExam.examId,
              subjectId: this.infoExam.subjectId,
              testId: data["id"],
              testCodeUrl: userTestCodeUrl
            })
              .then((isCreated) => {
                if (!isCreated) {
                  throw new Error("ERROR: userExaminationEndpoint.createTestUrl")
                }
                this.Questions = this.beauty(data["questions"]);
                this.Questions$.next(this.Questions);
                this.isReady.next(true);
              });
          } else {
            //Lấy bài làm cũ
            this.userExaminationEndpoint.getUserExercise(examSubjectStudentId).then((userExercises) => {
              this.userExercises = [...userExercises];
              this.latedUserAnswers = [...userExercises];
              this.updateToLocalStorage();
              this.Questions = this.beauty(data["questions"]);
              this.Questions$.next(this.Questions);
              this.isReady.next(true);
            });
          }
        })
      })
      .catch((err) => {
        console.error(err);
        throw new Error(err);
      });
  }
  initDataShowResult(examSubjectStudentId: number) {
    let userTestCodeUrl;
    this.resetData();
    return Promise.resolve()
      .then(() => {
        //Lấy thông tin ca thi bên system
        return this.userExaminationEndpoint.getInforExamSubjectStudentById(examSubjectStudentId);
      })
      .then((info) => {
        this.infoExam = {
          id: info.id,
          organizeExamId: null,
          examId: info.examId,
          gradeId: info.gradeId,
          gradeName: info.gradeName,
          portalName: info.portalName,
          subjectId: info.subjectId,
          shiftId: info.shiftId,
          subjectName: info.subjectName,
          startDate: info.startDate,
          endDate: info.endDate,
          timeRemaining: info.timeRemaining,
          isCompleted: false,
          isSuspended: false,
          testId: null,
          testKitId: null,
          isShuffle: null,
          testCodeURL: "",
          displayPerPage: null,
          numberOfMediaOpening: null,
          isPreservingTime: false,
          examName: "",
          testTime: null,
          isLockTestScreen: false,
          isPublicScore: false,
          isPublicAnswer: false,
        };
        // Lấy thông tin đề thi bên Exam
        return this.userExaminationEndpoint.getDetailInfoExam(info.examId, info.subjectId, examSubjectStudentId);
      })
      .then((info) => {
        this.infoExam.isCompleted = info.isCompleted,
          this.infoExam.isSuspended = info.isSuspended,
          this.infoExam.testId = info.testId,
          this.infoExam.testKitId = info.testKitId,
          this.infoExam.isShuffle = info.isShuffler,
          this.infoExam.testCodeURL = info.testCodeURL,
          this.infoExam.displayPerPage = info.displayPerPage,
          this.infoExam.numberOfMediaOpening = info.numberOfMediaOpening,
          this.infoExam.isPreservingTime = info.isPreservingTime,
          this.infoExam.examName = info.name;
        this.infoExam.testTime = info.testTime;
        this.infoExam.isLockTestScreen = info.isLockTestScreen;
        this.infoExam.isPublicAnswer = info.isPublicAnswer;
        this.infoExam.isPublicScore = info.isPublicScore;
        UserExaminationService.userExcerCisesStorageKey = JSON.stringify(examSubjectStudentId);
      })
      .then(() => {
        let testCodeUrlResponse: TestCodeUrlResponse = { testLink: this.infoExam.testCodeURL }
        return testCodeUrlResponse;
      })
      .then((testCodeUrl) => {
        userTestCodeUrl = testCodeUrl["testLink"];
        return this.GetListQuestionFromUrl(testCodeUrl["testLink"]);
      })
      .then((data$) => {
        data$.subscribe((data) => {
          //Lấy bài làm cũ
          this.userExaminationEndpoint.getResultDoExam(examSubjectStudentId).then((result) => {
            this.resultDoExam = { ...result };
            this.userExercises = [...result.userExercises];
            this.latedUserAnswers = [...result.userExercises];
            this.updateToLocalStorage();
            this.Questions = this.beauty(data["questions"]);
            this.Questions$.next(this.Questions);
            this.isReadyResult.next(true);
          });
        })
      })
      .catch((err) => {
        console.error(err);
        throw new Error(err);
      });
  }

  /**Hàm remove các span trống trong nội dung câu hỏi và đáp án */
  public beauty(questions: QuestionTest[]): QuestionTest[] {
    let beautyQuestions = [...questions];
    let regex = /text-indent:36pt;|&#xa0;|\u0000|<(html|head|\/head|body|\/body|\/html|\?xml|\!DOCTYPE|meta|title|\/title)[^>]{0,}>|<(span)[^>]{0,}>&#xa0;<\/span>|<(div)[^>]{0,}>&nbsp;<\/div>|<(p)[^>]{0,}>&nbsp;<\/p>/g;
    beautyQuestions.forEach((qst) => {
      //Remove trong nội dung câu hỏi
      qst.content = qst.content.replace(regex, "");
      //Remove trong nội dung đáp án
      if (qst.answers != null) {
        qst.answers.forEach((answer) => {
          answer.content = answer.content.replace(regex, "");
        });
      }
      //Câu chùm
      if (qst.questions != null) {
        qst.questions.forEach((child) => {
          //Remove trong nội dung câu con
          child.content = child.content.replace(regex, "");

          //Remove trong đáp án câu con
          child.answers.forEach((answerChild) => {
            answerChild.content = answerChild.content.replace(regex, "");
          });
        });
      }
    })
    return beautyQuestions;
  }

  private resetData(): void {
    this.clearLocalStorage();
    UserExaminationService.userExcerCisesStorageKey = "";
    this.userExercises = [];
    this.latedUserAnswers = [];
    this.userUpdateAnswers = [];
    this.userInsertAnswers = [];
    this.userUpdateAnswer.next([]);
    this.userAnswersSubject.next([]);
    this.isReady.next(false);
    this.isReadyResult.next(false);
    this.infoExam == undefined;
  }

  public GetListQuestionFromUrl(url: string): Observable<any> {
    return this.httpClient.get(url);
  }

  clearLocalStorage(): void {
    this.storageService.remove(UserExaminationService.userExcerCisesStorageKey);
  }
  fetchFromLocalStorage() {
    this.userExercises = this.storageService.getValue<UserExercise[]>(UserExaminationService.userExcerCisesStorageKey) || [];
    this.userAnswersSubject.next(this.userExercises);
  }

  updateToLocalStorage() {
    this.storageService.setObject(UserExaminationService.userExcerCisesStorageKey, this.userExercises);
  }
  getQuestions(): Observable<QuestionTest[]> {
    return this.Questions$;
  }
  getRightAnswer() {
    return this.testKitBankEndpoint.getResultForTest(this.infoExam.examId, this.infoExam.testKitId, this.infoExam.testId);
  }
  getUserAnswer() {
    return this.userExercises;
  }
  onChangeUserExercise(questionId: number, AnswerOfUsers: AnswerOfUser[]) {
    // cập nhật danh sách local
    const index = this.userExercises.indexOf(this.userExercises.find(x => x.questionId == questionId));
    if (index == -1) {
      let userExercise: UserExercise = { questionId: questionId, userAnswers: AnswerOfUsers, isMark: false, isAnswered: true, isEssayQuestion: false };
      this.addExercise(this.userExercises, userExercise)
    } else {
      this.updateExercise(this.userExercises, AnswerOfUsers, index);
    }

    // so sánh với danh sách đã gửi lên phân luồng tạo với và update
    const indexlated = this.latedUserAnswers.indexOf(this.latedUserAnswers.find(x => x.questionId == questionId));
    if (indexlated == -1) {
      this.onChangeInsertAnswer(questionId, AnswerOfUsers);
    } else {
      this.onChangeUpdateAnswer(questionId, AnswerOfUsers);
    }
    this.updateToLocalStorage();
  }

  onChangeUserExerciseEssay(questionId: number, content: string, fileUrl: string) {
    // cập nhật danh sách 
    fileUrl = fileUrl ?? "";
    content = content ?? "";
    const index = this.userExercises.indexOf(this.userExercises.find(x => x.questionId == questionId));
    if (isNullOrEmpty(content) && isNullOrEmpty(fileUrl)) {
      if (index >= 0) {
        this.userExercises.splice(index, 1);
        const indexlated = this.latedUserAnswers.indexOf(this.latedUserAnswers.find(x => x.questionId == questionId));
        if (indexlated > 0) {
          this.onChangeUpdateAnswerEssay(questionId, content, fileUrl);
        } else {
          let indexInsert = this.userInsertAnswers.findIndex(x => x.questionId == questionId);
          this.userInsertAnswers.splice(indexInsert, 1);
        }
      }
    } else {
      if (index == -1) {
        let userExercise: UserExercise = { questionId: questionId, userAnswers: [], isMark: false, isAnswered: true, isEssayQuestion: true, content: (content == null || content == undefined) ? "" : content, fileUrl: (fileUrl == null || fileUrl == undefined) ? "" : fileUrl };
        this.addExercise(this.userExercises, userExercise)
      } else {
        let userExercise = this.userExercises[index];
        userExercise.content = content;
        userExercise.fileUrl = fileUrl;
        userExercise.isAnswered = true;
        this.updateExerciseEssay(this.userExercises, userExercise, index);
      }

      // so sánh với danh sách đã gửi lên phân luồng tạo với và update
      const indexlated = this.latedUserAnswers.indexOf(this.latedUserAnswers.find(x => x.questionId == questionId));
      if (indexlated == -1) {
        this.onChangeInsertAnswerEssay(questionId, content, fileUrl);
      } else {
        this.onChangeUpdateAnswerEssay(questionId, content, fileUrl);
      }
    }
    this.updateToLocalStorage();


  }

  onChangeUpdateAnswerEssay(questionId: number, content: string, fileUrl: string) {
    const index = this.userUpdateAnswers.indexOf(this.userUpdateAnswers.find(x => x.questionId == questionId));
    let oldUserExcercise = this.latedUserAnswers.find(x => x.questionId == questionId);
    if (index == -1) {
      let userExercise: UserExercise = { questionId: questionId, userAnswers: [], isMark: oldUserExcercise.isMark, isAnswered: true, isEssayQuestion: true, content: (content == null || content == undefined) ? "" : content, fileUrl: (fileUrl == null || fileUrl == undefined) ? "" : fileUrl };
      this.addExercise(this.userUpdateAnswers, userExercise)
    } else {
      let userExercise = this.userUpdateAnswers[index];
      userExercise.content = content;
      userExercise.fileUrl = fileUrl;
      userExercise.isAnswered = true;
      this.updateExerciseEssay(this.userUpdateAnswers, userExercise, index);
    }
  }
  onChangeInsertAnswerEssay(questionId: number, content: string, fileUrl: string) {
    const index = this.userInsertAnswers.indexOf(this.userInsertAnswers.find(x => x.questionId == questionId));
    if (index == -1) {
      let userExercise: UserExercise = { questionId: questionId, userAnswers: [], isMark: false, isAnswered: true, isEssayQuestion: true, content: (content == null || content == undefined) ? "" : content, fileUrl: (fileUrl == null || fileUrl == undefined) ? "" : fileUrl };
      this.addExercise(this.userInsertAnswers, userExercise)
    } else {
      let userExercise = this.userInsertAnswers[index];
      userExercise.content = content;
      userExercise.fileUrl = fileUrl;
      userExercise.isAnswered = true;
      this.updateExerciseEssay(this.userInsertAnswers, userExercise, index);
    }
  }

  // cập nhanh dnah sách câu hỏi đã cập nhau sau khi gọi api
  onChangeUpdateAnswer(questionId: number, AnswerOfUsers: AnswerOfUser[]) {
    const index = this.userUpdateAnswers.indexOf(this.userUpdateAnswers.find(x => x.questionId == questionId));
    let oldUserExcercise = this.latedUserAnswers.find(x => x.questionId == questionId);
    if (index == -1) {
      let userExercise: UserExercise = { questionId: questionId, userAnswers: AnswerOfUsers, isMark: oldUserExcercise.isMark, isAnswered: true, isEssayQuestion: false };
      this.addExercise(this.userUpdateAnswers, userExercise)
    } else {
      this.updateExercise(this.userUpdateAnswers, AnswerOfUsers, index);
    }
  }
  onChangeInsertAnswer(questionId: number, AnswerOfUsers: AnswerOfUser[]) {
    const index = this.userInsertAnswers.indexOf(this.userInsertAnswers.find(x => x.questionId == questionId));
    if (index == -1) {
      let userExercise: UserExercise = { questionId: questionId, userAnswers: AnswerOfUsers, isMark: false, isAnswered: true, isEssayQuestion: false };
      this.addExercise(this.userInsertAnswers, userExercise)
    } else {
      this.updateExercise(this.userInsertAnswers, AnswerOfUsers, index);
    }
  }
  updateIsAnsweredGroup(question: QuestionTest, parentId: number) {
    let parentQuestion = this.Questions.find(x => x.id == parentId);
    let check = this.latedUserAnswers.find(x => x.questionId == parentQuestion.id);
    this.onChangeIsAnswredGroupCurrent(parentQuestion);
    if (check != undefined) {
      this.onChangeIsAnswredGroupUpdate(parentQuestion);
    }
    else {
      this.onChangeIsAnswredGroupInsert(parentQuestion);
    }
    this.updateToLocalStorage();
  }
  onChangeIsAnswredGroupInsert(parentQuestion: QuestionTest) {
    const index = this.userInsertAnswers.indexOf(this.userInsertAnswers.find(x => x.questionId == parentQuestion.id));
    if (index == -1) {
      let userExercise: UserExercise = { questionId: parentQuestion.id, userAnswers: [], isMark: false, isAnswered: true, isEssayQuestion: false };
      this.userInsertAnswers.push(userExercise);
    } else {
      this.userInsertAnswers[index].isAnswered = true;
    }
  }
  onChangeIsAnswredGroupUpdate(parentQuestion: QuestionTest) {
    let oldUserExcercise = this.latedUserAnswers.find(x => x.questionId == parentQuestion.id);
    const index = this.userUpdateAnswers.indexOf(this.userUpdateAnswers.find(x => x.questionId == parentQuestion.id));
    if (index == -1) {
      let userExercise: UserExercise = { questionId: parentQuestion.id, userAnswers: [], isMark: oldUserExcercise.isMark, isAnswered: true, isEssayQuestion: false };
      this.userUpdateAnswers.push({ ...userExercise });
    } else {
      this.userUpdateAnswers[index].isAnswered = true;
    }
  }
  onChangeIsAnswredGroupCurrent(parentQuestion: QuestionTest) {
    const index = this.userExercises.indexOf(this.userExercises.find(x => x.questionId == parentQuestion.id));
    if (index == -1) {
      let userExercise: UserExercise = { questionId: parentQuestion.id, userAnswers: [], isMark: false, isAnswered: true, isEssayQuestion: false };
      this.userExercises.push(userExercise);
    } else {
      this.userExercises[index].isAnswered = true;
    }
  }
  // thêm câu trả lời
  addExercise(userExcercises: UserExercise[], userExercise: UserExercise) {
    userExercise.isAnswered = true;
    userExcercises.push({ ...userExercise });
  }
  // cập nhật câu trả lời
  updateExercise(userExcercises: UserExercise[], AnswerOfUsers: AnswerOfUser[], index: number) {
    //update đã trả lời
    if (AnswerOfUsers && AnswerOfUsers.length > 0) {
      userExcercises[index].isAnswered = true;
    } else {
      userExcercises[index].isAnswered = false;
    }
    userExcercises[index].userAnswers = [...AnswerOfUsers];
  }
  updateExerciseEssay(userExcercises: UserExercise[], userExcercise: UserExercise, index: number) {
    userExcercises[index] = { ...userExcercise };
  }
  // đổi trạng thái gắn cờ câu hỏi
  changeTickUserExercise(id: number) {
    this.onChangeTickAnswer(this.userExercises, id);
    let isContain = this.latedUserAnswers.some(x => x.questionId == id);
    if (isContain) {
      this.onChangeTickAnswer(this.userUpdateAnswers, id);
    } else {
      this.onChangeTickAnswer(this.userInsertAnswers, id);
    }
    this.updateToLocalStorage();
  }
  onChangeTickAnswer(userExcercises: UserExercise[], id) {
    let question = this.Questions.find(x => x.id == id);
    let userExcercise = userExcercises.find(x => x.questionId == id);
    let oldExcercise = this.latedUserAnswers.find(x => x.questionId == id);
    if (question.type == Type.essay) {
      if (userExcercise == undefined) {
        if (oldExcercise != undefined) {
          userExcercises.push({ questionId: id, userAnswers: [], isMark: !oldExcercise.isMark, isAnswered: oldExcercise.isAnswered, isEssayQuestion: true, content: oldExcercise.content, fileUrl: oldExcercise.fileUrl });
        }
        else {
          userExcercises.push({ questionId: id, userAnswers: [], isMark: true, isAnswered: false, isEssayQuestion: true });
        }
      }
      else {
        userExcercises.map((x) => {
          if (x.questionId == id) {
            x.isMark = !x.isMark;
          }
        });
      }
    }
    else {
      if (userExcercise == undefined) {
        if (oldExcercise != undefined) {
          userExcercises.push({ questionId: id, userAnswers: oldExcercise.userAnswers, isMark: !oldExcercise.isMark, isAnswered: oldExcercise.isAnswered, isEssayQuestion: false });
        } else {
          userExcercises.push({ questionId: id, userAnswers: [], isMark: true, isAnswered: false, isEssayQuestion: false });
        }
      }
      else {
        userExcercises.map((x) => {
          if (x.questionId == id) {
            x.isMark = !x.isMark;
          }
        });
      }
    }
  }
  getAnswerOfQuestion(questionId: number) {
    return this.userExercises.find(x => x.questionId == questionId);
  }
  // Xóa danh sách các câu hỏi được cập nhật sau khi đã gửi lên server
  clearUpdateAnswerOfUser() {
    this.userUpdateAnswers = [];
    this.userInsertAnswers = [];
    this.latedUserAnswers = [...this.userExercises];
  }
  endExam() {
    this.sendToServer();
    this.userUpdateAnswers = [];
    this.userInsertAnswers = [];
    this.latedUserAnswers = [];
    this.userExercises = [];
    this.storageService.remove(UserExaminationService.userExcerCisesStorageKey);
  }
  sendToServer() {
    console.log('đã gửi dữ liệu lên server');
  }
  userSendSupportMessage(supportMessage: SupportMessage) {
    console.log(supportMessage);
    alert('gửi yêu cầu thành công');
  }
  getTimeRemaining(): Promise<number> {
    return this.userExaminationEndpoint.getTimeRemaining();
  }
  log(): void {
    console.log(this.userExercises);
  }
  updateUserExerciseSubject(): void {
    this.userUpdateAnswer.next(this.userUpdateAnswers);
    this.userAnswersSubject.next(this.userInsertAnswers);
  }

  updateUserExercise(isFinish: boolean, examSubjectStudentId: number): Promise<boolean> {
    localStorage.setItem('insertUserExam', JSON.stringify(this.userInsertAnswers));
    localStorage.setItem('updateUserExam', JSON.stringify(this.userUpdateAnswers));

    if (this.userInsertAnswers.length > 0 || this.userUpdateAnswers.length > 0) {
      let request: UpdateUserExerciseRequest = {
        examSubjectStudentId: examSubjectStudentId,
        testId: this.infoExam.testId,
        testKitId: this.infoExam.testKitId,
        isFinish: isFinish,
        userExercisesAdd: this.userInsertAnswers,
        userExercisesUpdate: this.userUpdateAnswers
      };
      return this.userExaminationEndpoint.updateUserExercise(request);
    }
    else {
      return new BehaviorSubject<boolean>(null).toPromise();
    }
  }
  submitUserExercise(isSuspend: boolean, examSubjectStudentId: number): Promise<boolean> {
    let request: SubmitUserExerciseRequest = {
      examSubjectStudentId: examSubjectStudentId,
      userExercisesAdd: this.userInsertAnswers,
      userExercisesUpdate: this.userUpdateAnswers,
      isSuppend: isSuspend
    }
    return this.userExaminationEndpoint.submitUserExercise(request);
  }
  isCorrectQuestion(questionId: number, questionsResult: QuestionTest[], isChildQuestion?: boolean, parentId?: number): boolean {
    let question: QuestionTest;
    if (isChildQuestion) {
      let parent = questionsResult.find(x => x.id == parentId);
      question = parent.questions.find(x => x.id == questionId);
    } else {
      question = questionsResult.find(x => x.id == questionId);
    }
    if (question == undefined) throw new Error("Question not found!");
    if (question.format == FormatQuestion.group && isChildQuestion != true) {
      let isTrueQuestion: boolean = true;
      question.questions.forEach(x => {
        if (this.isCorrectQuestion(x.id, questionsResult, true, question.id) == false) {
          isTrueQuestion = false;
        }
      })
      return isTrueQuestion;
    }
    let userExercise = this.latedUserAnswers.find(x => x.questionId == questionId);
    if (userExercise == undefined) return false;
    let userAnswers = userExercise.userAnswers;
    switch (question.type) {
      case Type.singlechoice: {
        if (userAnswers && userAnswers.length > 0) {
          let answerId = userAnswers[0].answerId;
          return !(question.answers.find(x => x.id == answerId).trueAnswer != 1)
        } else {
          //alert('câu đơn sai ( chưa làm)')
          return false;
        }
        break;
      }
      case Type.truefasle: {
        let typeAnswer = question.answers[0].content;
        if (userAnswers && userAnswers.length > 0) {
          let trueAnswer = userAnswers[0].answer;
          return ((trueAnswer == 1 && typeAnswer == 'ans-true') || ((trueAnswer == 0 && typeAnswer == 'ans-false')))
        } else {
          //alert('true false sai ( chưa làm)')
          return false;
        }
        break;
      }
      case Type.fillblank: {
        let count = 0;
        let arrayAnswer = [];
        question.answers.forEach(
          element => {
            arrayAnswer.push({ order: 0, content: element });
          }
        )
        let numberOfFillBlank = (question.content.match(/class="Blanking/g) || []).length;
        if (userAnswers && userAnswers.length > 0) {
          for (let index = 0; index < question.answers.length; index++) {
            let ans = question.answers[index];
            let userans = userAnswers.find(x => x.answerId == ans.id);
            if (userans) {
              let data = arrayAnswer.find(x => x.content.id == userans.answerId)
              if (userans.fillInPosition == ans.trueAnswer) {
                count++;
                data.isTrue = true;
              } else {
                data.isTrue = false;
              }
            }
          }
        } else {
          return false;
        }
        return (count == numberOfFillBlank);
        break;
      }
      case Type.essay: {
        return null;
        break;
      }
      case Type.fillblank2: {
        let count = 0;
        if (userAnswers && userAnswers.length > 0) {
          userAnswers.forEach(userans => {
            let answer = question.answers.find(x => x.id == userans.answerId);
            if (answer.trueAnswer == 1 && answer.groupOfFillBlank2 == userans.fillInPosition) {
              count++;
            }
          });
        } else {
          //console.log('chưa làm')
          return false;
        }
        //Kiểm tra lại chỗ này
        return (count == this.groupAnswers(question).length);
        break;
      }
      case Type.multichoice: {
        let isTrueQuestion: boolean = true;
        if (userAnswers && userAnswers.length > 0) {
          let listTrueAnswer = question.answers.filter(x => x.trueAnswer == 1);
          if (listTrueAnswer.length < userAnswers.length) {
            isTrueQuestion = false;
          }
          listTrueAnswer.forEach((trueAnswer) => {
            let isCorrect = userAnswers.find(x => x.answerId == trueAnswer.id && x.answer == 1)
            if (isCorrect == undefined) {
              isTrueQuestion = false;
            }
          });
        } else {
          // console.log('chưa làm')
          isTrueQuestion = false;
        }
        return isTrueQuestion;
        break;
      }
      case Type.truefaslechause: {
        let count = 0;
        if (userAnswers && userAnswers.length > 0) {
          userAnswers.forEach(userans => {
            let answer = question.answers.find(x => x.id == userans.answerId);
            if (answer.trueAnswer == userans.answer) {
              count++;
            }
          });
        } else {
          return false;
        }
        return (count == question.answers.length)
        break;
      }
      case Type.underline: {
        if (userAnswers && userAnswers.length > 0) {
          let answerId = userAnswers[0].answerId;
          return !(question.answers.find(x => x.id == answerId).trueAnswer != 1)
        } else {
          // alert('gạch chân sai ( chưa làm)')
          return false;
        }
        break;
      }
      case Type.matching: {
        // lây số căp đáp án 
        var countTrue = 0;
        var checkTrue = 0;
        var checkFalse = 0;
        let isTrueQuestion: boolean = false;
        for (let j = 0; j < question.answers.length - 1; j++) {
          if (question.answers[j].trueAnswer == question.answers[j + 1].trueAnswer) {
            countTrue++;
          }
        }

        for (let i = 0; i < userAnswers.length; i++) {
          //tìm vế trai
          let left = question.answers.find(x => x.id == userAnswers[i].answerId)?.trueAnswer;
          //tìm vế phải
          let right = question.answers.find(x => x.id == userAnswers[i].answer)?.trueAnswer;
          //so sanh coi cùng đáp án không
          //xuất kết quả
          if (left == right) {
            checkTrue++;
          }
          else {
            checkFalse++;
          }
        }
        //câu trả lời bị sai
        if (checkFalse != 0) {
          isTrueQuestion = false;
        }
        //câu trả lời đúng
        if (checkFalse == 0 && checkTrue != 0) {
          isTrueQuestion = true;
        }
        // trả lời thiếu
        if (checkTrue < countTrue) {
          isTrueQuestion = false;
        }
        return isTrueQuestion;
        break;
      }
      default:
        break;
    }
  }

  SortedAnswers(question: QuestionTest) {
    return question.answers.sort((a, b) => a.groupOfFillBlank2 > b.groupOfFillBlank2 ? 1 : a.groupOfFillBlank2 < b.groupOfFillBlank2 ? -1 : 0);
  }
  groupAnswers(question: QuestionTest) {
    return [...new Set(this.SortedAnswers(question).map(item => item.groupOfFillBlank2))]
  }
}

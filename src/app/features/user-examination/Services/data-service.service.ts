import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { QuestionResponse } from 'src/app/shared/modules/userexam/QuestionResponse';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {

  //private REST_API_SERVER = "http://localhost:3000/dataQuestion";
  _baseUrl = `${environment.apiBaseUrl}/assets/fakedata`;

  constructor(private httpClient: HttpClient) { }

  getQuestionData(): Observable<QuestionResponse[]> {
    return this.httpClient
      .get<any>(`${this._baseUrl}/data.json`)
      .pipe(
        delay(2000), // Simulate request delay
        map(res => {
          return <QuestionResponse[]>res.data;
        }),
        map(data => { return data; })
      )
  }
}
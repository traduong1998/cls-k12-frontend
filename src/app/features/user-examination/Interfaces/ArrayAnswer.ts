import { AnswerTest } from "sdk/cls-k12-sdk-js/src/services/question/models/question-test";

export interface ArrayAnswer{
    order:number;
    content:AnswerTest;
    isTrue?:boolean;
}
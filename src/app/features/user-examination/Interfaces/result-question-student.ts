export interface ResultQuestionStudent{
    questionId: number;
    isCorrect: boolean;
    score?: number;
}
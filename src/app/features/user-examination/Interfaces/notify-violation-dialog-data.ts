export interface NotifyViolationDialogData{
    violatedCode: string;
    creatorId: number;
    reason: string;
    minusScore?: number;
}
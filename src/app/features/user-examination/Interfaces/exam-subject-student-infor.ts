export interface ExamSubjectStudentInfo {
    id: number;
    organizeExamId: number;
    examId: number;
    gradeId: number;
    gradeName: string;
    portalName: string;
    subjectId?: number;
    shiftId?: number;
    subjectName?: string;
    startDate?: Date;
    endDate?: Date;
    timeRemaining?: number;
    examName: string;
    isCompleted: boolean;
    isSuspended: boolean;
    testId: number;
    testKitId: number;
    isShuffle: boolean;
    testCodeURL: string;
    displayPerPage: number;
    numberOfMediaOpening: number;
    isPreservingTime: boolean;
    testTime: number;
    isLockTestScreen: boolean;
    isPublicScore: boolean;
    isPublicAnswer: boolean;
}
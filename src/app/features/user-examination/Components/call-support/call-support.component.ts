import { Component, OnInit } from '@angular/core';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { SupportMessage } from 'src/app/shared/modules/userexam/exam-supervision/SupportMessage';
import { UserExaminationService } from '../../Services/user-examination.service';

@Component({
  selector: 'app-call-support',
  templateUrl: './call-support.component.html',
  styleUrls: ['./call-support.component.scss']
})
export class CallSupportComponent implements OnInit {

  selectedType = "loi-ki-thuat";
  userDescription: string;
  supportMessage: SupportMessage;
  constructor(private UserExaminationService: UserExaminationService) { }

  ngOnInit(): void {
  }
  sendSupportMessage() {
    this.supportMessage = { supportMessageId: generateRandomUniqueInteger(1000, 9999), userId: 1, examId: 50, supportType: this.selectedType, userDescription: this.userDescription, timeRequest: new Date() }
    this.UserExaminationService.userSendSupportMessage(this.supportMessage);
  }
}

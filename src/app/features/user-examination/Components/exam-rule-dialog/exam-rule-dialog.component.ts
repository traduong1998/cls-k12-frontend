import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ExamSubjectStudentInfo } from '../../Interfaces/exam-subject-student-infor';

@Component({
  selector: 'app-exam-rule-dialog',
  templateUrl: './exam-rule-dialog.component.html',
  styleUrls: ['./exam-rule-dialog.component.scss']
})
export class ExamRuleDialogComponent implements OnInit {

  public lockScreenTest: string = '';
  public publicScore: string = '';
  public publicAnswer: string = '';
  constructor(private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public info: ExamSubjectStudentInfo) {
    this.lockScreenTest = info.isLockTestScreen ? ' Không được phép sử dụng các ứng dụng, chương trình khác' : 'Được phép sử dụng các ứng dụng, chương trình khác';
    this.publicScore = info.isPublicScore ? 'Được phép xem điểm sau khi nộp bài' : ' Không được phép xem điểm sau khi nộp bài ';
    this.publicAnswer = info.isPublicAnswer ? 'Được phép xem kết quả bài làm khi kỳ thi kết thúc' : 'Không được phép xem kết quả bài làm khi kỳ thi kết thúc';
  }

  ngOnInit(): void {
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamRuleDialogComponent } from './exam-rule-dialog.component';

describe('ExamRuleDialogComponent', () => {
  let component: ExamRuleDialogComponent;
  let fixture: ComponentFixture<ExamRuleDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExamRuleDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamRuleDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

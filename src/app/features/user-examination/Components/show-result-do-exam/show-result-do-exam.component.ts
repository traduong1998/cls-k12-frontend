import { ResultDoExamResponse } from 'sdk/cls-k12-sdk-js/src/services/user-examination/endpoint/responses/result-do-exam-response';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, HostListener, Input, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { UserExaminationService } from '../../Services/user-examination.service';
import { ExamSubjectStudentInfo } from '../../Interfaces/exam-subject-student-infor';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { ExamRuleDialogComponent } from '../exam-rule-dialog/exam-rule-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-show-result-do-exam',
  templateUrl: './show-result-do-exam.component.html',
  styleUrls: ['./show-result-do-exam.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ShowResultDoExamComponent implements OnInit, OnDestroy {
  isResult: boolean = true;
  resultDoExam: ResultDoExamResponse;
  examSubjectStudentId: number;
  currentPage = 1;
  screenWidth;
  itemsPerPage;
  questions: QuestionTest[] = [];
  questionsResult: QuestionTest[] = [];
  userAnswer = [];
  cdr;
  totalPage: number;
  isReady: boolean = false;
  infoExam: ExamSubjectStudentInfo;
  isExamEnd: boolean;
  get questionType(): typeof Type {
    return Type;
  }
  get questionFormat(): typeof FormatQuestion {
    return FormatQuestion;
  }

  constructor(private userExamServices: UserExaminationService, cdr: ChangeDetectorRef,public dialog: MatDialog, private route: ActivatedRoute, private router: Router, private httpClient: HttpClient, private spinner: NgxSpinnerService) {
    document.body.classList.add(`learn-custom-global-css`);
    this.examSubjectStudentId = +route.snapshot.paramMap.get("id");
    this.onResize();
    this.cdr = cdr;
    this.spinner.show();
    this.userExamServices.initDataShowResult(this.examSubjectStudentId).then(() => {
    });
    this.userExamServices.isReadyResult.subscribe((isReady) => {
      if (isReady) {
        this.infoExam = this.userExamServices.infoExam;
        // let now = new Date(); 
        // let end = new Date(this.infoExam.endDate);
        // this.isExamEnd = ((now.getTime() - end.getTime())> 0);  
        this.itemsPerPage = this.infoExam.displayPerPage;
        this.resultDoExam = this.userExamServices.resultDoExam;
        this.userExamServices.Questions$.subscribe(data => {
          this.questions = this.userExamServices.beauty(this.SortedQuestions(data));
          if (this.questions.length <= this.itemsPerPage) {
            this.totalPage = 1;
          }
          else {
            this.totalPage = Math.floor(this.questions.length / this.itemsPerPage);
            if (this.questions.length % this.itemsPerPage > 0) {
              this.totalPage++;
            };
          }
          if (this.infoExam.isPublicAnswer) {
            this.userExamServices.getRightAnswer().then(testCodeUrl => {
              this.GetListQuestionFromUrl(testCodeUrl.testLink).subscribe((testBaseResult) => {
                this.questionsResult = this.userExamServices.beauty(this.SortedQuestions(testBaseResult["questions"]));
                this.isReady = true;
                this.spinner.hide();

              })
            });
          }
          else {
            this.questionsResult = this.questions;
            this.isReady = true;
            this.spinner.hide();
          }
          console.log("totalPage", this.totalPage);
        });
        this.userAnswer = this.userExamServices.getUserAnswer();

      }
    })
  }
  ngOnDestroy(): void {
    this.userExamServices.clearLocalStorage();
    document.body.classList.remove(`learn-custom-global-css`);
  }


  ngOnInit(): void {
  }
  public GetListQuestionFromUrl(url: string): Observable<any> {
    return this.httpClient.get(url);
  }

  SortedQuestions(questions: QuestionTest[]) {
    return questions.sort((a, b) => a.order > b.order ? 1 : a.order < b.order ? -1 : 0);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenWidth = window.innerWidth;
    // if (this.screenWidth < 960) {
    //   this.itemsPerPage = 1;
    // }
  }

  pagingQuestions() {
    let currentItem = (this.currentPage - 1) * this.itemsPerPage
    return this.questions.splice(currentItem, currentItem + this.itemsPerPage - 1);
  }
  hideloader() {
    // Setting display of spinner 
    // element to none 
    document.getElementById('loading')
      .style.display = 'none';
  }
  userAnswerById(questionId) {
    let answerQuestion = this.userAnswer.find(x => x.questionId == questionId);
    if (answerQuestion) {
      if (answerQuestion.userAnswer) {
        return answerQuestion.userAnswer;
      } else {
        return answerQuestion
      }
    } else return [];

  }
  //chuyển lên đầu trang khi chuyển trang
  pageChanged(event): void {
    this.currentPage = event;
    let firstQuestionOfPage = ((this.currentPage - 1) * this.itemsPerPage) + 1;
    setTimeout(() => {
      var element = document.getElementById("cauhoi_" + firstQuestionOfPage);
      if (element) {
        var elementPosition = element.offsetTop;
      }
      window.scrollTo({
        top: elementPosition,
        behavior: "smooth"
      });
    }, 100);
  }


  scrollToElement(index): void {
    //khi ở man hình nhỏ
    // console.log("crurent",this.currentPage);
    //tìm trang hiện tại của index
    var currentPageOfIndex = Math.floor((index + (this.itemsPerPage - 1)) / this.itemsPerPage);
    // nếu khác trang thì sẽ chuyển trang
    if (this.currentPage != currentPageOfIndex) {
      this.currentPage = currentPageOfIndex;
      setTimeout(() => {
        var element = document.getElementById("cauhoi_" + index);
        var elementPosition = element.offsetTop;
        element.scrollIntoView({ behavior: "smooth" });
        // console.log(elementPosition);
        // var current=document.getElementsByTagName('app-content-exam')[0];
        // window.scrollTo({
        //   top: elementPosition,
        //   behavior: "smooth"
        // });

      }, 300);
    } else {
      var element = document.getElementById("cauhoi_" + index);
      var elementPosition = element.offsetTop;
      element.scrollIntoView({ behavior: "smooth" });
      // var current=document.getElementsByTagName('app-content-exam')[0];
      // window.scrollTo({
      //   top: elementPosition,
      //   behavior: "smooth"
      // });
    }
  }
  onNextPage(): void {
    if (this.currentPage < this.totalPage) {
      this.currentPage++;
      this.pageChanged(this.currentPage);
    }
    // this.currentPage<this.totalPage??this.currentPage++
  }
  onBackPage(): void {
    if (this.currentPage > 0) {
      this.currentPage--;
      this.pageChanged(this.currentPage);
    }
  }

  getQuestionBaseById(questionId: number): QuestionTest {
    let qst = this.questions.find(x => x.id == questionId);
    let baseQuestion = this.questionsResult.find(x => x.id == questionId);
    baseQuestion.order = qst.order;
    return baseQuestion;
  }

  exitButtonHandleClick(): void {
    this.router.navigate([`learner/user-examination`]);
  }

  openExamRuleDialog(): void {
    let dialogRef = this.dialog.open(ExamRuleDialogComponent, { panelClass: 'exam-rule-dialog', data: this.userExamServices.infoExam });
  }
}

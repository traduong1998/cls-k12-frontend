import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowResultDoExamComponent } from './show-result-do-exam.component';

describe('ShowResultDoExamComponent', () => {
  let component: ShowResultDoExamComponent;
  let fixture: ComponentFixture<ShowResultDoExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowResultDoExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowResultDoExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

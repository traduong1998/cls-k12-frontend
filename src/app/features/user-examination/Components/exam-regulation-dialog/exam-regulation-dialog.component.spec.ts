import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamRegulationDialogComponent } from './exam-regulation-dialog.component';

describe('ExamRegulationDialogComponent', () => {
  let component: ExamRegulationDialogComponent;
  let fixture: ComponentFixture<ExamRegulationDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExamRegulationDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamRegulationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

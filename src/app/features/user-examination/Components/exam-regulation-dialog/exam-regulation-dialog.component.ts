import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserExaminationEndpoint } from 'cls-k12-sdk-js/src/services/user-examination/endpoint/user-examination.enpoint';

@Component({
  selector: 'app-exam-regulation-dialog',
  templateUrl: './exam-regulation-dialog.component.html',
  styleUrls: ['./exam-regulation-dialog.component.scss']
})
export class ExamRegulationDialogComponent implements OnInit {
  userExaminationEndpoint: UserExaminationEndpoint;

  public portalName: string = 'Sở giáo dục';
  public examName: string = 'Kỳ thi cuối học kì I';
  public subjectName: string = 'Môn thi toán 12';
  public lockScreenTest: string = '';
  public publicScore: string = '';
  public publicAnswer: string = '';
  public testTime: number = 0;
  public isStartDoExam: boolean = false;
  private startDate: Date;
  public examId: number;
  public subjectId: number;

  constructor(
    public dialogRef: MatDialogRef<ExamRegulationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      examId: number,
      subjectId: number,
      shiftId: number
    }) {
    this.userExaminationEndpoint = new UserExaminationEndpoint();
  }

  ngOnInit(): void {
    this.userExaminationEndpoint.getDetailInfoExam(this.data.examId, this.data.subjectId, this.data.shiftId).then(res => {
      this.examName = res.name;
      this.lockScreenTest = res.isLockTestScreen ? ' Không được phép sử dụng các ứng dụng, chương trình khác' : 'Được phép sử dụng các ứng dụng, chương trình khác';
      this.publicScore = res.isPublicScore ? 'Được phép xem điểm sau khi nộp bài' : ' Không được phép xem điểm sau khi nộp bài ';
      this.publicAnswer = res.isPublicAnswer ? 'Được phép xem kết quả bài làm khi kỳ thi kết thúc' : 'Không được phép xem kết quả bài làm khi kỳ thi kết thúc';
      this.testTime = res.testTime;
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}

import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, OnChanges, SimpleChanges } from '@angular/core';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { AnswerOfUser, UserExercise } from 'src/app/shared/modules/userexam/UserAnswer';
import { UserExaminationService } from '../../Services/user-examination.service';

@Component({
  selector: 'app-multiple-choice',
  templateUrl: './multiple-choice.component.html',
  styleUrls: ['./multiple-choice.component.scss']
})
export class MultipleChoiceComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() dataQuestion: QuestionTest;
  @Input() userAnswer;
  @Input() isResult;
  @Input() parentId: number;
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  @Input() isGroup;
  
  @Input() isPublicAnswer:boolean;
  @Output() dataOfGroup = new EventEmitter();
  @Output() reciveTrueQuestionEvent = new EventEmitter();
  isFlag = false;
  isTrueQuestion: boolean;
  selectedAnswer: AnswerOfUser[] = [];
  checkedAnswer: boolean[] = [];
  format = FormatQuestion;
  isReady: boolean = false;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  constructor(private userExamServices: UserExaminationService) { }
  ngOnChanges(changes: SimpleChanges): void {
    this.isReady = true;
  }
  ngAfterViewInit(): void {
    this.CheckTrueQuestion();
  }
  CheckTrueQuestion() {
    this.reciveTrueQuestionEvent.emit({ isTrueQuestion: this.isTrueQuestion, questionId: this.dataQuestion.id });
  }

  ngOnInit(): void {
    this.isGroup = this.dataQuestion.format;
    var oldData = this.userExamServices.getAnswerOfQuestion(this.dataQuestion.id);
    if (oldData) {
      this.isFlag = oldData.isMark;
    }
    if (oldData && oldData.userAnswers.length > 0) {
      this.AnswerOfUser = [...oldData.userAnswers];
      this.selectedAnswer = oldData.userAnswers;
      for (let index = 0; index < this.dataQuestion.answers.length; index++) {
        if (this.selectedAnswer.some(x => x.answerId == this.dataQuestion.answers[index].id)) {
          this.checkedAnswer.push(true);
        }
        else {
          this.checkedAnswer.push(false)
        }
      }
    }
    //bôi đen đáp án người dùng
    if (this.userAnswer) {
      for (let i = 0; i < this.userAnswer.length; i++) {
        if (this.dataQuestion.id == this.userAnswer[i].questionId) {
          // this.rightAnswer = this.userAnswer[i].userAnswer[0].answerId;
        }
      }
    }
    if (this.isResult) {
      this.isAnswerTrue();
    }
  }

  isMark() {
    this.isFlag = !this.isFlag;
    this.userExamServices.changeTickUserExercise(this.dataQuestion.id);
  }
  AnswerOfUser: AnswerOfUser[] = [];
  AnswerOfUserGroup: UserExercise;
  isTrueAnswers: boolean[] = [];
  isAnswerTrue() {
    this.isTrueQuestion = true;
    let userAnswers = this.userAnswer.userAnswers;
    if (userAnswers && userAnswers.length > 0) {
      let listTrueAnswer = this.dataQuestion.answers.filter(x => x.trueAnswer == 1);
      if(listTrueAnswer.length < userAnswers.length){
        this.isTrueQuestion = false;
      }
      else{
        listTrueAnswer.forEach((trueAnswer) => {
          let isCorrect = userAnswers.find(x => x.answerId == trueAnswer.id && x.answer == 1)
          if (isCorrect == undefined) {
            this.isTrueQuestion = false;
          }
        });
      }
    } else {
      // console.log('chưa làm')
      this.isTrueQuestion = false;
    }
  }

  changeSelectAnswer(event, item): void {
    if (event.checked == false) {
      let index = this.AnswerOfUser.findIndex(x => x.answerId == item.id);
      this.AnswerOfUser.splice(index, 1);
    }
    else {
      this.AnswerOfUser.push({ answerId: item.id, answer: 1 })
    }
    this.userExamServices.onChangeUserExercise(this.dataQuestion.id, this.AnswerOfUser);
    if (this.parentId != undefined) {
      this.userExamServices.updateIsAnsweredGroup(this.dataQuestion, this.parentId);
    }
  }
}

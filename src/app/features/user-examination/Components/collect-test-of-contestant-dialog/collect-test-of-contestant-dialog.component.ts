import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserExamViewModel } from '../../Views/exam-supervision/exam-supervision.component';

@Component({
  selector: 'use-before-do-exam',
  templateUrl: './collect-test-of-contestant-dialog.component.html',
  styleUrls: ['./collect-test-of-contestant-dialog.component.scss']
})
export class CollectTestOfContestantDialog implements OnInit {
  state: 'wait' | 'now';
  contestant: UserExamViewModel;

  //#region CONSTRUCTOR
  constructor(public dialogRef: MatDialogRef<CollectTestOfContestantDialog>,
    @Inject(MAT_DIALOG_DATA) public data: { contestant: UserExamViewModel }) {
    console.log(`CollectTestOfContestantDialog`, data);
    this.contestant = data.contestant;
  }

  //#endregion

  //#region LIFECYCLE
  /**
 * ngOnInit
 */
  ngOnInit(): void {

  }
  ngOnDestroy(): void {

  }
  //#endregion

  //#region Public PUBLIC METHOD
  collectTest(state: 'wait' | 'now') {
    this.dialogRef.close({
      state: state
    });
  }
  //#endregion

  //#region PRIVATE METHOD


  //#endregion
}

import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, OnChanges, SimpleChanges } from '@angular/core';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { AnswerOfUser, UserExercise } from 'src/app/shared/modules/userexam/UserAnswer';
import { UserExaminationService } from '../../Services/user-examination.service';

@Component({
  selector: 'app-true-false-clause',
  templateUrl: './true-false-clause.component.html',
  styleUrls: ['./true-false-clause.component.scss']
})
export class TrueFalseClauseComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() dataQuestion: QuestionTest;
  @Input() userAnswer;
  @Input() isResult;
  @Input() parentId;
  @Input() isGroup;
  @Input() isPublicAnswer:boolean;
  @Output() dataOfGroup = new EventEmitter;
  format = FormatQuestion;
  rightAnswer;
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  isFlag = false;
  isTrueQuestion;
  AnswerOfUser: AnswerOfUser[] = [];
  AnswerOfUserGroup: UserExercise;
  selectedAnswer: AnswerOfUser[] = [];
  isReady: boolean = false;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  constructor(private userExamServices: UserExaminationService) { }
  ngOnChanges(changes: SimpleChanges): void {
    this.isReady = true;
  }
  ngAfterViewInit(): void {

  }

  ngOnInit(): void {
    this.isGroup = this.dataQuestion.format;
    var oldData = this.userExamServices.getAnswerOfQuestion(this.dataQuestion.id);
    if (oldData) {
      this.isFlag = oldData.isMark;
    }
    if (oldData && oldData.userAnswers.length > 0) {
      this.AnswerOfUser = oldData.userAnswers;
      this.selectedAnswer = oldData.userAnswers;
    }

    if (this.userAnswer) {
      for (let i = 0; i < this.userAnswer.length; i++) {
        if (this.dataQuestion.id == this.userAnswer[i].questionId) {
          this.rightAnswer = this.userAnswer[i].userAnswer;
        }
      }
    }
    if (this.isResult) {
      this.isAnswerTrue();
    }
  }
  isMark() {
    this.isFlag = !this.isFlag;
    this.userExamServices.changeTickUserExercise(this.dataQuestion.id);
  }
  isCheckedAnswer(id) {
    let answer = this.selectedAnswer.find(x => x.answerId == id);
    if (answer) {
      return answer.answer;
    }
  }
  isAnswerTrue() {
    let count = 0;
    let userAnswers = this.userAnswer.userAnswers;
    if (userAnswers && userAnswers.length > 0) {
      userAnswers.forEach(userans => {
        let answer = this.dataQuestion.answers.find(x => x.id == userans.answerId);
        if (answer.trueAnswer == userans.answer) {
          count++;
        }
      });
    } else {
      //console.log('chưa làm')
      this.isTrueQuestion = false;
    }
    if (count == this.dataQuestion.answers.length) {
      //console.log('đúng')
      this.isTrueQuestion = true;
    } else {
      //console.log('sai')
      this.isTrueQuestion = false;
    }
  }
  changeSelectAnswer(e, id) {
    if (this.AnswerOfUser.some(x => x.answerId == id) == true) {
      this.AnswerOfUser.map((ans, i) => {
        if (ans.answerId == id) {
          ans.answer = parseInt(e.value);
        }
      });
    } else {
      this.AnswerOfUser.push({ answerId: id, answer: parseInt(e.value) });
    }
    this.userExamServices.onChangeUserExercise(this.dataQuestion.id, this.AnswerOfUser)
    if (this.parentId != undefined) {
      this.userExamServices.updateIsAnsweredGroup(this.dataQuestion, this.parentId);
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrueFalseClauseComponent } from './true-false-clause.component';

describe('TrueFalseClauseComponent', () => {
  let component: TrueFalseClauseComponent;
  let fixture: ComponentFixture<TrueFalseClauseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrueFalseClauseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrueFalseClauseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

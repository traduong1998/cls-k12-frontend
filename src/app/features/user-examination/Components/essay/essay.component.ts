import { MatSnackBar } from '@angular/material/snack-bar';
import { AfterViewInit, Component, Input, OnInit, ViewChild, OnChanges, SimpleChanges, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CKEditorComponent } from 'ckeditor4-angular';
import { Observable, of, iif } from 'rxjs';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { ProgressDialogComponent } from 'src/app/features/learn/components/progress-dialog/progress-dialog.component';
import { isNullOrWhiteSpace, isNullOrEmpty } from 'src/app/shared/helpers/validation.helper';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { UserExaminationService } from '../../Services/user-examination.service';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';

@Component({
  selector: 'app-essay',
  templateUrl: './essay.component.html',
  styleUrls: ['./essay.component.scss']
})
export class EssayComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() dataQuestion;
  @Input() userAnswer;
  @Input() isResult;
  @Input() examSubjectStudentId: number;
  @ViewChild("ck") ck: CKEditorComponent
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>;
  private fileUrl: string;
  progressData: Observable<any>;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }
  fileServer: FileServerEndpoint;
  isFlag = false;
  ckeditorContent;
  configQuestion;
  configCkeditor;
  numbers: 0;
  fileUrlResponse: string;
  fileName: string;
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  @ViewChild("fileQuestionEssay") fileQuestionEssay: ElementRef;

  constructor(private userExamServices: UserExaminationService, public dialog: MatDialog, private _snackBar: MatSnackBar) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      // startupFocus: true,
      allowedContent: true,
      extraPlugins: ckConfig.extraPlugins,
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
    };
    this.configCkeditor = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      // startupFocus: true,
      allowedContent: true,
      extraPlugins: ckConfig.extraPlugins,
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
      readOnly: ckConfig.readOnly
    }
    this.fileServer = new FileServerEndpoint();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.fileName = localStorage.getItem(`file-${this.examSubjectStudentId}-${this.dataQuestion.id}`);
    if (isNullOrEmpty(this.fileName)) {
      if (!isNullOrEmpty(this.userAnswer.fileUrl)) {
        this.fileName = this.userAnswer.fileUrl.slice(this.userAnswer.fileUrl.lastIndexOf('/') + 1);
      }
    }
  }
  ngAfterViewInit(): void {

  }

  ngOnInit(): void {
    var oldData = this.userExamServices.getAnswerOfQuestion(this.dataQuestion.id);
    if (oldData) {
      this.isFlag = oldData.isMark;
      this.fileUrlResponse = oldData.fileUrl;
    }
    if (oldData && !isNullOrEmpty(oldData.content)) {
      this.ckeditorContent = oldData.content;
    }
    if (this.isResult) {
    }
  }
  isMark() {
    this.isFlag = !this.isFlag;
    this.userExamServices.changeTickUserExercise(this.dataQuestion.id);
    this.userExamServices.log();
  }

  uploadFileDocument(e) {
    const file = e.target.files[0];
    var type = file.name.substring(file.name.lastIndexOf(".") + 1).toLowerCase();
    if (["ppt", "pptx", "doc", "docx", "xls", "xlsx", "pdf", "jpeg", "png", "gif", "svg", "jpg"].includes(type)) {
      // this.documentFile = file;
      // this.nameFileDocument = file.name;
      Promise.resolve()
        .then(() => {
          this.openProgressDialog(null);
          //gửi video
          return this.fileServer.uploadFile({
            file: file,
            isPrivate: false,
            moduleName: ServiceName.EXAM
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          });

        })
        .then((res) => {
          this.fileUrl = res?.link;
          this.fileName = file.name;
          localStorage.setItem(`file-${this.examSubjectStudentId}-${this.dataQuestion.id}`, file.name);
          this.userExamServices.onChangeUserExerciseEssay(this.dataQuestion.id, this.ckeditorContent, this.fileUrl);
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
    else {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Không đúng định dạng file, mời bạn nhập lại",
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
    }
  }
  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.numbers }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  updateContentEssay(): void {
    this.userExamServices.onChangeUserExerciseEssay(this.dataQuestion.id, this.ckeditorContent, this.fileUrl);
  }
  downLoadFile(): void {
    window.open(this.fileUrlResponse);
  }
  addFile(): void {
    this.fileQuestionEssay.nativeElement.click();
  }
}

import { Component, Input, OnInit } from '@angular/core';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { AnswerOfUser } from 'src/app/shared/modules/userexam/UserAnswer';
import { UserExaminationService } from '../../Services/user-examination.service';

@Component({
  selector: 'app-group-question',
  templateUrl: './group-question.component.html',
  styleUrls: ['./group-question.component.scss']
})
export class GroupQuestionComponent implements OnInit {
  @Input() dataQuestion:QuestionTest;
  @Input() isResult;
  @Input() order:number;
  @Input() isPublicAnswer:boolean;
  userAnswer = [];
  type=Type;
  isFlag = false;
  isGroup = FormatQuestion.group;
  AnswerOfUser: AnswerOfUser[] = [];
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  constructor(private userExamServices: UserExaminationService) { }

  ngOnInit(): void {
    var oldData = this.userExamServices.getAnswerOfQuestion(this.dataQuestion.id);
    if (oldData) {
      this.isFlag = oldData.isMark;
    }
    this.userAnswer = this.userExamServices.getUserAnswer();
    console.log('câu chùm',this.dataQuestion);
  }
  isMark() {
    this.isFlag = !this.isFlag;
    this.userExamServices.changeTickUserExercise(this.dataQuestion.id);
  }
  userAnswerById(questionId){
    let answerQuestion=this.userAnswer.find(x=>x.questionId==questionId);
    if(answerQuestion){
      if(answerQuestion.userAnswer){
        return answerQuestion.userAnswer;
      }else{
        return answerQuestion
      }
      
    }else return [];
   
  }

}

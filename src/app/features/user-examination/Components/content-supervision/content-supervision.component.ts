import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ContextMenuComponent } from "ngx-contextmenu";
import { UserExamSupervisonService } from 'src/app/core/services/UserExamSupervison/user-exam-supervison.service';
import { hideloader } from 'src/app/shared/helpers/cls.helper';
import { HandlingViolationsComponent } from '../handling-violations/handling-violations.component';
import { InforUserDetailComponent } from '../infor-user-detail/infor-user-detail.component';
import {UserExamInfoSmallComponent} from '../user-exam-info-small/user-exam-info-small.component'

@Component({
  selector: 'app-content-supervision',
  templateUrl: './content-supervision.component.html',
  styleUrls: ['./content-supervision.component.scss']
})
export class ContentSupervisionComponent implements OnInit {
  userExamData;
  currentPage=1;
  selectedUserPerPage=0;
  watchingPage ="detail";
  selectedStatus="all";
  name;
  @ViewChild(ContextMenuComponent) public basicMenu: ContextMenuComponent;
  constructor(private userExamSupervision:UserExamSupervisonService,public dialog: MatDialog) { }
  ngOnInit(): void {
    this.userExamSupervision.getUserExamData().subscribe(data => {
      this.userExamData = data;
      hideloader();
    });
  }
  pageChanged(event): void {
    this.currentPage = event;
  }
  resetCurentPage(){
    this.currentPage=1;
  }
  WatchingPage(event){
    this.watchingPage= event.value;
  }
  ShowDetail(userExam){
    this.dialog.open(InforUserDetailComponent, {panelClass: 'my-full-screen-dialog',data:userExam});
  }

  HandlingViolations(){
    this.dialog.open(HandlingViolationsComponent, {panelClass: 'callsupport-dialog'});
  }
  
}

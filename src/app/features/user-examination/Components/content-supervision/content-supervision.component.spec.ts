import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentSupervisionComponent } from './content-supervision.component';

describe('ContentSupervisionComponent', () => {
  let component: ContentSupervisionComponent;
  let fixture: ComponentFixture<ContentSupervisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentSupervisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentSupervisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

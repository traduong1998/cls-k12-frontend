import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { UserExercise } from 'src/app/shared/modules/userexam/UserAnswer';
import { UserExaminationService } from '../../Services/user-examination.service';

@Component({
  selector: 'app-true-false',
  templateUrl: './true-false.component.html',
  styleUrls: ['./true-false.component.scss']
})
export class TrueFalseComponent implements OnInit {
  @Input() dataQuestion:QuestionTest;
  @Input() userAnswer;
  @Input() isResult;
  @Input() parentId;
  @Input() isPublicAnswer:boolean;
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  @Input() isGroup;
  @Output() dataOfGroup = new EventEmitter();
  isFlag = false;
  isTrueQuestion;
  selectedAnswer: number;
  rightAnswer;
  type=Type;
  format=FormatQuestion;
  AnswerOfUserGroup: UserExercise;
  constructor(private userExamServices: UserExaminationService) { }

  ngOnInit(): void {
    this.isGroup = this.dataQuestion.format;
    var oldData = this.userExamServices.getAnswerOfQuestion(this.dataQuestion.id);
    if (oldData) {
      this.isFlag = oldData.isMark;
    }
    if (oldData && oldData.userAnswers.length > 0) {
      this.selectedAnswer = oldData.userAnswers[0].answer;
    }
    if (this.isResult) {
      this.isAnswerTrue();
    }
  }
  isMark() {
    this.isFlag = !this.isFlag;
    this.userExamServices.changeTickUserExercise(this.dataQuestion.id);
  }
  isAnswerTrue() {
    let typeQuestion = this.dataQuestion.answers[0].content;
    let userAnswers = this.userAnswer.userAnswers;
    if (userAnswers && userAnswers.length > 0) {
      let trueAnswer = userAnswers[0].answer;
      if ((trueAnswer == 1 && typeQuestion == 'ans-true') || ((trueAnswer == 0 && typeQuestion == 'ans-false'))) {
        //alert('true false đúng ')
        this.isTrueQuestion =true;
      } else {
        //alert('true false sai')
        this.isTrueQuestion =false;
      }
    } else {
      //alert('true false sai ( chưa làm)')
      this.isTrueQuestion =false;
    }
  }
  changeSelectAnswer(e, id) {
    this.AnswerOfUserGroup = { questionId: this.dataQuestion.id, userAnswers: [{ answerId: id, answer: parseInt(e.value) }], isAnswered: true, isMark: null }
    this.userExamServices.onChangeUserExercise(this.dataQuestion.id, [{ answerId: id, answer: parseInt(e.value) }])
    if (this.parentId != undefined) {
      this.userExamServices.updateIsAnsweredGroup(this.dataQuestion,this.parentId);
    }
  }
}

import { Component, HostListener, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from 'constants';
import { UserExaminationService } from '../../Services/user-examination.service';
import { AnswerOfUser, UserExercise } from 'src/app/shared/modules/userexam/UserAnswer';
import { ArrayAnswer } from '../../Interfaces/ArrayAnswer';
import { QuestionTest } from 'src/app/features/learn/intefaces/questionTest';
declare var $: any;

@Component({
  selector: 'app-matching',
  templateUrl: './matching.component.html',
  styleUrls: ['./matching.component.scss']
})
export class MatchingComponent implements OnInit {
  @Input() dataQuestion;
  @Input() userAnswer;
  @Input() isResult;
  @Input() isPublicAnswer: boolean;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  answers;
  //biên
  IdQuestion;
  isDisabledAnswer = true;
  isLoadData = true;
  isTrueQuestion: boolean = false;
  screenWidth;
  AnswerOfUserTemp = [];
  data: ArrayAnswer[] = [];

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenWidth = window.innerWidth;

    if (this.userAnswer) {
      for (let i = 0; i < this.answers.false.length; i++) {
        //gán datamatch cho những thẻ có chưa câu trả lời
        for (let j = 0; j < this.userAnswer.length; j++) {
          if (this.answers.false[i].id == this.userAnswer[j].answerId) {
            $('.right-position-' + this.userAnswer[j].answer).css('right', 20);
          }
        }
      }
    }
    else {
      if (this.dataQuestion) {
        var oldData = this.userExamServices.getAnswerOfQuestion(this.dataQuestion.id);
        if (oldData != null || oldData != undefined) {
          for (let i = 0; i < this.answers.false.length; i++) {
            for (let j = 0; j < oldData.userAnswers.length; j++) {
              if (this.answers.false[i].id == oldData.userAnswers[j].answerId) {
                $('.right-position-' + oldData.userAnswers[j].answer).css('left', 'calc(50% - 25px)');
              }
            }
          }
        }
      }
    }
  }
  isFlag = false;
  constructor(private userExamServices: UserExaminationService, private cdRef: ChangeDetectorRef) {
    this.onResize();
  }
  ngAfterViewChecked(): void {
    this.cdRef.detectChanges();
  }
  ngOnInit(): void {
    //tach nhóm
    this.answers = this.dataQuestion.answers.reduce((r, a) => {
      r[a.position] = [...r[a.position] || [], a];
      return r;
    }, {});
    this.IdQuestion = this.dataQuestion.id;
    var oldData = this.userExamServices.getAnswerOfQuestion(this.dataQuestion.id);
    if (oldData) {
      this.isFlag = oldData.isMark;
    }
    if (this.screenWidth < 960) {
      this.initOnMobile();
    }
  }
  ngAfterViewInit(): void {
    this.dragItem(this.IdQuestion, this.answers);
    //load lại câu hỏi
    var oldData = this.userExamServices.getAnswerOfQuestion(this.dataQuestion.id);
    if (oldData != null || oldData != undefined) {
      let index = 0;
      let topIndexs: number[] = [];
      this.answers.true.forEach((rightElement, i) => {
        topIndexs.push(i);
      });
      for (let i = 0; i < this.answers.false.length; i++) {
        //gán datamatch cho những thẻ có chưa câu trả lời
        // let tem = this.answers.false[i].id;
        for (let j = 0; j < oldData.userAnswers.length; j++) {
          if (this.answers.false[i].id == oldData.userAnswers[j].answerId) {
            $('.right-position-' + oldData.userAnswers[j].answer).css('left', (this.isResult && this.isPublicAnswer) ? 'calc(50% - 1px)' : 'calc(50% - 25px)');
            let top = index * 100;
            topIndexs.splice(topIndexs.findIndex(x => x == index), 1);
            $('.right-position-' + oldData.userAnswers[j].answer).css('top', top + 'px');
            $('.right-position-' + oldData.userAnswers[j].answer).attr('data-position-top-right', top);
            $('.right-position-' + oldData.userAnswers[j].answer).css('z-index', 1);
          }
        }
        index++;
      }
      this.answers.true.forEach((rightElement) => {
        if (oldData.userAnswers.find(x => x.answer == rightElement.id) == undefined) {
          let top = topIndexs.shift() * 100;
          $('.right-position-' + rightElement.id).css('top', top + 'px');
          $('.right-position-' + rightElement.id).attr('data-position-top-right', top);
        }
      });
    }

    var checkTrue = 0;
    var checkFalse = 0;
    if (this.isResult) {
      $('.answer-list-' + this.IdQuestion + ' .right-position').draggable({
        disabled: true
      });

      //kiểm tra đúng sai
      if (oldData) {
        // lây số căp đáp án 
        var countTrue = 0;
        for (let j = 0; j < this.dataQuestion.answers.length - 1; j++) {
          if (this.dataQuestion.answers[j].trueAnswer == this.dataQuestion.answers[j + 1].trueAnswer) {
            countTrue++;
          }
        }
        for (let i = 0; i < oldData.userAnswers.length; i++) {
          //tìm vế trai
          let left = this.dataQuestion.answers.find(x => x.id == oldData.userAnswers[i].answerId)?.trueAnswer;
          //tìm vế phải
          let right = this.dataQuestion.answers.find(x => x.id == oldData.userAnswers[i].answer)?.trueAnswer;
          //so sanh coi cùng đáp án không
          //xuất kết quả
          if (left == right) {
            if (this.isPublicAnswer) {
              $('.check-' + oldData.userAnswers[i].answerId).append("<mat-icon _ngcontent-serverapp-c205=\"\" role=\"img\" class=\"mat-icon notranslate check-true material-icons mat-icon-no-color done\" style=\"color: #4CAF50\" aria-hidden=\"true\" data-mat-icon-type=\"font\">done</mat-icon>");
            }
            checkTrue++;
            if (this.screenWidth < 960) {
              this.data.find(x => x.content.id == this.userAnswer[i].answerId).isTrue = true;
            }
          }
          else {
            if (this.isPublicAnswer) {
              $('.check-' + oldData.userAnswers[i].answerId).append("<mat-icon _ngcontent-serverapp-c207=\"\" role=\"img\" class=\"mat-icon notranslate wrong-answer material-icons mat-icon-no-color close\" style=\"color: #DA251D\" aria-hidden=\"true\" data-mat-icon-type=\"font\">close</mat-icon>");
            }
            checkFalse++;
            if (this.screenWidth < 960) {
              this.data.find(x => x.content.id == this.userAnswer[i].answerId).isTrue = false;
            }
          }
        }
        //câu trả lời bị sai
        if (checkFalse != 0) {
          this.isTrueQuestion = false;
        }
        //câu trả lời đúng
        if (checkFalse == 0 && checkTrue != 0) {
          this.isTrueQuestion = true;
        }
        // trả lời thiếu
        if (checkTrue < countTrue) {
          this.isTrueQuestion = false;
        }
      }
    }
  }
  dragItem(maCauHoi, answers) {
    this.isLoadData = false;
    this.thucHienCauHoiGhepDoi(maCauHoi);
    //cập nhật lại lại vị trí khi thay đổi câu hỏi
    $('answer-list .right-position').sortable();
  }




  thucHienCauHoiGhepDoi(maCauHoi) {
    //check câu trả lời
    var AnswerOfUserTemp = []
    var dataLoading = this.userExamServices.getAnswerOfQuestion(maCauHoi);
    if (dataLoading != null || dataLoading != undefined) {
      AnswerOfUserTemp = dataLoading.userAnswers;
    }
    //thực hiện
    var countTagRightPosition = $('.answer-list-' + maCauHoi).find('.right-position').length;
    
    var countTagLeftPosition = $('.answer-list-' + maCauHoi).find('.left-position').length;
    if (countTagLeftPosition > countTagRightPosition) {
      $('.answer-list-' + maCauHoi).css({ 'margin-left': '0', 'height': 100 * countTagLeftPosition});
    } else {
      $('.answer-list-' + maCauHoi).css({ 'margin-left': '0', 'height': 100 * countTagRightPosition });
    }
    var valueTopCss = 0;
    $('.answer-list-' + maCauHoi).find('.right-position').each(function (index, ele) {
      $(this).css('top', valueTopCss);
      $(this).attr('data-position-top-right', valueTopCss);
      //$(this).css('z-index', 0);
      valueTopCss += 100;
    });

    var valueTopPosition = 0;
    $('.answer-list-' + maCauHoi).find('.left-position').each(function (index, ele) {
      $(this).attr({ 'data-position-top-left': valueTopPosition });
      valueTopPosition += 100;
    });

    var firstTop = 0;
    if (this.isDisabledAnswer) {
      // var firstTop;
      $('.answer-list-' + maCauHoi + ' .right-position').draggable({
        containment: 'parent',
        cursor: 'move',
        revert: true,
        start: (event, ui) => {
          ui.helper.addClass('dragging');
          var data = ui.helper.attr('data-id');
          $('.full-sentence[data-match=' + data + ']').attr('data-match', '');
          //firstTop = ui.helper.css('top');
          firstTop = ui.helper.attr('data-position-top-right');
        },
        stop: (event, ui) => {
          ui.helper.removeClass('dragging');
          console.log("ui", ui);
          var data = ui.helper.attr('data-id');
          ui.helper.draggable('option', 'revert', true);
        }
      });

      $('.answer-list-' + maCauHoi + ' .full-sentence').droppable({
        accept: '.right-position',
        hoverClass: 'hovered',
        drop: (event, ui) => {
          var drag = ui.draggable;
          //var $el = $(event.target);
          var drop = $(event.target);
          //var savedata = $el;

          var postionData = drop.find('*[data-position-top-left]').attr('data-position-top-left') + 'px';
          if (drop.attr('data-match') != '') {
            var myWidth = $('.right-position[data-id=' + drop.attr('data-match') + ']').width();
            drop.parent().children('.right-position[data-id=' + drop.attr('data-match') + ']')
              .css('top', firstTop)
              .css('right', '0')
              .css('left', 'auto')
              .css('bottom', 'auto');
          };

          drop.attr('data-match', drag.attr('data-id'));
          drag.draggable('option', 'revert', false);
          drag.position({ of: drop, my: 'right top', at: 'right top' });
          drag.parent().children('.right-position:not([data-id=' + drag.attr('data-id') + '])').each(function () {
            if (drop.css('top') === postionData) {
              drop.css('top', firstTop + 'px');
            }
          });


          // Lấy dữ liệu người dùng đã trả lời
          let maPhuongAn = parseInt($(drop).attr('data-id'));
          let dapAnNguoiDung = parseInt($(drop).attr('data-match'));
          $('.answer-list-' + maCauHoi).find('.right-position').each(function (index, ele) {
            let xxx = $(ele).attr('data-position-top-right');
            let a = $(drop).children().attr('data-position-top-left');
            let b = firstTop;

            // nếu vế phải có position top == vế trái hiện tại và ko phải là đáp án cảu người dùng thì thế chỗ cái phương án ban đầu
            if ($(ele).attr('data-position-top-right') == $(drop).children().attr('data-position-top-left') && $(ele).attr('data-id') != dapAnNguoiDung) {
              $(ele).css('top', firstTop + 'px').css('right', '0').css('left', 'auto').css('bottom', 'auto');

              $(ele).attr('data-position-top-right', firstTop);
            }
            //
            else if ($(ele).attr('data-id') == dapAnNguoiDung) {
              $(ele).attr('data-position-top-right', $(drop).children().attr('data-position-top-left'));
            }
          });
          // Nếu vế trái đã được ghép với vế phải nào trước đó
          let oldAnswerOfUser = AnswerOfUserTemp.find(x => x.answerId == maPhuongAn);
          if (oldAnswerOfUser != undefined) {
            // Nếu vế phải đã được ghép trước đó khác với vế phải hiện tại
            if (oldAnswerOfUser.answer != dapAnNguoiDung) {
              // gỡ đáp án cũ ra
              AnswerOfUserTemp.splice(AnswerOfUserTemp.indexOf(oldAnswerOfUser), 1);
              // $('.right-position-' + oldAnswerOfUser.answer).css('top', firstTop + 'px');
              // $('.right-position-' + oldAnswerOfUser.answer).attr('data-position-top-right', firstTop);
            }
            //kiểm tra đáp án người dùng đã được gán cho cái nào trc đó rồi gỡ ra
            if (AnswerOfUserTemp.some(x => x.answer == dapAnNguoiDung) == true) {
              //gỡ nó ra
              let index = AnswerOfUserTemp.indexOf(AnswerOfUserTemp.find(x => x.answer == dapAnNguoiDung));
              AnswerOfUserTemp.splice(index, 1);
            }
            //thêm đáp án vào
            AnswerOfUserTemp.push({ answerId: maPhuongAn, answer: dapAnNguoiDung });
          }
          // không thì thêm mới vào
          else {
            AnswerOfUserTemp.push({ answerId: maPhuongAn, answer: dapAnNguoiDung });
          }
          this.userExamServices.onChangeUserExercise(parseInt(maCauHoi), AnswerOfUserTemp);

        },
      });
    }

  }

  isMark() {
    this.isFlag = !this.isFlag;
    this.userExamServices.changeTickUserExercise(this.dataQuestion.id);
  }

  groupAnswers() {
    let a = [...new Set(this.dataQuestion.answers.map(item => item.trueAnswer))]
    return a;
  }
  answersOfGroup(index) {
    let a = [...this.dataQuestion.answers].filter(x => x.trueAnswer == index).sort((a,b)=> b.position - a.position);
    return a;
  }

  initOnMobile() {
    this.answers.false.forEach(
      element => {
        this.data.push({ order: 0, content: element });
      }
    )
    //lòa load data cũ
    var oldData = this.userExamServices.getAnswerOfQuestion(this.dataQuestion.id);
    if (oldData != null || oldData != undefined) {
      this.AnswerOfUserTemp = oldData.userAnswers;
    }

    if (oldData && oldData.userAnswers.length > 0) {
      for (let index = 0; index < this.data.length; index++) {
        if (oldData.userAnswers.some(x => x.answerId == this.data[index].content.id)) {
          let oldAnswer = oldData.userAnswers.find(x => x.answerId == this.data[index].content.id)
          this.data[index].order = oldAnswer.answer;
        }
      }
    }
  }
  ChoiceAnswer(event, i) {
    //cập nhật lại vị trí khi thay đổi câu hỏi
    this.data.map((ch, i) => {
      if (ch.order == parseInt(event.value)) {
        this.data[i].order = 0;
      }
    });
    this.data[i].order = parseInt(event.value);
    //thêm vào danh sách đáp án
    if (this.AnswerOfUserTemp.some(x => x.answer == event.value) == true) {
      //gỡ cái trùng
      const index = this.AnswerOfUserTemp.indexOf(this.AnswerOfUserTemp.find(x => x.answer == event.value));
      this.AnswerOfUserTemp.splice(index, 1);
      //cập nhật lại mới
      if (this.AnswerOfUserTemp.some(x => x.answerId == this.answers.false[i].id) != true) {
        this.AnswerOfUserTemp.push({ answerId: this.answers.false[i].id, answer: event.value });
      } else {
        const index = this.AnswerOfUserTemp.indexOf(this.AnswerOfUserTemp.find(x => x.answerId == this.answers.false[i].id));
        this.AnswerOfUserTemp.splice(index, 1);
        this.AnswerOfUserTemp.push({ answerId: this.answers.false[i].id, answer: event.value });
      }
    }
    else {
      this.AnswerOfUserTemp.push({ answerId: this.answers.false[i].id, answer: event.value });
    }

    this.userExamServices.onChangeUserExercise(this.dataQuestion.id, this.AnswerOfUserTemp);
  }
}

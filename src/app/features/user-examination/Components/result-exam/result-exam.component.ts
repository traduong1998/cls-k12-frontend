import { Router, ActivatedRoute } from '@angular/router';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-result-exam',
  templateUrl: './result-exam.component.html',
  styleUrls: ['./result-exam.component.scss']
})
export class ResultExamComponent implements OnInit {

  mark: number = 0;
  markDoExam: number = 0;
  examSubjectStudentId: number;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private router: Router) {
  }

  ngOnInit(): void {
    
  }
  showResult(): void {
    this.router.navigateByUrl(`dashboard/user-examination/${this.data}/result`);
  }
}

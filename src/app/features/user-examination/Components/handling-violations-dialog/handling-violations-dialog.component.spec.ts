import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HandlingViolationsDialogComponent } from './handling-violations-dialog.component';

describe('HandlingViolationsDialogComponent', () => {
  let component: HandlingViolationsDialogComponent;
  let fixture: ComponentFixture<HandlingViolationsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HandlingViolationsDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HandlingViolationsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

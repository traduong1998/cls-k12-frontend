import { _isNumberValue } from '@angular/cdk/coercion';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ViolationTypes } from 'cls-k12-sdk-js/src';

@Component({
  selector: 'app-handling-violations-dialog',
  templateUrl: './handling-violations-dialog.component.html',
  styleUrls: ['./handling-violations-dialog.component.scss']
})
export class HandlingViolationsDialogComponent implements OnInit {
  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT
  //#endregion

  //#region PRIVATE PROPERTY
  //#endregion

  //#region PUBLIC PROPERTY
  ViolationTypes = ViolationTypes;

  minisScoreValidationDefault = []
  public formGroup = this.fb.group({
    id: [''],
    studentName: new FormControl('', Validators.required),
    violationType: new FormControl('', Validators.required),
    minusScore: new FormControl(0, this.minisScoreValidationDefault),
    reason: new FormControl('', Validators.required)
  })

  public student: any;
  public isProgressSubmit: boolean = false;
  public isHidenMinusScore = false;
  get id() { return this.formGroup.get('id'); }
  get studentName() { return this.formGroup.get('studentName'); }
  get minusScore() { return this.formGroup.get('minusScore'); }
  get violationType() { return this.formGroup.get('violationType'); }
  get reason() { return this.formGroup.get('reason'); }

  //#endregion

  //#region CONSTRUCTOR
  constructor(
    public dialogRef: MatDialogRef<HandlingViolationsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public fb: FormBuilder) {
    dialogRef.disableClose = true;
    this.student = data;
    console.log(data)
    console.log(data)
    console.log(data)
  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {
    this.formGroup.controls['id'].setValue(this.student.userId);
    this.formGroup.controls['studentName'].setValue(this.student.fullName);

    this.violationType.valueChanges.subscribe((val) => {
      console.log('change', val);
      if (val == ViolationTypes.SubtractScore) {
        this.minusScore.setValidators([Validators.required, Validators.min(0.1), Validators.max(10)]);
      }
      else {
        this.minusScore.setValidators(this.minisScoreValidationDefault);
      }

      this.minusScore.updateValueAndValidity();
    });
  }

  ngAfterViewInit() {

  }
  //#endregion

  //#region PUBLIC METHOD

  public onNoClick(): void {
    this.dialogRef.close();
  }

  public onViolationTypeHandleChange(val: any) {
    console.log(val);
    if (val == ViolationTypes.Suspend || val == ViolationTypes.Warning) {
      this.isHidenMinusScore = true;
    } else {
      this.isHidenMinusScore = false;
    }
  }

  onSubmit() {
    // this.reason.markAsTouched();
    // this.reason.markAsDirty();
    // this.reason.markAsDirty();
    // if (!this.formGroup.controls['reason'].valid) {
    //   return;
    // }
    if (this.formGroup.invalid) {
      return;
    }
    // if (this.minusScore.value && !_isNumberValue(this.minusScore.value)) {
    //   alert('Điểm trừ bạn vừa nhập không phải là số');
    //   return;
    // }

    this.dialogRef.close({
      id: this.id.value,
      violationType: this.violationType.value,
      reason: this.reason.value,
      minusScore: this.minusScore.value ? parseFloat(this.minusScore.value) : null
    });
  }
  //#endregion

  //#region PRIVATE METHOD
  //#endregion

}

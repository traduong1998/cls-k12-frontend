import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissingWorld2Component } from './missing-world2.component';

describe('MissingWorld2Component', () => {
  let component: MissingWorld2Component;
  let fixture: ComponentFixture<MissingWorld2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MissingWorld2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MissingWorld2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

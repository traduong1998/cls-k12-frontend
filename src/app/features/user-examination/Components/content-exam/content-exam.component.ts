import { takeUntil } from 'rxjs/operators';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Subject, Subscription } from 'rxjs';
import { Component, HostListener, Input, NgZone, OnInit, ViewEncapsulation, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { UserExercise } from 'src/app/shared/modules/userexam/UserAnswer';
import { UserExaminationService } from '../../Services/user-examination.service';
import { ExamRuleDialogComponent } from '../exam-rule-dialog/exam-rule-dialog.component';
import { NotficationComponent } from '../notfication/notfication.component';
import { UserExaminationEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-examination/endpoint/user-examination.enpoint';
import { DoExamHub } from 'sdk/cls-k12-sdk-js/src';
import { ResultExamComponent } from '../result-exam/result-exam.component';
import { ViolationStudentTestDialogComponent } from '../violation-student-test-dialog/violation-student-test-dialog.component';
import { ViolationTypes } from 'sdk/cls-k12-sdk-js/src/services/exam-signal/enums/violation-types';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'src/app/shared/services/message.service';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';

@Component({
  selector: 'app-content-exam',
  templateUrl: './content-exam.component.html',
  styleUrls: ['./content-exam.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContentExamComponent implements OnInit, OnChanges, OnDestroy {
  @Input() examSubjectStudentId: number;
  @Input() testId: number;
  @Input() testKitId: number;
  @Input() itemsPerPage = 5;
  userExercise: UserExercise;
  questions: QuestionTest[] = [];
  currentPage: number = 1;
  screenWidth;
  //isLoading = false;
  isStartDoExam = false;
  timeCountDown: Date;
  timeLeft: number;
  totalPage: number;
  isReady: boolean = false;
  userExamEndpoint: UserExaminationEndpoint;
  subcriberUserAnswersInsert: Subscription;
  subcriberUserAnswersUpdate: Subscription;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  destroy$: Subject<any> = new Subject();
  isSingleClickFinish: boolean = true;
  isDisableFinishButton: boolean = false;
  isFullScreen = true
  private intervalWritelog;
  private userAnswersInsert: UserExercise[];
  private userAnswersUpdate: UserExercise[];
  private isNeedUpdateBeforeDestroy: boolean = true;
  private doExamHub: DoExamHub;
  get questionType(): typeof Type {
    return Type;
  }
  get questionFormat(): typeof FormatQuestion {
    return FormatQuestion;
  }
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenWidth = window.innerWidth;
  }

  @HostListener('window:unload', ['$event'])
  unloadHandler(event: Event) {
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event: Event) {
    //destroy các subcription dữ liệu
    this.destroy$.next();
    //clear interval 
    if (this.intervalWritelog) {
      clearInterval(this.intervalWritelog);
    }
    //Update bài thi trước khi reload/close tab
    this.userExamServices.updateUserExercise(false, this.examSubjectStudentId).then((res) => {
      if (res) {
        localStorage.setItem('updateBeforeUnloadHander', "true," + this.examSubjectStudentId.toString() + "," + new Date().toLocaleDateString());
      }
      else {
        localStorage.setItem('updateBeforeUnloadHander', "false," + this.examSubjectStudentId.toString() + "," + new Date().toLocaleDateString());
      }
    }).catch(() => {
      localStorage.setItem('updateBeforeUnloadHander', "error," + this.examSubjectStudentId.toString() + "," + new Date().toLocaleDateString());
    });
    //Ngắt kết nối signal
    this.doExamHub.dispose();
    this.doExamHub = null;
  }

  @HostListener('document:mouseleave', ['$event'])
  onMouseLeave(event: MouseEvent) {
    console.log("out");
    console.log(event);
  }
  
  constructor(private zone: NgZone, private userExamServices: UserExaminationService, public dialog: MatDialog, private router: Router, private _messageService: MessageService, private _snackBar: MatSnackBar, private spinner: NgxSpinnerService) {
    this.onResize();
    this.currentPage = 1;
    this.userExamEndpoint = new UserExaminationEndpoint();
    this.doExamHub = new DoExamHub();
    console.warn(`constructor content`);
  }
  ngOnChanges(changes: SimpleChanges): void {
    if ('examSubjectStudentId' in changes) {
      this.spinner.show();
      //Clear interval ghi log bài làm
      if (this.intervalWritelog) {
        clearInterval(this.intervalWritelog);
      }
      this.intervalWritelog = undefined;
      //Subcribe các dữ liệu từ service để xử lý bài làm 
      this.userExamServices.userUpdateAnswer$.pipe(takeUntil(this.destroy$)).subscribe((userExercises) => {
        this.userAnswersUpdate = userExercises;
      });
      this.userExamServices.userAnswer$.pipe(takeUntil(this.destroy$)).subscribe((userExercises) => {
        this.userAnswersInsert = userExercises;
      });
      //Dữ liệu đã khởi tạo xong thì bắt đầu connect signalR, khởi tạo 1 số dữ liệu và set interval để ghi log bài làm 
      this.userExamServices.isReady.pipe(takeUntil(this.destroy$)).subscribe((isReady) => {
        if (isReady) {
          //subscribe này chạy 2 lần nên cần kiểm tra trạng thái
          //signalR.HubConnectionState: {Disconnected: "Disconnected", Connecting: "Connecting", Connected: "Connected", Disconnecting: "Disconnecting", Reconnecting: "Reconnecting"}
          if (this.doExamHub.connection && (this.doExamHub.connection.state == "Disconnected" || this.doExamHub.connection.state == "Disconnecting")) {
            this.doExamHub.start()
              .then(() => {
                this.doExamHub.joinRoom({ examId: this.userExamServices.infoExam.examId, subjectId: this.userExamServices.infoExam.subjectId, shiftId: this.userExamServices.infoExam.shiftId, device: undefined })
              });
          }
          else {
            console.warn(`cẩn thận, connection state không cần thiết start hub lần nữa`);
          }
          this.listenEventFromConnection(this.doExamHub.connection);
          this.StartDoExam();
          //Thời gian còn lại của bài làm
          this.timeLeft = this.userExamServices.infoExam.timeRemaining;
          //Tính toán cho phần phân trang câu hỏi
          this.userExamServices.Questions$.subscribe(data => {
            this.questions = data;
            if (this.questions.length <= this.itemsPerPage) {
              this.totalPage = 1;
            }
            else {
              this.totalPage = Math.floor(this.questions.length / this.itemsPerPage);
              if (this.questions.length % this.itemsPerPage > 0) {
                this.totalPage++;
              };
            }
            this.isReady = true;
            this.spinner.hide();
          });
          //Set interval ghi log bài làm cho học sinh
          this.intervalWritelog = setInterval(() => {
            this.updateTest(this.examSubjectStudentId);
          }, UserExaminationService.timeToWriteLog * 60 * 1000);
        }
      })
    }
  }

  ngOnDestroy(): void {
    //Dispose kết nối tới singalR
    this.doExamHub.dispose();
    this.doExamHub = null;
    //Clear interval ghi log
    if (this.intervalWritelog) {
      clearInterval(this.intervalWritelog);
    }
    //Distroy các subcription dữ liệu
    this.destroy$.next();
    //Nếu cần update bài làm trước khi rời khỏi thì update
    if (this.isNeedUpdateBeforeDestroy) {
      this.userExamServices.updateUserExercise(false, this.examSubjectStudentId).then((res) => {
        if (res) {
          //Set localstorage examSubjectStudentId + thời gian update bài làm để dễ debug
          localStorage.setItem('updateWhenDestroy', "true" + this.examSubjectStudentId.toString() + "," + new Date().toLocaleDateString());
        }
      });
    }
  }

  ngOnInit(): void { }

  /**Lắng nghe các sự kiện từ signalR (đình chỉ thi, cảnh cáo, trừ điểm) */
  private listenEventFromConnection(connection) {
    connection.on("Violated", (violatedCode: string, creatorId: number, reason: string, minusScore: number) => {
      let dialogRef = this.dialog.open(ViolationStudentTestDialogComponent, { data: { violatedCode: violatedCode, creatorId: creatorId, reason: reason, minusScore: minusScore } });
      dialogRef.afterClosed().subscribe(() => {
        //ĐÌnh chỉ thi thì nộp bài cho thí sinh ngay và luôn
        if (violatedCode == ViolationTypes.Suspend) {
          this.isNeedUpdateBeforeDestroy = false;
          //Nộp bài
          this.userExamServices.submitUserExercise(false, this.examSubjectStudentId).then((res) => {
            if (res) {
              this.isNeedUpdateBeforeDestroy = false;
              clearInterval(this.intervalWritelog);
              let dialogShowResult = this.dialog.open(ResultExamComponent);
              dialogShowResult.afterClosed().subscribe((isShowResult) => {
                if (isShowResult) {
                  this.router.navigate([`learner/user-examination/${this.examSubjectStudentId}/result`])
                }
                else {
                  this.router.navigate([`learner/user-examination`])
                }
              });
              try {
                this.doExamHub.submit();
              }
              catch (err) { console.error(`không submit được`); }
            } else {
              clearInterval(this.intervalWritelog);
              this.router.navigate(['learner/user-examination']);
            }
          })
            .catch((ERR) => {
              console.error(ERR);
              clearInterval(this.intervalWritelog);
              this.router.navigate(['learner/user-examination']);
            });
        }
      });
      console.log(violatedCode, creatorId, reason);
    });
    connection.onreconnecting(error => {

    });

    connection.onreconnected(connectionId => {
    });

    connection.onclose(error => {
    });

    connection.on("collectedTest", (data) => {
      console.log(`collectedTest`, data);
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Bị bắt buộc thu bài",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });

      //thu bài
      this.userExamServices.submitUserExercise(false, this.examSubjectStudentId).then((res) => {
        if (res) {
          this.isNeedUpdateBeforeDestroy = false;
          clearInterval(this.intervalWritelog);
          let dialogShowResult = this.dialog.open(ResultExamComponent);
          dialogShowResult.afterClosed().subscribe((isShowResult) => {
            if (isShowResult) {
              this.router.navigate([`learner/user-examination/${this.examSubjectStudentId}/result`])
            }
            else {
              this.router.navigate([`learner/user-examination`])
            }
          });
          try {
            this.doExamHub.submit();
          }
          catch (err) { console.error(`không submit được`); }
        } else {
          clearInterval(this.intervalWritelog);
          this.router.navigate(['learner/user-examination']);
        }
      })
        .catch((ERR) => {
          console.error(ERR);
          clearInterval(this.intervalWritelog);
          this.router.navigate(['learner/user-examination']);
        });
    });
  }

  /**Hàm update bài làm, sẽ được gọi tự động sau mỗi khoảng thời gian ghi log cố định (UserExaminationService.timeToWriteLog) */
  updateTest(examSubjectStudentId: number) {
    //console.log('gửi dữ liệu lên server', new Date().toLocaleString());
    this.userExamServices.updateUserExercise(false, examSubjectStudentId).then((isUpdated) => {
      if (!isUpdated) {
        console.log("Có lỗi xảy ra");
      }
      else {
        //Update thành công thì clear danh sách các mảng bài làm cần gửi lên
        this.userExamServices.clearUpdateAnswerOfUser();
        //console.log('đã clear danh sách câu hỏi được cập nhật');
      }
    });
  }
  StartDoExam = () => {
    this.isStartDoExam = true;
    console.log(this.isStartDoExam);
  }

  pagingQuestions() {
    let currentItem = (this.currentPage - 1) * this.itemsPerPage
    return this.questions.splice(currentItem, currentItem + this.itemsPerPage - 1);
  }

  //chuyển trang khi bấm vào câu hỏi số ??? và scroll tới đó
  scrollToElement(index): void {
    //khi ở man hình nhỏ
    //tìm trang hiện tại của index
    var currentPageOfIndex = Math.floor((index + (this.itemsPerPage - 1)) / this.itemsPerPage);
    // nếu khác trang thì sẽ chuyển trang
    if (this.currentPage != currentPageOfIndex) {
      this.currentPage = currentPageOfIndex;
      setTimeout(() => {
        var element = document.getElementById("cauhoi_" + index);
        var elementPosition = element.offsetTop;
        element.scrollIntoView({ behavior: "smooth" });
      }, 300);
    } else {
      var element = document.getElementById("cauhoi_" + index);
      var elementPosition = element.offsetTop;
      element.scrollIntoView({ behavior: "smooth" });
    }
  }
  //chuyển lên đầu trang khi chuyển trang
  pageChanged(event): void {
    this.currentPage = event;
    let firstQuestionOfPage = ((this.currentPage - 1) * this.itemsPerPage) + 1;
    setTimeout(() => {
      var element = document.getElementById("cauhoi_" + firstQuestionOfPage);
      var elementPosition = element.offsetTop;
      element.scrollIntoView({ behavior: "smooth" });
    }, 100);
  }

  /**Hàm nộp bài thi, được gọi khi click vào button nộp bài*/
  finishExam() {
    this.isSingleClickFinish = true;
    setTimeout(() => {
      //Nếu là single click thì mới xử lý nộp bài, tránh gọi 1 lúc 2 lần sẽ bị double 
      if (this.isSingleClickFinish) {
        this.isDisableFinishButton = true;
        let dialogRef = this.dialog.open(NotficationComponent, { panelClass: 'notification-dialog', data: { dataAnswer: this.userExamServices.getUserAnswer(), dataQuestion: this.questions } });
        dialogRef.afterClosed().subscribe((isSubmit) => {
          if (isSubmit == true) {
            this.isFullScreen = true;
            this.spinner.show();
            //Gọi api nộp bài
            this.userExamServices.submitUserExercise(false, this.examSubjectStudentId).then((res) => {
              if (res) {
                //Gán isNeedUpdateBeforeDestroy = false để nếu tắt hoặc thoát trình duyệt sẽ không gọi update bài làm nữa
                this.isNeedUpdateBeforeDestroy = false;
                this.spinner.hide();
                //Clear interval ghi log bài làm
                clearInterval(this.intervalWritelog);
                //Mở dialog xem kết quả bài làm
                let dialogShowResult = this.dialog.open(ResultExamComponent);
                dialogShowResult.afterClosed().subscribe((isShowResult) => {
                  if (isShowResult) {
                    this.router.navigate([`learner/user-examination/${this.examSubjectStudentId}/result`])
                  }
                  else {
                    this.router.navigate([`learner/user-examination`])
                  }
                });
                try {
                  this.doExamHub.submit();
                }
                catch (err) { console.error(`không submit được`); }
              } else {
                this.spinner.hide();
                this._messageService.failureCallback("Nộp bài không thành công");
              }
            })
              .catch((ERR) => {
                this.spinner.hide();
                this._messageService.failureCallback("Nộp bài không thành công");
              });
          }
          else{
            this.isDisableFinishButton = false;
          }
        })
      }
    }, 250);
  }

  //Xử lý khi double click nộp bài
  onDoubleClickFinish(): void {
    this.isSingleClickFinish = false;
  }
  onNextPage(): void {
    if (this.currentPage < this.totalPage) {
      this.currentPage++;
      this.pageChanged(this.currentPage);
    }
    // this.currentPage<this.totalPage??this.currentPage++
  }
  onBackPage(): void {
    if (this.currentPage > 0) {
      this.currentPage--;
      this.pageChanged(this.currentPage);
    }
  }
  openExamRuleDialog(): void {
    let dialogRef = this.dialog.open(ExamRuleDialogComponent, { panelClass: 'exam-rule-dialog', data: this.userExamServices.infoExam });
  }
  SortedQuestions() {
    return this.questions.sort((a, b) => { return a.order - b.order });
  }

}

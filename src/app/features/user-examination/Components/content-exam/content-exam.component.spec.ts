import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentExamComponent } from './content-exam.component';

describe('ContentExamComponent', () => {
  let component: ContentExamComponent;
  let fixture: ComponentFixture<ContentExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

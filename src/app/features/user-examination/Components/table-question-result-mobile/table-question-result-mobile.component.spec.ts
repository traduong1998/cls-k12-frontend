import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableQuestionResultMobileComponent } from './table-question-result-mobile.component';

describe('TableQuestionResultMobileComponent', () => {
  let component: TableQuestionResultMobileComponent;
  let fixture: ComponentFixture<TableQuestionResultMobileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableQuestionResultMobileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableQuestionResultMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

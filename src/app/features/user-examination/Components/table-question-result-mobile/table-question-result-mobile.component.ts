import { OnChanges, SimpleChanges } from '@angular/core';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { ResultDoExamResponse } from 'sdk/cls-k12-sdk-js/src/services/user-examination/endpoint/responses/result-do-exam-response';
import { UserExaminationService } from '../../Services/user-examination.service';

@Component({
  selector: 'app-table-question-result-mobile',
  templateUrl: './table-question-result-mobile.component.html',
  styleUrls: ['./table-question-result-mobile.component.scss']
})
export class TableQuestionResultMobileComponent implements OnInit, OnChanges {

  @Input() dataQuestion;
  @Input() dataQuestionBase: QuestionTest[];
  @Input() totalPage;
  @Input() currentPage;
  @Input() screenWidth;
  @Input() examSubjectStudentId: number;
  @Input() resultDoExam: ResultDoExamResponse;
  @Output() indexQuestion = new EventEmitter();
  @Output() pageIndex = new EventEmitter();
  isShow = false;
  questions: QuestionTest[] = [];
  totalScore: number = 0;
  infoExam: any;

  isReady: boolean = false;
  constructor(private userExamSerivce: UserExaminationService, private dialog: MatDialog, private _router: Router) {
    this.userExamSerivce.fetchFromLocalStorage();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.totalScore = this.getTotalScore(this.dataQuestionBase);
    this.infoExam = this.userExamSerivce.infoExam;
    this.resultDoExam.realScore = Math.round(this.resultDoExam.realScore * 100)/100;
    this.isReady = true;
  }

  ngOnInit(): void {
    this.userExamSerivce.getQuestions().subscribe(data => {
      this.questions = data;
    });
  }

  ngOnDestroy(): void {
  }
  show() {
    this.isShow = !this.isShow;
  }
  getResult(questionId: number): boolean {
    return this.userExamSerivce.isCorrectQuestion(questionId, this.dataQuestionBase);
  }
  SendIndexQuestion(id) {
    this.indexQuestion.emit(id);
    this.show();
  }

  NextPage() {
    if (this.currentPage < this.dataQuestion.length) {
      this.currentPage = this.currentPage + 1;
    }
    else {
      this.currentPage = this.dataQuestion.length
    }
    this.indexQuestion.emit(this.currentPage);
    //console.log("ssss",this.dataQuestion.length);
  }
  PreviousPage() {
    if (this.currentPage <= 1) {
      this.currentPage = 1;
    } else {
      this.currentPage = this.currentPage - 1;
    }

    this.indexQuestion.emit(this.currentPage);
    console.log("ssss", this.currentPage);
  }

  exitButtonHandleClick(): void {
    this._router.navigate([`learner/user-examination`]);
  }

  getTotalScore(questions: QuestionTest[]) {
    let totalScore = 0;
    questions.forEach((question) => {
      if (question.format == FormatQuestion.single) totalScore += question.score;
      else {
        question.questions.forEach((child) => {
          totalScore += child.score;
        })
      }
    })
    return Math.round(totalScore * 100) / 100;
  }
}

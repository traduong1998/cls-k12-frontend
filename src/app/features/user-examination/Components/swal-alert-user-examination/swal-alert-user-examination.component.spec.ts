import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwalAlertUserExaminationComponent } from './swal-alert-user-examination.component';

describe('SwalAlertUserExaminationComponent', () => {
  let component: SwalAlertUserExaminationComponent;
  let fixture: ComponentFixture<SwalAlertUserExaminationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwalAlertUserExaminationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwalAlertUserExaminationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

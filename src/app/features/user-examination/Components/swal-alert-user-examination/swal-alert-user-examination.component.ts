import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from 'src/app/features/learn/intefaces/dialogData';

@Component({
  selector: 'app-swal-alert-user-examination',
  templateUrl: './swal-alert-user-examination.component.html',
  styleUrls: ['./swal-alert-user-examination.component.scss']
})
export class SwalAlertUserExaminationComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<SwalAlertUserExaminationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }


  ngOnInit(): void {

  }
  
  onNoClick(): void {
    this.dialogRef.close();
  }

}

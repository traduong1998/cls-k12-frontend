import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserExamInforComponent } from './user-exam-infor.component';

describe('UserExamInforComponent', () => {
  let component: UserExamInforComponent;
  let fixture: ComponentFixture<UserExamInforComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserExamInforComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserExamInforComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { MatDialog,MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HandlingViolationsComponent } from '../handling-violations/handling-violations.component';
import {InforUserDetailComponent} from '../infor-user-detail/infor-user-detail.component'

@Component({
  selector: 'app-user-exam-infor',
  templateUrl: './user-exam-infor.component.html',
  styleUrls: ['./user-exam-infor.component.scss']
})
export class UserExamInforComponent implements OnInit {
  @Input() userExam;
  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
    console.log("details",this.userExam)
  }
  ShowDetail(userExam){
    this.dialog.open(InforUserDetailComponent, {panelClass: 'my-full-screen-dialog',data:userExam});
  }

  HandlingViolations(){
    this.dialog.open(HandlingViolationsComponent, {panelClass: 'callsupport-dialog'});
  }
}

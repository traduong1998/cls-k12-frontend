import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableQuestionMoblieComponent } from './table-question-moblie.component';

describe('TableQuestionMoblieComponent', () => {
  let component: TableQuestionMoblieComponent;
  let fixture: ComponentFixture<TableQuestionMoblieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableQuestionMoblieComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableQuestionMoblieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

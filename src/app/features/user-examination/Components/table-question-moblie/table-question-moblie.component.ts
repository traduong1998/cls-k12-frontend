import { Component, Input, OnInit, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src';
import { countdown, countdownRealTime } from 'src/app/shared/helpers/cls.helper';
import { ExamSubjectStudentInfo } from '../../Interfaces/exam-subject-student-infor';
import { UserExaminationService } from '../../Services/user-examination.service';
import { CallSupportComponent } from '../call-support/call-support.component';
import { EndTimeDialogComponent } from '../end-time-dialog/end-time-dialog.component';
import { NotficationComponent } from '../notfication/notfication.component';
import { ResultExamComponent } from '../result-exam/result-exam.component';

@Component({
  selector: 'app-table-question-moblie',
  templateUrl: './table-question-moblie.component.html',
  styleUrls: ['./table-question-moblie.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TableQuestionMoblieComponent implements OnInit {
  @Input() dataQuestion;
  @Input() timeLeft;
  @Input() totalPage;
  @Input() screenWidth;
  @Input() isShowResult = false;
  @Input() examSubjectStudentId: number;
  @Output() indexQuestion = new EventEmitter();
  @Output() pageIndex = new EventEmitter();
  infoExam: ExamSubjectStudentInfo;
  isShow = false;
  destroy$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  questions: QuestionTest[] = [];
  @Input() currentPage = 1;

  constructor(private userExamSerivce: UserExaminationService, private dialog: MatDialog, private router: Router, private _snackBar: MatSnackBar) {
    this.userExamSerivce.fetchFromLocalStorage();
  }

  ngOnInit(): void {
    this.infoExam = this.userExamSerivce.infoExam;
    this.userExamSerivce.getQuestions().subscribe(data => {
      this.questions = data;
    });
    if (this.timeLeft > 0 && this.screenWidth <= 960) {
      this.destroy$.next(false);
      countdownRealTime(this.timeLeft * 1000, this.EndTime, "countdowntimer-mobile", this.destroy$);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
  }
  EndTime = () => {
    this.userExamSerivce.submitUserExercise(false, this.examSubjectStudentId).then((res) => {
      if (res) {
        this._snackBar.open("Nộp bài thành công!", 'Ok', {
          duration: 3000,
          panelClass: ['green-snackbar'],
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        this.destroy$.next(true);
        let dialogShowResult = this.dialog.open(ResultExamComponent);
        dialogShowResult.afterClosed().subscribe((isShowResult) => {
          if (isShowResult) {
            this.router.navigate([`learner/user-examination/${this.examSubjectStudentId}/result`])
          }
          else{
            this.router.navigate([`learner/user-examination`])
          }
        })
      } else {
        this._snackBar.open("Có lỗi xảy ra, vui lòng thử lại!", 'ERR', {
          duration: 3000,
          panelClass: ['red-snackbar'],
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    })
      .catch((ERR) => {
        console.error(ERR);
        this._snackBar.open("Có lỗi xảy ra, vui lòng thử lại!", 'ERR', {
          duration: 3000,
          panelClass: ['red-snackbar'],
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
  }
  show() {
    this.isShow = !this.isShow;
  }
  getType(id): number {
    let answer = this.userExamSerivce.getAnswerOfQuestion(id);
    if (answer == undefined) {
      return 0
    } else if (answer.isAnswered == false && answer.isMark == true) {
      return 1
    }
    else if (answer.isAnswered == true && answer.isMark == false) {
      return 2
    }
    else if (answer.isAnswered == false && answer.isMark == false) {
      return 0
    }
    else {
      return 3
    }
  }
  SendIndexQuestion(id) {
    this.indexQuestion.emit(id);
    this.show();
  }

  NextPage() {
    if (this.currentPage < this.dataQuestion.length) {
      this.currentPage = this.currentPage + 1;
    }
    else {
      this.currentPage = this.dataQuestion.length
    }
    this.pageIndex.emit(this.currentPage);
    //console.log("ssss",this.dataQuestion.length);
  }
  PreviousPage() {
    if (this.currentPage <= 1) {
      this.currentPage = 1;
    } else {
      this.currentPage = this.currentPage - 1;
    }

    this.pageIndex.emit(this.currentPage);
    console.log("ssss", this.currentPage);
  }

  callForSupport() {
    this.dialog.open(CallSupportComponent);
  }

  finishExam() {
    this.dialog.open(NotficationComponent);
    console.log('nộp bài', this.userExamSerivce.getUserAnswer());
    this.userExamSerivce.endExam();
  }

  TotalMark() {
    return Math.round(this.questions.reduce(function (prev, cur) {
      return prev + cur.score;
    }, 0) * 100)/100 ;
  }
}

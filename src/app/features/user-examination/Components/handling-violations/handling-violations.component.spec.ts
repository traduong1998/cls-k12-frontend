import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HandlingViolationsComponent } from './handling-violations.component';

describe('HandlingViolationsComponent', () => {
  let component: HandlingViolationsComponent;
  let fixture: ComponentFixture<HandlingViolationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HandlingViolationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HandlingViolationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

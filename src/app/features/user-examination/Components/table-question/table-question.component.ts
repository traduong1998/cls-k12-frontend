import { DOCUMENT } from '@angular/common';
import { Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { UserExaminationService } from '../../Services/user-examination.service';
import { countdown, countdownRealTime } from 'src/app/shared/helpers/cls.helper';
import { MatDialog } from '@angular/material/dialog';
import { CallSupportComponent } from '../call-support/call-support.component';
import { NotficationComponent } from '../notfication/notfication.component';
import { EndTimeDialogComponent } from '../end-time-dialog/end-time-dialog.component';
import { Router } from '@angular/router';
import { QuestionTest } from 'src/app/features/learn/intefaces/questionTest';
import { BehaviorSubject } from 'rxjs';
import { ExamRuleDialogComponent } from '../exam-rule-dialog/exam-rule-dialog.component';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ResultExamComponent } from '../result-exam/result-exam.component';
import { ExamSubjectStudentInfo } from '../../Interfaces/exam-subject-student-infor';
@Component({
  selector: 'app-table-question',
  templateUrl: './table-question.component.html',
  styleUrls: ['./table-question.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TableQuestionComponent implements OnInit, OnDestroy {
  //Danh sách câu hỏi
  @Input() dataQuestion;
  @Input() screenWidth;
  @Input() examSubjectStudentId: number;
  //@Input() isLoading;
  @Input() timeLeft;
  @Output() indexQuestion = new EventEmitter();
  //câu hỏi đã trả lời
  questionAnswered = [];
  elem: any;
  isFull = false;
  isSubmitSuccess = true;
  questions: QuestionTest[] = [];
  funcTimeout;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  destroy$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  infoExam: ExamSubjectStudentInfo;
  constructor(@Inject(DOCUMENT) private document: any, public dialog: MatDialog, private router: Router, private userExamServices: UserExaminationService, private _snackBar: MatSnackBar) {
    this.userExamServices.fetchFromLocalStorage();
  }
  ngOnDestroy(): void {
    clearTimeout(this.funcTimeout);
    this.destroy$.next(true);
  }
  ngOnInit(): void {
    this.elem = document.documentElement;
    this.userExamServices.getQuestions().subscribe(data => {
      this.questions = data;
    });
    //Nếu còn thời gian thì thực hiện đếm giừo
    if (this.timeLeft > 0 && this.screenWidth > 960) {
      this.destroy$.next(false);
      document.getElementById("countdownExam").style.display = "block";
      countdownRealTime(this.timeLeft * 1000, this.EndTime, "countdowntimer", this.destroy$);
    };
    this.infoExam = this.userExamServices.infoExam;
  }

  /**Hàm nộp bài, được gọi khi kết thúc đếm ngược */
  EndTime = () => {
    this.userExamServices.submitUserExercise(false, this.examSubjectStudentId).then((res) => {
      if (res) {
        this._snackBar.open("Nộp bài thành công!", 'Ok', {
          duration: 3000,
          panelClass: ['green-snackbar'],
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        this.destroy$.next(true);
        let dialogShowResult = this.dialog.open(ResultExamComponent);
        dialogShowResult.afterClosed().subscribe((isShowResult) => {
          if (isShowResult) {
            this.router.navigate([`learner/user-examination/${this.examSubjectStudentId}/result`])
          }
          else{
            this.router.navigate([`learner/user-examination`])
          }
        })
      } else {
        this._snackBar.open("Có lỗi xảy ra, vui lòng thử lại!", 'ERR', {
          duration: 3000,
          panelClass: ['red-snackbar'],
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    })
      .catch((ERR) => {
        console.error(ERR);
        this._snackBar.open("Có lỗi xảy ra, vui lòng thử lại!", 'ERR', {
          duration: 3000,
          panelClass: ['red-snackbar'],
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
  }

  TotalMark() {
    return Math.round(this.questions.reduce(function (prev, cur) {
      return prev + cur.score;
    }, 0) * 100)/100 ;
  }
  openFullscreen(): void {
    this.isFull = !this.isFull;
    if (this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
    } else if (this.elem.mozRequestFullScreen) {
      /* Firefox */
      this.elem.mozRequestFullScreen();
    } else if (this.elem.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      this.elem.webkitRequestFullscreen();
    } else if (this.elem.msRequestFullscreen) {
      /* IE/Edge */
      this.elem.msRequestFullscreen();
    }
  }
  /* Close fullscreen */
  closeFullscreen(): void {
    this.isFull = !this.isFull;
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
    }
  }

  finishExam() {
    this.dialog.open(NotficationComponent, { panelClass: 'notification-dialog', data: { dataAnswer: this.userExamServices.getUserAnswer(), dataQuestion: this.dataQuestion } });
    //console.log('nộp bài', this.userExamSerivce.getUserAnswer());
    //this.userExamSerivce.endExam();
  }
  SendIndexQuestion(id) {
    this.indexQuestion.emit(id);
  }
  getType(id): number {
    let answer = this.userExamServices.getAnswerOfQuestion(id);
    if (answer == undefined) {
      return 0
    } else if (answer.isAnswered == false && answer.isMark == true) {
      return 1
    }
    else if (answer.isAnswered == true && answer.isMark == false) {
      return 2
    }
    else if (answer.isAnswered == false && answer.isMark == false) {
      return 0
    }
    else {
      return 3
    }
  }
  callForSupport() {
    this.dialog.open(CallSupportComponent, { panelClass: 'callsupport-dialog', });
  }
  openExamRuleDialog(): void {
    let dialogRef = this.dialog.open(ExamRuleDialogComponent, { panelClass: 'exam-rule-dialog', data: this.userExamServices.infoExam });

  }
}

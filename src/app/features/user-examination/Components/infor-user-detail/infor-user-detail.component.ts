import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog,MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-infor-user-detail',
  templateUrl: './infor-user-detail.component.html',
  styleUrls: ['./infor-user-detail.component.scss']
})
export class InforUserDetailComponent implements OnInit {

  displayedColumns: string[] = ['infringeName', 'hinhThucXuLy','diemBiTru', 'infringeTime']
  constructor(@Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit(): void {
  }

}

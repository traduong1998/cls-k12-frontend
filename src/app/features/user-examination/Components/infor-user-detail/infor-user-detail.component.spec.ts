import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InforUserDetailComponent } from './infor-user-detail.component';

describe('InforUserDetailComponent', () => {
  let component: InforUserDetailComponent;
  let fixture: ComponentFixture<InforUserDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InforUserDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InforUserDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

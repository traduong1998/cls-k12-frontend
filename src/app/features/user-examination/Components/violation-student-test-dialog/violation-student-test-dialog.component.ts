import { NotifyViolationDialogData } from './../../Interfaces/notify-violation-dialog-data';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ViolationTypes } from 'sdk/cls-k12-sdk-js/src/services/exam-signal/enums/violation-types';

@Component({
  selector: 'app-violation-student-test-dialog',
  templateUrl: './violation-student-test-dialog.component.html',
  styleUrls: ['./violation-student-test-dialog.component.scss']
})
export class ViolationStudentTestDialogComponent implements OnInit {

  public get violationType(): typeof ViolationTypes {
    return ViolationTypes;
  }
  constructor(@Inject(MAT_DIALOG_DATA) public info: NotifyViolationDialogData) { }

  ngOnInit(): void {
  }

}

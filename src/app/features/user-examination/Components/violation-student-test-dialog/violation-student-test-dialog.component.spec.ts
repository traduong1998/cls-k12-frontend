import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViolationStudentTestDialogComponent } from './violation-student-test-dialog.component';

describe('ViolationStudentTestDialogComponent', () => {
  let component: ViolationStudentTestDialogComponent;
  let fixture: ComponentFixture<ViolationStudentTestDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViolationStudentTestDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViolationStudentTestDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

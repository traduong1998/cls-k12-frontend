import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforeDoExamComponent } from './before-do-exam.component';

describe('BeforeDoExamComponent', () => {
  let component: BeforeDoExamComponent;
  let fixture: ComponentFixture<BeforeDoExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforeDoExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforeDoExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

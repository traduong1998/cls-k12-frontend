import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { UserExaminationEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-examination/endpoint/user-examination.enpoint';
import { ExamDetaiInfo } from 'sdk/cls-k12-sdk-js/src/services/user-examination/model/exam-detail-info';
import { InforExamSubjectStudent } from 'sdk/cls-k12-sdk-js/src/services/user-examination/model/infor-exam-subject-student';
import { countdown, countdownRealTime } from 'src/app/shared/helpers/cls.helper';
import { UserExaminationService } from '../../Services/user-examination.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-before-do-exam',
  templateUrl: './before-do-exam.component.html',
  styleUrls: ['./before-do-exam.component.scss']
})
export class BeforeDoExamComponent implements OnInit {
  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT
  userExaminationEndpoint: UserExaminationEndpoint
  //#endregion

  //#region PRIVATE PROPERTY
  /**
   * clear count-down when submit
   */
  private destroy$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private examId: number;
  private subjectId: number;

  /**
   * Time start count down
   */
  private startTime: number = 0;

  private examSubjectStudentId: number;
  private startDate: Date;

  //#endregion

  //#region PUBLIC PROPERTY

  public examSubjectStudent: InforExamSubjectStudent = null;
  public examInfo: ExamDetaiInfo = null;

  public portalName: string = 'Sở giáo dục';
  public examName: string = 'Kỳ thi cuối học kì I';
  public subjectName: string = 'Môn thi toán 12';
  public lockScreenTest: string = '';
  public publicScore: string = '';
  public publicAnswer: string = '';
  public testTime: number = 0;
  public isStartDoExam: boolean = false;
  public isReady: boolean = false;
  //#endregion

  //#region CONSTRUCTOR
  /**
   * constructor
   * @param _router 
   */
  isFullScreen = true;

  @HostListener('document:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    console.log("Click f11 out");
    console.log(event);
    if (event.keyCode == 123 || event.which == 122) { // Prevent F12
      event.preventDefault()
    }
    else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
      event.preventDefault()
    }
  }

  @HostListener('document:contextmenu', ['$event'])
  onRightClick(event: MouseEvent){
    event.preventDefault();
  }
  
  constructor(private _router: Router, private _activeRoute: ActivatedRoute, private userExamService: UserExaminationService,private _messageService :MessageService, private spinner: NgxSpinnerService) {
    this.examSubjectStudentId = +this._activeRoute.snapshot.paramMap.get('id');
    this.userExaminationEndpoint = new UserExaminationEndpoint();
  }

  //#endregion

  //#region LIFECYCLE
  /**
 * ngOnInit
 */
  ngOnInit(): void {

    this.isFullScreen = true;
    this.spinner.show();
    this.destroy$.next(false);
    console.log("this.startDate before", this.startDate);
    this.userExaminationEndpoint.getInforExamSubjectStudentById(this.examSubjectStudentId).then(res => {
      if (res) {
        this.examSubjectStudent = res;
        this.subjectName = res.subjectName;
        this.portalName = res.portalName;
        this.startDate = res.startDate;
        this.examId = res.examId;
        this.subjectId = res.subjectId;

        this.userExaminationEndpoint.getDetailInfoExam(this.examId, this.subjectId, this.examSubjectStudentId).then(res => {
          this.examInfo = res;
          this.examName = res.name;
          this.lockScreenTest = res.isLockTestScreen ? ' Không được phép sử dụng các ứng dụng, chương trình khác' : 'Được phép sử dụng các ứng dụng, chương trình khác';
          this.publicScore = res.isPublicScore ? 'Được phép xem điểm sau khi nộp bài' : ' Không được phép xem điểm sau khi nộp bài ';
          this.publicAnswer = res.isPublicAnswer ? 'Được phép xem kết quả bài làm khi kỳ thi kết thúc' : 'Không được phép xem kết quả bài làm khi kỳ thi kết thúc';
          this.testTime = res.testTime;
          this.isReady = true;
        })

        this.startTime = this.convertCountDown(new Date(res.startDate));
        if (this.startTime < 0) {
          this.endTimeCoundown();
        }
        else {
          countdownRealTime(this.startTime, () => { this.endTimeCoundown() }, 'countdown', this.destroy$);
        }
      } else {
        // TODO : Validation 
        return;
      }
      this.spinner.hide();
    })
    .catch (()=>{
      this.spinner.hide();
      this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại!");
    });
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.startTime = 0;
  }
  //#endregion

  //#region Public PUBLIC METHOD
  /*
   * navigate start exam
   */
  onNavigateHandleClick() {
    this._router.navigateByUrl(`/dashboard/user-examination/${this.examSubjectStudentId}/create`);
  }

  endTimeCoundown(): void {
    this.destroy$.next(true);
    this.isStartDoExam = true;
  }
  onShowResultClick(): void {
    if (this.examInfo.isCompleted && this.examInfo.isPublicAnswer) {
      this._router.navigateByUrl(`/learner/user-examination/${this.examSubjectStudentId}/result`);
    }
  }
  //#endregion

  //#region PRIVATE METHOD

  /**
   * minus date
   * @param date start date exam
   * @returns miliseconds countdown
   */
  private convertCountDown(date: Date): number {
    let now = new Date();
    console.log(date);
    console.log(now);
    let diff = date.getTime() - now.getTime();

    return diff;
  }
  //#endregion
}

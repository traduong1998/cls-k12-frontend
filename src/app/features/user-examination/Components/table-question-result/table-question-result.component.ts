import { ResultDoExamResponse } from './../../../../../../sdk/cls-k12-sdk-js/src/services/user-examination/endpoint/responses/result-do-exam-response';
import { ResultQuestionStudent } from './../../Interfaces/result-question-student';
import { DOCUMENT } from '@angular/common';
import { Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Output, OnChanges, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { QuestionTest } from 'cls-k12-sdk-js/src';
import { UserExaminationService } from '../../Services/user-examination.service';
import { ExamRuleDialogComponent } from '../exam-rule-dialog/exam-rule-dialog.component';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';

@Component({
  selector: 'app-table-question-result',
  templateUrl: './table-question-result.component.html',
  styleUrls: ['./table-question-result.component.scss']
})
export class TableQuestionResultComponent implements OnInit, OnDestroy, OnChanges {
  @Input() dataQuestion: QuestionTest[];
  @Input() dataQuestionBase: QuestionTest[];
  @Input() screenWidth;
  @Input() examSubjectStudentId: number;
  @Input() resultDoExam: ResultDoExamResponse;
  @Output() indexQuestion = new EventEmitter();
  elem: any;
  isFull = false;
  questions: QuestionTest[] = [];
  isReady: boolean = false;
  totalScore: number = 0;
  infoExam: any;
  isExamEnd: boolean;
  constructor(@Inject(DOCUMENT) private document: any, public dialog: MatDialog, private _router: Router, private userExamServices: UserExaminationService) {
    this.userExamServices.fetchFromLocalStorage();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.totalScore = this.getTotalScore(this.dataQuestionBase);
    this.resultDoExam.realScore = Math.round(this.resultDoExam.realScore * 100)/100;
    this.isReady = true;
  }
  ngOnDestroy(): void {
  }
  ngOnInit(): void {
    this.elem = document.documentElement;
    this.userExamServices.getQuestions().subscribe(data => {
      this.questions = data;
    });
    this.infoExam = this.userExamServices.infoExam;
    let now = new Date(); 
    let end = new Date(this.infoExam.endDate);
    this.isExamEnd = ((now.getTime() - end.getTime())> 0);  
  }
  openFullscreen(): void {
    this.isFull = !this.isFull;
    if (this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
    } else if (this.elem.mozRequestFullScreen) {
      /* Firefox */
      this.elem.mozRequestFullScreen();
    } else if (this.elem.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      this.elem.webkitRequestFullscreen();
    } else if (this.elem.msRequestFullscreen) {
      /* IE/Edge */
      this.elem.msRequestFullscreen();
    }
  }
  /* Close fullscreen */
  closeFullscreen(): void {
    this.isFull = !this.isFull;
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
    }
  }
  SendIndexQuestion(id) {
    this.indexQuestion.emit(id);
  }
  openExamRuleDialog(): void {
    let dialogRef = this.dialog.open(ExamRuleDialogComponent, { panelClass: 'exam-rule-dialog', data: this.userExamServices.infoExam });
  }
  getResult(questionId: number): boolean {
    return this.userExamServices.isCorrectQuestion(questionId, this.dataQuestionBase);
  }
  getTotalScore(questions: QuestionTest[]) {
    let totalScore = 0;
    questions.forEach((question) => {
      if (question.format == FormatQuestion.single) totalScore += question.score;
      else {
        question.questions.forEach((child) => {
          totalScore += child.score;
        })
      }
    })
    return Math.round(totalScore * 100) / 100;
  }
  isAnswered(questionId: number): boolean {
    let userExercise = this.resultDoExam.userExercises.find(x => x.questionId == questionId && x.isAnswered);
    return (userExercise != undefined)
  }
}

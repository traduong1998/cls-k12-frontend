import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissingWorld1Component } from './missing-world1.component';

describe('MissingWorld1Component', () => {
  let component: MissingWorld1Component;
  let fixture: ComponentFixture<MissingWorld1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MissingWorld1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MissingWorld1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

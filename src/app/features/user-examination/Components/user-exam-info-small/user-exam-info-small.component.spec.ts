import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserExamInfoSmallComponent } from './user-exam-info-small.component';

describe('UserExamInfoSmallComponent', () => {
  let component: UserExamInfoSmallComponent;
  let fixture: ComponentFixture<UserExamInfoSmallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserExamInfoSmallComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserExamInfoSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

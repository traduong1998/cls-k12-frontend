import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { HandlingViolationsComponent } from '../handling-violations/handling-violations.component';
import { InforUserDetailComponent } from '../infor-user-detail/infor-user-detail.component';

@Component({
  selector: 'app-user-exam-info-small',
  templateUrl: './user-exam-info-small.component.html',
  styleUrls: ['./user-exam-info-small.component.scss']
})
export class UserExamInfoSmallComponent implements OnInit {

  @Input() userExam;
  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
    console.log("details",this.userExam)
  }
  ShowDetail(userExam){
    this.dialog.open(InforUserDetailComponent, {panelClass: 'my-full-screen-dialog',data:userExam});
  }

  HandlingViolations(){
    this.dialog.open(HandlingViolationsComponent, {panelClass: 'callsupport-dialog'});
  }

}

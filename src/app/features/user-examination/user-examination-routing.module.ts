import { Component, NgModule } from '@angular/core';
import { UserExaminationComponent } from './user-examination.component';
import { RouterModule, Routes } from '@angular/router';
import { StartExamComponent } from './Views/start-test/start-exam.component';
import { ExamSupervisionComponent } from './Views/exam-supervision/exam-supervision.component';
import { ShowResultDoExamComponent } from './Components/show-result-do-exam/show-result-do-exam.component'
import { AuthGuard } from 'src/app/core/guards';
import { ListSubjectSupervisionComponent } from './Views/list-subject-supervision/list-subject-supervision.component';

import { BeforeDoExamComponent } from './Components/before-do-exam/before-do-exam.component';
import { ListExamComponent } from './Views/list-exam/list-exam.component';
import { ListSubjectExamComponent } from './Views/list-subject-exam/list-subject-exam.component';
import { ListUnitsExamComponent } from './Views/list-units-exam/list-units-exam.component';
import { CheckScoreDoExamComponent } from './Views/check-score-do-exam/check-score-do-exam.component';

const routes: Routes = [
  {
    path: '', component: UserExaminationComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: ListExamComponent,
        data: {
          title: 'Danh sách kỳ thi'
        }
      },
      {
        path: ':id/infor-exam',
        component: BeforeDoExamComponent,
        data: {
          title: 'Thông tin kỳ thi'
        }
      },
      {
        path: ':id/create',
        component: StartExamComponent,
        data: {
          title: 'Làm bài thi'
        }
      },
      {
        path: ':id/subjects/:subjectId/shifts/:shiftId/organizeExams/:organizeExamId/supervision',
        component: ExamSupervisionComponent
      },
      {
        path: ':id/result',
        component: ShowResultDoExamComponent,
        data: {
          title: 'Kết quả thi'
        }
      }
      ,
      {
        path: 'list-subject-supervision',
        component: ListSubjectSupervisionComponent,
        data: {
          title: 'Danh sách môn thi giám sát'
        }
      },
      {
        path: 'list-subject-exam',
        component: ListSubjectExamComponent,
        data: {
          title: 'Danh sách môn thi'
        }
      }
      ,
      {
        path: ':id/list-units-exam',
        component: ListUnitsExamComponent,
        data: {
          title: 'Danh sách bài thi'
        }
      },
      {
        path: ':id/list-units-exam/:examsubjectstudentid/marking',
        component: CheckScoreDoExamComponent,
        data: {
          title: 'Chấm điểm thi'
        }
      }
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserExaminationRoutingModule { }

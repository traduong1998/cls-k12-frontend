import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSubjectSupervisionComponent } from './list-subject-supervision.component';

describe('ListSubjectSupervisionComponent', () => {
  let component: ListSubjectSupervisionComponent;
  let fixture: ComponentFixture<ListSubjectSupervisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListSubjectSupervisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSubjectSupervisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { SubjectsSupervisionEndpoint, SubjectsSupervision, SubjectsSupervisionExam, SubjectsSupervisionSystem, FilterSubjectSupervisionRequests } from 'cls-k12-sdk-js/src';

@Component({
  selector: 'app-list-subject-supervision',
  templateUrl: './list-subject-supervision.component.html',
  styleUrls: ['./list-subject-supervision.component.scss']
})
export class ListSubjectSupervisionComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT
  subjectSupervisionEndpoint: SubjectsSupervisionEndpoint
  //#endregion

  //#region PRIVATE PROPERTY
  private subjectsSupervisionSystem: SubjectsSupervisionSystem[] = [];
  private subjectsSupervisionExam: SubjectsSupervisionExam[] = [];
  private listExamIds: number[] = [];
  //#endregion

  //#region PUBLIC PROPERTY
  public displayedColumns: string[] = ['stt', 'name', 'examSubject', 'status', 'startDate', 'endDate', 'examRoom', 'examShift', 'function'];
  public isLoadingResults;
  public filterExam = this.fb.group({
    startDate: new FormControl(''),
    endDate: new FormControl(''),
    keyword: new FormControl('')
  })

  public subjectsSupervisionDisplay: SubjectsSupervision[] = [];

  public isReady = false;

  public pagingLength: number = 0;

  public pageNumber: number = 0;
  public pageSize: number = 10;
  pageEvent: PageEvent;

  public dataSource: any;
  //#endregion

  //#region CONSTRUCTOR
  constructor(private fb: FormBuilder, private router: Router) {
    this.subjectSupervisionEndpoint = new SubjectsSupervisionEndpoint();
    this.dataSource = new MatTableDataSource(this.subjectsSupervisionDisplay);
    this.dataSource.paginator = this.paginator;

  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {

  }
  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.getDataAsync(true);
  }
  //#endregion

  //#region PUBLIC METHOD
  public onSubmit() {
    this.getDataAsync(false);
  }
  public onListStudentsExamHandleClick(id: number, subjectId: number, shiftId: number, organizeExamId: number) {

    this.router.navigateByUrl(`dashboard/user-examination/${id}/subjects/${subjectId}/shifts/${shiftId}/organizeExams/${organizeExamId}/supervision`);
  }


  pageChange(event?: PageEvent) {
    this.pageNumber = this.paginator.pageIndex + 1;
    this.pageSize = this.paginator.pageSize;

    this.getDataAsync(false);
  }
  //#endregion

  //#region PRIVATE METHOD
  private passingData() {
    this.subjectsSupervisionDisplay = [];
    this.isReady = false;
    this.subjectsSupervisionSystem.forEach((system, index) => {
      let dataDisplay = new SubjectsSupervision();
      dataDisplay.id = system.examId;
      dataDisplay.organizeExamId = system.organizeExamId;
      // dataDisplay.examName = exam.name;
      dataDisplay.startDate = system.startDate;
      dataDisplay.endDate = system.endDate;
      dataDisplay.examRoom = system.roomName;
      dataDisplay.examShift = system.shiftName;
      dataDisplay.status = system.status;
      dataDisplay.subjectName = system.subjectName;
      dataDisplay.subjectId = system.subjectId;
      dataDisplay.shiftId = system.shiftId;
      dataDisplay.examName = this.subjectsSupervisionExam.find(x => x.examId === system.examId)?.name;
      this.subjectsSupervisionDisplay.push(dataDisplay);
      this.isReady = true;

    })
    console.log(`this.subjectsSupervisionDisplay`, this.subjectsSupervisionDisplay)

    this.isReady = true;
    this.dataSource.data = this.subjectsSupervisionDisplay;
  }

  private async getDataAsync(isFirstLoad: boolean) {
    let keyword = this.filterExam.controls['keyword'].value;

    let request: FilterSubjectSupervisionRequests = new FilterSubjectSupervisionRequests();
    request.pageNumber = this.pageNumber;
    request.pageSize = this.pageSize;

    let requestClone = new FilterSubjectSupervisionRequests();
    if (!isFirstLoad) {

      if (this.filterExam.controls['startDate'].value) {
        request.startDate = this.filterExam.controls['startDate'].value;
      }
      if (this.filterExam.controls['endDate'].value) {
        request.endDate = this.filterExam.controls['endDate'].value;
      }

      if (JSON.stringify(requestClone) == JSON.stringify(request)) {
        return
      }
    }
    if (keyword) {
      //tìm kiếm kỳ thi từ service exam
      var examIds = await this.subjectSupervisionEndpoint.getListExamIdByKeyword(keyword);
      if (examIds && examIds.length == 0) {
        this.subjectsSupervisionExam = [];
        this.passingData();

        return;
      }
    }

    request.listExamId = examIds;

    // this.subjectSupervisionEndpoint
    //   .getListExamIdByKeyword(keyword)
    //   .then(res => {
    //     request.listExamId = res;
    //   })
    this.subjectSupervisionEndpoint
      .getSubjectsSupervisionSystemPaging(request)
      .then((res) => {
        if (res) {
          this.subjectsSupervisionSystem = res.items;
          this.pagingLength = res.totalItems;
          res.items.map((value, index) => {
            this.listExamIds.push(value.examId);
          });

          return this.listExamIds.length > 0 ? this.subjectSupervisionEndpoint.getSubjectsSupervisionExamPaging(this.listExamIds) : [];
        }

      })
      .then((res) => {
        this.subjectsSupervisionExam = res;
        this.passingData();
      })
      .catch((er) => {

      });
  }
  //#endregion
}

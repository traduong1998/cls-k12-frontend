import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { DoExamLogEndpoint, ExamEnpoint, ExamSignalSupervisionHub, ExamSupervisionEndpoint, ExamUser } from 'cls-k12-sdk-js/src';
import { ViolatorsResponse, ViolationTypes } from 'cls-k12-sdk-js/src';
import { UserExaminationEndpoint } from 'cls-k12-sdk-js/src/services/user-examination/endpoint/user-examination.enpoint';
import { PaginatorResponse } from 'cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { ExamSupervisionStudent } from 'cls-k12-sdk-js/src/services/exam/models/exam-supervision-student';
import { ExamRegulationDialogComponent } from '../../Components/exam-regulation-dialog/exam-regulation-dialog.component';
import { HandlingViolationsDialogComponent } from '../../Components/handling-violations-dialog/handling-violations-dialog.component';
import { SwalAlertUserExaminationComponent } from '../../Components/swal-alert-user-examination/swal-alert-user-examination.component';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { CollectTestOfContestantDialog } from '../../Components/collect-test-of-contestant-dialog/collect-test-of-contestant-dialog.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { ProcessViolateRequest } from 'sdk/cls-k12-sdk-js/src/services/exam-signal/requests/process-violate-request';

@Component({
  selector: 'app-exam-supervision',
  templateUrl: './exam-supervision.component.html',
  styleUrls: ['./exam-supervision.component.scss']
})
export class ExamSupervisionComponent implements OnInit {
  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT
  //#endregion

  //#region PRIVATE PROPERTY
  private isFirstLoad = true;
  //#endregion

  //#region PUBLIC PROPERTY
  public filterExamForm = this.fb.group({
    status: new FormControl(''),
    keyword: new FormControl('')
  })

  public examId: number;
  public subjectId: number;
  public shiftId: number;
  public organizeExamId: number;

  public examName: string;

  public allNotStarted: boolean;

  /**Thí sinh đang online */
  public contestantsOnline: ExamUser[] = [];

  /**Thí sinh đã hoàn thành bài thi */
  public contestantsCompleted: number[] = [];
  /**Thí sinh chưa tham gia thi */
  public contestantsNotStarted: number[] = [];
  public listUserIds?: number[] = null;
  public excludeUserIds?: number[] = null;

  /**Thí sinh trong ca thi */
  public totalContestantInShift: number = 0;

  public examStudentViewModels: UserExamViewModel[] = [];
  public totalItemsPaging = 0;
  public lostConnectedTotals: number = 0;
  public notConnectedTotals: number = 0;

  public page: number = 0;
  public pageSize: number = 32;
  listPage: number[] = [1, 2, 3];

  violators: ViolatorsResponse[] = [];
  public subtractTotals: number = 0;
  public warningTotals: number = 0;
  public suspendedTotals: number = 0;
  public violationWarningTotals: number = 0;
  public connectedTotals: number = 0;
  supervisionHub: ExamSignalSupervisionHub;

  examSupervisionEndpoint: ExamSupervisionEndpoint;
  doExamLogEndpoint: DoExamLogEndpoint;
  userExaminationEndpoint: UserExaminationEndpoint;
  examEnpoint: ExamEnpoint;

  get status() { return this.filterExamForm.get('status'); }
  get keyword() { return this.filterExamForm.get('keyword'); }

  horizontalPosition: MatSnackBarHorizontalPosition = 'right';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  //#endregion

  //#region CONSTRUCTOR
  constructor(private fb: FormBuilder, private router: ActivatedRoute, public examRegulationDialog: MatDialog, private _snackBar: MatSnackBar, private dialog: MatDialog) {
    this.examId = +this.router.snapshot.paramMap.get('id');
    this.subjectId = +this.router.snapshot.paramMap.get('subjectId');
    this.shiftId = +this.router.snapshot.paramMap.get('shiftId');
    this.organizeExamId = +this.router.snapshot.paramMap.get('organizeExamId');

    // TODO : remove it
    this.supervisionHub = new ExamSignalSupervisionHub();
    this.examSupervisionEndpoint = new ExamSupervisionEndpoint();
    this.doExamLogEndpoint = new DoExamLogEndpoint();
    this.userExaminationEndpoint = new UserExaminationEndpoint();
    this.examEnpoint = new ExamEnpoint();
  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {
    this.examStudentViewModels = [];
    this.initEventsOfSignal();

    console.log(`đang kết nối...`);

    this.examEnpoint.getInfoExam(this.examId)
      .then((res) => {
        this.examName = res.name;

        //kết nối tới signal
        return this.supervisionHub.start();
      })
      .then(() => {
        console.log(`kết nối thành công`);
        return this.supervisionHub.registerSupervision({
          examId: this.examId,
          subjectId: this.subjectId,
          shiftId: this.shiftId,
          device: undefined
        });
      })
      .then(() => {
        console.log(`registerSupervision ok`);
        //lấy danh sách thí sinh đang online
        //và lấy danh sách người dùng trong ca thi này
        return Promise.all([
          this.supervisionHub.getContestantsOnline(),
          this.getContestants(true),
          this.examEnpoint.getViolators(this.examId, this.subjectId, this.shiftId),
          this.examEnpoint.getContestantIds(this.examId, this.subjectId, this.organizeExamId, { isCompleted: true })
        ])
      })
      .then(([contestantsOnline, contestants, violators, contestantsCompleted]) => {
        this.violators = violators;
        this.contestantsOnline = contestantsOnline;
        this.contestantsCompleted = contestantsCompleted;

        console.log(`contestantsOnline`, contestantsOnline);
        console.log(`contestants`, contestants);
        console.log(`contestantsCompleted`, contestantsCompleted);

        for (let v of this.violators) {
          if (v.violations.find(y => y.violationType == ViolationTypes.SubtractScore)) {
            this.subtractTotals++;
          }
          if (v.violations.find(y => y.violationType == ViolationTypes.Warning)) {
            this.warningTotals++;
          }
          if (v.violations.find(y => y.violationType == ViolationTypes.Suspend)) {
            this.suspendedTotals++;
          }
        }

        this.totalContestantInShift = contestants.totalItems;

        this.refreshUserViewModel(contestants);

      })
      .then(() => {
        //lấy thông tin những thí sinh tham gia thi
        //=> lấy thí sinh đã tham gia thi (không cần truyền thông số lọc) ở service exam và loại trừ những id này để có kết quả
        this.examEnpoint.getContestantIds(this.examId, this.subjectId, this.organizeExamId, {})
          .then((contestantsStarted) => {
            console.log(`contestantsStarted`, contestantsStarted);
            // if (contestantsStarted && contestantsStarted.length > 0) {
            this.examSupervisionEndpoint.getExamSupervisionFilter({
              shiftId: this.shiftId,
              listUserIds: [],
              notListUserIds: contestantsStarted,
              keyword: null,
              getCount: false,
              page: 1,
              size: 10000
            })
              .then((contestantsNotStarted) => {
                console.log(`contestantsNotStarted`, contestantsNotStarted);

                this.contestantsNotStarted = contestantsNotStarted.items.map(x => x.userId);

                this.refreshUserViewModel();

              });
            // }
            // else {
            //   //chưa ai tham gia à?
            //   this.contestantsNotStarted = [];
            //   this.allNotStarted = true;
            // }
          });

      })
      .catch((err) => {
        console.error(err);
        let errorMessage = 'Có lỗi xảy ra, vui lòng thử lại!';
        // if (err as Error) {
        //   errorMessage = (err as Error).message;
        // }
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: errorMessage,
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
  }

  ngOnDestroy() {
    console.log(`dispose supervision`);
    this.supervisionHub.dispose();
    this.supervisionHub = null;
  }

  /**
   * Build lại view model
   * @param newUsers danh sách người dùng cần build lại viewmodel. Null nếu chỉ muốn làm mới trạng thái người dùng trang hiện tại
   */
  private refreshUserViewModel(newUsers?: PaginatorResponse<ExamSupervisionStudent>) {
    //update lại trang hiện tại
    if (newUsers != null) {
      this.totalItemsPaging = newUsers.totalItems;

      this.examStudentViewModels = [];

      newUsers.items.forEach(u => {
        let examStudentViewModel: UserExamViewModel = new UserExamViewModel();
        examStudentViewModel.examSubjectStudentId = u.examSubjectStudentId;
        examStudentViewModel.userId = u.userId;
        examStudentViewModel.fullName = u.fullName;
        examStudentViewModel.avatar = u.avatar;
        this.examStudentViewModels.push(examStudentViewModel);
      });
    }

    this.examStudentViewModels.forEach(examStudentViewModel => {
      let isOnline = this.contestantsOnline.find(x => x.userId === examStudentViewModel.userId) ? true : false;
      let isCompleted = this.contestantsCompleted.find(x => x === examStudentViewModel.userId) ? true : false;
      let hasNotStarted = this.contestantsNotStarted.find(x => x === examStudentViewModel.userId) ? true : false;

      // console.log(`hasNotStarted, isOnline,isCollectedTest`, examStudentViewModel.userId, examStudentViewModel.fullName, hasNotStarted, isOnline, examStudentViewModel.isCollectedTest);
      if (isOnline) {
        hasNotStarted = false;
      }

      examStudentViewModel.connectionStatus = isOnline ? 'connected' : 'disconnected';
      examStudentViewModel.addViolations(this.violators.find(o => o.id == examStudentViewModel.userId)?.violations.map(x => x.violationType));
      examStudentViewModel.isCompleted = isCompleted;
      examStudentViewModel.hasNotStarted = hasNotStarted;
      examStudentViewModel.setCollectedTest(examStudentViewModel.isCollectedTest);
    });

  }

  //#endregion

  //#region PUBLIC METHOD

  public onExamregulationsHandleClick() {
    this.openExamRegulationDialog();
  }

  public onHandlingViolationsHandleClick(userId: number) {
    this.onHandlingViolationsDialog(userId);
  }

  public onInfomationHandleClick(userId: number) {
    alert('Tính năng đang được phát triển !')
  }

  public examReIssued() {
    this.onExamReIssuedDialog();
  }

  /**
   * includeIds: danh sách id cần bao gồm.
   * excludeIds: danh sách id loại trừ.
   * @param status 
   * @returns 
   * includeIds | excludeIds = null => không có dữ liệu tác động đến thuộc tính.
   * includeIds | excludeIds = [] => lúc này mỗi list không có id nào.
   */
  private getUserIdsFromFilter(status) {
    return new Promise<{ includeIds?: number[], excludeIds?: number[] }>((resolve, reject) => {
      switch (status) {
        // student Joined
        case 'JOI'://đang kết nối
          resolve({
            includeIds: this.contestantsOnline.map(x => x.userId),
            excludeIds: []
          });
          break;
        case 'NON'://chưa kết nối lần nào
          resolve({
            includeIds: this.contestantsNotStarted.map(x => x),
            excludeIds: []
          });
          break;
        case 'LOS'://mất kết nối
          let exclude = this.contestantsOnline ? this.contestantsOnline.map(x => x.userId) : null;
          resolve({
            includeIds: [],
            excludeIds: exclude && exclude.length > 0 ? exclude : null
          });
          break;
        case 'SUBVIO':
          var warning: number[] = this.violators.filter(x => x.violations.find(y => y.violationType == ViolationTypes.Warning)).map(x => x.id);

          resolve({
            includeIds: warning,
            excludeIds: []
          });
          break;
        case 'SUS':
          var suspend: number[] = this.violators.filter(x => x.violations.find(y => y.violationType == ViolationTypes.Suspend)).map(x => x.id);
          resolve({
            includeIds: suspend,
            excludeIds: []
          });
          break;
        case 'SUB':
          resolve({
            includeIds: this.contestantsCompleted,
            excludeIds: []
          });
          break;
        default:
          resolve({ includeIds: null, excludeIds: null });
          break;
      }
    })
  }

  public onSubmit(): void {
    this.page = 1;
    let status = this.filterExamForm.controls['status'].value;
    this.getUserIdsFromFilter(status)
      .then((data) => {
        console.log(`data`, data);
        this.listUserIds = data.includeIds;

        //không có id nào thì trả về list phân trang không có phần tử
        if (data.includeIds && data.includeIds.length == 0 && data.excludeIds && data.excludeIds.length == 0) {
          let res: PaginatorResponse<ExamSupervisionStudent> = { items: [], totalItems: 0 };
          return Promise.resolve(res);
        }

        return this.getContestants(true, data.includeIds, data.excludeIds);
      })
      .then((res) => {
        this.refreshUserViewModel(res);
      })
      .catch((err) => {

      });
  }

  public pageChange(event: any) {
    this.page = event;
    this.getContestants(false, this.listUserIds, this.excludeUserIds)
      .then((res) => {
        this.refreshUserViewModel(res);
      });
  }

  openCollectTestOfContestantDialog(contestant: UserExamViewModel) {
    const dialogRef = this.examRegulationDialog.open(CollectTestOfContestantDialog, {
      data: { contestant: contestant }
    });

    dialogRef.afterClosed().subscribe((result: { state: 'wait' | 'now' }) => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        if (result.state == 'wait') {
          //state này diễn ra bất đồng bộ
          contestant.setCollectedTest(true);
          this.supervisionHub.collectTestOfContestant({ contestantId: contestant.userId });
        }
        else if (result.state == 'now') {
          //lúc này người dùng không quan tâm thông báo bên kia, ta có thể request api để lưu luôn
          contestant.setCollectedTest(true);
          this.userExaminationEndpoint.teacherFinishDoExam(contestant.examSubjectStudentId)
            .then(() => {
              contestant.isCompleted = true;
            })
            .catch((err) => {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: "Có lỗi xảy ra, vui lòng thử lại!",
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            });
        }
      }
    });
  }

  onCollectAllTest() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '375px',
      maxWidth: '90%',
      data: {
        message: ' Bạn có chắc chắn muốn thu bài tất cả học viên ?',
        buttonText: {
          ok: 'Đồng ý',
          cancel: 'Hủy',
          colorButtonSubmit: 'warn'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (!confirmed) {
        return;
      }

      this.supervisionHub.collectTest()
        .then(() => {
          this._snackBar.openFromComponent(SuccessSnackBarComponent, {
            data: "Đã gửi yêu cầu thu bài đến từng học viên",
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        })
        .catch((err) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: "Có lỗi xảy ra, vui lòng thử lại!",
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
    });
  }

  onFinishTest() {
    //lấy tất cả thí sinh
    this.examSupervisionEndpoint.getExamSupervisionFilter({
      shiftId: this.shiftId,
      // listUserIds: [],
      // notListUserIds: [],
      keyword: null,
      getCount: false,
      page: 1,
      size: 10000
    })
      .then((contestants) => {
        let allIds = contestants.items.map(x => x.examSubjectStudentId);

        return this.userExaminationEndpoint.finishAllDoExam({ listIds: allIds });
      })
      .then(() => {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: "Đã kết thúc",
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      })
      .catch((err) => {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: "Có lỗi xảy ra, vui lòng thử lại!",
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
  }
  //#endregion

  //#region PRIVATE METHOD

  private openExamRegulationDialog() {
    const dialog = this.examRegulationDialog.open(ExamRegulationDialogComponent, {
      data: {
        examId: this.examId,
        subjectId: this.subjectId,
        shiftId: this.shiftId
      }
    });
    dialog.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  private onHandlingViolationsDialog(userId: number) {
    let data = this.examStudentViewModels.find(x => x.userId == userId);
    const dialog = this.examRegulationDialog.open(HandlingViolationsDialogComponent, {
      data: data
    });
    dialog.afterClosed().subscribe((result: { id: number, violationType: ViolationTypes, reason: string, minusScore?: number }) => {
      let callbackSuccess = () => {
        var constant = this.examStudentViewModels.find(x => x.userId == result.id);
        if (!constant) {
          console.warn(`không tìm thấy thí sinh này`);
          return;
        }

        //tăng số người bị xử lý nếu người này chưa vi phạm
        if (!constant.hasViolationType(result.violationType))
          this.increaseVisitorsTotal(result.violationType, 1);

        constant.addViolation(result.violationType);

        //thông báo cho thí sinh vi phạm
        this.supervisionHub.processViolate({
          contestantId: result.id,
          violationType: result.violationType,
          reason: result.reason,
          minusScore: result.minusScore
        });
      }
      let callbackError = (err) => {
        console.error(err);
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: "Có lỗi xảy ra, vui lòng thử lại!",
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }

      if (result) {
        if (result.violationType == ViolationTypes.SubtractScore) {
          this.userExaminationEndpoint.minusScore({
            examSubjectStudentId: data.examSubjectStudentId,
            contestantId: result.id,
            examId: this.examId,
            subjectId: this.subjectId,
            shiftId: this.shiftId,
            score: result.minusScore,
            reason: result.reason
          })
            .then(() => { callbackSuccess(); })
            .catch((err) => { callbackError(err); });
        }
        else if (result.violationType == ViolationTypes.Suspend) {
          this.userExaminationEndpoint.suspend({
            examSubjectStudentId: data.examSubjectStudentId,
            contestantId: result.id,
            examId: this.examId,
            subjectId: this.subjectId,
            shiftId: this.shiftId,
            reason: result.reason
          })
            .then(() => { callbackSuccess(); })
            .catch((err) => { callbackError(err); });
        }
        else if (result.violationType == ViolationTypes.Warning) {
          this.userExaminationEndpoint.violationWarning({
            contestantId: result.id,
            examId: this.examId,
            subjectId: this.subjectId,
            shiftId: this.shiftId,
            score: result.minusScore,
            reason: result.reason
          })
            .then(() => { callbackSuccess(); })
            .catch((err) => { callbackError(err); });
        }
      }
    });
  }

  /**Tăng số người bị xử lý */
  private increaseVisitorsTotal(violationType: ViolationTypes, num: number) {
    if (violationType == ViolationTypes.SubtractScore) {
      this.subtractTotals = this.subtractTotals + num;
    }
    else if (violationType == ViolationTypes.Suspend) {
      this.suspendedTotals = this.suspendedTotals + num;
    }
    else if (violationType == ViolationTypes.Warning) {
      this.warningTotals = this.warningTotals + num;
    }
  }

  private onExamReIssuedDialog() {
    const dialog = this.examRegulationDialog.open(SwalAlertUserExaminationComponent);
    dialog.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  /**
   * Lấy danh sách thí sinh
   * @param listUserIds lấy danh sách theo id
   * @returns listUserExamFilter
   */
  private getContestants(isFirstPage: boolean, listUserIds?: number[], excludeUserIds?: number[]) {
    if (!listUserIds) {
      listUserIds = [];
    };
    if (!excludeUserIds) {
      excludeUserIds = [];
    };

    let request = {
      shiftId: this.shiftId,
      keyword: this.keyword.value,
      listUserIds: listUserIds,
      notListUserIds: excludeUserIds,
      page: this.page,
      size: this.pageSize,
      getCount: true
    };
    if (isFirstPage) {
      this.page = 1;
      request.page = 1;
    }

    return this.examSupervisionEndpoint.getExamSupervisionFilter(request)
      .then((res) => {

        this.isFirstLoad = true;
        return res;
      });
  }

  /**Khởi tạo sự kiện cho signaling */
  private initEventsOfSignal() {
    this.supervisionHub.connection.onreconnecting(error => {
      console.log(`Connection lost due to error "${error}". Reconnecting.`);
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Đang thử kết nối lại với máy chủ...",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    });

    this.supervisionHub.connection.onreconnected(connectionId => {
      console.log(`Connection reestablished. Connected with connectionId "${connectionId}".`);
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '375px',
        maxWidth: '90%',
        data: {
          message: 'Mất kết nối đến máy chủ. Vui lòng làm mới (F5) lại trang web',
          buttonText: {
            ok: 'Đồng ý',
            cancel: 'Hủy',
            colorButtonSubmit: 'warn'
          }
        }
      });
      dialogRef.afterClosed().subscribe((confirmed: boolean) => {
        if (!confirmed) {
          return;
        }

        location.reload();
      });
    });

    this.supervisionHub.connection.onclose(error => {
      console.log(`Connection closed due to error "${error}". Try refreshing this page to restart the connection.`);
    });

    this.supervisionHub.connection.on("NewContestantJoin", (user: ExamUser) => {
      console.log('có thí sinh mới vào', user);
      if (!this.contestantsOnline.find(x => x.userId == user.userId))
        this.contestantsOnline.push(user);
      this.contestantsNotStarted = this.contestantsNotStarted.filter(x => x != user.userId);

      //nếu có trên view thì đổi trạng thái
      let newUser = this.examStudentViewModels.find(x => x.userId == user.userId);
      if (newUser) {
        newUser.connectionStatus = 'connected';
        newUser.hasNotStarted = false;
      }


    });

    this.supervisionHub.connection.on("HasAContestantLeftRoom", (user: ExamUser) => {
      console.log('có thí sinh rời phòng', user);
      this.contestantsOnline = this.contestantsOnline.filter(x => x.userId != user.userId);
      let leftUser = this.examStudentViewModels.find(x => x.userId == user.userId);
      if (leftUser) {
        leftUser.connectionStatus = 'disconnected';
      }
    });

    this.supervisionHub.connection.on("HasAContestantSubmit", (data: ExamUser) => {
      console.log('có thí sinh nộp bài', data);
      this.contestantsCompleted;
      if (!this.contestantsCompleted.find(x => x == data.userId))
        this.contestantsCompleted.push(data.userId);
      let contestant = this.examStudentViewModels.find(x => x.userId == data.userId);
      if (contestant) {
        contestant.isCompleted = true;

        //nếu người dùng này đang trong trạng thái bị thu bài
        if (contestant.isCollectedTest) {
          console.log(`thí sinh này bị ép buộc thu bài`);
        }
      }
    });

    this.supervisionHub.connection.on("HasAContestantProcessViolate", (supervisor, info: ProcessViolateRequest) => {
      console.log('Có thí sinh đã bị xử lý vi phạm bởi giám thị', supervisor, info);
      var constant = this.examStudentViewModels.find(x => x.userId == info.contestantId);
      if (!constant) {
        console.warn(`không tìm thấy thí sinh này`);
        return;
      }

      //tăng số người bị xử lý nếu người này chưa vi phạm
      if (!constant.hasViolationType(<ViolationTypes>info.violationType))
        this.increaseVisitorsTotal(<ViolationTypes>info.violationType, 1);

      constant.addViolation(<ViolationTypes>info.violationType);
    });
  }


  //#endregion
}

export class UserExamViewModel {
  examSubjectStudentId: number;
  userId: number;
  fullName: string;
  avatar: string;
  connectionStatus: 'connected' | 'disconnected' | 'reconnecting';
  violations: string[] = [];
  isCompleted: boolean;
  hasNotStarted: boolean;

  /**Bị thu bài */
  isCollectedTest: boolean;

  /**Bị đình chỉ */
  get isSuspended(): boolean {
    return this.violations?.find(y => y == ViolationTypes.Suspend) ? true : false;
  }
  /**Bị cảnh báo */
  get isWarning(): boolean {
    return this.violations?.find(y => y == ViolationTypes.Warning) ? true : false;
  }

  constructor() {
    this.violations = [];
  }

  public addViolation(vi: ViolationTypes) {
    this.violations.push(vi);
  }

  public addViolations(vis: string[]) {
    if (vis) {
      if (!this.violations)
        this.violations = [];
      vis.forEach((vi) => {
        this.violations.push(vi);
      })
    }
  }

  public hasViolation() {
    if (!this.violations)
      return false;
    return this.violations && this.violations.length > 0
  }

  public hasViolationType(violationType: ViolationTypes) {
    if (!this.violations)
      return false;
    return this.violations.find(x => x == violationType);
  }

  public setCollectedTest(yesNo: boolean) {
    this.isCollectedTest = yesNo;
  }
}
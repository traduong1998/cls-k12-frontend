import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamSupervisionComponent } from './exam-supervision.component';

describe('ExamSupervisionComponent', () => {
  let component: ExamSupervisionComponent;
  let fixture: ComponentFixture<ExamSupervisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExamSupervisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamSupervisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

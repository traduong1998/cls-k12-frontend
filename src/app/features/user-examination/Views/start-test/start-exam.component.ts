import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, HostListener, OnInit, ViewEncapsulation } from '@angular/core';
import { UserExaminationService } from '../../Services/user-examination.service';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
declare var CKEDITOR: any;
CKEDITOR.plugins.addExternal(
  'insertaudio',
  window.location.origin + '/assets/ckeditor/plugins/InsertAudio/',
  'InsertAudio.js');
CKEDITOR.plugins.addExternal(
  'insertimage',
  window.location.origin + '/assets/ckeditor/plugins/InsertImage/',
  'InsertImage.js');
CKEDITOR.plugins.addExternal(
  'insertvideo',
  window.location.origin + '/assets/ckeditor/plugins/InsertVideo/',
  'InsertVideo.js');
CKEDITOR.plugins.addExternal(
  'createunderline',
  window.location.origin + '/assets/ckeditor/plugins/underline/',
  'plugin.js');
CKEDITOR.plugins.addExternal(
  'fillblank1',
  window.location.origin + '/assets/ckeditor/plugins/fillblank1/',
  'plugin.js');
CKEDITOR.plugins.addExternal(
  'fillblank2',
  window.location.origin + '/assets/ckeditor/plugins/fillblank2/',
  'plugin.js');
CKEDITOR.plugins.addExternal('ckeditor_wiris', window.location.origin + '/assets/ckeditor/plugins/mathtype/', 'plugin.js');

@Component({
  selector: 'app-start-exam',
  templateUrl: './start-exam.component.html',
  styleUrls: ['./start-exam.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StartExamComponent implements OnInit {
  examSubjectStudentId: number;
  testId: number;
  testKitId: number;
  itemPerPage: number;
  isLoading: boolean;

  // @HostListener('document:keydown', ['$event'])
  // onKeyDown(event: KeyboardEvent) {
  //   console.log("Click f11 out");
  //   console.log(event);
  //   if (event.keyCode == 123 || event.which == 122) { // Prevent F12
  //     event.preventDefault()
  //   }
  //   else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
  //     event.preventDefault()
  //   }
  // }

  // @HostListener('document:contextmenu', ['$event'])
  // onRightClick(event: MouseEvent){
  //   event.preventDefault();
  // }

  constructor(private userExamService: UserExaminationService, private _route: ActivatedRoute, private _snackBar: MatSnackBar, private router: Router) {
    document.body.classList.add(`learn-custom-global-css`);//TODO: tạm thời khi layout chưa hỗ trợ ẩn
    this.examSubjectStudentId = +this._route.snapshot.paramMap.get("id");
    this.userExamService.initData(this.examSubjectStudentId)
      .then(() => {
        this.isLoading = false;
        this.testId = this.userExamService.infoExam.testId;
        this.testKitId = this.userExamService.infoExam.testKitId;
        this.itemPerPage = this.userExamService.infoExam.displayPerPage;
      })
      .catch((err) => {
        if (this.userExamService.infoExam) {
          if(this.userExamService.infoExam.isCompleted){
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Bạn đã hoàn thành bài thi',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
            this.router.navigate(['learner/user-examination']);
          }
          if(this.userExamService.infoExam.isSuspended){
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Bạn đã bị đình chỉ thi',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
            this.router.navigate(['learner/user-examination']);
          }
        } else {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Có lỗi xảy ra vui lòng thử lại',
            duration: 3000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });
        }
      });
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    document.body.classList.remove(`learn-custom-global-css`);
    this.userExamService.clearLocalStorage();
  }
}

import { number } from 'ngx-custom-validators/src/app/number/validator';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { ReplaySubject, Subject } from 'rxjs';
import { pairwise, startWith, takeUntil } from 'rxjs/operators';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { GradeEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { SubjectsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/subjectsEndpoint';
import { UserExaminationEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-examination/endpoint/user-examination.enpoint';
import { ListExamOfUser } from 'sdk/cls-k12-sdk-js/src/services/user-examination/model/list-exam-of-user';
import { MessageService } from 'src/app/shared/services/message.service';
import * as sj from '../../../../features/learn/intefaces/subject';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/manage/models/subject';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';

@Component({
  selector: 'app-list-exam',
  templateUrl: './list-exam.component.html',
  styleUrls: ['./list-exam.component.scss']
})
export class ListExamComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  page = 1;
  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT
  userExaminationEndpoint: UserExaminationEndpoint;
  endpointGrade: GradeEndpoint;
  userIdentity: UserIdentity;
  endpointSubject: SubjectsEndpoint

  //#endregion

  //#region PRIVATE PROPERTY
  //#endregion

  //#region PUBLIC PROPERTY
  protected _onDestroy = new Subject<void>();
  public statusCtrl: FormControl = new FormControl();
  public examTypeCtrl: FormControl = new FormControl();
  protected grades: GradeOption[];
  public gradeFilterCtrl: FormControl = new FormControl();
  public subjectFilterCtrl: FormControl = new FormControl();
  protected subjects: SubjectOption[];
  public filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  public dataExamPagination: PaginatorResponse<ListExamOfUser> = {
    items: [],
    totalItems: 0
  };
  public minDate: Date = new Date();
  examFilterClone = this.fb.group({});
  examFilter = this.fb.group({
    gradeId: new FormControl(''),
    subjectId: new FormControl(''),
    examType: new FormControl(''),
    startDate: new FormControl(''),
    endDate: new FormControl(''),
    keyword: new FormControl(''),
    status: new FormControl(''),
    statusCompleted: new FormControl(''),
    pageNumber: [''],
    pageSize: [''],
    sortField: 'CRE',
    sortDirection: 'ASC',
  })

  get gradeId() { return this.examFilter.get('gradeId'); }
  isFirstLoadGrade = true;

  get subjectId() { return this.examFilter.get('subjectId'); }
  isFirstLoadSubject = true;
  isDisableSubjectId = true;

  get examTypeId() { return this.examFilter.get('examTypeId'); }
  get status() { return this.examFilter.get('status'); }
  get statusCompleted() { return this.examFilter.get('statusCompleted'); }
  get startDate() { return this.examFilter.get('startDate'); }

  get endDate() { return this.examFilter.get('endDate'); }
  isShowFilter = true;
  isFullScreen = true;
  //#endregion

  //#region CONSTRUCTOR
  constructor(private fb: FormBuilder, private messageService: MessageService, private spinner: NgxSpinnerService) {
    this.endpointGrade = new GradeEndpoint();
    this.userIdentity = new UserIdentity();
    this.endpointSubject = new SubjectsEndpoint();
    this.userExaminationEndpoint = new UserExaminationEndpoint();
    this.examFilter.controls['pageNumber'].setValue(1);
    this.examFilter.controls['pageSize'].setValue(12);
    if (this.isFirstLoadGrade) {
      this.endpointGrade.getGradeForCombobox()
        .then(res => {
          this.isFirstLoadGrade = false;
          this.grades = res;
          this.examFilter.get('gradeId').setValue(res[0].id);
          if (this.grades && !this.grades.find(x => x.id == 0)) {
            this.grades.unshift({ id: 0, name: 'Chọn khối' });
          }
          this.filteredGrades.next(this.grades.slice());
        })
        .catch(err => { console.log(err) }
        )
    } else {

    }
  }
  //#endregion

  //#region LIFECYCLE
  ngAfterViewInit() {
    // this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    // this.paginator.page.subscribe(() => {
    //   this.examFilter.controls['pageNumber'].setValue(this.paginator.pageIndex + 1);
    //   this.examFilter.controls['pageSize'].setValue(this.paginator.pageSize);

    // });
    //this.onSubmit();
    this.gradeId.valueChanges
      .pipe(startWith(this.gradeId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetSubjectSelect();
            if (value < 1) {

              this.isDisableSubjectId = true;

            } else {

              this.isDisableSubjectId = false;
            }
          }
        }
      )

    this.subjectId.valueChanges
      .pipe(startWith(this.subjectId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

          }
        });

    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    this.subjectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSubjects();
      });

    // this.setInitialValue();
  }

  ngOnInit(): void {
    this.examFilter.controls['gradeId'].value == this.gradeId;
    this.loadUserExamData()
  }
  //#endregion

  //#region PUBLIC METHOD
  /* Get Grade */
  public onGradeSelectClicked() {


  }

  /* Get Subject */
  onSubjectSelectClicked() { // TODO : system service was not have this API
    if (this.isDisableSubjectId == true || this.gradeId.value == null || this.gradeId.value == 0 || !this.isFirstLoadSubject) {
      return;
    } else {
      this.endpointSubject.getSubjectByGradeIdOptions(this.gradeId.value).then(res => {
        this.subjects = res;
        this.isFirstLoadSubject = false;
        if (this.subjects && !this.subjects.find(x => x.id == 0)) {
          this.subjects.unshift({ id: 0, name: 'Chọn môn' });
        }
        this.filteredSubjects.next(this.subjects.slice());
      }).catch(err => {
        // sthis.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
    }
  }
  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSubjects() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.subjectFilterCtrl.value;
    if (!search) {
      this.filteredSubjects.next(this.subjects.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSubjects.next(
      this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }
  public onSubmit() {
    if (!compareTwoObject(this.examFilter.value, this.examFilterClone)) {
      this.page = 1;
      this.loadUserExamData();
    }
  }
  public loadUserExamData() {
    this.isFullScreen = false;
    this.spinner.show();
    // TODO : compare 2 object current and clone if == return
    if (this.examFilter.controls['keyword'].value == '') {
      if (this.examFilter.controls['pageSize'].value == null || this.examFilter.controls['pageSize'].value == undefined) {
        this.examFilter.controls['pageSize'].setValue(12);
      }
      this.userExaminationEndpoint.getListExamOfUser(this.examFilter.value, null).then((res) => {
        this.dataExamPagination = res;
        this.examFilterClone = { ...this.examFilter.value };
        console.log(this.dataExamPagination.items, 5555);
        if (res.items.length > 0) {
          let requestInfo = res.items.map(elem => (
            {
              subjectId: elem.subjectId,
              examId: elem.examId
            }
          ));
          /// lấy thông tin kỳ thi
          this.userExaminationEndpoint.getListExamInfo(requestInfo).then((res) => {
            this.dataExamPagination.items.forEach(x => {
              let currentInfo = res.find(r => r.examId == x.examId && r.subjectId == x.subjectId);
              if (currentInfo) {
                x.examName = currentInfo.name;
                x.testTime = currentInfo.testTime;
              }
            });
          }).catch(() => {
            this.messageService.failureCallback('Có lỗi xảy ra vui lòng thử lại');
            this.dataExamPagination.totalItems = 0;
            this.dataExamPagination.items = [];
          });;
        }
      });
      this.spinner.hide();
    } else {
      this.userExaminationEndpoint.getListExamIdByKeyword(this.examFilter.controls['keyword'].value)
        .then((res) => {
          // kiểm tra nếu có danh sach thì lấy thông tin
          if (res && res.length > 0) {
            this.userExaminationEndpoint.getListExamOfUser(this.examFilter.value, res).then((res) => {
              this.dataExamPagination = res;
              console.log(this.dataExamPagination.items, 5555);
              if (res.items.length > 0) {
                let requestInfo = res.items.map(elem => (
                  {
                    subjectId: elem.subjectId,
                    examId: elem.examId
                  }
                ));
                /// lấy thông tin kỳ thi
                this.userExaminationEndpoint.getListExamInfo(requestInfo).then((res) => {
                  this.dataExamPagination.items.forEach(x => {
                    let currentInfo = res.find(r => r.examId == x.examId && r.subjectId == x.subjectId);
                    if (currentInfo) {
                      x.examName = currentInfo.name;
                      x.testTime = currentInfo.testTime;
                    }
                  });
                }).catch(() => {
                  this.messageService.failureCallback('Có lỗi xảy ra vui lòng thử lại');
                  this.dataExamPagination.totalItems = 0;
                  this.dataExamPagination.items = [];
                });;
              }
            });
          } else {
            this.dataExamPagination.totalItems = 0;
            this.dataExamPagination.items = [];
          }
        }).catch(() => {
          this.messageService.failureCallback('Có lỗi xảy ra vui lòng thử lại');
        });
      this.spinner.hide();
    }
  }
  public getNowUTC(startDate: string): number {
    return (new Date(startDate).getTime() - new Date().getTime()) / (1000 * 60 * 60);
  }
  //#endregion

  //#region PRIVATE METHOD

  /* reset change Subject */
  private onResetSubjectSelect() {
    this.examFilter.controls['subjectId'].setValue(0);
    this.isFirstLoadSubject = true;
    // this.filterLessonForm.controls['subjectFilterCtrl'].setValue(null);
  }
  //#endregion
  //page change
  onChange(event) {
    this.examFilter.controls['pageNumber'].setValue(event);
    this.page = event;
    this.loadUserExamData();
    setTimeout(() => {
      var element = document.getElementById("kithi_" + 1);
      element.scrollIntoView({ behavior: "smooth" });
    }, 100);
  }

  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }
}

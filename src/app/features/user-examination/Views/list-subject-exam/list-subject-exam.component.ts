import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { Router } from '@angular/router';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CLSModules, CLSPermissions, DivisionEndpoint, DivisionOption, ExamEnpoint, GradeEndpoint, SchoolEndpoint, SubjectsSupervisionEndpoint, UserEndpoint, UserIdentity, UserTypeEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { TeacherMarkingEssay } from 'sdk/cls-k12-sdk-js/src/services/exam/models/teacher-marking-essay';
import { GetExamSubjectToMarkRequest } from 'sdk/cls-k12-sdk-js/src/services/exam/requests/get-exam-subject-to-mark-request';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { SubjectsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/subjectsEndpoint';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/manage/models/subject';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { AuthService } from 'src/app/core/services';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';
import { MessageService } from 'src/app/shared/services/message.service';
import * as sj from '../../../learn/intefaces/subject'
@Component({
  selector: 'app-list-subject-exam',
  templateUrl: './list-subject-exam.component.html',
  styleUrls: ['./list-subject-exam.component.scss']
})
export class ListSubjectExamComponent implements OnInit, AfterViewInit {
  baseApiUrl = 'http://localhost:65000';
  examEndpoint: ExamEnpoint;
  displayedColumns: string[] = ['stt', 'examName', 'subjectName', 'gradName', 'status', 'function'];
  public count: number = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  /** list of divisions */
  protected divisions: DivisionOption[];
  /** control for the selected division */
  // public divisionCtrl: FormControl = new FormControl();
  /** control for the MatSelect filter keyword */
  public divisionFilterCtrl: FormControl = new FormControl();
  /** list of divisions filtered by search keyword */
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;


  protected schools: SchoolOption[];
  // public schoolCtrl: FormControl = new FormControl();
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  protected grades: GradeOption[];
  // public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  /* Subject */
  protected subjects: SubjectOption[];
  public subjectFilterCtrl: FormControl = new FormControl();
  public filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  @ViewChild('singleSelectSubject', { static: true }) singleSelectSubject: MatSelect;

  private endpointUserType: UserTypeEndpoint;
  private endpointUser: UserEndpoint;
  private endpointDivision: DivisionEndpoint;
  private endpointSchool: SchoolEndpoint;
  private endpointGrade: GradeEndpoint;
  private endpointSubject: SubjectsEndpoint;
  private subjectSupervisionEndpoint: SubjectsSupervisionEndpoint
  userIdentity: UserIdentity;

  isFirstLoadDivisions = true;
  isFirstLoadSchools = true;
  isFirstLoadGrades = true;
  isFirstLoadSubject = true;
  isDisableSubject = true;

  public flexValue = 0;
  public filterSubjectDoExam: FormGroup;
  public hasAddPermission: boolean;
  public hasDeletePermission: boolean;
  public hasEditPermission: boolean;
  public levelManageValue: number = 0;
  protected _onDestroy = new Subject<void>();

  get divisionCtrl() { return this.filterSubjectDoExam.get('divisionCtrl'); }
  get schoolCtrl() { return this.filterSubjectDoExam.get('schoolCtrl'); }
  get gradeCtrl() { return this.filterSubjectDoExam.get('gradeCtrl'); }
  get subjectCtrl() { return this.filterSubjectDoExam.get('subjectCtrl'); }
  get statusCtrl() { return this.filterSubjectDoExam.get('statusCtrl'); }


  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  queryClone: GetExamSubjectToMarkRequest;
  query: GetExamSubjectToMarkRequest = {
    gradeId: 0
    , subjectId: 0
    , status: ""
    , page: 1
    , size: 10
  };
  dataExamReponse: TeacherMarkingEssay[];
  totalItems: number;
  isShowFilter = true;

  constructor(
    private _authService: AuthService, private router: Router, private messageService: MessageService) {

    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUserType = new UserTypeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUser = new UserEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubject = new SubjectsEndpoint({ baseUrl: this.baseApiUrl });
    this.examEndpoint = new ExamEnpoint();
    this.subjectSupervisionEndpoint = new SubjectsSupervisionEndpoint();
    this.userIdentity = _authService.getTokenInfo();

    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.Exam, CLSPermissions.Add);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.Exam, CLSPermissions.Edit);
    this.hasDeletePermission = this.userIdentity.hasPermission(CLSModules.Exam, CLSPermissions.Delete);

    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
      this.flexValue = 50;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      this.flexValue = 75;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
      this.flexValue = 0;
      // Tài khoản cấp Trường, schoolid fillter mặc định theo id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
    }

    this.filterSubjectDoExam = new FormGroup({
      divisionCtrl: new FormControl(''),
      schoolCtrl: new FormControl(''),
      gradeCtrl: new FormControl(''),
      subjectCtrl: new FormControl(''),
      statusCtrl: new FormControl('NON')
    });
  }
  ngAfterViewInit(): void {
    // listen for search field value changes
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });

    // listen for search field value changes
    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    this.subjectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSubjects();
      });
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.query.page = this.paginator.pageIndex + 1;
      this.query.size = this.paginator.pageSize;
      this.getData();
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  public onSubmit() {
    this.paginator.firstPage();
    this.query.gradeId = this.gradeCtrl.value;
    this.query.status = this.statusCtrl.value;
    this.query.subjectId = this.subjectCtrl.value;
    if (!compareTwoObject(this.query, this.queryClone)) {
      this.getData();
    }
  }
  public onChangeDivisionSelected(id: any) {
    // this.resetSchoolSelectCtrl();
    // this.resetGradeSelectCtrl();
  }

  public onChangeSchoolSelected(id: any) {
    this.resetGradeSelectCtrl();
  }

  public onChangeGradeSelected(id: any) {
    this.isDisableSubject = this.gradeCtrl.value > 0;
    this.subjectCtrl.setValue(0);
  }

  public onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.divisions = res;
          if (this.divisions.find(x => x.id !== null)) {
            this.divisions.splice(0, 0, { id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(

        )
    } else {

    }
  }

  public onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionCtrl.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchools || this.schoolFilter.divisionId != divisionIdSelected) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchools = false;
          this.schools = res;
          if (this.schools.find(x => x.id !== null)) {
            this.schools.splice(0, 0, { id: null, name: 'Chọn trường' })
          }
          this.schoolFilter.divisionId = divisionIdSelected;
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(

        )
    } else {

    }
  }
  public onGradeSelectClicked() {
    var schoolIdSelected = this.schoolCtrl.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrades || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          if (this.grades.find(x => x.id !== null)) {
            this.grades.splice(0, 0, { id: null, name: 'Chọn khối' })
          }
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch(

        )
    } else {

    }
  }

  public onSubjectSelectClicked() { // TODO : system service was not have this API
    if (!this.gradeCtrl.value || !this.isFirstLoadSubject) {
      return;
    } else {
      this.endpointSubject.getSubjectByGradeIdOptions(this.gradeCtrl.value)
        .then(res => {
          this.subjects = res;
          this.isFirstLoadSubject = false;
          if (this.subjects && !this.subjects.find(x => x.id == 0)) {
            this.subjects.unshift({ id: 0, name: 'Chọn môn' });
          }
          this.filteredSubjects.next(this.subjects.slice());
        }).catch(err => {
          // sthis.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
        });
    }
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSubjects() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.subjectFilterCtrl.value;
    if (!search) {
      this.filteredSubjects.next(this.subjects.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSubjects.next(
      this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }

  private getData() {
    this.examEndpoint.getExamSubjectToMarkQuery(this.query)
      .then(res => {
        this.queryClone = { ...this.query };
        this.dataExamReponse = res.items;
        this.totalItems = res.totalItems;
        let listExamIds = this.dataExamReponse.map((x) => {
          return x.examId;
        })
        console.log('examID', listExamIds);
        if (listExamIds.length > 0) {
          this.subjectSupervisionEndpoint.getSubjectsSupervisionExamPaging(listExamIds).then(res => {
            this.dataExamReponse.forEach(x => {
              let currentInfo = res.find(r => r.examId == x.examId);
              if (currentInfo) {
                x.examName = currentInfo.name;
              }
            });
          })
        }
      }).catch(() => {
        this.messageService.failureCallback('Có lỗi xảy ra vui lòng thử lại!');
      })
  }

  private resetSchoolSelectCtrl() {
    this.schools = [];
    this.isFirstLoadSchools = true;
    this.schoolCtrl.setValue(null);
  }

  private resetGradeSelectCtrl() {
    this.grades = [];
    this.isFirstLoadGrades = true;
    this.gradeCtrl.setValue(null);
  }

  private resetSubjectSelectCtrl() {

  }
  navigateMarking(id: number) {
    this.router.navigate(['dashboard/user-examination/', id, 'list-units-exam']);
  }
  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }

}

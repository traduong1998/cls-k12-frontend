import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSubjectExamComponent } from './list-subject-exam.component';

describe('ListSubjectExamComponent', () => {
  let component: ListSubjectExamComponent;
  let fixture: ComponentFixture<ListSubjectExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListSubjectExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSubjectExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

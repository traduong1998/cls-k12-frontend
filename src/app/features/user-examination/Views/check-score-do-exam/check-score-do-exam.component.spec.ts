import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckScoreDoExamComponent } from './check-score-do-exam.component';

describe('CheckScoreDoExamComponent', () => {
  let component: CheckScoreDoExamComponent;
  let fixture: ComponentFixture<CheckScoreDoExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckScoreDoExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckScoreDoExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

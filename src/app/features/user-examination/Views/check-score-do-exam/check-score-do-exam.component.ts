import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { CKEditorComponent } from 'ckeditor4-angular';
import { Observable, Subscription } from 'rxjs';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src';
import { TestKitBankEnpoint } from 'sdk/cls-k12-sdk-js/src/services/testkit-bank/endpoints/testkit-bank-endpoint';
import { MarkingEssayRequest } from 'sdk/cls-k12-sdk-js/src/services/user-examination/endpoint/request/marking-essay-request';
import { UserExaminationEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-examination/endpoint/user-examination.enpoint';
import { DoExamEssay, InfoMarkingEssay } from 'sdk/cls-k12-sdk-js/src/services/user-examination/model/info-marking-essay';
import { DialogConfirmComponent } from 'src/app/features/examination-manager/Components/dialog-confirm/dialog-confirm.component';
import { DialogConfirm } from 'src/app/features/examination-manager/interfaces/dialog-confirm';
import { MessageService } from 'src/app/shared/services/message.service';
import { ckConfig } from 'src/environments/ckeditorConfig';

@Component({
  selector: 'app-check-score-do-exam',
  templateUrl: './check-score-do-exam.component.html',
  styleUrls: ['./check-score-do-exam.component.scss']
})
export class CheckScoreDoExamComponent implements OnInit, AfterViewInit {
  @ViewChildren("cks") cks: QueryList<CKEditorComponent>;
  request: MarkingEssayRequest = {
    examSubjectStudentId: 0,
    teacherMarkings: null,
    isFinish: false
  };
  configEditor;
  organizeExamId: number;
  userExaminationEndpoint: UserExaminationEndpoint;
  examSubjectStudentId: number;
  routeSub: Subscription;
  infoMarkingEssay: InfoMarkingEssay = {
    testKitId: 0,
    examId: 0,
    testCodeURL: "",
    testId: 0,
    userExercises: [],
    examName: ""
  };
  testKitBankEndpoint: TestKitBankEnpoint;
  userExercises: DoExamEssay[] = [];
  latedUserExercises: DoExamEssay[] = [];
  private questions: QuestionTest[] = [];
  dialogConfirm: DialogConfirm;
  isValid = true;
  constructor(private router: Router, private route: ActivatedRoute, private httpClient: HttpClient, private messageService: MessageService, public dialog: MatDialog) {
    this.userExaminationEndpoint = new UserExaminationEndpoint();
    this.testKitBankEndpoint = new TestKitBankEnpoint();
  }
  ngAfterViewInit(): void {

  }
  ngOnInit(): void {
    this.configEditor = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: false,
      allowedContent: true,
      extraPlugins: ckConfig.extraPlugins,
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
      removePlugins: 'elementspath,magicline',
      heigth: "300px"
    };
    this.routeSub = this.route.params.subscribe(params => {
      this.examSubjectStudentId = params['examsubjectstudentid'];
      this.organizeExamId = params['id'];
      this.request.examSubjectStudentId = +this.examSubjectStudentId;
    });
    this.getData();
  }
  private getData() {
    // lấy bài làm tự luận
    this.userExaminationEndpoint.getEssayDoExam(this.examSubjectStudentId)
      .then(res => {
        this.infoMarkingEssay = res;
        this.userExercises = res.userExercises;
        this.latedUserExercises = JSON.parse(JSON.stringify(this.userExercises));

        // lấy đề của thí sinh
        this.GetListQuestionFromUrl(this.infoMarkingEssay.testCodeURL).subscribe(qts => {
          this.questions = qts.questions;
          this.userExercises.forEach(item => {
            let question = this.questions.find(x => x.id == item.questionId);
            if (question) {
              item.questionContent = question.content;
              item.order = question.order;
              item.questionScore = question.score;
            }
          });
        })

      })
      .catch((err) => {
        this.messageService.failureCallback('có lỗi xảy ra vui lòng thử lại1')
      })

  }

  public GetListQuestionFromUrl(url: string): Observable<any> {
    return this.httpClient.get(url);
  }

  SortedUserExercies() {
    return this.userExercises.sort((a, b) => { return a.order - b.order });
  }
  cancel() {
    let lated = this.latedUserExercises.map(x => ({ score: x.score, comment: x.teacherComment }));
    let current = this.userExercises.map(x => ({ score: x.score, comment: x.teacherComment }));
    // kiểm tra thay đổi xem có hủy hay k
    if (JSON.stringify(lated) !== JSON.stringify(current)) {
      this.dialogConfirm = { title: 'LƯU THAY ĐỔI', description: 'Bạn có muốn lưu thay đổi bài chấm thi không?', type: 'confirm', labelYes: 'Lưu', labelNo: 'Không' }
      const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.save();
        } else if (result == false) {
          this.navigateMarking();
        }
      });
    } else {
      this.navigateMarking();
    }

  }
  save() {
    if (!this.isValid) {
      this.messageService.failureCallback('Dữ liệu không chính xác, không thể lưu');
      return;
    }
    this.request.teacherMarkings = this.userExercises.map(x => ({ doExamId: x.id, score: x.score, comment: x.teacherComment }));
    this.request.isFinish = false;
    this.userExaminationEndpoint.markingEssay(this.request).then((res) => {
      if (!res) {
        this.messageService.failureCallback('có lỗi xảy ra trong quá trình lưu');
      } else {
        this.messageService.susccessCallback('Lưu thành công!');
        this.navigateMarking();
      }
    }).catch(() => {
      this.messageService.failureCallback('có lỗi xảy ra trong quá trình lưu');
    });
  }
  done() {
    if (!this.isValid) {
      this.messageService.failureCallback('Dữ liệu không chính xác, không thể lưu');
      return;
    }
    this.request.teacherMarkings = this.userExercises.map(x => ({ doExamId: x.id, score: x.score, comment: x.teacherComment }));
    this.request.isFinish = true;
    this.userExaminationEndpoint.markingEssay(this.request).then((res) => {
      if (!res) {
        this.messageService.failureCallback('có lỗi xảy ra trong quá trình lưu');
      } else {
        this.messageService.susccessCallback('Chấm bài thành công!');
        this.navigateMarking();
      }
    }).catch(() => {
      this.messageService.failureCallback('có lỗi xảy ra trong quá trình lưu');
    });
  }
  navigateMarking() {
    this.router.navigate(['dashboard/user-examination/', this.organizeExamId, 'list-units-exam']);
  }
  validateScore() {
    this.isValid = !this.userExercises.some(x => x.score > x.questionScore || x.score < 0);
  }
  downloadFile(url, index) {
    // const a = document.createElement('a');
    // a.style.display = 'none';
    // a.href = url;
    // // the filename you want
    // a.download = 'file' + (index + 1);
    // document.body.appendChild(a);
    // a.click();
    window.open(url, '_blank');
  }
}

import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ExamEnpoint } from 'sdk/cls-k12-sdk-js/src';
import { ExamSubjectStudentToMark } from 'sdk/cls-k12-sdk-js/src/services/exam/models/exam-subject-student-tomark';
import { GetExamSubjectStudentToMarkRequest } from 'sdk/cls-k12-sdk-js/src/services/exam/requests/get-exam-subject-student-tomark-request';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-list-units-exam',
  templateUrl: './list-units-exam.component.html',
  styleUrls: ['./list-units-exam.component.scss']
})
export class ListUnitsExamComponent implements OnInit,AfterViewInit {
  displayedColumns: string[] = ['position', 'status', 'checkScore'];
  public count: number = 0;
  public filterSubjectDoExam: FormGroup;
  routeSub: Subscription;
  organizeExamId: number;
  examEndpoint: ExamEnpoint;
  request: GetExamSubjectStudentToMarkRequest = { organizeExamId: 0, isFinish: null, page: 1, size: 10 };
  get statusCtrl() { return this.filterSubjectDoExam.get('statusCtrl'); }
  oldStatus:string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  data: ExamSubjectStudentToMark[];
  totalItems: number;
  isShowFilter = true;
  constructor(private router: Router, private route: ActivatedRoute,private messageService:MessageService) {
    this.filterSubjectDoExam = new FormGroup({
      statusCtrl: new FormControl()
    })
    this.examEndpoint = new ExamEnpoint();
  }
  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.request.page = this.paginator.pageIndex + 1;
      this.request.size = this.paginator.pageSize;
      this.getData();
    });
  }
  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.request.organizeExamId = params['id'];
    });
    this.getData();
  }
  public onSubmit() {
    if(this.oldStatus!=this.statusCtrl.value){
      this.paginator.firstPage();
      this.getData();
    }
  }
  private getData() {
    this.request.isFinish=this.statusCtrl.value;
    this.examEndpoint.getListExamSubjectStudentToMark(this.request)
      .then(res => {
        this.oldStatus=this.statusCtrl.value;
        this.data = res.items;
        this.totalItems = res.totalItems;
      })
      .catch(() => {
        this.messageService.failureCallback('Có lỗi xảy ra vui lòng thử lại!');
      })
  }
  navigateMarking(id: number) {
    this.router.navigate(['dashboard/user-examination/', this.request.organizeExamId, 'list-units-exam', id, 'marking']);
  }
  previous(){
    this.router.navigate(['dashboard/user-examination/list-subject-exam']);
  }
  
  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }
}

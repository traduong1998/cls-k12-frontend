import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUnitsExamComponent } from './list-units-exam.component';

describe('ListUnitsExamComponent', () => {
  let component: ListUnitsExamComponent;
  let fixture: ComponentFixture<ListUnitsExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListUnitsExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUnitsExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

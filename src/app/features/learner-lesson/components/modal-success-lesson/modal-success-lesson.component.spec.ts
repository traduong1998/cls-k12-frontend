import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSuccessLessonComponent } from './modal-success-lesson.component';

describe('ModalSuccessLessonComponent', () => {
  let component: ModalSuccessLessonComponent;
  let fixture: ComponentFixture<ModalSuccessLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalSuccessLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSuccessLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

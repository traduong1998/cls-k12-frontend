import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutContentCompleteDialogComponent } from './out-content-complete-dialog.component';

describe('OutContentCompleteDialogComponent', () => {
  let component: OutContentCompleteDialogComponent;
  let fixture: ComponentFixture<OutContentCompleteDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OutContentCompleteDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OutContentCompleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

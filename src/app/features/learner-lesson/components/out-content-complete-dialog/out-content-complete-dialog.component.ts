import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-out-content-complete-dialog',
  templateUrl: './out-content-complete-dialog.component.html',
  styleUrls: ['./out-content-complete-dialog.component.scss']
})
export class OutContentCompleteDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<OutContentCompleteDialogComponent>,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit(): void {
  }

  onClickOut() {
    this.router.navigateByUrl('learner/user-learn')
  }

}

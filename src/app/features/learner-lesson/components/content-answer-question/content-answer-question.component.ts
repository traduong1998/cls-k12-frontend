import { takeUntil } from 'rxjs/operators';
import { OnChanges, SimpleChanges } from '@angular/core';
import { CongratulationComponent } from './../congratulation/congratulation.component';
import { UserContentTestInfo } from './../../../../../../sdk/cls-k12-sdk-js/src/services/user-test/responses/user-content-test-info-response';
import { Component, EventEmitter, HostListener, Input, NgZone, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { UserTestEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-test/endpoint/user-test-endpoint';
import { UserExercise } from 'src/app/shared/modules/userexam/UserAnswer';
import { NotificationComponent } from '../notification/notification.component';
import { ResultTestComponent } from '../result-test/result-test.component';
import { RuleTestDialogComponent } from '../rule-test-dialog/rule-test-dialog.component';
import { DoTestService } from '../../Services/do-test-service.service';
import { ProgressPlayerService } from 'src/app/features/learner-lesson/Services/progress-player.service';
import { ResultTestDialogComponent } from '../result-test-dialog/result-test-dialog.component';

@Component({
  selector: 'learner-lesson-content-answer-question',
  templateUrl: './content-answer-question.component.html',
  styleUrls: ['./content-answer-question.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContentAnswerQuestionComponent implements OnInit, OnChanges {
  @Input() userContentTestId: number;
  @Input() curriculumId: number;
  @Input() testId: number;
  @Output() isChildReady = new EventEmitter<boolean>();
  itemsPerPage = 5;
  userExercise: UserExercise;
  questions: QuestionTest[] = [];
  currentPage: number = 1;
  screenWidth;
  isStartDoTest = false;
  timeCountDown: Date;
  timeLeft: number;
  totalPage: number;
  isReady: boolean = false;
  usertestEndpoint: UserTestEndpoint;
  subcriberUserAnswersInsert: Subscription;
  subcriberUserAnswersUpdate: Subscription;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  infoTest: UserContentTestInfo;
  destroy$: Subject<any> = new Subject();
  isSingleClickFinish: boolean = true;
  isDisableFinishButton: boolean = false;
  private intervalWritelog;
  private userAnswersInsert: UserExercise[];
  private userAnswersUpdate: UserExercise[];
  private isNeedUpdateBeforeDestroy: boolean = true;
  get questionType(): typeof Type {
    return Type;
  }
  get questionFormat(): typeof FormatQuestion {
    return FormatQuestion;
  }
  @HostListener('window:resize', ['$event'])
  darkMode = false;

  onResize(event?) {
    this.screenWidth = window.innerWidth;
  }

  @HostListener('window:unload', ['$event'])
  unloadHandler(event: Event) {
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event: Event) {
    this.destroy$.next();
    if (this.intervalWritelog) {
      clearInterval(this.intervalWritelog);
    }
    this.doTestService.updateUserExercise(false, this.userContentTestId).then((res) => {
      if (res) {
        localStorage.setItem('updateBeforeUnloadHander', "true," + this.userContentTestId.toString() + "," + new Date().toLocaleDateString());
      }
      else {
        localStorage.setItem('updateBeforeUnloadHander', "false" + this.userContentTestId.toString() + "," + new Date().toLocaleDateString());
      }
    }).catch(() => {
      localStorage.setItem('updateBeforeUnloadHander', "error" + this.userContentTestId.toString() + "," + new Date().toLocaleDateString());
    })
  }

  pagingQuestions() {
    let currentItem = (this.currentPage - 1) * this.itemsPerPage
    return this.questions.splice(currentItem, currentItem + this.itemsPerPage - 1);
  }
  constructor(private zone: NgZone, private doTestService: DoTestService, public dialog: MatDialog, private router: Router, private _snackBar: MatSnackBar, private progressPlayerService: ProgressPlayerService) {
    this.onResize();
    this.currentPage = 1;
    this.usertestEndpoint = new UserTestEndpoint();
    clearInterval(this.intervalWritelog);
  }
  ngOnChanges(changes: SimpleChanges): void {
    if ('userContentTestId' in changes) {
      setTimeout(() => {
        this.progressPlayerService.emitLightModeEvent(false);
      }, 0);

      if (localStorage.getItem('light_mode') === 'true') {
        this.darkMode = true;
      }

      if (this.intervalWritelog) {
        //console.log("Clear interval onChange", this.userContentTestId);
        clearInterval(this.intervalWritelog);
      }
      this.intervalWritelog = undefined;

      this.doTestService.userUpdateAnswer$.pipe(takeUntil(this.destroy$)).subscribe((userExercises) => {
        this.userAnswersUpdate = userExercises;
      });
      this.doTestService.userAnswer$.pipe(takeUntil(this.destroy$)).subscribe((userExercises) => {
        this.userAnswersInsert = userExercises;
      });

      this.doTestService.isReady.pipe(takeUntil(this.destroy$)).subscribe((isReady) => {
        if (isReady) {
          this.StartDoExam();
          this.infoTest = this.doTestService.infoTest;
          this.timeLeft = this.infoTest.timeRemaining;
          this.doTestService.Questions$.subscribe(data => {
            this.questions = data;
            if (this.questions.length <= this.itemsPerPage) {
              this.totalPage = 1;
            }
            else {
              this.totalPage = Math.floor(this.questions.length / this.itemsPerPage);
              if (this.questions.length % this.itemsPerPage > 0) {
                this.totalPage++;
              };
            }
            //console.log("totalpage", this.totalPage);
            this.isChildReady.emit(true);
            this.isReady = true;
          });
          this.intervalWritelog = setInterval(() => {
            this.updateTest(this.userContentTestId);
          }, DoTestService.timeToWriteLog * 60 * 1000);
        }
      })
    }
  }
  ngOnDestroy(): void {
    if (this.intervalWritelog) {
      clearInterval(this.intervalWritelog);
    }
    if (this.isNeedUpdateBeforeDestroy) {
      this.doTestService.updateUserExercise(false, this.userContentTestId).then((res) => {
        if (res) {
          localStorage.setItem('updateWhenDestroy', "true" + this.userContentTestId.toString() + "," + new Date().toLocaleDateString());
        }
      });
    }
    this.destroy$.next();
    if (this.darkMode) {
      this.progressPlayerService.emitLightModeEvent(true);
    }
  }
  ngOnInit(): void { }
  updateTest(userContentTestId: number): void {
    //console.log('gửi dữ liệu lên server', new Date().toLocaleString());
    this.doTestService.updateUserExercise(false, userContentTestId).then((isUpdated) => {
      if (isUpdated) {
        this.doTestService.clearUpdateAnswerOfUser();
        //console.log('đã clear danh sách câu hỏi được cập nhật');
      }
      else {
        //console.log("Có lỗi xảy ra");
      }
    });
  }

  StartDoExam = () => {
    this.isStartDoTest = true;
    console.log(this.isStartDoTest);
  }

  //chuyển trang khi bấm vào câu hỏi số ??? và scroll tới đó
  scrollToElement(index): void {
    //khi ở man hình nhỏ
    //tìm trang hiện tại của index
    var currentPageOfIndex = Math.floor((index + (this.itemsPerPage - 1)) / this.itemsPerPage);
    // nếu khác trang thì sẽ chuyển trang
    if (this.currentPage != currentPageOfIndex) {
      this.currentPage = currentPageOfIndex;
      setTimeout(() => {
        var element = document.getElementById("cauhoi_" + index);
        var elementPosition = element.offsetTop;
        element.scrollIntoView({ behavior: "smooth" });
      }, 300);
    } else {
      var element = document.getElementById("cauhoi_" + index);
      var elementPosition = element.offsetTop;
      element.scrollIntoView({ behavior: "smooth" });
    }
  }
  //chuyển lên đầu trang khi chuyển trang
  pageChanged(event): void {
    this.currentPage = event;
    let firstQuestionOfPage = ((this.currentPage - 1) * this.itemsPerPage) + 1;
    setTimeout(() => {
      var element = document.getElementById("cauhoi_" + firstQuestionOfPage);
      var elementPosition = element.offsetTop;
      element.scrollIntoView({ behavior: "smooth" });
    }, 100);
  }
  finishTest() {
    this.isSingleClickFinish = true;
    setTimeout(() => {
      if (this.isSingleClickFinish) {
        this.isDisableFinishButton = true;
        let dialogRef = this.dialog.open(NotificationComponent, { panelClass: 'notification-dialog', data: { dataAnswer: this.doTestService.getUserAnswer(), dataQuestion: this.questions } });
        dialogRef.afterClosed().subscribe((isSubmit) => {
          if (isSubmit == true) {
            this.doTestService.submitUserExercise(this.userContentTestId, this.curriculumId).then((finishDoTestResponse) => {
              this.isNeedUpdateBeforeDestroy = false;
              clearInterval(this.intervalWritelog);
              if (finishDoTestResponse.isFinishedLesson) {
                let dialogCongra = this.dialog.open(CongratulationComponent, { width: '100%', height: '95vh' });
                dialogCongra.afterClosed().subscribe(() => {
                  let dialogShowResult = this.dialog.open(ResultTestDialogComponent, {
                    width: '615px',
                    data: {finishDoTestResponse: finishDoTestResponse, contentName: this.infoTest.name, isAnswer: true }
                  });
  
                  dialogShowResult.afterClosed().subscribe(value => {
                    switch (value) {
                      case "RED":
                        this.usertestEndpoint.createUserContentTest(this.infoTest.lessonId, this.infoTest.contentId).then((res) => {
                          this.isDisableFinishButton = false;
                          this.router.navigate([`learner/lessons/${this.infoTest.lessonId}/contents/${this.infoTest.contentId}/start-test/${res.id}`]);
                        });
                        break;
                      case "VIE":
                        this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/result/${this.userContentTestId}`])
                        break;
                      case "COF":
                        this.usertestEndpoint.confirmResultTest(null, this.userContentTestId).then(userLesson=>{
                          if (userLesson) {
                            this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`])
                          }
                        });
                        break;
                      default:
                      this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`])
                        break;
                    }
                  })
                })
              }
              else {
                let dialogShowResult = this.dialog.open(ResultTestDialogComponent, {
                  width: '615px',
                  data: {finishDoTestResponse: finishDoTestResponse, contentName: this.infoTest.name, isAnswer: true }
                });
                dialogShowResult.afterClosed().subscribe(value => {
                  switch (value) {
                    case "RED":
                      this.usertestEndpoint.createUserContentTest(this.infoTest.lessonId, this.infoTest.contentId).then((res) => {
                        this.isDisableFinishButton = false;
                        this.router.navigate([`learner/lessons/${this.infoTest.lessonId}/contents/${this.infoTest.contentId}/start-test/${res.id}`]);
                      });
                      break;
                    case "VIE":
                      this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/result/${this.userContentTestId}`])
                      break;
                    case "COF":
                      this.usertestEndpoint.confirmResultTest(null, this.userContentTestId).then(userLesson=>{
                        if (userLesson) {
                          this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`])
                        }
                      });
                      break;
                    default:
                    this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`])
                      break;
                  }
                })
              }
            })
              .catch((ERR) => {
                console.error(ERR);
                this._snackBar.open("Có lỗi xảy ra, vui lòng thử lại!", 'ERR', {
                  duration: 3000,
                  panelClass: ['red-snackbar'],
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              });
          }
          else{
            this.isDisableFinishButton = false;
          }
        })
      }
    }, 250);
  }

  onDoubleClickFinish(): void {
    this.isSingleClickFinish = false;
  }


  finishTestOverTime(): void {
    this.doTestService.submitUserExercise(this.userContentTestId, this.curriculumId).then((finishDoTestResponse) => {
      this.isNeedUpdateBeforeDestroy = false;
      clearInterval(this.intervalWritelog);

      if (finishDoTestResponse.isFinishedLesson) {
        let dialogCongra = this.dialog.open(CongratulationComponent, { width: '100%', height: '95vh' });
        dialogCongra.afterClosed().subscribe(() => {
          let dialogShowResult = this.dialog.open(ResultTestDialogComponent, {
            width: '615px',
            data: {finishDoTestResponse: finishDoTestResponse, contentName: this.infoTest.name, isAnswer: true }
          });
          dialogShowResult.afterClosed().subscribe(value => {
            switch (value) {
              case "RED":
                this.usertestEndpoint.createUserContentTest(this.infoTest.lessonId, this.infoTest.contentId).then((res) => {
                  this.isDisableFinishButton = false;
                  this.router.navigate([`learner/lessons/${this.infoTest.lessonId}/contents/${this.infoTest.contentId}/start-test/${res.id}`]);
                });
                break;
              case "VIE":
                this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/result/${this.userContentTestId}`])
                break;
              case "COF":
                this.usertestEndpoint.confirmResultTest(null, this.userContentTestId).then(userLesson=>{
                  if (userLesson) {
                    this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`])
                  }
                });
                break;
              default:
              this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`])
                break;
            }
          })
        })
      }
      else {
        let dialogShowResult = this.dialog.open(ResultTestDialogComponent, {
          width: '615px',
          data: {finishDoTestResponse: finishDoTestResponse, contentName: this.infoTest.name, isAnswer: true }
        });

        dialogShowResult.afterClosed().subscribe(value => {
          switch (value) {
            case "RED":
              this.usertestEndpoint.createUserContentTest(this.infoTest.lessonId, this.infoTest.contentId).then((res) => {
                this.isDisableFinishButton = false;
                this.router.navigate([`learner/lessons/${this.infoTest.lessonId}/contents/${this.infoTest.contentId}/start-test/${res.id}`]);
              });
              break;
            case "VIE":
              this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/result/${this.userContentTestId}`])
              break;
            case "COF":
              this.usertestEndpoint.confirmResultTest(null, this.userContentTestId).then(userLesson=>{
                if (userLesson) {
                  this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`])
                }
              });
              break;
            default:
            this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`])
              break;
          }
        })
      }
    })
      .catch((ERR) => {
        console.error(ERR);
        this._snackBar.open("Có lỗi xảy ra, vui lòng thử lại!", 'ERR', {
          duration: 3000,
          panelClass: ['red-snackbar'],
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
  }
  onNextPage(): void {
    if (this.currentPage < this.totalPage) {
      this.currentPage++;
      this.pageChanged(this.currentPage);
    }
    // this.currentPage<this.totalPage??this.currentPage++
  }
  onBackPage(): void {
    if (this.currentPage > 0) {
      this.currentPage--;
      this.pageChanged(this.currentPage);
    }
  }
  openExamRuleDialog(): void {
    let dialogRef = this.dialog.open(RuleTestDialogComponent, { panelClass: 'exam-rule-dialog', data: this.doTestService.infoTest });
  }
  SortedQuestions() {
    return this.questions.sort((a, b) => { return a.order - b.order });
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentAnswerQuestionComponent } from './content-answer-question.component';

describe('ContentAnswerQuestionComponent', () => {
  let component: ContentAnswerQuestionComponent;
  let fixture: ComponentFixture<ContentAnswerQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentAnswerQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentAnswerQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

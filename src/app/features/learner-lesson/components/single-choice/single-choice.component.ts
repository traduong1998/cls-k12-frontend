import { DoTestService } from './../../Services/do-test-service.service';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { UserExercise } from 'src/app/shared/modules/userexam/UserAnswer';

@Component({
  selector: 'learner-lesson-single-choice',
  templateUrl: './single-choice.component.html',
  styleUrls: ['./single-choice.component.scss']
})
export class SingleChoiceComponent implements OnInit {
  @Input() dataQuestion: QuestionTest;
  @Input() userAnswer;
  @Input() isResult;
  @Input() parentId;
  @Input() isGroup;
  @Input() order;
  @Input() isPublicAnswer:boolean;
  @Output() reciveTrueQuestionEvent = new EventEmitter<any>();
  @Output() dataOfGroup = new EventEmitter();
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  selectedAnswer: number;
  rightAnswer: number;
  AnswerOfUserGroup: UserExercise;
  isFlag = false;
  isTrueQuestion;
  format = FormatQuestion;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];

  constructor(private doTestService: DoTestService) { }
  ngOnInit(): void {
    this.isGroup = this.dataQuestion.format;
    var oldData = this.doTestService.getAnswerOfQuestion(this.dataQuestion.id);
    if (oldData) {
      this.isFlag = oldData.isMark;
    }
    if (this.dataQuestion.format == this.format.group) {
      console.log('câu con', this.dataQuestion);
    }
    if (oldData && oldData.userAnswers.length > 0) {
      this.selectedAnswer = oldData.userAnswers[0].answerId;
    }
    //lây vị trí đáp án đúng bôi đen
    if (this.userAnswer) {
      for (let i = 0; i < this.userAnswer.length; i++) {
        if (this.dataQuestion.id == this.userAnswer[i].questionId) {
          this.rightAnswer = this.userAnswer[i].userAnswer[0].answerId;
        }
      }
    }
    if (this.isResult) {
      this.isAnswerTrue();
    }
  }
  isMark() {
    this.isFlag = !this.isFlag;
    this.doTestService.changeTickUserExercise(this.dataQuestion.id);
  }
  isAnswerTrue() {
    let userAnswers = this.userAnswer.userAnswers;
    if (userAnswers && userAnswers.length > 0) {
      let answerId = userAnswers[0].answerId;
      if (this.dataQuestion.answers.find(x => x.id == answerId).trueAnswer != 1) {
        //alert('câu đơn sai')
        this.isTrueQuestion = false;
      }
      else {
        this.isTrueQuestion = true;
      }
    } else {
      //alert('câu đơn sai ( chưa làm)')
      this.isTrueQuestion = false;
    }
  }
  changeSelectAnswer(id) {
    this.doTestService.onChangeUserExercise(this.dataQuestion.id, [{ answerId: id, answer: 1 }]);
    if (this.parentId != undefined) {
      this.doTestService.updateIsAnsweredGroup(this.dataQuestion, this.parentId);
    }
  }

  CheckTrueQuestion() {
    this.reciveTrueQuestionEvent.emit({ isTrueQuestion: this.isTrueQuestion, questionId: this.dataQuestion.id });
  }
  ngAfterViewInit(): void {
    this.CheckTrueQuestion();
  }
  private namae(params) {

  }

}

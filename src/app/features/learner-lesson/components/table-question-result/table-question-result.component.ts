import { UserContentTestInfo } from './../../../../../../sdk/cls-k12-sdk-js/src/services/user-test/responses/user-content-test-info-response';
import { RuleTestDialogComponent } from './../rule-test-dialog/rule-test-dialog.component';
import { DoTestService } from './../../Services/do-test-service.service';
import { DOCUMENT } from '@angular/common';
import { ResultDoTestResponse } from 'sdk/cls-k12-sdk-js/src/services/user-test/responses/result-do-test-response';
import { Component, Input, OnInit, Output, EventEmitter, Inject, SimpleChanges } from '@angular/core';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';

@Component({
  selector: 'learner-lesson-table-question-result',
  templateUrl: './table-question-result.component.html',
  styleUrls: ['./table-question-result.component.scss']
})
export class TableQuestionResultComponent implements OnInit {
  @Input() dataQuestion: QuestionTest[];
  @Input() dataQuestionBase: QuestionTest[];
  @Input() screenWidth;
  @Input() resultDoTest: ResultDoTestResponse;
  @Output() indexQuestion = new EventEmitter();
  elem: any;
  isFull = false;
  questions: QuestionTest[] = [];
  isReady: boolean = false;
  totalScore: number = 0;
  infoTest: UserContentTestInfo;

  constructor(@Inject(DOCUMENT) private document: any, public dialog: MatDialog, private _router: Router, private doTestService: DoTestService) {
    this.doTestService.fetchFromLocalStorage();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.totalScore = this.getTotalScore(this.dataQuestionBase);
    this.isReady = true;
  }
  ngOnDestroy(): void {
  }
  ngOnInit(): void {
    this.elem = document.documentElement;
    this.doTestService.getQuestions().subscribe(data => {
      this.questions = data;
      console.log("xxx", data, this.questions.length);
    });
    this.infoTest = this.doTestService.infoTest;
  }
  openFullscreen(): void {
    this.isFull = !this.isFull;
    if (this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
    } else if (this.elem.mozRequestFullScreen) {
      /* Firefox */
      this.elem.mozRequestFullScreen();
    } else if (this.elem.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      this.elem.webkitRequestFullscreen();
    } else if (this.elem.msRequestFullscreen) {
      /* IE/Edge */
      this.elem.msRequestFullscreen();
    }
  }
  /* Close fullscreen */
  closeFullscreen(): void {
    this.isFull = !this.isFull;
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
    }
  }
  SendIndexQuestion(id) {
    this.indexQuestion.emit(id);
  }
  openExamRuleDialog(): void {
    let dialogRef = this.dialog.open(RuleTestDialogComponent, { panelClass: 'exam-rule-dialog', data: this.doTestService.infoTest });
  }
  getResult(questionId: number): boolean {
    return this.doTestService.isCorrectQuestion(questionId, this.dataQuestionBase);
  }
  getTotalScore(questions: QuestionTest[]) {
    let totalScore = 0;
    questions.forEach((question) => {
      if (question.format == FormatQuestion.single) totalScore += question.score;
      else {
        question.questions.forEach((child) => {
          totalScore += child.score;
        })
      }
    })
    return Math.round(totalScore * 100) / 100;
  }
  isAnswered(questionId: number): boolean{
    let userExercise = this.resultDoTest.userExercises.find(x=>x.questionId == questionId && x.isAnswered);
    return (userExercise != undefined)
  }
}

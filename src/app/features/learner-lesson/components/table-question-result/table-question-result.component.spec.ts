import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableQuestionResultComponent } from './table-question-result.component';

describe('TableQuestionResultComponent', () => {
  let component: TableQuestionResultComponent;
  let fixture: ComponentFixture<TableQuestionResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableQuestionResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableQuestionResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

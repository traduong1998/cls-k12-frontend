import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-out-content-dialog',
  templateUrl: './out-content-dialog.component.html',
  styleUrls: ['./out-content-dialog.component.scss']
})
export class OutContentDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<OutContentDialogComponent>,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit(): void {
  }

  onClickOut() {
    this.router.navigateByUrl('learner/user-learn')
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutContentDialogComponent } from './out-content-dialog.component';

describe('OutContentDialogComponent', () => {
  let component: OutContentDialogComponent;
  let fixture: ComponentFixture<OutContentDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OutContentDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OutContentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

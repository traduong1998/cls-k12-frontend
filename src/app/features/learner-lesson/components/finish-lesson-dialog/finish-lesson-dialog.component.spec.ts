import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishLessonDialogComponent } from './finish-lesson-dialog.component';

describe('FinishLessonDialogComponent', () => {
  let component: FinishLessonDialogComponent;
  let fixture: ComponentFixture<FinishLessonDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinishLessonDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishLessonDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Identifiers } from '@angular/compiler';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services';

@Component({
  selector: 'app-finish-lesson-dialog',
  templateUrl: './finish-lesson-dialog.component.html',
  styleUrls: ['./finish-lesson-dialog.component.scss']
})
export class FinishLessonDialogComponent implements OnInit {
  userName: string;
  id:number;
  lessonName:string;
  isFromListContentView: boolean = false;
  constructor(private route: Router, public dialogRef: MatDialogRef<FinishLessonDialogComponent>, private _authService: AuthService, @Inject(MAT_DIALOG_DATA) data) {
    this.userName = this._authService.getTokenInfo().fullName;
    this.id = data.id;
    this.lessonName = data.name;
    this.isFromListContentView = data.isFromListContentView;
  }

  ngOnInit(): void {
  }

  exit() {
    this.dialogRef.close(false);
    this.route.navigateByUrl(`learner/user-learn`);
  }

  seeAgain() {
    if(!this.isFromListContentView){
      this.route.navigateByUrl(`learner/lessons/${this.id}/contents`);
    }
    this.dialogRef.close(true);
  }

}

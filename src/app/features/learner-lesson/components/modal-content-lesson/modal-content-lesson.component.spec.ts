import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalContentLessonComponent } from './modal-content-lesson.component';

describe('ModalContentLessonComponent', () => {
  let component: ModalContentLessonComponent;
  let fixture: ComponentFixture<ModalContentLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalContentLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalContentLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

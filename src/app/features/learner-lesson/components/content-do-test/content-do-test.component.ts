import { RestartDotestDialogComponent } from './../restart-dotest-dialog/restart-dotest-dialog.component';
import { OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { UserContentTestInfo } from './../../../../../../sdk/cls-k12-sdk-js/src/services/user-test/responses/user-content-test-info-response';
import { RuleTestDialogComponent } from './../rule-test-dialog/rule-test-dialog.component';
import { ResultTestComponent } from './../result-test/result-test.component';
import { Component, HostListener, Input, NgZone, OnInit, Output, ViewEncapsulation, EventEmitter } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NotificationComponent } from '../notification/notification.component';
import { DoTestService } from '../../Services/do-test-service.service';
import { UserExercise } from 'src/app/shared/modules/userexam/UserAnswer';
import { UserTestEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-test/endpoint/user-test-endpoint';
import { CongratulationComponent } from '../congratulation/congratulation.component';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'src/app/shared/services/message.service';
import { ProgressPlayerService } from 'src/app/features/learner-lesson/Services/progress-player.service';
import { debug } from 'console';
import { takeUntil } from 'rxjs/operators';
import { ResultTestDialogComponent } from '../result-test-dialog/result-test-dialog.component';

@Component({
  selector: 'learner-lesson-content-do-test',
  templateUrl: './content-do-test.component.html',
  styleUrls: ['./content-do-test.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContentDoTestComponent implements OnInit, OnChanges, OnDestroy {
  @Input() userContentTestId: number;
  @Input() testId: number;
  @Output() isChildReady = new EventEmitter<boolean>();
  itemsPerPage: number;
  userExercise: UserExercise;
  questionsView: QuestionTest[] = [];
  currentPage: number = 1;
  screenWidth;
  //isLoading = false;
  isStartDoTest = false;
  timeCountDown: Date;
  timeLeft: number;
  totalPage: number;
  isReady: boolean = false;
  userTestEndpoint: UserTestEndpoint;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  infoTest: UserContentTestInfo;
  destroy$: Subject<any> = new Subject(); 
  isSingleClickFinish: boolean = true;
  isDisableFinishButton: boolean = false;
  private intervalWritelog;
  private userAnswersInsert: UserExercise[];
  private userAnswersUpdate: UserExercise[];
  private isNeedUpdateBeforeDestroy: boolean = true;
  get questionType(): typeof Type {
    return Type;
  }
  get questionFormat(): typeof FormatQuestion {
    return FormatQuestion;
  }
  @HostListener('window:resize', ['$event'])
  darkMode = false;
  onResize(event?) {
    this.screenWidth = window.innerWidth;
  }

  @HostListener('window:unload', ['$event'])
  unloadHandler(event: Event) {
  }

  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHander(event: Event) {
    this.destroy$.next();
    if(this.intervalWritelog){
      clearInterval(this.intervalWritelog);
    }
    this.doTestServices.updateUserExercise(false, this.userContentTestId).then((res) => {
      if (res) {
        localStorage.setItem('updateBeforeUnloadHander',"true," + this.userContentTestId.toString() +","+ new Date().toLocaleDateString());
      }
      else {
        localStorage.setItem('updateBeforeUnloadHander', "false" + this.userContentTestId.toString() +","+ new Date().toLocaleDateString());
      }
    }).catch(() => {
      localStorage.setItem('updateBeforeUnloadHander', "error" + this.userContentTestId.toString() +","+ new Date().toLocaleDateString());
    })
  }
  pagingQuestions() {
    let currentItem = (this.currentPage - 1) * this.itemsPerPage
    return this.questionsView.splice(currentItem, currentItem + this.itemsPerPage - 1);
  }
  constructor(private zone: NgZone, private doTestServices: DoTestService, public dialog: MatDialog, private router: Router, private _snackBar: MatSnackBar,private _messageService:MessageService, private spinner: NgxSpinnerService, private progressPlayerService: ProgressPlayerService) {
    this.onResize();
    this.currentPage = 1;
    this.userTestEndpoint = new UserTestEndpoint();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if('userContentTestId' in changes){
      if(this.intervalWritelog){
        clearInterval(this.intervalWritelog);
      }
      if (this.isNeedUpdateBeforeDestroy) {
        this.doTestServices.updateUserExercise(false, this.userContentTestId).then((res) => {
          if (res) {
            localStorage.setItem('updateWhenDestroy', "true" + this.userContentTestId.toString() +","+ new Date().toLocaleDateString());
          }
        });
      }
      console.log("userContentTestId", this.userContentTestId);
      this.spinner.show();
      if(this.intervalWritelog){
        //console.log("Clear interval onChange", this.userContentTestId);
        clearInterval(this.intervalWritelog);
      }
      this.intervalWritelog = undefined;
      this.doTestServices.userUpdateAnswer$.pipe(takeUntil(this.destroy$)).subscribe((userExercises) => {
        this.userAnswersUpdate = userExercises;
      });
      this.doTestServices.userAnswer$.pipe(takeUntil(this.destroy$)).subscribe((userExercises) => {
        this.userAnswersInsert = userExercises;
      });

      setTimeout(() => {
        this.progressPlayerService.emitLightModeEvent(false);
      }, 0);

      if (localStorage.getItem('light_mode') === 'true'){
        this.darkMode = true;
      }
      this.doTestServices.isReady.pipe(takeUntil(this.destroy$)).subscribe((isReady) => {
        if (isReady) {
          this.StartDoExam();
          this.infoTest = this.doTestServices.infoTest;
          this.itemsPerPage = this.doTestServices.infoTest.displayPerPage;
          this.timeLeft = this.infoTest.timeRemaining;
          this.doTestServices.Questions$.subscribe(data => {
            this.questionsView = data;
            if (this.questionsView.length <= this.itemsPerPage) {
              this.totalPage = 1;
            }
            else {
              this.totalPage = Math.floor(this.questionsView.length / this.itemsPerPage);
              if (this.questionsView.length % this.itemsPerPage > 0) {
                this.totalPage++;
              };
            }
            this.isChildReady.emit(true);
            this.isReady = true;
          });
          this.intervalWritelog = setInterval(()=>{
            //console.log("Update:", this.userContentTestId);
            this.updateTest(this.userContentTestId)
          }, DoTestService.timeToWriteLog * 60 * 1000);
        }
      })
      this.spinner.hide();
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if(this.intervalWritelog){
      clearInterval(this.intervalWritelog);
    }
    if (this.isNeedUpdateBeforeDestroy) {
      this.doTestServices.updateUserExercise(false, this.userContentTestId).then((res) => {
        if (res) {
          localStorage.setItem('updateWhenDestroy', "true" + this.userContentTestId.toString() +","+ new Date().toLocaleDateString());
        }
      });
    }
    this.destroy$.next();
    if (this.darkMode){
      this.progressPlayerService.emitLightModeEvent(true);
    }
  }


  updateTest(userContentTestId: number): void {
    //console.log('gửi dữ liệu lên server', new Date().toLocaleString());
    this.doTestServices.updateUserExercise(false, userContentTestId).then((isUpdated) => {
      if (isUpdated) {
        this.doTestServices.clearUpdateAnswerOfUser();
        //console.log('đã clear danh sách câu hỏi được cập nhật');
      }
      else {
        //console.log("Có lỗi xảy ra");
      }
    });
  }

  StartDoExam = () => {
    this.isStartDoTest = true;
    console.log(this.isStartDoTest);
  }

  //chuyển trang khi bấm vào câu hỏi số ??? và scroll tới đó
  scrollToElement(index): void {

    //khi ở man hình nhỏ
    //tìm trang hiện tại của index
    var currentPageOfIndex = Math.floor((index + (this.itemsPerPage - 1)) / this.itemsPerPage);
    // nếu khác trang thì sẽ chuyển trang
    if (this.currentPage != currentPageOfIndex) {
      this.currentPage = currentPageOfIndex;
      setTimeout(() => {
        var element = document.getElementById("cauhoi_" + index);
        var elementPosition = element.offsetTop;
        element.scrollIntoView({ behavior: "smooth" });
      }, 300);
    } else {
      var element = document.getElementById("cauhoi_" + index);
      var elementPosition = element.offsetTop;
      element.scrollIntoView({ behavior: "smooth" });
    }
  }
  //chuyển lên đầu trang khi chuyển trang
  pageChanged(event): void {
    this.currentPage = event;
    let firstQuestionOfPage = ((this.currentPage - 1) * this.itemsPerPage) + 1;
    setTimeout(() => {
      var element = document.getElementById("cauhoi_" + firstQuestionOfPage);
      var elementPosition = element.offsetTop;
      element.scrollIntoView({ behavior: "smooth" });
    }, 100);
  }
  finishTest() {
    this.isSingleClickFinish = true;
    setTimeout(() => {
      if(this.isSingleClickFinish){
        this.isDisableFinishButton = true;
        let dialogRef = this.dialog.open(NotificationComponent, { data: { dataAnswer: this.doTestServices.getUserAnswer(), dataQuestion: this.questionsView } });
        dialogRef.afterClosed().subscribe((isSubmit) => {
          console.log("afterClosed dialog");
          if (isSubmit == true) {
            this.spinner.show();
            this.doTestServices.submitUserExercise(this.userContentTestId).then((finishDoTestResponse) => {
              this.isNeedUpdateBeforeDestroy = false;
              clearInterval(this.intervalWritelog);
              if (finishDoTestResponse.isFinishedLesson) {
                let dialogCongra = this.dialog.open(CongratulationComponent, { width: '100%', height: '95vh' });
                dialogCongra.afterClosed().subscribe(() => {
                  let dialogShowResult = this.dialog.open(ResultTestDialogComponent, {
                    width: '615px',
                    data: {finishDoTestResponse: finishDoTestResponse, contentName: this.infoTest.name, isAnswer: false }
                  });
  
                  dialogShowResult.afterClosed().subscribe(value => {
                    switch (value) {
                      case "RED":
                        this.userTestEndpoint.createUserContentTest(this.infoTest.lessonId, this.infoTest.contentId).then((res) => {
                          this.isDisableFinishButton = false;
                          this.router.navigate([`learner/lessons/${this.infoTest.lessonId}/contents/${this.infoTest.contentId}/start-test/${res.id}`]);
                        });
                        break;
                      case "VIE":
                        this.router.navigate([`learner/lessons/${this.doTestServices.infoTest.lessonId}/contents/${this.doTestServices.infoTest.contentId}/result/${this.userContentTestId}`])
                        break;
                      case "COF":
                        this.userTestEndpoint.confirmResultTest(null, this.userContentTestId).then(userLesson=>{
                          if (userLesson) {
                            this.router.navigate([`learner/lessons/${this.doTestServices.infoTest.lessonId}/contents/${this.doTestServices.infoTest.contentId}/detail-test`])
                          }
                        });
                        break;
                      default:
                      this.router.navigate([`learner/lessons/${this.doTestServices.infoTest.lessonId}/contents/${this.doTestServices.infoTest.contentId}/detail-test`])
                        break;
                    }
                  })
                })
              }
              else {
                let dialogShowResult = this.dialog.open(ResultTestDialogComponent, {
                  width: '615px',
                  data: {finishDoTestResponse: finishDoTestResponse, contentName: this.infoTest.name, isAnswer: false }
                });

                dialogShowResult.afterClosed().subscribe(value => {
                  switch (value) {
                    case "RED":
                      this.userTestEndpoint.createUserContentTest(this.infoTest.lessonId, this.infoTest.contentId).then((res) => {
                        this.isDisableFinishButton = false;
                        this.router.navigate([`learner/lessons/${this.infoTest.lessonId}/contents/${this.infoTest.contentId}/start-test/${res.id}`]);
                      });
                      break;
                    case "VIE":
                      this.router.navigate([`learner/lessons/${this.doTestServices.infoTest.lessonId}/contents/${this.doTestServices.infoTest.contentId}/result/${this.userContentTestId}`])
                      break;
                    case "COF":
                      this.userTestEndpoint.confirmResultTest(null, this.userContentTestId).then(userLesson=>{
                        if (userLesson) {
                          this.router.navigate([`learner/lessons/${this.doTestServices.infoTest.lessonId}/contents/${this.doTestServices.infoTest.contentId}/detail-test`])
                        }
                      });
                      break;
                    default:
                    this.router.navigate([`learner/lessons/${this.doTestServices.infoTest.lessonId}/contents/${this.doTestServices.infoTest.contentId}/detail-test`])
                      break;
                  }
                })
              }
              this.spinner.hide();
            })
              .catch((ERR) => {
                console.error(ERR);
                this.spinner.hide();
                this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại");
                this.isDisableFinishButton = false;
              });
          }else{
            this.isDisableFinishButton = false;
          }
        })
      }
    }, 250);
  }

  onDoubleClickFinish(): void {
    this.isSingleClickFinish = false;
  }
  finishTestOverTime(): void {
    this.spinner.show();
    this.doTestServices.submitUserExercise(this.userContentTestId).then((finishDoTestResponse) => {
      this.isNeedUpdateBeforeDestroy = false;
      clearInterval(this.intervalWritelog);
      if (finishDoTestResponse.isFinishedLesson) {
        let dialogCongra = this.dialog.open(CongratulationComponent, { width: '100%', height: '95vh' });
        dialogCongra.afterClosed().subscribe(() => {
          let dialogShowResult = this.dialog.open(ResultTestDialogComponent, {
            width: '615px',
            data: {finishDoTestResponse: finishDoTestResponse, contentName: this.infoTest.name, isAnswer: false }
          });

          dialogShowResult.afterClosed().subscribe(value => {
            switch (value) {
              case "RED":
                this.userTestEndpoint.createUserContentTest(this.infoTest.lessonId, this.infoTest.contentId).then((res) => {
                  this.isDisableFinishButton = false;
                  this.router.navigate([`learner/lessons/${this.infoTest.lessonId}/contents/${this.infoTest.contentId}/start-test/${res.id}`]);
                });
                break;
              case "VIE":
                this.router.navigate([`learner/lessons/${this.doTestServices.infoTest.lessonId}/contents/${this.doTestServices.infoTest.contentId}/result/${this.userContentTestId}`])
                break;
              case "COF":
                this.userTestEndpoint.confirmResultTest(null, this.userContentTestId).then(userLesson=>{
                  if (userLesson) {
                    this.router.navigate([`learner/lessons/${this.doTestServices.infoTest.lessonId}/contents/${this.doTestServices.infoTest.contentId}/detail-test`])
                  }
                });
                break;
              default:
              this.router.navigate([`learner/lessons/${this.doTestServices.infoTest.lessonId}/contents/${this.doTestServices.infoTest.contentId}/detail-test`])
                break;
            }
          })
        })
      }
      else {
        let dialogShowResult = this.dialog.open(ResultTestDialogComponent, {
          width: '615px',
          data: {finishDoTestResponse: finishDoTestResponse, contentName: this.infoTest.name, isAnswer: false }
        });

        dialogShowResult.afterClosed().subscribe(value => {
          switch (value) {
            case "RED":
              this.userTestEndpoint.createUserContentTest(this.infoTest.lessonId, this.infoTest.contentId).then((res) => {
                this.isDisableFinishButton = false;
                this.router.navigate([`learner/lessons/${this.infoTest.lessonId}/contents/${this.infoTest.contentId}/start-test/${res.id}`]);
              });
              break;
            case "VIE":
              this.router.navigate([`learner/lessons/${this.doTestServices.infoTest.lessonId}/contents/${this.doTestServices.infoTest.contentId}/result/${this.userContentTestId}`])
              break;
            case "COF":
              this.userTestEndpoint.confirmResultTest(null, this.userContentTestId).then(userLesson=>{
                if (userLesson) {
                  this.router.navigate([`learner/lessons/${this.doTestServices.infoTest.lessonId}/contents/${this.doTestServices.infoTest.contentId}/detail-test`])
                }
              });
              break;
            default:
            this.router.navigate([`learner/lessons/${this.doTestServices.infoTest.lessonId}/contents/${this.doTestServices.infoTest.contentId}/detail-test`])
              break;
          }
        })
      }
      this.spinner.hide();
    })
      .catch((ERR) => {
        console.error(ERR);
        this.spinner.hide();
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại");
      });
  }
  onNextPage(): void {
    if (this.currentPage < this.totalPage) {
      this.currentPage++;
      this.pageChanged(this.currentPage);
    }
    // this.currentPage<this.totalPage??this.currentPage++
  }
  onBackPage(): void {
    if (this.currentPage > 0) {
      this.currentPage--;
      this.pageChanged(this.currentPage);
    }
  }
  openExamRuleDialog(): void {
    let dialogRef = this.dialog.open(RuleTestDialogComponent, { panelClass: 'exam-rule-dialog', data: this.doTestServices.infoTest });
  }
  SortedQuestions() {
    return this.questionsView.sort((a, b) => { return a.order - b.order });
  }

}

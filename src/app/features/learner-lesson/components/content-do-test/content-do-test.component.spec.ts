import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentDoTestComponent } from './content-do-test.component';

describe('ContentDoTestComponent', () => {
  let component: ContentDoTestComponent;
  let fixture: ComponentFixture<ContentDoTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentDoTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentDoTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

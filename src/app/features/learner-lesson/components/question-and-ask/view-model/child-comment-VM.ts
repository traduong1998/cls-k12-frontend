import { ChildrenComment, CommentTeacher } from "sdk/cls-k12-sdk-js/src/services/comment/models/comment-teacher";

export class ChildCommentVM implements ChildrenComment {
    commentTeacherId: number;
    userId: number;
    lessonId: number;
    messageContent: string;
    parentId: number;
    createdDate: Date;

    userName: string;
    userAvartar: string;
    time:string

    static From(data: ChildrenComment, userName: string, userAvartar: string,time:string) {
        var m: ChildCommentVM = {
            commentTeacherId: data.commentTeacherId,
            userId: data.userId,
            lessonId: data.lessonId,
            messageContent: data.messageContent,
            parentId:data.parentId,
            createdDate: data.createdDate,
            userName: userName,
            userAvartar: userAvartar,
            time:time
        }
        return m;
    }
}
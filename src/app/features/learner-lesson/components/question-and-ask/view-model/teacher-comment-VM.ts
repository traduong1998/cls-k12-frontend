import { ChildrenComment, CommentTeacher } from "sdk/cls-k12-sdk-js/src/services/comment/models/comment-teacher";
import { ChildCommentVM } from "./child-comment-VM";

export class CommentTeacherVM implements CommentTeacher {
    commentTeacherId: number;
    userId: number;
    lessonId: number;
    messageContent: string;
    createdDate: Date;
    listCommentChildrend: ChildCommentVM[];

    userName: string;
    userAvartar: string;
    time: string;

    static From(data: CommentTeacher, listCommentChildrend: ChildCommentVM[], userName: string, userAvartar: string, time: string) {
        var m: CommentTeacherVM = {
            commentTeacherId: data.commentTeacherId,
            userId: data.userId,
            lessonId: data.lessonId,
            messageContent: data.messageContent,
            createdDate: data.createdDate,
            listCommentChildrend: listCommentChildrend,
            userName: userName,
            userAvartar: userAvartar,
            time: time
        }
        return m;
    }
}
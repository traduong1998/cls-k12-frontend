import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionAndAskComponent } from './question-and-ask.component';

describe('QuestionAndAskComponent', () => {
  let component: QuestionAndAskComponent;
  let fixture: ComponentFixture<QuestionAndAskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionAndAskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionAndAskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

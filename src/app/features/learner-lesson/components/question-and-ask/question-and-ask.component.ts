import { I } from '@angular/cdk/keycodes';
import { Component, Input, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { UserEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { CommentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/comment/comment-enpoint';
import { CommentUpdate } from 'sdk/cls-k12-sdk-js/src/services/comment/models/comment-update';
import { QuestionOfStudent } from 'sdk/cls-k12-sdk-js/src/services/comment/models/question-of-student';
import { TeacherOption } from 'sdk/cls-k12-sdk-js/src/services/user/models/teacher';
import { AuthService } from 'src/app/core/services';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { threadId } from 'worker_threads';
import { ChildCommentVM } from './view-model/child-comment-VM';
import { CommentTeacherVM } from './view-model/teacher-comment-VM';

@Component({
  selector: 'app-question-and-ask',
  templateUrl: './question-and-ask.component.html',
  styleUrls: ['./question-and-ask.component.scss']
})
export class QuestionAndAskComponent implements OnInit {
  @Input() isDarkMode;
  @Input() tab;
  isFirstLoad = true;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  commentEndpoint: CommentEndpoint;
  userEndpoint: UserEndpoint;
  commentTeacherVM: CommentTeacherVM[] = [];
  childCommentVM: ChildCommentVM[] = [];
  userOption: TeacherOption[];

  teacherOption: TeacherOption[];
  question: string;
  commentUpdate: string;
  lessonId: number;
  userId: number;
  isEdit = false;
  isHide = false;
  select: number;
  userAvartar: string;
  constructor(private _route: ActivatedRoute, private _authService: AuthService, private _snackBar: MatSnackBar) {
    this.commentEndpoint = new CommentEndpoint();
    this.userEndpoint = new UserEndpoint();
    this.lessonId = + this._route.snapshot.paramMap.get("lessonId");
    this.userId = +this._authService.getTokenInfo().userId;
    this.userAvartar = this._authService.getTokenInfo().avatar;

    if (!this.userAvartar) {
      this.userAvartar = '/assets/images/users/user-avartar.png'
    }
  }

  ngOnInit(): void {
    
  }
  ngOnChanges(): void {
    if (this.isFirstLoad) {
      if (this.tab == 2) {
        this.getCommentTeacher();
        this.isFirstLoad = false;
      }
    }
  }


  getCommentTeacher() {
    Promise.resolve()
      .then(() => {
        return this.commentEndpoint.getCommentTeacher(this.lessonId, "CRE", "DESC", 1, 5);
      })
      .then((res) => {
        if (res.length < 5) {
          this.isHide = true;
        }
        else {
          this.isHide = false;
        }

        let listStudentInfo = res.map(x => x.userId);
        Promise.all([
          this.userEndpoint.GetListUserByIds(listStudentInfo).then(r => {
            this.userOption = r
          })
        ]).then(() => {
          // lặp lấy thông tin của giáo viên
          res.forEach((item) => {
            let listTeacher = item.listCommentChildrend.map(x => x.userId);
            var b = moment.utc(item.createdDate).local().lang("vi");
            var timeALl = b.fromNow();
            this.userEndpoint.GetListUserByIds(listTeacher).then(r => {
              this.teacherOption = r
              //gán nó vô
              this.childCommentVM = [];
              item.listCommentChildrend.forEach((it) => {
                var a = moment.utc(it.createdDate).local().lang("vi");
                var time = a.fromNow();

                let teacher = this.teacherOption.find(x => x.id == it.userId);
                if (!teacher.avatar) {
                  teacher.avatar = '/assets/images/users/user-avartar.png'
                }
                this.childCommentVM.push(ChildCommentVM.From(it, teacher.name, teacher.avatar, time));
              });
            }).then(() => {
              // gán thông tin giáo viên
              let user = this.userOption.find(x => x.id == item.userId);
              if (!user.avatar) {
                user.avatar = '/assets/images/users/user-avartar.png'
              }
              this.commentTeacherVM.push(CommentTeacherVM.From(item, this.childCommentVM, user.name, user.avatar, timeALl));
            })
          })
        })
      })
  }

  sendQuestion() {
    if (this.question.trim() == '') {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Bạn chưa nhập nội dung',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      this.question = ''
    } else {
      let questionOfStudent: QuestionOfStudent = { lessonId: this.lessonId, messageContent: this.question, parentId: null }
      this.commentEndpoint.createStudentAskQuestion(questionOfStudent).then(res => {
        this.question = ''
        this.commentTeacherVM = [];
        this.getCommentTeacher();
      })
    }
  }
  updateCommentClick(commentId, content) {
    this.isEdit = true;
    this.select = commentId;
    this.commentUpdate = content;
  }

  updateComment(commentId) {
    if (this.commentUpdate.trim() == '') {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Bạn chưa nhập nội dung',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      this.commentUpdate = ''
    } else {
      let commentUpdate: CommentUpdate = { id: commentId, messageContent: this.commentUpdate }
      this.commentEndpoint.updateCommentTeacher(commentUpdate).then(res => {
        this.commentTeacherVM = [];
        this.getCommentTeacher();
        this.isEdit = false;
        this.pageCurrrent = 1;
      })
    }
  }

  deleteComment(commentId) {
    this.commentEndpoint.deleteCommentTeacher(commentId).then(res => {
      this.commentTeacherVM = [];
      this.pageCurrrent = 1;
      this.getCommentTeacher();
    })
  }

  pageCurrrent: number = 1;

  seeMoreParent() {
    this.pageCurrrent = this.pageCurrrent + 1;
    Promise.resolve()
      .then(() => {
        return this.commentEndpoint.getCommentTeacher(this.lessonId, "CRE", "DESC", this.pageCurrrent, 5);
      })
      .then((res) => {
        if (res.length < 5) {
          this.isHide = true;
        } else {
          this.isHide = false;
        }

        let listStudentInfo = res.map(x => x.userId);
        Promise.all([
          this.userEndpoint.GetListUserByIds(listStudentInfo).then(r => {
            this.userOption = r
          })
        ]).then(() => {
          // lặp lấy thông tin của giáo viên
          res.forEach((item) => {
            let listTeacher = item.listCommentChildrend.map(x => x.userId);

            var b = moment.utc(item.createdDate).local().lang("vi");
            var timeAll = b.fromNow();


            this.userEndpoint.GetListUserByIds(listTeacher).then(r => {
              this.teacherOption = r
              //gán nó vô
              this.childCommentVM = [];
              item.listCommentChildrend.forEach((it) => {
                var a = moment.utc(it.createdDate).local().lang("vi");
                var time = a.fromNow();

                let teacher = this.teacherOption.find(x => x.id == it.userId);

                if (!teacher.avatar) {
                  teacher.avatar = '/assets/images/users/user-avartar.png'
                }
                this.childCommentVM.push(ChildCommentVM.From(it, teacher.name, teacher.avatar, time));
              });
            }).then(() => {
              // gán thông tin giáo viên
              let user = this.userOption.find(x => x.id == item.userId);
              if (!user.avatar) {
                user.avatar = '/assets/images/users/user-avartar.png'
              }
              this.commentTeacherVM.push(CommentTeacherVM.From(item, this.childCommentVM, user.name, user.avatar, timeAll));
            })
          })
        })
      })
  }
}

import { AnswerOfUser } from './../../../../shared/modules/userexam/UserAnswer';
import { DoTestService } from './../../Services/do-test-service.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { ArrayAnswer } from 'src/app/features/user-examination/Interfaces/ArrayAnswer';
import { UserExercise } from 'src/app/shared/modules/userexam/UserAnswer';

@Component({
  selector: 'learner-lesson-missing-word1',
  templateUrl: './missing-word1.component.html',
  styleUrls: ['./missing-word1.component.scss']
})
export class MissingWord1Component implements OnInit {
  @Input() dataQuestion: QuestionTest;
  @Input() userAnswer;
  @Input() isResult;
  @Input() parentId;
  @Input() isGroup;
  @Input() order;
  @Input() isPublicAnswer:boolean;
  @Output() dataOfGroup = new EventEmitter();
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  dapAn: number[] = [];
  cauhoi;
  isTrueQuestion;
  AnswerOfUserGroup: UserExercise;
  arrayAnswer: ArrayAnswer[] = [];
  contentQuestion;
  isFlag = false;
  format = FormatQuestion;
  numberOfFillBlank: number = 0;
  constructor(private doTestService: DoTestService) { }
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.updateFillBlankQuestion();
    }, 100);
  }
  ngOnInit(): void {
    //gan cau hoi
    this.isGroup = this.dataQuestion.format;
    this.cauhoi = this.dataQuestion.content;
    //danh sách dáp dan của câu hỏi hiện tại
    this.numberOfFillBlank = (this.dataQuestion.content.match(/class="Blanking/g) || []).length;
    for (let index = 0; index < this.numberOfFillBlank; index++) {
      this.dapAn.push(index + 1)
    }

    this.dataQuestion.answers.forEach(
      element => {
        this.arrayAnswer.push({ order: 0, content: element });
      }
    )
    var oldData = this.doTestService.getAnswerOfQuestion(this.dataQuestion.id);
    if (oldData) {
      this.isFlag = oldData.isMark;
    }
    if (oldData && oldData.userAnswers.length > 0) {
      for (let index = 0; index < this.arrayAnswer.length; index++) {
        if (oldData.userAnswers.some(x => x.answerId == this.arrayAnswer[index].content.id)) {
          let oldAnswer = oldData.userAnswers.find(x => x.answerId == this.arrayAnswer[index].content.id)
          this.arrayAnswer[index].order = oldAnswer.fillInPosition;
        }
      }
    }
    if (this.isResult == true) {
      console.log('câu hỏi', this.dataQuestion);
      this.isAnswerTrue();
    }
  }
  isMark() {
    this.isFlag = !this.isFlag;
    this.doTestService.changeTickUserExercise(this.dataQuestion.id);
  }

  ChosseRightAnswer(ans, viTri) {
    //reset lai thông tin như ban đâu
    this.cauhoi = this.dataQuestion.content;
    for (let i = 0; i < this.dapAn.length; i++) {
      if (this.arrayAnswer[i].order != 0) {
        $(`.Blanking:eq(` + (i) + `)`).replaceWith(`<span class='Blanking'>` + `(` + (i + 1) + `)` + `</span>`);
      }
    }

    let index = parseInt(ans.value);
    // sắp xếp lại vị trí
    this.arrayAnswer.map((ch, i) => {
      if (ch.order == parseInt(ans.value)) {
        this.arrayAnswer[i].order = 0;
      }
    });
    this.arrayAnswer[viTri].order = parseInt(ans.value);
    let AnswerOfUser: AnswerOfUser[] = [];
    // điền nội dung theo vị trí
    for (let i = 0; i < this.arrayAnswer.length; i++) {
      if (this.arrayAnswer[i].order != 0) {
        let element = $(`#` + this.dataQuestion.id + ` span .Blanking:eq(` + (this.arrayAnswer[i].order - 1) + `)`)
        element.replaceWith(`<span class='Blanking'>` + this.arrayAnswer[i].content.content + `</span>`);
        // let b = $(`#` + this.dataQuestion.id +'p span');
        // b.attr('class', 'Blanking');
        AnswerOfUser.push({ answerId: this.arrayAnswer[i].content.id, answer: this.arrayAnswer[i].content.trueAnswer, fillInPosition: this.arrayAnswer[i].order })
      }
      this.doTestService.onChangeUserExercise(this.dataQuestion.id, AnswerOfUser);
      if (this.parentId != undefined) {
        this.doTestService.updateIsAnsweredGroup(this.dataQuestion, this.parentId);
      }
    }
  }
  isAnswerTrue() {
    let count = 0;
    console.log(this.arrayAnswer);
    let userAnswers = this.userAnswer.userAnswers;
    if (userAnswers && userAnswers.length > 0) {
      for (let index = 0; index < this.dataQuestion.answers.length; index++) {
        let ans = this.dataQuestion.answers[index];
        let userans = userAnswers.find(x => x.answerId == ans.id);
        console.log('ans', ans)
        if (userans) {
          let data = this.arrayAnswer.find(x => x.content.id == userans.answerId)
          if (userans.fillInPosition == ans.trueAnswer) {
            count++;
            data.isTrue = true;
          } else {
            data.isTrue = false;
          }
        }
      }
    } else {
      this.isTrueQuestion = false;
    }
    if (count == this.numberOfFillBlank) {
      //console.log('đúng')
      this.isTrueQuestion = true;
    } else {
      //console.log('sai')
      this.isTrueQuestion = false;
    }
  }

  updateFillBlankQuestion() {
    for (let i = 0; i < this.arrayAnswer.length; i++) {
      if (this.arrayAnswer[i].order != 0) {
        $(`#` + this.dataQuestion.id + ` .Blanking:eq(` + (this.arrayAnswer[i].order - 1) + `)`).replaceWith(`<span class='Blanking'>` + this.arrayAnswer[i].content.content + `</span>`);
        // let b = $('p span');
        // b.attr('class', 'Blanking');
      }
    }
  }
}

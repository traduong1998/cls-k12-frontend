import { DoTestService } from './../../Services/do-test-service.service';
import { Component, Input, OnInit, EventEmitter, Output, OnChanges, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ResultDoTestResponse } from 'sdk/cls-k12-sdk-js/src/services/user-test/responses/result-do-test-response';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { UserContentTestInfo } from 'sdk/cls-k12-sdk-js/src/services/user-test/responses/user-content-test-info-response';

@Component({
  selector: 'learner-lesson-table-question-result-mobile',
  templateUrl: './table-question-result-mobile.component.html',
  styleUrls: ['./table-question-result-mobile.component.scss']
})
export class TableQuestionResultMobileComponent implements OnInit, OnChanges {
  @Input() dataQuestion;
  @Input() dataQuestionBase: QuestionTest[];
  @Input() currentPage;
  @Input() totalPage;
  @Input() screenWidth;
  @Input() examSubjectStudentId: number;
  @Input() resultDoTest: ResultDoTestResponse;
  @Output() indexQuestion = new EventEmitter();
  @Output() pageIndex = new EventEmitter();
  isShow = false;
  totalScore: number = 0;
  infoTest: UserContentTestInfo;
  questions: QuestionTest[] = [];

  constructor(private doTestService: DoTestService, private dialog: MatDialog, private _router: Router) {
    this.doTestService.fetchFromLocalStorage();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.totalScore = this.getTotalScore(this.dataQuestionBase);
  }

  ngOnInit(): void {
    this.doTestService.getQuestions().subscribe(data => {
      this.questions = data;
      console.log("xxx", data, this.questions.length);
    });
    this.infoTest = this.doTestService.infoTest;
  }

  ngOnDestroy(): void {
  }
  show() {
    this.isShow = !this.isShow;
  }
  getResult(questionId: number): boolean {
    return this.doTestService.isCorrectQuestion(questionId, this.dataQuestionBase);
  }
  SendIndexQuestion(id) {
    this.indexQuestion.emit(id);
    this.show();
  }

  NextPage() {
    if (this.currentPage < this.dataQuestion.length) {
      this.currentPage = this.currentPage + 1;
    }
    else {
      this.currentPage = this.dataQuestion.length
    }
    this.pageIndex.emit(this.currentPage);
    //console.log("ssss",this.dataQuestion.length);
  }
  PreviousPage() {
    if (this.currentPage <= 1) {
      this.currentPage = 1;
    } else {
      this.currentPage = this.currentPage - 1;
    }

    this.pageIndex.emit(this.currentPage);
    console.log("ssss", this.currentPage);
  }
  exitButtonHandleClick(): void {
    this._router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`])
  }

  getTotalScore(questions: QuestionTest[]) {
    let totalScore = 0;
    questions.forEach((question) => {
      if (question.format == FormatQuestion.single) totalScore += question.score;
      else {
        question.questions.forEach((child) => {
          totalScore += child.score;
        })
      }
    })
    return Math.round(totalScore * 100) / 100;
  }

}

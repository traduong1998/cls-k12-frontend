import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RuleTestDialogComponent } from './rule-test-dialog.component';

describe('RuleTestDialogComponent', () => {
  let component: RuleTestDialogComponent;
  let fixture: ComponentFixture<RuleTestDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RuleTestDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RuleTestDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserContentTestInfo } from 'sdk/cls-k12-sdk-js/src/services/user-test/responses/user-content-test-info-response';

@Component({
  selector: 'learner-lesson-rule-test-dialog',
  templateUrl: './rule-test-dialog.component.html',
  styleUrls: ['./rule-test-dialog.component.scss']
})
export class RuleTestDialogComponent implements OnInit {
  public lockScreenTest: string = '';
  public publicScore: string = '';
  public publicAnswer: string = '';
  constructor(private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public info: UserContentTestInfo) {
    // this.lockScreenTest = ' Không được phép sử dụng các ứng dụng, chương trình khác';
    this.publicScore = info.isPublicScore ? 'Được phép xem điểm sau khi nộp bài' : ' Không được phép xem điểm sau khi nộp bài ';
    this.publicAnswer = info.isPublicAnswer ? 'Được phép xem đáp án đúng sau khi nộp bài' : 'Không được phép xem đáp án đúng sau khi nộp bài';
  }

  ngOnInit(): void {
  }
}

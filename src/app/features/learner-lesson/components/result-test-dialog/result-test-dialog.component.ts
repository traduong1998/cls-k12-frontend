import { FinishDoTestResponse } from './../../../../../../sdk/cls-k12-sdk-js/src/services/learner-lesson/responses/finish-do-test-response';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-result-test-dialog',
  templateUrl: './result-test-dialog.component.html',
  styleUrls: ['./result-test-dialog.component.scss']
})
export class ResultTestDialogComponent implements OnInit {

  constructor(public MatDialogRef: MatDialogRef<ResultTestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  canToRedo(config: FinishDoTestResponse): boolean {
    if (config.numberOfRemainingAttempts != null) {
      if (config.numberOfRemainingAttempts > 0) {
        return true;
      }
    }
    return false;
  }

}

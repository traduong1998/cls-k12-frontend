import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TestInfoResponse } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/responses/test-info-response';
import { DialogData } from 'src/app/features/learn/intefaces/dialogData';

@Component({
  selector: 'app-restart-dotest-dialog',
  templateUrl: './restart-dotest-dialog.component.html',
  styleUrls: ['./restart-dotest-dialog.component.scss']
})
export class RestartDotestDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<RestartDotestDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public testInfoStudent: TestInfoResponse) { }

  ngOnInit(): void {
  }

  canToRedo(config: TestInfoResponse): boolean {
    if (config.theMaximumNumberToRedo != null) {
      if (config.numberOfRemainingAttempts > 0) {
        return true;
      }
    }
    return false;
  }

}

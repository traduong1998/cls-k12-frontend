import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestartDotestDialogComponent } from './restart-dotest-dialog.component';

describe('RestartDotestDialogComponent', () => {
  let component: RestartDotestDialogComponent;
  let fixture: ComponentFixture<RestartDotestDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RestartDotestDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestartDotestDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

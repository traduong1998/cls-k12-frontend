import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DecriptionContentComponent } from './decription-content.component';

describe('DecriptionContentComponent', () => {
  let component: DecriptionContentComponent;
  let fixture: ComponentFixture<DecriptionContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DecriptionContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DecriptionContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

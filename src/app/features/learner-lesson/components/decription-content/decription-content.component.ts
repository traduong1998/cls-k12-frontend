import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from 'src/app/features/learn/intefaces/dialogData';

@Component({
  selector: 'learn-decription-content',
  templateUrl: './decription-content.component.html',
  styleUrls: ['./decription-content.component.scss']
})
export class DecriptionContentComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DecriptionContentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit(): void {
    if (this.data.trim().length == 0) {
      this.data = null;
    }
  }

  public onNoButtonHandleClick() {
    this.dialogRef.close();
  }
}

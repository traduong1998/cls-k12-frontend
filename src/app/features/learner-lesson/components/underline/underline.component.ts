import { DoTestService } from './../../Services/do-test-service.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';

@Component({
  selector: 'learner-lesson-underline',
  templateUrl: './underline.component.html',
  styleUrls: ['./underline.component.scss']
})
export class UnderlineComponent implements OnInit {
  @Input() dataQuestion: QuestionTest;
  @Input() userAnswer;
  @Input() isResult;
  @Input() parentId;
  @Input() order;
  @Input() isPublicAnswer:boolean;
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  @Input() isGroup;
  isTrueQuestion;
  selectedAnswer: number;
  isFlag = false;
  format = FormatQuestion;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  constructor(private doTestService: DoTestService) { }
  ngOnInit(): void {
    this.isGroup = this.dataQuestion.format;
    var oldData = this.doTestService.getAnswerOfQuestion(this.dataQuestion.id);
    if (oldData) {
      this.isFlag = oldData.isMark;
    }
    if (oldData && oldData.userAnswers.length > 0) {
      this.selectedAnswer = oldData.userAnswers[0].answerId;
    }
    if (this.isResult) {
      this.isAnswerTrue();
    }
  }
  isMark() {
    this.isFlag = !this.isFlag;
    this.doTestService.changeTickUserExercise(this.dataQuestion.id);
  }
  isAnswerTrue() {
    let userAnswers = this.userAnswer.userAnswers;
    if (userAnswers && userAnswers.length > 0) {
      let answerId = userAnswers[0].answerId;
      if (this.dataQuestion.answers.find(x => x.id == answerId).trueAnswer != 1) {
        //alert('gạch chân sai')
        this.isTrueQuestion = false;
      } else {
        //alert('gạch chân đúng')
        this.isTrueQuestion = true;
      }
    } else {
      // alert('gạch chân sai ( chưa làm)')
      this.isTrueQuestion = false;
    }
  }
  changeSelectAnswer(id) {
    this.doTestService.onChangeUserExercise(this.dataQuestion.id, [{ answerId: id, answer: 1 }]);
    if (this.parentId != undefined) {
      this.doTestService.updateIsAnsweredGroup(this.dataQuestion, this.parentId);
    }
  }

}

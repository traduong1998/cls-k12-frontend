import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'learner-lesson-result-test',
  templateUrl: './result-test.component.html',
  styleUrls: ['./result-test.component.scss']
})
export class ResultTestComponent implements OnInit {

  mark: number = 0;
  markDoExam: number = 0;
  examSubjectStudentId: number;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private router: Router) {
  }
  ngOnInit(): void {
    
  }
  showResult(): void {
    this.router.navigateByUrl(`dashboard/user-examination/${this.data}/result`);
  }
}

import { DoTestService } from './../../Services/do-test-service.service';
import { AnswerOfUser, UserExercise } from './../../../../shared/modules/userexam/UserAnswer';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';

@Component({
  selector: 'learner-lesson-missing-word2',
  templateUrl: './missing-word2.component.html',
  styleUrls: ['./missing-word2.component.scss']
})
export class MissingWord2Component implements OnInit {
  @Input() dataQuestion:QuestionTest;
  @Input() userAnswer;
  @Input() isResult;
  @Input() isPublicAnswer:boolean;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  dataAnswers;
  isFlag = false;
  isTrueQuestion;
  answerOfStudent: number[] = [];
  AnswerOfUser: AnswerOfUser[] = [];
  constructor(private doTestService: DoTestService) { }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.updateFillBlankQuestion();
    }, 100);
  }
  ngOnInit(): void {
    this.dataAnswers = this.dataQuestion.answers.reduce((r, a) => {
      r[a.groupOfFillBlank2] = [...r[a.groupOfFillBlank2] || [], a];
      return r;
    }, {});
    for (let index = 0; index < this.groupAnswers().length; index++) {
      this.answerOfStudent.push(-1);
    }
    // load lại câu hỏi cũ

    var oldData = this.doTestService.getAnswerOfQuestion(this.dataQuestion.id);
    if (oldData) {
      this.isFlag = oldData.isMark;
    }
    if (oldData && oldData.userAnswers.length > 0) {
      this.AnswerOfUser = oldData.userAnswers;
      for (let index = 0; index < this.groupAnswers().length; index++) {

        oldData.userAnswers.map((ch, i) => {
          if (ch.fillInPosition == index + 1) {
            this.answerOfStudent[index] = ch.answerId;
          }
        });
      }
    }
    if (this.isResult) {
      this.isAnswerTrue();
    }
    //console.log("fgdshfdsf", this.groupAnswers())
  }
  isMark() {
    this.isFlag = !this.isFlag;
    this.doTestService.changeTickUserExercise(this.dataQuestion.id);
  }

  SortedAnswers() {
    return this.dataQuestion.answers.sort((a, b) => a.groupOfFillBlank2 > b.groupOfFillBlank2 ? 1 : a.groupOfFillBlank2 < b.groupOfFillBlank2 ? -1 : 0);
  }
  groupAnswers() {
    return [...new Set(this.SortedAnswers().map(item => item.groupOfFillBlank2))]
  }
  answersOfGroup(index) {
    return this.dataQuestion.answers.filter(x => x.groupOfFillBlank2 == index);
  }
  changeSelectAnswer(index) {
    let id = this.answerOfStudent[index];
    let currentAnswer = this.dataQuestion.answers.find(x => x.id == id);;
    if (this.AnswerOfUser.some(x => x.fillInPosition == (index + 1)) == true) {
      this.AnswerOfUser.map((ans, i) => {
        if (ans.fillInPosition == index + 1) {
          ans.answerId = currentAnswer.id;
        }
      });
    } else {
      this.AnswerOfUser.push({ answerId: currentAnswer.id, answer: 1, fillInPosition: index + 1 });
    }
    $(`#` + this.dataQuestion.id + ` .Blanking2:eq(` + index + `)`).replaceWith(`<span class="Blanking2">` + currentAnswer.content + `</span>`);
    this.doTestService.onChangeUserExercise(this.dataQuestion.id, this.AnswerOfUser);
  }
  updateFillBlankQuestion() {
    for (let i = 0; i < this.answerOfStudent.length; i++) {
      if (this.answerOfStudent[i] != -1) {
        let currentAnswer = this.dataQuestion.answers.find(x => x.id == this.answerOfStudent[i]);;
        $(`#` + this.dataQuestion.id + ` .Blanking2:eq(` + i + `)`).replaceWith(`<span class="Blanking2">` + currentAnswer.content + `</span>`);
      }
    }
  }
  isAnswerTrue() {
    let count = 0;
    let userAnswers = this.userAnswer.userAnswers;
    if (userAnswers && userAnswers.length > 0) {
      userAnswers.forEach(userans => {
        let answer = this.dataQuestion.answers.find(x => x.id == userans.answerId);
        if (answer.trueAnswer == 1 && answer.groupOfFillBlank2 == userans.fillInPosition) {
          count++;
        }
      });
    } else {
      //console.log('chưa làm')
      this.isTrueQuestion = false;
    }
    if (count == this.groupAnswers().length) {
      this.isTrueQuestion = true;
    } else {
      this.isTrueQuestion = false;
    }
  }
}

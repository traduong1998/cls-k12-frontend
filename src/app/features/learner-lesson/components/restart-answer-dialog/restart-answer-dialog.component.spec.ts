import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RestartAnswerDialogComponent } from './restart-answer-dialog.component';

describe('RestartAnswerDialogComponent', () => {
  let component: RestartAnswerDialogComponent;
  let fixture: ComponentFixture<RestartAnswerDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RestartAnswerDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RestartAnswerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

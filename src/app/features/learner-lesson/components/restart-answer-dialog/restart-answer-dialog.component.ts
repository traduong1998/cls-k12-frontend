import { TestInfoResponse } from './../../../../../../sdk/cls-k12-sdk-js/src/services/learner-lesson/responses/test-info-response';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-restart-answer-dialog',
  templateUrl: './restart-answer-dialog.component.html',
  styleUrls: ['./restart-answer-dialog.component.scss']
})
export class RestartAnswerDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<RestartAnswerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public testInfoStudent: TestInfoResponse) { }

  ngOnInit(): void {
  }

  canToRedo(config: TestInfoResponse): boolean {
    if (config.theMaximumNumberToRedo != null) {
      if (config.numberOfRemainingAttempts > 0) {
        return true;
      }
    }
    return false;
  }

}

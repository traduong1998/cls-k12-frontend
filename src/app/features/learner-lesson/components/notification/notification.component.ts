import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';

@Component({
  selector: 'learner-lesson-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  check = false;
  constructor(private dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    debugger
    let a = this.countQuestions(this.data.dataQuestion);
    if (this.data.dataAnswer.length == this.countQuestions(this.data.dataQuestion)) {
      this.check = true;
    }
  }
  countQuestions(questions: any): number {
    let count = 0;
    questions.forEach(question => {
      if (question.format == FormatQuestion.single) count++;
      else {
        count++;
        question.questions.forEach((child) => count++);
      }
    });
    return count;
  }

}

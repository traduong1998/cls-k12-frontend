import { Component, Input, OnInit } from '@angular/core';
import { Comment } from 'sdk/cls-k12-sdk-js/src/services/comment/models/comment';
import { CommentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/comment/comment-enpoint';
import { ActivatedRoute } from '@angular/router';
import { CommentVM } from './view-model/coment-VM';
import { UserEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { TeacherOption } from 'sdk/cls-k12-sdk-js/src/services/manage/models/teacher';
import { AuthService } from 'src/app/core/services';
import { CommentUpdate } from 'sdk/cls-k12-sdk-js/src/services/comment/models/comment-update';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { StudentFilter } from 'sdk/cls-k12-sdk-js/src/services/comment/requests/studentFilter';
import * as moment from 'moment';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';

@Component({
  selector: 'app-chat-tab-lesson',
  templateUrl: './chat-tab-lesson.component.html',
  styleUrls: ['./chat-tab-lesson.component.scss']
})
export class ChatTabLessonComponent implements OnInit {
  @Input() isDarkMode;
  @Input() tab;
  isFisrtLoad = true;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  commentContent: string;
  commentUpdate: string;
  commentChildUpdate: string;
  commentContentChild: string;
  commentEndpoint: CommentEndpoint;
  userEndpoint: UserEndpoint;
  commentResponse: CommentVM[] = [];
  userOption: TeacherOption[];
  commentChildResponse: CommentVM[] = [];


  listChild: any;
  commet: Comment
  lessonId: number;
  userId: number;
  isEdit = false;
  isEditChild = false;
  isAnswer = false;
  isHideParent = false;
  select: number;
  selectChild: number
  answerSelect: number;
  pageNumber: number = 1;
  pageNumberChild: number = 0;
  userAvartar: string;
  constructor(private _route: ActivatedRoute, private _authService: AuthService, private _snackBar: MatSnackBar) {
    this.commentEndpoint = new CommentEndpoint();
    this.userEndpoint = new UserEndpoint();
    this.lessonId = + this._route.snapshot.paramMap.get("lessonId");

    this.userId = +this._authService.getTokenInfo().userId;
    this.userAvartar = this._authService.getTokenInfo().avatar;

    if (!this.userAvartar) {
      this.userAvartar = '/assets/images/users/user-avartar.png'
    }
  }

  ngOnInit(): void {

  }
  ngOnChanges(): void {
    if (this.isFisrtLoad) {
      if (this.tab == 1) {
        this.getCommet();
        this.isFisrtLoad = false;
      }
    }
  }
  getCommet() {
    Promise.resolve().
      then(() => {
        let studentPaginationFilter: StudentFilter = {
          pageNumber: 0,
          sizeNumber: 5,
          getCount: true,
          lessonId: this.lessonId,
          sortDirection: 'DESC',
          SortField: 'CRE',
        }
        this.pageNumber = 1;

        return this.commentEndpoint.getCommentParent(studentPaginationFilter)
      })
      .then(res => {

        if (res.items.length < 5) {
          this.isHideParent = true;
        }
        else {
          this.isHideParent = false;
        }

        let listUser = res.items.map(x => x.userId);
        Promise.all([
          this.userEndpoint.GetListUserByIds(listUser).then(r => {
            this.userOption = r
          })
        ]).then(() => {
          res.items.forEach((item) => {
            var a = moment.utc(item.createdDate).local().lang("vi");
            var time = a.fromNow();
            let user = this.userOption.find(x => x.id == item.userId);

            if (!user.avatar) {
              user.avatar = '/assets/images/users/user-avartar.png'
            }
            this.commentResponse.push(CommentVM.From(item, user.name, user.avatar, time));
          })
        })
      })
  }

  answer(commentId) {
    this.isAnswer = true;
    this.answerSelect = commentId;
  }

  sendComment() {
    if (this.commentContent.trim() == '') {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Bạn chưa nhập nội dung',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      this.commentContent='';
    } else {
      let comment: Comment = { lessonId: this.lessonId, messageContent: this.commentContent, parentId: null }
      this.commentEndpoint.createComment(comment).then(res => {
        this.commentContent = '';
        this.commentResponse = [];
        this.getCommet();
      });
    }
  }
  saveChildComment(commentId: number) {
    if (this.commentContentChild.trim() == '') {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Bạn chưa nhập nội dung trả lời',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      this.commentContentChild = '';
    } else {
      let comment: Comment = { lessonId: this.lessonId, messageContent: this.commentContentChild, parentId: commentId }
      this.commentEndpoint.createComment(comment).then(res => {
        this.commentContentChild = '';
        this.seeMore(commentId, 100);
      });
    }
  }

  seeMore(commentId: number, totalPage?: number) {
    let page = this.commentResponse.find(x => x.commentStudentId == commentId).pageChild + 1;
    if (totalPage == 100) {
      totalPage = 100
      page = 1;
    } else {
      totalPage = 3;
    }

    Promise.resolve().
      then(() => {
        if (totalPage == 100) {
          this.listChild = [];
          this.commentResponse.find(x => x.commentStudentId == commentId).listChild = [];
        }
        return this.commentEndpoint.getCommentChild(this.lessonId, commentId, "CRE", "DESC", page, totalPage);
      })
      .then((res) => {

        if (res.items.length > 0) {
          this.commentResponse.find(x => x.commentStudentId == commentId).pageChild = this.commentResponse.find(x => x.commentStudentId == commentId).pageChild + 1;
          this.commentResponse.find(x => x.commentStudentId == commentId).isHideChild = false;
        }
        else {
          this.commentResponse.find(x => x.commentStudentId == commentId).isHideChild = true;
        }
        let listUser = res.items.map(x => x.userId);
        Promise.all([
          this.userEndpoint.GetListUserByIds(listUser).then(r => {
            this.userOption = r
          })
        ]).then(() => {
          res.items.forEach((item) => {
            let a = moment.utc(item.createdDate).local().lang("vi");
            let time = a.fromNow();
            let user = this.userOption.find(x => x.id == item.userId);

            if (!user.avatar) {
              user.avatar = '/assets/images/users/user-avartar.png'
            }
            this.commentResponse.find(x => x.commentStudentId == commentId).listChild.push(CommentVM.From(item, user.name, user.avatar, time));
            this.listChild = this.commentResponse.find(x => x.commentStudentId == commentId).listChild;
          })
        })
      })
  }

  seeMoreParent() {
    this.pageNumber = this.pageNumber + 1;
    Promise.resolve().
      then(() => {
        let studentPaginationFilter: StudentFilter = {
          pageNumber: this.pageNumber,
          sizeNumber: 5,
          getCount: true,
          lessonId: this.lessonId,
          sortDirection: 'DESC',
          SortField: 'CRE',
        }
        return this.commentEndpoint.getCommentParent(studentPaginationFilter);
      })
      .then(res => {

        if (res.items.length < 5) {
          this.isHideParent = true;
        }
        else {
          this.isHideParent = false;
        }

        let listUser = res.items.map(x => x.userId);
        Promise.all([
          this.userEndpoint.GetListUserByIds(listUser).then(r => {
            this.userOption = r
          })
        ]).then(() => {
          res.items.forEach((item) => {
            var a = moment.utc(item.createdDate).local().lang("vi");
            var time = a.fromNow();
            let user = this.userOption.find(x => x.id == item.userId);


            if (!user.avatar) {
              user.avatar = '/assets/images/users/user-avartar.png'
            }
            this.commentResponse.push(CommentVM.From(item, user.name, user.avatar, time));
          })
        })
      })
  }

  editComment(commentId, content) {
    this.isEdit = true;
    this.select = commentId;
    this.commentUpdate = content;
  }

  updateComment(commentId) {
    if (this.commentUpdate.trim() == '') {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Bạn chưa nhập nội dung',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      this.commentUpdate = '';
    } else {
      let commentUpdate: CommentUpdate = { id: commentId, messageContent: this.commentUpdate }
      this.commentEndpoint.updateComment(commentUpdate).then(res => {
        this.commentResponse = [];
        this.getCommet();
        this.isEdit = false;
      })
    }
  }

  deleteComment(commentId) {
    this.commentEndpoint.deleteComment(commentId).then(res => {
      this.commentResponse = [];
      this.getCommet();
    })
  }
  deleteCommentChild(parentId, childId) {
    let listChild = this.commentResponse.find(x => x.commentStudentId == parentId).listChild;
    let item = listChild.find(x => x.commentStudentId == childId);
    let index = listChild.indexOf(item);
    if (index != -1) {
      listChild.splice(index, 1);
    }
    this.commentEndpoint.deleteComment(childId).then(res => {
      this.seeMore(parentId);
    })
  }

  updateChildClick(childComment, content) {
    this.isEditChild = true;
    this.selectChild = childComment;
    this.commentChildUpdate = content;
  }
  updateCommentChild(parentId, childComment) {
    if (this.commentChildUpdate.trim() == '') {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Bạn chưa nhập nội dung!',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      this.commentChildUpdate = '';
    } else {
      //update list
      let listChild = this.commentResponse.find(x => x.commentStudentId == parentId).listChild;
      let item = listChild.find(x => x.commentStudentId == childComment);
      item.messageContent = this.commentChildUpdate;
      let commentUpdate: CommentUpdate = { id: childComment, messageContent: this.commentChildUpdate }
      this.commentEndpoint.updateComment(commentUpdate).then(res => {
        this.isEditChild = false;
      })
    }
  }
}

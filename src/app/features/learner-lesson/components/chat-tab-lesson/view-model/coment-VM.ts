import { CommentResponse } from "sdk/cls-k12-sdk-js/src/services/comment/models/comment-response";

export class CommentVM implements CommentResponse {
    commentStudentId: number;
    userId: number;
    lessonId: number;
    messageContent: string;
    parentId: number;
    createdDate: Date;

    userName: string;
    userAvartar: string;
    listChild: any;
    pageChild: number;
    time: string;
    isHideChild:boolean;

    static From(data: CommentResponse, userName: string, userAvartar: string,time:string) {
        var m: CommentVM = {
            lessonId: data.lessonId,
            userId: data.userId,
            messageContent: data.messageContent,
            commentStudentId: data.commentStudentId,
            parentId: data.parentId,
            createdDate: data.createdDate,
            userName: userName,
            userAvartar: userAvartar,
            pageChild: 0,
            listChild: [],
            time: time,
            isHideChild: false,
        }
        return m;
    }
}
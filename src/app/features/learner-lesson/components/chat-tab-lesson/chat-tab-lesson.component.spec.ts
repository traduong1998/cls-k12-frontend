import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatTabLessonComponent } from './chat-tab-lesson.component';

describe('ChatTabLessonComponent', () => {
  let component: ChatTabLessonComponent;
  let fixture: ComponentFixture<ChatTabLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChatTabLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatTabLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

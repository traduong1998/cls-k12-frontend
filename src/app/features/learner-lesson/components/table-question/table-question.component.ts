import { RuleTestDialogComponent } from './../rule-test-dialog/rule-test-dialog.component';
import { NotificationComponent } from './../notification/notification.component';
import { ResultTestComponent } from './../result-test/result-test.component';
import { DoTestService } from './../../Services/do-test-service.service';
import { DOCUMENT } from '@angular/common';
import { Component, Input, OnInit, Output, EventEmitter, Inject, ViewEncapsulation, OnChanges, SimpleChanges, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { countdown, countdownRealTime } from 'src/app/shared/helpers/cls.helper';
import { UserContentTestInfo } from 'sdk/cls-k12-sdk-js/src/services/user-test/responses/user-content-test-info-response';

@Component({
  selector: 'learner-lesson-table-question',
  templateUrl: './table-question.component.html',
  styleUrls: ['./table-question.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TableQuestionComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() dataQuestion: QuestionTest[];
  @Input() screenWidth;
  @Input() userContentTestId: number;
  @Input() timeLeft;
  @Input() infoTest: UserContentTestInfo;
  @Input() isAnswer: boolean;
  @Output() indexQuestion = new EventEmitter();
  @Output() finishTest = new EventEmitter();
  //câu hỏi đã trả lời
  questionAnswered = [];
  elem: any;
  isFull = false;
  isSubmitSuccess = true;
  funcTimeout;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  destroy$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isReady: boolean = false;
  constructor(@Inject(DOCUMENT) private document: any, public dialog: MatDialog, private router: Router, private doTestService: DoTestService, private _snackBar: MatSnackBar) {
    this.doTestService.fetchFromLocalStorage();
  }
  ngAfterViewInit(): void {
    this.elem = document.documentElement;
    if (this.timeLeft > 0 && this.screenWidth > 960) {
      this.destroy$.next(false);
      document.getElementById("countdownExam").style.display = "block";
      countdownRealTime(this.timeLeft * 1000, this.EndTime, "countdowntimer", this.destroy$);
    };
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.isReady = true;
    this.destroy$.next(true);
    console.log("timeLeft", this.timeLeft);
    if (this.timeLeft > 0 && this.screenWidth > 960) {
      this.destroy$.next(false);
      document.getElementById("countdownExam").style.display = "block";
      countdownRealTime(this.timeLeft * 1000, this.EndTime, "countdowntimer", this.destroy$);
    };
  }
  ngOnDestroy(): void {
    clearTimeout(this.funcTimeout);
    this.destroy$.next(true);
  }
  ngOnInit(): void {
   
  }

  EndTime = () => {
    this.finishTest.emit(true);
  }

  TotalMark() {
    return Math.round(this.dataQuestion.reduce(function (prev, cur) {
      return prev + cur.score;
    }, 0) * 100)/100 ;
  }
  openFullscreen(): void {
    this.isFull = !this.isFull;
    if (this.elem.requestFullscreen) {
      this.elem.requestFullscreen();
    } else if (this.elem.mozRequestFullScreen) {
      /* Firefox */
      this.elem.mozRequestFullScreen();
    } else if (this.elem.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      this.elem.webkitRequestFullscreen();
    } else if (this.elem.msRequestFullscreen) {
      /* IE/Edge */
      this.elem.msRequestFullscreen();
    }
  }
  /* Close fullscreen */
  closeFullscreen(): void {
    this.isFull = !this.isFull;
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
    }
  }

  SendIndexQuestion(id) {
    this.indexQuestion.emit(id);
  }
  getType(id): number {
    let answer = this.doTestService.getAnswerOfQuestion(id);
    if (answer == undefined) {
      return 0
    } else if (answer.isAnswered == false && answer.isMark == true) {
      return 1
    }
    else if (answer.isAnswered == true && answer.isMark == false) {
      return 2
    }
    else if (answer.isAnswered == false && answer.isMark == false) {
      return 0
    }
    else {
      return 3
    }
  }
  callForSupport() {
    //this.dialog.open(CallSupportComponent, { panelClass: 'callsupport-dialog', });
  }
  openExamRuleDialog(): void {
    let dialogRef = this.dialog.open(RuleTestDialogComponent, { panelClass: 'exam-rule-dialog', data: this.infoTest });
  }
}

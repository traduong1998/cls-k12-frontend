import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EndTimeDialogComponent } from './end-time-dialog.component';

describe('EndTimeDialogComponent', () => {
  let component: EndTimeDialogComponent;
  let fixture: ComponentFixture<EndTimeDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EndTimeDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EndTimeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

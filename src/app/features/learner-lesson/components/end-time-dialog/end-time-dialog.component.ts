import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'learner-lesson-end-time-dialog',
  templateUrl: './end-time-dialog.component.html',
  styleUrls: ['./end-time-dialog.component.scss']
})
export class EndTimeDialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public userContentTestId: number) {
    console.log("userContentTestId: ",this.userContentTestId);
  }
  ngOnInit(): void {
  }
}

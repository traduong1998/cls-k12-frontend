import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchingMobileComponent } from './matching-mobile.component';

describe('MatchingMobileComponent', () => {
  let component: MatchingMobileComponent;
  let fixture: ComponentFixture<MatchingMobileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatchingMobileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchingMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

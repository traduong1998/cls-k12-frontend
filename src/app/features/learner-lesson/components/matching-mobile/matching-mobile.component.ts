import { DoTestService } from './../../Services/do-test-service.service';
import { Component, Input, OnInit } from '@angular/core';
import { ArrayAnswer } from 'src/app/features/user-examination/Interfaces/ArrayAnswer';

@Component({
  selector: 'learner-lesson-matching-mobile',
  templateUrl: './matching-mobile.component.html',
  styleUrls: ['./matching-mobile.component.scss']
})
export class MatchingMobileComponent implements OnInit {
  @Input() dataQuestion;
  @Input() userAnswer;
  @Input() isResult;
  @Input() isPublicAnswer: boolean;

  isFlag = false;
  answers;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  data: ArrayAnswer[] = [];
  AnswerOfUserTemp = [];
  isTrueQuestion: boolean;
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  constructor(private doTestService: DoTestService) { }

  ngOnInit(): void {
    this.answers = this.dataQuestion.answers.reduce((r, a) => {
      r[a.position] = [...r[a.position] || [], a];
      return r;
    }, {});

    this.answers.false.forEach(
      element => {
        this.data.push({ order: 0, content: element });
      }
    )
    //lòa load data cũ
    var dataLoading = this.doTestService.getAnswerOfQuestion(this.dataQuestion.id);
    if (dataLoading != null || dataLoading != undefined) {
      this.AnswerOfUserTemp = dataLoading.userAnswers;
    }
    var oldData = this.doTestService.getAnswerOfQuestion(this.dataQuestion.id);
    if (oldData) {
      this.isFlag = oldData.isMark;
    }
    if (oldData && oldData.userAnswers.length > 0) {
      for (let index = 0; index < this.data.length; index++) {
        if (oldData.userAnswers.some(x => x.answerId == this.data[index].content.id)) {
          let oldAnswer = oldData.userAnswers.find(x => x.answerId == this.data[index].content.id)
          this.data[index].order = oldAnswer.answer;
        }
      }
    }

    if (this.isResult) {
      this.isTrueQuestion = this.isAnswerTrue();
    }
  }
  isMark() {
    this.isFlag = !this.isFlag;
    this.doTestService.changeTickUserExercise(this.dataQuestion.id);
  }
  ChoiceAnswer(event, i) {
    //cập nhật lại vị trí khi thay đổi câu hỏi
    this.data.map((ch, i) => {
      if (ch.order == parseInt(event.value)) {
        this.data[i].order = 0;
      }
    });
    this.data[i].order = parseInt(event.value);
    //thêm vào danh sách đáp án
    if (this.AnswerOfUserTemp.some(x => x.answer == event.value) == true) {
      //gỡ cái trùng
      const index = this.AnswerOfUserTemp.indexOf(this.AnswerOfUserTemp.find(x => x.answer == event.value));
      this.AnswerOfUserTemp.splice(index, 1);
      //cập nhật lại mới
      if (this.AnswerOfUserTemp.some(x => x.answerId == this.answers.false[i].id) != true) {
        this.AnswerOfUserTemp.push({ answerId: this.answers.false[i].id, answer: event.value });
      } else {
        const index = this.AnswerOfUserTemp.indexOf(this.AnswerOfUserTemp.find(x => x.answerId == this.answers.false[i].id));
        this.AnswerOfUserTemp.splice(index, 1);
        this.AnswerOfUserTemp.push({ answerId: this.answers.false[i].id, answer: event.value });
      }
    }
    else {
      this.AnswerOfUserTemp.push({ answerId: this.answers.false[i].id, answer: event.value });
    }

    this.doTestService.onChangeUserExercise(this.dataQuestion.id, this.AnswerOfUserTemp);
  }
  groupAnswers() {
    return [...new Set(this.dataQuestion.answers.map(item => item.trueAnswer))]
  }
  answersOfGroup(index) {
    return this.dataQuestion.answers.filter(x => x.trueAnswer == index);
  }

  isAnswerTrue() {
    console.log("dataQuestion",this.dataQuestion);
    console.log("userAnswer",this.userAnswer);

    var countTrue = 0;
    var checkTrue = 0;
    var checkFalse = 0;
    var userAnswers = this.userAnswer.userAnswers;
    let isTrueQuestion: boolean = false;
    for (let j = 0; j < this.dataQuestion.answers.length - 1; j++) {
      if (this.dataQuestion.answers[j].trueAnswer == this.dataQuestion.answers[j + 1].trueAnswer) {
        countTrue++;
      }
    }

    for (let i = 0; i < userAnswers.length; i++) {
      //tìm vế trai
      let left = this.dataQuestion.answers.find(x => x.id == userAnswers[i].answerId)?.trueAnswer;
      //tìm vế phải
      let right = this.dataQuestion.answers.find(x => x.id == userAnswers[i].answer)?.trueAnswer;
      //so sanh coi cùng đáp án không
      //xuất kết quả
      if (left == right) {
        checkTrue++;
      }
      else {
        checkFalse++;
      }
    }
    //câu trả lời bị sai
    if (checkFalse != 0) {
      isTrueQuestion = false;
    }
    //câu trả lời đúng
    if (checkFalse == 0 && checkTrue != 0) {
      isTrueQuestion = true;
    }
    // trả lời thiếu
    if (checkTrue < countTrue) {
      isTrueQuestion = false;
    }
    return isTrueQuestion;
  }

}

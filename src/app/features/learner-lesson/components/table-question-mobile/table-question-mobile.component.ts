import { NotificationComponent } from './../notification/notification.component';
import { ResultTestComponent } from './../result-test/result-test.component';
import { DoTestService } from './../../Services/do-test-service.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { BehaviorSubject } from 'rxjs';
import { Component, Input, OnInit, EventEmitter, Output, ViewEncapsulation } from '@angular/core';
import { countdown, countdownRealTime } from 'src/app/shared/helpers/cls.helper';
import { UserContentTestInfo } from 'sdk/cls-k12-sdk-js/src/services/user-test/responses/user-content-test-info-response';

@Component({
  selector: 'learner-lesson-table-question-mobile',
  templateUrl: './table-question-mobile.component.html',
  styleUrls: ['./table-question-mobile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TableQuestionMobileComponent implements OnInit {
  @Input() dataQuestion;
  @Input() timeLeft;
  @Input() currentPage;
  @Input() totalPage;
  @Input() infoTest: UserContentTestInfo;
  @Input() screenWidth;
  @Input() isShowResult = false;
  @Input() userContentTestId: number;
  @Output() pageIndex = new EventEmitter();
  @Output() indexQuestion = new EventEmitter();
  isShow = false;
  destroy$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(private doTestService: DoTestService, private dialog: MatDialog, private router: Router, private _snackBar: MatSnackBar) {
    this.doTestService.fetchFromLocalStorage();
  }

  ngOnInit(): void {
    if (this.timeLeft > 0 && this.screenWidth <= 960) {
      this.destroy$.next(false);
      countdownRealTime(this.timeLeft * 1000, this.EndTime, "countdowntimer-mobile", this.destroy$);
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
  }
  EndTime = () => {
    this.doTestService.submitUserExercise(this.userContentTestId).then((res) => {
      if (res) {
        this.destroy$.next(true);
        if (this.doTestService.infoTest.isPublicAnswer || this.doTestService.infoTest.isPublicScore) {
          let dialogShowResult = this.dialog.open(ResultTestComponent);
          dialogShowResult.afterClosed().subscribe((isShowResult) => {
            if (isShowResult) {
              this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/result/${this.userContentTestId}`])
            }
            else {
              this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`])
            }
          })
        }else{
          this._snackBar.open("Nộp bài thành công!", 'Ok', {
            duration: 3000,
            panelClass: ['green-snackbar'],
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`])
        }
      } else {
        this._snackBar.open("Có lỗi xảy ra, vui lòng thử lại!", 'ERR', {
          duration: 3000,
          panelClass: ['red-snackbar'],
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    })
      .catch((ERR) => {
        console.error(ERR);
        this._snackBar.open("Có lỗi xảy ra, vui lòng thử lại!", 'ERR', {
          duration: 3000,
          panelClass: ['red-snackbar'],
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
  }
  show() {
    this.isShow = !this.isShow;
  }
  getType(id): number {
    let answer = this.doTestService.getAnswerOfQuestion(id);
    if (answer == undefined) {
      return 0
    } else if (answer.isAnswered == false && answer.isMark == true) {
      return 1
    }
    else if (answer.isAnswered == true && answer.isMark == false) {
      return 2
    }
    else if (answer.isAnswered == false && answer.isMark == false) {
      return 0
    }
    else {
      return 3
    }
  }
  SendIndexQuestion(id) {
    this.indexQuestion.emit(id);
    this.show();
  }

  NextPage() {
    if (this.currentPage < this.dataQuestion.length) {
      this.currentPage = this.currentPage + 1;
    }
    else {
      this.currentPage = this.dataQuestion.length
    }
    this.pageIndex.emit(this.currentPage);
    //console.log("ssss",this.dataQuestion.length);
  }
  PreviousPage() {
    if (this.currentPage <= 1) {
      this.currentPage = 1;
    } else {
      this.currentPage = this.currentPage - 1;
    }

    this.pageIndex.emit(this.currentPage);
    console.log("ssss", this.currentPage);
  }

  callForSupport() {
    //this.dialog.open(CallSupportComponent);
  }

  finishExam() {
    this.dialog.open(NotificationComponent);
    console.log('nộp bài', this.doTestService.getUserAnswer());
    this.doTestService.endExam();
  }

  TotalMark() {
    return Math.round(this.dataQuestion.reduce(function (prev, cur) {
      return prev + cur.score;
    }, 0) * 100)/100 ;
  }
}

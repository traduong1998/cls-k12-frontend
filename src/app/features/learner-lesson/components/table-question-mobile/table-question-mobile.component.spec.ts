import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableQuestionMobileComponent } from './table-question-mobile.component';

describe('TableQuestionMobileComponent', () => {
  let component: TableQuestionMobileComponent;
  let fixture: ComponentFixture<TableQuestionMobileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableQuestionMobileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableQuestionMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeforeAnswerDialogComponent } from './before-answer-dialog.component';

describe('BeforeAnswerDialogComponent', () => {
  let component: BeforeAnswerDialogComponent;
  let fixture: ComponentFixture<BeforeAnswerDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BeforeAnswerDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BeforeAnswerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

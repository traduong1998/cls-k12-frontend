import { TestInfoResponse } from './../../../../../../sdk/cls-k12-sdk-js/src/services/learner-lesson/responses/test-info-response';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RestartDotestDialogComponent } from '../restart-dotest-dialog/restart-dotest-dialog.component';

@Component({
  selector: 'app-before-answer-dialog',
  templateUrl: './before-answer-dialog.component.html',
  styleUrls: ['./before-answer-dialog.component.scss']
})
export class BeforeAnswerDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<BeforeAnswerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public testInfoStudent: TestInfoResponse) { }

  ngOnInit(): void {
  }

  canToRedo(config: TestInfoResponse): boolean {
    if (config.theMaximumNumberToRedo != null) {
      if (config.numberOfRemainingAttempts > 0) {
        return true;
      }
    }
    return false;
  }

}

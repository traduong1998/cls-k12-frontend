import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContentLearnerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/contentLearnerEndpoint';
import { Content } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/models/content';

@Component({
  selector: 'app-details-content',
  templateUrl: './details-content.component.html',
  styleUrls: ['./details-content.component.scss']
})
export class DetailsContentComponent implements OnInit {

  @Output() emitIsShow = new EventEmitter();
  @Output() emitDarkMode = new EventEmitter();
  @Output() emitFUllMode = new EventEmitter();

  //#region INPUT OUTPUT PROPERTY

  //#endregion

  //#region ENDPOINT
  contentLearnerEndpoint: ContentLearnerEndpoint;
  //#endregion

  //#region PUBLIC PROPERTY
  lessonId: number;
  contentId: number;
  content: Content;
  isSameType = false;
  isReady: boolean = false;
  typesOfContent: string;
  //#endregion

  //#region PRIVATE PROPERTY
  //#endregion

  //#region CONSTRUCTOR
  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe((params) => {
      this.lessonId = + this.route.parent.snapshot.paramMap.get('lessonId');
      this.contentId = + this.route.snapshot.paramMap.get('id');
      this.contentLearnerEndpoint = new ContentLearnerEndpoint();
      this.contentLearnerEndpoint.getContent(this.lessonId, this.contentId).then(res => {
        // console.log( "content", res);
        if (res) {
          this.typesOfContent = res.typesOfContent;
          this.content = res;
          this.content.lessonId = this.lessonId;
          this.isReady = true;
        }
      });
    })
  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {
  }

  isShowContent(event) {
    this.emitIsShow.emit(event);
  }

  isDarkMode(event) {
    this.emitDarkMode.emit(event);
  }

  isFullContent(event) {
    this.emitFUllMode.emit(event);
  }

}

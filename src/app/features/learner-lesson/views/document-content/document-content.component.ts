import { AfterViewChecked, ChangeDetectorRef, Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { PdfJsViewerComponent } from 'ng2-pdfjs-viewer';
import { NgxSpinnerService } from 'ngx-spinner';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ContentLearnerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/contentLearnerEndpoint';
import { Content } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/models/content';
import { ConditionForCompletion } from 'src/app/features/learn/intefaces/conditionForCompletion';
import { DecriptionContentComponent } from '../../components/decription-content/decription-content.component';
import { LearnerService } from '../../Services/learnerServive.service';
import { ProgressPlayerService } from '../../Services/progress-player.service';
@Component({
  selector: 'learner-document-content',
  templateUrl: './document-content.component.html',
  styleUrls: ['./document-content.component.scss']
})
export class DocumentContentComponent implements OnInit {
  //#region INPUT OUTPUT PROPERTY
  //
  destroy: ReplaySubject<any> = new ReplaySubject<any>(1);

  progressService;
  forwardService;
  isPlay: boolean = false;
  progress;
  percent = 0;
  top = 0;
  lightMode;
  isLightMode: boolean;
  totalHeight;
  progressPercent;
  @ViewChild('scrollframe') scroll: ElementRef;
  @ViewChild('viewerjs') viewerjs: PdfJsViewerComponent;
  @Input() content: Content;
  @Output() isShowContent: EventEmitter<any> = new EventEmitter();
  @Output() isDarkMode: EventEmitter<any> = new EventEmitter();
  @Output() isFullContent: EventEmitter<any> = new EventEmitter();
  //#endregion

  //#region PRIVATE PROPERTY
  private isFirstLoad = true;
  // Loaded via <script> tag, create shortcut to access PDF.js exports.
  // pdfjsLib = window['pdfjs-dist/build/pdf'];
  loadingTask;

  //#endregion

  //#region PUBLIC PROPERTY
  pdfSource;

  lessonId: number;
  contentId: number;
  isSameType = false;
  isReady: boolean = false;
  iscompleteOfCondition: boolean = false;
  typeOfContent: string = 'DOC';
  contentLearnerEndpoint: ContentLearnerEndpoint;

  percentProgressPlay: number;
  sjSubcribe;
  heightCanvas: number = 0;
  widthCanvas: number = 0;
  numberCanvas: number = 0;
  pageNow: number = 1;
  totalPage: number = 1;
  pageInput: number = 1;
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    // this.width = window.innerWidth;
    // //this.screenWidth = window.innerWidth;
    if (window.innerWidth > 600) {
      this.zoomInOut = '100';
      this.checkMobile = false;
    } else {
      this.zoomInOut = 'page-fit';
      this.checkMobile = true;
    }
  }
  zoomInOut: string = '100';
  isReadyPDF: boolean = false;

  speed: number = 1;

  openProgressBar: boolean = true;
  checkMobile: boolean = false;

  oldLink: string = "";

  prevPageScroll = false;
  nextPageScroll = true;

  // ////////pdfjs var
  // pdfDoc = null;
  // pageNum = 1;
  // pageRendering = false;
  // pageNumPending = null;
  // scale = 0.8;
  // canvas = <HTMLCanvasElement>document.getElementById('the-canvas');
  // ctx = this.canvas.getContext('2d');
  pageNum: number = 0;


  isChangeLink = true;
  ///////


  //#endregion

  //#region CONSTRUCTOR

  constructor(
    private route: ActivatedRoute,
    public dialog: MatDialog,
    protected sanitizer: DomSanitizer,
    private _learnerService: LearnerService,
    private _progressPlayerService: ProgressPlayerService,
    private cdr: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
    this.onResize();
    this.escEvent();
  }



  //true =>đèn sang
  //fasle => tắt đèn
  getStatusLightMode() {
    if (localStorage.getItem('light_mode') == "true") {
      this.isLightMode = true;
    }
    this.lightMode = this._progressPlayerService.isLightMode$.subscribe((status) => {
      this.isLightMode = status;
      this.isDarkMode.emit(status);
    })
  }
  //#endregion

  //#region LIFECYCLE

  ngOnInit(): void {

    if (!this.content.link) {
      setTimeout(() => this._progressPlayerService.statusContent.next('BLANK'));
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(`ngOnChanges`);
    // 

    console.log("ngOnchangesDoc");
    this.spinner.show();

    this.isFirstLoad = false;

    this._progressPlayerService.changeLinkDocument.next({ type: 'doc', page: this.pageNow, totalPage: this.totalPage });
    this.pageNow = 1;
    this.pageInput = 1;
    this.percent = 0;

    if (document.getElementById("PDFJS") != null) {
      this.isChangeLink = false;
      setTimeout(() => {
        this.isChangeLink = true;
      }, 0);
    }

    if (this.progressService) {
      this._progressPlayerService.resetSubject();
    }

    this.isReady = true;
    if (this.content.conditionsForCompletion == ConditionForCompletion.answerQuestion) {
      this.iscompleteOfCondition = true;
    }
    this._learnerService.updateContentCurriculums(this.content.id, this.content.curriculumId, true);
  }

  ngAfterViewInit() {
    console.log(`ngAfterViewInit`);
    setTimeout(() => {
      this._progressPlayerService.emitPlayEvent.next(false);
      this.getStatusLightMode();
      this.getFullScreenStatus();
      this.clickExitFullScreen();
      this.getOnClickSlider();
      this.getOnClickPrev();
      this.getOnClickNext();
      this.cdr.detectChanges();
    }, 0);

  }

  getOnClickSlider() {
    this.progressPercent = this._progressPlayerService.percentScroll$.subscribe((p: number) => {

      console.log("percent-doc", p, this.pageInput);
      setTimeout(() => {
        this.pageInput = this.pageNow;
      }, 0);
      setTimeout(() => {
        this.pageInput = p;
      }, 0);


      this.percent = p / this.totalPage * 100;
      this._progressPlayerService.emitPercent.next({ percent: this.percent, page: this.pageInput, totalPage: this.totalPage });
    })
  }
  getOnClickPrev() {
    this.forwardService = this._progressPlayerService.previous$.pipe(takeUntil(this.destroy)).subscribe((value) => {
      if (this.pageNow <= 1) {
        return;
      }
      this.pageNow--;
      this.pageInput = this.pageNow;
      this.percent = (this.pageNow / this.totalPage) * 100;
      this.progressService = this._progressPlayerService.emitPercent.next({ percent: this.percent, page: this.pageNow, totalPage: this.totalPage });
    })
  }

  getOnClickNext() {
    this.forwardService = this._progressPlayerService.next$.pipe(takeUntil(this.destroy)).subscribe((value) => {
      if (this.pageNow >= this.totalPage) {
        return;
      }
      this.pageNow++;
      this.pageInput = this.pageNow;
      this.percent = (this.pageNow / this.totalPage) * 100;
      this.progressService = this._progressPlayerService.emitPercent.next({ percent: this.percent, page: this.pageNow, totalPage: this.totalPage });
    })
  }

  testPagesLoaded($event) {
    console.log('testPagesLoaded', $event, this.isChangeLink)
    const appConfig = this.viewerjs.PDFViewerApplication.appConfig;

    appConfig.sidebar.viewerContainer.className = 'scrollbar-pdf';

    //set style cho ng2-pdfjs-viewer
    var doc = this.viewerjs.iframe.nativeElement.contentDocument || this.viewerjs.iframe.nativeElement.document;
    var styleCustom = doc.createElement('link');
    styleCustom.rel = 'stylesheet';
    styleCustom.type = 'text/css';
    styleCustom.href = '/assets/pdfjs-extend/pdfjs-extend.css';
    doc.querySelector('head').appendChild(styleCustom);
    //

    this.totalPage = $event
    this.percent = (this.pageNow / this.totalPage) * 100;
    // document.getElementById('page_count').textContent = (this.totalPage).toString();
    this._progressPlayerService.emitPercent.next({ percent: this.percent, page: this.pageNow, totalPage: this.totalPage });
    this.spinner.hide();
  }

  testPageChange($event) {
    console.log('testPageChange', $event, this.isChangeLink, this.pageInput)
    this.pageNow = $event;
    this.percent = ($event / this.totalPage) * 100;
    // document.getElementById('page_num').textContent = (this.pageNow).toString();
    this._progressPlayerService.emitPercent.next({ percent: this.percent, page: this.pageNow, totalPage: this.totalPage });
  }

  //#endregion

  //#region PUBLIC METHOD

  public onDecriptionHandleClick() {
    if (window.innerWidth <= 525) {
      this.dialog.open(DecriptionContentComponent, { data: this.content.description, width: '300px' });
    } else {
      const dialogRef = this.dialog.open(DecriptionContentComponent, {
        width: '474px',
        data: this.content.description
      });
    }
  }
  ngOnDestroy(): void {
    console.log(`ngOnDestroy-doc`);
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    if (this.progressService) {
      this.progressService.unsubscribe();
    }
    if (this.lightMode) {
      this.lightMode.unsubscribe();
    }
    if (this.forwardService) {
      this.forwardService.unsubscribe();
    }
    if (this.progressPercent) {
      this.progressPercent.unsubscribe();
    }
    this._progressPlayerService.emitPlayEvent.next(false);
  }
  //#endregion

  //#region PRIVATE METHOD
  //#endregion

  //PDF


  isShow = false;
  ShowMenuContent() {
    this.isShow = !this.isShow;
    // // bắn qua thằng cha
    this.isShowContent.emit(this.isShow);
  }

  isFull = false;
  getFullScreenStatus() {
    let fullscreen = document.querySelector("#mat-card-content-document") as HTMLElement & {
      mozRequestFullScreen(): Promise<void>;
      webkitRequestFullscreen(): Promise<void>;
      msRequestFullscreen(): Promise<void>;
      requestFullscreen(): Promise<void>;
    };
    this.progressService = this._progressPlayerService.fullScreen$.subscribe((value) => {
      // console.log("alo", value);
      this.isFull = value;
      if (value) {
        // mainContentPDF.style.paddingBottom = `320px`;
        // fullscreen.webkitRequestFullscreen();
        if (fullscreen.requestFullscreen) {
          fullscreen.requestFullscreen();
        } else if (fullscreen.mozRequestFullScreen) {
          fullscreen.mozRequestFullScreen();
        } else if (fullscreen.webkitRequestFullscreen) {
          fullscreen.webkitRequestFullscreen();
        } else if (fullscreen.msRequestFullscreen) {
          fullscreen.msRequestFullscreen();
        }
        // fullscreen.requestFullscreen();
        // this.getTotalHeight();
        this.openProgressBar = false;
        this.openProgressFullScreen();
        // this.progressService = this._progressPlayerService.emitFullScreenEvent(false);
      }
    })
  }


  escEvent() {
    document.addEventListener('fullscreenchange', (event) => {
      if (document.fullscreenElement) {
        // console.log(`Element: ${document.fullscreenElement.id} entered full-screen mode.`);
      } else {
        // console.log(`Element: exit`);
        this.isFull = false;
        this.openProgressBar = true;
        this._progressPlayerService.emitFullScreenEvent(false);
      }
    });
  }

  clickExitFullScreen() {
    this.progressService = this._progressPlayerService.fullScreen$.subscribe((value) => {
      this.isFull = value;
      if (!value) {
        this._progressPlayerService.emitPercent.next({ percent: this.percent, page: this.pageNow, totalPage: this.totalPage });
        document.exitFullscreen();
        this.openProgressBar = true;
      }
    })
  }

  openProgressFullScreen() {
    if (this.isFull == true) {
      document.addEventListener('mousemove', () => {
        if (this.openProgressBar == false && this.isFull == true) {
          this.openProgressBar = true;
          setTimeout(() => {
            if (this.isFull == true) {
              this.openProgressBar = false;
            }
          }, 3000);
        }

      })
    }
  }

  handleDurationDocument(value): void {
    console.log(`value`, value);
    // const player = document.getElementById('sound') as HTMLAudioElement;
    if (value) {

      // player.currentTime = value.duration;

      // this.percent = value.duration;
      this.pageNow = Math.round(value.duration / 100 * value.totalPage);
      this.pageNow < 1 ? 1 : this.pageNow;
      this.pageInput = Math.round(value.duration / 100 * value.totalPage);
      this.pageInput < 1 ? 1 : this.pageInput;
      this.totalPage = value.totalPage;
      this.totalPage < 1 ? 1 : this.totalPage;

      // this._progressPlayerService.emitPercentClickEvent(this.percent);
      this._progressPlayerService.linkDocumentLocalStorage.next(value.id);
    }
  }

  getAllProperties(): void {
    const doc = {
      type: 'doc',
      link: this.content.link,
    };
    this._progressPlayerService.allPropertiesDocument.next(doc);
  }
}

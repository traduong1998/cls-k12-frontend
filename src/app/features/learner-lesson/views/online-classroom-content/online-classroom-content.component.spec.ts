import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineClassroomContentComponent } from './online-classroom-content.component';

describe('OnlineClassroomContentComponent', () => {
  let component: OnlineClassroomContentComponent;
  let fixture: ComponentFixture<OnlineClassroomContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnlineClassroomContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineClassroomContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit, SimpleChanges, OnDestroy } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CLS, UserEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { ContentLearnerDetailEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/content-learner-detail-endpoint';
import { Content } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/models/content';
import { MeetingDetailandSignature } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/models/meeting-detail-and-signature';
import { AuthService } from 'src/app/core/services';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { LearnerService } from '../../Services/learnerServive.service';
import { ProgressPlayerService } from '../../Services/progress-player.service';
import { DecriptionContentComponent } from '../../components/decription-content/decription-content.component';
import { MatDialog } from '@angular/material/dialog';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
export enum MeetingStatus {
  started = 'started',
  waiting = 'waiting'
}

@Component({
  selector: 'learn-online-classroom-content',
  templateUrl: './online-classroom-content.component.html',
  styleUrls: ['./online-classroom-content.component.scss']
})
export class OnlineClassroomContentComponent implements OnInit, OnDestroy {
  public baseUrl: string = '';
  @Input() content: Content;
  virutalClassroomEndpoint: ContentLearnerDetailEndpoint;
  endpointUser: UserEndpoint;

  private userIdentity: UserIdentity;
  private userCreateId: number = null;
  private isFirstLoad = true;
  zoomMeetingId: string;
  staredDate: Date;
  endDate: Date;
  userName: string;
  isStartMeeting: boolean = false;
  isEndMeeting: boolean = false;
  isReady: boolean;
  virtualClassroomId: number;
  public hostId: number;
  isDarkMode = false;
  subjectInfoClass;

  constructor(
    private _learnerService: LearnerService,
    private _authService: AuthService,
    private _snackBar: MatSnackBar,
    private progressService: ProgressPlayerService,
    public dialog: MatDialog
  ) {
    this.virutalClassroomEndpoint = new ContentLearnerDetailEndpoint();
    this.endpointUser = new UserEndpoint();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.isStartMeeting = false;
    this.isEndMeeting = false;
    if (!this.isFirstLoad) {
      this.ngOnInit();
      this.ngAfterViewInit();
    }
    this.isFirstLoad = false;
    this.isReady = true;
    this._learnerService.updateContentCurriculums(this.content.id, this.content.curriculumId, true);
  }
  ngOnInit(): void {
    this.userIdentity = this._authService.getTokenInfo();
    this.userCreateId = +this.userIdentity.userId
    this.virutalClassroomEndpoint
      .getVirtualClassroomContent(this.content.lessonId, this.content.id)
      .then(res => {
        if (res) {
          this.zoomMeetingId = res.zoomMeetingId;
          this.staredDate = res.startDate;
          this.endDate = res.finishDate;
          this.virtualClassroomId = res.meetingId;
          this.hostId = +res.userId;

          if (new Date().valueOf() - +new Date(res.startDate).valueOf() > 0) {
            this.isStartMeeting = true;
          }
          if (+new Date(res.finishDate).valueOf() - new Date().valueOf() < 0) {
            this.isEndMeeting = true;
          }
          this.endpointUser.GetListUserByIds([res.userId]).then(user => {
            this.userName = user[0].name;
          }).catch(err => {
            console.log(err);
          });
        }
      });
    this.progressService.isLightMode$.subscribe(res => this.isDarkMode = res);
    this.onDecriptionHandleClick();
  }

  ngAfterViewInit() {
  }
  //#endregion

  //#region PUBLIC METHOD
  public onStartClassroomHandler() {
    var newTab = window.open('', '_blank');
    console.log("this.zoomMeetingId", this.zoomMeetingId)
    this.virutalClassroomEndpoint
      .getZoomMeetingDetailLearner(this.content.lessonId, this.content.id, this.zoomMeetingId)
      .then((res: MeetingDetailandSignature) => {
        if (res) {
          if (res.meeting.status == MeetingStatus.waiting && this.hostId !== this.userCreateId) {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: ` Giáo viên chưa vào phòng. Vui lòng đợi `,
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });

            newTab.close();
            return;
          }
          if (res.meeting.status == MeetingStatus.started && this.hostId !== this.userCreateId) {
            this._learnerService
              .clickStartVirtualClassRoom(
                {
                  contentId: this.content.id,
                  curriculumId: this.content.curriculumId,
                  virtualClassRoomId: this.virtualClassroomId
                }
              )
            newTab.location.href = res.meeting.joinUrl;
          }
        }
      }).then(() => {
      })
      .catch((res: ErrorResponse) => {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: ` Giáo viên chưa vào phòng. Vui lòng đợi `,
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
        newTab.close();
      })
  }

  onDecriptionHandleClick(): void {
    this.subjectInfoClass = this.progressService.infoVideo$.subscribe((res: any) => {
      if (res.type === 'VIR') {
        if (window.innerWidth <= 525) {
          this.dialog.open(DecriptionContentComponent, { data: this.content.description, width: '300px' });
        } else {
          this.dialog.open(DecriptionContentComponent, {
            width: res.full ? '420px' : '474px',
            data: this.content.description,
            position: { left: res.full ? 'calc(50% - 210px)' : 'calc(37.5% - 237px)' }
          });
        }
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subjectInfoClass) {
      this.subjectInfoClass.unsubscribe();
    }
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnerContentsLessonComponent } from './learner-contents-lesson.component';

describe('LearnerContentsLessonComponent', () => {
  let component: LearnerContentsLessonComponent;
  let fixture: ComponentFixture<LearnerContentsLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LearnerContentsLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnerContentsLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

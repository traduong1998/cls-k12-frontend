import { BeforeAnswerDialogComponent } from './../../components/before-answer-dialog/before-answer-dialog.component';
import { takeUntil } from 'rxjs/operators';
import { ContentType } from './../../../learn/intefaces/contentType';
import { ContentLearnerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/contentLearnerEndpoint';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { ContentRule } from './../../../learn/intefaces/contentRule';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Component, OnInit, ViewChild, ViewEncapsulation, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { ContentGroupAndContent, ContentGroupsResponse } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/responses/listContentGroupsResponse';
import { ConditionForCompletion } from 'src/app/features/learn/intefaces/conditionForCompletion';
import { countdown } from 'src/app/shared/helpers/cls.helper';
import { ContentGroupLearnerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/contentGroupLearnerEndpoint';
import { LearnerService } from '../../Services/learnerServive.service';
import { ReferencesEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/referencesEndpoint';
import { References } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/references';
import { ReferenceResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/referenceResponses';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { TestInfoResponse } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/responses/test-info-response';
import { UserTestEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-test/endpoint/user-test-endpoint';
import { ContentLearnerDetailEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/content-learner-detail-endpoint';
import { UserEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { ProgressPlayerService } from '../../Services/progress-player.service';
import { VideoContentComponent } from '../video-content/video-content.component';
import { OutContentDialogComponent } from '../../components/out-content-dialog/out-content-dialog.component';
import { FinishLessonDialogComponent } from '../../components/finish-lesson-dialog/finish-lesson-dialog.component';
import { OutContentCompleteDialogComponent } from '../../components/out-content-complete-dialog/out-content-complete-dialog.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { RestartDotestDialogComponent } from '../../components/restart-dotest-dialog/restart-dotest-dialog.component';
import { ContentCurriculum } from '../../interfaces/contentCurriculum';
import { RestartAnswerDialogComponent } from '../../components/restart-answer-dialog/restart-answer-dialog.component';
export class ReferenceDisplay {
  name: string;
  type: string;
  link: string;
  isShowLink: boolean;
}
@Component({
  selector: 'app-learner-contents-lesson',
  templateUrl: './learner-contents-lesson.component.html',
  styleUrls: ['./learner-contents-lesson.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LearnerContentsLessonComponent implements OnInit, OnDestroy {
  @ViewChild('tabGroup') tabGroup;
  @ViewChild(VideoContentComponent, { static: false }) videoContent: VideoContentComponent;
  loadedDocument = false;
  learnContentsResponse: ContentGroupsResponse;
  contentGroupEndpoint: ContentGroupLearnerEndpoint;
  contentLearnerEndpoint: ContentLearnerEndpoint;
  userTestEndpoint: UserTestEndpoint;
  contentLearnerDetailEndpoint: ContentLearnerDetailEndpoint;
  private isFirstLoadContent = true;
  public tabChange: number = 0;
  public isShowExitButton = true;
  lessonId: number;
  lessonName: string;
  contentId: number = 0;
  isReady: boolean = false;
  totalPercent: number = 50;
  stylePercent: string;
  currentContent: ContentGroupAndContent;
  referenceEndpoint: ReferencesEndpoint;
  userEndpoint: UserEndpoint;
  references: References[];
  referencesDisplay: ReferenceDisplay[] = [];
  listContents: ContentGroupAndContent[] = [];
  listContentGroup: ContentGroupAndContent[] = [];
  lastContent: ContentGroupAndContent;
  firstContent: ContentGroupAndContent;
  destroy$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  contentCurriculums: ContentCurriculum[];
  testInfoStudent: TestInfoResponse;
  isLoadingTestInfo: boolean = true;
  avatar: string;
  teacherName: string;
  teacherId: number;
  bgImage = '';
  sjLightMode;
  sjBgImage;
  sjIsLearner;
  showButtonInfoVideo = false;
  lastItem: number;
  contentLastGroupId: number;
  contentIdTemp: number;
  groupIdTemp: number;
  temp: boolean = false;
  rootContent = 0;

  isFisrtLoad = true;
  // Hiển thị banner hoàn thành 1 nội dung
  showBannerComplete = false;
  // Hiển thị khi bài học không có nội dung;
  showBlankContentLesson = null;
  // Hiển thị có nội dung đang đợi xử lý
  showBannerWaitingContent = false;
  showCompleteButton = true;
  unsubcribe$: Subject<boolean> = new Subject<boolean>();
  isDisableButtonComplete = false;
  check = false;
  reviewLesson: boolean = false;
  constructor(private _route: ActivatedRoute, private router: Router, private learnerService: LearnerService, private dialog: MatDialog, private progressService: ProgressPlayerService, private spinner: NgxSpinnerService,
    private cdr: ChangeDetectorRef,
  ) {
    router.events.pipe(takeUntil(this.unsubcribe$)).subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (this.router.url.includes('/detail-content') || this.router.url.includes('/detail-test')) {
          this.check = true;
        }
        if (this.check && !this.reviewLesson) {
          if (this.router.url.endsWith('contents')) {
            this.router.navigate(['/learner/user-learn'])
          }
        }
      }
    });

    this.spinner.show();
    document.body.classList.add(`learn-custom-global-css`);//TODO: tạm thời khi layout chưa hỗ trợ ẩn
    //Gọi api
    this.contentGroupEndpoint = new ContentGroupLearnerEndpoint();
    this.contentLearnerEndpoint = new ContentLearnerEndpoint();
    this.referenceEndpoint = new ReferencesEndpoint();
    this.userTestEndpoint = new UserTestEndpoint();
    this.contentLearnerDetailEndpoint = new ContentLearnerDetailEndpoint();
    this.userEndpoint = new UserEndpoint();

    this.learnerService.contentCurriculums$.pipe(takeUntil(this.unsubcribe$)).subscribe((res) => {
      this.contentCurriculums = res;
    });

    //Lấy id trên route
    this.lessonId = + _route.snapshot.paramMap.get("lessonId");
    if (_route.firstChild != null) {
      this.contentId = + this._route.firstChild.params["value"]["id"];
    }

    Promise.all([
      this.contentGroupEndpoint.getListContentGroupsForLearner(this.lessonId).then((res) => {
        this.lessonName = res.lessonName;
        this.teacherId = res.teacherId;
        if (!res.avatar) {
          this.avatar = '/assets/images/lesson/lesson-default.png';
        } else {
          this.avatar = res.avatar;
        }

        this.learnContentsResponse = res;
        // console.log("sdsa", this.learnContentsResponse.listContentGroupsAndContent);
        this.learnContentsResponse.listContentGroupsAndContent.sort((a, b) => a.order - b.order);
        this.learnContentsResponse.listContentGroupsAndContent.map(x => {
          // x.isLearned = false;
          x.isExpanded = false;
        });
        this.onHandleGetPercentFromLocal(this.learnContentsResponse.listContentGroupsAndContent);
        this.listContents = this.learnContentsResponse.listContentGroupsAndContent.filter(x => x.contentId != null && x.contentId != undefined);
        // Bài học có nội dung
        if (this.learnContentsResponse.listContentGroupsAndContent.length > 0) {

          this.showBlankContentLesson = true;
          //tìm thèn đầu tiên và cuối cùng hợp lệ

          let isHaveFisrtItem = false;
          for (let index = 0; index < this.learnContentsResponse.listContentGroupsAndContent.length; index++) {
            //
            // loại thèn đầu tiên là tiết rỗng
            if (this.learnContentsResponse.listContentGroupsAndContent[index].contentId == null && this.learnContentsResponse.listContentGroupsAndContent[index].contentGroupId != null
              && (this.learnContentsResponse.listContentGroupsAndContent[index].listContents == undefined
                || this.learnContentsResponse.listContentGroupsAndContent[index].listContents?.length == 0)) {
              this.temp = true;
            } else {
              //nếu nó là nội dung => đã có thèn đầu tiên
              if (isHaveFisrtItem == false) {
                this.contentIdTemp = this.learnContentsResponse.listContentGroupsAndContent[index]?.contentId;
                this.groupIdTemp = this.learnContentsResponse.listContentGroupsAndContent[index]?.contentGroupId;
                this.temp = false;
                isHaveFisrtItem = true;
              }
            }
            //lây ra thèn cuối cung
            if (index == this.learnContentsResponse.listContentGroupsAndContent.length - 1) {
              this.temp = false;
              this.lastItem = this.learnContentsResponse.listContentGroupsAndContent[index].order;
              this.contentLastGroupId = this.learnContentsResponse.listContentGroupsAndContent[index].contentGroupId;
            }
          }
          this.learnContentsResponse.listContentGroupsAndContent.forEach((value, index) => {
            if (index === this.learnContentsResponse.listContentGroupsAndContent.length - 1) {
              value['lastItem'] = true;
            }
          });
          //Lọc ra 2 list này riêng biệt cho dễ xử lý
          this.firstContent = [...this.listContents].shift();
          this.listContentGroup = this.learnContentsResponse.listContentGroupsAndContent.filter(x => x.contentId == null);
          //Nếu quy luật nội dung là tự do hoặc nội dung đó đã hoàn thành thì set isActive = true
          this.learnContentsResponse.listContentGroupsAndContent.map((item) => {
            if (this.learnContentsResponse.formOfLearn == this.formOfLearn.free || item.isComplete) item.isActive = true;
          });
          if (this.learnContentsResponse.formOfLearn == this.formOfLearn.sequentially) {
            let lastContentFinished = this.listContents.filter(x => x.isComplete).pop();
            let index = this.listContents.indexOf(lastContentFinished);
            this.listContents.map((content, i) => {
              if (i <= index + 1) content.isActive = true;
            })
          }
          //Gán content hiện tại
          if (this.contentId > 0) {
            let content = this.learnContentsResponse.listContentGroupsAndContent.find(x => x.contentId == this.contentId);
            if (content && (content.isActive || content.isComplete)) {
              this.currentContent = content;
              if (this.currentContent.contentGroupId != null) {
                this.listContentGroup.find(x => x.contentGroupId == this.currentContent.contentGroupId).isExpanded = true;
              }
            }
            else {
              this.currentContent = this.listContents[0];
              this.currentContent.isActive = true;
              if (this.currentContent.contentGroupId != null) {
                this.listContentGroup.find(x => x.contentGroupId == this.currentContent.contentGroupId).isExpanded = true;
              }
            }
          }
          else {
            if (this.listContents.length > 0) {
              this.currentContent = this.listContents[0];
              this.currentContent.isActive = true;
              if (this.currentContent.contentGroupId != null) {
                this.listContentGroup.find(x => x.contentGroupId == this.currentContent.contentGroupId).isExpanded = true;
              }
              //  phải thèn đầu tiên
              this.progressService.disableButtonBackEmit.next(true);
            }
          }
          if (this.currentContent) {
            this.currentContent.isActive = true;
            this.rootContent = this.currentContent.contentId;
            //navigate qua component hiển thị nội dung
            this.onClickContent(this.currentContent);
            //set phần trăm hoàn thành và tổng số content trong tiết
            this.setTotalContentFinished();
            if (this.learnContentsResponse.percentageOfCompletion != null) {
              if (this.learnContentsResponse.percentageOfCompletion != this.totalPercent && this.learnContentsResponse.percentageOfCompletion != 100) {
                this.contentGroupEndpoint.updatePercentCompletion(this.lessonId).then(userLesson => {
                  this.totalPercent = userLesson.percentageOfCompletion;
                })
              }
            }
            this.isReady = true;
          }
          this.progressService.isLearner.next(true);
        } else {
          // Bài học không có nội dung
          this.showBlankContentLesson = false;
          this.progressService.isLearner.next(false);
        }
      })
    ]).then(() => {
      if (this.learnContentsResponse.listContentGroupsAndContent.length > 0) {
        this.getTeacherInfor();
      }
      this.spinner.hide();
    });
  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {
    this.learnerService.eventClickStartVirtualClassRoom$.pipe(takeUntil(this.unsubcribe$)).subscribe((event) => {
      if (event) {
        if (this.currentContent.typesOfContent == this.contentType.virtualClassRoom) {
          this.onSubmitContent();
        }
      }
    });
    this.sjLightMode = this.progressService.isLightMode$.pipe(takeUntil(this.unsubcribe$)).subscribe(res => {
      this.isDarkMode = res;
      if (res) {
        this.bgImage = '';
      } else {
        if (localStorage.getItem('bg_learn') !== null) {
          this.bgImage = localStorage.getItem('bg_learn');
        }
      }
    });
    this.sjBgImage = this.progressService.bgImage$.pipe(takeUntil(this.unsubcribe$)).subscribe(res => this.bgImage = res);
    setTimeout(() => { this.sjIsLearner = this.progressService.isLearner.next(true) }, 0);
    if (localStorage.getItem('fullscreen_learn') === 'true') {
      this.isFull = true;
    }
    if (localStorage.getItem('bg_learn') !== null) {
      this.bgImage = localStorage.getItem('bg_learn');
    }
    // nếu type content là video thì get phần trăm hoàn thành video từ local
    this.progressService.percentCompleteVideo$.pipe(takeUntil(this.unsubcribe$)).subscribe((res: any) => {
      switch (res.type) {
        case 'youtube':
          this.onHandleDurationStorage('duration_videoyt', res.link);
          break;
        case 'videojs':
          this.onHandleDurationStorage('duration_videojs', res.link);
          break;
      }
    });
    // get phần trăm hoàn thành sound từ local
    this.progressService.percentCompleteSound$.pipe(takeUntil(this.unsubcribe$)).subscribe(idSound => {
      if (idSound) {
        this.onHandleDurationStorage('duration_audio', idSound);
      }
    });

    // get phần trăm hoàn thành document từ local
    this.progressService.linkDocumentLocalStorage$.subscribe(idDoc => {
      this.onHandleDurationStorageDocument('duration_document', idDoc);
    });

    // get phần trăm hoàn thành pure từ local
    this.progressService.idPureLocalStorage$.subscribe(idPure => {
      this.onHandleDurationStoragePure('duration_pure', idPure);
    });

    this.progressService.nextContent$.pipe(takeUntil(this.unsubcribe$)).subscribe((p) => {
      if (p) {
        this.onClickNextContent();
      }
    });

    this.progressService.backContent$.pipe(takeUntil(this.unsubcribe$)).subscribe((p) => {
      if (p) {
        this.onClickBackContent();
      }
    });

    this.progressService.statusContent$.pipe(takeUntil(this.unsubcribe$)).subscribe(res => {
      if (res !== 'COM') {
        this.showBannerWaitingContent = true;
        this.showButtonInfoVideo = false;
      }
      if (res === 'BLANK') {
        this.showCompleteButton = false;
      } else {
        this.showCompleteButton = true;
      }
    });

    this.progressService.percentCompleteVideoOut$.pipe(takeUntil(this.unsubcribe$)).subscribe(res => {
      this.getPercentFromLocal(res);
    });
  }

  getPercentFromLocal(res): void {
    if (this.learnContentsResponse?.listContentGroupsAndContent.length > 0 && res) {
      this.learnContentsResponse?.listContentGroupsAndContent.forEach(item => {
        if (item.contentId === +res.contentId) {
          item.percentVideo = Math.round(res.percent);
        }
      });
    }
  }

  ngOnDestroy(): void {
    document.body.classList.remove(`learn-custom-global-css`);
    if (this.sjLightMode) {
      this.sjLightMode.unsubscribe();
    }
    if (this.sjBgImage) {
      this.sjBgImage.unsubscribe();
    }
    this.progressService.isLearner.next(false);
    this.destroy$.next(true);
    this.unsubcribe$.next(true);
    this.unsubcribe$.complete();
  }

  getTeacherInfor() {
    this.userEndpoint.getListTeacherByIds([this.teacherId]).then(res => {
      this.teacherName = res[0].name;
    })
  }

  get conditionForCompletion(): typeof ConditionForCompletion {
    return ConditionForCompletion;
  }
  get formOfLearn(): typeof ContentRule {
    return ContentRule;
  }
  get contentType(): typeof ContentType {
    return ContentType;
  }

  onHandleDurationStorage(player: string, idVideo): void {
    if (idVideo && localStorage.getItem(player)) {
      const value = JSON.parse(localStorage.getItem(player));
      const filterArr = value.find(item => item.id === idVideo);
      if (filterArr && this?.currentContent && this.learnContentsResponse?.listContentGroupsAndContent.length > 0) {
        const currentObj = this.learnContentsResponse?.listContentGroupsAndContent.find(item =>
          item.contentId === this.currentContent?.contentId);
        currentObj.percentVideo = Math.round(filterArr.percent) > 100 ? 100 : Math.round(filterArr.percent);
        this.cdr.detectChanges();
      }
    }
  }

  onHandleDurationStorageDocument(player: string, idDoc): void {

    if (idDoc && localStorage.getItem(player)) {
      const value = JSON.parse(localStorage.getItem(player));
      const filterArr = value.find(item => item.id === idDoc);
      if (filterArr && this?.currentContent && this.learnContentsResponse?.listContentGroupsAndContent.length > 0) {
        const currentObj = this.learnContentsResponse?.listContentGroupsAndContent.find(item =>
          item.contentId === this.currentContent?.contentId);
        currentObj.percentVideo = Math.round(filterArr.percent) > 100 ? 100 : Math.round(filterArr.percent);
      }
    }
  }

  onHandleDurationStoragePure(player: string, idPure): void {

    if (idPure && localStorage.getItem(player)) {
      const value = JSON.parse(localStorage.getItem(player));
      const filterArr = value.find(item => item.id === idPure);
      if (filterArr && this?.currentContent && this.learnContentsResponse?.listContentGroupsAndContent.length > 0) {
        const currentObj = this.learnContentsResponse?.listContentGroupsAndContent.find(item =>
          item.contentId === this.currentContent?.contentId);
        currentObj.percentVideo = Math.round(filterArr.percent) > 100 ? 100 : Math.round(filterArr.percent);
      }
    }
  }

  /**Tính toán phần trăm hoàn thành và số nội dung hoàn thành trong mỗi tiết */
  setTotalContentFinished(): void {
    let toltalContentFinished: number = 0;
    this.learnContentsResponse.listContentGroupsAndContent.forEach((item) => {
      //Nếu là tiết học
      if (item.contentId == null) {
        //set tổng số nd và tổng số nd hoàn thành trong tiết
        item.listContents = this.listContents.filter(x => x.contentGroupId == item.contentGroupId);
        item.totalContentFinished = this.listContents.filter(x => x.contentGroupId == item.contentGroupId && x.isComplete).length;
        toltalContentFinished += item.totalContentFinished;
      }
      else if (item.contentId != null && item.contentGroupId == null) {//Nếu là nội dung ngoài tiết 
        if (item.isComplete) toltalContentFinished++;
      }
    });
    this.totalPercent = Math.round((toltalContentFinished / this.listContents.length) * 100);
    this.stylePercent = `${(this.totalPercent <= 3) ? 1 : (this.totalPercent - (this.totalPercent / 10) * 1.17 - 4)}%`;

    // if (this.totalPercent == 100) {
    //   this.dialog.open(FinishLessonDialogComponent, { disableClose: true });
    // }
  }

  /** Tìm content tiếp theo hoặc trước đó */

  nearContent(isNextContent: boolean): ContentGroupAndContent {
    let index = this.listContents.indexOf(this.currentContent);
    return isNextContent ? this.listContents[index + 1] : this.listContents[index - 1];
  }
  /**Sự kiện khi click content */
  onClickContent(content: ContentGroupAndContent): void {
    //backk
    if (content.contentId == this.contentIdTemp && content.contentGroupId == this.groupIdTemp) {
      this.progressService.disableButtonBackEmit.next(true);
    }
    else {
      this.progressService.disableButtonBackEmit.next(false);
    }
    //next
    if (this.temp == true) {
      this.progressService.disableButtonNext.next(true);
    }

    if ((content.order + 1 == this.lastItem) && this.contentLastGroupId == null && (content.listContents?.length == 0 || content.listContents == undefined)) {
      if (content.order < this.lastItem) {
        this.progressService.disableButtonNext.next(false);
      }
      else {
        this.progressService.disableButtonNext.next(true);

      }
    } else {
      if (content.order == this.lastItem) {
        this.progressService.disableButtonNext.next(true);
      }
      else {
        this.progressService.disableButtonNext.next(false);
      }
    }

    if (this.learnContentsResponse.formOfLearn == 'SEQ') {
      if (!content.isComplete) {
        this.progressService.disableButtonNext.next(true);
      }
      else {
        this.progressService.disableButtonNext.next(false);
      }
    }
    if (this.learnContentsResponse.listContentGroupsAndContent
      && (this.learnContentsResponse.listContentGroupsAndContent.length === content.order || content['lastItem'])) {
      this.progressService.disableButtonNext.next(true);
    }

    this.isLoadingTestInfo = true;
    // Đủ điều kiện mới cho click vào nội dung
    if (content.isActive || content.isComplete) {
      if (this.rootContent !== content.contentId) {
        this.showBannerWaitingContent = false;
        this.rootContent = content.contentId;
      }
      this.showBannerComplete = content.isComplete ? false : true;
      // Nếu chuyển từ nội dung đếm ngược thời gian qua thì nhớ distroy cái interval
      if (this.currentContent.conditionsForCompletion == this.conditionForCompletion.interval) {
        this.destroy$.next(true);
      }
      if (!content.isLearned) {
        //Goi api thêm vào userCOntent, sau khi thêm xong thì gán isLearned = true;
        this.contentLearnerEndpoint.addUserContent(content.contentId, this.lessonId).then((res) => {
          content.isLearned = true;
        })
      }
      //Gán currentContent thành content hiện tại đang chọn
      this.currentContent = content;
      // Nếu nọi dung là nội dung có nút hoàn thanh
      if (this.currentContent?.conditionsForCompletion === this.conditionForCompletion.clickButton && this.currentContent) {
        this.showCompleteButton = true;
      }
      //Nếu điều kiện hoàn thành là sau khoảng thời gian thì gọi countdown 
      if (content.conditionsForCompletion == this.conditionForCompletion.interval && !content.isComplete) {
        this.destroy$.next(false);
        let ngUnsubscribe = new Subject();
        this.learnerService.contentCurriculums$.pipe(takeUntil(ngUnsubscribe)).subscribe((contentCurriculums) => {
          let contentCurriculum = contentCurriculums.find(x => x.contentId == content.contentId);
          if (contentCurriculum) {
            ngUnsubscribe.next();
            ngUnsubscribe.complete();
            if (contentCurriculum.isLoaded) {
              countdown(content.timeToComplete * 1000, () => { this.onSubmitContent() }, 'countdown', this.destroy$);
            }
          }
        });
      }
      if (this.currentContent.typesOfContent === 'VIR') {
        this.isShowExitButton = false;
      }
      // Nếu là video thì hiển thị button mô tả video
      this.showButtonInfoVideo = (this.currentContent.typesOfContent === 'VID'
        || this.currentContent.typesOfContent === 'SOU' || this.currentContent.typesOfContent === 'VIR') ? true : false;

      if ({ ...content }.typesOfContent !== 'TES' && { ...content }.conditionsForCompletion == ConditionForCompletion.answerQuestion) {
        this.contentLearnerDetailEndpoint.getTestInfoForLearner({ ...content }.contentId).then((res) => {
          this.testInfoStudent = res;
          // console.log("this.testInfoStudent in content", res);
          this.isLoadingTestInfo = false;
          this.isShowExitButton = true;
        });
      }
      else {
        this.isShowExitButton = true;
      }

      if ({ ...content }.typesOfContent == 'TES') {
        this.learnerService.eventClickConfirmResultTest$.pipe(takeUntil(this.unsubcribe$)).subscribe(userContentTestId => {
          console.log("eventClickConfirmResultTest:", userContentTestId);
          this.userTestEndpoint.confirmResultTest(null, userContentTestId).then(userLesson => {
            if (userLesson) {
              //set isCompleted cho currentContent = true
              this.currentContent.isComplete = true;
              this.showBannerComplete = false;
              //Tìm content tiếp theo để active cho nó, người học có thể click chọn sau khi active
              let nextContent = this.nearContent(true);
              if (nextContent != undefined) {
                nextContent.isActive = true;
                //Nếu nằm trong contentGroup thì set isExpanded = true để mở expand panel
                if (nextContent.contentGroupId != null) {
                  this.listContentGroup.find(x => x.contentGroupId == nextContent.contentGroupId).isExpanded = true;
                }
              }
              // gọi lại hàm tính toán phần trăm hoàn thành và số content hoàn thành
              this.setTotalContentFinished();
              this.totalPercent = userLesson.percentageOfCompletion;
              if (userLesson.completionDate != null && userLesson.completionDate != undefined) {
                this.dialog.open(FinishLessonDialogComponent, {
                  data:
                  {
                    id: userLesson.lessonId,
                    name: this.lessonName,
                    isFromListContentView: true
                  },
                  disableClose: true
                });
                //this.dialog.open(CongratulationComponent, { width: '100%', height: '95vh' });
              }
            }
          });
        })
      }
      // navigate qua component chi tiết nội dung
      if (content.typesOfContent == ContentType.test) {
        this.router.navigateByUrl(`learner/lessons/${this.lessonId}/contents/${content.contentId}/detail-test`);
      } else {
        this.router.navigateByUrl(`learner/lessons/${this.lessonId}/contents/${content.contentId}/detail-content`);
      }
      if (window.innerWidth <= 525) {
        var elmnt = document.getElementById("content-mobile-scroll");
        elmnt.scrollIntoView({ behavior: 'smooth' });
      }
    }
    // this.checkNextAndBackButton();
  }
  /**Sự kiện click qua bài */
  onClickNextContent(): void {
    //Tìm content tiếp theo để nhảy qua
    let content = this.nearContent(true);
    //Nếu nằm trong contentGroup thì set isExpanded = true để mở expand panel
    if (content.contentGroupId != null) {
      this.listContentGroup.find(x => x.contentGroupId == content.contentGroupId).isExpanded = true;
    }
    //Gọi lại hàm onclick content
    this.onClickContent(content);
  }

  /**Sự kiện click trở lại */
  onClickBackContent(): void {
    //Tìm content trước đó để nhảy qua
    let content = this.nearContent(false);
    //Nếu nằm trong contentGroup thì set isExpanded = true để mở expand panel
    if (content.contentGroupId != null) {
      this.listContentGroup.find(x => x.contentGroupId == content.contentGroupId).isExpanded = true;
    }
    //Gọi lại hàm onclick content
    this.onClickContent(content);
  }
  onSubmitContent(): void {
    if (!this.currentContent.isComplete) {
      if (this.learnContentsResponse.formOfLearn == 'SEQ') {
        if (!this.currentContent.isComplete) {
          if (this.currentContent['lastItem']) {
            this.progressService.disableButtonNext.next(true);
          } else {
            this.progressService.disableButtonNext.next(false);
          }
        }
      }

      //Gọi api hoàn thành nội dungggggg
      //lấy curriculum từ trong component chi tiết nnooij dung đã
      // console.log("curriculumIds", this.contentCurriculums);
      let curriculumId = this.contentCurriculums.find(x => x.contentId == this.currentContent.contentId).curriculumId;
      this.contentLearnerEndpoint.submitContent({ lessonId: this.lessonId, contentId: this.currentContent.contentId, curriculumId: curriculumId }).then((userLesson) => {
        if (userLesson) {
          //set isCompleted cho currentContent = true
          this.currentContent.isComplete = true;
          this.showBannerComplete = false;
          //Tìm content tiếp theo để active cho nó, người học có thể click chọn sau khi active
          let nextContent = this.nearContent(true);
          if (nextContent != undefined) {
            nextContent.isActive = true;
            //Nếu nằm trong contentGroup thì set isExpanded = true để mở expand panel
            if (nextContent.contentGroupId != null) {
              this.listContentGroup.find(x => x.contentGroupId == nextContent.contentGroupId).isExpanded = true;
            }
          }
          // gọi lại hàm tính toán phần trăm hoàn thành và số content hoàn thành
          this.setTotalContentFinished();
          this.totalPercent = userLesson.percentageOfCompletion;
          if (userLesson.completionDate != null && userLesson.completionDate != undefined) {
            this.dialog.open(FinishLessonDialogComponent, {
              data:
              {
                id: userLesson.lessonId,
                name: this.lessonName,
                isFromListContentView: true
              },
              disableClose: true
            });
            //this.dialog.open(CongratulationComponent, { width: '100%', height: '95vh' });
          }
        }
      })
    }
  }

  public onClickOutContent() {
    // this.router.navigateByUrl(`learner/lessons/${this.lessonId}/contents/${content.contentId}/detail-content`);
    if (this.totalPercent == 100) {
      if (window.innerWidth <= 525) {
        this.dialog.open(OutContentCompleteDialogComponent, {});
      } else {
        this.dialog.open(OutContentCompleteDialogComponent, {
          width: '420px',
          position: { left: 'calc(50% - 210px)' }
        });
      }
    }
    else {
      if (window.innerWidth <= 525) {
        this.dialog.open(OutContentDialogComponent, {});
      } else {
        this.dialog.open(OutContentDialogComponent, {
          width: '420px',
          position: { left: 'calc(50% - 210px)' }
        });
      }
    }
    // this.openConfirmDialog(`Bạn chắc chắn muốn thoát hay không?`).subscribe(res => {
    //   if (res) {
    //     this.router.navigateByUrl('learner/user-learn')
    //   }
    // })
  }


  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.tabChange = tabChangeEvent.index;
    if (this.tabChange === 3) {
      if (this.isFisrtLoad) {
        this.referenceEndpoint.getListReferenceByLessonIdLearner(this.lessonId).then(res => {
          if (res) {
            this.references = [...res];
            res.map(reference => {
              const referenceDisplay = this.getReferenceDisplay(reference);
              this.referencesDisplay.push(referenceDisplay);

            });
            this.isFisrtLoad = false;
          }
        });
      }
    }
  }

  isTimeoutCurrent(testInfoStudent: TestInfoResponse): boolean {
    if (testInfoStudent.testTime == 0) return false;
    let now = new Date();
    let since = new Date(testInfoStudent.startDate);
    let diff = now.getTime() - since.getTime();
    if (diff >= (testInfoStudent.testTime * 60 * 1000)) {
      return true;
    } else {
      return false;
    }
  }
  startDoTest(): void {
    if (this.testInfoStudent.numberOfQuestion === 0) {
      // Nếu không có câu hỏi nào thì hiện popup thông báo
      this.openConfirmDialog('Nội dung bài kiểm tra đang được cập nhật, xin vui lòng quay lại sau');
    } else {
      var dialogref = this.dialog.open(BeforeAnswerDialogComponent, {
        width: '650px',
        data: this.testInfoStudent
      })
      dialogref.afterClosed().subscribe(value => {
        if (value == 'STA') {
          this.isDisableButtonComplete = true;
          let curriculumId = this.contentCurriculums.find(x => x.contentId == this.currentContent.contentId).curriculumId;
          //Nếu chưa có user-content-test hoặc đã nộp bài rồi và cho phép thi lại thì gọi api tạo mới user-content-test
          if ((this.testInfoStudent.userContentTestId == null || this.testInfoStudent.userContentTestId == 0) || (this.testInfoStudent.userContentTestId > 0 && this.canToRedo(this.testInfoStudent) && (this.testInfoStudent.statusOver == 'FAI' || this.testInfoStudent.statusOver == 'WAI'))) {
            this.userTestEndpoint.createUserContentTest(this.lessonId, this.currentContent.contentId).then((res) => {
              this.router.navigate([`learner/lessons/${this.lessonId}/contents/${this.currentContent.contentId}/curriculums/${curriculumId}/answer-questions/${res.id}`]);
              this.isDisableButtonComplete = false;
            });
          }
          else {
            this.router.navigate([`learner/lessons/${this.lessonId}/contents/${this.currentContent.contentId}/curriculums/${curriculumId}/answer-questions/${this.testInfoStudent.userContentTestId}`]);
            this.isDisableButtonComplete = false;
          }
        }
      });
    }
  }

  restartDoTest(): void {
    var dialogref = this.dialog.open(RestartAnswerDialogComponent, {
      width: '650px',
      data: this.testInfoStudent
    })
    dialogref.afterClosed().subscribe(value => {
      switch (value) {
        case "RED":
          this.isDisableButtonComplete = true;
          let curriculumIdRed = this.contentCurriculums.find(x => x.contentId == this.currentContent.contentId).curriculumId;
          //Nếu chưa có user-content-test hoặc đã nộp bài rồi và cho phép thi lại thì gọi api tạo mới user-content-test
          if ((this.testInfoStudent.userContentTestId == null || this.testInfoStudent.userContentTestId == 0) || (this.testInfoStudent.userContentTestId > 0 && this.canToRedo(this.testInfoStudent) && (this.testInfoStudent.statusOver == 'FAI' || this.testInfoStudent.statusOver == 'WAI'))) {
            this.userTestEndpoint.createUserContentTest(this.lessonId, this.currentContent.contentId).then((res) => {
              this.router.navigate([`learner/lessons/${this.lessonId}/contents/${this.currentContent.contentId}/curriculums/${curriculumIdRed}/answer-questions/${res.id}`]);
              this.isDisableButtonComplete = false;
            });
          }
          else {
            this.router.navigate([`learner/lessons/${this.lessonId}/contents/${this.currentContent.contentId}/curriculums/${curriculumIdRed}/answer-questions/${this.testInfoStudent.userContentTestId}`]);
            this.isDisableButtonComplete = false;
          }
          break;
        case "VIE":
          this.viewResult();
          break;
        case "COF":
          let curriculumId = this.contentCurriculums.find(x => x.contentId == this.currentContent.contentId).curriculumId;
          this.userTestEndpoint.confirmResultTest(curriculumId, this.testInfoStudent.userContentTestId).then(userLesson => {
            if (userLesson) {
              //set isCompleted cho currentContent = true
              this.currentContent.isComplete = true;
              this.showBannerComplete = false;
              //Tìm content tiếp theo để active cho nó, người học có thể click chọn sau khi active
              let nextContent = this.nearContent(true);
              if (nextContent != undefined) {
                nextContent.isActive = true;
                //Nếu nằm trong contentGroup thì set isExpanded = true để mở expand panel
                if (nextContent.contentGroupId != null) {
                  this.listContentGroup.find(x => x.contentGroupId == nextContent.contentGroupId).isExpanded = true;
                }
              }
              // gọi lại hàm tính toán phần trăm hoàn thành và số content hoàn thành
              this.setTotalContentFinished();
              this.totalPercent = userLesson.percentageOfCompletion;
              if (userLesson.completionDate != null && userLesson.completionDate != undefined) {
                this.dialog.open(FinishLessonDialogComponent, {
                  data:
                  {
                    id: userLesson.lessonId,
                    name: this.lessonName,
                    isFromListContentView: true
                  },
                  disableClose: true
                });
                //this.dialog.open(CongratulationComponent, { width: '100%', height: '95vh' });
              }
            }
          });
          break;
        default:
          break;
      }
    })
  }
  canToRedo(config: TestInfoResponse): boolean {
    if (config.theMaximumNumberToRedo != null) {
      if (config.numberOfRemainingAttempts > 0) {
        return true;
      }
    }
    return false;
  }
  viewResult(): void {
    this.router.navigate([`learner/lessons/${this.lessonId}/contents/${this.contentId}/result/${this.testInfoStudent.userContentTestId}`]);
  }
  // openDialog(): void {
  //   this.dialog.open(CongratulationComponent);
  // }

  private getReferenceDisplay(_reference: ReferenceResponses): ReferenceDisplay {
    let referenceDisplay = new ReferenceDisplay();
    if (_reference) {
      let data: string[] = _reference.fileName.split('.');
      referenceDisplay.type = data[data.length - 1];
      // data.pop();
      // referenceDisplay.name = data.join('.');
      referenceDisplay.link = _reference.link;
      referenceDisplay.name = _reference.name;
      $.get(referenceDisplay.link, () => {
      }).done(() => {
        referenceDisplay.isShowLink = true;
      })
        .fail(() => {
          referenceDisplay.isShowLink = false;
        })
    }
    return referenceDisplay;
  }

  openConfirmDialog(value: string): void {
    if (window.innerWidth <= 525) {
      this.dialog.open(ConfirmationDialogComponent, {
        data: {
          message: `${value}`,
          buttonText: {
            ok: 'Yes',
            cancel: 'Đóng'
          }
        },
        width: '300px'
      })
    } else {
      this.dialog.open(ConfirmationDialogComponent, {
        width: this.isFull ? '420px' : '474px',
        data: {
          message: `${value}`,
          buttonText: {
            ok: 'Yes',
            cancel: 'Đóng'
          }
        },
        position: { left: this.isFull ? 'calc(50% - 210px)' : 'calc(37.5% - 237px)' }
      });
    }
  }

  // openCongraDialog(): void {
  //   this.dialog.open(CongratulationComponent, { width: '100%', height: '95vh' });
  // }

  isShow = false;
  isDarkMode = false;
  isFull = false;
  onActivate(event) {
    if (event.emitIsShow) {
      event.emitIsShow.subscribe((data) => {
        // console.log(data);
        // Will receive the data from child here 
        this.isShow = data;
      })
    }
    if (event.emitDarkMode) {
      event.emitDarkMode.subscribe((data) => {
        this.isDarkMode = data;

        // console.log("sdsd", this.isDarkMode);
      });
    }
    if (event.emitFUllMode) {
      event.emitFUllMode.subscribe((data) => {
        this.isFull = data;
      });
    }
  }

  // Chế độ toàn màn hình (ẩn danh mục bên phải)
  onFullscreenMode(): void {
    this.isFull = !this.isFull;
    localStorage.setItem('fullscreen_learn', `${this.isFull}`);
  }

  // Show pop mô tả
  onDetailVideo(): void {
    const type = this.currentContent.typesOfContent;
    this.progressService.infoVideoEmit.next({ type, full: this.isFull });
  }

  // Load nội dung của bài giảng
  onSeenContent(): void {
    this.showBannerComplete = true;
    setTimeout(() => this.progressService.returnDurationVideo.next(true), 0);
  }

  // Trở về danh sách bài giảng
  onBackLesson(): void {
    this.router.navigate(['learner/user-learn']);
  }

  onHandleGetPercentFromLocal(list): void {
    if (list) {
      let arrLocal = [];
      if (localStorage.getItem('duration_videoyt')) {
        arrLocal = [...arrLocal, ...JSON.parse(localStorage.getItem('duration_videoyt'))];
      }
      if (localStorage.getItem('duration_videojs')) {
        arrLocal = [...arrLocal, ...JSON.parse(localStorage.getItem('duration_videojs'))];
      }
      if (localStorage.getItem('duration_audio')) {
        arrLocal = [...arrLocal, ...JSON.parse(localStorage.getItem('duration_audio'))];
      }
      if (localStorage.getItem('duration_document')) {
        arrLocal = [...arrLocal, ...JSON.parse(localStorage.getItem('duration_document'))];
      }
      if (localStorage.getItem('duration_pure')) {
        arrLocal = [...arrLocal, ...JSON.parse(localStorage.getItem('duration_pure'))];
      }
      if (arrLocal) {
        list.forEach(item => {
          arrLocal.forEach(value => {
            if (item.contentId === + value.contentID) {
              item.percentVideo = Math.round(value.percent);
            }
          });
        });
      }
    }
  }

}

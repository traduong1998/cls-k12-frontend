import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowResultDoTestComponent } from './show-result-do-test.component';

describe('ShowResultDoTestComponent', () => {
  let component: ShowResultDoTestComponent;
  let fixture: ComponentFixture<ShowResultDoTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowResultDoTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowResultDoTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { MatDialog } from '@angular/material/dialog';
import { ProgressPlayerService } from 'src/app/features/learner-lesson/Services/progress-player.service';
import { UserContentTestInfo } from 'sdk/cls-k12-sdk-js/src/services/user-test/responses/user-content-test-info-response';
import { DoTestService } from './../../Services/do-test-service.service';
import { ChangeDetectorRef, Component, HostListener, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';
import { ResultDoTestResponse } from 'sdk/cls-k12-sdk-js/src/services/user-test/responses/result-do-test-response';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { RuleTestDialogComponent } from '../../components/rule-test-dialog/rule-test-dialog.component';

@Component({
  selector: 'app-show-result-do-test',
  templateUrl: './show-result-do-test.component.html',
  styleUrls: ['./show-result-do-test.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ShowResultDoTestComponent implements OnInit, OnDestroy {
  isResult: boolean = true;
  resultDoTest: ResultDoTestResponse;
  userContentTestId: number;
  currentPage = 1;
  screenWidth;
  itemsPerPage;
  questions: QuestionTest[] = [];
  questionsResult: QuestionTest[] = [];
  userAnswer = [];
  cdr;
  totalPage: number;
  isReady: boolean = false;
  testInfo: UserContentTestInfo;
  get questionType(): typeof Type {
    return Type;
  }
  get questionFormat(): typeof FormatQuestion {
    return FormatQuestion;
  }

  constructor(private doTestService: DoTestService, public dialog: MatDialog, cdr: ChangeDetectorRef, private route: ActivatedRoute, private router: Router, private httpClient: HttpClient, private spinner: NgxSpinnerService,
		private progressService: ProgressPlayerService
    ) {
    document.body.classList.add(`learn-custom-global-css`);
    this.spinner.show();
    this.userContentTestId = +route.snapshot.paramMap.get("id");
    this.onResize();
    this.cdr = cdr;
    this.doTestService.initDataShowResult(this.userContentTestId).then(() => {

    });
    this.doTestService.isReadyResult.subscribe((isReady) => {
      if (isReady) {
        this.testInfo = this.doTestService.infoTest;
        this.itemsPerPage = this.testInfo.displayPerPage;
        this.resultDoTest = this.doTestService.resultDoTest;
        this.doTestService.Questions$.subscribe(data => {
          this.questions = this.doTestService.beauty(this.SortedQuestions(data));
          if (this.questions.length <= this.itemsPerPage) {
            this.totalPage = 1;
          }
          else {
            this.totalPage = Math.floor(this.questions.length / this.itemsPerPage);
            if (this.questions.length % this.itemsPerPage > 0) {
              this.totalPage++;
            };
          }
          if (this.testInfo.isPublicAnswer) {
            this.doTestService.getRightAnswer(this.doTestService.infoTest.lessonId, doTestService.infoTest.contentId, doTestService.infoTest.testId).then(testCodeUrl => {
              this.GetListQuestionFromUrl(testCodeUrl.testLink).subscribe((testBaseResult) => {
                this.questionsResult = this.doTestService.beauty(this.SortedQuestions(testBaseResult["questions"]));
                this.isReady = true;
                this.spinner.hide();
              })
            });
          } else {
            this.questionsResult = this.questions;
            this.isReady = true;
            this.spinner.hide();
          }
        });
        this.userAnswer = this.doTestService.getUserAnswer();
      }
    })
  }
  ngOnDestroy(): void {
    this.doTestService.clearLocalStorage();
    document.body.classList.remove(`learn-custom-global-css`);
    this.progressService.isLearner.next(true);
  }
  ngOnInit(): void {
    this.progressService.isLearner.next(false);
  }
  exitButtonHandleClick(): void {
    this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`])
  }
  public GetListQuestionFromUrl(url: string): Observable<any> {
    return this.httpClient.get(url);
  }

  SortedQuestions(questions: QuestionTest[]) {
    return questions.sort((a, b) => a.order > b.order ? 1 : a.order < b.order ? -1 : 0);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenWidth = window.innerWidth;
    // if (this.screenWidth < 960) {
    //   this.itemsPerPage = 1;
    // }
  }

  pagingQuestions() {
    let currentItem = (this.currentPage - 1) * this.itemsPerPage
    return this.questions.splice(currentItem, currentItem + this.itemsPerPage - 1);
  }
  hideloader() {
    // Setting display of spinner 
    // element to none 
    document.getElementById('loading')
      .style.display = 'none';
  }
  userAnswerById(questionId) {
    let answerQuestion = this.userAnswer.find(x => x.questionId == questionId);
    if (answerQuestion) {
      if (answerQuestion.userAnswer) {
        return answerQuestion.userAnswer;
      } else {
        return answerQuestion
      }
    } else return [];

  }
  //chuyển lên đầu trang khi chuyển trang
  pageChanged(event): void {
    this.currentPage = event;
    let firstQuestionOfPage = ((this.currentPage - 1) * this.itemsPerPage) + 1;
    setTimeout(() => {
      var element = document.getElementById("cauhoi_" + firstQuestionOfPage);
      if (element) {
        var elementPosition = element.offsetTop;
      }
      window.scrollTo({
        top: elementPosition,
        behavior: "smooth"
      });
    }, 100);
  }


  scrollToElement(index): void {
    //khi ở man hình nhỏ
    // console.log("crurent",this.currentPage);
    //tìm trang hiện tại của index
    var currentPageOfIndex = Math.floor((index + (this.itemsPerPage - 1)) / this.itemsPerPage);
    // nếu khác trang thì sẽ chuyển trang
    if (this.currentPage != currentPageOfIndex) {
      this.currentPage = currentPageOfIndex;
      setTimeout(() => {
        var element = document.getElementById("cauhoi_" + index);
        var elementPosition = element.offsetTop;
        element.scrollIntoView({ behavior: "smooth" });
        // console.log(elementPosition);
        // var current=document.getElementsByTagName('app-content-exam')[0];
        // window.scrollTo({
        //   top: elementPosition,
        //   behavior: "smooth"
        // });

      }, 300);
    } else {
      var element = document.getElementById("cauhoi_" + index);
      var elementPosition = element.offsetTop;
      element.scrollIntoView({ behavior: "smooth" });
      // var current=document.getElementsByTagName('app-content-exam')[0];
      // window.scrollTo({
      //   top: elementPosition,
      //   behavior: "smooth"
      // });
    }
  }
  onNextPage(): void {
    if (this.currentPage < this.totalPage) {
      this.currentPage++;
      this.pageChanged(this.currentPage);
    }
    // this.currentPage<this.totalPage??this.currentPage++
  }
  onBackPage(): void {
    if (this.currentPage > 0) {
      this.currentPage--;
      this.pageChanged(this.currentPage);
    }
  }

  getQuestionBaseById(questionId: number): QuestionTest {
    let qst = this.questions.find(x => x.id == questionId);
    let baseQuestion = this.questionsResult.find(x => x.id == questionId);
    baseQuestion.order = qst.order;
    return baseQuestion;
  }

  openExamRuleDialog(): void {
    let dialogRef = this.dialog.open(RuleTestDialogComponent, { panelClass: 'exam-rule-dialog', data: this.doTestService.infoTest });
  }
}

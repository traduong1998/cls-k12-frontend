import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestSelfCreatedComponent } from './test-self-created.component';

describe('TestSelfCreatedComponent', () => {
  let component: TestSelfCreatedComponent;
  let fixture: ComponentFixture<TestSelfCreatedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestSelfCreatedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestSelfCreatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

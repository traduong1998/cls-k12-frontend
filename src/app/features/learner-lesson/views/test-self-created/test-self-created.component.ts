import { BehaviorSubject, Observable } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContentLearnerDetailEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/content-learner-detail-endpoint';
import { UserTestEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-test/endpoint/user-test-endpoint';
import { NgxSpinnerService } from 'ngx-spinner';
import { TestInfoResponse } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/responses/test-info-response';
import { ProgressPlayerService } from '../../Services/progress-player.service';
import { LearnerService } from '../../Services/learnerServive.service';

@Component({
  selector: 'learner-test-self-created',
  templateUrl: './test-self-created.component.html',
  styleUrls: ['./test-self-created.component.scss']
})
export class TestSelfCreatedComponent implements OnInit, OnDestroy {
  contentLearnerDetailEndpoint: ContentLearnerDetailEndpoint;
  userTestEndpoint: UserTestEndpoint;
  lessonId: number;
  contentId: number;
  isReadySubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isReady$: Observable<boolean> = this.isReadySubject.asObservable();
  isReady: boolean = false;
  testInfoStudent: TestInfoResponse;
  isDarkMode = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private spinner: NgxSpinnerService,
    private progressService: ProgressPlayerService,
    private learnerService: LearnerService
  ) {
    this.spinner.show();
    this.isReady$.subscribe((isReady) => {
      if (isReady) {
        setTimeout(() => {
          this.isReady = isReady;
          this.spinner.hide();
        }, 700);
      }
    });
    this.route.params.subscribe((params) => {
      this.lessonId = + this.route.parent.snapshot.paramMap.get('lessonId');
      this.contentId = + this.route.snapshot.paramMap.get('id');
      this.contentLearnerDetailEndpoint = new ContentLearnerDetailEndpoint();
      this.userTestEndpoint = new UserTestEndpoint();
      this.contentLearnerDetailEndpoint.getTestInfoForLearner(this.contentId).then((res) => {
        this.testInfoStudent = res;
        // Hiển thị banner khi bài thi có số câu hỏi = 0;
        if (this.testInfoStudent?.numberOfQuestion === 0) {
          setTimeout(() => this.progressService.statusContent.next('BLANK'));
        }
        //Nếu chưa nộp bài mà hết thời gian thì nộp bài cho học sinh
        if (this.isTimeoutCurrent(this.testInfoStudent) && this.testInfoStudent.userContentTestId !== 0
          && this.testInfoStudent.statusOver === 'NOT') {
          this.userTestEndpoint.submitUserExercise({
            userContentTestId: this.testInfoStudent.userContentTestId,
            userExercisesAdd: [], userExercisesUpdate: []
          }).then((isSubmited) => {
            this.contentLearnerDetailEndpoint.getTestInfoForLearner(this.contentId).then((res) => {
              this.testInfoStudent = res;
              this.isReadySubject.next(true);
            });
          });
        } else {
          this.isReadySubject.next(true);
        }
      });
    });
  }
  ngOnDestroy(): void {
    this.spinner.hide();
  }

  ngOnInit(): void {
    this.progressService.isLightMode$.subscribe(res => this.isDarkMode = res);
  }
  isTimeoutCurrent(testInfoStudent: TestInfoResponse): boolean {
    if (testInfoStudent.testTime == 0) return false;
    let now = new Date();
    let since = new Date(testInfoStudent.startDate);
    let diff = now.getTime() - since.getTime();
    if (diff >= (testInfoStudent.testTime * 60 * 1000)) {
      return true;
    } else {
      return false;
    }
  }
  startDoTest(): void {
    //Nếu chưa có user-content-test hoặc đã nộp bài rồi và cho phép thi lại thì gọi api tạo mới user-content-test
    if ((this.testInfoStudent.userContentTestId == null || this.testInfoStudent.userContentTestId === 0)
      || (this.testInfoStudent.userContentTestId > 0 && this.canToRedo(this.testInfoStudent)
        && (this.testInfoStudent.statusOver === 'FAI' || this.testInfoStudent.statusOver === 'WAI'))) {
      this.userTestEndpoint.createUserContentTest(this.lessonId, this.contentId).then((res) => {
        this.router.navigate([`learner/lessons/${this.lessonId}/contents/${this.contentId}/start-test/${res.id}`]);
      });
    }
    else {
      this.router.navigate([`learner/lessons/${this.lessonId}/contents/${this.contentId}/start-test/${this.testInfoStudent.userContentTestId}`]);
    }

  }

  showResult(): void { }

  canToRedo(config: TestInfoResponse): boolean {
    if (config.theMaximumNumberToRedo != null) {
      if (config.numberOfRemainingAttempts > 0) {
        return true;
      }
    }
    return false;
  }

  confirmResult() : void {
    this.learnerService.clickConfirmResultTest(this.testInfoStudent.userContentTestId);
    console.log("confirmResult()");
  }

}

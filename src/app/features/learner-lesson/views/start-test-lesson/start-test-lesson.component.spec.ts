import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StartTestLessonComponent } from './start-test-lesson.component';

describe('StartTestLessonComponent', () => {
  let component: StartTestLessonComponent;
  let fixture: ComponentFixture<StartTestLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StartTestLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StartTestLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { DoTestService } from './../../Services/do-test-service.service';
import { Component, OnInit, ViewEncapsulation, OnDestroy, HostListener, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgxSpinnerService } from 'ngx-spinner';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { DOCUMENT } from '@angular/common';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
declare var CKEDITOR: any;
CKEDITOR.plugins.addExternal(
  'insertaudio',
  window.location.origin + '/assets/ckeditor/plugins/InsertAudio/',
  'InsertAudio.js');
CKEDITOR.plugins.addExternal(
  'insertimage',
  window.location.origin + '/assets/ckeditor/plugins/InsertImage/',
  'InsertImage.js');
CKEDITOR.plugins.addExternal(
  'insertvideo',
  window.location.origin + '/assets/ckeditor/plugins/InsertVideo/',
  'InsertVideo.js');

CKEDITOR.plugins.addExternal(
  'createunderline',
  window.location.origin + '/assets/ckeditor/plugins/underline/',
  'plugin.js');

CKEDITOR.plugins.addExternal(
  'fillblank1',
  window.location.origin + '/assets/ckeditor/plugins/fillblank1/',
  'plugin.js');
CKEDITOR.plugins.addExternal(
  'fillblank2',
  window.location.origin + '/assets/ckeditor/plugins/fillblank2/',
  'plugin.js');
CKEDITOR.plugins.addExternal('ckeditor_wiris', window.location.origin + '/assets/ckeditor/plugins/mathtype/', 'plugin.js');

@Component({
  selector: 'app-start-test-lesson',
  templateUrl: './start-test-lesson.component.html',
  styleUrls: ['./start-test-lesson.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StartTestLessonComponent implements OnInit, OnDestroy {
  userContentTestId: number;
  lessonId: number;
  testId: number;
  itemPerPage: number;
  isLoading: boolean;
  documentElement: any;

  @HostListener('document:keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    console.log("Click f11 out");
    console.log(event);
    if (event.keyCode == 123 || event.which == 122) { // Prevent F12
      event.preventDefault()
    }
    else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
      event.preventDefault()
    }
  }

  // @HostListener('document:contextmenu', ['$event'])
  // onRightClick(event: MouseEvent){
  //   event.preventDefault();
  // }

  constructor(@Inject(DOCUMENT) private document: any, private doTestService: DoTestService, private _route: ActivatedRoute, private _snackBar: MatSnackBar, private router: Router, private spinner: NgxSpinnerService) {
    document.body.classList.add(`learn-custom-global-css`);
    this.spinner.show();
    //this.userContentTestId = +this._route.snapshot.paramMap.get("id");
    this.lessonId = +this._route.snapshot.paramMap.get('lessonId');
    this._route.paramMap.subscribe(params => {
      this.doTestService.clearLocalStorage();
      this.userContentTestId = +params.get("id");
      console.log("userContentTestIdkk", this.userContentTestId);
      this.doTestService.initData(this.userContentTestId)
        .then(() => {
          this.isLoading = false;
          this.testId = this.doTestService.infoTest.testId;
          this.itemPerPage = this.doTestService.infoTest.displayPerPage;
        })
        .catch((err) => {
          this.spinner.hide();
          if (this.doTestService.infoTest) {
            if (this.doTestService.infoTest.isCompleted) {
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: 'Bạn đã hoàn thành bài thi',
                duration: 3000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });
              this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`]);
            }
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Có lỗi xảy ra vui lòng thử lại',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          }
        });
    })

  }

  ngAfterViewInit(): void {
    this.documentElement = document.documentElement;
    if (this.documentElement.requestFullscreen) {
      this.documentElement.requestFullscreen();
    } else if (this.documentElement.mozRequestFullScreen) {
      /* Firefox */
      this.documentElement.mozRequestFullScreen();
    } else if (this.documentElement.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      this.documentElement.webkitRequestFullscreen();
    } else if (this.documentElement.msRequestFullscreen) {
      /* IE/Edge */
      this.documentElement.msRequestFullscreen();
    }
  }
  ngOnDestroy(): void {
    document.body.classList.remove(`learn-custom-global-css`);
    this.doTestService.clearLocalStorage();
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
    }
  }

  ngOnInit(): void {
  }

  hideSpinner(isReady): void {
    if (isReady) {
      this.spinner.hide();
    }
  }
}

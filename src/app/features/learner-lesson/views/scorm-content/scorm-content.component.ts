import { Component, Input, OnInit, AfterViewInit, OnChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ContentLearnerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/contentLearnerEndpoint';
import { Content } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/models/content';
import { ConditionForCompletion } from 'src/app/features/learn/intefaces/conditionForCompletion';
import { LearnerService } from '../../Services/learnerServive.service';
import { ProgressPlayerService } from '../../Services/progress-player.service';

@Component({
  selector: 'learner-scorm-content',
  templateUrl: './scorm-content.component.html',
  styleUrls: ['./scorm-content.component.scss']
})
export class ScormContentComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() content: Content;

  lessonId: number;
  contentId: number;
  isSameType = false;
  isReady: boolean = false;
  iscompleteOfCondition: boolean = false;
  typeOfContent: string = 'SCO';
  contentLearnerEndpoint: ContentLearnerEndpoint;
  isDarkMode = false;
  fullLayout = false;

  constructor(
    private learnerService: LearnerService,
    public dialog: MatDialog,
    private progressPlayerService: ProgressPlayerService
  ) {
    this.contentLearnerEndpoint = new ContentLearnerEndpoint();
  }

  ngOnInit(): void {
    this.progressPlayerService.isLightMode$.subscribe(res => this.isDarkMode = res);
    // Lấy chế độ sáng tối từ localStorage
    if (localStorage.getItem('light_mode') === 'true') {
      setTimeout(() => { this.progressPlayerService.emitLightModeEvent(true); }, 0);
    }
    // Hiển thị fullscreen
    this.progressPlayerService.fullScreen$.subscribe(res => {
      const element = document.getElementById('scorm-frame');
      if (element && res) {
        this.launchIntoFullscreen(element);
      } else {
        this.onCloseFullscreen();
      }
    });
  }

  ngOnChanges(): void {
    // Nếu link là rỗng thì hiện banner
    if (!this.content.link) {
      setTimeout(() => this.progressPlayerService.statusContent.next('BLANK'));
    }
    this.isReady = true;
    if (this.content.conditionsForCompletion === ConditionForCompletion.answerQuestion) {
      this.iscompleteOfCondition = true;
    }
    this.learnerService.updateContentCurriculums(this.content.id, this.content.curriculumId, true);
  }

  ngAfterViewInit(): void {
    if (this.content.link) {
      this.contentLearnerEndpoint.getContent(this.content.lessonId, this.content.id).then(res => {
        if (res && res.typesOfContent === this.typeOfContent) {
          this.content = res;
        }
      });
      this.isReady = true;
    }
  }

  // Chế độ toàn màn hình với từng trình duyệt
  launchIntoFullscreen(element): void {
    this.fullLayout = true;
    if (element.requestFullscreen) {
      element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
      element.mozRequestFullScreen();
    } else if (element.webkitRequestFullscreen) {
      element.webkitRequestFullscreen();
    } else if (element.msRequestFullscreen) {
      element.msRequestFullscreen();
    }
  }

  onCloseFullscreen(): void {
    this.fullLayout = false;
    if (document['exitFullscreen']) {
      document['exitFullscreen']();
    } else if (document['webkitExitFullscreen']) {
      document['webkitExitFullscreen']();
    } else if (document['mozCancelFullScreen']) {
      document['mozCancelFullScreen']();
    } else if (document['msExitFullscreen']) {
      document['msExitFullscreen']();
    }
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScormContentComponent } from './scorm-content.component';

describe('ScormContentComponent', () => {
  let component: ScormContentComponent;
  let fixture: ComponentFixture<ScormContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScormContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScormContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

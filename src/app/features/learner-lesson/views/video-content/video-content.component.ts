import { Component, Input, OnInit, OnChanges, SimpleChanges, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ContentLearnerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/contentLearnerEndpoint';
import { Content } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/models/content';
import { ConditionForCompletion } from 'src/app/features/learn/intefaces/conditionForCompletion';
import { DecriptionContentComponent } from '../../components/decription-content/decription-content.component';
import { LearnerService } from '../../Services/learnerServive.service';
import { ProgressPlayerService } from '../../Services/progress-player.service';

@Component({
  selector: 'learner-video-content',
  templateUrl: './video-content.component.html',
  styleUrls: ['./video-content.component.scss']
})
export class VideoContentComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild('videoLearn', { static: false }) videoLearn: ElementRef<HTMLVideoElement>;
  @Input() content: Content;

  idLinkYoutube = '';
  isSameType = false;public contentLink = '';
  typeOfContent = 'VID';
  isReady = false;
  iscompleteOfCondition = false;
  typeLoad = false;
  isDarkMode = false;
  isYoutubeLink = false;
  subjectInfoVideo;

  contentLearnerEndpoint: ContentLearnerEndpoint;

  constructor(
    public dialog: MatDialog,
    private learnerService: LearnerService,
    private progressService: ProgressPlayerService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    // Nếu link video là rỗng thì hiện banner
    if (!this.content.link) {
      setTimeout(() => this.progressService.statusContent.next('BLANK'));
    }
    this.isYoutubeLink = false;
    this.onCheckUrlLinkVideo();
    this.isReady = true;
    if (this.content.conditionsForCompletion === ConditionForCompletion.answerQuestion) {
      this.iscompleteOfCondition = true;
    }
    this.learnerService.updateContentCurriculums(this.content.id, this.content.curriculumId, true);
  }
  ngOnInit(): void {
    this.progressService.isLightMode$.subscribe(res => this.isDarkMode = res);
    this.onDecriptionHandleClick();
  }

  // Check link video và truyền cho component con
  onCheckUrlLinkVideo(): void {
    if (this.content && this.content.link && this.content.link.includes('https://www.youtube.com/')) {
      this.isYoutubeLink = true;
      this.idLinkYoutube = this.content.link.split('v=')[1];
      const ampersandPosition = this.idLinkYoutube.indexOf('&');
      if (ampersandPosition !== -1) {
        this.idLinkYoutube = this.idLinkYoutube.substring(0, ampersandPosition)
      }
      setTimeout(() => { this.progressService.percentCompleteVideo.next({ link: this.idLinkYoutube, type: 'youtube' }); }, 0);
    } else {
      if (this.content.link) {
        this.progressService.percentCompleteVideo.next({ link: this.content.link, type: 'videojs' });
      }
    }
  }

  ngOnDestroy(): void {
    if (this.subjectInfoVideo) {
      this.subjectInfoVideo.unsubscribe();
    }
  }

  onDecriptionHandleClick(): void {
    this.subjectInfoVideo = this.progressService.infoVideo$.subscribe((res: any) => {
      if (res.type === 'VID') {
        if(window.innerWidth <= 525){
          this.dialog.open(DecriptionContentComponent, {data: this.content.description, width: '300px'});
        }else{
          this.dialog.open(DecriptionContentComponent, {
            width: res.full ? '420px' : '474px',
            data: this.content.description,
            position: { left: res.full ? 'calc(50% - 210px)' : 'calc(37.5% - 237px)' }
          });
        }
      }
    });
  }

}

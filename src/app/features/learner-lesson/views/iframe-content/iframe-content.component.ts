import { Component, Input, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { ContentLearnerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/contentLearnerEndpoint';
import { Content } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/models/content';
import { ConditionForCompletion } from 'src/app/features/learn/intefaces/conditionForCompletion';
import { DecriptionContentComponent } from '../../components/decription-content/decription-content.component';
import { LearnerService } from '../../Services/learnerServive.service';
import { ProgressPlayerService } from '../../Services/progress-player.service';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'learner-iframe-content',
  templateUrl: './iframe-content.component.html',
  styleUrls: ['./iframe-content.component.scss']
})
export class IframeContentComponent implements OnInit, OnChanges, OnDestroy {

  @Input() content: Content;
  private isFirstLoad = true;
  isSameType = false;
  isReady = false;
  iscompleteOfCondition = false;
  typeOfContent = 'SOU';
  isDarkMode = false;
  contentLearnerEndpoint: ContentLearnerEndpoint;
  subjectInfoVideo;

  constructor(
    public dialog: MatDialog,
    protected sanitizer: DomSanitizer,
    private learnerService: LearnerService,
    private progressPlayerService: ProgressPlayerService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.spinner.show();
    if (localStorage.getItem('light_mode') === 'true') {
      setTimeout(() => { this.progressPlayerService.emitLightModeEvent(true); }, 0);
    }
    this.progressPlayerService.isLightMode$.subscribe(res => this.isDarkMode = res);
    this.spinner.hide();
  }

  ngOnChanges(): void {
    // Nếu link rỗng thì hiển thị banner
    if (!this.content.link) {
      setTimeout(() => this.progressPlayerService.statusContent.next('BLANK'));
    } else {
      if (!this.isFirstLoad) {
        this.ngOnInit();
      }
      this.isFirstLoad = false;
      this.isReady = true;
      if (this.content.conditionsForCompletion === ConditionForCompletion.answerQuestion) {
        this.iscompleteOfCondition = true;
      }
      this.learnerService.updateContentCurriculums(this.content.id, this.content.curriculumId, true);
    }
    this.spinner.hide();
  }

  onDecriptionHandleClick(): void {
    this.subjectInfoVideo = this.progressPlayerService.infoVideo$.subscribe((res) => {
      if (res) {
        if(window.innerWidth <= 525){
          this.dialog.open(DecriptionContentComponent, {data: this.content.description, width: '300px'});
        }else{
          this.dialog.open(DecriptionContentComponent, {
            width: '474px',
            data: this.content.description,
            position: { left: 'calc(37.5% - 237px)' }
          });
        }
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subjectInfoVideo) {
      this.subjectInfoVideo.unsubscribe();
    }
  }

}

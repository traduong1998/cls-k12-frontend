import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoundContentComponent } from './sound-content.component';

describe('SoundContentComponent', () => {
  let component: SoundContentComponent;
  let fixture: ComponentFixture<SoundContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoundContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoundContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {
  Component, EventEmitter, Input, OnInit, Output, HostListener, OnChanges, OnDestroy, AfterViewInit
} from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { ContentLearnerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/contentLearnerEndpoint';
import { Content } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/models/content';
import { ConditionForCompletion } from 'src/app/features/learn/intefaces/conditionForCompletion';
import { DecriptionContentComponent } from '../../components/decription-content/decription-content.component';
import { LearnerService } from '../../Services/learnerServive.service';
import { ProgressPlayerService } from '../../Services/progress-player.service';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { takeUntil } from 'rxjs/operators';
import { FileServerEndpoint } from '../../../../../../sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';

@Component({
  selector: 'learner-sound-content',
  templateUrl: './sound-content.component.html',
  styleUrls: ['./sound-content.component.scss']
})
export class SoundContentComponent implements OnInit, OnChanges, OnDestroy, AfterViewInit {
  destroy$: ReplaySubject<any> = new ReplaySubject<any>(1);
  @Output() isFullContent: EventEmitter<any> = new EventEmitter();
  @Input() content: Content;

  // @Output() isShowContent: EventEmitter<any> = new EventEmitter();
  // @Output() isDarkMode: EventEmitter<any> = new EventEmitter();
  // @ViewChild('audioOption') audioPlayerRef: ElementRef;
  // @ViewChild('fullScreen') fullScreen: ElementRef;
  // private isFirstLoad = true;

  isPlay = false;
  isSameType = false;
  isReady = false;
  iscompleteOfCondition = false;
  typeOfContent = 'SOU';
  public soundLink = '';
  isDarkModeEmit = false;
  contentLearnerEndpoint: ContentLearnerEndpoint;
  isOnBottom;
  // Disable trình phát khi xử lý;
  isLoadDone = false;
  // trình phát âm thanh
  soundPlayer;
  currentTime: number;
  isFullScreen = false;
  isFull = false;
  fileServer: FileServerEndpoint;

  constructor(
    private progressPlayerService: ProgressPlayerService,
    public dialog: MatDialog,
    private learnerService: LearnerService,
    private spinner: NgxSpinnerService
  ) {
    this.escEvent();
    this.fileServer = new FileServerEndpoint();
  }

  @HostListener('window:mousemove', ['$event'])
  mousemove(e: any): void {
    if (e.y >= $(document).height() - 90) {
      this.isOnBottom = true;
    } else {
      this.isOnBottom = false;
    }
  }

  ngOnInit(): void {
    setTimeout(() => this.progressPlayerService.isLightMode$.subscribe(res => this.isDarkModeEmit = res), 0);
    this.progressPlayerService.returnDurationVideo$.pipe(takeUntil(this.destroy$)).subscribe(res => {
      // this.soundPlayer.load();
    });
  }

  ngOnChanges(): void {
    if (this.soundPlayer) {
      // Nếu chuyển qua lại giữa các âm thanh thì lưu lại % hoàn thành, nếu có tua thì không lưu lại
      this.progressPlayerService.changeLinkAudio.next('audio');
      // Dừng và reload lại audio;
      this.soundPlayer.pause();
      this.soundPlayer.removeAttribute('src'); // xoá src audio
      this.soundPlayer.load();
      this.initSound();
    }
    this.soundLink = this.content.link;
    // Nếu không có link thì hiển thị banner
    if (this.soundLink === '') {
      setTimeout(() => {
        this.progressPlayerService.statusContent.next('BLANK');
      }, 0);
    }
    this.isReady = false;
    if (this.content.conditionsForCompletion === ConditionForCompletion.answerQuestion) {
      this.iscompleteOfCondition = true;
    }
    this.learnerService.updateContentCurriculums(this.content.id, this.content.curriculumId, true);
    this.isReady = true;
  }

  ngAfterViewInit(): void {
    this.spinner.show();
    this.soundPlayer = document.getElementById('sound') as HTMLAudioElement;
    if (this.soundLink) {
      this.initSound();
    }
    setTimeout(() => this.isLoadDone = true);
    this.onDecriptionHandleClick();
  }

  // Lắng nghe tất cả hiện kiện ở đây
  onHandleEventSubscribe(): void {
    // Chế độ toàn màn hình
    this.progressPlayerService.fullScreen$.pipe(takeUntil(this.destroy$)).subscribe(res => {
      const element = document.getElementById('sound-player');
      if (element && res) {
        this.launchIntoFullscreen(element);
      } else {
        this.closeFullscreen();
      }
    });
    // Âm lượng
    this.progressPlayerService.volume$.pipe(takeUntil(this.destroy$)).subscribe((v) => {
      this.soundPlayer['volume'] = v / 100;
    });
    // Phát lại âm thanh
    this.progressPlayerService.replaySound$.pipe(takeUntil(this.destroy$)).subscribe((v) => {
      if (v) {
        this.soundPlayer.currentTime = 0;
        this.soundPlayer.play();
      }
    });
    // phát, tạm dừng âm thanh
    this.progressPlayerService.isPlay$.pipe(takeUntil(this.destroy$)).subscribe((p) => {
      this.isPlay = p;
      if (this.isPlay) {
        this.soundPlayer.play();
      }
      else {
        this.soundPlayer.pause();
      }
    });
    // Thời gian hiện tại của âm thanh
    this.progressPlayerService.currentTimeSending$.pipe(takeUntil(this.destroy$)).subscribe((duration) => {
      // this.isSeekToVideo = true;
      this.soundPlayer.pause();
      this.currentTime = duration;
      this.soundPlayer.currentTime = this.currentTime;
    });

    // Tạm dừng và chạy lại
    this.progressPlayerService.stopAndPlay$.pipe(takeUntil(this.destroy$)).subscribe((res) => {
      if (res) {
        this.soundPlayer.play();
      }
    });
    // Tốc độ phát âm thanh
    this.progressPlayerService.speedDoc$.pipe(takeUntil(this.destroy$)).subscribe((value) => {
      if (this.isPlay) {
        if (value === 2) {
          this.soundPlayer.playbackRate = 0.5;
        }
        if (value === 0.5) {
          this.soundPlayer.playbackRate = 2;
        }
        if (value === 1) {
          this.soundPlayer.playbackRate = value;
        }
      }
    });
    // Tua tới [s] giây
    this.progressPlayerService.next$.pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        this.soundPlayer.currentTime += value;
      });
    // Lùi lại [s] giây
    this.progressPlayerService.previous$.pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        this.currentTime -= value;
        this.soundPlayer.currentTime -= value;
        console.table(this.currentTime, this.soundPlayer.currentTime);
      });

    // Tắt/ bật tiếng
    this.progressPlayerService.muted$.pipe(takeUntil(this.destroy$))
      .subscribe((p) => this.soundPlayer.muted = p);
  }

  // Thiết lập trình phát
  initSound(): void {
    this.soundPlayer.onloadedmetadata = () => {
      if (this.soundPlayer.duration) {
        this.soundPlayer.pause();
        this.progressPlayerService.emitTimeEvent(this.soundPlayer.duration);
      }
      this.trackingAudio();
    };
    // Nếu audio đang xử lý thì hiển thị banner
    this.soundPlayer.addEventListener('error', this.checkStatusSound());
  }

  trackingAudio(): void {
    this.soundPlayer.addEventListener('timeupdate', (event) => {
      this.progressPlayerService.emitCurrentTimeEvent((event.target as HTMLAudioElement).currentTime);
      // Nếu hết âm thanh thì hiển thị nút replay
      if ((event.target as HTMLAudioElement).ended) {
        this.progressPlayerService.emitPlayEvent.next(false);
        this.progressPlayerService.endingAudio.next(true);
        this.soundPlayer.pause();
      }
    });
    this.onHandleEventSubscribe();
  }

  // Kiểm tra link và gọi api
  checkStatusSound(): void {
    // convert link;
    const link = (this.soundLink.split('/audio/').pop()).split('/origin.mp3')[0];
    if (link) {
      this.fileServer.getStatusFile(link).then(res => {
        if (res) {
          // Gửi trạng thái của audio để hiển thị ở learn component
          this.progressPlayerService.statusContent.next(res);
        }
      });
    }
  }

  // Hiển thị popup mô tả âm thanh
  onDecriptionHandleClick(): void {
    this.progressPlayerService.infoVideo$.pipe(takeUntil(this.destroy$))
      .subscribe((res: any) => {
        if (res.type === 'SOU') {
          if(window.innerWidth <= 525){
            this.dialog.open(DecriptionContentComponent, {data: this.content.description, width: '300px'});
          }else{
            this.dialog.open(DecriptionContentComponent, {
              width: res.full ? '420px' : '474px',
              data: this.content.description,
              position: { left: res.full ? 'calc(50% - 210px)' : 'calc(37.5% - 237px)' }
            });
          }
        }
      });
  }

  getFullScreenStatus(): void {
    this.progressPlayerService.fullScreen$.pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        this.isFull = value;
        this.isFullContent.emit(value);
      });
  }

  // Chế độ toàn màn hình với từng trình duyệt
  launchIntoFullscreen(element): void {
    this.isFullScreen = true;
    if (element.requestFullscreen) {
      element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
      element.mozRequestFullScreen();
    } else if (element.webkitRequestFullscreen) {
      element.webkitRequestFullscreen();
    } else if (element.msRequestFullscreen) {
      element.msRequestFullscreen();
    }
  }

  closeFullscreen(): void {
    this.isFullScreen = false;
    if (document.exitFullscreen) {
      document.exitFullscreen();
    }
  }

  escEvent(): void {
    document.addEventListener('fullscreenchange', () => {
      if (document.fullscreenElement) {
      } else {
        this.isFullScreen = false;
        this.progressPlayerService.emitFullScreenEvent(false);
      }
    });
  }

  // Tracking khi audio đang chạy
  handleDurationAudio(value): void {
    const player = document.getElementById('sound') as HTMLAudioElement;
    if (value) {
      player.currentTime = value.duration;
      this.currentTime = value.duration;
      this.progressPlayerService.percentCompleteSound.next(value.id);
      player.pause();
      player.removeAttribute('src'); // empty source
      player.load();
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next(null);
    this.destroy$.complete();
    if (this.soundPlayer) {
      this.soundPlayer.pause();
      this.soundPlayer.removeAttribute('src');
    }
  }

  // Lấy tất cả thuộc tính của audio-player
  getAllProperties(): void {
    const player = {
      type: 'audio-player',
      link: this.soundLink,
    };
    this.progressPlayerService.allPropertiesPlayer.next(player);
  }

}

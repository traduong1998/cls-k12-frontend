import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PureContentComponent } from './pure-content.component';

describe('PureContentComponent', () => {
  let component: PureContentComponent;
  let fixture: ComponentFixture<PureContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PureContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PureContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

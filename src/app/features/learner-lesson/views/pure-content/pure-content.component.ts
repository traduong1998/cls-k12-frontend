import { Component, Input, OnInit, SimpleChanges, HostListener, ViewChild, ElementRef, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Content } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/models/content';
import { ConditionForCompletion } from 'src/app/features/learn/intefaces/conditionForCompletion';
import { DecriptionContentComponent } from '../../components/decription-content/decription-content.component';
import { LearnerService } from '../../Services/learnerServive.service';
import { ProgressPlayerService } from '../../Services/progress-player.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';

@Component({
  selector: 'learner-pure-content',
  templateUrl: './pure-content.component.html',
  styleUrls: ['./pure-content.component.scss']
})
export class PureContentComponent implements OnInit {
  destroy: ReplaySubject<any> = new ReplaySubject<any>(1);

  @Output() isShowContent: EventEmitter<any> = new EventEmitter();
  @Output() isDarkMode: EventEmitter<any> = new EventEmitter();
  @Output() isFullContent: EventEmitter<any> = new EventEmitter();

  isPlay: boolean;
  progress;
  percent = 0;

  lightMode;
  isLightMode: boolean;
  top = 0;
  @ViewChild('scrollframe') scroll: ElementRef;

  @Input() content: Content;

  private isFirstLoad = true;

  lessonId: number;
  contentId: number;
  typeOfContent: string = 'TEX';
  isSameType = false;
  isReady: boolean;
  iscompleteOfCondition: boolean = false;

  totalHeight;
  progressPercent;

  speed = 20;
  checkMobile: boolean = false;

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    // this.width = window.innerWidth;
    // //this.screenWidth = window.innerWidth;
    debugger
    if (window.innerWidth > 600) {
      this.checkMobile = false;
    } else {
      this.checkMobile = true;
    }
  }

  constructor(
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private _learnerService: LearnerService,
    private _progressPlayerService: ProgressPlayerService,
    private spinner: NgxSpinnerService
  ) {
    this.onResize();
    this.escEvent();
  }
  ngOnInit(): void {
    this.spinner.show();
    this.onCheckBlankContent();
    this.getSpeed();
    this.getPlayEvent();
    this.getStatusLightMode();
    this.getValueNext();
    this.getValuePrevious();
    this.getActionZoomText();
    this.getFullScreenStatus();

    if (localStorage.getItem('light_mode') === 'true') {
      this.isLightMode = true;
      setTimeout(() => { this._progressPlayerService.emitLightModeEvent(true) }, 0);
    }
    this.spinner.hide();
  }

  @HostListener("document:scroll", ['$event'])

  //topOld: number;

  isPause = false;
  onWindowScroll(e) {
    if (this.isPlay) {
      if (e.target.scrollTop == this.top) {
        this.isPause = false;
      }
      else {
        this.isPause = true;
        //this._progressPlayerService.emitPlayEvent.next(false);
      }

      if (this.isPause == true) {
        setTimeout(() => {
          // this._progressPlayerService.emitPlayEvent.next(true);
        }, 100);

        //this._progressPlayerService.emitPlayEvent.next(true);
      }
    }
    this.top = e.target.scrollTop;
    this.percent = (this.top / this.totalHeight) * 100;
    this._progressPlayerService.emitPercent.next({ percent: this.percent, page: 1, totalPage: 1 });
  }
  isOnBottom = false;
  openProgressFullScreen() {
    if (this.isFullScreen == true) {
      document.addEventListener('mousemove', () => {

        if (this.isOnBottom == false && this.isFullScreen == true) {
          this.isOnBottom = true;
          setTimeout(() => {
            if (this.isFull == true) {
              this.isOnBottom = false;
            }
          }, 3000);
        }

      })
    }
  }

  // @HostListener('window:mousemove', ['$event'])
  // mousemove(e: any) {
  //   this.isOnBottom = true;
  //   setTimeout(() => {
  //     if (this.isFull == true) {
  //       this.isOnBottom = false;
  //     }
  //   }, 3000);

  //   console.log("this.isOnBottom", this.isOnBottom)
  // }

  // @HostListener('window:mouseover', ['$event'])
  // mouseover(e: any) {
  //   // if (e.y >= $(document).height()) {
  //   //   this.isOnBottom = true;
  //   // } else {
  //   //   this.isOnBottom = false;
  //   // }
  //   this.isOnBottom = false;

  //   console.log("this.mouseout", this.isOnBottom)
  // }

  //auto
  startProgress(speed: number) {

    this.progress = setInterval(() => {
      if (this.percent <= 100) {
        //this.topOld = this.top;
        this.top = this.top + 1;

        this.scroll.nativeElement.scroll({
          top: this.top,
          left: 0,
          behavior: 'auto'
        });
      } else {
        clearInterval(this.progress);
      }
    }, speed)
  }

  pauseProgress() {
    clearInterval(this.progress);
  }

  getTotalHeight() {

    var elmnt = document.getElementById("scroll-content");
    if (elmnt) {
      this.totalHeight = this.scroll.nativeElement.scrollHeight - elmnt.offsetHeight;
      // this.percent = 0;
      if (this.totalHeight <= 0 && this.content.detailsOfContent !== '') {
        this.percent = 100;
        this._progressPlayerService.emitIsDisableEvent(true);
      } else {
        this._progressPlayerService.emitIsDisableEvent(false);
      }
      this._progressPlayerService.emitPercent.next({ percent: this.percent, page: 1, totalPage: 1 });
    }
  }

  getPlayEvent() {
    this._progressPlayerService.isPlay$.subscribe((p) => {
      this.isPlay = p;
      if (this.isPlay) {
        this.startProgress(this.speed);
      }
      else {
        this.pauseProgress();
      }
    })
  }

  //tốc độ phát
  getSpeed() {
    this._progressPlayerService.speedDoc$.pipe(takeUntil(this.destroy)).subscribe((value) => {
      this.speed = value * 20;
      if (this.isPlay) {
        this.pauseProgress();
        this.startProgress(value * 20);
      }
    })
  }

  getPercent() {
    this.progressPercent = this._progressPlayerService.percentScroll$.subscribe((p) => {

      let top = (+p * this.totalHeight) / 100;
      this.top = top;
      this.scroll.nativeElement.scroll({
        top: top,
        left: 0,
        behavior: 'auto'
      });

    })
  }

  //true =>đèn sang
  //fasle => tắt đèn
  getStatusLightMode() {
    this.lightMode = this._progressPlayerService.isLightMode$.pipe(takeUntil(this.destroy)).subscribe((status) => {
      this.isLightMode = status;
    })
  }

  //next
  getValueNext() {
    this._progressPlayerService.next$.pipe(takeUntil(this.destroy)).subscribe((value) => {

      // cộng thêm percent => chuyển qua bên kia
      this.percent = this.percent + +value;
      this._progressPlayerService.emitPercent.next({ percent: this.percent, page: 1, totalPage: 1 });
      // tính lại cái top mới
      let top = (this.percent * this.totalHeight) / 100;
      this.top = top;
      this.scroll.nativeElement.scroll({
        top: top,
        left: 0,
        behavior: 'smooth'
      });
    })
  }
  //previous
  getValuePrevious() {
    this._progressPlayerService.previous$.pipe(takeUntil(this.destroy)).subscribe((value) => {
      // trừ thêm percent => chuyển qua bên kia
      if (this.percent >= value) {
        this.percent = this.percent - +value;
      }
      else {
        this.percent = 0
      }
      this._progressPlayerService.emitPercent.next({ percent: this.percent, page: 1, totalPage: 1 });
      // tính lại cái top mới
      let top = (this.percent * this.totalHeight) / 100;
      this.top = top;
      this.scroll.nativeElement.scroll({
        top: top,
        left: 0,
        behavior: 'smooth'
      });
    })
  }
  size: number;
  lineHeight: number;
  width: number;
  //tăng giảm text
  getActionZoomText() {
    this._progressPlayerService.zoomText$.pipe(takeUntil(this.destroy)).subscribe((value) => {
      let contentText = document.getElementById("content-text");
      let contentPage = document.getElementById("content-page");
      this.size = parseInt(window.getComputedStyle(contentText, null).getPropertyValue('font-size'));
      this.lineHeight = parseInt(window.getComputedStyle(contentText, null).getPropertyValue('line-height'));
      this.width = parseInt(window.getComputedStyle(contentPage, null).getPropertyValue('max-width'));
      if (value == true) {
        contentText.style.fontSize = (this.size + 2) + 'px';
        contentText.style.lineHeight = (this.lineHeight + 2) + 'px';
        this.width = this.width + 20;
      }
      else {
        contentText.style.fontSize = (this.size - 2) + 'px';
        contentText.style.lineHeight = (this.lineHeight - 2) + 'px';
        this.width = this.width - 20;
        if (this.size <= 16) {
          contentText.style.fontSize = '16px';
          contentText.style.lineHeight = '22px';
          this.width = 1050;
        }
      }

      setTimeout(() => {
        this.getTotalHeight();
        this.percent = (this.scroll.nativeElement.scrollTop / this.totalHeight) * 100;
        this._progressPlayerService.emitPercent.next({ percent: this.percent, page: 1, totalPage: 1 });
        console.log(this.percent)
      }, 0);
    })
  }

  ngOnDestroy(): void {
    this.destroy.next(null);
    this._progressPlayerService.resetSubject();
    if (this.percent > +localStorage.getItem('percent')) {
      // localStorage.setItem("percent", this.percent.toString());
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log("ngOnchangesPure");
    this._progressPlayerService.changeLinkPure.next('pure');
    this.onCheckBlankContent();
    this._progressPlayerService.resetSubject();
    this.isReady = true;
    if (this.content.conditionsForCompletion == ConditionForCompletion.answerQuestion) {
      this.iscompleteOfCondition = true;
    }
    this._learnerService.updateContentCurriculums(this.content.id, this.content.curriculumId, true);
  }
  ngAfterViewInit() {
    setTimeout(() => {
      this.getTotalHeight();

      this._progressPlayerService.emitPercentClickEvent(this.percent);
    }, 1000);

    this.getPercent();
    this.cdr.detectChanges();
  }
  public onDecriptionHandleClick() {
    if (window.innerWidth <= 525) {
      this.dialog.open(DecriptionContentComponent, { data: this.content.description, width: '300px' });
    } else {
      const dialogRef = this.dialog.open(DecriptionContentComponent, {
        width: '474px',
        data: this.content.description
      });
    }
  }

  destroyRightClick() {
    document.addEventListener("contextmenu", function (e) {
      e.preventDefault();
    }, false);
    document.addEventListener("keydown", function (e) {
      //document.onkeydown = function(e) {
      // "I" key
      if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
        disabledEvent(e);
      }
      // "J" key
      if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
        disabledEvent(e);
      }
      // "S" key + macOS
      if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
        disabledEvent(e);
      }
      // "U" key
      if (e.ctrlKey && e.keyCode == 85) {
        disabledEvent(e);
      }
      // "F12" key
      if (e.keyCode == 123) {
        disabledEvent(e);
      }
    }, false);

    function disabledEvent(e) {
      if (e.stopPropagation) {
        e.stopPropagation();
      } else if (window.event) {
        window.event.cancelBubble = true;
      }
      e.preventDefault();
      return false;
    }
  }

  isShow = false;
  ShowMenuContent() {
    this.isShow = !this.isShow;
    this.isShowContent.emit(this.isShow);
  }
  isFull = false;
  getFullScreenStatus() {
    this._progressPlayerService.fullScreen$.pipe(takeUntil(this.destroy)).subscribe((value) => {
      this.isFull = value;
      if (value) {
        let element = document.getElementById('full');
        this.launchIntoFullscreen(element);
      }
      else {
        if (document.exitFullscreen) {
          document.exitFullscreen();
        }
      }
    })
  }
  isFullScreen = false;
  // Chế độ toàn màn hình với từng trình duyệt
  launchIntoFullscreen(element): void {
    this.isFullScreen = true;
    if (element.requestFullscreen) {
      element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
      element.mozRequestFullScreen();
    } else if (element.webkitRequestFullscreen) {
      element.webkitRequestFullscreen();
    } else if (element.msRequestFullscreen) {
      element.msRequestFullscreen();
    }

    this.openProgressFullScreen();
  }

  escEvent() {
    document.addEventListener('fullscreenchange', (event) => {
      if (document.fullscreenElement) {
      } else {
        this.isFullScreen = false;
        this.isFull = false;
        this._progressPlayerService.emitFullScreenEvent(false);
      }
    });
  }

  onCheckBlankContent(): void {
    if (!this.content.detailsOfContent) {
      setTimeout(() => this._progressPlayerService.statusContent.next('BLANK'));
    }
  }

  handleDurationPure(value): void {
    // console.log(`value`, value);

    // const player = document.getElementById('sound') as HTMLAudioElement;
    if (value) {
      // player.currentTime = value.duration;
      this.percent = value.duration;
      this._progressPlayerService.emitPercentClickEvent(this.percent);

      this._progressPlayerService.idPureLocalStorage.next(value.id);
    }
  }

  getAllProperties(): void {
    const doc = {
      type: 'pure',
      id: this.content.id,
    };
    this._progressPlayerService.allPropertiesPure.next(doc);
  }
}

import { MeetingDetailandSignature } from './../../../../../../sdk/cls-k12-sdk-js/src/services/learner-lesson/models/meeting-detail-and-signature';
import { Component, HostListener, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContentLearnerDetailEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/content-learner-detail-endpoint';
import { CLS, UserEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { AuthService } from 'src/app/core/services';
import { DomSanitizer } from '@angular/platform-browser';
export class ZoomMeeting {
  name: string
  mn: string
  email: string
  pwd: string
  role: string
  lang: string
  signature: string
  china: string
  apiKey: string
}

@Component({
  selector: 'app-zoom-meeting',
  templateUrl: './zoom-meeting.component.html',
  styleUrls: ['./zoom-meeting.component.scss']
})
export class ZoomMeetingComponent implements OnInit {

  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT

  virtualClassroomEndpoint: ContentLearnerDetailEndpoint;

  //#endregion

  //#region PUBLIC PROPERTY
  public baseUrl: string;
  linkZoomUrl: string = '';
  zoomMeetingId: string;
  lessonId: number;
  contentId: number;
  zoomMeeting: any;
  endpointUser: UserEndpoint;
  userIdentity: UserIdentity;
  apiKey = 'IcL6-LMxTx-QhTFw2MMkIw'
  meetingNumber = ''
  leaveUrl = 'http://localhost:4200'
  userName = ''
  userEmail = 'lovenco0410@gmail.com'
  passWord = ''
  signature = '';
  data: ZoomMeeting = new ZoomMeeting();
  //#endregion

  //#region PRIVATE PROPERTY
  //#endregion

  //#region CONSTRUCTOR
  constructor(private router: ActivatedRoute,
    private _authService: AuthService,
    private sanitizer: DomSanitizer) {

    this.lessonId = +this.router.snapshot.paramMap.get('lessonId');
    this.contentId = +this.router.snapshot.paramMap.get('id');
    this.zoomMeetingId = this.router.snapshot.paramMap.get('meetingId');
    this.virtualClassroomEndpoint = new ContentLearnerDetailEndpoint();
    this.endpointUser = new UserEndpoint();
    this.baseUrl = CLS.getConfig().apiBaseUrl;
  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {
    this.getMeetingDetailAndGenarateSignature();
    this.userIdentity = this._authService.getTokenInfo();
    this.userName = this.userIdentity.fullName;
  }

  ngAfterViewInit() {
  }
  //#endregion

  //#region PUBLIC METHOD
  public getLinkZoomResourceURL() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.linkZoomUrl);
  }

  @HostListener("window:message", ["$event"])
  SampleFunction($event: MessageEvent) {

    console.log($event.data)// {foo:"foo"}
    if ($event.data?.leave) {
      window.close();
    }
  }
  //#endregion

  //#region PRIVATE METHOD
  private getMeetingDetailAndGenarateSignature() {
    this.virtualClassroomEndpoint
      .getZoomMeetingDetailLearner(this.lessonId, this.contentId, this.zoomMeetingId)
      .then((res: MeetingDetailandSignature) => {
        if (res) {
          this.data.apiKey = res.meeting.apiKey
          this.meetingNumber = res.meeting.id.toString();
          this.data.china = "0";
          this.data.email = res.email;
          this.data.name = this.userIdentity.fullName;
          this.passWord = res.meeting.password;
          this.signature = res.signature;
          this.data.role = '0'; // TODO : Change value in here
          this.linkZoomUrl = `http://localhost:59909/api/v1/learners/${this.lessonId}/contents/${this.contentId}/ZoomMeetings/join-meeting?name=${this.userName}&mn=${this.meetingNumber}&email=${this.data.email}&pwd=${this.passWord}&role=0&lang=vi-VN&signature=${res.signature}&china=0&apiKey=${this.data.apiKey}&lessonId=${this.lessonId}&contentId=${this.contentId}`
        }
      }).then(() => {
      })
  }
  //#endregion
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StartAnswerQuestionComponent } from './start-answer-question.component';

describe('StartAnswerQuestionComponent', () => {
  let component: StartAnswerQuestionComponent;
  let fixture: ComponentFixture<StartAnswerQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StartAnswerQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StartAnswerQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

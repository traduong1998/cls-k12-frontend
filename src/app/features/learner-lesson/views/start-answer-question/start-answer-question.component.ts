import { DoTestService } from './../../Services/do-test-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, HostListener, OnInit, ViewEncapsulation, AfterViewInit, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-start-answer-question',
  templateUrl: './start-answer-question.component.html',
  styleUrls: ['./start-answer-question.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class StartAnswerQuestionComponent implements OnInit, AfterViewInit {
  userContentTestId: number;
  curriculumId: number;
  lessonId: number;
  testId: number;
  itemPerPage: number;
  isLoading: boolean;
  documentElement: any;

  // @HostListener('document:keydown', ['$event'])
  // onKeyDown(event: KeyboardEvent) {
  //   console.log("Click f11 out");
  //   console.log(event);
  //   if (event.keyCode == 123 || event.which == 122) { // Prevent F12, F11
  //     event.preventDefault()
  //   }
  //   else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
  //     event.preventDefault()
  //   }
  // }

  // @HostListener('document:contextmenu', ['$event'])
  // onRightClick(event: MouseEvent){
  //   event.preventDefault(); // Prevent right click  
  // }

  constructor(@Inject(DOCUMENT) private document: any,  private doTestService: DoTestService, private _route: ActivatedRoute, private _snackBar: MatSnackBar, private router: Router, private spinner: NgxSpinnerService) {
    document.body.classList.add(`learn-custom-global-css`);
    this.spinner.show();
    //this.userContentTestId = +this._route.snapshot.paramMap.get("id");
    this.curriculumId = +this._route.snapshot.paramMap.get("curriculumId");
    this.lessonId = +this._route.snapshot.paramMap.get('lessonId');
    this._route.paramMap.subscribe(params => {
      this.doTestService.clearLocalStorage();
      this.userContentTestId = +params.get("id");
      this.doTestService.initData(this.userContentTestId)
      .then(() => {
        this.isLoading = false;
        this.testId = this.doTestService.infoTest.testId;
        this.itemPerPage = this.doTestService.infoTest.displayPerPage;
      })
      .catch((err) => {
        this.spinner.hide();
        if (this.doTestService.infoTest) {
          if(this.doTestService.infoTest.isCompleted){
            // this._snackBar.open("Bạn đã hoàn thành bài thi!", 'OK', {
            //   duration: 3000,
            //   panelClass: ['green-snackbar'],
            //   horizontalPosition: 'center',
            //   verticalPosition: 'top',
            // });
            this.router.navigate([`learner/lessons/${this.doTestService.infoTest.lessonId}/contents/${this.doTestService.infoTest.contentId}/detail-test`]);
          }
        } else {
          this._snackBar.open("Có lỗi xảy ra vui lòng thử lại", 'OK', {
            duration: 5000,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });
        }
      });
    })

  }
  ngAfterViewInit(): void {
    this.documentElement = document.documentElement;
    if (this.documentElement.requestFullscreen) {
      this.documentElement.requestFullscreen();
    } else if (this.documentElement.mozRequestFullScreen) {
      /* Firefox */
      this.documentElement.mozRequestFullScreen();
    } else if (this.documentElement.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      this.documentElement.webkitRequestFullscreen();
    } else if (this.documentElement.msRequestFullscreen) {
      /* IE/Edge */
      this.documentElement.msRequestFullscreen();
    }
  }
  ngOnDestroy(): void {
    document.body.classList.remove(`learn-custom-global-css`);
    this.doTestService.clearLocalStorage();
    if (this.document.exitFullscreen) {
      this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      this.document.msExitFullscreen();
    }
  }

  ngOnInit(): void {
  }

  hideSpinner(isReady): void {
    if(isReady){
      this.spinner.hide();
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-learner-lesson',
  templateUrl: './learner-lesson.component.html',
  styleUrls: ['./learner-lesson.component.scss']
})
export class LearnerLessonComponent implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit(): void {
  }

  public onTestRoutingHandleClick() {
    this._router.navigate(['/learner/lessons/video-content']);
  }
}

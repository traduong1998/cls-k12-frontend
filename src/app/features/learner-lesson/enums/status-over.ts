export enum StatusOver {
    notYet = 'NOT',
    fail = 'FAI',
    pass = 'PAS',
    wait = 'WAI'
}
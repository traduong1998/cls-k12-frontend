import { DoTestService } from './Services/do-test-service.service';
import { LearnMessageService } from './../learn/Services/learn-message-service.service';
import { LearnerService } from './Services/learnerServive.service';
import { NgModule } from '@angular/core';
import { LearnerLessonRoutingModule } from './learner-lesson-routing.module';
import { LearnerLessonComponent } from './learner-lesson.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TestSelfCreatedComponent } from './views/test-self-created/test-self-created.component';
import { IframeContentComponent } from './views/iframe-content/iframe-content.component';
import { ScormContentComponent } from './views/scorm-content/scorm-content.component';
import { VideoContentComponent } from './views/video-content/video-content.component';
import { SoundContentComponent } from './views/sound-content/sound-content.component';
import { DocumentContentComponent } from './views/document-content/document-content.component';
import { LearnerContentsLessonComponent } from './views/learner-contents-lesson/learner-contents-lesson.component';
import { PureContentComponent } from './views/pure-content/pure-content.component';
import { CommonModule } from '@angular/common';
import { DetailsContentComponent } from './views/details-content/details-content.component';
import { DecriptionContentComponent } from './components/decription-content/decription-content.component';
import { OnlineClassroomContentComponent } from './views/online-classroom-content/online-classroom-content.component';
import { StartTestLessonComponent } from './views/start-test-lesson/start-test-lesson.component';
import { ContentDoTestComponent } from './components/content-do-test/content-do-test.component';
import { NotificationComponent } from './components/notification/notification.component';
import { ResultTestComponent } from './components/result-test/result-test.component';
import { RuleTestDialogComponent } from './components/rule-test-dialog/rule-test-dialog.component';
import { ShowResultDoTestComponent } from './views/show-result-do-test/show-result-do-test.component';
import { EndTimeDialogComponent } from './components/end-time-dialog/end-time-dialog.component';
import { EssayComponent } from './components/essay/essay.component';
import { GroupQuestionComponent } from './components/group-question/group-question.component';
import { MatchingComponent } from './components/matching/matching.component';
import { MatchingMobileComponent } from './components/matching-mobile/matching-mobile.component';
import { MissingWord1Component } from './components/missing-word1/missing-word1.component';
import { MissingWord2Component } from './components/missing-word2/missing-word2.component';
import { MultipleChoiceComponent } from './components/multiple-choice/multiple-choice.component';
import { SingleChoiceComponent } from './components/single-choice/single-choice.component';
import { TableQuestionComponent } from './components/table-question/table-question.component';
import { TableQuestionMobileComponent } from './components/table-question-mobile/table-question-mobile.component';
import { TableQuestionResultComponent } from './components/table-question-result/table-question-result.component';
import { TableQuestionResultMobileComponent } from './components/table-question-result-mobile/table-question-result-mobile.component';
import { TrueFalseComponent } from './components/true-false/true-false.component';
import { TrueFalseClauseComponent } from './components/true-false-clause/true-false-clause.component';
import { UnderlineComponent } from './components/underline/underline.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxPaginationModule } from 'ngx-pagination';
import { CKEditorModule } from 'ckeditor4-angular';
import { FormsModule } from '@angular/forms';
import { MathModule } from 'src/app/shared/math/math.module';
import { AngularCountdownTimerModule } from 'angular8-countdown-timer';
import { ContextMenuModule } from 'ngx-contextmenu';
import { ChatTabLessonComponent } from './components/chat-tab-lesson/chat-tab-lesson.component';
import { QuestionAndAskComponent } from './components/question-and-ask/question-and-ask.component';
import { NgxPrettyDateModule } from 'ngx-pretty-date';
import { StartAnswerQuestionComponent } from './views/start-answer-question/start-answer-question.component';
import { ContentAnswerQuestionComponent } from './components/content-answer-question/content-answer-question.component';
import { CongratulationComponent } from './components/congratulation/congratulation.component';
import { CircleProgressOptions, NgCircleProgressModule } from 'ng-circle-progress';
import { OutContentDialogComponent } from './components/out-content-dialog/out-content-dialog.component';
import { FinishLessonDialogComponent } from './components/finish-lesson-dialog/finish-lesson-dialog.component';
import { OutContentCompleteDialogComponent } from './components/out-content-complete-dialog/out-content-complete-dialog.component';
import { ModalSuccessLessonComponent } from './components/modal-success-lesson/modal-success-lesson.component';
import { ModalContentLessonComponent } from './components/modal-content-lesson/modal-content-lesson.component';
import { RestartDotestDialogComponent } from './components/restart-dotest-dialog/restart-dotest-dialog.component';
import { BeforeAnswerDialogComponent } from './components/before-answer-dialog/before-answer-dialog.component';
import { ResultTestDialogComponent } from './components/result-test-dialog/result-test-dialog.component';
import { RestartAnswerDialogComponent } from './components/restart-answer-dialog/restart-answer-dialog.component';


@NgModule({
  declarations: [
    LearnerLessonComponent,
    TestSelfCreatedComponent,
    PureContentComponent,
    VideoContentComponent,
    ScormContentComponent,
    IframeContentComponent,
    SoundContentComponent,
    DocumentContentComponent,
    LearnerContentsLessonComponent,
    DetailsContentComponent,
    DecriptionContentComponent,
    OnlineClassroomContentComponent,
    StartTestLessonComponent,
    ContentDoTestComponent,
    NotificationComponent,
    ResultTestComponent,
    RuleTestDialogComponent,
    ShowResultDoTestComponent,
    EndTimeDialogComponent,
    EssayComponent,
    GroupQuestionComponent,
    MatchingComponent,
    MatchingMobileComponent,
    MissingWord1Component,
    MissingWord2Component,
    MultipleChoiceComponent,
    SingleChoiceComponent,
    TableQuestionComponent,
    TableQuestionMobileComponent,
    TableQuestionResultComponent,
    TableQuestionResultMobileComponent,
    TrueFalseComponent,
    TrueFalseClauseComponent,
    UnderlineComponent,
    ChatTabLessonComponent,
    QuestionAndAskComponent,
    StartAnswerQuestionComponent,
    ContentAnswerQuestionComponent,
    CongratulationComponent,
    ModalSuccessLessonComponent,
    ModalContentLessonComponent,
    OutContentDialogComponent,
    FinishLessonDialogComponent,
    OutContentCompleteDialogComponent,
    RestartDotestDialogComponent,
    BeforeAnswerDialogComponent,
    ResultTestDialogComponent,
    RestartAnswerDialogComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    LearnerLessonRoutingModule,
    NgCircleProgressModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    CKEditorModule,
    FormsModule,
    MathModule,
    NgxPrettyDateModule,
    AngularCountdownTimerModule,
    ContextMenuModule.forRoot()
  ],
  providers: [
    LearnerService,
    LearnMessageService,
    DoTestService,
    CircleProgressOptions
  ]
})
export class LearnerLessonModule { }


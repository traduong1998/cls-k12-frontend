import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgressPlayerService {

  //phần trăm hoàn thành
  emitPercent = new Subject<any>();
  percent$ = this.emitPercent.asObservable();

  //phần trăm hoàn thành
  emitPercentScroll = new Subject<any>();
  percentScroll$ = this.emitPercentScroll.asObservable();

  // sự kiến play/ pause
  emitPlayEvent = new BehaviorSubject(false);
  isPlay$ = this.emitPlayEvent.asObservable();

  //light-mode
  private emitLightMode = new Subject<boolean>();
  public isLightMode$ = this.emitLightMode.asObservable();

  //next
  private emitNext = new Subject<number>();
  public next$ = this.emitNext.asObservable();

  //previous
  private emitPrevious = new Subject<number>();
  public previous$ = this.emitPrevious.asObservable();

  //âm thanh
  private emitVolume = new Subject<number>();
  public volume$ = this.emitVolume.asObservable();

  //muted
  private emitMuted = new Subject<boolean>();
  public muted$ = this.emitMuted.asObservable();

  // thời gian
  private emitTime = new Subject<number>();
  public time$ = this.emitTime.asObservable();

  //thời gian hiện tại
  public emitCurrentTime = new Subject<number>();
  public currentTime$ = this.emitCurrentTime.asObservable();

  // bắn thời gian hiện tại
  private emitCurrentTimeSending = new Subject<number>();
  public currentTimeSending$ = this.emitCurrentTimeSending.asObservable();

  // tạm dừng và play trình phát
  public stopAndPlay = new Subject<boolean>();
  public stopAndPlay$ = this.stopAndPlay.asObservable();

  // Replay sound
  public replaySound = new Subject<boolean>();
  public replaySound$ = this.replaySound.asObservable();

  // disable progress
  private emitIsDisable = new Subject<boolean>();
  public isDisable$ = this.emitIsDisable.asObservable();

  //zoom + chữ
  private emitZoomText = new Subject<boolean>();
  public zoomText$ = this.emitZoomText.asObservable();

  //full screen
  private emitFullScreen = new Subject<boolean>();
  public fullScreen$ = this.emitFullScreen.asObservable();

  // bắn sự kiện click back content
  private emitBackContent = new Subject<boolean>();
  public backContent$ = this.emitBackContent.asObservable();

  // bắn sự kiện click next content
  private emitNextContent = new Subject<boolean>();
  public nextContent$ = this.emitNextContent.asObservable();

  // bắn sự kiện thông tin video [videojs, youtube]
  public infoVideoEmit = new Subject();
  public infoVideo$ = this.infoVideoEmit.asObservable();

  // đặt background image cho trang học
  public backgroundEmit = new Subject<string>();
  public bgImage$ = this.backgroundEmit.asObservable();

  // disable nút qua bài và trở lại -- linh fix
  public disableButtonNext = new BehaviorSubject<boolean>(false);
  public disableButtonNext$ = this.disableButtonNext.asObservable();

  public disableButtonBackEmit = new BehaviorSubject<boolean>(false);
  public disableButtonBack$ = this.disableButtonBackEmit.asObservable();

  // Là trang learn thì ẩn header 
  public isLearner = new Subject<boolean>();
  public isLearner$ = this.isLearner.asObservable();

  // phần trăm hoàn thành video [videojs, youtube];
  public percentCompleteVideo = new Subject();
  public percentCompleteVideo$ = this.percentCompleteVideo.asObservable();

  // phần trăm hoàn thành video [videojs, youtube];
  public percentCompleteVideoOut = new Subject();
  public percentCompleteVideoOut$ = this.percentCompleteVideoOut.asObservable();

  // phần trăm hoàn thành doc [videojs, youtube];
  public percentCompleteDocOut = new Subject();
  public percentCompleteDocOut$ = this.percentCompleteDocOut.asObservable();

  // phần trăm hoàn thành âm thanh
  public percentCompleteSound = new Subject();
  public percentCompleteSound$ = this.percentCompleteSound.asObservable();

  // Lấy số giây của video
  public returnDurationVideo = new Subject<boolean>();
  public returnDurationVideo$ = this.returnDurationVideo.asObservable();

  // kết thúc video
  public endingAudio = new Subject<boolean>();
  public endingAudio$ = this.endingAudio.asObservable();

  // khi click 2 link audio liên tục
  public changeLinkAudio = new Subject<string>();
  public changeLinkAudio$ = this.changeLinkAudio.asObservable();

  // khi click 2 link audio liên tục
  public allPropertiesPlayer = new Subject();
  public allPropertiesPlayer$ = this.allPropertiesPlayer.asObservable();

  // khi click 2 link document liên tục
  public changeLinkDocument = new Subject<any>();
  public changeLinkDocument$ = this.changeLinkDocument.asObservable();

  // khi click 2 link document liên tục
  public allPropertiesDocument = new Subject();
  public allPropertiesDocument$ = this.allPropertiesDocument.asObservable();

  // nội dung đang xử lý
  public statusContent = new Subject<string>();
  public statusContent$ = this.statusContent.asObservable();

  //bắn sự kiện tốc độ phát video
  public emitSpeedDoc = new Subject<number>();
  public speedDoc$ = this.emitSpeedDoc.asObservable();

  // link document local %;
  public linkDocumentLocalStorage = new Subject();
  public linkDocumentLocalStorage$ = this.linkDocumentLocalStorage.asObservable();

  // id nội dung thuần local%;
  public idPureLocalStorage = new Subject();
  public idPureLocalStorage$ = this.idPureLocalStorage.asObservable();

  // khi click 2 link pure liên tục
  public changeLinkPure = new Subject<string>();
  public changeLinkPure$ = this.changeLinkPure.asObservable();

  // khi click 2 link pure liên tục
  public allPropertiesPure = new Subject();
  public allPropertiesPure$ = this.allPropertiesPure.asObservable();

  constructor() { }

  emitSpeedDocEvent(value: number) {
    this.emitSpeedDoc.next(value);
  }

  emitFullScreenEvent(value) {
    this.emitFullScreen.next(value);
  }
  //tăng kích thước chữ
  //true = tawng
  //false = giamr
  emitZoomTextEvent(value) {
    this.emitZoomText.next(value);
  }

  // cho thanh disable
  emitIsDisableEvent(value) {
    this.emitIsDisable.next(value);
  }

  //bắn % progress-player
  emitPercentClickEvent(percent: number): void {
    this.emitPercentScroll.next(percent);
  }


  //bắn % progress-player
  emitPercentEvent(percent: number): void {
    this.emitPercent.next(percent);
  }

  //bắn sự kiện click light-mode
  emitLightModeEvent(status: boolean): void {
    this.emitLightMode.next(status);
  }

  emitNextEvent(value: number) {
    this.emitNext.next(value);
  }

  emitPreviousEvent(value: number) {
    this.emitPrevious.next(value);
  }
  //=> media
  emitVolumeEvent(value: number) {
    this.emitVolume.next(value);
  }

  emitMutedEvent(value: boolean) {
    this.emitMuted.next(value);
  }

  emitTimeEvent(value: number) {
    this.emitTime.next(value);
  }

  emitCurrentTimeEvent(value: number) {
    this.emitCurrentTime.next(value);
  }

  emitCurrentTimeSendingEvent(value: number) {
    this.emitCurrentTimeSending.next(value);
  }

  emitBackContentEvent(value) {
    this.emitBackContent.next(value);
  }

  emitNextContentEvent(value) {
    this.emitNextContent.next(value);
  }

  resetSubject() {
    this.emitMuted.next();
    this.emitCurrentTimeSending.next();

    this.emitVolume.next();
    // this.emitNext.next();
    // this.emitPrevious.next();
    // this.emitLightMode.next();
    // this.emitPlayEvent.next(false);
    this.emitIsDisable.next();
    this.emitNextContent.next();
    this.emitBackContent.next();
    // this.emitSpeedDoc.next();
    this.emitTime.next(0);
    this.emitCurrentTime.next(0);
    this.emitCurrentTimeSending.next(0);


    // this.emitSpeedDoc.next();

    // this.emitFullScreen.next();

    // this.emitZoomText.next();

    // this.emitIsDisable.next();

    // this.emitPercentScroll.next();

    // this.emitPercent.next();

    // this.emitLightMode.next();

    // this.emitNext.next();

    // this.emitPrevious.next();

    // this.emitVolume.next();

    // this.emitMuted.next();

    // this.emitTime.next(0);

    // this.emitCurrentTime.next(0);

    // this.emitCurrentTimeSending.next(0);

    // this.emitBackContent.next();
    // this.infoVideoEmit.next();
    // this.emitNextContent.next();

  }
}

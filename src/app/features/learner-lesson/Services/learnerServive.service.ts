import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { ContentCurriculum } from '../interfaces/contentCurriculum';
import { VirtualClassRoomOnClickResponse } from '../interfaces/virtualClassRoomOnClickResponse';
@Injectable()
export class LearnerService{
    private contentCurriculums: ContentCurriculum[];
    private contentCurriculumsSubject: BehaviorSubject<ContentCurriculum[]> = new BehaviorSubject<ContentCurriculum[]>([]);
    private eventClickStartVirtualClassRoomSubject: Subject<VirtualClassRoomOnClickResponse> = new Subject<VirtualClassRoomOnClickResponse>();
    private eventClickConfirmResultTestSubject: Subject<number> = new Subject<number>();

    public contentCurriculums$: Observable<ContentCurriculum[]> = this.contentCurriculumsSubject.asObservable();
    public eventClickStartVirtualClassRoom$: Observable<VirtualClassRoomOnClickResponse> = this.eventClickStartVirtualClassRoomSubject.asObservable();
    public eventClickConfirmResultTest$: Observable<number> = this.eventClickConfirmResultTestSubject.asObservable();


    constructor() {
        this.contentCurriculums = [];
    }

    /**Get curriculumId by contentId, return -1 if failed */
    getCurriculumByContentId(contentId: number): number {
        let contentCurriculum = this.contentCurriculums.find(x => x.contentId == contentId);
        if (contentCurriculum != undefined) {
            return contentCurriculum.curriculumId;
        }
        else return -1;
    }

    clickStartVirtualClassRoom(event: VirtualClassRoomOnClickResponse){
        this.eventClickStartVirtualClassRoomSubject.next(event);
    }

    clickConfirmResultTest(userContentTestId: number){
        this.eventClickConfirmResultTestSubject.next(userContentTestId);
        console.log("clickConfirmResultTest()");
    }
    updateContentCurriculums(contentId: number, curriculumId: number, isLoaded?: boolean): void {
        let contentCurriculum = this.contentCurriculums.find(x => x.contentId == contentId);
        if (contentCurriculum != undefined) {
            this.contentCurriculums.map(x => {
                if (x.contentId == contentId) {
                    x.curriculumId = curriculumId;
                    if (isLoaded != null && isLoaded != undefined) {
                        x.isLoaded = isLoaded;
                    }
                }
            });
        }
        else {
            this.contentCurriculums.push({ contentId: contentId, curriculumId: curriculumId, isLoaded: (isLoaded != null && isLoaded != undefined) ? isLoaded : false });
        }
        this.contentCurriculumsSubject.next(this.contentCurriculums);
    }

    updateIsLoadedContent(contentId: number, isLoaded: boolean): boolean {
        let contentCurriculum = this.contentCurriculums.find(x => x.contentId == contentId);
        if (contentCurriculum != undefined) {
            this.contentCurriculums.map(x => {
                if (x.contentId == contentId) x.isLoaded = isLoaded;
            });
            this.contentCurriculumsSubject.next(this.contentCurriculums);
            return true;
        } else return false;
    }
}

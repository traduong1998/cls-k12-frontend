import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LearnerLessonComponent } from './learner-lesson.component';

describe('LearnerLessonComponent', () => {
  let component: LearnerLessonComponent;
  let fixture: ComponentFixture<LearnerLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LearnerLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LearnerLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

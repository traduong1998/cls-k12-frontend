export interface VirtualClassRoomOnClickResponse {
    contentId: number;
    curriculumId: number;
    virtualClassRoomId: number;
}
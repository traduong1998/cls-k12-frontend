import { ShowResultDoTestComponent } from './views/show-result-do-test/show-result-do-test.component';
import { TestSelfCreatedComponent } from './views/test-self-created/test-self-created.component';
import { LearnerContentsLessonComponent } from './views/learner-contents-lesson/learner-contents-lesson.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsContentComponent } from './views/details-content/details-content.component';
import { StartTestLessonComponent } from './views/start-test-lesson/start-test-lesson.component';
import { StartAnswerQuestionComponent } from './views/start-answer-question/start-answer-question.component';

const routes: Routes = [
  {
    path: ':lessonId/contents',
    component: LearnerContentsLessonComponent,
    data: {
      title: 'Danh sách nội dung',
    },
    children: [
      {
        path: ':id/detail-content',
        component: DetailsContentComponent,
        data: {
          title: 'Thông tin nội dung',
        }
      }, {
        path:':id/detail-test',
        component: TestSelfCreatedComponent,
        data: {
          title: 'Thông tin bài kiểm tra',
        }
      }
    ]
  },
  {
    path: ':lessonId/contents/:contentId/start-test/:id',
    component: StartTestLessonComponent,
    data: {
      title: 'Làm bài kiểm tra',
    }
  },
  {
    path: ':lessonId/contents/:contentId/result/:id',
    component: ShowResultDoTestComponent,
    data: {
      title: 'Kết quả bài làm',
    }
  },
  {
    path: ':lessonId/contents/:contentId/curriculums/:curriculumId/answer-questions/:id',
    component: StartAnswerQuestionComponent,
    data: {
      title: 'Trả lời câu hỏi',
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LearnerLessonRoutingModule { }

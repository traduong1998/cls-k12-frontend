import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { CLSModules, CLSPermissions, UserEndpoint, UserIdentity, UserTypeEndpoint } from 'cls-k12-sdk-js/src';
import { DivisionEndpoint } from 'sdk/cls-k12-sdk-js/src/services/division/endpoints/DivisionEndpoint';
import { DivisionOption } from 'sdk/cls-k12-sdk-js/src/services/division/models/Division';
import { GradeEndpoint } from 'sdk/cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { GroupStudentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/endpoints/GroupStudentEndpoint';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { SchoolEndpoint } from 'sdk/cls-k12-sdk-js/src/services/school/endpoints/SchoolEndpoint';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { User } from 'sdk/cls-k12-sdk-js/src/services/user/models/User';
import { UsersFilterRequests } from 'sdk/cls-k12-sdk-js/src/services/user/models/UserPagingRequest';
import { FormControl } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { UserTypeOption } from 'sdk/cls-k12-sdk-js/src/services/usertype/models/UserType';
import { AppConfigService, AuthService } from 'src/app/core/services';

import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { NgxSpinnerService } from 'ngx-spinner';

const usersPaginationDefault: PaginatorResponse<User> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UsersComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';

  @ViewChild('dpFromDate') dpFromDate;
  @ViewChild('dpToDate') dpToDate;

  endpointUserType: UserTypeEndpoint;
  endpointUser: UserEndpoint;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointGroupStudent: GroupStudentEndpoint;
  userIdentity: UserIdentity;
  levelManageValue: number = 0;
  currentSchoolYear: number;

  hasAddPermission: boolean;
  hasDeletePermission: boolean;
  hasEditPermission: boolean;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  userFilterClone;
  constructor(private spinner: NgxSpinnerService, private _authService: AuthService, public dialog: MatDialog, private configService: AppConfigService, private router: Router, private _snackBar: MatSnackBar) {
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUserType = new UserTypeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUser = new UserEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();

    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.User, CLSPermissions.Add);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.User, CLSPermissions.Edit);
    this.hasDeletePermission = this.userIdentity.hasPermission(CLSModules.User, CLSPermissions.Delete);

    console.log("Tài khoản cấp: ", this.userIdentity.levelManage);
    console.log("Vai trò: ", this.userIdentity.userTypeRole);//'SUPER'
    //console.log(this.userIdentity);

    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
      // Tài khoản cấp Trường, schoolid fillter mặc định theo id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
      this.groupStudentFilter.schoolId = this.userIdentity.schoolId;
      this.usersPaginationFilter.schoolId = this.userIdentity.schoolId;
    }

    this.currentSchoolYear = this.configService.getConfig().unit.currentSchoolYear;

    this.spinner.show();
  }

  isShowFilter = true;
  isFirstLoadUserTypes = true;
  isFirstLoadDivisions = true;
  isFirstLoadSchools = true;
  isFirstLoadGrades = true;
  isFirstLoadGroupStudents = true;

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  groupStudentFilter = {
    schoolId: null,
    gradeId: null
  }

  public schoolYearCtrl: FormControl = new FormControl('');
  public statusCtrl: FormControl = new FormControl('ATV');
  public txtFullName: FormControl = new FormControl('');
  public fromDateCtrl: FormControl = new FormControl('');
  public toDateCtrl: FormControl = new FormControl('');

  protected userTypes: UserTypeOption[];
  public userTypeCtrl: FormControl = new FormControl();
  public userTypeFilterCtrl: FormControl = new FormControl();
  public filteredUserTypes: ReplaySubject<UserTypeOption[]> = new ReplaySubject<UserTypeOption[]>(1);
  @ViewChild('singleSelectUserType', { static: true }) singleSelectUserType: MatSelect;

  /** list of divisions */
  protected divisions: DivisionOption[];
  /** control for the selected division */
  public divisionCtrl: FormControl = new FormControl();
  /** control for the MatSelect filter keyword */
  public divisionFilterCtrl: FormControl = new FormControl();
  /** list of divisions filtered by search keyword */
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;


  protected schools: SchoolOption[];
  public schoolCtrl: FormControl = new FormControl();
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected groupStudents: GroupStudentOption[];
  public groupStudentCtrl: FormControl = new FormControl();
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;

  protected _onDestroy = new Subject<void>();

  // displayedColumns: string[] = ['id', 'stt', "name", "username", "userTypeName", "schoolName", "getStatus", "getActivedDate", "action"];
  displayedColumns: string[] = ['stt', "name", "username", "schoolName", "userTypeName", "date", "function"];
  usersPagination: PaginatorResponse<User> = usersPaginationDefault;
  usersPaginationFilter: UsersFilterRequests = {
    pageNumber: 1,
    sizeNumber: 10,
    divisionId: null,
    fromDate: null,
    getCount: true,
    gradeId: null,
    groupStudentId: null,
    keyFullName: null,
    keyUsername: null,
    schoolId: null,
    schoolYear: null,
    sortDirection: 'DESC',
    sortField: 'CRE',
    status: 'ATV',
    toDate: null,
    userTypeId: null
  }

  selection = new SelectionModel<User>(true, []);
  selectedUserIds = [];
  isLoadingResults = true;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit(): void {
    this.schoolYearCtrl.setValue(this.currentSchoolYear + '');
    this.getUsersData();

    // listen for search field value changes
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });

    // listen for search field value changes
    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    // listen for search field value changes
    this.groupStudentFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudents();
      });

    // listen for search field value changes
    this.userTypeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterUserTypes();
      });
  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.usersPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.usersPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.getUsersData();
    });
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }

  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        // triggers initializing the selection according to the initial value of
        // the form control (i.e. _initializeSelection())
        // this needs to be done after the filteredDivisions are loaded initially
        // and after the mat-option elements are available
        this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id;
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id;
      });

    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
      });

    this.filteredGroupStudents
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGroupStudent.compareWith = (a: GroupStudentOption, b: GroupStudentOption) => a && b && a.id === b.id;
      });
  }
  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterUserTypes() {
    if (!this.userTypes) {
      return;
    }
    // get the search keyword
    let search = this.userTypeFilterCtrl.value;
    if (!search) {
      this.filteredUserTypes.next(this.userTypes.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredUserTypes.next(
      this.userTypes.filter(userType => userType.name.toLowerCase().indexOf(search) > -1)
    );
  }

  failureCallback() {
    this.isLoadingResults = false;
    alert("hihi 400");
  }

  getUsersData() {
    this.isLoadingResults = true;
    this.endpointUser.getUsersByPaging(this.usersPaginationFilter)
      .then(res => {
        this.spinner.hide();
        this.isLoadingResults = false;
        this.usersPagination = res;
        this.userFilterClone = { ...this.usersPaginationFilter };
      })
      .catch(this.failureCallback);
  }

  changeCheck(event, row) {
    if (event.checked) {
      let index = this.selectedUserIds.indexOf(row.id);
      if (index == -1) {
        this.selectedUserIds.push(row.id);
      }
    } else {
      let index = this.selectedUserIds.indexOf(row.id);
      this.selectedUserIds = this.selectedUserIds.splice(index, 1);
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.usersPagination.items.length;
    return numSelected === numRows;
  }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.selectedUserIds = [];
    } else {
      this.usersPagination.items.forEach(row => {
        this.selection.select(row);
        this.selectedUserIds.push(row.id);
      })
    }
  }
  checkboxLabel(row?: User): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }

  onUserTypeSelectClicked() {
    if (this.isFirstLoadUserTypes) {
      this.endpointUserType.getUserTypeOptions("")
        .then(res => {
          this.isFirstLoadUserTypes = false;
          this.userTypes = res;
          this.filteredUserTypes.next(this.userTypes.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.divisions = res;
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionCtrl.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchools || this.schoolFilter.divisionId != divisionIdSelected) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchools = false;
          this.schools = res;
          this.schoolFilter.divisionId = divisionIdSelected;
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(

        )
    } else {

    }
  }
  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolCtrl.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrades || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch(

        )
    } else {

    }
  }
  onGroupStudentSelectClicked() {
    var schoolIdSelected = this.schoolCtrl.value;
    var gradeIdSelected = this.gradeCtrl.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGroupStudents || this.groupStudentFilter.schoolId != schoolIdSelected
      || this.groupStudentFilter.gradeId != gradeIdSelected) {
      this.endpointGroupStudent.getGroupStudentOptions(schoolIdSelected, gradeIdSelected)
        .then(res => {
          this.isFirstLoadGroupStudents = false;
          this.groupStudents = res;
          this.groupStudentFilter.gradeId = gradeIdSelected;
          this.groupStudentFilter.schoolId = schoolIdSelected;
          this.filteredGroupStudents.next(this.groupStudents.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onChangeDivisionSelected(id: any) {
    this.resetSchoolSelectCtrl();
    this.resetGradeSelectCtrl();
    this.resetGroupStudentSelectCtrl();
  }
  onChangeSchoolSelected(id: any) {
    this.resetGradeSelectCtrl();
    this.resetGroupStudentSelectCtrl();
  }
  onChangeGradeSelected(id: any) {
    this.resetGroupStudentSelectCtrl();
  }

  resetSchoolSelectCtrl() {
    this.schools = [];
    this.isFirstLoadSchools = true;
    this.schoolCtrl.setValue(undefined);
  }
  resetGradeSelectCtrl() {
    this.grades = [];
    this.isFirstLoadGrades = true;
    this.gradeCtrl.setValue(undefined);
  }
  resetGroupStudentSelectCtrl() {
    this.groupStudents = [];
    this.isFirstLoadGroupStudents = true;
    this.groupStudentCtrl.setValue(undefined);
  }

  openDpFromDate() {
    this.dpFromDate.open();
  }
  openDpToDate() {
    this.dpToDate.open();
  }

  getDateStringFormat(date: Date) {
    if (date) {
      var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [year, month, day].join('-');
    }
    return "";
  }

  getFilterParam() {
    this.usersPaginationFilter.userTypeId = this.userTypeCtrl.value;
    this.usersPaginationFilter.divisionId = this.divisionCtrl.value;
    this.usersPaginationFilter.schoolId = this.schoolCtrl.value;

    if (this.userIdentity.levelManage == "DVS") {
      this.usersPaginationFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.usersPaginationFilter.divisionId = this.userIdentity.divisionId;
      this.usersPaginationFilter.schoolId = this.userIdentity.schoolId;
    }

    this.usersPaginationFilter.gradeId = this.gradeCtrl.value;
    this.usersPaginationFilter.groupStudentId = this.groupStudentCtrl.value;
    this.usersPaginationFilter.fromDate = this.getDateStringFormat(this.fromDateCtrl.value);
    this.usersPaginationFilter.toDate = this.getDateStringFormat(this.toDateCtrl.value);
    this.usersPaginationFilter.keyFullName = this.txtFullName.value;
    this.usersPaginationFilter.schoolYear = this.schoolYearCtrl.value;
    this.usersPaginationFilter.status = this.statusCtrl.value;
    this.usersPaginationFilter.pageNumber = 1;
    this.paginator.pageIndex = 0;
  }

  onSubmitFilter() {
    this.getFilterParam();
    if (this.objHasChanged(this.userFilterClone, this.usersPaginationFilter)) {
      this.getUsersData();
    }
  }
  objHasChanged(obj, obj2) {
    return JSON.stringify(obj) != JSON.stringify(obj2);
  }
  onDeleteUserClicked(userDelete) {
    // const dialogRef = this.dialog.open(DeleteUserConfirmdialogComponent, {
    //   width: '650px',
    //   height: 'auto',
    //   disableClose: true,
    //   data: userDelete
    // });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result !== undefined) {
    //     if (result === 'submit') {
    //       /**Nếu là submit sẽ load lại data */
    //       this.getUsersData();
    //     }
    //   }
    // });
    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      disableClose: true,
      width: '396px',
      height: 'auto',
      data: {
        title: 'Xóa người dùng', // xóa cái gì
        name: (userDelete.username),// tên đối tượng cần xóa
        message: 'Xin lưu ý hành động này không thể được khôi phục trong tương lai' //nội dung message chi tiết
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == undefined) {
        //todo
      }
      else if (result == 'DONGY') {
        // gọi api xóa
        this.endpointUser.deleteUser(userDelete.id)
          .then(res => {
            if (res) {
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: 'Xóa người dùng thành công!',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
              this.getUsersData();
            } else {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Xóa người dùng không thành công. Vui lòng thử lại!',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            }
          })
          .catch((err: ErrorResponse) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: err.errorDetail,
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
      }
      else {
        // nhâp sai
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Mã xác nhận chưa chính xác. Vui lòng kiểm tra lại!',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    });
  }

  public addTeacherFromFileHandleClick() {
    this.router.navigateByUrl(`dashboard/manage-users/add-teachers-from-file`)
  }
  public addStudentFromFileHandleClick() {
    this.router.navigateByUrl(`dashboard/manage-users/add-students-from-file`)
  }

  clearDate(field: string) {
    if (field === 'from') {
      this.fromDateCtrl.setValue('');
    } else {
      this.toDateCtrl.setValue('');
    }
  }

  getFirstName(fullName: string): string {
    fullName = fullName.trim();
    let lastIndex = fullName.lastIndexOf(' ');
    if (lastIndex != -1) {
      return fullName[lastIndex + 1];
    }
    return fullName[0];
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTeacherFromFileComponent } from './add-teacher-from-file.component';

describe('AddTeacherFromFileComponent', () => {
  let component: AddTeacherFromFileComponent;
  let fixture: ComponentFixture<AddTeacherFromFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTeacherFromFileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTeacherFromFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

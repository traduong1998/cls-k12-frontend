import { _isNumberValue } from '@angular/cdk/coercion';
import { ChangeDetectorRef, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { iif, ReplaySubject, Subject } from 'rxjs';
import { pairwise, startWith, take, takeUntil } from 'rxjs/operators';
import { CLS, DivisionEndpoint, DivisionOption, GroupStudentEndpoint, SchoolEndpoint, UserEndpoint, UserIdentity, UserTypeEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { TeacherFromFile } from 'sdk/cls-k12-sdk-js/src/services/user/models/teacher-from-file';
import { CreateTeachersFromFile } from 'sdk/cls-k12-sdk-js/src/services/user/requests/create-teachers-from-file';
import { CreateTeacherErrorResponse, ErrorResult } from 'sdk/cls-k12-sdk-js/src/services/user/responses/create-teacher-error-response';
import { UserTypeOption } from 'sdk/cls-k12-sdk-js/src/services/usertype/models/UserType';
import { AuthService } from 'src/app/core/services';
import { Configuration, TIMEOUT_SCROLL } from 'src/app/shared/configurations';
import { isValidUserName, isValidLengthString } from 'src/app/shared/helpers/cls.helper';
import { isEmail, isNullOrWhiteSpace, isPhoneNum } from 'src/app/shared/helpers/validation.helper';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import * as XLSX from 'xlsx';


@Component({
  selector: 'app-add-teacher-from-file',
  templateUrl: './add-teacher-from-file.component.html',
  styleUrls: ['./add-teacher-from-file.component.scss']
})
export class AddTeacherFromFileComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('scrollBottom') scrollBottom: ElementRef;
  protected _onDestroy = new Subject<void>();
  config = { baseUrl: '' }
  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT
  private requestForm: CreateTeachersFromFile = {
    userTypeId: null,
    divisionId: null,
    schoolId: null,
    teachersData: [],
  }
  private endpointUserType: UserTypeEndpoint;
  private endpointDivision: DivisionEndpoint;
  private endpointSchool: SchoolEndpoint;
  private endpointUser: UserEndpoint;
  userIdentity: UserIdentity;

  //#endregion

  //#region PRIVATE PROPERTY
  public isWrongFormat = false;
  public isDisableSendButton = true;
  private maxColumn = 200;
  private maleStringVN = 'nam'
  private feMaleStringVN = 'nữ'
  public file: File;
  private oldKeys: string[] = ['STT', 'Họ', 'Tên (*)', 'Mã định danh (*)', 'Email', 'Ngày sinh (ngày/tháng/năm)', 'Số điện thoại', 'Giới tính', 'Địa chỉ'];
  private newKeys: string[] = ['no', 'lastName', 'firstName', 'userName', 'email', 'birthday', 'phone', 'gender', 'address'];
  private isFirstLoadUserTypes: boolean = true;
  private isFirstLoadDivisions: boolean = true;
  private isFirstLoadSchools: boolean = true;
  //#endregion

  //#region PUBLIC PROPERTY
  public ishasData = null;
  public isHasFile = true;
  public isLoadTableData = false;
  dataSourceError: CreateTeacherErrorResponse[];
  dataSource: MatTableDataSource<CreateTeacherErrorResponse>;
  public isMaxColumn: boolean = false;
  public errorCount: number = 0;
  public isShowInformation: boolean = false;
  public totalUpCount: number = 0;
  public userTypeLevelManage: string;
  public isSchoolLevelUserType: boolean = true;
  public isDivisionLevelUserType: boolean = true;
  public isDepartmentLevelUserType: boolean = true;
  public levelManageValue: number = 0;
  public nameFileUpload: string = '';
  addTeacherFromFile: FormGroup;
  divisionRequired: boolean;
  protected userTypes: UserTypeOption[];
  public userTypeCtrl: FormControl = new FormControl('', Validators.required);
  public userTypeFilterCtrl: FormControl = new FormControl();
  public filteredUserTypes: ReplaySubject<UserTypeOption[]> = new ReplaySubject<UserTypeOption[]>(1);
  @ViewChild('singleSelectUserType', { static: true }) singleSelectUserType: MatSelect;

  protected divisions: DivisionOption[];
  public divisionCtrl: FormControl = new FormControl('', Validators.required);
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  protected schools: SchoolOption[];
  public schoolCtrl: FormControl = new FormControl('', Validators.required);
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  protected groupStudents: GroupStudentOption[];
  public groupStudentCtrl: FormControl = new FormControl();
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;

  public displayedColumns = ['position', 'column', 'firstName', 'lastName', 'userName', 'email', 'infoError'];

  schoolFilter = {
    divisionId: null
  }
  //#endregion

  //#region CONSTRUCTOR
  constructor(private _authService: AuthService,
    private _snackBar: MatSnackBar,
    private router: Router,
    private cdr: ChangeDetectorRef,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.addTeacherFromFile = new FormGroup({
    });
    this.userIdentity = _authService.getTokenInfo();

    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
    }

    this.baseApiUrl = CLS.getConfig().apiBaseUrl;
  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {
    this.endpointUserType = new UserTypeEndpoint(this.config);
    this.endpointDivision = new DivisionEndpoint(this.config);
    this.endpointSchool = new SchoolEndpoint();
    this.endpointUser = new UserEndpoint();
    this.userIdentity = new UserIdentity();

    // listen for search field value changes
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });

    // listen for search field value changes
    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });


    // listen for search field value changes
    this.groupStudentFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudents();
      });

    // listen for search field value changes
    this.userTypeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterUserTypes();
      });
  }

  ngAfterViewInit() {
    if (this.dataSourceError) {
      this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
      this.paginator.page.subscribe(() => {
        this.paginator.pageIndex
        this.paginator.pageSize
        this.dataSource = new MatTableDataSource<CreateTeacherErrorResponse>(this.dataSourceError);
        this.dataSource.paginator = this.paginator;
      });
    }

    this.userTypeCtrl.valueChanges
      .pipe(startWith(this.userTypeCtrl.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            if (this.userTypeCtrl.value) {
              let userType = this.userTypes.find(x => x.id === this.userTypeCtrl.value)
              if (userType !== null) {
                this.userTypeLevelManage = this.userTypes.find(x => x.id === this.userTypeCtrl.value).levelManage;

                this.isSchoolLevelUserType = this.userTypeLevelManage == "SCH";
                this.isDivisionLevelUserType = this.userTypeLevelManage == "DVS";
                this.isDepartmentLevelUserType = this.userTypeLevelManage == "DPM";
                this.onChangeUserTypeSelected();
              }
            }
          }
        }
      )

    this.divisionCtrl.valueChanges
      .pipe(startWith(this.divisionCtrl.value),
        pairwise())
      .subscribe(
        ([old, value]) => {

          if (old != value) {
            this.isDisableSendButton = false;
          }
        }
      )

    this.divisionCtrl.valueChanges
      .pipe(startWith(this.divisionCtrl.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          debugger
          if (old != value) {
            this.onChangeDivisionSelected();
            this.isDisableSendButton = false;
          }
        }
      )

    this.schoolCtrl.valueChanges
      .pipe(startWith(this.divisionCtrl.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            this.onChangeSchoolSelected();
            this.isDisableSendButton = false;
          }
        }
      )

    // this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  //#endregion

  //#region PUBLIC METHOD

  onUserTypeSelectClicked() {
    if (this.isFirstLoadUserTypes) {
      this.endpointUserType.getUserTypeOptions('admin_teacher')
        .then(res => {
          this.isFirstLoadUserTypes = false;
          this.userTypes = res.filter(x => x.name !== 'Học sinh');
          if (!this.userTypes.find(x => x.id == null || x.id == 0)) {
            this.userTypes.unshift({ id: null, name: 'Chọn kiểu người dùng', levelManage: '' })
          }
          this.filteredUserTypes.next(this.userTypes.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.divisions = res;
          if (!this.divisions.find(x => x.id == null || x.id === 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionCtrl.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchools || this.schoolFilter.divisionId != divisionIdSelected) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchools = false;
          this.schools = res;
          if (!this.schools.find(x => x.id == null || x.id === 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.schoolFilter.divisionId = divisionIdSelected;
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(

        )
    } else {

    }
  }

  public getLinkExcelTemplate() {
    return `${this.baseApiUrl}/system/public/files/cdn/user-from-file-template/${Configuration.FILE_NAME_CREATE_USERS}`
  }

  public uploadFile(event: any) {
    /* wire up file reader */
    //const target: DataTransfer = <DataTransfer>(event.target);
    if (event.addedFiles.length !== 1) {
      throw new Error('Cannot use multiple files');
    }

    this.file = event.addedFiles[0];
    this.nameFileUpload = this.file?.name;

    this.isHasFile = true;
    this.isDisableSendButton = false;
  }

  onRemove(event) {
    this.file = undefined;
  }

  public onChooseFileButtonHandleClick() {
    this.isHasFile = null;
  }

  public onBackToManagerHandleClick() {
    this.router.navigateByUrl(`dashboard/manage-users`)
  }

  public onSubmitHandleClick() {
    this.isMaxColumn = false;
    this.ishasData = null;
    this.isDisableSendButton = true;

    this.totalUpCount = 0;
    this.errorCount = 0;

    if (!this.userTypeCtrl.value) {
      this.userTypeCtrl.markAsTouched();
      return;
    }
    if (this.levelManageValue <= 2) {
      this.schoolCtrl.markAsTouched();
    }
    if (this.isDivisionLevelUserType) {
      this.divisionCtrl.markAsTouched();
    }
    if (this.file == null) {
      this.isHasFile = false;
      return;
    } else {
      this.isHasFile = true;
    }
    if ((this.levelManageValue == 3 || !this.isSchoolLevelUserType || this.schoolCtrl.valid)
      && (!this.isDivisionLevelUserType || this.divisionCtrl.valid)) {
      this.requestForm.divisionId = this.divisionCtrl.value;
      this.requestForm.schoolId = this.schoolCtrl.value;

      if (this.userIdentity.levelManage == "DVS") {
        this.requestForm.divisionId = this.userIdentity.divisionId;
      } else if (this.userIdentity.levelManage == "SCH") {
        this.requestForm.divisionId = this.userIdentity.divisionId;
        this.requestForm.schoolId = this.userIdentity.schoolId;
      }
      if (this.file) {

        this.readExcel();
      }
    } else {
    }

  }
  //#endregion

  //#region PRIVATE METHOD

  private clearValues() {
    this.userTypeCtrl.markAsTouched();
    this.resetUserTypeSelected();
    this.resetDivisionSelected();
    this.resetSchoolSelectCtrl();
    this.onResetValueInput();
  }
  private readExcel() {
    this.spinner.show();
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(this.file);
    if (this.file && this.file.name.includes('xlsx')) {
      this.nameFileUpload = this.file?.name;
      //read excel file
      reader.onload = (e: any) => {
        /* create workbook */
        const binarystr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

        /* selected the first sheet */
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];

        /* save data */
        let data: any = XLSX.utils.sheet_to_json(ws); // to get 2d array pass 2nd parameter as object {header: 1}
        console.log(data); // Data will be logged in array format containing objects
        if (data.length == 0) {
          this.ishasData = false;
          this.isMaxColumn = null;
          this.isLoadTableData = false;
          this.isDisableSendButton = false;
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Tệp của bạn tải lên hiện đang trống !',
            duration: 2000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });

          this.spinner.hide();
          return;
        }

        if (data.length > this.maxColumn) {
          this.isMaxColumn = true;
          this.ishasData = true;
          this.isLoadTableData = false;
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: `Số lượng tải lên vượt quá ${this.maxColumn}`,
            duration: 2000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });

          this.spinner.hide();
          return;
        }

        this.changeNameKeyJson(data, this.oldKeys, this.newKeys);
        this.isWrongFormat = false;
        let importTeachers: TeacherFromFile[] = [];
        return new Promise(resolve => {
          [...data].forEach(element => {
            let importTeacher = new TeacherFromFile();
            importTeacher.no = element.no
            importTeacher.firstName = element.firstName
            importTeacher.lastName = element.lastName
            importTeacher.userName = element.userName
            importTeacher.email = element.email
            importTeacher.birthday = element.birthday
            importTeacher.phone = element.phone
            switch (element.gender?.toLowerCase()) {
              case this.maleStringVN:
                importTeacher.gender = 'MALE';
                break;
              case this.feMaleStringVN:
                importTeacher.gender = 'FEMALE';
                break;
              case undefined:
              case '':
                importTeacher.gender = null;
                break;
              default:
                importTeacher.gender = 'WOR';
                break;
            }
            importTeacher.address = element.address;
            if (importTeacher.userName != undefined || importTeacher.firstName != undefined)
              importTeachers.push(importTeacher);
            else {
              this.isWrongFormat = true;
              return;
            }
          });
          console.log(typeof importTeachers)
          resolve(importTeachers);
        })
          .then((res: TeacherFromFile[]) => {
            if (res && !this.isWrongFormat) {
              this.totalUpCount = data.length;
              let check = this.requestValidatorFrontEnd(res);
              if (check) {
                this.requestValidatorServer(res);
              }

            } else {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: ` Tệp sai định dạng `,
                duration: 3000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });
              this.hidenTable();
              this.spinner.hide();
              return;
            }
          })
      };
    } else {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: `Tệp không hợp lệ `,
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });

      this.hidenTable();
      this.spinner.hide();
    }
  }
  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id;
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id;
      });

    this.filteredGroupStudents
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGroupStudent.compareWith = (a: GroupStudentOption, b: GroupStudentOption) => a && b && a.id === b.id;
      });
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterUserTypes() {
    if (!this.userTypes) {
      return;
    }
    // get the search keyword
    let search = this.userTypeFilterCtrl.value;
    if (!search) {
      this.filteredUserTypes.next(this.userTypes.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredUserTypes.next(
      this.userTypes.filter(userType => userType.name.toLowerCase().indexOf(search) > -1)
    );
  }

  private changeNameKeyJson(objs: any, oldKeys: string[], newKeys: string[]) {
    this.isWrongFormat = false;
    objs.forEach(obj => {
      for (let index = 0; index < oldKeys.length; index++) {
        this.renameKey(obj, oldKeys[index], newKeys[index])
      }
    });
  }

  private renameKey(obj: any, oldKey: string, newKey: string) {
    obj[newKey] = obj[oldKey];
    delete obj[oldKey];
  }
  private onChangeUserTypeSelected() {
    this.ishasData = null;
    debugger;

    if (this.isDivisionLevelUserType == false || this.levelManageValue === 2) {
      this.divisionCtrl.markAsUntouched();
      this.divisionCtrl = new FormControl('');
      this.divisionRequired = false;
    }
    else {
      this.divisionRequired = true;
      this.divisionCtrl.markAsUntouched();
      this.divisionCtrl = new FormControl('', Validators.required);
    }

    this.resetDivisionSelected()
    this.resetSchoolSelectCtrl();
    this.onResetValueInput();
  }
  public onChangeDivisionSelected(id?: any) {

    this.resetSchoolSelectCtrl();
    this.onResetValueInput();
  }
  private onChangeSchoolSelected() {
    this.onResetValueInput();
  }

  private resetUserTypeSelected() {
    this.userTypes = [];
    this.isFirstLoadUserTypes = true;
    this.userTypeCtrl.setValue(0)
  }

  private resetDivisionSelected() {
    this.divisions = [];
    this.isFirstLoadDivisions = true;
    this.divisionCtrl.setValue(null);
  }
  private resetSchoolSelectCtrl() {
    this.schools = [];
    this.isFirstLoadSchools = true;
    this.schoolCtrl.setValue(null);
  }

  private onResetValueInput() {
    this.nameFileUpload = '';
    this.file = null;
  }

  private requestValidatorFrontEnd(data: TeacherFromFile[]) {
    debugger;
    let dataErrors: CreateTeacherErrorResponse[] = [];
    data.forEach((teacherFromFile, index) => {
      let errResults: ErrorResult[] = [];
      if (!teacherFromFile.firstName || isNullOrWhiteSpace(teacherFromFile.firstName.toString())) {
        errResults.push({ columnError: 'FirstName', columnIndex: index, reasonDetail: '', reasonType: 'NUL' })
      } else if (!isValidLengthString(teacherFromFile.firstName, 100)) {
        errResults.push({ columnError: 'FirstName', columnIndex: index, reasonDetail: '100', reasonType: 'MAL' })
      }

      if (teacherFromFile.lastName && !isNullOrWhiteSpace(teacherFromFile.lastName.toString()) && !isValidLengthString(teacherFromFile.lastName.toString(), 100)) {
        errResults.push({ columnError: 'LastName', columnIndex: index, reasonDetail: '', reasonType: 'NUL' });
      }

      if (!teacherFromFile.userName || isNullOrWhiteSpace(teacherFromFile.userName.toString())) {
        errResults.push({ columnError: 'UserName', columnIndex: index, reasonDetail: '', reasonType: 'NUL' });
      } else if (!isValidLengthString(teacherFromFile.userName, 50)) {
        errResults.push({ columnError: 'UserName', columnIndex: index, reasonDetail: '50', reasonType: 'MAL' });
      } else if (!isValidUserName(teacherFromFile.userName)) {
        errResults.push({ columnError: 'UserName', columnIndex: index, reasonDetail: '', reasonType: 'BAF' });
      }

      if (teacherFromFile.email && !isNullOrWhiteSpace(teacherFromFile.email.toString()) && !isEmail(teacherFromFile.email.toString())) {
        errResults.push({ columnError: 'Email', columnIndex: index, reasonDetail: '', reasonType: 'BAF' });
      }

      if (teacherFromFile.gender && !isNullOrWhiteSpace(teacherFromFile.gender.toString()) && !"MALE".includes(teacherFromFile.gender) && !"FEMALE".includes(teacherFromFile.gender)) {
        errResults.push({ columnError: 'Gender', columnIndex: index, reasonDetail: '', reasonType: 'BAF' });
      }

      if (teacherFromFile.phone && !isNullOrWhiteSpace(teacherFromFile.phone.toString())) {
        if (!isValidLengthString(teacherFromFile.firstName, 100))
          errResults.push({ columnError: 'Phone', columnIndex: index, reasonDetail: '20', reasonType: 'MAL' });

        if (!isPhoneNum(teacherFromFile.phone)) {
          errResults.push({ columnError: 'Phone', columnIndex: index, reasonDetail: '', reasonType: 'BAF' });
        }
      }

      if (teacherFromFile.address && !isNullOrWhiteSpace(teacherFromFile.address.toString()) && !isValidLengthString(teacherFromFile.address.toString(), 255))
        errResults.push({ columnError: 'Address', columnIndex: index, reasonDetail: '255', reasonType: 'MAL' });

      // check valid in file
      let userNameExists = [];
      let emailExists = [];

      let dataExists = [...data];
      dataExists.splice(index, 1);

      dataExists.map((element, index) => {
        if (element.userName && (element.userName == teacherFromFile.userName)) {
          userNameExists.push(index);
        }
        if (element.email && (element.email == teacherFromFile.email)) {
          emailExists.push(index)
        }
      });
      if (userNameExists.length > 0) {
        errResults.push({ columnError: 'UserName', columnIndex: index, reasonDetail: userNameExists.join(','), reasonType: 'IES' });
      }

      if (emailExists.length > 0) {
        errResults.push({ columnError: 'Email', columnIndex: index, reasonDetail: emailExists.join(','), reasonType: 'IES' });
      }
      if (errResults.length > 0) {
        dataErrors.push({ errorResult: errResults, rowData: teacherFromFile, errorCount: errResults.length })
      }
    });

    if (dataErrors.length > 0) {
      dataErrors.forEach(item => {
        item.errorResult.forEach(errorResult => {
          var index = this.newKeys
            .findIndex(x => x.toLocaleLowerCase() == errorResult.columnError.toLocaleLowerCase());
          if (index) {
            errorResult.columnError = this.oldKeys[index];
          }
        })
      })

      this.dataSource
        = new MatTableDataSource<CreateTeacherErrorResponse>(dataErrors);
      this.dataSource.paginator = this.paginator;
      this.errorCount = dataErrors.length;
      this.isLoadTableData = true;
      this.isLoadTableData = true;
      this.spinner.hide();

      this.scrollToElement();

      return false;
    } else {
      return true;
    }
  }

  private requestValidatorServer(data: TeacherFromFile[]) {
    let request: CreateTeachersFromFile = {
      userTypeId: this.userTypeCtrl.value,
      divisionId: this.divisionCtrl.value,
      schoolId: this.schoolCtrl.value,
      teachersData: [...data]
    }
    this.endpointUser.createTeacherImportFromFile(request).then(res => {

      this.errorCount = res.length;
      if (res.length > 0) {
        this.dataSourceError = res;
        this.dataSourceError.forEach(item => {
          item.errorResult.forEach(errorResult => {
            var index = this.newKeys
              .findIndex(x => x.toLocaleLowerCase() == errorResult.columnError.toLocaleLowerCase());
            if (index) {
              errorResult.columnError = this.oldKeys[index];
            }
          })
        })

        this.dataSource = new MatTableDataSource<CreateTeacherErrorResponse>(this.dataSourceError);
        this.dataSource.paginator = this.paginator;
        this.errorCount = res.length;
        this.isLoadTableData = true;
        this.spinner.hide();

        this.scrollToElement();

      } else {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: `Tạo người dùng thành công ${this.totalUpCount} người dùng`,
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
        this.spinner.hide();
        this.errorCount = 0;
        this.isDisableSendButton = true;
        this.hidenTable();
      }
    }).catch(err => {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: `Thêm thất bại`,
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });

      this.spinner.hide();
    });
  }

  private hidenTable() {
    this.isLoadTableData = false;
  }

  private scrollToElement(): void {
    setTimeout(() => {
      this.scrollBottom.nativeElement.scrollIntoView({ behavior: 'smooth' });
    }, TIMEOUT_SCROLL);
  }
  //#endregion
}

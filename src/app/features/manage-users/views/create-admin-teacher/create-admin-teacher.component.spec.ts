import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAdminTeacherComponent } from './create-admin-teacher.component';

describe('CreateAdminTeacherComponent', () => {
  let component: CreateAdminTeacherComponent;
  let fixture: ComponentFixture<CreateAdminTeacherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateAdminTeacherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAdminTeacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

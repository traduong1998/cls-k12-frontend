import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddStudentsFromFileComponent } from './add-students-from-file.component';

describe('AddStudentsFromFileComponent', () => {
  let component: AddStudentsFromFileComponent;
  let fixture: ComponentFixture<AddStudentsFromFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddStudentsFromFileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddStudentsFromFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ReplaySubject, Subject } from 'rxjs';
import { retry, takeUntil } from 'rxjs/operators';
import { CLS, DivisionEndpoint, DivisionOption, GradeEndpoint, GroupStudentEndpoint, SchoolEndpoint, UserEndpoint, UserIdentity, UserTypeEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { StudentFromFile } from 'sdk/cls-k12-sdk-js/src/services/user/models/student-from-file';
import { CreateStudentsFromFile } from 'sdk/cls-k12-sdk-js/src/services/user/requests/create-students-from-file';
import { CreateStudentErrorResponse, ErrorResult } from 'sdk/cls-k12-sdk-js/src/services/user/responses/create-student-error-response';
import { UserTypeOption } from 'sdk/cls-k12-sdk-js/src/services/usertype/models/UserType';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { Configuration } from 'src/app/shared/configurations';
import { isValidUserName, isValidLengthString } from 'src/app/shared/helpers/cls.helper';
import { isEmail, isNullOrWhiteSpace, isPhoneNum } from 'src/app/shared/helpers/validation.helper';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import * as XLSX from 'xlsx';

interface ErrorValidateResult {

}

@Component({
  selector: 'app-add-students-from-file',
  templateUrl: './add-students-from-file.component.html',
  styleUrls: ['./add-students-from-file.component.scss']
})
export class AddStudentsFromFileComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  @ViewChild('uploader', { static: false }) inputValue: ElementRef
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('scrollBottom') scrollBottom: ElementRef;
  baseApiUrl = 'http://localhost:65000';
  isFirstLoadUserTypes = true;
  isFirstLoadDivisions = true;
  isFirstLoadSchools = true;
  isFirstLoadGrades = true;
  isFirstLoadGroupStudents = true;
  private requestForm: CreateStudentsFromFile = {
    userTypeId: null,
    divisionId: null,
    schoolId: null,
    gradeId: null,
    groupStudentId: null,
    schoolYear: null,
    studentsData: [],
  }
  private maxColumn: number = 200
  private oldKeys: string[] = ['STT', 'Họ', 'Tên (*)', 'Mã định danh (*)', 'Email', 'Ngày sinh (ngày/tháng/năm)', 'Số điện thoại', 'Giới tính', 'Địa chỉ'];
  private newKeys: string[] = ['no', 'lastName', 'firstName', 'userName', 'email', 'birthday', 'phone', 'gender', 'address'];
  public displayedColumns = ['position', 'column', 'firstName', 'lastName', 'userName', 'email', 'infoError'];
  public isMaxColumn: boolean = false;
  public isLoadTableData = false;
  public nameFileUpload: string = '';
  public isHasFile = true;
  public isSuccess = false;
  public errorCount: number = 0;
  public totalUpCount: number = 0;
  public ishasData: boolean = null;
  public isWrongFormat = false;
  public isDisableSendButton = true;

  get userTypeId() { return this.addStudentFromFile.get('userTypeId'); }
  get divisionId() { return this.addStudentFromFile.get('divisionId'); }
  get schoolId() { return this.addStudentFromFile.get('schoolId'); }
  get gradeId() { return this.addStudentFromFile.get('gradeId'); }
  get groupStudentId() { return this.addStudentFromFile.get('groupStudentId'); }
  get schoolYearId() { return this.addStudentFromFile.get('schoolYearId'); }

  dataSourceError: CreateStudentErrorResponse[];
  dataSource: MatTableDataSource<CreateStudentErrorResponse>;
  public file: File;
  private maleStringVN = 'nam'
  private feMaleStringVN = 'nữ'
  userIdentity: UserIdentity;
  endpointUserType: UserTypeEndpoint;
  endpointUser: UserEndpoint;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointGroupStudent: GroupStudentEndpoint;
  addStudentFromFile: FormGroup;
  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  groupStudentFilter = {
    schoolId: null,
    gradeId: null
  }
  isStudentRole: boolean;
  levelManageValue: number = 0;
  /** list of divisions */
  protected userTypes: UserTypeOption[];
  /** control for the selected division */
  // public userTypeCtrl: FormControl = new FormControl('', Validators.required);
  /** control for the MatSelect filter keyword */
  public userTypeFilterCtrl: FormControl = new FormControl();
  /** list of divisions filtered by search keyword */
  public filteredUserTypes: ReplaySubject<UserTypeOption[]> = new ReplaySubject<UserTypeOption[]>(1);
  @ViewChild('singleSelectUserType', { static: true }) singleSelectUserType: MatSelect;


  protected divisions: DivisionOption[];
  // public divisionId: FormControl = new FormControl();
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;


  protected schools: SchoolOption[];
  // public schoolCtrl: FormControl = new FormControl('', Validators.required);
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;


  protected grades: GradeOption[];
  // public gradeId: FormControl = new FormControl('', Validators.required);
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected groupStudents: GroupStudentOption[];
  // public groupStudentId: FormControl = new FormControl('', Validators.required);
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();

  constructor(
    private _authService: AuthService,
    private _snackBar: MatSnackBar,
    private configService: AppConfigService,
    private router: Router,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {

    this.intitialEnpoint();
    this.baseApiUrl = CLS.getConfig().apiBaseUrl;
  }

  ngOnInit(): void {

    this.isStudentRole = true;

    this.addStudentFromFile = new FormGroup({
      schoolYearId: new FormControl(''),
      userTypeId: new FormControl(),
      divisionId: new FormControl(),
      schoolId: new FormControl('', Validators.required),
      gradeId: new FormControl('', Validators.required),
      groupStudentId: new FormControl('', Validators.required),
    });

    this.onUserTypeSelectClicked();

    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
      // Tài khoản cấp Trường, schoolid fillter mặc định theo id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
      this.groupStudentFilter.schoolId = this.userIdentity.schoolId;
    }

    // listen for search field value changes
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });

    // listen for search field value changes
    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    // listen for search field value changes
    this.groupStudentFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudents();
      });

    // listen for search field value changes
    this.userTypeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterUserTypes();
      });

    this.addStudentFromFile.get('schoolYearId')
      .setValue(this.configService.getConfig().unit.currentSchoolYear.toString());

    this.getStudentUserTypeId()

  }

  ngAfterViewInit() {
    if (this.dataSourceError) {
      this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
      this.paginator.page.subscribe(() => {
        this.paginator.pageIndex
        this.paginator.pageSize
        this.dataSource = new MatTableDataSource<CreateStudentErrorResponse>(this.dataSourceError);
        this.dataSource.paginator = this.paginator;
      });
    }
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterUserTypes() {
    if (!this.userTypes) {
      return;
    }
    // get the search keyword
    let search = this.userTypeFilterCtrl.value;
    if (!search) {
      this.filteredUserTypes.next(this.userTypes.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredUserTypes.next(
      this.userTypes.filter(userType => userType.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onUserTypeSelectClicked() {
    if (this.isFirstLoadUserTypes) {
      this.endpointUserType.getUserTypeOptions("student")
        .then(res => {
          this.isFirstLoadUserTypes = false;
          this.userTypes = res;
          this.filteredUserTypes.next(this.userTypes.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.divisions = res;
          if (this.divisions.find(x => x.id !== null)) {
            this.divisions.splice(0, -1, { id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionId.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchools || this.schoolFilter.divisionId != divisionIdSelected) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchools = false;
          this.schools = res;
          if (this.schools.find(x => x.id !== null)) {
            this.schools.splice(0, -1, { id: null, name: 'Chọn trường' })
          }
          this.schoolFilter.divisionId = divisionIdSelected;
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolId.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrades || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          if (this.grades.find(x => x.id !== null)) {
            this.grades.splice(0, -1, { id: null, name: 'Chọn khối' })
          }
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onGroupStudentSelectClicked() {
    var schoolIdSelected = this.schoolId.value;
    var gradeIdSelected = this.gradeId.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGroupStudents || this.groupStudentFilter.schoolId != schoolIdSelected
      || this.groupStudentFilter.gradeId != gradeIdSelected) {
      this.endpointGroupStudent.getGroupStudentOptions(schoolIdSelected, gradeIdSelected)
        .then(res => {
          this.isFirstLoadGroupStudents = false;
          this.groupStudents = res;
          if (this.groupStudents.find(x => x.id !== null)) {
            this.groupStudents.splice(0, -1, { id: null, name: 'Chọn lớp' })
          }
          this.groupStudentFilter.gradeId = gradeIdSelected;
          this.groupStudentFilter.schoolId = schoolIdSelected;
          this.filteredGroupStudents.next(this.groupStudents.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onChangeDivisionSelected(id: any) {
    this.resetSchoolSelectCtrl();
    this.resetGradeSelectCtrl();
    this.resetGroupStudentSelectCtrl();
    this.isDisableSendButton = false;
  }

  onChangeSchoolSelected(id: any) {
    console.log('this.schoolId.value', this.schoolId.value)
    this.resetGradeSelectCtrl();
    this.resetGroupStudentSelectCtrl();
    this.isDisableSendButton = false;
  }

  onChangeGradeSelected(id: any) {
    console.log('this.gradeId.value', this.gradeId.value)
    this.resetGroupStudentSelectCtrl();
    this.isDisableSendButton = false;
  }

  resetSchoolSelectCtrl() {
    this.schools = [];
    this.isFirstLoadSchools = true;
    this.schoolId.setValue(null);
    this.isDisableSendButton = false;
  }

  resetGradeSelectCtrl() {
    this.grades = [];
    this.isFirstLoadGrades = true;
    this.gradeId.setValue(null);
    this.isDisableSendButton = false;
  }

  resetGroupStudentSelectCtrl() {
    this.groupStudents = [];
    this.isFirstLoadGroupStudents = true;
    this.groupStudentId.setValue(null);
    this.isDisableSendButton = false;
  }

  public onBackToManagerHandleClick() {
    this.router.navigateByUrl(`dashboard/manage-users`)
  }

  public onSubmitHandleClick() {
    this.isMaxColumn = false;
    this.ishasData = null;
    this.isSuccess = false;

    this.totalUpCount = 0;
    this.errorCount = 0;

    this.addStudentFromFile.get('userTypeId').markAsTouched();
    if (this.levelManageValue <= 2) {
      this.schoolId.markAsTouched();
    }

    this.gradeId.markAsTouched();
    this.groupStudentId.markAsTouched();

    if (!this.file) {
      this.isHasFile = false;
      return;
    }

    this.isHasFile = true;
    if (this.gradeId.valid && this.groupStudentId.valid && this.addStudentFromFile.get('userTypeId').valid && (this.levelManageValue == 3 || this.schoolId.valid)) {
      this.requestForm.userTypeId = this.addStudentFromFile.get('userTypeId').value;
      this.requestForm.divisionId = this.divisionId.value;
      this.requestForm.schoolId = this.schoolId.value;
      if (this.userIdentity.levelManage == "DVS") {
        this.requestForm.divisionId = this.userIdentity.divisionId;
      } else if (this.userIdentity.levelManage == "SCH") {
        this.requestForm.divisionId = this.userIdentity.divisionId;
        this.requestForm.schoolId = this.userIdentity.schoolId;
      }
      this.requestForm.gradeId = this.gradeId.value;
      this.requestForm.groupStudentId = this.groupStudentId.value;

      if (this.file) {

        this.readExcel();
      }
    } else {

    }
  }

  public uploadFile(event: any) {
    /* wire up file reader */
    //const target: DataTransfer = <DataTransfer>(event.target);
    if (event.addedFiles.length !== 1) {
      throw new Error('Cannot use multiple files');
    }

    this.file = event.addedFiles[0];
    this.nameFileUpload = this.file?.name;

    this.isHasFile = true;
    this.isDisableSendButton = false;
  }

  onRemove(event) {
    this.file = undefined;
  }

  public onChooseFileButtonHandleClick() {
    this.isHasFile = null;
    this.changeFileInput();
  }

  public getLinkExcelTemplate() {
    return `${this.baseApiUrl}/system/public/files/cdn/user-from-file-template/${Configuration.FILE_NAME_CREATE_USERS}`
  }

  private resetValues() {
    this.addStudentFromFile.reset();
    this.file = null;
    this.nameFileUpload = ''
    this.getStudentUserTypeId();
  }
  private intitialEnpoint() {
    this.userIdentity = this._authService.getTokenInfo();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUserType = new UserTypeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUser = new UserEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
  }
  private getStudentUserTypeId() {
    this.endpointUserType.getUserTypeOptions("student")
      .then(res => {
        if (res) {
          this.addStudentFromFile.get('userTypeId')
            .setValue(res[0].id);
        } else {
          return '';
        }
      })
  }

  private changeFileInput() {
    this.inputValue.nativeElement.click()
  }
  private changeNameKeyJson(objs: any, oldKeys: string[], newKeys: string[]) {
    this.isWrongFormat = false;
    objs.forEach(obj => {
      for (let index = 0; index < oldKeys.length; index++) {
        this.renameKey(obj, oldKeys[index], newKeys[index])
      }
    });

  }

  private renameKey(obj: any, oldKey: string, newKey: string) {
    console.log(obj[oldKey]);
    obj[newKey] = obj[oldKey];
    delete obj[oldKey];
  }

  private readExcel() {
    this.spinner.show();
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(this.file);
    if (this.file && this.file.name.includes('xlsx')) {
      this.nameFileUpload = this.file?.name;
      //read excel file
      reader.onload = (e: any) => {
        /* create workbook */
        const binarystr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

        /* selected the first sheet */
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        debugger;
        /* save data */
        let data: any = XLSX.utils.sheet_to_json(ws);
        console.log(data);

        if (data.length == 0) {
          this.ishasData = false;
          this.isMaxColumn = null;
          this.hidenTable();
          this.isSuccess = false;

          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: `Tệp của bạn tải lên hiện đang trống `,
            duration: 3000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });
          this.spinner.hide();
          return;
        }

        if (data.length > this.maxColumn) {
          this.isMaxColumn = true;
          this.ishasData = true;
          this.hidenTable();
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: `Số lượng tải lên vượt quá ${this.maxColumn} `,
            duration: 3000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });
          this.spinner.hide();
          return;
        }

        this.changeNameKeyJson(data, this.oldKeys, this.newKeys);
        this.isWrongFormat = false;
        let importStudents: StudentFromFile[] = [];
        return new Promise(resolve => {
          [...data].forEach(element => {
            let importStudent = new StudentFromFile();
            importStudent.no = element.no
            importStudent.firstName = element.firstName
            importStudent.lastName = element.lastName
            importStudent.userName = element.userName
            importStudent.email = element.email
            importStudent.birthday = element.birthday
            importStudent.phone = element.phone
            switch (element.gender?.toLowerCase()) {
              case this.maleStringVN:
                importStudent.gender = 'MALE';
                break;
              case this.feMaleStringVN:
                importStudent.gender = 'FEMALE';
                break;
              case undefined:
              case '':
                importStudent.gender = null;
                break;
              default:
                importStudent.gender = 'WOR';
                break;
            }

            importStudent.address = element.address
            if (importStudent.userName != undefined || importStudent.firstName != undefined) {
              importStudents.push(importStudent);
            } else {
              this.isWrongFormat = true;
            }
          });
          resolve(importStudents);
        })
          .then((res: StudentFromFile[]) => {
            this.totalUpCount = data.length
            if (res && !this.isWrongFormat) {
              let check = this.requestValidatorFrontEnd(res);
              if (check) {
                this.requestValidatorServer(res);
              }
            } else {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: `Tệp sai định dạng`,
                duration: 2000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });

              this.isDisableSendButton = true;
              this.hidenTable();
              this.spinner.hide();
              return;
            }
          })
          .catch((err) => {
            console.error(err);
            this.spinner.hide();
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: `Có lỗi xảy ra vui lòng thử lại `,
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          })
      };
    } else {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: `Tệp không hợp lệ `,
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });

      this.isDisableSendButton = true;
      this.hidenTable();
      this.spinner.hide();
    }
  }
  private requestValidatorFrontEnd(data: StudentFromFile[]) {
    let dataErrors: CreateStudentErrorResponse[] = [];
    data.forEach((studentFromFile, index) => {
      let errResults: ErrorResult[] = [];
      if (!studentFromFile.firstName || isNullOrWhiteSpace(studentFromFile.firstName.toString())) {
        errResults.push({ columnError: 'FirstName', columnIndex: index, reasonDetail: '', reasonType: 'NUL' })
      } else if (!isValidLengthString(studentFromFile.firstName, 100)) {
        errResults.push({ columnError: 'FirstName', columnIndex: index, reasonDetail: '100', reasonType: 'MAL' })
      }

      if (studentFromFile.lastName && !isNullOrWhiteSpace(studentFromFile.lastName.toString()) && !isValidLengthString(studentFromFile.lastName, 100)) {
        errResults.push({ columnError: 'LastName', columnIndex: index, reasonDetail: '', reasonType: 'NUL' });
      }

      if (!studentFromFile.userName || isNullOrWhiteSpace(studentFromFile.userName.toString())) {
        errResults.push({ columnError: 'UserName', columnIndex: index, reasonDetail: '', reasonType: 'NUL' });
      } else if (!isValidLengthString(studentFromFile.userName, 50)) {
        errResults.push({ columnError: 'UserName', columnIndex: index, reasonDetail: '50', reasonType: 'MAL' });
      } else if (!isValidUserName(studentFromFile.userName)) {
        errResults.push({ columnError: 'UserName', columnIndex: index, reasonDetail: '', reasonType: 'BAF' });
      }

      if (studentFromFile.email != undefined && !isNullOrWhiteSpace(studentFromFile.email.toString()) && !isEmail(studentFromFile.email.toString())) {
        errResults.push({ columnError: 'Email', columnIndex: index, reasonDetail: '', reasonType: 'BAF' });
      }

      if (studentFromFile.gender != undefined && !isNullOrWhiteSpace(studentFromFile.gender.toString()) && !"MALE".includes(studentFromFile.gender.toString()) && !"FEMALE".includes(studentFromFile.gender)) {
        errResults.push({ columnError: 'Gender', columnIndex: index, reasonDetail: '', reasonType: 'BAF' });
      }

      if (studentFromFile.phone != undefined && !isNullOrWhiteSpace(studentFromFile.phone.toString())) {
        if (!isValidLengthString(studentFromFile.firstName, 100))
          errResults.push({ columnError: 'Phone', columnIndex: index, reasonDetail: '20', reasonType: 'MAL' });

        if (!isPhoneNum(studentFromFile.phone.toString())) {
          errResults.push({ columnError: 'Phone', columnIndex: index, reasonDetail: '', reasonType: 'BAF' });
        }
      }

      if (studentFromFile.address != undefined && !isNullOrWhiteSpace(studentFromFile.address.toString()) && !isValidLengthString(studentFromFile.address.toString(), 255))
        errResults.push({ columnError: 'Address', columnIndex: index, reasonDetail: '255', reasonType: 'MAL' });

      // check valid in file
      let userNameExists = [];
      let emailExists = [];

      let dataExists = [...data];
      dataExists.splice(index, 1);

      dataExists.map((element, index) => {
        if (element.userName && element.userName == studentFromFile.userName) {
          userNameExists.push(index);
        }
        if (element.email && element.email == studentFromFile.email) {
          emailExists.push(index)
        }
      });
      if (userNameExists.length > 0) {
        errResults.push({ columnError: 'UserName', columnIndex: index, reasonDetail: userNameExists.join(','), reasonType: 'IES' });
      }

      if (emailExists.length > 0) {
        errResults.push({ columnError: 'Email', columnIndex: index, reasonDetail: emailExists.join(','), reasonType: 'IES' });
      }
      if (errResults.length > 0) {
        dataErrors.push({ errorResult: errResults, rowData: studentFromFile, errorCount: errResults.length })
      }
    });

    if (dataErrors.length > 0) {
      dataErrors.forEach(item => {
        item.errorResult.forEach(errorResult => {
          var index = this.newKeys
            .findIndex(x => x.toLocaleLowerCase() == errorResult.columnError.toLocaleLowerCase());
          if (index) {
            errorResult.columnError = this.oldKeys[index];
          }
        })
      })

      this.dataSource
        = new MatTableDataSource<CreateStudentErrorResponse>(dataErrors);
      this.dataSource.paginator = this.paginator;
      this.errorCount = dataErrors.length;
      this.isLoadTableData = true;

      this.totalUpCount = data.length;
      this.isLoadTableData = true;
      this.spinner.hide();

      this.scrollToElement();
      return false;
    } else {
      return true;
    }
  }
  private requestValidatorServer(data: StudentFromFile[]) {

    let request: CreateStudentsFromFile = {
      userTypeId: this.addStudentFromFile.get('userTypeId').value,
      divisionId: this.divisionId.value,
      schoolId: this.schoolId.value,
      gradeId: this.gradeId.value,
      groupStudentId: this.groupStudentId.value,
      schoolYear: this.schoolYearId.value,
      studentsData: [...data]
    }

    this.endpointUser.createStudentImportFromFile(request).then(res => {
      this.errorCount = res.length;
      if (this.errorCount > 0) {
        this.dataSourceError = res;
        this.dataSourceError.forEach(item => {
          item.errorResult.forEach(errorResult => {
            var index = this.newKeys
              .findIndex(x => x.toLocaleLowerCase() == errorResult.columnError.toLocaleLowerCase());
            if (index) {
              errorResult.columnError = this.oldKeys[index];
            }
          })
        })

        this.dataSource
          = new MatTableDataSource<CreateStudentErrorResponse>(this.dataSourceError);
        this.dataSource.paginator = this.paginator;
        this.errorCount = res.length;
        this.isLoadTableData = true;

        this.totalUpCount = data.length;
        this.isLoadTableData = true;
        this.spinner.hide();
        this.scrollToElement();
      } else {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: `Tạo người dùng thành công ${this.totalUpCount} người dùng`,
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
        this.isSuccess = true;
        this.isDisableSendButton = true;

        this.hidenTable();
        this.totalUpCount = data.length;
        this.errorCount = 0;
        this.spinner.hide();
      }
    })
      .catch(() => {
        this.spinner.hide();
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: `Có lỗi xảy ra vui lòng thử lại `,
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
      })
  }

  private hidenTable() {
    this.isLoadTableData = false;
  }

  private scrollToElement(): void {
    setTimeout(() => {
      this.scrollBottom.nativeElement.scrollIntoView({ behavior: 'smooth' });
    }, 300);
  }
}

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CreateUserRequest } from 'sdk/cls-k12-sdk-js/src/services/user/requests/createUserRequest';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { take, takeUntil } from 'rxjs/operators';
import { ReplaySubject, Subject } from 'rxjs';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { DivisionOption } from 'sdk/cls-k12-sdk-js/src/services/division/models/Division';
import { UserTypeOption } from 'sdk/cls-k12-sdk-js/src/services/usertype/models/UserType';
import { DivisionEndpoint, GradeEndpoint, GroupStudentEndpoint, SchoolEndpoint, UserEndpoint, UserIdentity, UserTypeEndpoint } from 'cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/core/services';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { FileServerEndpoint } from 'cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  form: FormData = new FormData();
  formCreateUser: FormGroup;
  imageUrl: any = null;
  @ViewChild("inputImageFile") inputImageFile: ElementRef;
  @ViewChild('dpDayOfBirth') dpDayOfBirth;
  emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
  usernamePattern = "^[a-zA-Z0-9_.-]{1,50}$";

  // usernamePattern = '^([A-Za-z0-9]+(?:\.[A-Za-z0-9]+)*)$'
  passwordPattern = "^.{6,50}$";
  passwordInputType: string = 'password';

  endpointUserType: UserTypeEndpoint;
  endpointUser: UserEndpoint;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointGroupStudent: GroupStudentEndpoint;
  fileServerEndpoint: FileServerEndpoint;
  userRole: string = "";
  isStudentRole: boolean;
  userIdentity: UserIdentity;
  levelManageValue: number = 0;
  avatarFile: File;

  constructor(private _snackBar: MatSnackBar, private route: ActivatedRoute, private _authService: AuthService, private router: Router) {
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUserType = new UserTypeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUser = new UserEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
    this.fileServerEndpoint = new FileServerEndpoint();

    this.userIdentity = _authService.getTokenInfo();

    this.formCreateUser = new FormGroup({
      firstName: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      lastName: new FormControl('', [Validators.maxLength(100)]),
      username: new FormControl('', [Validators.required, Validators.maxLength(50), Validators.pattern(this.usernamePattern)]),
      email: new FormControl('', [Validators.maxLength(100), Validators.pattern(this.emailPattern)]),
      password: new FormControl('', [Validators.required, Validators.pattern(this.passwordPattern)]),
      gender: new FormControl(null),
      isActive: new FormControl('1', Validators.required),
      dayOfBirth: new FormControl(''),
      dpDayOfBirthInput: new FormControl(''),
      avatar: new FormControl(''),
      academicTitle: new FormControl('', Validators.maxLength(50)),
      degree: new FormControl('', Validators.maxLength(50)),
      officeAddress: new FormControl('', Validators.maxLength(50)),
      address: new FormControl('', Validators.maxLength(255)),
      phone: new FormControl('', Validators.maxLength(20)),
      biography: new FormControl(''),
    });
    console.log("Tài khoản cấp: ", this.userIdentity.levelManage);
    console.log("Vai trò: ", this.userIdentity.userTypeRole);

    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
      // Tài khoản cấp Trường, schoolid fillter mặc định theo id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
      this.groupStudentFilter.schoolId = this.userIdentity.schoolId;
    }
    console.log("levelManageValue: ", this.levelManageValue);
  }

  isFirstLoadUserTypes = true;
  isFirstLoadDivisions = true;
  isFirstLoadSchools = true;
  isFirstLoadGrades = true;
  isFirstLoadGroupStudents = true;

  userCreate: CreateUserRequest = {
    username: '',
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    userTypeId: null,
    gender: null,
    status: '',
    divisionId: null,
    schoolId: null,
    gradeId: null,
    groupStudentId: null,

    dayOfBirth: new Date('04/25/2021'),
    avatar: undefined,
    academicTitle: null,
    degree: null,
    officeAddress: null,
    address: null,
    phone: null,
    biography: null
  }

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  groupStudentFilter = {
    schoolId: null,
    gradeId: null
  }

  /** list of divisions */
  protected userTypes: UserTypeOption[];
  /** control for the selected division */
  public userTypeCtrl: FormControl = new FormControl('', Validators.required);
  /** control for the MatSelect filter keyword */
  public userTypeFilterCtrl: FormControl = new FormControl();
  /** list of divisions filtered by search keyword */
  public filteredUserTypes: ReplaySubject<UserTypeOption[]> = new ReplaySubject < UserTypeOption[] > (1);

  @ViewChild('singleSelectUserType', { static: true }) singleSelectUserType: MatSelect;


  protected divisions: DivisionOption[];
  public divisionCtrl: FormControl = new FormControl();
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject < DivisionOption[] > (1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;


  protected schools: SchoolOption[];
  public schoolCtrl: FormControl = new FormControl('', Validators.required);
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject < SchoolOption[] > (1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;


  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl('', Validators.required);
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject < GradeOption[] > (1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected groupStudents: GroupStudentOption[];
  public groupStudentCtrl: FormControl = new FormControl('', Validators.required);
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject < GroupStudentOption[] > (1);
  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject < void> ();


  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        this.userRole = params.type == undefined ? "student" : params.type;
        this.isStudentRole = this.userRole == 'student';
      });

    // listen for search field value changes
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });

    // listen for search field value changes
    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    // listen for search field value changes
    this.groupStudentFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudents();
      });

    // listen for search field value changes
    this.userTypeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterUserTypes();
      });


    // Lấy danh sách kiểu người dùng ra trước
    this.endpointUserType.getUserTypeOptions("student")
      .then(res => {
        this.isFirstLoadUserTypes = false;
        this.userTypes = res;
        this.filteredUserTypes.next(this.userTypes.slice());

        if (this.userTypes.length > 0) {
          this.userTypeCtrl.setValue(this.userTypes[0].id);
        }
      })
      .catch(

      )
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  onChangeImageInput(event): void {
    var reader = new FileReader();
    let fileData: File = null;
    if (event.target.files && event.target.files[0]) {
      let file = event.target.files[0];

      this.avatarFile = file;
      // this.userCreate.avatar = event.target.files[0];
      // Show preview 
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        this.imageUrl = reader.result;
      }
    }
  }
  uploadImage(): void {
    this.inputImageFile.nativeElement.click();
  }

  submitForm() {
    this.formCreateUser.markAllAsTouched();
    this.userTypeCtrl.markAsTouched();
    if (this.levelManageValue <= 2) {
      this.schoolCtrl.markAsTouched();
    }

    this.gradeCtrl.markAsTouched();
    this.groupStudentCtrl.markAsTouched();

    if (this.formCreateUser.valid && this.gradeCtrl.valid && this.groupStudentCtrl.valid && (this.levelManageValue == 3 || this.schoolCtrl.valid)) {
      this.userCreate.username = this.formCreateUser.value.username?.trim();
      this.userCreate.firstName = this.formCreateUser.value.firstName?.trim();
      this.userCreate.lastName = this.formCreateUser.value.lastName?.trim();
      this.userCreate.email = this.formCreateUser.value.email?.trim();
      if (this.userCreate.email != null) {
        this.userCreate.email = this.userCreate.email.toLowerCase();
      }
      this.userCreate.password = this.formCreateUser.value.password;
      this.userCreate.status = this.formCreateUser.value.isActive == "1" ? "ATV" : "IAT";
      this.userCreate.userTypeId = this.userTypeCtrl.value;
      this.userCreate.divisionId = this.divisionCtrl.value;
      this.userCreate.schoolId = this.schoolCtrl.value;
      if (this.userIdentity.levelManage == "DVS") {
        this.userCreate.divisionId = this.userIdentity.divisionId;
      } else if (this.userIdentity.levelManage == "SCH") {
        this.userCreate.divisionId = this.userIdentity.divisionId;
        this.userCreate.schoolId = this.userIdentity.schoolId;
      }
      this.userCreate.gradeId = this.gradeCtrl.value;
      this.userCreate.groupStudentId = this.groupStudentCtrl.value;
      this.userCreate.dayOfBirth = this.formCreateUser.value.dpDayOfBirthInput == "" ? null : this.formCreateUser.value.dpDayOfBirthInput;
      // this.userCreate.avatar = this.formCreateUser.value.avatar;
      console.log('this.formCreateUser.value.avatar', this.formCreateUser.value.avatar);
      this.userCreate.academicTitle = this.formCreateUser.value.academicTitle;
      this.userCreate.degree = this.formCreateUser.value.degree;
      this.userCreate.officeAddress = this.formCreateUser.value.officeAddress;
      this.userCreate.address = this.formCreateUser.value.address;
      this.userCreate.phone = this.formCreateUser.value.phone;
      
      this.userCreate.gender = this.formCreateUser.value.gender == "1" ? true : this.formCreateUser.value.gender == null ? null : false;

      Promise.resolve()
        .then(() => {
          return this.avatarFile ? this.fileServerEndpoint.uploadFile({
            file: this.avatarFile,
            isPrivate: false
          }) : null;
        })
        .then((res) => {
          if (res) {
            this.userCreate.avatar = res.link;
          }
          return this.endpointUser.createUser(this.userCreate);
        })
        .then(res => {
          if (res) {
            this.resetForm();
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Tạo người dùng thành công!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Tạo người dùng không thành công. Vui lòng thử lại!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });

          }
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
      return;
    } else {
      //alert("Kiê");
    }

  }

  btnCancelClicked() {
    this.router.navigate(['dashboard/manage-users']);
    //this.resetForm();
  }
  resetForm() {
    this.formCreateUser.reset();
    //this.divisionCtrl.reset();
    //this.userTypeCtrl.reset();
    //this.schoolCtrl.reset();
    //this.gradeCtrl.reset();
    //this.groupStudentCtrl.reset();
    this.formCreateUser.get('isActive').setValue('1');
    this.formCreateUser.get('dpDayOfBirthInput').setValue('');
    this.avatarFile = null;
    this.imageUrl = null;
  }
  get f() {
    return this.formCreateUser;
  }


  /**
  * Sets the initial value after the filteredDivisions are loaded initially
  */
  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        // triggers initializing the selection according to the initial value of
        // the form control (i.e. _initializeSelection())
        // this needs to be done after the filteredDivisions are loaded initially
        // and after the mat-option elements are available
        this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id;
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id;
      });

    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
      });

    this.filteredGroupStudents
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGroupStudent.compareWith = (a: GroupStudentOption, b: GroupStudentOption) => a && b && a.id === b.id;
      });

    this.filteredUserTypes
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectUserType.compareWith = (a: UserTypeOption, b: UserTypeOption) => a && b && a.id === b.id;
      });
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterUserTypes() {
    if (!this.userTypes) {
      return;
    }
    // get the search keyword
    let search = this.userTypeFilterCtrl.value;
    if (!search) {
      this.filteredUserTypes.next(this.userTypes.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredUserTypes.next(
      this.userTypes.filter(userType => userType.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onUserTypeSelectClicked() {
    if (this.isFirstLoadUserTypes) {
      this.endpointUserType.getUserTypeOptions("student")
        .then(res => {
          this.isFirstLoadUserTypes = false;
          this.userTypes = res;
          this.filteredUserTypes.next(this.userTypes.slice());

          if (this.userTypes.length > 0) {
            this.userTypeCtrl.setValue(this.userTypes[0].id);
          }
        })
        .catch(

        )
    } else {

    }
  }

  onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionCtrl.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchools || this.schoolFilter.divisionId != divisionIdSelected) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchools = false;
          this.schools = res;
          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.schoolFilter.divisionId = divisionIdSelected;
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolCtrl.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrades || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          if (this.grades && !this.grades.find(x => x.id == 0)) {
            this.grades.unshift({ id: null, name: 'Chọn khối' })
          }
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onGroupStudentSelectClicked() {
    var schoolIdSelected = this.schoolCtrl.value;
    var gradeIdSelected = this.gradeCtrl.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGroupStudents || this.groupStudentFilter.schoolId != schoolIdSelected
      || this.groupStudentFilter.gradeId != gradeIdSelected) {
      this.endpointGroupStudent.getGroupStudentOptions(schoolIdSelected, gradeIdSelected)
        .then(res => {
          this.isFirstLoadGroupStudents = false;
          this.groupStudents = res;
          if (this.groupStudents && !this.groupStudents.find(x => x.id == 0)) {
            this.groupStudents.unshift({ id: null, name: 'Chọn lớp' })
          }
          this.groupStudentFilter.gradeId = gradeIdSelected;
          this.groupStudentFilter.schoolId = schoolIdSelected;
          this.filteredGroupStudents.next(this.groupStudents.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onChangeDivisionSelected(id: any) {
    this.resetSchoolSelectCtrl();
    this.resetGradeSelectCtrl();
    this.resetGroupStudentSelectCtrl();
  }

  onChangeSchoolSelected(id: any) {
    this.resetGradeSelectCtrl();
    this.resetGroupStudentSelectCtrl();
  }

  onChangeGradeSelected(id: any) {
    this.resetGroupStudentSelectCtrl();
  }

  resetSchoolSelectCtrl() {
    this.schools = [];
    this.isFirstLoadSchools = true;
    this.schoolCtrl.setValue(undefined);
  }

  resetGradeSelectCtrl() {
    this.grades = [];
    this.isFirstLoadGrades = true;
    this.gradeCtrl.setValue(undefined);
  }

  resetGroupStudentSelectCtrl() {
    this.groupStudents = [];
    this.isFirstLoadGroupStudents = true;
    this.groupStudentCtrl.setValue(undefined);
  }
  openDpDayOfBirth() {
    this.dpDayOfBirth.open();
  }
}

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { EditUserRequest, UserEndpoint, UserIdentity } from 'cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/core/services';
import { GetUserInfoResponse } from 'sdk/cls-k12-sdk-js/src/services/user/responses/get-user-info-response';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { FileServerEndpoint } from 'cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  form: FormData = new FormData();
  formEditUser: FormGroup;
  imageUrl: any = null;
  @ViewChild("inputImageFile") inputImageFile: ElementRef;
  @ViewChild('dpDayOfBirth') dpDayOfBirth;
  emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
  usernamePattern = "^[a-zA-Z0-9_.-]{1,50}$";
  passwordPattern = "^.{6,50}$";
  passwordInputType: string = 'password';

  endpointUser: UserEndpoint;
  fileServerEndpoint: FileServerEndpoint;
  userRole: string = "";
  isStudentRole: boolean;
  userIdentity: UserIdentity;
  levelManageValue: number = 0;
  isStudentUser: boolean = false;

  public userTypeNameInput: FormControl = new FormControl();
  public divisionNameInput: FormControl = new FormControl();
  public schoolNameInput: FormControl = new FormControl();
  public gradeNameInput: FormControl = new FormControl();
  public groupStudentNameInput: FormControl = new FormControl();
  avatarFile: File;

  constructor(private _snackBar: MatSnackBar, private route: ActivatedRoute, private _authService: AuthService, private router: Router) {
    this.endpointUser = new UserEndpoint({ baseUrl: this.baseApiUrl });
    this.fileServerEndpoint = new FileServerEndpoint();

    this.userIdentity = _authService.getTokenInfo();

    this.formEditUser = new FormGroup({
      firstName: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      lastName: new FormControl('', [Validators.maxLength(100)]),
      username: new FormControl('', [Validators.required, Validators.maxLength(50), Validators.pattern(this.usernamePattern)]),
      email: new FormControl('', [Validators.maxLength(100), Validators.pattern(this.emailPattern)]),
      password: new FormControl('', [Validators.pattern(this.passwordPattern)]),
      gender: new FormControl(null),
      isActive: new FormControl('1', Validators.required),
      dayOfBirth: new FormControl(''),
      dpDayOfBirthInput: new FormControl(''),
      avatar: new FormControl(''),
      academicTitle: new FormControl('', Validators.maxLength(50)),
      degree: new FormControl('', Validators.maxLength(50)),
      officeAddress: new FormControl('', Validators.maxLength(50)),
      address: new FormControl('', Validators.maxLength(255)),
      phone: new FormControl('', Validators.maxLength(20)),
      biography: new FormControl(''),
    });
    console.log("Tài khoản cấp: ", this.userIdentity.levelManage);
    console.log("Vai trò: ", this.userIdentity.userTypeRole);

    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;

    }
  }

  isFirstLoadUserTypes = true;
  isFirstLoadDivisions = true;
  isFirstLoadSchools = true;
  isFirstLoadGrades = true;
  isFirstLoadGroupStudents = true;
  isDisabledOrganizationInput = true;

  userEdit: EditUserRequest = {
    userId: 0,
    username: '',
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    gender: null,
    status: '',

    dayOfBirth: new Date('04/25/2021'),
    avatar: undefined,
    academicTitle: null,
    degree: null,
    officeAddress: null,
    address: null,
    phone: null,
    biography: null
  }
  isSchoolLevelUserType: boolean = true;
  isDivisionLevelUserType: boolean = true;
  isDepartmentLevelUserType: boolean = true;
  userLevelManageValue: number = 1;

  userInfoBackUp: GetUserInfoResponse;
  ngOnInit(): void {
    this.userEdit.userId = this.route.snapshot.params.id;

    this.endpointUser.getUserById(this.userEdit.userId)
      .then(res => {
        if (res) {
          console.log(res);
          this.userInfoBackUp = res;
          this.setData(res);

          if (res.userTypeRole == "STUDENT") {
            this.isStudentUser = true;
          }

          this.isSchoolLevelUserType = res.userTypeLevelManage == "SCH";
          this.isDivisionLevelUserType = res.userTypeLevelManage == "DVS";
          this.isDepartmentLevelUserType = res.userTypeLevelManage == "DPM";

          if (this.isDepartmentLevelUserType) {
            this.userLevelManageValue = 1;
          } else if (this.isDivisionLevelUserType) {
            this.userLevelManageValue = 2;
          } else if (this.isSchoolLevelUserType) {
            this.userLevelManageValue = 3;
          }
        }
      })
      .catch((err: ErrorResponse) => {
        console.log(err);
      });
  }

  ngAfterViewInit() {

  }

  ngOnDestroy() {

  }

  setData(res: GetUserInfoResponse) {
    this.userTypeNameInput.setValue(res.userTypeName);
    this.divisionNameInput.setValue(res.divisionName);
    this.schoolNameInput.setValue(res.schoolName);
    this.gradeNameInput.setValue(res.gradeName);
    this.groupStudentNameInput.setValue(res.groupStudentName);

    this.userTypeNameInput.disable();
    this.divisionNameInput.disable();
    this.schoolNameInput.disable();
    this.gradeNameInput.disable();
    this.groupStudentNameInput.disable();

    this.formEditUser.get('username').setValue(res.username);
    this.formEditUser.get('firstName').setValue(res.firstName);
    this.formEditUser.get('lastName').setValue(res.lastName);
    this.formEditUser.get('email').setValue(res.email);
    let genderValue = res.gender == null ? "" : res.gender ? '1' : '0';
    this.formEditUser.get('gender').setValue(genderValue);

    this.formEditUser.get('isActive').setValue(res.status);
    this.formEditUser.get('dpDayOfBirthInput').setValue(res.dayOfBirth);
    this.formEditUser.get('phone').setValue(res.phoneNumber);
    this.formEditUser.get('address').setValue(res.address);

    this.formEditUser.get('password').setValue('');
    this.formEditUser.get('avatar').setValue(res.avatar);

    this.imageUrl = res.avatar;
  }
  onChangeImageInput(event): void {
    var reader = new FileReader();
    let fileData: File = null;
    if (event.target.files && event.target.files[0]) {
      let file = event.target.files[0];

      this.avatarFile = file;
      // this.userEdit.avatar = event.target.files[0];
      // Show preview 
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        this.imageUrl = reader.result;
      }
    }
  }
  uploadImage(): void {
    this.inputImageFile.nativeElement.click();
  }

  submitForm() {
    this.formEditUser.markAllAsTouched();


    if (this.formEditUser.valid) {
      this.userEdit.username = this.formEditUser.value.username?.trim();
      this.userEdit.firstName = this.formEditUser.value.firstName?.trim();
      this.userEdit.lastName = this.formEditUser.value.lastName?.trim();
      this.userEdit.email = this.formEditUser.value.email?.trim();
      if (this.userEdit.email != null) {
        this.userEdit.email = this.userEdit.email.toLowerCase();
      }
      this.userEdit.password = this.formEditUser.value.password;

      this.userEdit.status = this.formEditUser.value.isActive;


      console.log(`this.formEditUser.value.dayOfBirth `, this.formEditUser.get('dpDayOfBirthInput').value);
      this.userEdit.dayOfBirth = this.formEditUser.get('dpDayOfBirthInput').value;
      this.userEdit.avatar = this.formEditUser.value.avatar;
      this.userEdit.academicTitle = this.formEditUser.value.academicTitle;
      this.userEdit.degree = this.formEditUser.value.degree;
      this.userEdit.officeAddress = this.formEditUser.value.officeAddress;
      this.userEdit.address = this.formEditUser.value.address;
      this.userEdit.phone = this.formEditUser.value.phone;
      this.userEdit.gender = this.formEditUser.value.gender == "1" ? true : this.formEditUser.value.gender == "" ? null : false;

      console.log(`this.userEdit: `, this.userEdit);

      Promise.resolve()
        .then(() => {
          return this.avatarFile ? this.fileServerEndpoint.uploadFile({
            file: this.avatarFile,
            isPrivate: false
          }) : null;
        })
        .then((res) => {
          if (res) {
            this.userEdit.avatar = res.link;
          }
          return this.endpointUser.editUser(this.userEdit)
        })
        .then((res) => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Cập nhật người dùng thành công!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            this.router.navigate(['dashboard/manage-users']);
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Cập nhật người dùng không thành công. Vui lòng thử lại!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
      return;
    } else {
      //alert("Kiê");
    }

  }

  btnCancelClicked() {
    this.router.navigate(['dashboard/manage-users']);
    //this.setData(this.userInfoBackUp);
  }

  get f() {
    return this.formEditUser;
  }



  openDpDayOfBirth() {
    this.dpDayOfBirth.open();
  }
}

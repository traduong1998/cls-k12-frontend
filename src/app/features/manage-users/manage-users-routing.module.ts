import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { ManageUsersComponent } from './manage-users.component';
import { AddStudentsFromFileComponent } from './views/add-students-from-file/add-students-from-file.component';
import { AddTeacherFromFileComponent } from './views/add-teacher-from-file/add-teacher-from-file.component';
import { CreateAdminTeacherComponent } from './views/create-admin-teacher/create-admin-teacher.component';
import { CreateUserComponent } from './views/create-user/create-user.component';
import { EditUserComponent } from './views/edit-user/edit-user.component';
import { UsersComponent } from './views/users/users.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: ManageUsersComponent,
    children: [
      {
        path: '',
        component: UsersComponent,
        data: {
          title: 'Quản lý người dùng',
        }
      },
      {
        path: 'create-student',
        component: CreateUserComponent,
        data: {
          title: 'Thêm tài khoản học sinh',
        }
      },
      {
        path: 'create-admin_teacher',
        component: CreateAdminTeacherComponent,
        data: {
          title: 'Thêm tài khoản giáo viên, QTV',
        }
      },
      {
        path: 'edit-user/:id',
        component: EditUserComponent,
        data: {
          title: 'Chỉnh sửa thông tin tài khoản người dùng',
        }
      },
      {
        path: 'add-teachers-from-file',
        component: AddTeacherFromFileComponent,
        data: {
          title: 'Thêm QTV, Giáo viên từ file',
        }
      },
      {
        path: 'add-students-from-file',
        component: AddStudentsFromFileComponent,
        data: {
          title: 'Thêm học sinh từ file',
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageUsersRoutingModule { }

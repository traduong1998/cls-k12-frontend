import { NgModule } from '@angular/core';
import { FormsModule, NgControl } from '@angular/forms';
import { ManageUsersRoutingModule } from './manage-users-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { UsersComponent } from './views/users/users.component';
import { ManageUsersComponent } from './manage-users.component';
import { CreateUserComponent } from './views/create-user/create-user.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { CreateAdminTeacherComponent } from './views/create-admin-teacher/create-admin-teacher.component';
import { EditUserComponent } from './views/edit-user/edit-user.component';
import { AddTeacherFromFileComponent } from './views/add-teacher-from-file/add-teacher-from-file.component';
import { AddStudentsFromFileComponent } from './views/add-students-from-file/add-students-from-file.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [ManageUsersComponent, UsersComponent, CreateUserComponent, CreateAdminTeacherComponent, EditUserComponent, AddTeacherFromFileComponent, AddStudentsFromFileComponent],
  imports: [
    SharedModule,
    ManageUsersRoutingModule,
    NgxMatSelectSearchModule,
    FormsModule,
    NgxSpinnerModule
  ]
})
export class ManageUsersModule { }

import { TestBed } from '@angular/core/testing';

import { CreateContentService } from './create-content.service';

describe('CreateContentService', () => {
  let service: CreateContentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CreateContentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

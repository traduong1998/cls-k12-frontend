import { TestBed } from '@angular/core/testing';

import { LearnMessageService } from './learn-message-service.service';

describe('LearnMessageServiceService', () => {
  let service: LearnMessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LearnMessageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

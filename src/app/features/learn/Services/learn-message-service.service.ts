import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

export interface Option {
  horizontalPosition: MatSnackBarHorizontalPosition;
  verticalPosition: MatSnackBarVerticalPosition;
  duration: number;
}
@Injectable()
export class LearnMessageService {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(private _snackBar: MatSnackBar) { }

  public successToast(message?: string, option?: Option) {
    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
      data: message ?? 'Thêm thành công!',
      duration: (option && option.duration) ? option.duration : 3000,
      horizontalPosition: (option && option.horizontalPosition) ? option.horizontalPosition : this.horizontalPosition,
      verticalPosition: (option && option.verticalPosition) ? option.verticalPosition : this.verticalPosition,
    });
  }

  public failToast(message?: string, option?: Option) {
    this._snackBar.openFromComponent(ErrorSnackBarComponent, {
      data: message ?? "Thêm thất bại",
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  public warToast(message?: string, option?: Option) {
    this._snackBar.openFromComponent(ErrorSnackBarComponent, {
      data: message ?? "Cảnh báo",
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }
}

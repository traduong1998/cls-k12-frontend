import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';
import { ProgressDialogComponent } from '../components/progress-dialog/progress-dialog.component';
import { DataDialogProgress } from '../intefaces/data-dialog-progress';


@Injectable()
export class ProgressDialogService {
  numbers: 0;
  private dialogRef: MatDialogRef<ProgressDialogComponent>
  progressData: Observable<any>;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }
  constructor(public dialog: MatDialog) { }

  /*
  * open progress dialog 
  */
  openProgressDialog(data?: DataDialogProgress): void {
    this.progressData = data.progressData
    var subscription = this.getprogressData().subscribe(value => {
      if (this.dialogRef && this.dialogRef.componentInstance) {
        this.dialogRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: data.fileName, catch: false };
      }
    });

    this.dialogRef = this.dialog.open(ProgressDialogComponent, {
      width: '500px',
      height: '250px',
      data: { numbers: this.numbers }
    });

    this.dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}

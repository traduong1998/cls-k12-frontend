import { takeUntil } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { isNullOrEmpty } from 'src/app/shared/helpers/validation.helper';
import { LessonStatus } from './../intefaces/lessonStatus';
import { TestDetailResponse } from 'cls-k12-sdk-js/src/services/lesson/responses/testDetailResponse';
import { FormatQuestion } from 'cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { UpdateTestRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/updateTestRequest';
import { TestObjectUpdate } from 'sdk/cls-k12-sdk-js/src/services/lesson/enum/objectUpdate';
import { Action } from 'src/app/shared/modules/question-banks/Interfaces/action';
import { AnswerResponse } from 'cls-k12-sdk-js/src/services/lesson/responses/answersResponses';
import { DetailContentResponses } from 'cls-k12-sdk-js/src/services/lesson/responses/detailContentResponses';
import { TestConfigResponses } from 'cls-k12-sdk-js/src/services/lesson/responses/testConfigResponses';
import { AddTestRequest } from 'cls-k12-sdk-js/src/services/lesson/requests/addTestRequest';
import { QuestionTest } from './../intefaces/questionTest';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { TestEndPoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/testEndpoint';
import { debug } from 'console';
import { createDebuggerStatement } from 'typescript';
@Injectable()
export class TestService {
  baseUrl: "http://localhost:58910";

  //Model gửi API thêm bài kiểm tra
  private testRequest: AddTestRequest;

  //Model gửi API update bài kiểm tra
  private updateTestRequest: UpdateTestRequest;
  //List câu hỏi đây
  private questions: QuestionTest[];
  //Subject theo dõi list câu hỏi trên, gán giá trị ban đầu cho nó là mảng rỗng
  private questionsSubject: BehaviorSubject<QuestionTest[]> = new BehaviorSubject<QuestionTest[]>([]);
  //subject theo dõi tổng câu hỏi
  private totalQuestionSubject: BehaviorSubject<number> = new BehaviorSubject(0);
  //subject theo dõi tổng điểm của các câu hỏi
  private totalScoreSubject: BehaviorSubject<number> = new BehaviorSubject(0);
  private isUpdatedContentSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private isUpdatedConfigSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private isAddedContentSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private isAddedConfigSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private isUpdatedContent$: Observable<boolean> = this.isUpdatedContentSubject.asObservable();
  private isUpdatedConfig$: Observable<boolean> = this.isUpdatedConfigSubject.asObservable();
  private isAddedContent$: Observable<boolean> = this.isAddedContentSubject.asObservable();
  private isAddedConfig$: Observable<boolean> = this.isAddedConfigSubject.asObservable();
  private isEditConfig: boolean = false;
  private isEditContent: boolean = false;
  private isEditQuestion: boolean = false;
  private testContent: TestDetailResponse;
  private testConfig: TestConfigResponses;
  //Vì các subject trên là private(chỉ trong service mới next được giá trị mới cho nó), nên ta cần các Observble để bên ngoai lawnsg nghe và nhận giá trị
  //Chuyển các subject trên thành Observble cho các đối tượng bên ngoài subscribe 
  questions$: Observable<QuestionTest[]> = this.questionsSubject.asObservable();
  totalQuestion$: Observable<number> = this.totalQuestionSubject.asObservable();
  totalScore$: Observable<number> = this.totalScoreSubject.asObservable();

  endpoint: TestEndPoint;
  constructor() {
    this.isEditConfig = false;
    this.isEditContent = false;
    this.isEditQuestion = false;
    this.endpoint = new TestEndPoint({ baseUrl: this.baseUrl });
    this.questions = [];
    this.testRequest = null;
    this.testRequest = new AddTestRequest();
    this.updateTestRequest = new UpdateTestRequest();
  }

  /**Cập nhật sữ liệu, bắn các giá trị mới cho các subject */
  private updateQuestionData(): void {
    this.questionsSubject.next(this.questions);
    this.totalQuestionSubject.next(this.questions.length);
    this.totalScoreSubject.next(this.getTotalScore());
  }

  private getTotalScore(): number {
    let toltalScore = 0;
    this.questions.forEach((x) => {
      if (x.action != Action.Delete) {
        toltalScore += x.score;
      }
    });
    return Math.round(toltalScore * 100) / 100;
  }

  public beauty(questions: QuestionTest[]): QuestionTest[] {
    let beautyQuestions = [...questions];
    beautyQuestions.forEach((qst) => {
      //Remove trong nội dung câu hỏi
      qst.content = qst.content.replace(/text-indent:36pt;|&#xa0;|\u0000|<(html|head|\/head|body|\/body|\/html|\?xml|\!DOCTYPE|meta|title|\/title)[^>]{0,}>|<(span)[^>]{0,}>&#xa0;<\/span>/g, "");
      //Remove trong nội dung đáp án
      if (qst.answers != null) {
        qst.answers.forEach((answer) => {
          answer.content = answer.content.replace(/text-indent:36pt;|&#xa0;|\u0000|<(html|head|\/head|body|\/body|\/html|\?xml|\!DOCTYPE|meta|title|\/title)[^>]{0,}>|<(span)[^>]{0,}>&#xa0;<\/span>/g, "");
        });
      }
      //Câu chùm
      if (qst.questions != null) {
        qst.questions.forEach((child) => {
          //Remove trong nội dung câu con
          child.content = child.content.replace(/text-indent:36pt;|&#xa0;|\u0000|<(html|head|\/head|body|\/body|\/html|\?xml|\!DOCTYPE|meta|title|\/title)[^>]{0,}>|<(span)[^>]{0,}>&#xa0;<\/span>/g, "");

          //Remove trong đáp án câu con
          child.answers.forEach((answerChild) => {
            answerChild.content = answerChild.content.replace(/text-indent:36pt;|&#xa0;|\u0000|<(html|head|\/head|body|\/body|\/html|\?xml|\!DOCTYPE|meta|title|\/title)[^>]{0,}>|<(span)[^>]{0,}>&#xa0;<\/span>/g, "");
          });
        });
      }
    })
    return beautyQuestions;
  }


  updateDateTestContent(testContent: TestDetailResponse) {
    this.testContent = { ...testContent };
  }
  updateDateTestConfig(testConfig: TestConfigResponses) {
    this.testConfig = { ...testConfig };
  }
  getListQuestion(testId: number): void {
    this.endpoint.getListQuestionByTestId(testId).then((res) => {
      let id = -1;
      res.map((question) => {
        question.action = Action.Add;
        question.id = id;
        id--;
      });
      this.questions = [...res];
      this.updateQuestionData();
    }); // Gọi api từ service sdk
  }

  /**Lấy danh sách câu trả lời theo mã câu hỏi và map vào câu hỏi */
  getListAnswerByQuestionId(questionId: number): Observable<boolean> {
    //khởi tạo một subject kiểu bool để tí return 1 observble
    let sucsess: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    this.endpoint.getListAnswer(questionId).then((answers) => {
      //map câu trả lời theo mã câu hỏi
      //Xử lý câu chùm với nhaaaaa
      answers.forEach(answer => {
        this.addAnswerToQuestion(answer);
      });
      //Xong thì bắn giá trị true cho thằng gọi hàm từ ngoài biết để xử lý tiếp
      sucsess.next(true);
    });
    //Trả về 1 Observble cho thằng gọi hàm ngoài đăng ký
    return sucsess.asObservable();
  }

  /**Thêm câu trả lời vào câu hỏi theo mã câu hỏi */
  addAnswerToQuestion(answer: AnswerResponse): void {
    //Nhớ xử lý câu chùm
    //Lấy câu hỏi nè
    //nếu đáp án trong câu chùm
    if (answer.questionGroupId > 0) {
      let parent = this.questions.find(x => x.id == answer.questionGroupId);
      let child = parent.questions.find(x => x.id == answer.questionId);
      let index = parent.questions.indexOf(child);
      parent.questions[index].answers.push({
        id: answer.id,
        questionId: answer.questionId,
        content: answer.content,
        trueAnswer: answer.trueAnswer,
        position: answer.position,
        groupOfFillBlank2: answer.groupOfFillBlank2,
        isShuffler: answer.isShuffler
      });
    }
    else {
      let question = this.questions.find(x => x.id == answer.questionId);
      let index = this.questions.indexOf(question);
      //Map đáp án vào thôi
      this.questions[index].answers.push({
        id: answer.id,
        questionId: answer.questionId,
        content: answer.content,
        trueAnswer: answer.trueAnswer,
        position: answer.position,
        groupOfFillBlank2: answer.groupOfFillBlank2,
        isShuffler: answer.isShuffler
      });
    }
  }

  /**Thêm list câu hỏi vào list câu hỏi cũ :v */
  addListQuestion(listQuestionToAdd: QuestionTest[]) {
    this.questions = this.questions.concat(listQuestionToAdd);
    //Xong thì nhớ update dữ liệu, bắn giá trị cho các subject
    this.updateQuestionData();
    //Chech là đã sửa danh sách câu hỏi rồi nha, biết đường mà gửi data lên API
    this.isEditQuestion = true;
  }

  loadDataQuestions(listQuestionToAdd: QuestionTest[]): void {
    this.questions = [...listQuestionToAdd];
    //Xong thì nhớ update dữ liệu, bắn giá trị cho các subject
    this.updateQuestionData();
    this.isEditQuestion = true;
  }

  /**Xóa câu hỏi nha */
  deleteQuestion(question: QuestionTest): void {
    question = { ...question };
    if (question.questionGroupId != null) {
      this.questions.map((qst) => {
        if (qst.id == question.questionGroupId && qst.action != Action.Delete) {
          let totalScore = qst.score;
          let index = qst.questions.findIndex(x => x.id == question.id);
          //Nếu là xóa câu  hỏi mới thêm từ file vào thì xóa bay luôn
          if (question.action == Action.Add) {
            qst.questions.splice(index, 1);
          }
          //Nếu là các câu hỏi cũ thì cần set action = delete cho câu hỏi đó
          else {
            question.action = Action.Delete;
            qst.questions[index] = question;
          }
          qst.score = Math.round(totalScore - question.score);
          if (qst.questions.filter(x => x.action != Action.Delete).length <= 0) {
            this.deleteQuestion(qst);
          }
        }
      })
    }
    else {
      let index = this.questions.indexOf(this.questions.find(x => x.id == question.id));
      if (index != -1) {
        debugger;
        //Câu cha
        if (question.format == FormatQuestion.group) {
          //Nếu là xóa câu  hỏi mới thêm từ file vào thì xóa bay luôn
          if (question.action == Action.Add) {
            this.questions.splice(index, 1);
          }
          //Nếu là các câu hỏi cũ thì cần set action = delete cho câu hỏi đó
          else {
            //let listSplice: QuestionTest[];
            question.questions.forEach((child) => {
              if (child.action == Action.Add) {
                question.questions.splice(index, 1);
              }
              //Nếu là các câu hỏi cũ thì cần set action = delete cho câu hỏi đó
              else {
                child.action = Action.Delete;
                //question.questions[index] = child;
              }
            })
            question.action = Action.Delete;
            this.questions[index] = question;
          }
        }
        //Câu đơn
        else {
          let index = this.questions.indexOf(this.questions.find(x => x.id == question.id));
          //Nếu là xóa câu  hỏi mới thêm từ file vào thì xóa bay luôn
          if (question.action == Action.Add) {
            this.questions.splice(index, 1);
          }
          //Nếu là các câu hỏi cũ thì cần set action = delete cho câu hỏi đó
          else {
            question.action = Action.Delete;
            this.questions[index] = question;
          }
        }
      }
    }
    console.log("Questions:", [...this.questions]);
    //Xong thì nhớ update dữ liệu, bắn giá trị cho các subject
    this.updateQuestionData();
    //Chech là đã sửa danh sách câu hỏi rồi nha, biết đường mà gửi data lên API
    this.isEditQuestion = true;
  }

  checkGroupQuestion(question: QuestionTest, isChecked: boolean) {
    this.questions.map((qts) => {
      if (qts.id == question.id) {
        qts.isChecked = isChecked;
        qts.questions.map((child) => {
          child.isChecked = isChecked;
        })
      }
    });
    this.updateQuestionData();
  }

  checkQuestion(question: QuestionTest, isChecked: boolean): void {
    if (question.questionGroupId == null || question.questionGroupId == undefined) {
      this.questions.map((qst) => {
        if (qst.id == question.id) qst.isChecked = isChecked;
      });
    } else {
      this.questions.map((qst) => {
        if (qst.id == question.questionGroupId) {
          qst.questions.map((child) => {
            if (child.id == question.id) child.isChecked = isChecked;
          })
        }
      });
    }
  }
  /**Xóa các câu hỏi đã check */
  deleteSelected(): void {
    let listDelete: QuestionTest[] = [];
    [...this.questions].forEach((x) => {
      if (x.format == FormatQuestion.group && x.action != Action.Delete) {
        let count = 0;
        [...x.questions].forEach((child) => {
          if (child.isChecked && child.action != Action.Delete) {
            listDelete.push({ ...child })
            count++;
          }
        });
        if (count == x.questions.filter((x) => x.action != Action.Delete).length) {
          listDelete.push({ ...x })
        }
      } else if (x.isChecked && x.action != Action.Delete) {
        listDelete.push({ ...x })
      }
    });
    //Xong thì nhớ update dữ liệu, bắn giá trị cho các subject
    listDelete.forEach((del) => {
      this.deleteQuestion(del);
    })
    //Chech là đã sửa danh sách câu hỏi rồi nha, biết đường mà gửi data lên API
    this.isEditQuestion = true;
  }

  /**Cập nhật điểm cho các câu đã chọn */
  updateScoreQuestions(score: number): void {
    //ko chọn câu nào thì thôi, xử lí chi
    this.questions.map((question) => {
      debugger;
      if (question.format == FormatQuestion.group && question.action != Action.Delete) {
        question.action = ((question.action != Action.Add) ? Action.EditScore : Action.Add);
        let totalScore = 0;
        question.questions.forEach((child) => {
          if (child.action != Action.Delete) {
            if (child.isChecked) {
              child.score = score;
              child.action = ((child.action != Action.Add) ? Action.EditScore : Action.Add);
            }
            totalScore += child.score;
          }
        });
        question.score = Math.round(totalScore * 100) / 100;
      }
      else if (question.isChecked && question.action != Action.Delete) {
        question.score = score;
        question.action = ((question.action != Action.Add) ? Action.EditScore : Action.Add);
      }
    });
    //Xong thì nhớ update dữ liệu, bắn giá trị cho các subject
    this.updateQuestionData();
    //Chech là đã sửa danh sách câu hỏi rồi nha, biết đường mà gửi data lên API
    this.isEditQuestion = true;
  }

  /**Cập nhật câu hỏi(không biết có dùng tới không) */
  updateQuestion(question: QuestionTest, lessonStatus: string): void {
    let newQuestion = { ...question };
    if (question.action != null || question.answers.some(x => x.action != null)) {
      let oldQuestion = this.questions.find(x => x.id == newQuestion.id);
      let index = this.questions.indexOf(oldQuestion);
      //Kiểm tra nếu là update câu hỏi cũ thì đổi action là edit, update câu hỏi mới thì vẫn giữ nguyên create
      newQuestion.action = (oldQuestion.action != Action.Add) ? Action.EditAll : Action.Add;
      this.questions[index] = newQuestion;
      this.updateQuestionData();
      this.isEditQuestion = true;
    }
  }

  updateScore(id: number, score: number) {
    if (score != undefined && score != null) {
      this.questions.map((question) => {
        if (question.id == id) {
          question.score = score;
          question.action = ((question.action != Action.Add) ? Action.EditScore : Action.Add);
        }
      });
      this.isEditQuestion = true;
    }
    this.updateQuestionData();
  }

  updateScoreChild(parentId: number, childId: number, score: number) {
    if (score != undefined && score != null) {
      this.questions.map((question) => {
        if (question.id == parentId) {
          question.questions.map((child) => {
            if (child.id == childId) {
              question.score -= child.score;
              child.score = score;
              question.score = Math.round((question.score + score) * 100) / 100;
              child.action = ((question.action != Action.Add) ? Action.EditScore : Action.Add);
            }
          });
          question.action = ((question.action != Action.Add) ? Action.EditScore : Action.Add);
        }
      });
      this.isEditQuestion = true;
      this.updateQuestionData();
    }
  }


  /**Mấy hàm để xử lý check box */
  checkAll(isChecked: boolean): void {
    this.questions.map((question) => {
      if (question.format == FormatQuestion.group) {
        question.isChecked = isChecked;
        question.questions.forEach((child) => {
          child.isChecked = isChecked
        });
      } else {
        question.isChecked = isChecked
      }
    });
    this.updateQuestionData();
  }
  /**Mấy hàm để xử lý check box */
  someChecked(allComplete: boolean): boolean {
    if (this.questions == undefined) return false;
    //return this.questions.filter(x => x.isChecked).length > 0 && !allComplete;
    let somchecked = false;
    for (let i = 0; i < this.questions.length; i++) {
      if (this.questions[i].isChecked) {
        somchecked = true;
        break;
      }
      if (this.questions[i].questions != null && this.questions[i].questions != undefined) {
        if (this.questions[i].questions.length > 0) {
          for (let j = 0; j < this.questions[i].questions.length; j++) {
            if (this.questions[i].questions[j].isChecked) {
              somchecked = true;
              break;
            }
          }
        }
      }
    }
    return (somchecked && !allComplete)
  }
  /**Mấy hàm để xử lý check box */
  isCheckAll(): boolean {
    if (this.questions == undefined) return false;
    let allChecked = true;
    for (let i = 0; i < this.questions.length; i++) {
      if (!this.questions[i].isChecked) {
        allChecked = false;
        break;
      }
      for (let j = 0; j < this.questions[i].questions.length; j++) {
        if (!this.questions[i].questions[j].isChecked) {
          allChecked = false;
          break;
        }
      }
    }
    return allChecked;
  }

  /**Thêm thông tin cho Model thêm BKT */
  addTestContent(testContent: TestDetailResponse): void {
    this.testRequest.name = testContent.name;
    this.testRequest.contentGroupId = testContent.contentGroupId;
    this.testRequest.listQuestion = this.questions;
    this.testRequest.scoreAfterFinish = testContent.scoreAfterFinish;
    this.testRequest.conditionsForCompletion = testContent.conditionsForCompletion;
    this.testRequest.numberOfQuestion = testContent.numberOfQuestion;
    this.testRequest.totalScore = testContent.totalScore;
    this.testRequest.totalQuizScore = testContent.totalQuizScore;
    this.testRequest.totalEssayScore = testContent.totalEssayScore;
    this.isAddedContentSubject.next(true);
  }

  /**Thêm cấu hình cho Model theme BKT */
  addTestConfig(testConfig: TestConfigResponses): void {
    this.testRequest.viewCorrectResultAfterSubmittingTheTest = testConfig.viewCorrectResultAfterSubmittingTheTest;
    this.testRequest.viewResultsAfterSubmitting = testConfig.viewResultsAfterSubmitting;
    this.testRequest.numberOfQuestionOnThePage = testConfig.numberOfQuestionOnThePage;
    this.testRequest.generateNumberRandomQuetions = testConfig.generateNumberRandomQuetions;
    this.testRequest.preserveTime = testConfig.preserveTime;
    this.testRequest.intervalWriteLog = testConfig.intervalWriteLog;
    this.testRequest.testTime = testConfig.testTime;
    this.testRequest.allowToRedo = testConfig.allowToRedo;
    this.testRequest.theMaximumNumberToRedo = testConfig.theMaximumNumberToRedo;
    this.testRequest.isRandomizeQuestions = testConfig.isRandomizeQuestions;
    this.isAddedConfigSubject.next(true);
  }

  /**Gửi lên API tạo BKT nè */
  public sub: Subscription;
  submitAddTest(lessonId: number): Promise<any> {
    var promise = new Promise((resolve, reject) => {
      let ngUnsubscribe = new Subject();


      let result = new Subject<boolean>();
      // this.isAddedConfigSubject.next(false);
      // this.isAddedContentSubject.next(false);
      let isAddedContent: boolean = false;
      let isAddedConfig: boolean = false;
      let isReady: Subject<boolean> = new Subject<boolean>();
      this.sub = isReady.pipe(takeUntil(ngUnsubscribe)).subscribe((resReady) => {
        if (resReady && this.isRequestAddValid(this.testRequest)) {
          debugger;
          this.testRequest.lessonId = lessonId;
          this.endpoint.addTestLesson(this.testRequest).then((res) => {
            if (res) {
              ngUnsubscribe.next();
              ngUnsubscribe.complete();
              //sub.unsubscribe();
              resolve(true);
              //result.next(true);
            } else {
              ngUnsubscribe.next();
              ngUnsubscribe.complete();
              //sub.unsubscribe();
              resolve(false);
              //result.next(false);
            }
          });
          ngUnsubscribe.next();
          ngUnsubscribe.complete();
          //sub.unsubscribe();
          this.isAddedConfigSubject.next(false);
          this.isAddedContentSubject.next(false);
        }
        else if (!this.isRequestAddValid(this.testRequest)) {
          //sub.unsubscribe();
          ngUnsubscribe.next();
          ngUnsubscribe.complete();
          // isAddedContentSub.unsubscribe();
          // isAddedConfigSub.unsubscribe();
          this.isAddedConfigSubject.next(false);
          this.isAddedContentSubject.next(false);
          resolve(false);
        }
      });

      this.isAddedContent$.pipe(takeUntil(ngUnsubscribe)).subscribe((resContent) => {
        isAddedContent = resContent;
        if (isAddedContent && isAddedConfig) {
          isReady.next(true);
        }
      });

      this.isAddedConfig$.pipe(takeUntil(ngUnsubscribe)).subscribe((resConfig) => {
        isAddedConfig = resConfig;
        if (isAddedContent && isAddedConfig) {
          isReady.next(true);
        }
      });
    });
    return promise;
  }


  /**Thêm thông tin cho Model thêm BKT */
  updateTestContent(testContent: TestDetailResponse, isNewContent: boolean = true): void {
    console.log("update content");
    this.updateTestRequest.name = testContent.name;
    this.updateTestRequest.contentGroupId = testContent.contentGroupId;
    this.updateTestRequest.listQuestion = this.questions;
    this.updateTestRequest.conditionsForCompletion = testContent.conditionsForCompletion;
    this.updateTestRequest.scoreAfterFinish = testContent.scoreAfterFinish;
    this.testRequest.numberOfQuestion = testContent.numberOfQuestion;
    this.testRequest.totalScore = testContent.totalScore;
    this.testRequest.totalQuizScore = testContent.totalQuizScore;
    this.testRequest.totalEssayScore = testContent.totalEssayScore;
    //Gán isEditContent = isNewContent để biết là đã sửa thông tin BKT rồi nha
    this.isEditContent = isNewContent;
    this.isUpdatedContentSubject.next(true);
  }

  /**Thêm cấu hình cho Model theme BKT */
  updateTestConfig(testConfig: TestConfigResponses, isNewCofig: boolean = true): void {
    console.log("update config");
    this.updateTestRequest.viewCorrectResultAfterSubmittingTheTest = testConfig.viewCorrectResultAfterSubmittingTheTest;
    this.updateTestRequest.viewResultsAfterSubmitting = testConfig.viewResultsAfterSubmitting;
    this.updateTestRequest.numberOfQuestionOnThePage = testConfig.numberOfQuestionOnThePage;
    this.updateTestRequest.generateNumberRandomQuetions = testConfig.generateNumberRandomQuetions;
    this.updateTestRequest.preserveTime = testConfig.preserveTime;
    this.updateTestRequest.intervalWriteLog = testConfig.intervalWriteLog;
    this.updateTestRequest.testTime = testConfig.testTime;
    this.updateTestRequest.allowToRedo = testConfig.allowToRedo;
    this.updateTestRequest.theMaximumNumberToRedo = testConfig.theMaximumNumberToRedo;
    this.updateTestRequest.isRandomizeQuestions = testConfig.isRandomizeQuestions;
    //Gán isEditConfig = isNewCofig để biết là đã sửa cấu hình BKT rồi nha
    this.isEditConfig = isNewCofig;
    this.isUpdatedConfigSubject.next(true);
  }

  public editSub: Subscription;
  submitEditTest(lessonId: number, contentId: number, lessonStatus: string): Promise<any> {
    var promise = new Promise((resolve, reject) => {
      let ngUnsubscribe = new Subject();

      let isUpdatedContent: boolean = false;
      let isUpdatedConfig: boolean = false;
      let isReady: Subject<boolean> = new Subject<boolean>();




      this.editSub = isReady.pipe(takeUntil(ngUnsubscribe)).subscribe((resReady) => {
        if (resReady && this.isRequestUpdateValid(this.updateTestRequest)) {
          if (!this.isEditConfig && !this.isEditContent && !this.isEditQuestion) {
            //result.next(true);
            ngUnsubscribe.next();
            ngUnsubscribe.complete();
            resolve(true);
          }
          this.updateTestRequest.lessonId = lessonId;
          this.updateTestRequest.id = contentId;

          if (this.isEditConfig) {
            this.updateTestRequest.objectUpdate.push(TestObjectUpdate.Config);
          }
          if (this.isEditQuestion) {
            this.updateTestRequest.objectUpdate.push(TestObjectUpdate.Questions);
          }
          if (this.isEditContent) {
            this.updateTestRequest.objectUpdate.push(TestObjectUpdate.Information);
          }
          if (lessonStatus == LessonStatus.Publish) {
            this.endpoint.editTestLessonAfter(this.updateTestRequest).then((res) => {
              console.log("ressss", res);
              if (res) {
                console.log("Updateed true");
                ngUnsubscribe.next();
                ngUnsubscribe.complete();
                resolve(true);
                //result.next(true);
              } else {
                console.log("Updateed false");
                ngUnsubscribe.next();
                ngUnsubscribe.complete();
                resolve(false);
                //return true;
                //result.next(false);
              }
            });
          }
          else {
            this.endpoint.editTestLessonBefore(this.updateTestRequest).then((res) => {
              if (res) {
                ngUnsubscribe.next();
                ngUnsubscribe.complete();
                resolve(true);
                //return true;
                //result.next(true);
              } else {
                ngUnsubscribe.next();
                ngUnsubscribe.complete();
                resolve(false);
                //return false;
                /// result.next(false);
              }
            });
          }
          ngUnsubscribe.next();
          ngUnsubscribe.complete();
          this.isUpdatedConfigSubject.next(false);
          this.isUpdatedContentSubject.next(false);

        }
        else if (!this.isRequestAddValid(this.testRequest)) {
          console.log("updateRequest");
          ngUnsubscribe.next();
          ngUnsubscribe.complete();
          this.isUpdatedConfigSubject.next(false);
          this.isUpdatedContentSubject.next(false);
          resolve(false);
        }
      });


      this.isUpdatedContent$.pipe(takeUntil(ngUnsubscribe)).subscribe((resContent) => {
        isUpdatedContent = resContent;
        if (isUpdatedContent && isUpdatedConfig) {
          isReady.next(true);
          console.log("ready", isUpdatedContent, isUpdatedConfig);
        }
        else{
          console.log("just cotnent", isUpdatedContent, isUpdatedConfig);
        }
      });

      this.isUpdatedConfig$.pipe(takeUntil(ngUnsubscribe)).subscribe((resConfig) => {
        isUpdatedConfig = resConfig;
        if (isUpdatedContent && isUpdatedConfig) {
          isReady.next(true);
          console.log("ready", isUpdatedContent, isUpdatedConfig);
        }  else{
          console.log("just config",isUpdatedContent, isUpdatedConfig);
        }
      });
   
    });

    return promise;
  }
  isRequestUpdateValid(updateRequest: UpdateTestRequest): boolean {
    if (isNullOrEmpty(updateRequest.name)) return false;
    if (updateRequest.numberOfQuestionOnThePage > updateRequest.numberOfQuestion) return false;
    if (updateRequest.scoreAfterFinish > this.getTotalScore() || updateRequest.scoreAfterFinish == null || updateRequest.scoreAfterFinish == undefined) return false;
    if (updateRequest.generateNumberRandomQuetions > updateRequest.numberOfQuestion) return false;
    return true;
  }
  isRequestAddValid(updateRequest: AddTestRequest): boolean {
    if (isNullOrEmpty(updateRequest.name)) return false;
    if (updateRequest.numberOfQuestionOnThePage > updateRequest.numberOfQuestion) return false;
    if (updateRequest.scoreAfterFinish > this.getTotalScore() || updateRequest.scoreAfterFinish == null || updateRequest.scoreAfterFinish == undefined) return false;
    if (updateRequest.generateNumberRandomQuetions > updateRequest.numberOfQuestion) return false;
    return true;
  }
  clear(): void {
    this.questions = [];
    this.updateQuestionData();
  }
}

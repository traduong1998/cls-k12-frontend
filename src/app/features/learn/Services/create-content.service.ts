import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export interface GroupContent {
  name: string;
  status: string;

}

export class ListGroupContent {
  id: number;
  name: string;
  groupContent: GroupContent[]
}


@Injectable()
export class CreateContentService {
  listGroupContent: ListGroupContent[];
  groupContentSubject: BehaviorSubject<ListGroupContent[]> = new BehaviorSubject<ListGroupContent[]>([]);
  groupContent$: Observable<ListGroupContent[]> = this.groupContentSubject.asObservable();


  constructor() { }

  public addGroupContent(content: any) {
    this.listGroupContent.push(content);
    
    this.updateData();
  }

  public updateData() {
    this.groupContentSubject.next(this.listGroupContent);
  }
}

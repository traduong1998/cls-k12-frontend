import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { AnswerTest, QuestionTest } from 'src/app/features/learn/intefaces/questionTest';
import { TestService } from './../../Services/test.service';
import { Component, Input, OnInit, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { TestDetailResponse } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/testDetailResponse';
import { TestConfigResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/testConfigResponses';
import { UseCase } from '../../intefaces/useCase';
import { TestEndPoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/testEndpoint';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    selector: 'learn-tab-add-test',
    templateUrl: './tab-add-test.component.html',
    styleUrls: ['./tab-add-test.component.scss']
})

export class TabAddTestComponent implements OnInit, OnChanges {
    baseUrl: "http://localhost:58910";
    @Input() useCase: number;
    @Input() contentId?: number;
    //Input này là observable lắng nghe sự kiện click save ở ngoài view chính để truyền vào trong mấy tab con
    @Input() submitEvents: Observable<void>;
    @Input() lessonId: number;
    @Input() lessonStatus: string;
    @Output() invalidContent = new EventEmitter<boolean>();
    @Output() invalidConfig = new EventEmitter<boolean>();
    testEndPoint: TestEndPoint;

    //Chia ra 2 model này để truyền input cho 2 tab content và config ở trong
    testDetailResponse: TestDetailResponse;
    testConfigResponse: TestConfigResponses;
    currentTabIndex: number;
    isReady: boolean = false;
    public get useCaseEnum(): typeof UseCase {
        return UseCase;
    }
    constructor(private testService: TestService, private httpClient: HttpClient, private spinner: NgxSpinnerService) {
        this.testEndPoint = new TestEndPoint({ baseUrl: this.baseUrl });
    }
    ngOnChanges(changes: SimpleChanges): void {
        //Nếu là sửa thì gọi API get content và binding dữ liệu cho các tab
        if (this.useCase == this.useCaseEnum.editting) {
            this.getContentTest(this.contentId, this.lessonId);
        }
        //Tạo mới thì thôi khỏi, dữ liệu sẵn sàng rồi
        else this.isReady = true;
    }
    ngOnInit(): void {
    }
    getContentTest(contentId: number, lessonId: number): void {
        this.spinner.show();
        this.testEndPoint.getContentTest(contentId, lessonId).then((res) => {
            //Bind phần thông tin vào detaiResponse model để truyền input  cho tab content
            this.testDetailResponse = {
                id: res.id,
                name: res.name,
                conditionsForCompletion: res.conditionsForCompletion,
                contentGroupId: res.contentGroupId,
                scoreAfterFinish: res.scoreAfterFinish,
                numberOfQuestion: res.numberOfQuestion,
                totalScore: res.totalScore,
                totalQuizScore: res.totalQuizScore,
                totalEssayScore: res.totalEssayScore
            };

            //Tương tự cũng bind phần config vào model và tuyền cho tab config
            this.testConfigResponse = {
                id: res.id,
                viewCorrectResultAfterSubmittingTheTest: res.viewCorrectResultAfterSubmittingTheTest,
                viewResultsAfterSubmitting: res.viewResultsAfterSubmitting,
                numberOfQuestionOnThePage: res.numberOfQuestionOnThePage,
                generateNumberRandomQuetions: res.generateNumberRandomQuetions,
                preserveTime: res.preserveTime,
                intervalWriteLog: res.intervalWriteLog,
                testTime: res.testTime,
                allowToRedo: res.allowToRedo,
                theMaximumNumberToRedo: res.theMaximumNumberToRedo,
                isRandomizeQuestions: res.isRandomizeQuestions,
                numberOfQuestion: res.numberOfQuestion
            };
            if (res.urlJsonTest) {
                this.GetListQuestionFromUrl(res.urlJsonTest).subscribe((res) => {
                    console.log("questions");
                    //Thêm list câu hỏi vào testService
                    let result: QuestionTest[] = res["questions"];
                    let listQuestions: QuestionTest[] = [];
                    result.forEach(question => {

                        let qst = new QuestionTest(question);
                        if (question.format == FormatQuestion.group) {
                            qst.questions = [];
                            question.questions.forEach((child) => {
                                let childQuestion = new QuestionTest(child);
                                childQuestion.answers = [];
                                childQuestion.questionGroupId = question.id;
                                child.answers.forEach(ans => {
                                    let answer: AnswerTest = {
                                        id: ans.id,
                                        content: ans.content,
                                        questionId: ans.questionId,
                                        trueAnswer: ans.trueAnswer,
                                        groupOfFillBlank2: ans.groupOfFillBlank2,
                                        position: ans.position,
                                        isShuffler: ans.isShuffler ? ans.isShuffler : false,
                                        action: null
                                    };
                                    childQuestion.answers.push(answer);
                                });
                                qst.questions.push(childQuestion);
                            });
                        } else {
                            qst.answers = [];
                            question.answers.forEach((ans) => {
                                let answer: AnswerTest = {
                                    id: ans.id,
                                    content: ans.content,
                                    questionId: ans.questionId,
                                    trueAnswer: ans.trueAnswer,
                                    groupOfFillBlank2: ans.groupOfFillBlank2,
                                    position: ans.position,
                                    isShuffler: ans.isShuffler ? ans.isShuffler : false,
                                    action: null
                                };
                                qst.answers.push(answer);
                            });
                        }
                        listQuestions.push(qst);
                    });
                    this.testService.loadDataQuestions(listQuestions);
                    // Dữ liệu đã sẵn sàng nha
                    this.spinner.hide();
                    this.isReady = true;
                });
            } else {
                this.spinner.hide();
                this.isReady = true;
            }
        })
    }
    public GetListQuestionFromUrl(url: string): Observable<any> {
        return this.httpClient.get(url);
    }
    tabChanged(tabChangeEvent: MatTabChangeEvent): void {
        this.currentTabIndex = tabChangeEvent.index;
    }
    invalidContentEmit(invalid: boolean): void{
        this.invalidContent.emit(invalid);
    }
    invalidConfigEmit(invalid: boolean): void{
        this.invalidConfig.emit(invalid);
    }
}

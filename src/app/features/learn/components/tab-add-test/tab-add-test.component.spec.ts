import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabAddTestComponent } from './tab-add-test.component';

describe('TabAddTestComponent', () => {
  let component: TabAddTestComponent;
  let fixture: ComponentFixture<TabAddTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabAddTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabAddTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

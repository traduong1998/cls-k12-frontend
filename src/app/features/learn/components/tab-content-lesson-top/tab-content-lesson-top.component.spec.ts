import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabContentLessonTopComponent } from './tab-content-lesson-top.component';

describe('TabContentLessonTopComponent', () => {
  let component: TabContentLessonTopComponent;
  let fixture: ComponentFixture<TabContentLessonTopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabContentLessonTopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabContentLessonTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

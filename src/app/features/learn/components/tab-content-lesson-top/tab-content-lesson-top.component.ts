import { Component, Input, OnInit, Output } from '@angular/core';
import { UseCase } from './../../intefaces/useCase';
import { MatDialog } from '@angular/material/dialog';
import { EventEmitter } from 'events';
import { AddPartDialogComponent } from '../add-part-dialog/add-part-dialog.component';

@Component({
  selector: 'learn-tab-content-lesson-top',
  templateUrl: './tab-content-lesson-top.component.html',
  styleUrls: ['./tab-content-lesson-top.component.scss']
})
export class TabContentLessonTopComponent implements OnInit {

  @Output() outputGroupContent: EventEmitter = new EventEmitter();
  @Input() useCase: number;
  isShowContent = false;
  public get useCaseEnum(): typeof UseCase {
    return UseCase;
  }
  constructor(public dialog: MatDialog){}

  ngOnInit(): void {
  }
  

}

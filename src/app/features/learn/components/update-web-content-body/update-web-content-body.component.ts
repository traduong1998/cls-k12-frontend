import { MatSnackBar } from '@angular/material/snack-bar';
import { ContentGroupForCombobox } from './../../../../../../sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Option } from '../../../../shared/interface/Option';
import { MatDialog } from '@angular/material/dialog';
import { AddQuestionFromFileDialogComponent } from '../../components/add-question-from-file-dialog/add-question-from-file-dialog.component';
import { FormControl, Validators } from '@angular/forms';
import { DetailContentRequests } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/detailContentRequests';
import { ContentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentEndpoint';
import { DetailContentResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/detailContentResponses';
import { ActivatedRoute, Router } from '@angular/router';
//qsb
import { QuestionFileService } from './../../../../core/services/question-file/question-file.service';
import { Question } from 'sdk/cls-k12-sdk-js/src/services/question/models/Question';
import { Answer } from 'sdk/cls-k12-sdk-js/src/services/question/models/Answer';
import { UseCase } from '../../intefaces/useCase';
import { Observable } from 'rxjs';
import { QuestionTest } from '../../intefaces/questionTest';
import { DialogUpdateScoreComponent } from '../dialog-update-score/dialog-update-score.component';
import { DialogImportQuestionsFromFileComponent } from '../dialog-import-questions-from-file/dialog-import-questions-from-file.component';
import { TestService } from '../../Services/test.service';
import { CKEditor4, CKEditorComponent } from 'ckeditor4-angular';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';

//grc
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { CreateCurriculumRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/createCurriculumRequest';
import { UpdateInfoContentRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/updateInfoContentRequest';
import { ContentGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentGroupEndpoint';
import { ContentGroupResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { LessonStatus } from '../../intefaces/lessonStatus';
import { CLS } from 'sdk/cls-k12-sdk-js/src';

@Component({
  selector: 'learn-update-web-content-body',
  templateUrl: './update-web-content-body.component.html',
  styleUrls: ['./update-web-content-body.component.scss']
})
export class UpdateWebContentBodyComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';
  @Input() detailContentResponses: DetailContentResponses;
  @Input() contentId: number;
  @Input() lessonStatus: string;
  lessonName = "PHƯƠNG TRÌNH BẬC 2 VÀ ĐỊNH LÝ VIET"
  configCkeditor = {};
  group_content: ContentGroupForCombobox[] = [];
  chooseGroupContent: number;
  contentGroupId: number;
  nameContent: string;
  conditionsForCompletion: string;
  minutes: number = 0;
  seconds: number = 0;
  contentCkEditor: string;
  detailContentRequests: DetailContentRequests;
  contentEndpoint: ContentEndpoint;
  contentGroupEndpoint: ContentGroupEndpoint;
  detailContent: DetailContentRequests;
  createCurriculumRequest: CreateCurriculumRequest;
  updateInfoContentRequest: UpdateInfoContentRequest;
  //qsb
  questions$: Observable<QuestionTest[]>;
  totalScore$: Observable<number>;
  allComplete: boolean = false;
  showListQuestion = false;
  question: Question = null;
  data: Question[] = [];
  displayedColumns: string[] = ['check', 'content', 'delete'];
  isReady: boolean = false;
  keyWord: string = "";
  nameContentControl = new FormControl('', [Validators.required, Validators.maxLength(255)]);

  minutesControl = new FormControl('', [Validators.required, Validators.min(0)]);

  secondsControl = new FormControl('', [Validators.required, Validators.min(0)]);
  //grc
  public groupContentControl: FormControl = new FormControl();
  public groupContentFilterControl: FormControl = new FormControl();
  public filteredGroupContents: ReplaySubject<ContentGroupForCombobox[]> = new ReplaySubject<ContentGroupForCombobox[]>(0);
  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();
  @ViewChild("ck") ck: CKEditorComponent;
  //grc
  @ViewChild("singleSelect") singleSelect: MatSelect;
  @ViewChild("fileQuestion") fileQuestion: ElementRef;

  constructor(public dialog: MatDialog, private route: ActivatedRoute, private questionFilleService: QuestionFileService, private testService: TestService, private routerr: Router, private _snackBar: MatSnackBar) {
    this.contentEndpoint = new ContentEndpoint();
    this.detailContentRequests = new DetailContentRequests;
    this.detailContentResponses = new DetailContentResponses;
    this.detailContent = new DetailContentRequests;
    this.createCurriculumRequest = new CreateCurriculumRequest;
    this.updateInfoContentRequest = new UpdateInfoContentRequest;
    this.contentGroupEndpoint = new ContentGroupEndpoint();
    this.contentCkEditor = this.detailContent.detailsOfContent;
    //ckeditor
    this.configCkeditor = {
      height: 150,
      allowedContent: false,
      EditorType: CKEditor4.EditorType.CLASSIC
    };
    this.baseApiUrl = CLS.getConfig().apiBaseUrl;

  }

  ngOnChanges() {
    this.detailContentRequests = { ...this.detailContentResponses };
    this.contentGroupEndpoint.getListContentGroupForCombobox(this.detailContentResponses.lessonId).then((res) => {
      this.group_content = res;
      this.group_content.unshift({ id: 0, name: "Chọn tiết", order: -1 });
      console.log("contentGroups:------>>>>>>", res);
      this.filteredGroupContents.next(this.group_content);
      this.filteredGroupContents.subscribe((res) => {
        this.chooseGroupContent =this.detailContentResponses.contentGroupId;
      });
      this.isReady = true;
    });
    this.groupContentControl.setValue(this.group_content[this.detailContentResponses.contentGroupId - 1]);
    this.chooseGroupContent = this.detailContentResponses.contentGroupId;
    this.nameContent = this.detailContentResponses.name;
    this.contentCkEditor = this.detailContentResponses.detailsOfContent;
    if (this.detailContentResponses.conditionsForCompletion == "INT") {
      this.conditionsForCompletion = "INT",
        this.minutes = Math.floor(this.detailContentResponses.timeToComplete / 60);
      this.seconds = (this.detailContentResponses.timeToComplete % 60);
    }
    else if (this.detailContentResponses.conditionsForCompletion == "ANS") {
      this.conditionsForCompletion = "ANS";
      if (this.detailContentResponses.testId != null) {
        this.showListQuestion = true;
        this.testService.getListQuestion(this.contentId);
      }
    }
    else if (this.detailContentResponses.conditionsForCompletion == "BUT") {
      this.conditionsForCompletion = "BUT";
    }
    if (!this.detailContentResponses.contentGroupId == null) {
    }
  }
  ngOnInit(): void {
    //qsb
    this.testService.getListQuestion(this.contentId);
    this.questions$ = this.testService.questions$;
    this.totalScore$ = this.testService.totalScore$;
    console.log("c", this.questions$);
  }
  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  onGroupContentChanged(grc: Option) {
    this.contentGroupId = grc.id;
    console.log('tttt', grc.name);
  }
  openDialog() {
    const dialogRef = this.dialog.open(AddQuestionFromFileDialogComponent, {
      width: '650px',
      height: 'auto',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.showListQuestion = result;
      console.log(`Dialog result: ${result}`);
    });
  }
  filterBanks() {
    if (!this.group_content) {
      return;
    }
    // get the search keyword
    let search = this.keyWord;
    if (!search) {
      this.filteredGroupContents.next(this.group_content.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredGroupContents.next(
      this.group_content.filter(bank => bank.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onCancelClick(): void {
  }
  checkInterval(): boolean {
    if (this.conditionsForCompletion == "INT") {
      if (this.detailContentResponses.timeToComplete == this.minutes * 60 + this.seconds) {
        return true;
      }
    }
    return false;
  }
  onSaveClick(): void {
    this.detailContentRequests.lessonId = this.detailContentResponses.lessonId;
    this.detailContentRequests.id = this.detailContentResponses.id;
    if (isNullOrWhiteSpace(this.nameContent) || isNullOrEmpty(this.nameContent)) {
      this._snackBar.open("Chưa nhập tên nội dung", 'WAR', {
        duration: 3000,
        panelClass: ['red-snackbar'],
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
      return;
    }
    else {
      this.detailContentRequests.name = this.nameContent;
    }
    this.detailContentRequests.typesOfContent = "WEB";
    if (this.conditionsForCompletion == null) {
      this._snackBar.open("Chưa chọn điều kiện hoàn thành nội dung", 'WAR', {
        duration: 3000,
        panelClass: ['red-snackbar'],
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
      return;
    }
    else {
      this.detailContentRequests.conditionsForCompletion = this.conditionsForCompletion;
      if (this.conditionsForCompletion == 'INT') {
        if (this.minutes < 0 || this.seconds < 0 || this.minutes == null || this.seconds == null) {
          this._snackBar.open("Thời gian hoàn thành không hợp lệ", 'WAR', {
            duration: 3000,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });
          return;
        }
        else {
          this.detailContentRequests.timeToComplete = this.minutes * 60 + this.seconds;
        }
      }
    }

    if (this.chooseGroupContent > 0) {
      this.detailContentRequests.contentGroupId = this.chooseGroupContent;
    }
    else {
      this.detailContentRequests.contentGroupId = null;
    }
    this.detailContentRequests.detailsOfContent = this.contentCkEditor;


    if (JSON.stringify(this.detailContentRequests) === JSON.stringify(this.detailContentResponses)) {
      alert(`Dữ liệu không thay đổi`);
      return;
    }
    if (this.detailContentRequests.fileId != this.detailContentResponses.fileId ||
      this.detailContentRequests.link != this.detailContentResponses.link ||
      this.detailContentRequests.detailsOfContent != this.detailContentResponses.detailsOfContent ||
      this.detailContentRequests.description != this.detailContentResponses.description
    ) {
      if (
        this.detailContentRequests.name != this.detailContentResponses.name ||
        this.detailContentRequests.conditionsForCompletion != this.detailContentResponses.conditionsForCompletion ||
        this.detailContentRequests.timeToComplete != this.detailContentResponses.timeToComplete ||
        this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId) {
        if (this.lessonStatus == LessonStatus.Publish) {
          this.contentEndpoint.updateContentAfterPublish(this.detailContentRequests).then((res) => {
            if (res) {
              alert('Lưu thành công');
              this.detailContentResponses = { ...this.detailContentRequests };
              this.routerr.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);
            } else {
              alert('Dữ liệu không thay đổi');
            }
          });
        }
        else {
          this.contentEndpoint.updateContentBeforPublish(this.detailContentRequests).then((res) => {
            if (res) {
              alert('Lưu thành công');
              this.detailContentResponses = { ...this.detailContentRequests };
              this.routerr.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);
            } else {
              alert('Dữ liệu không thay đổi');
            }
          });
        }
      }
      else {
        this.createCurriculumRequest.id = this.detailContentResponses.id;
        this.createCurriculumRequest.fileId = this.detailContentRequests.fileId;
        this.createCurriculumRequest.link = this.detailContentRequests.link;
        this.createCurriculumRequest.detailsOfContent = this.detailContentRequests.detailsOfContent;
        this.createCurriculumRequest.description = this.detailContentRequests.description;
        this.contentEndpoint.createCurriculum(this.detailContentRequests.lessonId, this.createCurriculumRequest).then((res) => {
          if (res) {
            alert('Lưu curriculum thành công');
            this.detailContentResponses = { ...this.detailContentRequests };
            this.routerr.navigateByUrl(`/learn/lesson/${this.detailContentRequests.lessonId}`);
          } else {
            alert('Dữ liệu không thay đổi');
          }
        })
      }

    }
    else {
      if (this.detailContentRequests.name != this.detailContentResponses.name ||
        this.detailContentRequests.conditionsForCompletion != this.detailContentResponses.conditionsForCompletion ||
        this.detailContentRequests.timeToComplete != this.detailContentResponses.timeToComplete ||
        this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId) {
        this.updateInfoContentRequest.name = this.detailContentRequests.name;
        this.updateInfoContentRequest.conditionsForCompletion = this.detailContentRequests.conditionsForCompletion;
        this.updateInfoContentRequest.timeToComplete = this.detailContentRequests.timeToComplete;
        this.updateInfoContentRequest.id = this.detailContentResponses.id;
        this.updateInfoContentRequest.contentGroupId = this.detailContentRequests.contentGroupId;
        this.contentEndpoint.updateInfoContent(this.detailContentRequests.lessonId, this.updateInfoContentRequest).then((res) => {
          if (res) {
            alert('Lưu info thành công');
            this.detailContentResponses = { ...this.detailContentRequests };
            this.routerr.navigateByUrl(`/learn/lesson/${this.detailContentRequests.lessonId}`);
          } else {
            alert('Dữ liệu không thay đổi');
          }
        })
      }
    }
  }

  //qsb

  someComplete(): boolean {
    return this.testService.someChecked(this.allComplete);
  }

  setScoreQuestions(score: number): void {
    this.testService.updateScoreQuestions(score);
  }

  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.testService.checkAll(completed);
  }

  updateAllComplete() {
    this.allComplete = this.testService.isCheckAll();
  }

  deleteQuestion(id: number): void {
    console.log(id);
    //this.testService.deleteQuestion(id);
  }

  deleteSelected(): void {
    this.testService.deleteSelected();
  }
  openUpdateScoreDialog(): void {
    if (!this.someComplete() && !this.allComplete) {
      alert("Không có câu hỏi nào được chọn!");
      return;
    }
    const dialogRef = this.dialog.open(DialogUpdateScoreComponent, {
      width: '377px',
      height: '234px',
      data: this.totalScore$
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      if (result) {
        this.setScoreQuestions(result);
      }
    });
  }

  openImportQuestionsDialog(): void {
    const dialogRef = this.dialog.open(DialogImportQuestionsFromFileComponent, {
      width: '650px',
      //data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  addQuestionFromFile() {
    this.fileQuestion.nativeElement.click();
  }

  //get file
  handle(e) {
    const file = e.target.files[0];
    if (file.name != null) {
      this.showListQuestion = true;
    }
  }
  ngAfterViewInit(): void {
    //load data question
    this.loadData(this.data);
  }

  updateQuestion(question) {
   // this.testService.updateQuestion(question);
    console.log("question edit", this.question);
  }

  loadData(data) {
    this.data = [];
    this.data = [...this.data, ...data];
  }

  delete(element) {
    this.testService.deleteQuestion(element.id);
  }

  chooseQuestion(item) {
    console.log("chosed", item);
    this.question = item;
    console.log(this.question);
  }

  getQuestionData() {
  
  }

  public getLinkTemplate(cases: number) {
    if (cases == 1) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-mau.docx`;
    }
    else if (cases == 2) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/huong-dan-su-dung-tep-tin-mau.docx`;
    }
  }
}

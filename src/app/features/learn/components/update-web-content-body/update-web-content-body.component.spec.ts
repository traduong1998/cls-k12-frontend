import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateWebContentBodyComponent } from './update-web-content-body.component';

describe('UpdateWebContentBodyComponent', () => {
  let component: UpdateWebContentBodyComponent;
  let fixture: ComponentFixture<UpdateWebContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateWebContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateWebContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

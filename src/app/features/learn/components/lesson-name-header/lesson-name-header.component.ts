import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'learn-lesson-name-header',
  templateUrl: './lesson-name-header.component.html',
  styleUrls: ['./lesson-name-header.component.scss']
})
export class LessonNameHeaderComponent implements OnInit {

  @Input() lessonName: string;
  @Input() type: string = '';
  constructor() { }

  ngOnInit(): void {
  }

}

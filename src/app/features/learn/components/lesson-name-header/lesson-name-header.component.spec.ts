import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonNameHeaderComponent } from './lesson-name-header.component';

describe('LessonNameHeaderComponent', () => {
  let component: LessonNameHeaderComponent;
  let fixture: ComponentFixture<LessonNameHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LessonNameHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonNameHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

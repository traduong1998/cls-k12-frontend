import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepHeaderLessonComponent } from './step-header-lesson.component';

describe('StepHeaderLessonComponent', () => {
  let component: StepHeaderLessonComponent;
  let fixture: ComponentFixture<StepHeaderLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepHeaderLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StepHeaderLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

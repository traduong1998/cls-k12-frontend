import { StepHeaderControl } from './../../intefaces/stepHeaderControl';
import { StepHeader, StepType } from './../../intefaces/stepHeader';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, Input, OnInit, OnChanges, SimpleChanges, DoCheck } from '@angular/core';

@Component({
  selector: 'learn-step-header-lesson',
  templateUrl: './step-header-lesson.component.html',
  styleUrls: ['./step-header-lesson.component.scss']
})
export class StepHeaderLessonComponent implements OnInit, OnChanges {
  @Input() stepHeaderCtrl: StepHeaderControl;
  steps: StepHeader[] = [];
  isReady: boolean = false;
  public get stepTypeEnum(): typeof StepType {
    return StepType;
  }
  constructor(private _router: Router, private route: ActivatedRoute) {
    //khởi tạo giá trị ban đầu cho các step
    this.steps = [
      { step: 1, label: "THÔNG TIN", type: StepType.basic, currentStep: false, url: "", iconBasic: "info", iconActive: "info", iconDisabled: "info-baned" },
      { step: 2, label: "NỘI DUNG", type: StepType.basic, currentStep: false, url: "", iconBasic: "content", iconActive: "content-completed", iconDisabled: "" },
      { step: 3, label: "THIẾT LẬP", type: StepType.basic, currentStep: false, url: "", iconBasic: "setting", iconActive: "setting-completed", iconDisabled: "setting-baned" },
      { step: 4, label: "HOÀN THÀNH", type: StepType.basic, currentStep: false, url: "", iconBasic: "finish", iconActive: "finish-completed", iconDisabled: "finish-baned" }
    ];
  }
  ngOnChanges(changes: SimpleChanges): void {
    //Page hiện tại thì gán currentPage = true
    if(this.steps[this.stepHeaderCtrl?.currentPage - 1]){
      this.steps[this.stepHeaderCtrl?.currentPage - 1].currentStep = true;
    }
    //Set url cho các step
    this.setUrlAllStep();
    // Kiểm tra giá trị step truyền vào, đang ở bước nào thì active tất cả các bước trước nó
    this.steps.forEach(x => {
      if (x.step <= (this.stepHeaderCtrl.step)) {
        x.type = StepType.active;
      } else {
        x.type = StepType.basic;
      }
      if (this.stepHeaderCtrl.currentPage - 1 == this.stepHeaderCtrl.step) {
        this.steps[this.stepHeaderCtrl.currentPage - 1].type = StepType.active;
      }
      //Nếu là đang tạo nội dung thì disable các bước <= step hiện tại (trừ bước nội dung)
      if (this.stepHeaderCtrl.isCreatingContent) {
        this.steps[0].type = StepType.disabled;
        this.steps.map((singleStep) => {
          if (singleStep.step <= this.stepHeaderCtrl.step && singleStep.step != 2) {
            singleStep.type = StepType.disabled;
          }
        });
      };
    });
  }

  ngOnInit(): void {
    this.isReady = true;
  }

  setUrlAllStep(): void {
    this.steps[0].url = "/dashboard/learn/edit-lesson/";
    this.steps[1].url = "/dashboard/learn/edit-content-lesson/";
    this.steps[2].url = "/dashboard/learn/edit-lesson-config/";
    this.steps[3].url = "/dashboard/learn/finish-lesson/";
  }

  //Hàm router ccho các button step
  onClickStep(item: StepHeader): void {
    if (item.type == StepType.active && this.stepHeaderCtrl.lessonId) {
      this._router.navigate([item.url + `${this.stepHeaderCtrl.lessonId}`]);
    }
  }



}

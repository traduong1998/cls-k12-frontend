import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxFilterBottomComponent } from './box-filter-bottom.component';

describe('BoxFilterBottomComponent', () => {
  let component: BoxFilterBottomComponent;
  let fixture: ComponentFixture<BoxFilterBottomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoxFilterBottomComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoxFilterBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

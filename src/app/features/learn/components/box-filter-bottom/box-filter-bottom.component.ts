import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/PaginatorResponse';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: Lesson[] = [{
  "id": 1,
  "name": "Bài giảng 1",
  "userName": "Veribet",
  "createdDate": "2/11/2021"
}, {
  "id": 2,
  "name": "Bài giảng 1",
  "userName": "Greenlam",
  "createdDate": "6/2/2020"
}, {
  "id": 3,
  "name": "Bài giảng 1",
  "userName": "Gembucket",
  "createdDate": "9/24/2020"
}, {
  "id": 4,
  "name": "Bài giảng 1",
  "userName": "Zoolab",
  "createdDate": "3/2/2021"
}, {
  "id": 5,
  "name": "Bài giảng 1",
  "userName": "Matsoft",
  "createdDate": "6/21/2020"
}, {
  "id": 6,
  "name": "Bài giảng 1",
  "userName": "Bytecard",
  "createdDate": "7/23/2020"
}, {
  "id": 7,
  "name": "Bài giảng 1",
  "userName": "Sub-Ex",
  "createdDate": "2/6/2021"
}, {
  "id": 8,
  "name": "Bài giảng 1",
  "userName": "Aerified",
  "createdDate": "5/4/2020"
}, {
  "id": 9,
  "name": "Bài giảng 1",
  "userName": "Flexidy",
  "createdDate": "11/17/2020"
}, {
  "id": 10,
  "name": "Bài giảng 2",
  "userName": "Zathin",
  "createdDate": "3/28/2021"
}, {
  "id": 11,
  "name": "Bài giảng 2",
  "userName": "Zathin",
  "createdDate": "12/31/2020"
}, {
  "id": 12,
  "name": "Bài giảng 2",
  "userName": "Tin",
  "createdDate": "9/12/2020"
}, {
  "id": 13,
  "name": "Bài giảng 1",
  "userName": "Redhold",
  "createdDate": "10/21/2020"
}, {
  "id": 14,
  "name": "Bài giảng 1",
  "userName": "Tres-Zap",
  "createdDate": "7/4/2020"
}, {
  "id": 15,
  "name": "Bài giảng 1",
  "userName": "Prodder",
  "createdDate": "6/5/2020"
}, {
  "id": 16,
  "name": "Bài giảng 1",
  "userName": "Stim",
  "createdDate": "6/5/2020"
}, {
  "id": 17,
  "name": "Bài giảng 1",
  "userName": "Flexidy",
  "createdDate": "11/6/2020"
}, {
  "id": 18,
  "name": "Bài giảng 1",
  "userName": "Biodex",
  "createdDate": "3/2/2021"
}, {
  "id": 19,
  "name": "Bài giảng 2",
  "userName": "Andalax",
  "createdDate": "3/29/2021"
}, {
  "id": 20,
  "name": "Bài giảng 1",
  "userName": "Pannier",
  "createdDate": "6/25/2020"
}, {
  "id": 21,
  "name": "Bài giảng 2",
  "userName": "Sub-Ex",
  "createdDate": "5/16/2020"
}, {
  "id": 22,
  "name": "Bài giảng 2",
  "userName": "Y-find",
  "createdDate": "10/20/2020"
}, {
  "id": 23,
  "name": "Bài giảng 1",
  "userName": "Temp",
  "createdDate": "7/31/2020"
}, {
  "id": 24,
  "name": "Bài giảng 2",
  "userName": "Vagram",
  "createdDate": "11/25/2020"
}, {
  "id": 25,
  "name": "Bài giảng 2",
  "userName": "Tin",
  "createdDate": "1/4/2021"
}, {
  "id": 26,
  "name": "Bài giảng 1",
  "userName": "Bamity",
  "createdDate": "9/9/2020"
}, {
  "id": 27,
  "name": "Bài giảng 2",
  "userName": "Stim",
  "createdDate": "5/24/2020"
}, {
  "id": 28,
  "name": "Bài giảng 1",
  "userName": "Voltsillam",
  "createdDate": "9/6/2020"
}, {
  "id": 29,
  "name": "Bài giảng 1",
  "userName": "Asoka",
  "createdDate": "6/4/2020"
}, {
  "id": 30,
  "name": "Bài giảng 2",
  "userName": "Tin",
  "createdDate": "7/11/2020"
}];



@Component({
  selector: 'learn-box-filter-bottom',
  templateUrl: './box-filter-bottom.component.html',
  styleUrls: ['./box-filter-bottom.component.scss']
})
export class BoxFilterBottomComponent implements OnInit {

  lessonEndpoint: LessonEndpoint;
  paginatorResponse: PaginatorResponse<Lesson>;
  lessonDataSource: Lesson[];

  constructor(private _router: Router, private _snackBar: MatSnackBar) {
    this.lessonEndpoint = new LessonEndpoint();
    this.lessonEndpoint.getLessonFilter(null).then(res => {
      if (res) {
        this.paginatorResponse = res;
        this.lessonDataSource = res.items;
        console.log("🚀 ~ file: box-filter-bottom.component.ts ~ line 187 ~ BoxFilterBottomComponent ~ this.lessonEndpoint.getLessonFilter ~ this.lessonDataSource", this.lessonDataSource)
      }
    })
      .catch(() => {
        this.onShowSnar(" Có lỗi hệ thống xảy ra, hãy liên hệ với nhà phát triển ");
      })

  }

  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol', 'action'];
  dataSource = ELEMENT_DATA;


  ngOnInit(): void {
  }

  protected onShowSnar(message: string) {
    this._snackBar.open(message, 'Ok', {
      duration: 3000,
      panelClass: ['green-snackbar'],
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  public addLesson() {
    console.log("here");
    this._router.navigate(['/dashboard/learn/create-lesson']);
    console.log("🚀 ~ file: box-filter-bottom.component.ts ~ line 46 ~ BoxFilterBottomComponent ~ addLesson ~ _router", this._router)

  }

  registerLesson(id: number): void {
    alert(`view đang kí duyệt bài giảng ! vói id bài giảng ${id}`);
  }

  editLesson(id: number): void {
    this._router.navigate([`/dashboard/learn/edit-lesson/${id}`])
  }

  copyLesson(id: number): void {
    alert(`view copu lesson ! vói id bài giảng ${id}`);
  }

  deleteLesson(id: number): void {
    alert(`view delete lesson ! vói id bài giảng ${id}`);
  }
}

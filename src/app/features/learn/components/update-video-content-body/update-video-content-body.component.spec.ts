import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateVideoContentBodyComponent } from './update-video-content-body.component';

describe('UpdateVideoContentBodyComponent', () => {
  let component: UpdateVideoContentBodyComponent;
  let fixture: ComponentFixture<UpdateVideoContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateVideoContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateVideoContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

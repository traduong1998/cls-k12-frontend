import { AnswerTest } from './../../intefaces/questionTest';
import { Action } from 'src/app/shared/modules/question-banks/Interfaces/action';
import { FormatQuestion } from './../../../../../../sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ContentGroupForCombobox } from './../../../../../../sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { Component, ElementRef, HostListener, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { Option } from '../../../../shared/interface/Option';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { DetailContentRequests } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/detailContentRequests';
import { ContentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentEndpoint';
import { DetailContentResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/detailContentResponses';
import { ActivatedRoute, Router } from '@angular/router';
//qsb
import { QuestionFileService } from './../../../../core/services/question-file/question-file.service';
import { Question } from 'sdk/cls-k12-sdk-js/src/services/question/models/Question';
import { Observable, of } from 'rxjs';
import { QuestionTest } from '../../intefaces/questionTest';
import { TestService } from '../../Services/test.service';
import { CKEditor4, CKEditorComponent } from 'ckeditor4-angular';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';

//grc
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { CreateCurriculumRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/createCurriculumRequest';
import { UpdateInfoContentRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/updateInfoContentRequest';
import { ContentGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentGroupEndpoint';
import { LessonStatus } from '../../intefaces/lessonStatus';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import videojs from 'video.js';
import { MimeTypes } from 'src/app/shared/enums/mime-type';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { ConditionForCompletion } from '../../intefaces/conditionForCompletion';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { QuestionConverterEndpoint } from 'sdk/cls-k12-sdk-js/src/services/question-converter/endpoints/question-converter-endpoint';
import { HttpClient } from '@angular/common/http';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { TestEndPoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/testEndpoint';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { ConfigUploadCkeditor } from 'src/app/core/interfaces/app-config';
import { CLS } from 'sdk/cls-k12-sdk-js/src';
import { DialogUpdateScoreComponent } from '../dialog-update-score/dialog-update-score.component';
import { QuestionsOverviewComponent } from '../questions-overview/questions-overview.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { Configuration } from 'src/app/shared/configurations';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { EStatusConvert } from '../../intefaces/eStatusConvert';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';


@Component({
  selector: 'learn-update-video-content-body',
  templateUrl: './update-video-content-body.component.html',
  styleUrls: ['./update-video-content-body.component.scss']
})
export class UpdateVideoContentBodyComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';
  @Input() contentId: number;
  @Input() detailContentResponses: DetailContentResponses;
  @Input() lessonStatus: string;
  @Input() lessonId: number;
  @Input() lessonName: string;
  toolTip: string = Configuration.MESSAGE_INFOR_VIDEO;
  progressData: Observable<any>;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }

  private FILE_LIMIT: number = 500
  numbers: 0;
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>
  group_content: ContentGroupForCombobox[] = [];
  chooseGroupContent: number;
  nameFileVideo: string = "";
  linkYoutubeVideo = "";
  public linkLoadVideo = ""
  linkFileVideo;
  showYoutubeVideo = false;
  statusVideo: 'INP' | 'COM' | 'NON' | 'FAI';
  showFileVideo = false;
  fileVideoId: string = '';
  videoContent: File;
  videoSource: number;
  contentGroupId: number;
  nameContent: string;
  conditionsForCompletion: string;
  minutes: number = 0;
  seconds: number = 0;
  contentCkEditor: string;
  configCkeditor = {
  };
  detailContentRequests: DetailContentRequests;
  contentEndpoint: ContentEndpoint;
  contentGroupEndpoint: ContentGroupEndpoint;
  detailContent: DetailContentRequests;
  fileServer: FileServerEndpoint;

  //qsb
  createCurriculumRequest: CreateCurriculumRequest;
  updateInfoContentRequest: UpdateInfoContentRequest;
  allComplete: boolean = false;
  showListQuestion = false;
  question: Question = null;
  displayedColumns: string[] = ['check', 'content', 'delete'];
  isReady: boolean = false;
  keyWord: string = "";
  nameContentControl = new FormControl('', [Validators.required, Validators.maxLength(255)]);
  minutesControl = new FormControl('', [Validators.required, Validators.min(0)]);
  secondsControl = new FormControl('', [Validators.required, Validators.min(0)]);
  isLoadDetailContent: boolean = false;
  currentQuestion: QuestionTest;
  questions: QuestionTest[];
  questionConverterEndpoint: QuestionConverterEndpoint;
  testEndPoint: TestEndPoint;
  childIndex: number;
  questionsShow: QuestionTest[] = [];
  configUpload: ConfigUploadCkeditor;
  totalScore: number;
  currentQuestionId: number;
  //grc
  public groupContentControl: FormControl = new FormControl();
  public groupContentFilterControl: FormControl = new FormControl();
  public filteredGroupContents: ReplaySubject<ContentGroupForCombobox[]> = new ReplaySubject<ContentGroupForCombobox[]>(0);
  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();
  @ViewChild("ck") ck: CKEditorComponent;
  //grc
  @ViewChild("singleSelect") singleSelect: MatSelect;
  @ViewChild("file") file: ElementRef;
  @ViewChild("fileQuestion") fileQuestion: ElementRef;
  playerLearn: videojs.Player;
  videoMimeTypeSupports = [MimeTypes.m3u8, MimeTypes.mp4];
  currentMimeType = 0;
  @ViewChild('videoLearn', { static: false }) videoLearn: ElementRef<HTMLVideoElement>;
  get questionFormat(): typeof FormatQuestion {
    return FormatQuestion;
  }
  get questionLevel(): typeof Level {
    return Level;
  }
  get questionType(): typeof Type {
    return Type;
  }
  get actionEnum(): typeof Action {
    return Action;
  }
  //#region Thiết lập câu hỏi
  isSetTestTime: boolean = false;
  isWriteLog: boolean = true;
  isGetRandomQuestion: boolean = false;
  totalQuestion: number;
  testTimeControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$"),
    Validators.min(5)
  ]);
  allowToReDoControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$")
  ]);
  itemPerPageCtrl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$")
  ])
  //#endregion
  constructor(public dialog: MatDialog,
    private route: ActivatedRoute,
    private questionFilleService: QuestionFileService,
    private testService: TestService,
    private routerr: Router,
    private _learnMessageService: LearnMessageService,
    private authService: AuthService,
    private httpClient: HttpClient,
    private _snackBar: MatSnackBar,
    private _authService: AuthService,
    private configService: AppConfigService,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig
  ) {
    this.statusVideo = 'NON';

    this.contentEndpoint = new ContentEndpoint();
    this.detailContentRequests = new DetailContentRequests;
    this.detailContentResponses = new DetailContentResponses;
    this.detailContent = new DetailContentRequests;
    this.createCurriculumRequest = new CreateCurriculumRequest;
    this.updateInfoContentRequest = new UpdateInfoContentRequest;
    this.contentGroupEndpoint = new ContentGroupEndpoint();
    this.fileServer = new FileServerEndpoint();
    this.questionConverterEndpoint = new QuestionConverterEndpoint();
    this.testEndPoint = new TestEndPoint();
    this.contentCkEditor = this.detailContent.detailsOfContent;
    //ckeditor
    this.configCkeditor = {
      height: 150,
      allowedContent: false,
      EditorType: CKEditor4.EditorType.CLASSIC,
      removeButtons: 'Image,Flash',
    };
    
    this.baseApiUrl = CLS.getConfig().apiBaseUrl;

    let baseUrl = CLS.getConfig().apiBaseUrls.fileServer ?? CLS.getConfig().apiBaseUrl;
    baseUrl += '/api';
    this.configUpload = {
      portalId: configService.getConfig().unit.portalId,
      token: _authService.getToken(),
      uploadUrl: baseUrl + "/file/upload"
    }
    this.testService.questions$.subscribe((questions) => {
      if(this.isReady){
        this.questions = JSON.parse(JSON.stringify(questions));
        this.questionsShow = [...this.questions.filter(x => x.action != Action.Delete)];
        this.questionsShow.map((qst) => {
          if (qst.format == FormatQuestion.group) {
            qst.questions = [...qst.questions.filter(x => x.action != Action.Delete)];
          }
        });
        this.itemPerPageCtrl = new FormControl('', [
          Validators.required,
          Validators.max(this.questionsShow.length)
        ])
        if (this.questionsShow.length > 0) {
          this.itemPerPageCtrl = new FormControl('', [
            Validators.min(1),
            Validators.required,
            Validators.max(this.questionsShow.length)
          ]);
        }
        this.detailContentRequests.testInfo.numberOfQuestionOnThePage = this.detailContentRequests.testInfo.numberOfQuestionOnThePage > this.questionsShow.length ?
        this.questionsShow.length : (this.detailContentRequests.testInfo.numberOfQuestionOnThePage == 0 && this.questionsShow.length > 5) ?
            5 : this.detailContentRequests.testInfo.numberOfQuestionOnThePage > 0 ?
              this.detailContentRequests.testInfo.numberOfQuestionOnThePage : this.questionsShow.length;
        this.itemPerPageCtrl.setValue(this.detailContentRequests.testInfo.numberOfQuestionOnThePage);
      }
    });
  }

  ngOnChanges() {
    this.statusVideo = 'COM';
    this.detailContentRequests = { ...this.detailContentResponses };
    this.contentGroupEndpoint.getListContentGroupForCombobox(this.detailContentResponses.lessonId).then((res) => {
      this.group_content = res;
      this.group_content.unshift({ id: 0, name: "Chọn tiết", order: -1 });
      this.filteredGroupContents.next(this.group_content);
      this.filteredGroupContents.subscribe((res) => {
        this.chooseGroupContent = this.detailContentResponses.contentGroupId;
      });
      if(this.detailContentResponses.conditionsForCompletion != "ANS"){
        this.isReady = true;
      }
    });
    this.groupContentControl.setValue(this.group_content[this.detailContentResponses.contentGroupId - 1]);
    this.chooseGroupContent = this.detailContentResponses.contentGroupId;
    this.nameContent = this.detailContentResponses.name;
    this.contentCkEditor = this.detailContentResponses.description;
    if (this.detailContentResponses.conditionsForCompletion == "INT") {
      this.conditionsForCompletion = "INT",
        this.minutes = Math.floor(this.detailContentResponses.timeToComplete / 60);
      this.seconds = (this.detailContentResponses.timeToComplete % 60);
    }
    else if (this.detailContentResponses.conditionsForCompletion == "ANS") {
      this.conditionsForCompletion = "ANS";
      if (this.detailContentResponses.testId != null) {
        this.showListQuestion = true;
        this.getContentTest(this.contentId, this.lessonId);
      }
    }
    else if (this.detailContentResponses.conditionsForCompletion == "BUT") {
      this.conditionsForCompletion = "BUT";
    }
    if (!this.detailContentResponses.contentGroupId == null) {

    }
    if (this.detailContentResponses.link != null) {

      switch (this.detailContentResponses.fileId) {

        case 1:
          this.showFileVideo = false;
          this.showYoutubeVideo = true;
          this.linkYoutubeVideo = this.detailContentResponses.link;
          this.linkLoadVideo = this.getEmbedLink(this.linkYoutubeVideo);
          this.videoSource = 1;
          break;

        case 2:
          this.linkFileVideo = this.detailContentResponses.link;
          this.videoSource = 2;
          break;

        default:
          this.videoSource = 1;

          this.linkYoutubeVideo = null;
          this.linkLoadVideo = null;

          this.linkFileVideo = null;
          this.showYoutubeVideo = false;
          this.showFileVideo = false;
          break;
      }
    } else {
      this.videoSource = 1;
      this.linkYoutubeVideo = null;
      this.linkFileVideo = null;
      this.showYoutubeVideo = false;
      this.showFileVideo = false;
      this.statusVideo = 'COM';
    }
    this.testService.totalScore$.subscribe((totalScore) => {
      this.totalScore = totalScore;
    })
  }
  ngOnInit(): void {
    this.chooseGroupContent = this.detailContentResponses.contentGroupId;
  }

  ngAfterViewInit(): void {
    this.showFileVideo = false;
    this.statusVideo = "NON";
    videojs.Hls.xhr.beforeRequest = options => {
      options.headers = options.headers || {};
      var token = `Bearer ${this.authService.getToken()}`;
      options.headers["Authorization"] = token;
    };
    this.chooseGroupContent = this.detailContentResponses.contentGroupId;
    if (this.videoSource == 2) {
      this.playerLearn = videojs(this.videoLearn.nativeElement, { autoplay: false, controls: true, errorDisplay: false }, () => {
        if (this.detailContentResponses.link) {
          this.fileVideoId = this.detailContentResponses.linkId;
          this.nameFileVideo = this.detailContentResponses.fileName;
          // let typeVideo = MimeTypes.m3u8;
          // if(this.detailContentResponses.link.includes('.mp4')){
          //   typeVideo = MimeTypes.mp4;
          // }
          this.playerLearn.src({
            src: this.detailContentResponses.link, type: MimeTypes.m3u8
          });

          this.playerLearn.on('loadedmetadata', () => {
            console.log("video loadedmetadata");
            this.statusVideo = 'COM';
          });

          this.playerLearn.on('error', () => {
            if(this.playerLearn.error().code === 4){
              this.currentMimeType++;
              if (this.currentMimeType > this.videoMimeTypeSupports.length) {
                return false;
              }
              this.playerLearn.src({ src: this.detailContentResponses.link, type: this.videoMimeTypeSupports[this.currentMimeType] });
            }
            this.fileVideoId && this.fileServer.getStatusFile(this.fileVideoId).then(res => {
              switch (res) {
                case EStatusConvert.Waiting:
                case EStatusConvert.Inprogress:
                  this.statusVideo = 'INP';
                  break;

                case EStatusConvert.Failed:
                case EStatusConvert.Skip:
                  this.statusVideo = 'FAI';
                  break;

                default:
                  break;
              }
            })
            console.log('error', this.statusVideo);
            // this.playerLearn.dispose();
            console.log(this.playerLearn.error().code)
            // if(this.playerLearn.error().code === 4){
            //   this.playerLearn.src({ src: this.detailContentResponses.link, type: MimeTypes.mp4 });
            // }
          });
        }
      });
      this.showFileVideo = true;
      console.log(this.statusVideo);
    }
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
    this.testService.clear();
  }

  changControlInput(): void {
    this.isSetTestTime ? this.testTimeControl.enable() : this.testTimeControl.disable();
    this.detailContentRequests.testInfo?.allowToRedo ? this.allowToReDoControl.enable() : this.allowToReDoControl.disable();
    if (!this.isSetTestTime) {
      this.testTimeControl.reset();
      this.detailContentRequests.testInfo.testTime = null;
    } else {
      this.testTimeControl.markAsTouched();
    }
    if (!this.detailContentRequests.testInfo?.allowToRedo) {
      this.allowToReDoControl.reset();
      this.detailContentRequests.testInfo.theMaximumNumberToRedo = null;
    }
    else {
      this.allowToReDoControl.markAsTouched();
    }
    //this.isGetRandomQuestion ? this.questionsRandomCtrl.enable() : this.questionsRandomCtrl.disable();
  }
  onGroupContentChanged(grc: Option) {
    this.contentGroupId = grc.id;
    console.log('tttt', grc.name);
  }
  filterBanks() {
    if (!this.group_content) {
      return;
    }
    // get the search keyword
    let search = this.keyWord;
    if (!search) {
      this.filteredGroupContents.next(this.group_content.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredGroupContents.next(
      this.group_content.filter(bank => bank.name.toLowerCase().indexOf(search) > -1)
    );
  }
  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
  }

  addYoutube(link: string) {
    this.showYoutubeVideo = false;
    if (this.validURL(link)) {

      this.linkYoutubeVideo = link;
      this.linkLoadVideo = this.getEmbedLink(link);
      this.showYoutubeVideo = false;
    }
    else {
      this._learnMessageService.failToast("Không đúng định dạng URL, mời bạn nhập lại");
      this.linkYoutubeVideo = '';
      this.linkLoadVideo = '';
      this.showYoutubeVideo = false;
    }
  }
  addFromFile() {
    this.file.nativeElement.click();
  }

  //get file
  uploadFileVideo(e) {
    const file = e.target.files[0];
    if (!file) {
      return;
    }

    // Kiểm tra limit file
    let confirmLimit = Configuration.getLimitFileMessage(file);
    if (!confirmLimit.status) {
      this._learnMessageService.failToast(confirmLimit.message);
      return;
    }

    var type = file.name.substring(file.name.lastIndexOf(".") + 1).toLowerCase();
    if (['mp4', 'webm', 'ogg', 'ogv', 'avi', 'mpeg', 'mpg', 'mov', 'wmv', '3gp', 'flv', 'rmvb', 'm4v', 'mkv'].includes(type)) {
      this.showFileVideo = false;
      this.videoContent = file;
      Promise.resolve()
        .then(() => {
          console.log(`start process`);
          this.openProgressDialog(null);
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.fileServer.uploadFile({
            file: file,
            isPrivate: true,
            moduleName: ServiceName.LEARN
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          this.fileVideoId && this.fileServer.skipFile(this.fileVideoId).then(resFileSkip => {
          });
          return res;
        })
        .then((res) => {

          this.fileVideoId = res.fileId;
          this.nameFileVideo = res?.name;
          this.linkFileVideo = res?.link
          this.showFileVideo = true;

          console.log(`Hoàn thành`);
          this.statusVideo = 'INP';
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
          if (this.videoSource == 2) {
            if (this.detailContentResponses.link) {
              this.showFileVideo = true;
              this.playerLearn = videojs(this.videoLearn.nativeElement, { autoplay: false, controls: true }, () => {
                this.playerLearn.src({ src: this.detailContentResponses.link, type: MimeTypes.m3u8 });
              });
            }
          }
        });
    }
    else {
      this._learnMessageService.failToast("Không đúng định dạng URL, mời bạn nhập lại");
      this.showFileVideo = false;
    }
    console.log('Change input file', e)
  }

  onCancelClick(): void {
    this.getRequest();
    if (this.detailContentRequests.name != (this.detailContentResponses.name ? this.detailContentResponses.name : '')
      || this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId
      || this.detailContentRequests.description != (this.detailContentResponses.description ? this.detailContentResponses.description : '')
      || this.detailContentRequests.linkId != (this.detailContentResponses.linkId ? this.detailContentResponses.linkId : null)
    ) {
      this.openConfirmDialog(Configuration.MESSAGE_EXIT_CONFIRM).subscribe((confirm) => {
        if (!confirm) {
          return;
        } else this.routerr.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);
      })
    } else this.routerr.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);

  }
  checkInterval(): boolean {
    if (this.conditionsForCompletion == "INT") {
      if (this.detailContentResponses.timeToComplete == this.minutes * 60 + this.seconds) {
        return true;
      }
    }
    return false;
  }
  onSaveClick(): void {
    this.detailContentRequests.lessonId = this.detailContentResponses.lessonId;
    this.detailContentRequests.id = this.detailContentResponses.id;
    if (isNullOrWhiteSpace(this.nameContent) || isNullOrEmpty(this.nameContent)) {
      this._learnMessageService.failToast("Chưa nhập tên nội dung");
      return;
    }
    else {
      this.detailContentRequests.name = this.nameContent;
    }
    this.detailContentRequests.typesOfContent = "VID";

    if (this.conditionsForCompletion == null) {

      this._learnMessageService.failToast("Chưa chọn điều kiện hoàn thành nội dung");
      return;
    }
    else {
      this.detailContentRequests.conditionsForCompletion = this.conditionsForCompletion;
      if (this.conditionsForCompletion == ConditionForCompletion.interval) {
        if (this.minutes < 0 || this.seconds < 0 || this.minutes == null || this.seconds == null) {
          this._learnMessageService.failToast("Thời gian hoàn thành không hợp lệ");

          return;
        }
        else {
          this.detailContentRequests.timeToComplete = this.minutes * 60 + this.seconds;
        }
      } else if (this.conditionsForCompletion == ConditionForCompletion.answerQuestion) {
        if(this.itemPerPageCtrl.invalid || this.testTimeControl.invalid || this.allowToReDoControl.invalid){
          this._learnMessageService.failToast("Kiểm tra dữ liệu thiết lập câu hỏi");
          return;
        }
        this.detailContentRequests.listQuestion = this.questions;
        this.detailContentRequests.scoreAfterFinish = this.totalScore;
        this.detailContentRequests.totalScore = this.totalScore;
        this.detailContentRequests.totalEssayScore = 0;
        [...this.questionsShow].filter(x=>x.type == 'ESS').forEach(element => {
          this.detailContentRequests.totalEssayScore+= element.score;
        });
        this.detailContentRequests.totalQuizScore = this.totalScore - this.detailContentRequests.totalEssayScore;
      }
    }

    if (this.chooseGroupContent > 0) {
      this.detailContentRequests.contentGroupId = this.chooseGroupContent;
    }
    else {
      this.detailContentRequests.contentGroupId = null;
    }
    debugger;
    if ((this.videoSource == 1)) {
      this.detailContentRequests.fileId = 1;
      this.detailContentRequests.link = this.linkYoutubeVideo;
      this.saveVideo();
    }
    else if (this.videoSource == 2) {
      this.detailContentRequests.linkId = this.fileVideoId
      this.detailContentRequests.fileName = this.nameFileVideo
      this.detailContentRequests.fileId = 2;
      this.detailContentRequests.link = this.linkFileVideo

      this.saveVideo()
    }
  }

  saveVideo() {
    this.detailContentRequests.description = this.contentCkEditor;
    if (JSON.stringify(this.detailContentRequests) === JSON.stringify(this.detailContentResponses)) {
      this.routerr.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);
      return;
    }

    this.spinner.show();

    if (this.detailContentRequests.fileId != this.detailContentResponses.fileId ||
      this.detailContentRequests.link != this.detailContentResponses.link ||
      this.detailContentRequests.detailsOfContent != this.detailContentResponses.detailsOfContent ||
      this.detailContentRequests.description != this.detailContentResponses.description || this.questions.length > 0
    ) {
      if (
        this.detailContentRequests.name != this.detailContentResponses.name ||
        this.detailContentRequests.conditionsForCompletion != this.detailContentResponses.conditionsForCompletion ||
        this.detailContentRequests.timeToComplete != this.detailContentResponses.timeToComplete ||
        this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId || this.questions.length > 0) {
        if (this.lessonStatus == LessonStatus.Publish) {
          this.contentEndpoint.updateContentAfterPublish(this.detailContentRequests).then((res) => {
            if (res) {
              this.spinner.hide();
              this._learnMessageService.successToast("Lưu thành công");

              this.detailContentResponses = { ...this.detailContentRequests };
              setTimeout(() => {
                this.routerr.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);
              }, 2000)
            } else {
              this.spinner.hide();
              this._learnMessageService.failToast("Có lỗi xảy ra");
            }
          }).catch(err => {
            this.spinner.hide();
            console.error('catch', err);
            this._learnMessageService.failToast("Có lỗi xảy ra");
          });
        }
        else {
          this.contentEndpoint.updateContentBeforPublish(this.detailContentRequests).then((res) => {
            if (res) {
              this.spinner.hide();
              this._learnMessageService.successToast("Lưu thành công");

              this.detailContentResponses = { ...this.detailContentRequests };
              setTimeout(() => {
                this.routerr.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);
              }, 2000)
            } else {
              this.spinner.hide();
              this._learnMessageService.failToast("Có lỗi xảy ra");
            }

          }).catch(err => {
            this.spinner.hide();
            console.error('catch', err);
            this._learnMessageService.failToast("Có lỗi xảy ra");
          });
        }
      }
      else {
        this.createCurriculumRequest.id = this.detailContentResponses.id;
        this.createCurriculumRequest.fileId = this.detailContentRequests.fileId;
        this.createCurriculumRequest.link = this.detailContentRequests.link;
        this.createCurriculumRequest.linkId = this.detailContentRequests.linkId;
        this.createCurriculumRequest.fileName = this.detailContentRequests.fileName;
        this.createCurriculumRequest.detailsOfContent = this.detailContentRequests.detailsOfContent;
        this.createCurriculumRequest.description = this.detailContentRequests.description;
        this.contentEndpoint.createCurriculum(this.detailContentRequests.lessonId, this.createCurriculumRequest).then((res) => {
          if (res) {
            this.spinner.hide();
            this._learnMessageService.successToast("Lưu thành công");

            this.detailContentResponses = { ...this.detailContentRequests };
            setTimeout(() => {
              this.routerr.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);
            }, 2000)
          } else {
            this.spinner.hide();
            this._learnMessageService.failToast("Có lỗi xảy ra");
          }
        }).catch(err => {
          this.spinner.hide();
          console.error('catch', err);
          this._learnMessageService.failToast("Có lỗi xảy ra");
        });
      }

    }
    else {
      if (this.detailContentRequests.name != this.detailContentResponses.name ||
        this.detailContentRequests.conditionsForCompletion != this.detailContentResponses.conditionsForCompletion ||
        this.detailContentRequests.timeToComplete != this.detailContentResponses.timeToComplete ||
        this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId) {
        this.updateInfoContentRequest.name = this.detailContentRequests.name;
        this.updateInfoContentRequest.conditionsForCompletion = this.detailContentRequests.conditionsForCompletion;
        this.updateInfoContentRequest.timeToComplete = this.detailContentRequests.timeToComplete;
        this.updateInfoContentRequest.id = this.detailContentResponses.id;
        this.updateInfoContentRequest.contentGroupId = this.detailContentRequests.contentGroupId;
        this.contentEndpoint.updateInfoContent(this.detailContentRequests.lessonId, this.updateInfoContentRequest).then((res) => {
          if (res) {
            this.spinner.hide();
            this._learnMessageService.successToast("Lưu thành công");

            this.detailContentResponses = { ...this.detailContentRequests };
            setTimeout(() => {
              this.routerr.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);
            }, 2000)
          } else {
            this.spinner.hide();
            this._learnMessageService.failToast("Có lỗi xảy ra");
          }
        }).catch(err => {
          this.spinner.hide();
          console.error('catch', err);
          this._learnMessageService.failToast("Có lỗi xảy ra");
        });
      }
      else {
        this.spinner.hide();
        this._learnMessageService.successToast("Lưu thành công");
        this.routerr.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);
      }
    }
  }

  someComplete(): boolean {
    return this.testService.someChecked(this.allComplete);
  }

  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.testService.checkAll(completed);
  }

  updateAllComplete(isChecked: boolean, question: QuestionTest) {
    this.allComplete = this.testService.isCheckAll();
    if (question.questionGroupId == undefined || question.questionGroupId == null && question.format == FormatQuestion.group) {
      this.testService.checkGroupQuestion(question, isChecked);
    } else {
      this.testService.checkQuestion(question, isChecked);
    }
  }
  changeComboo(event) {
    if (this.videoSource == 2 && this.linkFileVideo == null) {
      this.statusVideo = "NON";
    }
  }
  deleteQuestion(question: QuestionTest): void {
    this.testService.deleteQuestion(question);
    if (this.question != undefined && this.question != null) {
      if (question.id == this.question.id) {
        this.currentQuestionId = 0;
        this.question = undefined;
      }
    }
  }

  deleteSelected(): void {
    if (!this.someComplete() && !this.allComplete) {
      this._learnMessageService.failToast("Không có câu hỏi nào được chọn!");
      return;
    }
    else {
      this.openConfirmDialog("Bạn có chắc chắn muốn xóa các câu hỏi đã chọn?").subscribe((isConfirmed) => {
        if (isConfirmed) {
          if (this.question != undefined && this.question != null) {
            let currentQuestion = this.questions.find(x => x.id == this.question.id);
            if (currentQuestion.isChecked) {
              this.currentQuestionId = 0;
              this.question = undefined;
            }
          }
          this.testService.deleteSelected();
        }
      });
    }
  }

  addQuestionFromFile() {
    this.fileQuestion.nativeElement.click();
  }

  //get file
  handle(event) {
    const file = event.target.files;
    if (file) {
      Promise.resolve()
        .then(() => {
          this.openProgressDialog(null);
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.questionConverterEndpoint.uploadTestFiles(file, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              var subscription = this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          if (res) {
            let questions: QuestionTest[] = [];
            let result = res[0];
            let id = -1;
            console.log(res);
            result.questions.map((question) => {
              if (question.format == FormatQuestion.single) {
                questions.push({
                  id: id,
                  content: question.content,
                  level: question.level,
                  format: question.format,
                  questionGroupId: null,
                  isChecked: false,
                  score: question.score,
                  type: question.type,
                  unitId: question.unitId,
                  answers: question.answers,
                  action: Action.Add,
                  questions: []
                });
                id--;
              } else {
                let parentQuestion: QuestionTest = {
                  id: id,
                  content: question.content,
                  level: question.level,
                  format: question.format,
                  questionGroupId: null,
                  isChecked: false,
                  score: question.score,
                  type: question.type,
                  unitId: question.unitId,
                  answers: question.answers,
                  action: Action.Add,
                  questions: []
                };
                id--;
                let subQuestions: QuestionTest[] = [];
                question.questions.map((child) => {
                  subQuestions.push({
                    id: id,
                    content: child.content,
                    level: child.level,
                    format: child.format,
                    questionGroupId: parentQuestion.id,
                    isChecked: false,
                    score: child.score,
                    type: child.type,
                    unitId: child.unitId,
                    answers: child.answers,
                    action: Action.Add,
                    questions: []
                  });
                  id--;
                });
                parentQuestion.questions = subQuestions;
                questions.push(parentQuestion);
              }
            })
            this.testService.addListQuestion(questions);
            this.showListQuestion = true;
            this.fileQuestion.nativeElement.value = null;
          }
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err);
          this._learnMessageService.warToast(err.response.data.errorDetail);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }

  updateQuestion(qst: QuestionTest) {
    if (qst) {
      this._learnMessageService.successToast("Cập nhật thành công");
      this.testService.updateQuestion(qst, LessonStatus.Publish);
      this.currentQuestion = qst;
    } else {
      this._learnMessageService.failToast("Có lỗi xảy ra");
    }
  }

  chooseQuestion(questionSelected: QuestionTest, index?: number): void {
    if (index != undefined && this.childIndex == index + 1) {
      return
    }
    if (questionSelected.questionGroupId != null && questionSelected.questionGroupId != undefined) {
      let parent = this.questions.find(x => x.id == questionSelected.questionGroupId);
      this.question = JSON.parse(JSON.stringify(parent));
      this.currentQuestionId = parent.id;
    }
    else {
      this.question = JSON.parse(JSON.stringify(questionSelected));
      this.currentQuestionId = questionSelected.id;
    }
    this.isLoadDetailContent = true;
    if (index != null && index != undefined) {
      this.childIndex = index + 1;
    }
    else if (questionSelected.questionGroupId == null || questionSelected.questionGroupId == undefined) {
      this.childIndex = 1;
    }
  }
  previewTest(): void {
    //this._learnMessageService.warToast("Tính năng đang phát triển!");
    this.dialog.open(QuestionsOverviewComponent, {
      data: this.testService.beauty(this.questions),
      width: '80%',
      height: '95vh'
    })
  }
  getContentTest(contentId: number, lessonId: number): void {
    this.testEndPoint.getContentTest(contentId, lessonId).then((res) => {
      this.detailContentResponses.testInfo = {
        viewCorrectResultAfterSubmittingTheTest: res.viewCorrectResultAfterSubmittingTheTest,
        numberOfQuestionOnThePage : res.numberOfQuestionOnThePage,
        testTime: res.testTime,
        allowToRedo: res.allowToRedo,
        theMaximumNumberToRedo: res.theMaximumNumberToRedo,
        isRandomizeQuestions: res.isRandomizeQuestions
      }
      this.detailContentRequests.testInfo = this.detailContentResponses.testInfo; 
      //Check và set các tham số gán ngModel trên view
      if (this.detailContentRequests.testInfo.testTime > 0) {
        this.isSetTestTime = true;
      }
      this.changControlInput();
      this.GetListQuestionFromUrl(res.urlJsonTest).subscribe((res) => {
        console.log("questions");
        //Thêm list câu hỏi vào testService
        let result: QuestionTest[] = res["questions"];
        let listQuestions: QuestionTest[] = [];
        result.forEach(question => {

          let qst = new QuestionTest(question);
          if (question.format == FormatQuestion.group) {
            qst.questions = [];
            question.questions.forEach((child) => {
              let childQuestion = new QuestionTest(child);
              childQuestion.answers = [];
              child.answers.forEach(ans => {
                let answer: AnswerTest = {
                  id: ans.id,
                  content: ans.content,
                  questionId: ans.questionId,
                  trueAnswer: ans.trueAnswer,
                  groupOfFillBlank2: ans.groupOfFillBlank2,
                  position: ans.position,
                  isShuffler: ans.isShuffler ? ans.isShuffler : false,
                  action: null
                };
                childQuestion.answers.push(answer);
              });
              qst.questions.push(childQuestion);
            });
          } else {
            qst.answers = [];
            question.answers.forEach((ans) => {
              let answer: AnswerTest = {
                id: ans.id,
                content: ans.content,
                questionId: ans.questionId,
                trueAnswer: ans.trueAnswer,
                groupOfFillBlank2: ans.groupOfFillBlank2,
                position: ans.position,
                isShuffler: ans.isShuffler ? ans.isShuffler : false,
                action: null
              };
              qst.answers.push(answer);
            });
          }
          listQuestions.push(qst);
        });
        this.detailContentResponses.listQuestion = listQuestions;
        // Dữ liệu đã sẵn sàng nha
        this.isReady = true;
        this.testService.addListQuestion(listQuestions);
      });

    })
  }
  public GetListQuestionFromUrl(url: string): Observable<any> {
    return this.httpClient.get(url);
  }

  public onAddYoutubeHandleKeyup(val: any) {
    this.showYoutubeVideo = false;
    if (!val) {
      this.linkYoutubeVideo = this.detailContentResponses.link;
      this.showYoutubeVideo = true;
    } else {
      let linkEmbed = this.getEmbedLink(val);
      if (linkEmbed) {
        this.linkYoutubeVideo = val;
        this.linkLoadVideo = linkEmbed;
        this.showYoutubeVideo = true;
      } else {
        this.showYoutubeVideo = false;
      }
    }
  }

  private getEmbedLink(link: string) {
    let linkEmbed = '';
    let linkYouTube = link.includes("embed");
    if (linkYouTube) {
      linkEmbed = link;
    } else {
      linkEmbed = link.split('v=')[1];
      if (linkEmbed) {
        var ampersandPosition = linkEmbed.indexOf('&');
        if (ampersandPosition != -1) {
          linkEmbed = linkEmbed.substring(0, ampersandPosition);
        }
        linkEmbed = `https://www.youtube.com/embed/${linkEmbed}`;
      } else {
        return '';
      }
    }

    return linkEmbed;
  }
  /*
   * open progress dialog 
   */
  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.numbers }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  openUpdateScoreDialog(): void {
    if (!this.someComplete() && !this.allComplete) {
      this._learnMessageService.failToast("Không có câu hỏi nào được chọn!");
      return;
    }
    const dialogRef = this.dialog.open(DialogUpdateScoreComponent, {
      width: '377px',
      height: '234px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.setScoreQuestions(result);
      }
    });
  }
  setScoreQuestions(score: number): void {
    this.testService.updateScoreQuestions(score);
  }

  updateScore(id: number, score: number) {
    if (score == null) {
      this._learnMessageService.failToast("Điểm câu hỏi không được để trống!");
    }
    this.testService.updateScore(id, score);
  }
  updateScoreChild(parentId: number, childId: number, score: number) {
    if (score == null) {
      this._learnMessageService.failToast("Điểm câu hỏi không được để trống!");
    }
    this.testService.updateScoreChild(parentId, childId, score);
  }

  private openConfirmDialog(value: string): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: {
        message: `${value}`,
        buttonText: {
          ok: 'Có',
          cancel: 'Không'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }
  @HostListener('window:beforeunload', ['$event'])
  unloadHandler($event) {
    this.getRequest();
    if (this.detailContentRequests.name != (this.detailContentResponses.name ? this.detailContentResponses.name : '')
      || this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId
      || this.detailContentRequests.description != (this.detailContentResponses.description ? this.detailContentResponses.description : '')
      || this.detailContentRequests.linkId != (this.detailContentResponses.linkId ? this.detailContentResponses.linkId : null)
    ) return false
  }

  private getRequest() {
    this.detailContentRequests.name = this.nameContent ? this.nameContent : '';
    this.detailContentRequests.contentGroupId = this.chooseGroupContent ?? null;
    this.detailContentRequests.linkId = this.fileVideoId;
    this.detailContentRequests.description = this.contentCkEditor ? this.contentCkEditor : '';
  }

  public getLinkTemplate(cases: number) {
    if (cases == 1) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-mau.docx`;
    }
    else if (cases == 2) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/huong-dan-su-dung-tep-tin-mau.docx`;
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabContentLessonFooterComponent } from './tab-content-lesson-footer.component';

describe('TabContentLessonFooterComponent', () => {
  let component: TabContentLessonFooterComponent;
  let fixture: ComponentFixture<TabContentLessonFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabContentLessonFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabContentLessonFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

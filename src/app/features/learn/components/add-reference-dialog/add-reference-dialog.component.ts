import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, of, Subject } from 'rxjs';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';
import { References } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/references';
import { ReferenceRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/referenceRequest';
import { Configuration } from 'src/app/shared/configurations';
import { isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { EStatusConvert } from '../../intefaces/eStatusConvert';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';

@Component({
  selector: 'learn-add-reference-dialog',
  templateUrl: './add-reference-dialog.component.html',
  styleUrls: ['./add-reference-dialog.component.scss']
})
export class AddReferenceDialogComponent implements OnInit {
  toolTip: string = Configuration.MESSAGE_INFOR_REFERENCE;
  lessonId: number;
  reference: References;
  linkPreview: string = null;
  nameReference: string = '';
  fileServer: FileServerEndpoint;
  referenceRequest: ReferenceRequest;
  referenceRequestClone: ReferenceRequest;
  progressData: Observable<any>;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }
  numbers: 100;
  documentStatus: string = '';
  priFileId: string = '';
  dialogProgressRef: MatDialogRef<ProgressDialogComponent>

  constructor(
    public dialogRef: MatDialogRef<AddReferenceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _learnMessageService: LearnMessageService,
    private dialog: MatDialog) {
    // this.data.reference;
    this.fileServer = new FileServerEndpoint();
    this.lessonId = data;
  }

  onNoClick(): void {
    if ((this.priFileId != (this.referenceRequest.fileId ?? '')) || (this.nameReference != this.referenceRequest.name)) {
      this.openConfirmDialog(Configuration.MESSAGE_EXIT_CONFIRM).subscribe((confirm) => {
        if (!confirm) {
          return;
        } else this.dialogRef.close();
      })
    } else this.dialogRef.close();
  }

  ngOnInit(): void {
    console.log(this.data);
    this.referenceRequest = { name: '', fileId: '', fileName: '', lessonId: this.lessonId, link: null };
    this.referenceRequestClone = { ...this.referenceRequest };
  }

  files: File[] = [];
  fileData: File;
  val: any;
  onSelect(event) {
    this.files = [];
    this.files.push(...event.addedFiles);
    this.fileData = event.addedFiles;
    this.val = event;
    if (!this.files[0]) {
      return;
    }

    var type = this.files[0].name.substring(this.files[0].name.lastIndexOf(".") + 1).toLowerCase();
    if (["ppt", "pptx", "doc", "docx", "xls", "xlsx", "pdf"].includes(type)) {
      Promise.resolve()
        .then(() => {
          this.openProgressDialog(null);
          return this.fileServer.uploadFile({
            file: this.files[0],
            isPrivate: false,
            moduleName: ServiceName.LEARN
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: this.files[0].name, catch: false };
                }
              });
            }
          });
        })
        .then((res) => {
          this.priFileId && this.fileServer.skipFile(this.priFileId);
          this.checkLink(type, res.link)
          return res;
        })
        .then((res) => {
          console.log(`Hoàn thành`);

          this.linkPreview = res.link;
          this.priFileId = res.fileId
          this.referenceRequest = { name: this.nameReference, fileName: res.name, link: res.link, fileId: res.fileId, lessonId: this.lessonId }

          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: this.files[0].name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close()
          }, 2000);
        })
        .catch((err) => {
          this.files = [];

          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: this.fileData.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close()
          }, 2000);
        });
    }
  }
  onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
    this.files = [];
  }

  onAddReferenceHandleClick() {
    if (this.val == null) {
      this._learnMessageService.failToast("Bạn cần thêm tệp tài liệu");
      return;
    }
    if (isNullOrWhiteSpace(this.nameReference)) {
      this._learnMessageService.failToast("Bạn cần nhập tên tài liệu tham khảo");
      return;
    }

    let result = this.referenceRequest;
    result.name = this.nameReference;
    this.dialogRef.close(result);
  }

  /*
  * open progress dialog 
  */
  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.numbers }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  private checkLink(type: string, link: string) {
    this.documentStatus = '';
    // because mp3 it is not convert;
    if (['pdf'].includes(type) || type == '') {
      var jqxhr = $.get(link, () => {
      }).done(() => {
        this.documentStatus = 'COM';
      })
        .fail(() => {

          this.priFileId && this.fileServer.getStatusFile(this.priFileId).then(res => {
            switch (res) {
              case EStatusConvert.Waiting:
              case EStatusConvert.Inprogress:
                this.documentStatus = 'INP';
                break;

              case EStatusConvert.Failed:
              case EStatusConvert.Skip:
                this.documentStatus = 'FAI';
                break;
              case EStatusConvert.Completed:
                this.documentStatus = 'COM';
                break;
              default:
                break;
            }
          })
        })
    } else {
      this.documentStatus = 'INP';
    }
  }

  private openConfirmDialog(value: string): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: {
        message: `${value}`,
        buttonText: {
          ok: 'Có',
          cancel: 'Không'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }
  @HostListener('window:beforeunload', ['$event'])
  unloadHandler($event) {
    if ((this.priFileId != (this.referenceRequest.fileId ?? '')) || (this.nameReference != this.referenceRequest.name)) {
      return false
    }
  }
}

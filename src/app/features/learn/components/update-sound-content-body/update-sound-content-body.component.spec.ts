import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSoundContentBodyComponent } from './update-sound-content-body.component';

describe('UpdateSoundContentBodyComponent', () => {
  let component: UpdateSoundContentBodyComponent;
  let fixture: ComponentFixture<UpdateSoundContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateSoundContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSoundContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

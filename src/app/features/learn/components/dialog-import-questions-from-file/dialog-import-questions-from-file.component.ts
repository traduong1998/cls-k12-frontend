import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';

export interface PeriodicElement {
  contentQuestion: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { contentQuestion: 'Hydrogen' },
  { contentQuestion: 'Helium' },
  { contentQuestion: 'Lithium' },
  { contentQuestion: 'Beryllium' },
  { contentQuestion: 'Boron' },
  { contentQuestion: 'Carbon' },
  { contentQuestion: 'Nitrogen' },
  { contentQuestion: 'Oxygen' },
  { contentQuestion: 'Fluorine' },
  { contentQuestion: 'Neon' },
];

@Component({
  selector: 'learn-dialog-import-questions-from-file',
  templateUrl: './dialog-import-questions-from-file.component.html',
  styleUrls: ['./dialog-import-questions-from-file.component.scss']
})
export class DialogImportQuestionsFromFileComponent implements OnInit {

  @ViewChild("file") file: ElementRef;

  displayedColumns: string[] =
    [
      'contentQuestion', //nội dung câu hỏi
      'select',
    ];
  //dataSource = ELEMENT_DATA;
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  selection = new SelectionModel<PeriodicElement>(true, []);

  // Whether the number of selected elements matches the total number of rows. 
  
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  //Selects all rows if they are not all selected; otherwise clear selection. 
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  //The label for the checkbox on the passed row 
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.contentQuestion + 1}`;
  }

  addFromFile() {
    this.file.nativeElement.click();
  }

  //get file
  handle(e) {
    console.log('Change input file', e)
  }

  downloadTemplateFile() {

  }

  constructor() { }


  ngOnInit(): void {
  }

}
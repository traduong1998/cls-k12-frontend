import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogImportQuestionsFromFileComponent } from './dialog-import-questions-from-file.component';

describe('DialogImportQuestionsFromFileComponent', () => {
  let component: DialogImportQuestionsFromFileComponent;
  let fixture: ComponentFixture<DialogImportQuestionsFromFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogImportQuestionsFromFileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogImportQuestionsFromFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

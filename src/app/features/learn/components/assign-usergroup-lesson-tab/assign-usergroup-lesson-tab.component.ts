import { MatSnackBar } from '@angular/material/snack-bar';
import { UserGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-group/endpoints/user-group-endpoint';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { AssignStudentsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/assignStudentsEndpoint';
import { AssignAndApprovalStudentRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/assignStudentsRequest';
import { AssignFrom } from '../../intefaces/assignFrom';
import { Action } from '../../intefaces/action';
import { ReplaySubject, Subject } from 'rxjs';
import { UserGroup } from 'sdk/cls-k12-sdk-js/src/services/user-group/models/user-group';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { GetListStudentForAssignLessonRequest } from 'sdk/cls-k12-sdk-js/src/services/user-group/requests/list-student-assign-lesson-request';
import { StudentForAssignResponse } from 'sdk/cls-k12-sdk-js/src/services/user-group/responses/student-for-assign-response';
import { FormControl } from '@angular/forms';
import { pairwise, startWith, takeUntil } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'learn-assign-usergroup-lesson-tab',
  templateUrl: './assign-usergroup-lesson-tab.component.html',
  styleUrls: ['./assign-usergroup-lesson-tab.component.scss']
})
export class AssignUsergroupLessonTabComponent implements OnInit, AfterViewInit {
  lessonId: number;
  userGroupId: number;
  lesson: Lesson;
  lessonEndpoint: LessonEndpoint;
  assignStudentEndpoint: AssignStudentsEndpoint;
  userGroupEndpoint: UserGroupEndpoint;
  assignDisplayedColumns: string[] = ['fullName', 'userName', 'className', 'action'];
  userGroupControl: FormControl = new FormControl('');
  filteredUserGroups: ReplaySubject<UserGroup[]> = new ReplaySubject<UserGroup[]>(1);;
  isFirstLoad = true;
  isListStudentForAssignReady: boolean = false;
  isLessonInfoReady: boolean = false;
  baseApiUrl: string = '';
  isSingleAssignClick: boolean;
  isDisableAssignClick: boolean= false;
  @ViewChild('assignStudentPaginator') assignPaginator: MatPaginator;
  public userGroupFilterCtrl: FormControl = new FormControl();
  //Danh sách học sinh đã được ghi danh
  listOldStudentForAssignId: number[];

  studentsAssignPaginationFilter: GetListStudentForAssignLessonRequest = {
    page: 1,
    size: 10,
    getCount: true,
    lessonId: +this.router.snapshot.paramMap.get("id"),
    userGroupId: 0,
    sortDirection: 'DESC',
    sortField: 'CRE'
  }
  listStudentForAssignedDefault: PaginatorResponse<StudentForAssignResponse> = {
    items: [],
    totalItems: 0
  };
  assignUserGroupDataSource: PaginatorResponse<StudentForAssignResponse> = this.listStudentForAssignedDefault;
  assignUserGroupSelection = new SelectionModel<StudentForAssignResponse>(true, []);
  assignStudentRequest: AssignAndApprovalStudentRequest;
  protected _onDestroy = new Subject<void>();
  protected userGroups: UserGroup[];


  constructor(private router: ActivatedRoute, private _router: Router, private _learnMessageService: LearnMessageService, private spinner: NgxSpinnerService) {
    this.assignStudentEndpoint = new AssignStudentsEndpoint();
    this.userGroupEndpoint = new UserGroupEndpoint();
    this.lessonEndpoint = new LessonEndpoint();
    this.lessonId = +this.router.snapshot.paramMap.get("id");
    this.assignStudentRequest = {
      lessonId: this.lessonId,
      lessonStatus: "",
      listUserLesson: [],
      assignFrom: AssignFrom.Assign
    };
    this.listOldStudentForAssignId = [];
    this.assignUserGroupSelection.clear();
   }
  ngAfterViewInit(): void {
     //Assign
     this.assignPaginator._intl.itemsPerPageLabel = "Số dòng trên trang";
     this.assignPaginator.page.subscribe(() => {
       this.studentsAssignPaginationFilter.page = this.assignPaginator.pageIndex + 1;
       this.studentsAssignPaginationFilter.size = this.assignPaginator.pageSize;
       this.getListStudentForAssign();
     });
      // listen for search field value changes 
      this.userGroupFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });

      this.userGroupControl?.valueChanges
      .pipe(startWith(this.userGroupControl?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            this.studentsAssignPaginationFilter.userGroupId = value;
            this.getListStudentForAssign();
          }
        }
      )
  }

  ngOnInit(): void {
    this.getLessonInfo();
  }
  protected filterDivisions() {
    if (!this.userGroups) {
      return;
    }
    // get the search keyword
    let search = this.userGroupFilterCtrl.value;
    if (!search) {
      this.filteredUserGroups.next(this.userGroups.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredUserGroups.next(
      this.userGroups.filter(userGroup => userGroup.name.toLowerCase().indexOf(search) > -1)
    );
  }

   /**Mấy hàm này danh cho check box bên tab ghi danh */
  /** Whether the number of selected elements matches the total number of rows. */
  isAssignUserGroupAllSelected() {
    const numSelected = this.assignUserGroupSelection.selected.length;
    const numRows = this.assignUserGroupDataSource.items.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  assignUserGroupMasterToggle() {
    if (this.isAssignUserGroupAllSelected()) {
      //Nếu là bỏ chọn hết thì thêm những học sinh đã ghi danh vào model để gỡ ghi danh (action: Delete)
      this.assignUserGroupDataSource.items.forEach((std) => {
        //Mấy thằng cũ đã được ghi danh thì kiểm tra xem nó đã được thêm vào trước đó với action delete chưa
        if (std.isAssigned) {
          if (this.assignStudentRequest.listUserLesson.find(x => x.id == std.id && x.action == Action.Delete) == undefined) {
            this.assignStudentRequest.listUserLesson.push({ id: std.id, action: Action.Delete });
          }
        } else {
          let studentAdd = this.assignStudentRequest.listUserLesson.find(x => x.id == std.id && x.action == Action.Add);
          if (studentAdd != undefined) {
            let index = this.assignStudentRequest.listUserLesson.indexOf(studentAdd);
            this.assignStudentRequest.listUserLesson.splice(index, 1);
          }
        }
      });
      console.log(this.assignStudentRequest.listUserLesson);
      this.assignUserGroupSelection.clear();
    } else {
      // Nếu chọn hết thì thêm những học sinh chưa ghi dahnh vào model để gửi lên(action: Add)
      this.assignUserGroupDataSource.items.forEach((std) => {
        if (!std.isAssigned) {
          if (this.assignStudentRequest.listUserLesson.find(x => x.id == std.id && x.action == Action.Add) == undefined) {
            this.assignStudentRequest.listUserLesson.push({ id: std.id, action: Action.Add });
          }
        }
        else {
          let studentDel = this.assignStudentRequest.listUserLesson.find(x => x.id == std.id && x.action == Action.Delete);
          if (studentDel != undefined) {
            let index = this.assignStudentRequest.listUserLesson.indexOf(studentDel);
            this.assignStudentRequest.listUserLesson.splice(index, 1);
          }
        }
      });
      console.log(this.assignStudentRequest.listUserLesson);
      this.assignUserGroupDataSource.items.forEach(row => this.assignUserGroupSelection.select(row));
    }
  }
  onChangeAssignUserGroupCheckBox(checked, row: StudentForAssignResponse): void {
    this.assignUserGroupSelection.toggle(row);
    console.log(checked);
    // nếu chọn thì thêm vào list request với action là add
    if (checked) {
      //Kiểm tra xem nếu là chọn thằng cũ thì chắc là nãy nó bị bỏ chọn rồi giờ chọn lại nên remove khỏi list request đi
      if (this.listOldStudentForAssignId.includes(row.id)) {
        let std = this.assignStudentRequest.listUserLesson.find(x => x.id == row.id);
        let index = this.assignStudentRequest.listUserLesson.indexOf(std)
        this.assignStudentRequest.listUserLesson.splice(index, 1);
      }
      else {
        this.assignStudentRequest.listUserLesson.push({ id: row.id, action: Action.Add });
      }
    }
    // nếu bỏ chọn thì ...
    else {
      //Nếu bỏ chọn thằng đã được ghi danh thì thêm vào request để biết mà gỡ ghi danh
      if (this.listOldStudentForAssignId.includes(row.id)) {
        this.assignStudentRequest.listUserLesson.push({ id: row.id, action: Action.Delete });
      }
      //Bỏ chọn thằng mới thì gỡ ra luôn khỏi list, ko gửi lên
      else {
        let std = this.assignStudentRequest.listUserLesson.find(x => x.id == row.id);
        let index = this.assignStudentRequest.listUserLesson.indexOf(std);
        if (!(index < 0)) {
          this.assignStudentRequest.listUserLesson.splice(index, 1);
        }
      }
    }
    console.log("new", this.assignStudentRequest.listUserLesson);
    console.log("old", this.listOldStudentForAssignId);
  }

  /** The label for the checkbox on the passed row */
  assignUserGroupCheckboxLabel(row?: StudentForAssignResponse): string {
    if (!row) {
      return `${this.isAssignUserGroupAllSelected() ? 'select' : 'deselect'} all`;
    }
    let index = this.assignUserGroupDataSource.items.findIndex(x=>x.id == row.id);
    return `${this.assignUserGroupSelection.isSelected(row) ? 'deselect' : 'select'} row ${index + 1}`;
  }
  onUserGroupSelectClicked() {
    if (this.isFirstLoad) {
      this.userGroupEndpoint.getListUserGroup()
        .then(res => {
          this.userGroups = res;
          this.isFirstLoad = false;
          if (this.userGroups && !this.userGroups.find(x => x.id == 0)) {
            this.userGroups.unshift({ id: null, name: 'Chọn nhóm',   userGroupCode : '',
            createrId : null,
            createdDate : null,
            portalId : null,
            divisionId : null,
            schoolId : null})
          }
          this.filteredUserGroups.next(this.userGroups.slice());
        })
        .catch(err => {
        })
    } else {
    }
  }
  onCancelAssignStudentTabClick(): void {
    this._router.navigate(['dashboard/learn/list-lesson']);
  }

  onSaveAssignStudentClick(): void {
    this.isSingleAssignClick = true;
    this.isDisableAssignClick = true;
    setTimeout(() => {
      if(this.isSingleAssignClick){
        this.assignStudentEndpoint.assignStudentsForLesson(this.assignStudentRequest).then((res) => {
          this.isDisableAssignClick = false;
          if (res) {
            //Nếu thành công thì reset lại list request
            this._learnMessageService.successToast("Bạn vừa mới thực hiện ghi danh/ gỡ ghi danh thành công");
            //Update list oldId
            this.assignStudentRequest.listUserLesson.forEach((std) => {
              if (std.action == Action.Add) {
                this.listOldStudentForAssignId.push(std.id);
                this.assignUserGroupDataSource.items.map((x)=>{
                  if(x.id == std.id){
                    x.isAssigned = true;
                  }
                })
              }
              else {
                let index = this.listOldStudentForAssignId.indexOf(std.id);
                this.listOldStudentForAssignId.splice(index, 1);
                this.assignUserGroupDataSource.items.map((x)=>{
                  if(x.id == std.id){
                    x.isAssigned = false;
                  }
                })
              }
            });
          } else {
            this._learnMessageService.failToast("Có lỗi xảy ra");
            //Reset lại combobox
            this.assignStudentRequest.listUserLesson.forEach((std) => {
              let row = this.listStudentForAssignedDefault.items.find(x => x.id == std.id);
              this.assignUserGroupSelection.toggle(row);
            });
          }
          this.assignStudentRequest.listUserLesson = [];
        }).catch((err) => {
          this._learnMessageService.failToast("Có lỗi xảy ra");
          //Reset lại combobox
          this.assignStudentRequest.listUserLesson.forEach((std) => {
            let row = this.listStudentForAssignedDefault.items.find(x => x.id == std.id);
            this.assignUserGroupSelection.toggle(row);
          });
          this.assignStudentRequest.listUserLesson = [];
        });
      }
    }, 250);
  }

  onDoubleClick(): void {
    this.isSingleAssignClick = false;
  }
  private getLessonInfo(): void {
    this.lessonEndpoint.getLessonById(this.lessonId).then((res) => {
      this.lesson = { ...res };
      this.assignStudentRequest.lessonStatus = res.status;
      this.userGroupEndpoint.getListUserGroup()
        .then(res => {
          this.userGroups = res;
          this.userGroupControl.setValue(this.userGroups[0].id);
          this.studentsAssignPaginationFilter.userGroupId = this.userGroups[0].id;
          this.isFirstLoad = false;
          this.filteredUserGroups.next(this.userGroups.slice());
          this.isLessonInfoReady = true;
        })
        .catch(err => {
          // s  console.log(err);
        })
   
    });
  }
  private getListStudentForAssign() {
    this.spinner.show();
    this.userGroupEndpoint.getListStudentForAssignLesson(this.studentsAssignPaginationFilter)
      .then(res => {
        if (res) {
          this.assignUserGroupSelection.clear();
          this.assignUserGroupDataSource = res;
          //Gán những học sinh đã được ghi danh vào trong list này
          this.assignUserGroupDataSource.items.forEach((x) => {
            if (x.isAssigned) {
              this.listOldStudentForAssignId.push(x.id);
              this.assignUserGroupSelection.select(x);
            } else if (this.assignStudentRequest.listUserLesson.find(y => y.id == x.id && y.action == 'ADD') != undefined) {
              this.assignUserGroupSelection.select(x);
            }
          });
          this.spinner.hide();
          this.isListStudentForAssignReady = true;
        }
      });
  }
}

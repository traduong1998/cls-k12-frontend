import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignUsergroupLessonTabComponent } from './assign-usergroup-lesson-tab.component';

describe('AssignUsergroupLessonTabComponent', () => {
  let component: AssignUsergroupLessonTabComponent;
  let fixture: ComponentFixture<AssignUsergroupLessonTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignUsergroupLessonTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignUsergroupLessonTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

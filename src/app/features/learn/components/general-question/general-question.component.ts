import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-general-question',
  templateUrl: './general-question.component.html',
  styleUrls: ['./general-question.component.scss']
})
export class GeneralQuestionComponent implements OnInit {
  @Input() config:any;
  
  constructor() { }

  ngOnInit(): void {
  }
  ckeditorContent;
  configQuestion = {
    startupFocus: true,
    allowedContent: true,
    fullPage: true,
    toolbar: [
      { name: "styles", items: ['Format', 'Font', 'FontSize', "-", "TextColor", "BGColor"] },
      { name: "basicstyles", items: ["Bold", "Italic", "Underline", "RemoveFormat"] },
      { name: "justify", items: ["JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock"] },
      { name: "paragraph", items: ["NumberedList", "BulletedList", "-", "Outdent", "Indent", "-", "Blockquote"] },
      { name: "links", items: ["Link", "Unlink"] },
      { name: "insert", items: ["Image","Audio","Video", "Table", "HorizontalRule", "Smiley", "SpecialChar"] },
      { name: "document", items: ["Source"] },
    ],
  };
}

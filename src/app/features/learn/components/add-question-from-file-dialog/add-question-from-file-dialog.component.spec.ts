import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddQuestionFromFileDialogComponent } from './add-question-from-file-dialog.component';

describe('AddQuestionFromFileDialogComponent', () => {
  let component: AddQuestionFromFileDialogComponent;
  let fixture: ComponentFixture<AddQuestionFromFileDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddQuestionFromFileDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddQuestionFromFileDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

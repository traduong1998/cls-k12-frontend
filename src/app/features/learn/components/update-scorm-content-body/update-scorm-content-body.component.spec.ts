import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateScormContentBodyComponent } from './update-scorm-content-body.component';

describe('UpdateScormContentBodyComponent', () => {
  let component: UpdateScormContentBodyComponent;
  let fixture: ComponentFixture<UpdateScormContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateScormContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateScormContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonConfigBodyComponent } from './lesson-config-body.component';

describe('LessonConfigBodyComponent', () => {
  let component: LessonConfigBodyComponent;
  let fixture: ComponentFixture<LessonConfigBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LessonConfigBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonConfigBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

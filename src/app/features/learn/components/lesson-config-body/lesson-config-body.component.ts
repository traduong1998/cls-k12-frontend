import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ConfirmationDialogComponent } from './../../../../shared/material/confirmation-dialog/confirmation-dialog.component';
import { Router } from '@angular/router';
import { LearnTime } from './../../intefaces/learnTime';
import { LessonConfigRequest } from './../../../../../../sdk/cls-k12-sdk-js/src/services/lesson/requests/lessonConfigRequest';
import { Observable, Subject, Subscription } from 'rxjs';
import { UseCase } from './../../intefaces/useCase';
import { StartTimeLine } from './../../intefaces/startTimeLine';
import { ContentRule } from './../../intefaces/contentRule';
import { ShareSetting } from './../../intefaces/shareSetting';
import { Option } from 'src/app/shared/interface/Option';
import { Component, Input, OnInit, AfterViewInit, OnChanges, HostListener } from '@angular/core';
import { AssessmentLevelGroupEndPoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/assessmentLevelGroupEndPoint';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/lessonEndpoint';
import { ConfigLessonResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/configLessonResponses';
import { MatDialog } from '@angular/material/dialog';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'learn-lesson-config-body',
  templateUrl: './lesson-config-body.component.html',
  styleUrls: ['./lesson-config-body.component.scss']
})
export class LessonConfigBodyComponent implements OnInit, AfterViewInit, OnChanges {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  baseApiUrl = 'http://localhost:58910';
  @Input() useCase: number;
  @Input() lessonId: number;
  @Input() events: Observable<string>;
  @Input() lessonConfig: any;

  learnTimeControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$"),
    Validators.min(1)
  ]);
  private eventsSubscription: Subscription;
  public get learnTime(): typeof LearnTime {
    return LearnTime;
  }

  public get startTimeLine(): typeof StartTimeLine {
    return StartTimeLine;
  }

  public get contentRule(): typeof ContentRule {
    return ContentRule;
  }

  public get useCaseEnum(): typeof UseCase {
    return UseCase;
  }
  optionsRegister: Option[] = [
    { id: ShareSetting.apprpval, name: 'Đăng ký và chờ duyệt ' }, { id: ShareSetting.noApproval, name: 'Đăng ký và học ngay' }, { id: ShareSetting.register, name: 'Ghi danh' },
  ];

  optionDefaultShareSetting: Option;
  optionDefaultAssessmentLevelGroup: Option;
  listAssessmentLevelGroup: Option[] = [];
  assessmentLevelGroupEndPoint: AssessmentLevelGroupEndPoint;
  lessonEndPoint: LessonEndpoint;
  newLessonConfig: ConfigLessonResponses;

  request: LessonConfigRequest;

  // Kiểm tra dữ liệu đã được load hay chưa
  isReady: boolean = false;
  constructor(private _route: Router, private dialog: MatDialog, private _snackBar: MatSnackBar) {
    this.assessmentLevelGroupEndPoint = new AssessmentLevelGroupEndPoint();
    this.lessonEndPoint = new LessonEndpoint({ baseUrl: this.baseApiUrl });
    this.optionDefaultShareSetting = this.optionsRegister[0];
    this.lessonConfig = null;
  }

  ngOnChanges(): void {
    //Khởi tạo giá trị 
    //Gọi api danh sách thang đánh giá
    this.getAllAssessmentLevelGroup();
  }
  ngAfterViewInit(): void {

  }
  ngOnInit(): void {
    if (this.events != null && this.events != undefined) {
      this.eventsSubscription = this.events.subscribe((action) => {
        if (action == "save") {
          this.submitConfigLesson();
        }
        else if (action == "next") {
          this.nextToFinish();
        }
        else if (action == "back") {
          this.backToContents();
        }
      })
    }
  }
  ngOnDestroy() {
    this.eventsSubscription.unsubscribe();
  }

  initialData(): void {
    // Nếu là đang sửa/duyệt thì ko khởi tạo giá trị config
    if (this.useCase != this.useCaseEnum.creating) {
      if (this.lessonConfig.shareSetting) {
        this.optionDefaultShareSetting = this.optionsRegister.find(x => x.id == this.lessonConfig.shareSetting);
      }
      if (this.lessonConfig.assessmentLevelGroupId != undefined && this.lessonConfig.assessmentLevelGroupId != null) {
        this.optionDefaultAssessmentLevelGroup = this.listAssessmentLevelGroup.find(x => x.id == this.lessonConfig.assessmentLevelGroupId);
      }
      this.newLessonConfig = { ...this.lessonConfig };
      this.isReady = true;
    }
    // Nếu là tạo mới thì khởi tạo giá trị config mặc định
    else {
      this.lessonConfig = {
        id: this.lessonId,
        shareSetting: ShareSetting.apprpval,
        assessmentLevelGroupId: null,
        formOfLearn: ContentRule.sequentially,
        learnTime: LearnTime.infinite,
        interval: null,
        startTimeLine: null
      }
      this.newLessonConfig = { ...this.lessonConfig };
      this.isReady = true;
    }
  }
  getAllAssessmentLevelGroup(): void {
    this.assessmentLevelGroupEndPoint.getAll().then((res) => {
      this.listAssessmentLevelGroup = [...res];
      this.listAssessmentLevelGroup.unshift({ name: "Chọn thang đánh giá", id: 0 })
      this.initialData();
    });
  }

  submitConfigLesson(): void {
    if (this.useCase == this.useCaseEnum.editting) {
      let temp = { ...this.lessonConfig };

      if (this.newLessonConfig.learnTime == this.learnTime.infinite) {
        if (JSON.stringify(temp) != JSON.stringify(this.newLessonConfig)) {
          this.request = this.bindingRequest(this.newLessonConfig);
          this.updateLessonConfig(this.request);
        }
        else {
          this._snackBar.openFromComponent(SuccessSnackBarComponent, {
            data: 'Cập nhật thành công!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      } else {
        if (JSON.stringify(temp) != JSON.stringify(this.newLessonConfig) && this.learnTimeControl.valid) {
          this.request = this.bindingRequest(this.newLessonConfig);
          this.updateLessonConfig(this.request);
        }
        else if (JSON.stringify(temp) == JSON.stringify(this.newLessonConfig)) {
          this._snackBar.openFromComponent(SuccessSnackBarComponent, {
            data: 'Cập nhật thành công!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      }

    }
    else {
      if (this.newLessonConfig.learnTime == this.learnTime.infinite) {
        this.request = this.bindingRequest(this.newLessonConfig);
        this.updateLessonConfig(this.request);
      }
      else {
        if (this.learnTimeControl.valid) {
          this.request = this.bindingRequest(this.newLessonConfig);
          this.updateLessonConfig(this.request);
        }
      }

    }
  }

  nextToFinish(): void {
    if (this.useCase == this.useCaseEnum.editting) {
      let temp = { ...this.lessonConfig };
      //Nếu đã thay đổi thiết lập mà bấm tiếp tục thì thông báo có lưu hay không
      if (this.newLessonConfig.learnTime == this.learnTime.infinite) {
        if (JSON.stringify(temp) != JSON.stringify(this.newLessonConfig)) {
          this.openDialog().subscribe((confirm) => {
            if (confirm === true) {
              this.request = this.bindingRequest(this.newLessonConfig);
              this.updateLessonConfig(this.request).subscribe((result) => {
                if (result) {
                  this._route.navigate([`/dashboard/learn/finish-lesson/${this.lessonId}`]);
                }
              });
            } else if (confirm === false) {
              this._route.navigate([`/dashboard/learn/finish-lesson/${this.lessonId}`]);
            }
          });
          //Nếu không thay đổi thì qua luôn
        } else {
          this._route.navigate([`/dashboard/learn/finish-lesson/${this.lessonId}`]);
        }
      } else {
        if (JSON.stringify(temp) != JSON.stringify(this.newLessonConfig) && this.learnTimeControl.valid) {
          this.openDialog().subscribe((confirm) => {
            if (confirm === true) {
              this.request = this.bindingRequest(this.newLessonConfig);
              this.updateLessonConfig(this.request).subscribe((result) => {
                if (result) {
                  this._route.navigate([`/dashboard/learn/finish-lesson/${this.lessonId}`]);
                }
              });
            } else if (confirm === false) {
              this._route.navigate([`/dashboard/learn/finish-lesson/${this.lessonId}`]);
            }
          });
          //Nếu không thay đổi thì qua luôn
        } else if (JSON.stringify(temp) == JSON.stringify(this.newLessonConfig)) {
          this._route.navigate([`/dashboard/learn/finish-lesson/${this.lessonId}`]);
        }
      }
    }
    //Nếu là tạo mới thiết lập
    else {
      if (this.newLessonConfig.learnTime == this.learnTime.infinite) {
        this.request = this.bindingRequest(this.newLessonConfig);
        this.updateLessonConfig(this.request);
        this._route.navigate([`/dashboard/learn/finish-lesson/${this.lessonId}`]);
      }
      else {
        if (this.learnTimeControl.valid) {
          this.request = this.bindingRequest(this.newLessonConfig);
          this.updateLessonConfig(this.request);
          this._route.navigate([`/dashboard/learn/finish-lesson/${this.lessonId}`]);
        }
      }
    }
  }

  backToContents(): void {
    let temp = { ...this.lessonConfig };
    //Nếu đã thay đổi thiết lập mà bấm quay lại thì thông báo có lưu hay không

    if (this.newLessonConfig.learnTime == this.learnTime.infinite) {
      if (JSON.stringify(temp) != JSON.stringify(this.newLessonConfig)) {
        this.openDialog().subscribe((confirm) => {
          if (confirm === true) {
            this.request = this.bindingRequest(this.newLessonConfig);
            this.updateLessonConfig(this.request).subscribe((result) => {
              if (result == true) {
                this._route.navigate([`/dashboard/learn/edit-content-lesson/${this.lessonId}`]);
              }
            });
          }
          else if (confirm === false) {
            this._route.navigate([`/dashboard/learn/edit-content-lesson/${this.lessonId}`]);
          }
        });
      }
      //Nếu không thay đổi thì qua luôn
      else {
        this._route.navigate([`/dashboard/learn/edit-content-lesson/${this.lessonId}`]);
      }
    }
    else {
      if (JSON.stringify(temp) != JSON.stringify(this.newLessonConfig) && this.learnTimeControl.valid) {
        this.openDialog().subscribe((confirm) => {
          if (confirm === true) {
            this.request = this.bindingRequest(this.newLessonConfig);
            this.updateLessonConfig(this.request).subscribe((result) => {
              if (result == true) {
                this._route.navigate([`/dashboard/learn/edit-content-lesson/${this.lessonId}`]);
              }
            });
          }
          else if (confirm === false) {
            this._route.navigate([`/dashboard/learn/edit-content-lesson/${this.lessonId}`]);
          }
        });
      }
      //Nếu không thay đổi thì qua luôn
      else if (JSON.stringify(temp) == JSON.stringify(this.newLessonConfig)) {
        this._route.navigate([`/dashboard/learn/edit-content-lesson/${this.lessonId}`]);
      }
    }


    
  }
  openDialog(): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '303px',
      data: {
        message: 'Bạn có muốn lưu lại cấu hình bài giảng?',
        buttonText: {
          ok: 'Lưu',
          cancel: 'Hủy',
          colorButtonSubmit: 'primary'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }
  bindingRequest(model: ConfigLessonResponses): LessonConfigRequest {
    let request: LessonConfigRequest = {
      id: this.newLessonConfig.id,
      shareSetting: model.shareSetting,
      assessmentLevelGroupId: model.assessmentLevelGroupId,
      formOfLearn: model.formOfLearn,
      learnTime: model.learnTime,
      interval: model.interval,
      startTimeLine: model.startTimeLine
    }
    if (request.learnTime == LearnTime.infinite) {
      request.interval = null;
      request.startTimeLine = null;
    }
    return request;
  }

  onSelectShareSetting(option: Option): void {
    this.newLessonConfig.shareSetting = option.id;
  }

  onSelectAssessmentLevelGroup(option: Option): void {
    this.newLessonConfig.assessmentLevelGroupId = option.id;
  }

  onSelectDetermined(event): void {
    if (this.newLessonConfig.interval == null) {
      this.newLessonConfig.interval = 1;
    }
    if (this.newLessonConfig.startTimeLine == null) {
      this.newLessonConfig.startTimeLine = this.startTimeLine.start;
    }
  }

  updateLessonConfig(requestModel): Observable<boolean> {
    let result = new Subject<boolean>();
    this.lessonEndPoint.addLessonConfig(requestModel).then((res) => {
      if (res) {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: 'Cập nhật thành công!',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        this.lessonConfig = { ...this.newLessonConfig };
        result.next(true);
      }
      else {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Có lỗi xảy ra vui lòng thử lại!',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        result.next(false);
      }
    });
    return result.asObservable();
  }

  @HostListener('window:beforeunload', ['$event'])
  unloadHandler($event) {
    debugger;
    let temp = { ...this.lessonConfig };
    //Nếu đã thay đổi thiết lập mà bấm quay lại thì thông báo có lưu hay không
    if (JSON.stringify(temp) != JSON.stringify(this.newLessonConfig)) {
      return false
    }
    else {
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateIframeContentBodyComponent } from './update-iframe-content-body.component';

describe('UpdateIframeContentBodyComponent', () => {
  let component: UpdateIframeContentBodyComponent;
  let fixture: ComponentFixture<UpdateIframeContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateIframeContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateIframeContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { FormControl, Validators } from '@angular/forms';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../../intefaces/dialogData';
import { LearnMessageService } from '../../Services/learn-message-service.service';

@Component({
  selector: 'learn-add-part-dialog',
  templateUrl: './add-part-dialog.component.html',
  styleUrls: ['./add-part-dialog.component.scss']
})
export class AddPartDialogComponent implements OnInit {
  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT
  //#endregion

  //#region PRIVATE PROPERTY
  //#endregion

  //#region PUBLIC PROPERTY
  public valueDialog: string = '';
  public nameControl:FormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(255)
  ])
  //#endregion

  //#region CONSTRUCTOR
  constructor(
    public dialogRef: MatDialogRef<AddPartDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _learnMessageService: LearnMessageService) {
    this.valueDialog = data?.name;
  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {

  }

  //#endregion

  //#region PUBLIC METHOD
  public onNoClick(): void {

    this.dialogRef.close();
  }


  public onAddPartClick(): void {
    if (this.nameControl.invalid) {
      this.nameControl.markAllAsTouched();
    }
    else {
      this.dialogRef.close(this.valueDialog);
    }
  }

  public getErrorPartInput(){
    return true; // valueDialog
  } 
  //#endregion

  //#region PRIVATE METHOD
  //#endregion

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDetailContentBodyComponent } from './update-detail-content-body.component';

describe('UpdateDetailContentBodyComponent', () => {
  let component: UpdateDetailContentBodyComponent;
  let fixture: ComponentFixture<UpdateDetailContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateDetailContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDetailContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLessonToBeApprovedGridviewComponent } from './list-lesson-to-be-approved-gridview.component';

describe('ListLessonToBeApprovedGridviewComponent', () => {
  let component: ListLessonToBeApprovedGridviewComponent;
  let fixture: ComponentFixture<ListLessonToBeApprovedGridviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListLessonToBeApprovedGridviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLessonToBeApprovedGridviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

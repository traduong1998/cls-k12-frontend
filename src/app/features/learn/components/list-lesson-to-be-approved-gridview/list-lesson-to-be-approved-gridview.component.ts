import { Router } from '@angular/router';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { LessonToBeApprovedEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/lessonToBeApprovedEndpoint';
import { LessonToBeApprovedResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/lessonToBeApprovedResponses';
import { LessonToApprovalRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/lesson-to-approval-request';
import { UserEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user/endpoints/UserEndpoint';
import { PaginatorResponse } from 'cls-k12-sdk-js/src/core/api/responses/PaginatorResponse';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/lessonEndpoint';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';

const lessonPendingPaginationDefault: PaginatorResponse<LessonToBeApprovedResponses> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'learn-list-lesson-to-be-approved-gridview',
  templateUrl: './list-lesson-to-be-approved-gridview.component.html',
  styleUrls: ['./list-lesson-to-be-approved-gridview.component.scss']
})
export class ListLessonToBeApprovedGridviewComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  @ViewChild("ipSearch") ipSearch: ElementRef;

  displayedColumns: string[] = ['stt', 'name', 'nameOfUser', 'submissionDate', 'function'];

  lessonPagination: PaginatorResponse<LessonToBeApprovedResponses> = lessonPendingPaginationDefault;
  lessonPendingPaginationFilter: LessonToApprovalRequest = {
    pageNumber: 0,
    sizeNumber: 10,
    keyWord: '',
    getCount: true,
    sortDirection: 'DESC',
    sortField: 'CRE'
  }

  userEndpoint: UserEndpoint;
  lessonEndpoint: LessonEndpoint;
  lessonToBeApprovedEnpoint: LessonToBeApprovedEndpoint = null;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private _snackBar: MatSnackBar, private router: Router) {
    this.userEndpoint = new UserEndpoint();
    this.lessonToBeApprovedEnpoint = new LessonToBeApprovedEndpoint();
    this.lessonEndpoint = new LessonEndpoint();
  }
  ngOnInit(): void {
    this.getListLesson();
  }
  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.lessonPendingPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.lessonPendingPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.getListLesson();
    });
  }

  async applyFilter() {
    this.lessonPendingPaginationFilter.keyWord = this.ipSearch.nativeElement.value;
    this.getListLesson();
  }

  onClickApproved(id: any) {
    console.log(id)
  }

  getListLesson() {
    this.lessonToBeApprovedEnpoint.getListLessonToBeApproved(this.lessonPendingPaginationFilter)
      .then((res) => {
        this.lessonPagination = res;
        let listUserId = this.lessonPagination.items.map(a => a.userId);
        return this.userEndpoint.GetListUserByIds(listUserId);
      })
      .then((userNames) => {
        this.lessonPagination.items.forEach(x => {
          let user = userNames.find(y => y.id == x.userId);
          x.nameOfUser = user?.name;
        });
      })

  }
  approvalLesson(lessonId: number, lessonName: string): void {
    this.lessonEndpoint.approvalLesson(lessonId).then(async (res) => {
      if (res) {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: `Bài giảng ${lessonName} đã được duyệt`,
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        await this.getListLesson();
      }
      else {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Có lỗi xảy ra vui lòng thử lại',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    });
  }

  returnLesson(lessonId: number, lessonName: string): void {
    this.lessonEndpoint.returnLesson(lessonId).then(async (res) => {
      if (res) {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: `Bài giảng ${lessonName} đã được trả lại`,
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        await this.getListLesson();
      }
      else {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Có lỗi xảy ra vui lòng thử lại',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    });
  }

  detailApprovalLesson(lessonId: number){
    this.router.navigate([`dashboard/learn/approval-lesson/${lessonId}`])
  }
}


import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSoundContentBodyComponent } from './add-sound-content-body.component';

describe('AddSoundContentBodyComponent', () => {
  let component: AddSoundContentBodyComponent;
  let fixture: ComponentFixture<AddSoundContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSoundContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSoundContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

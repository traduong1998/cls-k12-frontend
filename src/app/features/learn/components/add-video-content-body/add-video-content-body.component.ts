import { LessonStatus } from './../../intefaces/lessonStatus';
import { ConditionForCompletion } from 'src/app/features/learn/intefaces/conditionForCompletion';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ContentGroupForCombobox } from './../../../../../../sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { Component, ElementRef, HostListener, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { Option } from '../../../../shared/interface/Option';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { DetailContentRequests } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/detailContentRequests';
import { ContentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentEndpoint';
import { DetailContentResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/detailContentResponses';
import { ActivatedRoute, Router } from '@angular/router';
//qsb
import { QuestionFileService } from './../../../../core/services/question-file/question-file.service';
import { Question } from 'sdk/cls-k12-sdk-js/src/services/question/models/Question';
import { UseCase } from '../../intefaces/useCase';
import { Observable, of } from 'rxjs';
import { QuestionTest } from '../../intefaces/questionTest';
import { TestService } from '../../Services/test.service';
import { CKEditor4, CKEditorComponent } from 'ckeditor4-angular';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
//grc
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatSelect } from '@angular/material/select';
import { ContentGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentGroupEndpoint';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import videojs from 'video.js';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { MimeTypes } from 'src/app/shared/enums/mime-type';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { QuestionConverterEndpoint } from 'sdk/cls-k12-sdk-js/src/services/question-converter/endpoints/question-converter-endpoint';
import { Action } from 'src/app/shared/modules/question-banks/Interfaces/action';
import { ConfigUploadCkeditor } from 'src/app/core/interfaces/app-config';
import { CLS } from 'sdk/cls-k12-sdk-js/src';
import { DialogUpdateScoreComponent } from '../dialog-update-score/dialog-update-score.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { QuestionsOverviewComponent } from '../questions-overview/questions-overview.component';
import { Configuration } from 'src/app/shared/configurations';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';


@Component({
  selector: 'learn-add-video-content-body',
  templateUrl: './add-video-content-body.component.html',
  styleUrls: ['./add-video-content-body.component.scss']
})
export class AddVideoContentBodyComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';

  @Input() lessonName: string;
  @Input() lessonId: number;
  @Input() contentId: number;
  @Input() useCase: number = UseCase.editting;
  toolTip: string = Configuration.MESSAGE_INFOR_VIDEO;
  playerLearn: videojs.Player;
  progressData: Observable<any>;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }
  private FILE_LIMIT: number = 500;
  numbers: 0;
  private serverFileId: string = null;
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>
  group_content: ContentGroupForCombobox[] = [];
  contentGroup: Option = { id: undefined, name: undefined };
  nameFileVideo: string = "";
  linkYoutubeVideo = "";
  linkLoadVideo = "";
  showYoutubeVideo = false;
  showFileVideo = false;
  isAddVideoYoutube = true;
  videoSource: number;
  contentGroupId: number;
  nameContent: string;
  conditionsForCompletion: string;
  minutes: number = 0;
  seconds: number = 0;
  contentCkEditor: string;
  configCkeditor = {};
  videoContent: File;
  detailContentRequests: DetailContentRequests;
  detailContentResponses: DetailContentResponses;
  contentEndpoint: ContentEndpoint;
  contentGroupEndpoint: ContentGroupEndpoint;
  detailContent: DetailContentRequests;
  fileServer: FileServerEndpoint;
  showListQuestion = false;
  question: Question = null;
  allComplete: boolean = false;
  isLoadDetailContent: boolean = false;
  currentQuestion: QuestionTest;
  questions: QuestionTest[];
  questionConverterEndpoint: QuestionConverterEndpoint;
  childIndex: number;
  questionsShow: QuestionTest[] = [];
  configUpload: ConfigUploadCkeditor;
  totalScore: number;
  currentQuestionId: number;
  private dialogRef: MatDialogRef<ProgressDialogComponent>;
  nameContentControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(255),
  ]);
  minutesControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
  ]);
  secondsControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
  ]);
  public groupContentControl: FormControl = new FormControl();
  public groupContentFilterControl: FormControl = new FormControl();
  public filteredGroupContents: ReplaySubject<ContentGroupForCombobox[]> = new ReplaySubject<ContentGroupForCombobox[]>(0);
  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();
  @ViewChild("fileQuestion") fileQuestion: ElementRef;
  @ViewChild("ck") ck: CKEditorComponent;
  @ViewChild("singleSelect") singleSelect: MatSelect;
  @ViewChild("file") file: ElementRef;
  @ViewChild('videoLearn') videoLearn: ElementRef<HTMLVideoElement>;
  get questionFormat(): typeof FormatQuestion {
    return FormatQuestion;
  }
  get questionLevel(): typeof Level {
    return Level;
  }
  get questionType(): typeof Type {
    return Type;
  }
  get actionEnum(): typeof Action {
    return Action;
  }

  //#region Thiết lập câu hỏi
  isSetTestTime: boolean = false;
  isWriteLog: boolean = true;
  isGetRandomQuestion: boolean = false;
  totalQuestion: number;
  testTimeControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$"),
    Validators.min(5)
  ]);
  allowToReDoControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$")
  ]);
  itemPerPageCtrl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$")
  ])
  //#endregion
  constructor(public dialog: MatDialog,
    private route: ActivatedRoute,
    private questionFilleService: QuestionFileService,
    private testService: TestService,
    private router: Router,
    private _learnMessageService: LearnMessageService,
    private authService: AuthService,
    private _snackBar: MatSnackBar,
    private _authService: AuthService,
    private configService: AppConfigService,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.contentEndpoint = new ContentEndpoint();
    this.detailContentRequests = new DetailContentRequests;
    this.detailContentRequests.testInfo = {
      viewCorrectResultAfterSubmittingTheTest: false,
      numberOfQuestionOnThePage: null,
      testTime: null,
      allowToRedo: false,
      theMaximumNumberToRedo: null,
      isRandomizeQuestions: false
    };
    this.detailContentResponses = new DetailContentResponses;
    this.detailContent = new DetailContentRequests;
    this.lessonId = + this.route.snapshot.paramMap.get("id");
    this.detailContentRequests.lessonId = this.lessonId;
    this.contentGroupEndpoint = new ContentGroupEndpoint();
    this.fileServer = new FileServerEndpoint();
    this.questionConverterEndpoint = new QuestionConverterEndpoint();
    this.contentGroupEndpoint.getListContentGroupForCombobox(this.lessonId).then((res) => {
      this.group_content = res;
      this.group_content.unshift({ id: 0, name: "Chọn tiết", order: -1 });
      this.filteredGroupContents.next(this.group_content);
    });
    //ckeditor
    this.configCkeditor = {
      height: 150,
      allowedContent: false,
      EditorType: CKEditor4.EditorType.CLASSIC,
      removeButtons: 'Image,Flash',
    };
    this.testService.questions$.subscribe((questions) => {
      this.questions = JSON.parse(JSON.stringify(questions));
      this.questionsShow = [...this.questions.filter(x => x.action != Action.Delete)];
      this.questionsShow.map((qst) => {
        if (qst.format == FormatQuestion.group) {
          qst.questions = [...qst.questions.filter(x => x.action != Action.Delete)];
        }
      });
      this.itemPerPageCtrl = new FormControl('', [
        Validators.required,
        Validators.max(this.questionsShow.length)
      ])
      if (this.questionsShow.length > 0) {
        this.itemPerPageCtrl = new FormControl('', [
          Validators.min(1),
          Validators.required,
          Validators.max(this.questionsShow.length)
        ]);
      }
      this.detailContentRequests.testInfo.numberOfQuestionOnThePage = 0;
      this.detailContentRequests.testInfo.numberOfQuestionOnThePage = this.detailContentRequests.testInfo.numberOfQuestionOnThePage > this.questionsShow.length ?
      this.questionsShow.length : (this.detailContentRequests.testInfo.numberOfQuestionOnThePage == 0 && this.questionsShow.length > 5) ?
          5 : this.detailContentRequests.testInfo.numberOfQuestionOnThePage > 0 ?
            this.detailContentRequests.testInfo.numberOfQuestionOnThePage : this.questionsShow.length;
      this.itemPerPageCtrl.setValue(this.detailContentRequests.testInfo.numberOfQuestionOnThePage);
    });
    this.conditionsForCompletion = "BUT";
    this.videoSource = 1;
    let baseUrl = CLS.getConfig().apiBaseUrls.fileServer ?? CLS.getConfig().apiBaseUrl;
    baseUrl += '/api';
    this.configUpload = {
      portalId: configService.getConfig().unit.portalId,
      token: _authService.getToken(),
      uploadUrl: baseUrl + "/file/upload"
    };
    this.testService.totalScore$.subscribe((totalScore) => {
      this.totalScore = totalScore;
    });

    this.baseApiUrl = CLS.getConfig().apiBaseUrl;

  }
  ngOnInit(): void {
    // set initial selection
    this.groupContentControl.setValue(this.group_content[-1]);
    // load the initial bank list
    this.filteredGroupContents.next(this.group_content.slice());
    // listen for search field value changes
    this.groupContentFilterControl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBanks();
      });
  }
  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
    this.testService.clear();
  }
  changControlInput(): void {
    this.isSetTestTime ? this.testTimeControl.enable() : this.testTimeControl.disable();
    this.detailContentRequests.testInfo?.allowToRedo ? this.allowToReDoControl.enable() : this.allowToReDoControl.disable();
    if (!this.isSetTestTime) {
      this.testTimeControl.reset();
      this.detailContentRequests.testInfo.testTime = null;
    } else {
      this.testTimeControl.markAsTouched();
    }
    if (!this.detailContentRequests.testInfo?.allowToRedo) {
      this.allowToReDoControl.reset();
      this.detailContentRequests.testInfo.theMaximumNumberToRedo = null;
    }
    else {
      this.allowToReDoControl.markAsTouched();
    }
    //this.isGetRandomQuestion ? this.questionsRandomCtrl.enable() : this.questionsRandomCtrl.disable();
  }
  protected filterBanks() {
    if (!this.group_content) {
      return;
    }
    // get the search keyword
    let search = this.groupContentFilterControl.value;
    if (!search) {
      this.filteredGroupContents.next(this.group_content.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredGroupContents.next(
      this.group_content.filter(bank => bank.name.toLowerCase().indexOf(search) > -1)
    );
  }

  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
  }
  addYoutube(link: string) {
    this.showYoutubeVideo = false;
    if (this.validURL(link)) {

      this.linkYoutubeVideo = link;
      this.linkLoadVideo = this.getEmbedLink(link);
      if (this.linkLoadVideo) {
        this.showYoutubeVideo = true;
      } else {
        this._learnMessageService.failToast("URL lỗi, mời bạn nhập lại");
        this.linkYoutubeVideo = '';
        this.linkLoadVideo = '';
      }
    }
    else {
      this._learnMessageService.failToast("Không đúng định dạng URL, mời bạn nhập lại");
      this.linkYoutubeVideo = '';
    }
  }
  addFromFile() {
    this.file.nativeElement.click();
  }
  //get file
  uploadFileVideo(e) {
    const file = e.target.files[0];
    // kiểm tra nếu ko có file thì return
    if (!file) {
      return;
    }

    // Kiểm tra limit file
    let confirmLimit = Configuration.getLimitFileMessage(file);
    if (!confirmLimit.status) {
      this._learnMessageService.failToast(confirmLimit.message);
      return;
    }
    var type = file.name.substring(file.name.lastIndexOf(".") + 1).toLowerCase();
    if (['mp4', 'webm', 'ogg', 'ogv', 'avi', 'mpeg', 'mpg', 'mov', 'wmv', '3gp', 'flv', 'rmvb', 'm4v', 'mkv'].includes(type)) {
      this.showFileVideo = false;
      this.videoContent = file;

      Promise.resolve()
        .then(() => {
          console.log(`start process`);
          this.openProgressDialog(null);
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.fileServer.uploadFile({
            file: file,
            isPrivate: true,
            moduleName: ServiceName.LEARN
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          // skip previous file
          this.serverFileId && this.fileServer.skipFile(this.serverFileId).then(resFileSkip => {
          });
          return res;
        })
        .then((res) => {
          console.log(`Hoàn thành`);

          this.nameFileVideo = res?.name;
          this.showFileVideo = true;
          this.serverFileId = res.fileId;
          this.detailContentRequests.link = res?.link;

          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {

          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
    else {
      this._learnMessageService.failToast("Không đúng định dạng URL, mời bạn nhập lại");
      this.showFileVideo = false;
    }
    console.log('Change input file', e)
  }
  onCancelClick(): void {
    this.getRequest();
    if (this.detailContentRequests.name != (this.detailContentResponses.name ? this.detailContentResponses.name : '')
      || this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId
      || this.detailContentRequests.description != (this.detailContentResponses.description ? this.detailContentResponses.description : '')
      || this.detailContentRequests.linkId != (this.detailContentResponses.linkId ? this.detailContentResponses.linkId : null)
    ) {
      this.openConfirmDialog(Configuration.MESSAGE_EXIT_CONFIRM).subscribe((confirm) => {
        if (!confirm) {
          return;
        } else this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
      })
    } else this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);

  }
  onSaveClick(): void {
    this.spinner.show();
    if (isNullOrWhiteSpace(this.nameContent) || isNullOrEmpty(this.nameContent)) {
      this.spinner.hide();
      this._learnMessageService.failToast("Chưa nhập tên nội dung");
      return;
    }
    else {
      this.detailContentRequests.name = this.nameContent;
    }
    this.detailContentRequests.typesOfContent = "VID";
    if (this.conditionsForCompletion == null) {
      this.spinner.hide();
      this._learnMessageService.failToast("Chưa chọn điều kiện hoàn thành nội dung");
      return;
    }
    else {
      this.detailContentRequests.conditionsForCompletion = this.conditionsForCompletion;
      if (this.conditionsForCompletion == ConditionForCompletion.interval) {
        if (this.minutes < 0) {
          this._learnMessageService.failToast("Phút không được nhỏ hơn 0");
          this.spinner.hide();
          return;
        }
        if (this.seconds < 0) {
          this._learnMessageService.failToast("Giây không được nhỏ hơn 0");
          this.spinner.hide();
          return;
        }
        if (this.minutes == null) {
          this.spinner.hide();
          this._learnMessageService.failToast("Chưa nhập phút");
          return;
        }
        else {
          if (this.seconds == null) {
            this.spinner.hide();
            this._learnMessageService.failToast("Chưa nhập giây");
            return;
          }
          else {
            this.detailContentRequests.timeToComplete = this.minutes * 60 + this.seconds;
          }
        }
      } else if (this.conditionsForCompletion == ConditionForCompletion.answerQuestion) {
        this.detailContentRequests.listQuestion = this.questions;
        this.detailContentRequests.scoreAfterFinish = this.totalScore;
        this.detailContentRequests.totalScore = this.totalScore;
        this.detailContentRequests.totalEssayScore = 0;
        [...this.questionsShow].filter(x=>x.type == 'ESS').forEach(element => {
          this.detailContentRequests.totalEssayScore+= element.score;
        });
        this.detailContentRequests.totalQuizScore = this.totalScore - this.detailContentRequests.totalEssayScore;
      }
    }
    if (this.groupContentControl.value == undefined) {
      this.detailContentRequests.contentGroupId = null;
    }
    else {
      this.detailContentRequests.contentGroupId = this.groupContentControl.value.id;
    }
    if ((this.videoSource == 1)) {

      this.detailContentRequests.link = this.linkYoutubeVideo;
      this.detailContentRequests.fileId = 1;
      // this.detailContentRequests.name = 'VideoYouTube';
      this.createVideoContent();
    }
    else if (this.videoSource == 2) {
      this.detailContentRequests.fileId = 2
      this.detailContentRequests.linkId = this.serverFileId;
      this.detailContentRequests.fileName = this.nameFileVideo;
      this.createVideoContent();
    }
  }

  private createVideoContent() {
    this.detailContentRequests.description = this.contentCkEditor;
    if (JSON.stringify(this.detailContentResponses) === JSON.stringify(this.detailContentRequests)) {
      this.spinner.hide();
      this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
    } else {
      this.contentEndpoint.createContent(this.detailContentRequests).then((res) => {
        if (res) {
          this.detailContentResponses = { ...this.detailContentRequests };
          this._learnMessageService.successToast();
          this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
        } else {
          this._learnMessageService.failToast();
        }
      })
    }

  }

  onGroupContentChanged(grc: Option) {
    this.contentGroupId = grc.id;
    console.log('tttt', grc.name);
  }

  onChangeTypeVideoSource(videoSource: number): void {
    console.log("video src", videoSource);
  }
  //qsb

  someComplete(): boolean {
    return this.testService.someChecked(this.allComplete);
  }

  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.testService.checkAll(completed);
  }

  updateAllComplete(isChecked: boolean, question: QuestionTest) {
    this.allComplete = this.testService.isCheckAll();
    if (question.questionGroupId == undefined || question.questionGroupId == null && question.format == FormatQuestion.group) {
      this.testService.checkGroupQuestion(question, isChecked);
    } else {
      this.testService.checkQuestion(question, isChecked);
    }
  }

  deleteQuestion(question: QuestionTest): void {
    this.testService.deleteQuestion(question);
    if (this.question != undefined && this.question != null) {
      if (question.id == this.question.id) {
        this.currentQuestionId = 0;
        this.question = undefined;
      }
    }
  }

  deleteSelected(): void {
    if (!this.someComplete() && !this.allComplete) {
      this._learnMessageService.failToast("Không có câu hỏi nào được chọn!");
      return;
    }
    else {
      this.openConfirmDialog("Bạn có chắc chắn muốn xóa các câu hỏi đã chọn?").subscribe((isConfirmed) => {
        if (isConfirmed) {
          if (this.question != undefined && this.question != null) {
            let currentQuestion = this.questions.find(x => x.id == this.question.id);
            if (currentQuestion.isChecked) {
              this.currentQuestionId = 0;
              this.question = undefined;
            }
          }
          this.testService.deleteSelected();
        }
      });
    }
  }
  addQuestionFromFile() {
    this.fileQuestion.nativeElement.click();
  }

  //get file
  handle(event) {
    const file = event.target.files;
    if (file) {
      Promise.resolve()
        .then(() => {
          this.openProgressDialog(null);
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.questionConverterEndpoint.uploadTestFiles(file, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              var subscription = this.getprogressData().subscribe(value => {
                if (this.dialogRef && this.dialogRef.componentInstance) {
                  this.dialogRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          if (res) {
            let questions: QuestionTest[] = [];
            let result = res[0];
            let id = -1;
            console.log(res);
            result.questions.map((question) => {
              if (question.format == FormatQuestion.single) {
                questions.push({
                  id: id,
                  content: question.content,
                  level: question.level,
                  format: question.format,
                  questionGroupId: null,
                  isChecked: false,
                  score: question.score,
                  type: question.type,
                  unitId: question.unitId,
                  answers: question.answers,
                  action: Action.Add,
                  questions: []
                });
                id--;
              } else {
                let parentQuestion: QuestionTest = {
                  id: id,
                  content: question.content,
                  level: question.level,
                  format: question.format,
                  questionGroupId: null,
                  isChecked: false,
                  score: question.score,
                  type: question.type,
                  unitId: question.unitId,
                  answers: question.answers,
                  action: Action.Add,
                  questions: []
                };
                id--;
                let subQuestions: QuestionTest[] = [];
                question.questions.map((child) => {
                  subQuestions.push({
                    id: id,
                    content: child.content,
                    level: child.level,
                    format: child.format,
                    questionGroupId: parentQuestion.id,
                    isChecked: false,
                    score: child.score,
                    type: child.type,
                    unitId: child.unitId,
                    answers: child.answers,
                    action: Action.Add,
                    questions: []
                  });
                  id--;
                });
                parentQuestion.questions = subQuestions;
                questions.push(parentQuestion);
              }
            })
            this.testService.addListQuestion(questions);
            this.showListQuestion = true;
            this.fileQuestion.nativeElement.value = null;
          }
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogRef && this.dialogRef.componentInstance) {
              this.dialogRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err.response.data.errorDetail);
          this._learnMessageService.warToast(err.response.data.errorDetail);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }

  ngAfterViewInit(): void {
    this.contentGroupId = this.detailContentResponses.contentGroupId ?? null;
  }

  updateQuestion(qst: QuestionTest) {
    if (qst) {
      this._learnMessageService.successToast("Cập nhật thành công");
      this.testService.updateQuestion(qst, LessonStatus.Publish);
      this.currentQuestion = qst;
    } else {
      this._learnMessageService.failToast("Có lỗi xảy ra");
    }
  }
  chooseQuestion(questionSelected: QuestionTest, index?: number): void {
    if (index != undefined && this.childIndex == index + 1) {
      return
    }
    if (questionSelected.questionGroupId != null && questionSelected.questionGroupId != undefined) {
      let parent = this.questions.find(x => x.id == questionSelected.questionGroupId);
      this.question = JSON.parse(JSON.stringify(parent));
      this.currentQuestionId = parent.id;
    }
    else {
      this.question = JSON.parse(JSON.stringify(questionSelected));
      this.currentQuestionId = questionSelected.id;
    }
    this.isLoadDetailContent = true;
    if (index != null && index != undefined) {
      this.childIndex = index + 1;
    }
    else if (questionSelected.questionGroupId == null || questionSelected.questionGroupId == undefined) {
      this.childIndex = 1;
    }
  }
  previewTest(): void {
    //this._learnMessageService.warToast("Tính năng đang phát triển!");
    this.dialog.open(QuestionsOverviewComponent, {
      data: this.testService.beauty(this.questions),
      width: '80%',
      height: '95vh'
    })
  }
  /*
   * open progress dialog 
   */
  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.numbers }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  public onAddYoutubeHandleKeyup(val: any) {
    this.showYoutubeVideo = false;
    if (val) {
      let linkEmbed = this.getEmbedLink(val);
      if (linkEmbed) {
        this.linkYoutubeVideo = val;
        this.linkLoadVideo = linkEmbed
        this.showYoutubeVideo = true;
      }
    }
  }

  private getEmbedLink(link: string) {
    let linkEmbed = '';
    let linkYouTube = link.includes("embed");
    if (linkYouTube) {
      linkEmbed = link;
    } else {
      linkEmbed = link.split('v=')[1];
      if (linkEmbed) {
        var ampersandPosition = linkEmbed.indexOf('&');
        if (ampersandPosition != -1) {
          linkEmbed = linkEmbed.substring(0, ampersandPosition);
        }
        linkEmbed = `https://www.youtube.com/embed/${linkEmbed}`;
      } else {
        return '';
      }
    }

    return linkEmbed;
  }
  openUpdateScoreDialog(): void {
    if (!this.someComplete() && !this.allComplete) {
      this._learnMessageService.failToast("Không có câu hỏi nào được chọn!");
      return;
    }
    const dialogRef = this.dialog.open(DialogUpdateScoreComponent, {
      width: '377px',
      height: '234px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.setScoreQuestions(result);
      }
    });
  }
  setScoreQuestions(score: number): void {
    this.testService.updateScoreQuestions(score);
  }

  updateScore(id: number, score: number) {
    if (score == null) {
      this._learnMessageService.failToast("Điểm câu hỏi không được để trống!");
    }
    this.testService.updateScore(id, score);
  }
  updateScoreChild(parentId: number, childId: number, score: number) {
    if (score == null) {
      this._learnMessageService.failToast("Điểm câu hỏi không được để trống!");
    }
    this.testService.updateScoreChild(parentId, childId, score);
  }

  private openConfirmDialog(value: string): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: {
        message: `${value}`,
        buttonText: {
          ok: 'Có',
          cancel: 'Không'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }
  @HostListener('window:beforeunload', ['$event'])
  unloadHandler($event) {
    this.getRequest();
    if (this.detailContentRequests.name != (this.detailContentResponses.name ? this.detailContentResponses.name : '')
      || this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId
      || this.detailContentRequests.description != (this.detailContentResponses.description ? this.detailContentResponses.description : '')
      || this.detailContentRequests.linkId != (this.detailContentResponses.linkId ? this.detailContentResponses.linkId : null)
    ) return false
  }

  private getRequest() {
    this.detailContentRequests.name = this.nameContent ? this.nameContent : '';
    this.detailContentRequests.contentGroupId = this.groupContentControl?.value?.id ?? null;
    this.detailContentRequests.linkId = this.serverFileId;
    this.detailContentRequests.description = this.contentCkEditor ? this.contentCkEditor : '';
  }

  public getLinkTemplate(cases: number) {
    if (cases == 1) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-mau.docx`;
    }
    else if (cases == 2) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/huong-dan-su-dung-tep-tin-mau.docx`;
    }
  }
}

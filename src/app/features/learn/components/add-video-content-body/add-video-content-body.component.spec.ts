import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVideoContentBodyComponent } from './add-video-content-body.component';

describe('AddVideoContentBodyComponent', () => {
  let component: AddVideoContentBodyComponent;
  let fixture: ComponentFixture<AddVideoContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddVideoContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVideoContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

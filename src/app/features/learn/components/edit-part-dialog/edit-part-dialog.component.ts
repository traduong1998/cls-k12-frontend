import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../../intefaces/dialogData';
import { LearnMessageService } from '../../Services/learn-message-service.service';

@Component({
  selector: 'app-edit-part-dialog',
  templateUrl: './edit-part-dialog.component.html',
  styleUrls: ['./edit-part-dialog.component.scss']
})
export class EditPartDialogComponent implements OnInit {
  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT
  //#endregion

  //#region PRIVATE PROPERTY
  //#endregion

  //#region PUBLIC PROPERTY
  public valueDialog: string = '';
  public nameControl:FormControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(255)
  ])
  //#endregion



  //#region CONSTRUCTOR
  constructor(
    public dialogRef: MatDialogRef<EditPartDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _learnMessageService: LearnMessageService) {
    this.valueDialog = data?.name;
  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {

  }
  //#endregion

  //#region PUBLIC METHOD
  public onNoClick(): void {
    this.dialogRef.close();
  }

  public onEditPartHandleClick(): void {
    if (this.nameControl.invalid) {
      this.nameControl.markAllAsTouched();
    }

    if (!this.valueDialog) {
      this._learnMessageService.failToast(" Bạn chưa nhập vào tên tiết học ")
    }
    else {
      this.dialogRef.close(this.valueDialog);
    }
  }
  
  //#endregion

  //#region PRIVATE METHOD
  //#endregion






}

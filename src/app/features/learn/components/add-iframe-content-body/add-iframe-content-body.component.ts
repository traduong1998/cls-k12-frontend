import { ProgressDialogComponent } from './../progress-dialog/progress-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConditionForCompletion } from 'src/app/features/learn/intefaces/conditionForCompletion';
import { ContentGroupForCombobox } from './../../../../../../sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { Component, ElementRef, HostListener, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { Option } from '../../../../shared/interface/Option';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { DetailContentRequests } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/detailContentRequests';
import { ContentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentEndpoint';
import { DetailContentResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/detailContentResponses';
import { ActivatedRoute, Router } from '@angular/router';

//qsb
import { QuestionFileService } from './../../../../core/services/question-file/question-file.service';
import { Question } from 'sdk/cls-k12-sdk-js/src/services/question/models/Question';
import { UseCase } from '../../intefaces/useCase';
import { QuestionTest } from '../../intefaces/questionTest';
import { TestService } from '../../Services/test.service';
import { CKEditorComponent } from 'ckeditor4-angular';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';

//grc
import { Observable, of, ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatSelect } from '@angular/material/select';
import { ContentGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentGroupEndpoint';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { QuestionConverterEndpoint } from 'sdk/cls-k12-sdk-js/src/services/question-converter/endpoints/question-converter-endpoint';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { Action } from 'src/app/shared/modules/question-banks/Interfaces/action';
import { LessonStatus } from '../../intefaces/lessonStatus';
import { ConfigUploadCkeditor } from 'src/app/core/interfaces/app-config';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { CLS } from 'sdk/cls-k12-sdk-js/src';
import { DialogUpdateScoreComponent } from '../dialog-update-score/dialog-update-score.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { QuestionsOverviewComponent } from '../questions-overview/questions-overview.component';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { Configuration } from 'src/app/shared/configurations';

@Component({
  selector: 'learn-add-iframe-content-body',
  templateUrl: './add-iframe-content-body.component.html',
  styleUrls: ['./add-iframe-content-body.component.scss']
})
export class AddIframeContentBodyComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';

  @Input() lessonName: string;
  @Input() lessonId: number;
  @Input() contentId: number;
  @Input() useCase: number = UseCase.editting;
  group_content: ContentGroupForCombobox[] = [];
  linkIframe = "";
  showIframe = false;
  contentGroupId: number;
  nameContent: string;
  conditionsForCompletion: string;
  minutes: number;
  seconds: number;
  detailContentRequests: DetailContentRequests;
  detailContentResponses: DetailContentResponses;
  contentEndpoint: ContentEndpoint;
  detailContent: DetailContentRequests;
  questions: QuestionTest[];
  allComplete: boolean = false;
  showListQuestion = false;
  question: Question = null;
  contentGroupEndpoint: ContentGroupEndpoint;
  currentQuestion: QuestionTest;
  isLoadDetailContent: boolean = false;
  selectedRowIndex: number = -1;
  questionElement: QuestionTest | null;
  questionConverterEndpoint: QuestionConverterEndpoint;
  percent: number = 0;
  progressData: Observable<any>;
  questionsShow: QuestionTest[] = [];
  childIndex: number;
  configUpload: ConfigUploadCkeditor;
  totalScore: number;
  currentQuestionId: number;
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>;


  getprogressData(): Observable<any> {
    return of(this.progressData);
  }
  //grc
  public groupContentControl: FormControl = new FormControl();
  public groupContentFilterControl: FormControl = new FormControl();
  public filteredGroupContents: ReplaySubject<ContentGroupForCombobox[]> = new ReplaySubject<ContentGroupForCombobox[]>(0);
  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();
  @ViewChild("ck") ck: CKEditorComponent;
  @ViewChild("singleSelect") singleSelect: MatSelect;
  @ViewChild("fileQuestion") fileQuestion: ElementRef;
  nameContentControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(255),
  ]);

  minutesControl = new FormControl('', [
    Validators.required
  ]);

  secondsControl = new FormControl('', [
    Validators.required
  ]);

  get questionFormat(): typeof FormatQuestion {
    return FormatQuestion;
  }
  get questionLevel(): typeof Level {
    return Level;
  }
  get questionType(): typeof Type {
    return Type;
  }

  get actionEnum(): typeof Action {
    return Action;
  }


  //#region Thiết lập câu hỏi
  isSetTestTime: boolean = false;
  isWriteLog: boolean = true;
  isGetRandomQuestion: boolean = false;
  totalQuestion: number;
  testTimeControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$"),
    Validators.min(5)
  ]);
  allowToReDoControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$")
  ]);
  itemPerPageCtrl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$")
  ])
  //#endregion
  constructor(public dialog: MatDialog,
    private route: ActivatedRoute,
    private questionFilleService: QuestionFileService,
    private testService: TestService,
    private router: Router,
    private _learnMessageService: LearnMessageService,
    private _snackBar: MatSnackBar,
    private _authService: AuthService,
    private configService: AppConfigService,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.contentEndpoint = new ContentEndpoint();
    this.contentGroupEndpoint = new ContentGroupEndpoint();
    this.detailContentRequests = new DetailContentRequests;
    this.detailContentRequests.testInfo = {
      viewCorrectResultAfterSubmittingTheTest: false,
      numberOfQuestionOnThePage: 0,
      testTime: null,
      allowToRedo: false,
      theMaximumNumberToRedo: null,
      isRandomizeQuestions: false
    };
    this.detailContentResponses = new DetailContentResponses;
    this.detailContent = new DetailContentRequests;
    this.questionConverterEndpoint = new QuestionConverterEndpoint();
    this.minutes = 0;
    this.seconds = 0;
    this.lessonId = + this.route.snapshot.paramMap.get("id");
    this.detailContentRequests.lessonId = this.lessonId;
    this.contentGroupEndpoint.getListContentGroupForCombobox(this.lessonId).then((res) => {
      this.group_content = res;
      this.group_content.unshift({ id: 0, name: "Chọn tiết", order: -1 });
      this.filteredGroupContents.next(this.group_content);
    });
    this.testService.questions$.subscribe((questions) => {
      this.questions = JSON.parse(JSON.stringify(questions));
      this.questionsShow = [...this.questions.filter(x => x.action != Action.Delete)];
      this.questionsShow.map((qst) => {
        if (qst.format == FormatQuestion.group) {
          qst.questions = [...qst.questions.filter(x => x.action != Action.Delete)];
        }
      });
      this.itemPerPageCtrl = new FormControl('', [
        Validators.required,
        Validators.max(this.questionsShow.length)
      ])
      if (this.questionsShow.length > 0) {
        this.itemPerPageCtrl = new FormControl('', [
          Validators.min(1),
          Validators.required,
          Validators.max(this.questionsShow.length)
        ]);
      }
      this.detailContentRequests.testInfo.numberOfQuestionOnThePage = 0;
      this.detailContentRequests.testInfo.numberOfQuestionOnThePage = this.detailContentRequests.testInfo.numberOfQuestionOnThePage > this.questionsShow.length ?
      this.questionsShow.length : (this.detailContentRequests.testInfo.numberOfQuestionOnThePage == 0 && this.questionsShow.length > 5) ?
          5 : this.detailContentRequests.testInfo.numberOfQuestionOnThePage > 0 ?
            this.detailContentRequests.testInfo.numberOfQuestionOnThePage : this.questionsShow.length;
      this.itemPerPageCtrl.setValue(this.detailContentRequests.testInfo.numberOfQuestionOnThePage);
    });
    this.conditionsForCompletion = "BUT";

    let baseUrl = CLS.getConfig().apiBaseUrls.fileServer ?? CLS.getConfig().apiBaseUrl;
    baseUrl += '/api';
    this.configUpload = {
      portalId: configService.getConfig().unit.portalId,
      token: _authService.getToken(),
      uploadUrl: baseUrl + "/file/upload"
    }
    this.testService.totalScore$.subscribe((totalScore) => {
      this.totalScore = totalScore;
    });

    this.baseApiUrl = CLS.getConfig().apiBaseUrl;

  }

  ngOnInit(): void {
    // set initial selection
    this.groupContentControl.setValue(this.group_content[0]);
    // load the initial bank list
    this.filteredGroupContents.next(this.group_content.slice());
    // listen for search field value changes
    this.groupContentFilterControl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBanks();
      });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
    this.testService.clear();
  }
  changControlInput(): void {
    this.isSetTestTime ? this.testTimeControl.enable() : this.testTimeControl.disable();
    this.detailContentRequests.testInfo?.allowToRedo ? this.allowToReDoControl.enable() : this.allowToReDoControl.disable();
    if (!this.isSetTestTime) {
      this.testTimeControl.reset();
      this.detailContentRequests.testInfo.testTime = null;
    } else {
      this.testTimeControl.markAsTouched();
    }
    if (!this.detailContentRequests.testInfo?.allowToRedo) {
      this.allowToReDoControl.reset();
      this.detailContentRequests.testInfo.theMaximumNumberToRedo = null;
    }
    else {
      this.allowToReDoControl.markAsTouched();
    }
    //this.isGetRandomQuestion ? this.questionsRandomCtrl.enable() : this.questionsRandomCtrl.disable();
  }

  onGroupContentChanged(grc: Option) {
    this.contentGroupId = grc.id;
  }
  protected filterBanks() {
    if (!this.group_content) {
      return;
    }
    // get the search keyword
    let search = this.groupContentFilterControl.value;
    if (!search) {
      this.filteredGroupContents.next(this.group_content.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredGroupContents.next(
      this.group_content.filter(bank => bank.name.toLowerCase().indexOf(search) > -1)
    );
  }
  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
  }

  addIframe(link: string) {
    if (this.validURL(link)) {
      this.showIframe = true;
      this.linkIframe = link;
      this.detailContentRequests.link = this.linkIframe;
    }
    else {
      alert("Không đúng định dạng URL, mời bạn nhập lại");
      this.showIframe = false;
    }
  }
  onCancelClick(): void {
    this.getRequest();
    if (this.detailContentRequests.name != (this.detailContentResponses.name ? this.detailContentResponses.name : '')
      || this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId
      || this.detailContentRequests.link != (this.detailContentResponses.link ? this.detailContentResponses.link : '')
    ) {
      this.openConfirmDialog(Configuration.MESSAGE_EXIT_CONFIRM).subscribe((confirm) => {
        if (!confirm) {
          return;
        } this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
      })
    } else this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
  }
  onSaveClick(): void {
    this.spinner.show();
    if (isNullOrWhiteSpace(this.nameContent) || isNullOrEmpty(this.nameContent)) {
      this.spinner.hide();
      this._learnMessageService.failToast(" Bạn chưa nhập nội dung");
      return;
    }
    else {
      this.detailContentRequests.name = this.nameContent;
    }

    this.detailContentRequests.typesOfContent = "IFR";

    if (this.conditionsForCompletion == null) {
      this.spinner.hide();
      this._learnMessageService.failToast(" Chưa chọn điều kiện hoàn thành nội dung ");
      return;
    }
    else {
      this.detailContentRequests.conditionsForCompletion = this.conditionsForCompletion;
      if (this.conditionsForCompletion == 'INT') {
        if (this.minutes == null) {
          this.spinner.hide();
          this._learnMessageService.failToast(" Chưa nhập phút ");
          return;
        }
        else {
          if (this.seconds == null) {
            this.spinner.hide();
            this._learnMessageService.failToast(" Chưa nhập giây ");
            return;
          }
          else {
            this.detailContentRequests.timeToComplete = this.minutes * 60 + this.seconds;
          }
        }
      } else {
        if (this.conditionsForCompletion == ConditionForCompletion.answerQuestion) {
          this.detailContentRequests.listQuestion = this.questions;
          this.detailContentRequests.scoreAfterFinish = this.totalScore;
          this.detailContentRequests.totalScore = this.totalScore;
          this.detailContentRequests.totalEssayScore = 0;
          [...this.questionsShow].filter(x=>x.type == 'ESS').forEach(element => {
            this.detailContentRequests.totalEssayScore+= element.score;
          });
          this.detailContentRequests.totalQuizScore = this.totalScore - this.detailContentRequests.totalEssayScore;
        }
      }
    }
    this.detailContentRequests.contentGroupId = this.groupContentControl?.value?.id ?? null;
    this.contentEndpoint.createContent(this.detailContentRequests).then((res) => {
      if (res) {
        this.spinner.hide();
        this._learnMessageService.successToast();
        this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
      } else {
        this.spinner.hide();
        this._learnMessageService.failToast();
      }
    })
  }
  //qsb

  someComplete(): boolean {
    return this.testService.someChecked(this.allComplete);
  }

  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.testService.checkAll(completed);
  }

  updateAllComplete(isChecked: boolean, question: QuestionTest) {
    this.allComplete = this.testService.isCheckAll();
    if (question.questionGroupId == undefined || question.questionGroupId == null && question.format == FormatQuestion.group) {
      this.testService.checkGroupQuestion(question, isChecked);
    } else {
      this.testService.checkQuestion(question, isChecked);
    }
  }

  deleteQuestion(question: QuestionTest): void {
    this.testService.deleteQuestion(question);
    if (this.question != undefined && this.question != null) {
      if (question.id == this.question.id) {
        this.currentQuestionId = 0;
        this.question = undefined;
      }
    }
  }

  deleteSelected(): void {
    if (!this.someComplete() && !this.allComplete) {
      this._learnMessageService.failToast("Không có câu hỏi nào được chọn!");
      return;
    }
    else {
      this.openConfirmDialog("Bạn có chắc chắn muốn xóa các câu hỏi đã chọn?").subscribe((isConfirmed) => {
        if (isConfirmed) {
          if (this.question != undefined && this.question != null) {
            let currentQuestion = this.questions.find(x => x.id == this.question.id);
            if (currentQuestion.isChecked) {
              this.currentQuestionId = 0;
              this.question = undefined;
            }
          }
          this.testService.deleteSelected();
        }
      });
    }
  }

  addQuestionFromFile() {
    this.fileQuestion.nativeElement.click();
  }

  //get file
  handle(event) {
    const file = event.target.files;
    if (file) {
      Promise.resolve()
        .then(() => {
          this.openProgressDialog(null);
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.questionConverterEndpoint.uploadTestFiles(file, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              var subscription = this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          if (res) {
            let questions: QuestionTest[] = [];
            let result = res[0];
            let id = -1;
            console.log(res);
            result.questions.map((question) => {
              if (question.format == FormatQuestion.single) {
                questions.push({
                  id: id,
                  content: question.content,
                  level: question.level,
                  format: question.format,
                  questionGroupId: null,
                  isChecked: false,
                  score: question.score,
                  type: question.type,
                  unitId: question.unitId,
                  answers: question.answers,
                  action: Action.Add,
                  questions: []
                });
                id--;
              } else {
                let parentQuestion: QuestionTest = {
                  id: id,
                  content: question.content,
                  level: question.level,
                  format: question.format,
                  questionGroupId: null,
                  isChecked: false,
                  score: question.score,
                  type: question.type,
                  unitId: question.unitId,
                  answers: question.answers,
                  action: Action.Add,
                  questions: []
                };
                id--;
                let subQuestions: QuestionTest[] = [];
                question.questions.map((child) => {
                  subQuestions.push({
                    id: id,
                    content: child.content,
                    level: child.level,
                    format: child.format,
                    questionGroupId: parentQuestion.id,
                    isChecked: false,
                    score: child.score,
                    type: child.type,
                    unitId: child.unitId,
                    answers: child.answers,
                    action: Action.Add,
                    questions: []
                  });
                  id--;
                });
                parentQuestion.questions = subQuestions;
                questions.push(parentQuestion);
              }
            })
            this.testService.addListQuestion(questions);
            this.showListQuestion = true;
            this.fileQuestion.nativeElement.value = null;
          }
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err.response.data.errorDetail);
          this._learnMessageService.warToast(err.response.data.errorDetail);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }

  ngAfterViewInit(): void {
    this.contentGroupId = this.detailContentResponses.contentGroupId ?? null;
  }

  updateQuestion(qst: QuestionTest) {
    if (qst) {
      this._learnMessageService.successToast("Cập nhật thành công");
      this.testService.updateQuestion(qst, LessonStatus.Publish);
      this.currentQuestion = qst;
    } else {
      this._learnMessageService.failToast("Có lỗi xảy ra");
    }
  }

  chooseQuestion(questionSelected: QuestionTest, index?: number): void {
    if (index != undefined && this.childIndex == index + 1) {
      return
    }
    if (questionSelected.questionGroupId != null && questionSelected.questionGroupId != undefined) {
      let parent = this.questions.find(x => x.id == questionSelected.questionGroupId);
      this.question = JSON.parse(JSON.stringify(parent));
      this.currentQuestionId = parent.id;
    }
    else {
      this.question = JSON.parse(JSON.stringify(questionSelected));
      this.currentQuestionId = questionSelected.id;
    }
    this.isLoadDetailContent = true;
    if (index != null && index != undefined) {
      this.childIndex = index + 1;
    }
    else if (questionSelected.questionGroupId == null || questionSelected.questionGroupId == undefined) {
      this.childIndex = 1;
    }
  }

  previewTest(): void {
    //this._learnMessageService.warToast("Tính năng đang phát triển!");
    this.dialog.open(QuestionsOverviewComponent, {
      data: this.testService.beauty(this.questions),
      width: '80%',
      height: '95vh'
    })
  }
  highlight(row) {

    this.selectedRowIndex = row.id;
    this.questionElement = this.questionElement === row ? null : row
  }
  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.percent }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  openUpdateScoreDialog(): void {
    if (!this.someComplete() && !this.allComplete) {
      this._learnMessageService.failToast("Không có câu hỏi nào được chọn!");
      return;
    }
    const dialogRef = this.dialog.open(DialogUpdateScoreComponent, {
      width: '377px',
      height: '234px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.setScoreQuestions(result);
      }
    });
  }
  setScoreQuestions(score: number): void {
    this.testService.updateScoreQuestions(score);
  }
  updateScore(id: number, score: number) {
    if (score == null) {
      this._learnMessageService.failToast("Điểm câu hỏi không được để trống!");
    }
    this.testService.updateScore(id, score);
  }
  updateScoreChild(parentId: number, childId: number, score: number) {
    if (score == null) {
      this._learnMessageService.failToast("Điểm câu hỏi không được để trống!");
    }
    this.testService.updateScoreChild(parentId, childId, score);
  }

  private openConfirmDialog(value: string): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: {
        message: `${value}`,
        buttonText: {
          ok: 'Có',
          cancel: 'Không'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }

  @HostListener('window:beforeunload', ['$event'])
  unloadHandler($event) {
    this.getRequest();
    if (this.detailContentRequests.name != (this.detailContentResponses.name ? this.detailContentResponses.name : '')
      || this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId
      || this.detailContentRequests.link != (this.detailContentResponses.link ? this.detailContentResponses.link : '')
    ) return false
  }

  private getRequest() {
    this.detailContentRequests.name = this.nameContent ? this.nameContent : '';
    this.detailContentRequests.contentGroupId = this.groupContentControl?.value?.id ?? null;
    this.detailContentRequests.link = this.linkIframe ? this.linkIframe : '';
  }

  public getLinkTemplate(cases: number) {
    if (cases == 1) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-mau.docx`;
    }
    else if (cases == 2) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/huong-dan-su-dung-tep-tin-mau.docx`;
    }
  }
}

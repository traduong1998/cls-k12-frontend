import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIframeContentBodyComponent } from './add-iframe-content-body.component';

describe('AddIframeContentBodyComponent', () => {
  let component: AddIframeContentBodyComponent;
  let fixture: ComponentFixture<AddIframeContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddIframeContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIframeContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

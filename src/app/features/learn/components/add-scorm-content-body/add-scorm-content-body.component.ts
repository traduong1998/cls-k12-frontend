import { LessonStatus } from './../../intefaces/lessonStatus';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ContentGroupForCombobox } from './../../../../../../sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { Component, ElementRef, HostListener, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { Option } from '../../../../shared/interface/Option';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { DetailContentRequests } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/detailContentRequests';
import { ContentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentEndpoint';
import { DetailContentResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/detailContentResponses';
import { ActivatedRoute, Router } from '@angular/router';
//qsb
import { QuestionFileService } from './../../../../core/services/question-file/question-file.service';
import { Question } from 'sdk/cls-k12-sdk-js/src/services/question/models/Question';
import { Observable, of } from 'rxjs';
import { QuestionTest } from '../../intefaces/questionTest';
import { TestService } from '../../Services/test.service';
import { CKEditorComponent } from 'ckeditor4-angular';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';

//grc
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { MatSelect } from '@angular/material/select';
import { ContentGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentGroupEndpoint';
import { ContentGroupResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { QuestionConverterEndpoint } from 'sdk/cls-k12-sdk-js/src/services/question-converter/endpoints/question-converter-endpoint';
import { Action } from 'src/app/shared/modules/question-banks/Interfaces/action';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { ConfigUploadCkeditor } from 'src/app/core/interfaces/app-config';
import { CLS } from 'sdk/cls-k12-sdk-js/src';
import { DialogUpdateScoreComponent } from '../dialog-update-score/dialog-update-score.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { QuestionsOverviewComponent } from '../questions-overview/questions-overview.component';
import { Configuration } from 'src/app/shared/configurations';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';


@Component({
  selector: 'learn-add-scorm-content-body',
  templateUrl: './add-scorm-content-body.component.html',
  styleUrls: ['./add-scorm-content-body.component.scss']
})
export class AddScormContentBodyComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';

  @Input() lessonName: string;
  progressData: Observable<any>;
  toolTip: string = Configuration.MESSAGE_INFOR_SCORM;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }
  numbers: 0;
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>

  @Input() lessonId: number;
  @Input() contentId: number;
  nameFileScorm: "";
  showScorm = false;
  group_content: ContentGroupForCombobox[] = [];
  contentGroupId: number;
  nameContent: string;
  conditionsForCompletion: string;
  scormFile: File;
  scormLink: string;
  fileServer: FileServerEndpoint;
  minutes: number = 0;
  seconds: number = 0;
  detailContentRequests: DetailContentRequests;
  detailContentResponses: DetailContentResponses;
  contentEndpoint: ContentEndpoint;
  contentGroupEndpoint: ContentGroupEndpoint;
  detailContent: DetailContentRequests;
  allComplete: boolean = false;
  showListQuestion = false;
  question: Question = null;
  isLoadDetailContent: boolean = false;
  currentQuestion: QuestionTest;
  questions: QuestionTest[];
  questionConverterEndpoint: QuestionConverterEndpoint;
  childIndex: number;
  questionsShow: QuestionTest[] = [];
  configUpload: ConfigUploadCkeditor;
  totalScore: number;
  currentQuestionId: number;
  //grc
  public groupContentControl: FormControl = new FormControl();
  public groupContentFilterControl: FormControl = new FormControl();
  public filteredGroupContents: ReplaySubject<ContentGroupForCombobox[]> = new ReplaySubject<ContentGroupForCombobox[]>(0);

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();
  @ViewChild("file") file: ElementRef;
  @ViewChild("ck") ck: CKEditorComponent;
  @ViewChild("singleSelect") singleSelect: MatSelect;
  @ViewChild("fileQuestion") fileQuestion: ElementRef;
  nameContentControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(255),
  ]);

  minutesControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
  ]);

  secondsControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
  ]);

  onGroupContentChanged(grc: Option) {
    this.contentGroupId = grc.id;
    console.log('tttt', grc.name);
  }

  get questionFormat(): typeof FormatQuestion {
    return FormatQuestion;
  }
  get questionLevel(): typeof Level {
    return Level;
  }
  get questionType(): typeof Type {
    return Type;
  }
  get actionEnum(): typeof Action {
    return Action;
  }

  //#region Thiết lập câu hỏi
  isSetTestTime: boolean = false;
  isWriteLog: boolean = true;
  isGetRandomQuestion: boolean = false;
  totalQuestion: number;
  testTimeControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$"),
    Validators.min(5)
  ]);
  allowToReDoControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$")
  ]);
  itemPerPageCtrl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$")
  ])
  //#endregion

  constructor(public dialog: MatDialog,
    private route: ActivatedRoute,
    private questionFilleService: QuestionFileService,
    private testService: TestService,
    private router: Router,
    private _learnMessageService: LearnMessageService,
    private _snackBar: MatSnackBar,
    private _authService: AuthService,
    private configService: AppConfigService,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.contentEndpoint = new ContentEndpoint();
    this.detailContentRequests = new DetailContentRequests;
    this.detailContentRequests.testInfo = {
      viewCorrectResultAfterSubmittingTheTest: false,
      numberOfQuestionOnThePage: 0,
      testTime: null,
      allowToRedo: false,
      theMaximumNumberToRedo: null,
      isRandomizeQuestions: false
    };
    this.detailContentResponses = new DetailContentResponses;
    this.detailContent = new DetailContentRequests;
    this.lessonId = + this.route.snapshot.paramMap.get("id");
    this.detailContentRequests.lessonId = this.lessonId;
    this.contentGroupEndpoint = new ContentGroupEndpoint();
    this.questionConverterEndpoint = new QuestionConverterEndpoint();
    this.fileServer = new FileServerEndpoint();
    this.contentGroupEndpoint.getListContentGroupForCombobox(this.lessonId).then((res) => {
      this.group_content = res;
      this.group_content.unshift({ id: 0, name: "Chọn tiết", order: -1 });
      this.filteredGroupContents.next(this.group_content);
    });
    this.testService.questions$.subscribe((questions) => {
      this.questions = JSON.parse(JSON.stringify(questions));
      this.questionsShow = [...this.questions.filter(x => x.action != Action.Delete)];
      this.questionsShow.map((qst) => {
        if (qst.format == FormatQuestion.group) {
          qst.questions = [...qst.questions.filter(x => x.action != Action.Delete)];
        }
      });

      this.itemPerPageCtrl = new FormControl('', [
        Validators.required,
        Validators.max(this.questionsShow.length)
      ])
      if (this.questionsShow.length > 0) {
        this.itemPerPageCtrl = new FormControl('', [
          Validators.min(1),
          Validators.required,
          Validators.max(this.questionsShow.length)
        ]);
      }
      this.detailContentRequests.testInfo.numberOfQuestionOnThePage = 0;
      this.detailContentRequests.testInfo.numberOfQuestionOnThePage = this.detailContentRequests.testInfo.numberOfQuestionOnThePage > this.questionsShow.length ?
      this.questionsShow.length : (this.detailContentRequests.testInfo.numberOfQuestionOnThePage == 0 && this.questionsShow.length > 5) ?
          5 : this.detailContentRequests.testInfo.numberOfQuestionOnThePage > 0 ?
            this.detailContentRequests.testInfo.numberOfQuestionOnThePage : this.questionsShow.length;
      this.itemPerPageCtrl.setValue(this.detailContentRequests.testInfo.numberOfQuestionOnThePage);
    });
    this.conditionsForCompletion = "BUT";

    let baseUrl = CLS.getConfig().apiBaseUrls.fileServer ?? CLS.getConfig().apiBaseUrl;
    baseUrl += '/api';
    this.configUpload = {
      portalId: configService.getConfig().unit.portalId,
      token: _authService.getToken(),
      uploadUrl: baseUrl + "/file/upload"
    }
    this.testService.totalScore$.subscribe((totalScore) => {
      this.totalScore = totalScore;
    });

    this.baseApiUrl = CLS.getConfig().apiBaseUrl;

  }

  ngOnInit(): void {
    // set initial selection
    this.groupContentControl.setValue(this.group_content[-1]);
    // load the initial bank list
    this.filteredGroupContents.next(this.group_content.slice());
    // listen for search field value changes
    this.groupContentFilterControl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBanks();
      });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
    this.testService.clear();
  }

  changControlInput(): void {
    this.isSetTestTime ? this.testTimeControl.enable() : this.testTimeControl.disable();
    this.detailContentRequests.testInfo?.allowToRedo ? this.allowToReDoControl.enable() : this.allowToReDoControl.disable();
    if (!this.isSetTestTime) {
      this.testTimeControl.reset();
      this.detailContentRequests.testInfo.testTime = null;
    } else {
      this.testTimeControl.markAsTouched();
    }
    if (!this.detailContentRequests.testInfo?.allowToRedo) {
      this.allowToReDoControl.reset();
      this.detailContentRequests.testInfo.theMaximumNumberToRedo = null;
    }
    else {
      this.allowToReDoControl.markAsTouched();
    }
    //this.isGetRandomQuestion ? this.questionsRandomCtrl.enable() : this.questionsRandomCtrl.disable();
  }

  protected filterBanks() {
    if (!this.group_content) {
      return;
    }
    // get the search keyword
    let search = this.groupContentFilterControl.value;
    if (!search) {
      this.filteredGroupContents.next(this.group_content.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredGroupContents.next(
      this.group_content.filter(bank => bank.name.toLowerCase().indexOf(search) > -1)
    );
  }

  addFromFile() {
    this.file.nativeElement.click();
  }

  //get file
  uploadFileScorm(e) {
    const file = e.target.files[0];
    if (!file) {
      return
    }

    // Kiểm tra limit file
    let confirmLimit = Configuration.getLimitFileMessage(file);
    if (!confirmLimit.status) {
      this._learnMessageService.failToast(confirmLimit.message);
      return;
    }
    var type = file.name.substring(file.name.lastIndexOf(".") + 1).toLowerCase();
    if (["zip"].includes(type)) {
      this.showScorm = false;
      this.nameFileScorm = file.name;
      this.scormFile = file;
      Promise.resolve()
        .then(() => {
          this.openProgressDialog(null);
          //gửi video
          return this.fileServer.uploadFile({
            file: this.scormFile,
            isPrivate: false,
            moduleName: ServiceName.LEARN
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          });
        })
        .then((res) => {
          this.showScorm = true;
          this.detailContentRequests.link = res?.link;
          this.detailContentRequests.fileName = this.nameFileScorm;
          this.scormLink = res?.link;
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          if (err) {
            this._learnMessageService.failToast(err.errorDetail);
          } else {
            this._learnMessageService.failToast(" Có lỗi xảy ra ! Hãy thử lại ");
          }
          this.nameFileScorm = '';
          this.detailContentRequests.fileName = '';
          this.showScorm = false;
          this.detailContentRequests.link = '';
          this.scormLink = '';

          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });

    }
    else {
      this._learnMessageService.failToast("Không đúng định dạng file, mời bạn nhập lại ")
      this.showScorm = false;
    }
  }

  onCancelClick(): void {
    this.getRequest();
    if (this.detailContentRequests.name != (this.detailContentResponses.name ? this.detailContentResponses.name : '')
      || this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId
      || this.detailContentRequests.link != (this.detailContentResponses.link ? this.detailContentResponses.link : '')
    ) {
      this.openConfirmDialog(Configuration.MESSAGE_EXIT_CONFIRM).subscribe((confirm) => {
        if (!confirm) {
          return;
        } this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
      })
    } else this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);

  }

  onSaveClick(): void {
    this.spinner.show();
    if (isNullOrWhiteSpace(this.nameContent) || isNullOrEmpty(this.nameContent)) {
      this.spinner.hide();
      this._learnMessageService.failToast('Chưa nhập tên nội dung');
      return;
    }
    else {
      this.detailContentRequests.name = this.nameContent;
    }

    this.detailContentRequests.typesOfContent = "SCO";

    if (this.conditionsForCompletion == null) {
      this.spinner.hide();
      this._learnMessageService.failToast('Chưa chọn điều kiện hoàn thành nội dung');
      return;
    }
    else {
      this.detailContentRequests.conditionsForCompletion = this.conditionsForCompletion;
      if (this.conditionsForCompletion == 'INT') {
        this.spinner.hide();
        if (this.minutes < 0) {
          this._learnMessageService.failToast('Phút không được nhỏ hơn 0');
          return;
        }
        if (this.seconds < 0) {
          this._learnMessageService.failToast('Giây không được nhỏ hơn 0');
          return;
        }
        if (this.minutes == null) {
          this._learnMessageService.failToast('Chưa nhập phút');
          return;
        }
        else {
          if (this.seconds == null) {
            this._learnMessageService.failToast('Chưa nhập giây');
            return;
          }
          else {
            this.detailContentRequests.timeToComplete = this.minutes * 60 + this.seconds;
          }
        }
      } else if (this.conditionsForCompletion == 'ANS') {
        this.detailContentRequests.listQuestion = this.questions;
        this.detailContentRequests.scoreAfterFinish = this.totalScore;
        this.detailContentRequests.totalScore = this.totalScore;
        this.detailContentRequests.totalEssayScore = 0;
        [...this.questionsShow].filter(x=>x.type == 'ESS').forEach(element => {
          this.detailContentRequests.totalEssayScore+= element.score;
        });
        this.detailContentRequests.totalQuizScore = this.totalScore - this.detailContentRequests.totalEssayScore;
      }
    }

    if (this.groupContentControl.value == undefined) {
      this.detailContentRequests.contentGroupId = null;
    }
    else {
      this.detailContentRequests.contentGroupId = this.groupContentControl.value.id;
    }

    if (JSON.stringify(this.detailContentResponses) === JSON.stringify(this.detailContentRequests)) {
      this.spinner.hide();
      this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
    }

    this.contentEndpoint.createContent(this.detailContentRequests).then((res) => {
      if (res) {
        this.detailContentResponses = { ...this.detailContentRequests };
        this.spinner.hide();
        this._learnMessageService.successToast();
        this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
      } else {
        this.spinner.hide();
        this._learnMessageService.failToast();
      }
    })
  }

  someComplete(): boolean {
    return this.testService.someChecked(this.allComplete);
  }

  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.testService.checkAll(completed);
  }

  updateAllComplete(isChecked: boolean, question: QuestionTest) {
    this.allComplete = this.testService.isCheckAll();
    if (question.questionGroupId == undefined || question.questionGroupId == null && question.format == FormatQuestion.group) {
      this.testService.checkGroupQuestion(question, isChecked);
    } else {
      this.testService.checkQuestion(question, isChecked);
    }
  }

  deleteQuestion(question: QuestionTest): void {
    this.testService.deleteQuestion(question);
    if (this.question != undefined && this.question != null) {
      if (question.id == this.question.id) {
        this.currentQuestionId = 0;
        this.question = undefined;
      }
    }
  }

  deleteSelected(): void {
    if (!this.someComplete() && !this.allComplete) {
      this._learnMessageService.failToast("Không có câu hỏi nào được chọn!");
      return;
    }
    else {
      this.openConfirmDialog("Bạn có chắc chắn muốn xóa các câu hỏi đã chọn?").subscribe((isConfirmed) => {
        if (isConfirmed) {
          if (this.question != undefined && this.question != null) {
            let currentQuestion = this.questions.find(x => x.id == this.question.id);
            if (currentQuestion.isChecked) {
              this.currentQuestionId = 0;
              this.question = undefined;
            }
          }
          this.testService.deleteSelected();
        }
      });
    }
  }

  addQuestionFromFile() {
    this.fileQuestion.nativeElement.click();
  }

  //get file
  handle(event) {
    const file = event.target.files;
    if (file) {
      Promise.resolve()
        .then(() => {
          this.openProgressDialog(null);
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.questionConverterEndpoint.uploadTestFiles(file, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              var subscription = this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          if (res) {
            let questions: QuestionTest[] = [];
            let result = res[0];
            let id = -1;
            console.log(res);
            result.questions.map((question) => {
              if (question.format == FormatQuestion.single) {
                questions.push({
                  id: id,
                  content: question.content,
                  level: question.level,
                  format: question.format,
                  questionGroupId: null,
                  isChecked: false,
                  score: question.score,
                  type: question.type,
                  unitId: question.unitId,
                  answers: question.answers,
                  action: Action.Add,
                  questions: []
                });
                id--;
              } else {
                let parentQuestion: QuestionTest = {
                  id: id,
                  content: question.content,
                  level: question.level,
                  format: question.format,
                  questionGroupId: null,
                  isChecked: false,
                  score: question.score,
                  type: question.type,
                  unitId: question.unitId,
                  answers: question.answers,
                  action: Action.Add,
                  questions: []
                };
                id--;
                let subQuestions: QuestionTest[] = [];
                question.questions.map((child) => {
                  subQuestions.push({
                    id: id,
                    content: child.content,
                    level: child.level,
                    format: child.format,
                    questionGroupId: parentQuestion.id,
                    isChecked: false,
                    score: child.score,
                    type: child.type,
                    unitId: child.unitId,
                    answers: child.answers,
                    action: Action.Add,
                    questions: []
                  });
                  id--;
                });
                parentQuestion.questions = subQuestions;
                questions.push(parentQuestion);
              }
            })
            this.testService.addListQuestion(questions);
            this.showListQuestion = true;
            this.fileQuestion.nativeElement.value = null;
          }
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err.response.data.errorDetail);
          this._learnMessageService.warToast(err.response.data.errorDetail);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }
  /**
       * Sets the initial value after the filteredBanks are loaded initially
       */
  protected setInitialValue() {
    this.filteredGroupContents
      .pipe(
        take(1),
        takeUntil(this._onDestroy)
      )
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        // triggers initializing the selection according to the initial value of
        // the form control (i.e. _initializeSelection())
        // this needs to be done after the filteredBanks are loaded initially
        // and after the mat-option elements are available
        this.singleSelect.compareWith = (a: ContentGroupResponses, b: ContentGroupResponses) =>
          a && b && a.id === b.id;
      });
  }
  ngAfterViewInit(): void {

    this.contentGroupId = this.detailContentResponses.contentGroupId ?? null;
    //load data question
    this.setInitialValue();
  }

  updateQuestion(qst: QuestionTest) {
    if (qst) {
      this._learnMessageService.successToast("Cập nhật thành công");
      this.testService.updateQuestion(qst, LessonStatus.Publish);
      this.currentQuestion = qst;
    } else {
      this._learnMessageService.failToast("Có lỗi xảy ra");
    }
  }

  chooseQuestion(questionSelected: QuestionTest, index?: number): void {
    if (index != undefined && this.childIndex == index + 1) {
      return
    }
    if (questionSelected.questionGroupId != null && questionSelected.questionGroupId != undefined) {
      let parent = this.questions.find(x => x.id == questionSelected.questionGroupId);
      this.question = JSON.parse(JSON.stringify(parent));
      this.currentQuestionId = parent.id;
    }
    else {
      this.question = JSON.parse(JSON.stringify(questionSelected));
      this.currentQuestionId = questionSelected.id;
    }
    this.isLoadDetailContent = true;
    if (index != null && index != undefined) {
      this.childIndex = index + 1;
    }
    else if (questionSelected.questionGroupId == null || questionSelected.questionGroupId == undefined) {
      this.childIndex = 1;
    }
  }
  previewTest(): void {
    //this._learnMessageService.warToast("Tính năng đang phát triển!");
    this.dialog.open(QuestionsOverviewComponent, {
      data: this.testService.beauty(this.questions),
      width: '80%',
      height: '95vh'
    })
  }
  /*
   * open progress dialog 
   */
  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.numbers }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  openUpdateScoreDialog(): void {
    if (!this.someComplete() && !this.allComplete) {
      this._learnMessageService.failToast("Không có câu hỏi nào được chọn!");
      return;
    }
    const dialogRef = this.dialog.open(DialogUpdateScoreComponent, {
      width: '377px',
      height: '234px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.setScoreQuestions(result);
      }
    });
  }
  setScoreQuestions(score: number): void {
    this.testService.updateScoreQuestions(score);
  }

  updateScore(id: number, score: number) {
    if (score == null) {
      this._learnMessageService.failToast("Điểm câu hỏi không được để trống!");
    }
    this.testService.updateScore(id, score);
  }
  updateScoreChild(parentId: number, childId: number, score: number) {
    if (score == null) {
      this._learnMessageService.failToast("Điểm câu hỏi không được để trống!");
    }
    this.testService.updateScoreChild(parentId, childId, score);
  }

  private openConfirmDialog(value: string): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: {
        message: `${value}`,
        buttonText: {
          ok: 'Có',
          cancel: 'Không'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }

  @HostListener('window:beforeunload', ['$event'])
  unloadHandler($event) {
    this.getRequest();
    if (this.detailContentRequests.name != (this.detailContentResponses.name ? this.detailContentResponses.name : '')
      || this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId
      || this.detailContentRequests.link != (this.detailContentResponses.link ? this.detailContentResponses.link : '')
    ) return false
  }

  private getRequest() {
    this.detailContentRequests.name = this.nameContent ? this.nameContent : '';
    this.detailContentRequests.contentGroupId = this.groupContentControl?.value?.id ?? null;
    this.detailContentRequests.link = this.scormLink ? this.scormLink : '';
  }

  public getLinkTemplate(cases: number) {
    if (cases == 1) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-mau.docx`;
    }
    else if (cases == 2) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/huong-dan-su-dung-tep-tin-mau.docx`;
    }
  }
}

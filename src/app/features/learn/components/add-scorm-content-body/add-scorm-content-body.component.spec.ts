import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddScormContentBodyComponent } from './add-scorm-content-body.component';

describe('AddScormContentBodyComponent', () => {
  let component: AddScormContentBodyComponent;
  let fixture: ComponentFixture<AddScormContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddScormContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddScormContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { FormControl, Validators } from '@angular/forms';
import { LearnMessageService } from './../../Services/learn-message-service.service';
import { Observable, Subscription } from 'rxjs';
import { UseCase } from './../../intefaces/useCase';
import { TestConfigResponses } from './../../../../../../sdk/cls-k12-sdk-js/src/services/lesson/responses/testConfigResponses';
import { Component, Input, OnInit, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { TestService } from '../../Services/test.service';

@Component({
  selector: 'learn-setting-add-test',
  templateUrl: './setting-add-test.component.html',
  styleUrls: ['./setting-add-test.component.scss']
})
export class SettingAddTestComponent implements OnInit, OnChanges {
  @Input() currentTabIndex: number;
  //Trường hợp sử dụng (tạo mới / sửa nội dung)
  @Input() useCase: number;
  //thiết lập BKT nhận từ view tab-add-test
  @Input() testConfigResponse?: TestConfigResponses;
  // Observable lắng nghe sự kiện click save ngoài view chính
  @Input() submitEvent: Observable<void>;
  @Output() invalidConfig = new EventEmitter<boolean>();
  subcription: Subscription;
  //Model clone đổ lên view để lúc submit so sánh với model cũ xem có thay dổi không
  testConfigModel: TestConfigResponses;
  //Check dữ liệu load xong hay chưa nè
  isReady: boolean = false;
  //Vài thuộc tính để gán ngModel cho các checkBox trên view dựa theo modelConfig nhận vào
  isSetTestTime: boolean = false;
  isWriteLog: boolean = true;
  isGetRandomQuestion: boolean = false;
  totalQuestion: number;
  testTimeControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$"),
    Validators.min(5)
  ]);
  allowToReDoControl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$")
  ]);
  itemPerPageCtrl = new FormControl('', [
    Validators.required,
    Validators.pattern("^[0-9]*$")
  ])
  // questionsRandomCtrl = new FormControl('', [
  //   Validators.required
  // ]);
  public get useCaseEnum(): typeof UseCase {
    return UseCase;
  }
  constructor(private testService: TestService, private _learnMessageService: LearnMessageService) {

  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.currentTabIndex);
  }
  ngOnInit(): void {
    //Nếu là sửa thiết lập thì clone testConfigModel từ testConfigResponse
    if (this.useCase == this.useCaseEnum.editting) {
      this.testConfigModel = { ...this.testConfigResponse };
      //Check và set các tham số gán ngModel trên view
      if (this.testConfigModel.testTime > 0) {
        this.isSetTestTime = true;
      }
      if (this.testConfigModel.intervalWriteLog == null) {
        this.isWriteLog = false;
      }
      if (this.testConfigModel.generateNumberRandomQuetions > 0) {
        this.isGetRandomQuestion = true;
      }
      this.changControlInput();
      //Load xong rồi thì sao
      this.isReady = true;
    }
    else {
      //Nếu là tạo mới thì khởi tạo testConfigModel mặc định
      this.testConfigModel = {
        id: null,
        viewCorrectResultAfterSubmittingTheTest: false,
        viewResultsAfterSubmitting: true,
        numberOfQuestionOnThePage: 5,
        generateNumberRandomQuetions: null,
        preserveTime: false,
        intervalWriteLog: 5,
        testTime: null,
        allowToRedo: false,
        theMaximumNumberToRedo: null,
        isRandomizeQuestions: false,
        numberOfQuestion: 0,
        minimumScore: null
      }
      this.changControlInput();
      //Xong rồi thì làm gì
      this.isReady = true;
    }

    this.testService.totalQuestion$.subscribe((totalQuestion) => {
      this.totalQuestion = totalQuestion;
      // this.questionsRandomCtrl = new FormControl('', [
      //   Validators.required,
      //   Validators.max(totalQuestion)
      // ]);
      this.itemPerPageCtrl = new FormControl('', [
        Validators.required,
        Validators.max(totalQuestion)
      ])
      if (totalQuestion > 0) {
        this.itemPerPageCtrl = new FormControl('', [
          Validators.min(1),
          Validators.required,
          Validators.max(totalQuestion)
        ]);
      }
      this.testConfigModel.numberOfQuestionOnThePage = this.testConfigModel.numberOfQuestionOnThePage > totalQuestion ?
        totalQuestion : (this.testConfigModel.numberOfQuestionOnThePage == 0 && this.totalQuestion > 5) ?
          5 : this.testConfigModel.numberOfQuestionOnThePage > 0 ?
            this.testConfigModel.numberOfQuestionOnThePage : this.totalQuestion;
      this.itemPerPageCtrl.setValue(this.testConfigModel.numberOfQuestionOnThePage);
    })

    //Lắng nghe sự kiện click save ngoài view chính
    this.subcription = this.submitEvent.subscribe(() => {
      if (!this.isInValidFormControl()) {
        this.invalidConfig.emit(false);
        if (this.useCase == this.useCaseEnum.editting) {
          if (JSON.stringify(this.testConfigResponse) == JSON.stringify(this.testConfigModel)) {
            this.testService.updateTestConfig(this.testConfigModel, false);
          }
          else {
            this.testService.updateTestConfig(this.testConfigModel);
          }
        }
        else {
          this.testService.addTestConfig(this.testConfigModel);
        }
      }else{
        this.invalidConfig.emit(true);
        if(this.currentTabIndex == 0){
          this._learnMessageService.failToast("Vui lòng kiểm tra lại dữ liệu!");
        }
      }
    });
  }
  ngOnDestroy() {
    this.subcription.unsubscribe();
  }
  changControlInput(): void {
    this.isSetTestTime ? this.testTimeControl.enable() : this.testTimeControl.disable();
    this.testConfigModel.allowToRedo ? this.allowToReDoControl.enable() : this.allowToReDoControl.disable();
    if (!this.isSetTestTime) {
      this.testTimeControl.reset();
      this.testConfigModel.testTime = null;
    } else {
      this.testTimeControl.markAsTouched();
    }
    if (!this.testConfigModel.allowToRedo) {
      this.allowToReDoControl.reset();
      this.testConfigModel.theMaximumNumberToRedo = null;
    }
    else {
      this.allowToReDoControl.markAsTouched();
    }
    //this.isGetRandomQuestion ? this.questionsRandomCtrl.enable() : this.questionsRandomCtrl.disable();
  }
  isInValidFormControl(): boolean {
    !this.isSetTestTime ?? this.testTimeControl.markAllAsTouched();
    !this.testConfigModel.allowToRedo ?? this.allowToReDoControl.markAllAsTouched();
    //!this.isGetRandomQuestion ?? this.questionsRandomCtrl.markAllAsTouched();
    this.itemPerPageCtrl.markAllAsTouched();
    return this.testTimeControl.invalid || this.allowToReDoControl.invalid || this.itemPerPageCtrl.invalid;
  }
}

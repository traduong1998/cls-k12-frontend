import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingAddTestComponent } from './setting-add-test.component';

describe('SettingAddTestComponent', () => {
  let component: SettingAddTestComponent;
  let fixture: ComponentFixture<SettingAddTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingAddTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingAddTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

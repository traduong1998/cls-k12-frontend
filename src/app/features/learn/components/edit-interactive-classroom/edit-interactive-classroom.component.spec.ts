import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInteractiveClassroomComponent } from './edit-interactive-classroom.component';

describe('EditInteractiveClassroomComponent', () => {
  let component: EditInteractiveClassroomComponent;
  let fixture: ComponentFixture<EditInteractiveClassroomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditInteractiveClassroomComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInteractiveClassroomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

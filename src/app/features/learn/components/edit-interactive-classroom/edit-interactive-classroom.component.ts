import { Component, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSelect } from '@angular/material/select';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/errorResponse';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { ContentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentEndpoint';
import { VirtualClassRoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/virtual-classroom-endpoints';
import { Content } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/content';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { VirtualClassRoom } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/virtualClassRoom';
import { ContentGroups } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { DetailContentResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/detailContentResponses';
import { Configuration } from 'src/app/shared/configurations';
import { VerifyAccountDialogComponent, VerifyAccountType } from 'src/app/shared/dialog/verify-account-dialog/verify-account-dialog.component';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { InvalidDateTimeValidator } from 'src/app/shared/validations/learning-path-validation';
import { StepHeaderControl } from '../../intefaces/stepHeaderControl';
import { LearnMessageService } from '../../Services/learn-message-service.service';

@Component({
  selector: 'learn-edit-interactive-classroom',
  templateUrl: './edit-interactive-classroom.component.html',
  styleUrls: ['./edit-interactive-classroom.component.scss']
})
export class EditInteractiveClassroomComponent implements OnInit {
  baseApiUrl = 'http://localhost:58910';
  @Input() detailContentResponses: DetailContentResponses;
  @Input() lessonStatus: string;
  joinUrl: string;
  lessonEndPoint: LessonEndpoint;
  contentEndpoint: ContentEndpoint;
  virtualClassroomEndpoint = new VirtualClassRoomEndpoint();
  optionGradeDefault: number = 0;
  isCreatelink: boolean = true;
  minDate: Date = new Date();
  buttonText: string = ''
  protected contentGroups: ContentGroups[];
  public contentGroupsFilterCtrl: FormControl = new FormControl();
  public filteredGroupContents: ReplaySubject<ContentGroups[]> = new ReplaySubject<ContentGroups[]>(1);
  @ViewChild('singleSelectContentGroup', { static: true }) singleSelectContentGroup: MatSelect;

  protected _onDestroy = new Subject<void>();

  createInteractive = this.fb.group({
    id: new FormControl(),
    lessonId: new FormControl(null, [Validators.required]),
    TypesOfContent: new FormControl('VIR', [Validators.required]),
    name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    contentGroupId: new FormControl(''),
    startedDate: new FormControl('', [Validators.required]),
    endDate: new FormControl('', [Validators.required]),
    description: new FormControl(''),
  });
  get lessonId() { return this.createInteractive.get('lessonId'); }
  get name() { return this.createInteractive.get('name'); }
  get contentGroupId() { return this.createInteractive.get('contentGroupId'); }
  get startedDate() { return this.createInteractive.get('startedDate'); }
  get endDate() { return this.createInteractive.get('endDate'); }
  get description() { return this.createInteractive.get('description'); }

  private get TypesOfContent() { return this.createInteractive.get('TypesOfContent'); }
  public isDisableSubmit: boolean = true;
  ngAfterViewInit() {
    this.contentGroupsFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterContentGroups();
        if (this.contentGroupId.value < 0) {
        }
      });

    this.setInitialValue();
  }

  lesson: Lesson;
  zoomMeetingId: number;
  endpointLesson: LessonEndpoint;

  content: Content;
  stepHeaderControl: StepHeaderControl;

  public dateRangeInput: FormControl = new FormControl('', [Validators.required]);
  constructor(
    private router: ActivatedRoute,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private _router: Router,
    private _learnMessageService: LearnMessageService,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig
  ) {
    this.endpointLesson = new LessonEndpoint({ baseUrl: this.baseApiUrl });
    this.contentEndpoint = new ContentEndpoint({ baseUrl: this.baseApiUrl });
  }
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<string[]>;

  ngOnInit(): void {
    this.checkZoomKey()
    this.stepHeaderControl = {
      currentPage: 1,
      lessonId: this.detailContentResponses.lessonId,
      step: 0
    }
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected filterGrades() {
    if (!this.contentGroups) {
      return;
    }
    // get the search keyword
    let search = this.contentGroupsFilterCtrl.value;
    if (!search) {
      this.filteredGroupContents.next(this.contentGroups.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupContents.next(
      this.contentGroups.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onSubmit() {
    this.spinner.show();
    let current: Content = this.createInteractive.value;
    if (JSON.stringify(this.content) === JSON.stringify(current)) {
      this._router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);
      this.spinner.hide();
    } else {
      this.isDisableSubmit = true;
      this.createInteractive.controls['lessonId'].setValue(this.detailContentResponses.lessonId);
      this.createInteractive.controls['startedDate'].setValue(this.startedDate.value);
      this.createInteractive.controls['id'].setValue(this.detailContentResponses.id);
      this.createInteractive.controls['endDate'].setValue(this.endDate.value);
      this.createInteractive.controls['description'].setValue(this.description.value);
      if (this.createInteractive.valid) {
        this.spinner.show();
        if (!this.lesson?.publicationDate)
          this.contentEndpoint.updateVirClassroomUnpublish(this.createInteractive.value).then(res => {
            if (res) {
              this._learnMessageService.successToast(" Chỉnh sửa thành công bài học tương tác ");
              this._router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.createInteractive.controls['lessonId'].value}`);
              this.isDisableSubmit = false;
            }

            this.spinner.hide();
          })
            .catch((err: ErrorResponse) => {
              let data: VerifyAccountType = { title: err.errorDetail, detail: Configuration.MESSAGE_CHANGE_ZOOM_ROOM_DETAIL, buttonText: `Tạo tài khoản` }
              switch (err.statusCode) {
                case 401:
                  const verifyAccountDialog = this.dialog.open(VerifyAccountDialogComponent, {
                    data: data
                  });
                  verifyAccountDialog.afterClosed().subscribe(result => {
                    if (result) {
                      this._router.navigateByUrl(`learner/user-infor?tab=create-zoom-account`);
                      this.spinner.hide();
                    } else {
                      this.spinner.hide();
                      return;
                    }

                  })
                case 403:
                  this._learnMessageService.failToast(Configuration.MESSAGE_FORBIDEN_DETAIL);
                  this.spinner.hide();
                  break;
                case 404:
                  data.buttonText = "Cập nhật liên kết";
                  const verifyAccountDialog404 = this.dialog.open(VerifyAccountDialogComponent, {
                    data: data
                  });

                  verifyAccountDialog404.afterClosed().subscribe(result => {
                    if (result) {
                      this.virtualClassroomEndpoint.createMeeting(this.detailContentResponses.id, this.detailContentResponses.lessonId)
                        .then(res => {
                          this._learnMessageService.successToast(` Đã liên kết thành công, hãy lưu nội dung `);
                        })
                        .catch((err: ErrorResponse) => {
                          this._learnMessageService.failToast(` Liên kết thất bại. Hãy thử lại `);
                        })

                      this.spinner.hide();
                    } else {
                      this.spinner.hide();
                      return
                    }
                  })
                  break;
                default:
                  break;
              }

              this.isDisableSubmit = false;

              this.spinner.hide();
            });

        else {

          this.contentEndpoint.updateVirClassroomPublish(this.createInteractive.value).then(res => {
            if (res) {
              this._learnMessageService.successToast(" Chỉnh sửa thành công bài học tương tác ");
              this._router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.createInteractive.controls['lessonId'].value}`);
              this.isDisableSubmit = false;
            }
            this.spinner.hide();
          })
            .catch((err: ErrorResponse) => {
              let data: VerifyAccountType = { title: err.errorDetail, detail: Configuration.MESSAGE_CHANGE_ZOOM_ROOM_DETAIL, buttonText: `Tạo tài khoản` }
              switch (err.statusCode) {
                case 401:
                  const verifyAccountDialog = this.dialog.open(VerifyAccountDialogComponent, {
                    data: data
                  });
                  verifyAccountDialog.afterClosed().subscribe(result => {
                    if (result) {
                      this._router.navigateByUrl(`learner/user-infor?tab=create-zoom-account`);
                      this.spinner.hide();
                    } else {
                      this.spinner.hide();
                      return;
                    }

                  })
                case 403:
                  this._learnMessageService.failToast(Configuration.MESSAGE_FORBIDEN_DETAIL);
                  this.spinner.hide();
                  break;

                case 404:
                  data.buttonText = "Cập nhật liên kết";
                  const verifyAccountDialog404 = this.dialog.open(VerifyAccountDialogComponent, {
                    data: data
                  });

                  verifyAccountDialog404.afterClosed().subscribe(result => {
                    if (result) {
                      this.virtualClassroomEndpoint.createMeeting(this.detailContentResponses.id, this.detailContentResponses.lessonId)
                        .then(res => {
                          this._learnMessageService.successToast(` Đã liên kết thành công, hãy lưu nội dung `);
                        })
                        .catch((err: ErrorResponse) => {
                          this._learnMessageService.failToast(` Liên kết thất bại. Hãy thử lại `);
                        })

                      this.spinner.hide();
                    } else {
                      this.spinner.hide();
                      return
                    }
                  })
                  break;
                default:
                  break;
              }

              this.isDisableSubmit = false;

              this.spinner.hide();
            });
        }
      }
      else {
        console.warn('invalid form control', this.createInteractive);
        this.spinner.hide();
      }
    }
  }

  protected filterContentGroups() {
    if (!this.contentGroups) {
      return;
    }
    // get the search keyword
    let search = this.contentGroupsFilterCtrl.value;
    if (!search) {
      this.filteredGroupContents.next(this.contentGroups.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupContents.next(
      this.contentGroups.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected setInitialValue() {

  }
  onCancelButtonHandleClick() {
    this._router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);
  }
  public onDateLearningPathChange(value: any) {
    let startDateControl = this.startedDate;
    let endDateControl = this.endDate;
    this.validateDateControl(startDateControl, endDateControl)
  }
  public getValidateStartDateErrors(): boolean {
    if (this.startedDate.errors?.invalidDateFromTo) {
      this.isDisableSubmit = true;
      return true;
    }

    this.isDisableSubmit = false;
    return false;
  }

  public getValidateEndDateErrors(): boolean {
    if (this.endDate.errors?.invalidDateFromTo) {
      this.isDisableSubmit = true;
      return true;
    }

    this.isDisableSubmit = false;
    return false;
  }

  public getValidateStartDateRequiredErrors() {
    if (this.startedDate.errors?.required) {
      this.isDisableSubmit = true;
      return true;
    }

    this.isDisableSubmit = false;
    return false;
  }

  public getValidateEndDateRequiredErrors() {
    if (this.endDate.errors?.required) {
      this.isDisableSubmit = true;
      return true;
    }

    this.isDisableSubmit = false;
    return false;
  }


  private validateDateControl(startDateControl: AbstractControl, endDateControl: AbstractControl) {
    let startDate = new Date(startDateControl.value?.toString())
    let endDate = new Date(endDateControl.value?.toString())
    if (startDateControl.value && endDateControl.value) {
      if (startDate.getTime() > endDate.getTime()) {
        startDateControl
          .setValidators(
            [
              InvalidDateTimeValidator(new Date(endDateControl.value))
            ]
          )
        this.isDisableSubmit = true;
        startDateControl.updateValueAndValidity();
        startDateControl.markAsTouched();
        startDateControl.markAsDirty();
      } else {
        this.isDisableSubmit = false;
        startDateControl.setErrors(null);
        startDateControl.clearValidators();
        startDateControl.updateValueAndValidity();
      }
    } else {
      startDateControl.setErrors(null);
      startDateControl.clearValidators();
      if (!startDateControl.value) {
        startDateControl
          .setValidators([Validators.required])
        startDateControl.updateValueAndValidity();
        startDateControl.markAsTouched();
        startDateControl.markAsDirty();
        this.isDisableSubmit = true;
      }
      if (!endDateControl.value) {
        endDateControl
          .setValidators([Validators.required])
        endDateControl.updateValueAndValidity();
        endDateControl.markAsTouched();
        endDateControl.markAsDirty();
        this.isDisableSubmit = false;
      }
    }
  }

  /** 27/08/2021 LamNV split content */
  private getContent() {
    this.contentEndpoint
      .getVirtualClassRoom(this.detailContentResponses.lessonId, this.detailContentResponses.id).then((res: VirtualClassRoom) => {
        if (res) {
          console.log("Online class room", res);
          this.joinUrl = res.joinURL
          this.name.setValue(this.detailContentResponses?.name);
          this.contentGroupId.setValue(this.detailContentResponses.contentGroupId);
          this.startedDate.setValue(res.startDate);
          this.minDate = new Date(res.startDate);
          this.endDate.setValue(res.finishDate);
          this.description.setValue(this.detailContentResponses.detailsOfContent);
          this.content = { ...this.createInteractive.value };
        }
      })

    this.endpointLesson.getLessonContentsById(this.detailContentResponses.lessonId).then(res => {
      this.lesson = res;
      var contentGroups = res.contentGroups.filter(x => x.id > 0);
      this.contentGroups = contentGroups;
      this.filteredGroupContents.next(this.contentGroups);
    })
  }

  public copyClipBoard() {
    const create_copy = (e: ClipboardEvent) => {
      e.clipboardData.setData('text/plain', this.joinUrl);
      e.preventDefault();
    };

    document.addEventListener('copy', create_copy);
    document.execCommand('copy');
    document.removeEventListener('copy', create_copy);

    // event.clipboardData.setData('text/plain',
    //   this.joinUrl);
    // event.preventDefault();
    // document.removeEventListener('copy', null);
    // document.execCommand('copy');

    this._learnMessageService.successToast(" Đã sao chép liên kết ")
  }

  public createZoomLink() {
    this.checkZoomKey();
  }

  private checkZoomKey() {
    this.spinner.show();
    this.virtualClassroomEndpoint.getMeeting(this.detailContentResponses.id, this.detailContentResponses.lessonId)
      .then(res => {
        if (res) {
          this.getContent();
          this.isCreatelink = true;
          this.spinner.hide();
        } else {
          console.error('get meeting fail')
          this.spinner.hide();
        }
      })
      .catch((err: ErrorResponse) => {
        let data: VerifyAccountType = { title: err.errorDetail, detail: Configuration.MESSAGE_CHANGE_ZOOM_ROOM_DETAIL, buttonText: Configuration.BUTTONLINKTEXT }
        if (err.statusCode == 401 || err.statusCode == 404 || err.statusCode == 403) {
          switch (err.statusCode) {
            case 401:
              data.detail = Configuration.MESSAGE_DONT_ZOOM_ROOM_DETAIL
              data.buttonText = Configuration.BUTTONSETTINGTEXT
              this.buttonText = Configuration.BUTTONSETTINGTEXT
              break;
            case 404:
              this.buttonText = Configuration.BUTTONLINKTEXT
              break;
            default:
              break;
          }
          const verifyAccountDialog = this.dialog.open(VerifyAccountDialogComponent, {
            data: data
          });
          verifyAccountDialog.afterClosed().subscribe(result => {
            if (result) {
              switch (err.statusCode) {
                case 401:
                  this._router.navigateByUrl(`learner/user-infor?tab=create-zoom-account`);
                  this.spinner.hide();
                  break;
                case 403:
                  this._learnMessageService.failToast(Configuration.MESSAGE_FORBIDEN_DETAIL);
                  this.spinner.hide();
                  break;
                case 404:
                  this.virtualClassroomEndpoint.createMeeting(this.detailContentResponses.id, this.detailContentResponses.lessonId)
                    .then(res => {
                      this._learnMessageService.successToast(` Đã liên kết thành công`);
                      this.getContent();
                      this.isCreatelink = true;
                      this.spinner.hide();
                    })
                    .catch((err: ErrorResponse) => {
                      this._learnMessageService.failToast(` Liên kết thất bại. Hãy thử lại `);
                      this.spinner.hide();
                    })

                  break;
                default:
                  break;
              }

            } else {
              // this._router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.detailContentResponses.lessonId}`);
              // return
              this.getContent();
              this.isCreatelink = false
            }
          })
        }
        this.spinner.hide();
      });
  }
}

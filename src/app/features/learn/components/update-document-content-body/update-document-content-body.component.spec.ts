import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDocumentContentBodyComponent } from './update-document-content-body.component';

describe('UpdateDocumentContentBodyComponent', () => {
  let component: UpdateDocumentContentBodyComponent;
  let fixture: ComponentFixture<UpdateDocumentContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateDocumentContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDocumentContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

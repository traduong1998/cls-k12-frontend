import { Component, Inject, Input, NgZone, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';

@Component({
  selector: 'learn-questions-overview',
  templateUrl: './questions-overview.component.html',
  styleUrls: ['./questions-overview.component.scss']
})
export class QuestionsOverviewComponent implements OnInit {
  displayedColumns: string[] = ['index', 'question', 'score', 'type'];
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  constructor(private dialog: MatDialog, private _zone: NgZone, @Inject(MAT_DIALOG_DATA) public questions: QuestionTest[]) { }

  ngOnInit(): void {
    console.log(this.questions);
  }
  hideloader() {
    document.getElementById('loading')
      .style.display = 'none';
  }

  // Check type question in table
  checkTypeQuestionTable(type): string {
    switch (type) {
      case Type.singlechoice:
        return 'Trắc nghiệm một lựa chọn';
      case Type.multichoice:
        return 'Trắc nghiệm nhiều lựa chọn';
      case Type.truefasle:
        return 'Đúng sai';
      case Type.truefaslechause:
        return 'Mệnh đề đúng sai';
      case Type.fillblank:
        return 'Điền khuyết';
      case Type.fillblank2:
        return 'Câu điền khuyết loại 2';
      case Type.underline:
        return 'Gạch chân';
      case Type.matching:
        return 'Câu ghép đôi';
      case Type.essay:
        return 'Câu tự luận';
      default:
        return '';
    }
  }
  // Add not for approve or refuse question
  handleNoteQuestion(value, element) {
    if (value) {
      element.note = value;
    }
  }
}

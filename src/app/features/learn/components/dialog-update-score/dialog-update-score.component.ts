import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../../intefaces/dialogData';

@Component({
  selector: 'app-dialog-update-score',
  templateUrl: './dialog-update-score.component.html',
  styleUrls: ['./dialog-update-score.component.scss']
})
export class DialogUpdateScoreComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogUpdateScoreComponent>,
    @Inject(MAT_DIALOG_DATA) public data: number) { }
  onCancelClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
  }

}

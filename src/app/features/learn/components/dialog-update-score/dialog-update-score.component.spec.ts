import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogUpdateScoreComponent } from './dialog-update-score.component';

describe('DialogUpdateScoreComponent', () => {
  let component: DialogUpdateScoreComponent;
  let fixture: ComponentFixture<DialogUpdateScoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogUpdateScoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogUpdateScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

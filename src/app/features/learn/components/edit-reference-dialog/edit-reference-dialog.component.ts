import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable, of, Subject } from 'rxjs';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';
import { ReferencesEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/referencesEndpoint';
import { References } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/references';
import { ReferenceRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/referenceRequest';
import { Configuration } from 'src/app/shared/configurations';
import { isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { EStatusConvert } from '../../intefaces/eStatusConvert';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';

@Component({
  selector: 'app-edit-reference-dialog',
  templateUrl: './edit-reference-dialog.component.html',
  styleUrls: ['./edit-reference-dialog.component.scss']
})
export class EditReferenceDialogComponent implements OnInit {
  toolTip: string = Configuration.MESSAGE_INFOR_REFERENCE;
  lessonId: number;
  reference: References;
  referenceRequest: ReferenceRequest;
  nameReference: string = '';
  linkPreview: string = null;
  nameFile: string;
  fileType: string;
  fileServer: FileServerEndpoint;
  isFirstLoad: boolean = true;
  documentStatus = '';
  priFileId = '';
  progressData: Observable<any>;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }
  numbers: 0;

  dialogProgressRef: MatDialogRef<ProgressDialogComponent>
  referenceEndpoint: ReferencesEndpoint;
  constructor(
    public dialogRef: MatDialogRef<EditReferenceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _learnMessageService: LearnMessageService,
    private dialog: MatDialog) {
    this.fileServer = new FileServerEndpoint();
    this.referenceEndpoint = new ReferencesEndpoint();
  }

  onNoClick(): void {
    if ((this.priFileId != (this.referenceRequest.fileId ?? '')) || (this.nameReference != this.referenceRequest.name)) {
      this.openConfirmDialog(Configuration.MESSAGE_EXIT_CONFIRM).subscribe((confirm) => {
        if (!confirm) {
          return;
        } else this.dialogRef.close();
      })
    } else this.dialogRef.close();
  }

  ngOnInit(): void {
    console.log(this.data.fileName);
    this.referenceRequest = this.data;
    this.nameReference = this.data.name;
    this.nameFile = this.data.fileName.split('.')[0];
    let data: string[] = this.data.fileName.split('.');
    this.fileType = data[data.length - 1];
    data.pop();
    this.nameFile = data.join('.');

    this.linkPreview = this.data.link
    this.priFileId = this.data.fileId
    this.checkLink('', this.data.link);
  }

  files: File[] = [];
  fileData: File;
  val: any;
  onSelect(event) {
    this.files = [];
    this.isFirstLoad = false;
    this.files.push(...event.addedFiles);
    this.fileData = event.addedFiles;
    this.val = event;
    var type = this.files[0].name.substring(this.files[0].name.lastIndexOf(".") + 1).toLowerCase();
    Promise.resolve()
      .then(() => {
        this.openProgressDialog(null);
        return this.fileServer.uploadFile({
          file: this.files[0],
          isPrivate: false,
          moduleName: ServiceName.LEARN
        }, {
          onUploadProgress: (progressEvent) => {
            this.progressData = progressEvent;
            this.getprogressData().subscribe(value => {
              if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: this.fileData.name, catch: false };
              }
            });
          }
        });
      })
      .then((res) => {

        this.priFileId && this.fileServer.skipFile(this.priFileId);
        this.checkLink(type, res.link);
        return res;
      })
      .then((res) => {
        this.referenceRequest = { name: this.nameReference, fileName: res.name, link: res.link, fileId: res.fileId, lessonId: this.lessonId }
        this.linkPreview = res.link;
        this.priFileId = res.fileId;

        this.getprogressData().subscribe(value => {
          if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
            this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: this.fileData.name, catch: false };
          }
        });
        setTimeout(() => {
          this.dialogProgressRef.close();
        }, 2000);
      })
      .catch((err) => {
        console.error(`Lỗi trong quá trình xử lý`, err);
        this.getprogressData().subscribe(value => {
          if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
            this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: this.fileData.name, catch: true };
          }
        });
        setTimeout(() => {
          this.dialogProgressRef.close();
        }, 2000);
      });
  }

  onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
    this.files = [];
  }

  onAddReferenceHandleClick() {
    if (this.nameFile == null && this.val == null) {
      this._learnMessageService.failToast("Bạn cần thêm tệp tài liệu");
      return;
    }
    if (isNullOrWhiteSpace(this.nameReference)) {
      this._learnMessageService.failToast("Bạn cần nhập tên tài liệu tham khảo");
      return;
    }

    if (this.nameReference == this.data.name && this.nameFile == this.referenceRequest.name) {
      this._learnMessageService.failToast(" Bạn không thay đổi gì");
      return;
    }
    let result = this.referenceRequest;
    result.name = this.nameReference;
    result.id = this.data.id;
    this.dialogRef.close(result);
  }

  onRemoveFileInitHandleClick() {
    this.isFirstLoad = false;
    this.files = [];
    this.fileType = null;
    this.nameFile = null;
  }

  /*
  * open progress dialog 
  */
  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.numbers }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  private checkLink(type: string, link: string) {
    this.documentStatus = '';
    // because mp3 it is not convert;
    if (['pdf'].includes(type) || type == '') {
      var jqxhr = $.get(link, () => {
      }).done(() => {
        this.documentStatus = 'COM';

        console.log("done")
      })
        .fail(() => {
          console.log("fail")

          this.priFileId && this.fileServer.getStatusFile(this.priFileId).then(res => {
            switch (res) {
              case EStatusConvert.Waiting:
              case EStatusConvert.Inprogress:
                this.documentStatus = 'INP';
                break;

              case EStatusConvert.Failed:
              case EStatusConvert.Skip:
                this.documentStatus = 'FAI';
                break;
              case EStatusConvert.Completed:
                this.documentStatus = 'COM';
                break;
              default:
                break;
            }
          })
        })
    } else {
      this.documentStatus = 'INP';
    }
  }

  private openConfirmDialog(value: string): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: {
        message: `${value}`,
        buttonText: {
          ok: 'Có',
          cancel: 'Không'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }
  @HostListener('window:beforeunload', ['$event'])
  unloadHandler($event) {
    if ((this.priFileId != (this.referenceRequest.fileId ?? '')) || (this.nameReference != this.referenceRequest.name)) {
      return false
    }
  }
}

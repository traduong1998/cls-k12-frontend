import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderQuestionBankComponent } from './header-create-question.component';

describe('HeaderQuestionBankComponent', () => {
  let component: HeaderQuestionBankComponent;
  let fixture: ComponentFixture<HeaderQuestionBankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderQuestionBankComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderQuestionBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import {MatRadioChange, MatRadioModule} from '@angular/material/radio';
import { LoadContentService } from '../../Services/load-content.service';

@Component({
  selector: 'app-header-create-question',
  templateUrl: './header-create-question.component.html',
  styleUrls: ['./header-create-question.component.scss']
})
export class HeaderQuestionBankComponent implements OnInit {
  typeOfQuestion:number;
  isAutoApproval:boolean;
  @Input() dataQuestion;
  checkQuestionFormat="single";
  isShuffle;
  GetValue(event:MatRadioChange){
    this._formatOfQuestion.getFormatOfQuestion(event.value);
  }

  IsAutoApproval(event:MatCheckboxChange){
    this._formatOfQuestion.getIsAutoApproval(event.checked);
  }
  constructor(private _formatOfQuestion:LoadContentService) { }


  states: string[] = [
    'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware',
    'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky',
    'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
    'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico',
    'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania',
    'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
    'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
  ];

  ngOnInit(): void {
    if(this.dataQuestion!=null || this.dataQuestion!= undefined){
      this.isShuffle = this.dataQuestion.isShuffle;
      this.checkQuestionFormat = this.dataQuestion.questionFormat;
    }
  }
 selectedKindOfQuestion='1';
 selectedTypeOfQuestion='1';
  selectedLevel='1';
}

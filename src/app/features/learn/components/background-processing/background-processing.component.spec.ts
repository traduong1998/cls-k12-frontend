import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackgroundProcessingComponent } from './background-processing.component';

describe('BackgroundProcessingComponent', () => {
  let component: BackgroundProcessingComponent;
  let fixture: ComponentFixture<BackgroundProcessingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BackgroundProcessingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackgroundProcessingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

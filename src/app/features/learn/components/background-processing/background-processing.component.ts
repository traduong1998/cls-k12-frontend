import { Component, Input, OnInit } from '@angular/core';

enum TypeEnum {
  VIDEO = 'VID',
  DOCUMENT = 'DOC',
  SOUND = 'SOU',
}

@Component({
  selector: 'learn-background-processing',
  templateUrl: './background-processing.component.html',
  styleUrls: ['./background-processing.component.scss']
})

export class BackgroundProcessingComponent implements OnInit {
  @Input() type: string = '';
  @Input() status : string;
  content: string = '';
  
  constructor() {

  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    debugger;
    switch (this.type) {
      case TypeEnum.VIDEO:
        this.content = 'Video';
        break;
      case TypeEnum.DOCUMENT:
        this.content = 'Tài liệu';
        break;
      case TypeEnum.SOUND:
        this.content = 'Âm thanh';
        break;
      default:
        break;
    }
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentAddTestComponent } from './content-add-test.component';

describe('ContentAddTestComponent', () => {
  let component: ContentAddTestComponent;
  let fixture: ComponentFixture<ContentAddTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentAddTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentAddTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { QuestionsOverviewComponent } from './../questions-overview/questions-overview.component';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { ContentGroupForCombobox } from './../../../../../../sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { ActivatedRoute } from '@angular/router';
import { UseCase } from './../../intefaces/useCase';
import { TestService } from './../../Services/test.service';
import { Observable, of, ReplaySubject, Subject, Subscription } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Option } from './../../../../shared/interface/Option';
import { Component, Input, OnInit, OnChanges, SimpleChanges, OnDestroy, ViewChild, ElementRef, HostListener, Output, EventEmitter } from '@angular/core';
import { DialogUpdateScoreComponent } from '../dialog-update-score/dialog-update-score.component';
import { FormControl, Validators } from '@angular/forms';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { TestDetailResponse } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/testDetailResponse';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { QuestionTest } from '../../intefaces/questionTest';
import { ContentGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentGroupEndpoint';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { QuestionConverterEndpoint } from 'sdk/cls-k12-sdk-js/src/services/question-converter/endpoints/question-converter-endpoint';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { Action } from 'src/app/shared/modules/question-banks/Interfaces/action';
import { ConfigUploadCkeditor } from 'src/app/core/interfaces/app-config';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { CLS } from 'sdk/cls-k12-sdk-js/src';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'learn-content-add-test',
  templateUrl: './content-add-test.component.html',
  styleUrls: ['./content-add-test.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ContentAddTestComponent implements OnInit, OnChanges, OnDestroy {
  @Input() currentTabIndex: number;
  @Input() lessonId: number;
  //Thông tin của bài kiểm tra nhận từ view tab-add-test
  @Input() testDetail?: TestDetailResponse;
  @Input() useCase: number;
  @Input() lessonStatus: string;
  @Output() invalidContent = new EventEmitter<boolean>();
  //Cái này là Observble lắng nghe sự kiện click save ngoài view chính để xử lý
  @Input() submitEvent: Observable<void>;
  @Output() isDataReady = new EventEmitter<boolean>();
  subcription: Subscription;
  @ViewChild("fileQuestion") fileQuestion: ElementRef;
  @ViewChild("passScoreErr") passScoreErr: ElementRef;

  baseApiUrl = 'http://localhost:65000';

  contentGroups: Option[] = [{ id: 1, name: "Tiết 1" }, { id: 2, name: "Tiết 2" }, { id: 3, name: "Tiết 3" }];
  //Observble này lắng nghe danh sách câu hỏi ở testService
  questions$: Observable<QuestionTest[]>;
  //Observble này lắng nghe tổng điểm của các câu hỏi  

  totalScore: number;
  allComplete: boolean = false;
  //Model question để gửi dữ liệu câu hỏi cho bên module question xử lý
  question: QuestionTest = null;
  showListQuestion: boolean = false;
  //Clone testDetailResponse qua model này để trước khi gửi data lên cập nhật thông tin bài kiểm tra thì so sánh
  testDetailModel: TestDetailResponse;
  displayedColumns: string[] = ['check', 'content', 'score', 'delete'];
  questionElement: QuestionTest | null;
  questions: QuestionTest[] = [];
  questionsShow: QuestionTest[] = [];
  groupContentControl: FormControl = new FormControl();
  groupContentFilterControl: FormControl = new FormControl();
  filteredGroupContents: ReplaySubject<ContentGroupForCombobox[]> = new ReplaySubject<ContentGroupForCombobox[]>(0);
  contentGroupEndpoint: ContentGroupEndpoint;
  questionConverterEndpoint: QuestionConverterEndpoint;
  isLoadDetailContent: boolean = false;
  currentQuestionId: number;
  progressData: Observable<any>;
  percent: 0;
  isLoadingData: boolean = true;
  selectedRowIndex = -1;
  childIndex: number;
  configUpload: ConfigUploadCkeditor;


  getprogressData(): Observable<any> {
    return of(this.progressData);
  }
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>

  @HostListener('window:scroll')
  nameContentControl: FormControl;

  passScoreControl: FormControl;
  get questionFormat(): typeof FormatQuestion {
    return FormatQuestion;
  }
  get questionLevel(): typeof Level {
    return Level;
  }
  get questionType(): typeof Type {
    return Type;
  }
  get actionEnum(): typeof Action {
    return Action;
  }
  constructor(public dialog: MatDialog,
    private testService: TestService,
    private route: ActivatedRoute,
    private _learnMessageService: LearnMessageService, private _authService: AuthService, private configService: AppConfigService) {
    this.contentGroupEndpoint = new ContentGroupEndpoint();
    this.questionConverterEndpoint = new QuestionConverterEndpoint();

    this.baseApiUrl = CLS.getConfig().apiBaseUrl;

    let baseUrl = CLS.getConfig().apiBaseUrls.fileServer ?? CLS.getConfig().apiBaseUrl;
    baseUrl += '/api';
    this.configUpload = {
      portalId: configService.getConfig().unit.portalId,
      token: _authService.getToken(),
      uploadUrl: baseUrl + "/file/upload"
    }
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

  ngOnInit(): void {
    this.nameContentControl = new FormControl('', [
      Validators.required,
      Validators.maxLength(255),
    ]);
    //Nếu là trường hợp sửa BKT
    if (this.useCase == UseCase.editting) {
      //clone testDetai ra nha
      this.testDetailModel = { ...this.testDetail };
      //Xong thì show ds câu hỏi ra nè
      this.showListQuestion = true;
    } else {
      //Nếu là tạo mới thì nhớ khởi tạo model trước
      this.testDetailModel = {
        name: null,
        contentGroupId: null,
        conditionsForCompletion: 'SCO',
        numberOfQuestion: 0,
        scoreAfterFinish: 0,
        totalScore: 0,
        totalEssayScore: 0,
        totalQuizScore: 0
      }
    }
    //Để 2 Observble lắng nghe
    this.questions$ = this.testService.questions$;
    this.questions$.subscribe((questions) => {
      this.questions = JSON.parse(JSON.stringify(questions));
      this.questionsShow = [...this.questions.filter(x => x.action != Action.Delete)];
      this.questionsShow.map((qst) => {
        if (qst.format == FormatQuestion.group) {
          qst.questions = [...qst.questions.filter(x => x.action != Action.Delete)];
        }
      })
      console.log(this.questions);
    });
    this.testService.totalScore$.subscribe((totalScore) => {
      debugger;
      this.totalScore = totalScore;
      this.passScoreControl = new FormControl('', [
        Validators.max(this.totalScore),
        Validators.required
      ]);
      this.passScoreControl.setValue(this.testDetailModel.scoreAfterFinish);
    })
    this.contentGroupEndpoint.getListContentGroupForCombobox(this.lessonId).then((res) => {
      let contentGroup = res;
      contentGroup.unshift({ id: 0, name: "Chọn tiết", order: -1 });
      this.filteredGroupContents.next(contentGroup);
      this.isLoadingData = false;
    });

    //Đăng ký xử lý sự kiện click save ở ngoài view chính
    this.subcription = this.submitEvent.subscribe(() => {
      //Nhớ kiểm tra so sánh model ở đâyyyy trước khi submit
      //Tất nhiên update thì mới so sánh rồi
      this.nameContentControl.markAllAsTouched();
      this.passScoreControl.markAllAsTouched();
      if (this.nameContentControl.valid && this.passScoreControl.valid) {
        this.invalidContent.emit(false);
        this.testDetailModel.numberOfQuestion = this.questionsShow.length;
        this.testDetailModel.totalScore = this.totalScore;
        this.testDetailModel.totalEssayScore = 0;
        [...this.questionsShow].filter(x=>x.type == 'ESS').forEach(element => {
          this.testDetailModel.totalEssayScore+= element.score;
        });
        this.testDetailModel.totalQuizScore = this.totalScore - this.testDetailModel.totalEssayScore;
        if (this.useCase == UseCase.editting) {
          //convert qua Json rồi so sánh thôi
          if (JSON.stringify(this.testDetail) == JSON.stringify(this.testDetailModel)) {
            this.testService.updateTestContent(this.testDetailModel, false);
          }
          else {
            this.testService.updateTestContent(this.testDetailModel);
          }
        }
        else {
          this.testService.addTestContent(this.testDetailModel);
        }
      } else {
        // todo: emit output invalid data để ko hiện loading
        this.invalidContent.emit(true);
        if (this.currentTabIndex == 1) {
          this._learnMessageService.failToast("Vui lòng kiểm tra lại dữ liệu nhập vào!")
        }
      }
    })
  }
  ngOnDestroy(): void {
    //unsubcribe cái sự kiện ở trên đi
    this.subcription.unsubscribe();
    this.testService.clear();
  }

  highlight(row) {
    this.selectedRowIndex = row.id;
    this.questionElement = this.questionElement === row ? null : row
  }
  someComplete(): boolean {
    return this.testService.someChecked(this.allComplete);
  }

  /**Cập nhật điểm cho nhiều câu hỏi */
  setScoreQuestions(score: number): void {
    this.testService.updateScoreQuestions(score);
  }

  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.testService.checkAll(completed);
  }

  updateAllComplete(isChecked: boolean, question: QuestionTest) {
    this.allComplete = this.testService.isCheckAll();
    if (question.questionGroupId == undefined || question.questionGroupId == null && question.format == FormatQuestion.group) {
      this.testService.checkGroupQuestion(question, isChecked);
    } else {
      this.testService.checkQuestion(question, isChecked);
    }
  }

  deleteQuestion(question: QuestionTest): void {
    this.testService.deleteQuestion(question);
    if (this.question != undefined && this.question != null) {
      if (question.id == this.question.id) {
        this.currentQuestionId = 0;
        this.question = undefined;
      }
    }
  }

  deleteSelected(): void {
    if (!this.someComplete() && !this.allComplete) {
      this._learnMessageService.failToast("Không có câu hỏi nào được chọn!");
      return;
    }
    else {
      this.openConfirmDialog("Bạn có chắc chắn muốn xóa các câu hỏi đã chọn?").subscribe((isConfirmed) => {
        if (isConfirmed) {
          if (this.question != undefined && this.question != null) {
            let currentQuestion = this.questions.find(x => x.id == this.question.id);
            if (currentQuestion.isChecked) {
              this.currentQuestionId = 0;
              this.question = undefined;
            }
          }
          this.testService.deleteSelected();
        }
      });
    }
  }

  private openConfirmDialog(value: string): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      data: {
        message: `${value}`,
        buttonText: {
          ok: 'Có',
          cancel: 'Không'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }

  /**Hiện popup cập nhật điểm */
  openUpdateScoreDialog(): void {
    if (!this.someComplete() && !this.allComplete) {
      this._learnMessageService.failToast("Không có câu hỏi nào được chọn!");
      return;
    }
    const dialogRef = this.dialog.open(DialogUpdateScoreComponent, {
      width: '377px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.setScoreQuestions(result);
      }
    });
  }

  /**Cập nhật câu hỏi */
  updateQuestion(qst: QuestionTest) {

    console.log(qst);
    if (qst) {
      this._learnMessageService.successToast('Cập nhật thành công');
      this.testService.updateQuestion(qst, this.lessonStatus);
      this.currentQuestionId = qst.id;
    } else {
      this._learnMessageService.failToast('Có lỗi xảy ra');
    }
  }

  updateScore(id: number, score: number) {
    if (score == null) {
      this._learnMessageService.failToast("Điểm câu hỏi không được để trống!");
    } else {
      this.testService.updateScore(id, score);
    }

  }
  updateScoreChild(parentId: number, childId: number, score: number) {
    if (score == null) {
      this._learnMessageService.failToast("Điểm câu hỏi không được để trống!");
    }
    this.testService.updateScoreChild(parentId, childId, score);
  }

  /**Click câu hỏi để sửa */
  chooseQuestion(questionSelected: QuestionTest, index?: number): void {
    if (index != undefined && this.childIndex == index + 1) {
      return
    }
    if (questionSelected.questionGroupId != null && questionSelected.questionGroupId != undefined) {
      let parent = this.questions.find(x => x.id == questionSelected.questionGroupId);
      this.question = JSON.parse(JSON.stringify(parent));
      this.currentQuestionId = parent.id;
    }
    else {
      this.question = JSON.parse(JSON.stringify(questionSelected));
      this.currentQuestionId = questionSelected.id;
    }
    this.isLoadDetailContent = true;
    if (index != null && index != undefined) {
      this.childIndex = index + 1;
    }
    else if (questionSelected.questionGroupId == null || questionSelected.questionGroupId == undefined) {
      this.childIndex = 1;
    }
  }

  scrollTop(): void {
    window.scroll({
      top: 500,
      left: 0,
      behavior: 'smooth'
    });
  }
  /**Khi chọn tiết học nè */
  onSelectContentGroup(contentGroupSelected: Option): void {
    this.testDetailModel.contentGroupId = contentGroupSelected.id;
  }

  onChangeOption(contentGroupSelected: number): void {
    this.testDetailModel.contentGroupId = contentGroupSelected;
  }
  /**Xử lý khi click file câu hỏi */
  addQuestionFromFile() {
    this.fileQuestion.nativeElement.click();
  }

  /**Xử lý file gửi lên server và lấy file json danh sách câu hỏi về gửi qua testService */
  handle(event) {
    const file = event.target.files;
    if (file) {
      Promise.resolve()
        .then(() => {
          this.openProgressDialog(null);
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.questionConverterEndpoint.uploadTestFiles(file, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              var subscription = this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          console.log('resss',res);
          if (res) {
            let questions: QuestionTest[] = [];
            let result = res[0];
            let id = -1;
            console.log(res);
            result.questions.map((question) => {
              if (question.format == FormatQuestion.single) {
                questions.push({
                  id: id,
                  content: question.content,
                  level: question.level,
                  format: question.format,
                  questionGroupId: null,
                  isChecked: false,
                  score: question.score,
                  type: question.type,
                  answers: question.answers,
                  action: Action.Add,
                  questions: []
                });
                id--;
              } else {
                let parentQuestion: QuestionTest = {
                  id: id,
                  content: question.content,
                  level: question.level,
                  format: question.format,
                  questionGroupId: null,
                  isChecked: false,
                  score: question.score,
                  type: question.type,
                  answers: question.answers,
                  action: Action.Add,
                  questions: []
                };
                id--;
                let subQuestions: QuestionTest[] = [];
                question.questions.map((child) => {
                  subQuestions.push({
                    id: id,
                    content: child.content,
                    level: child.level,
                    format: child.format,
                    questionGroupId: parentQuestion.id,
                    isChecked: false,
                    score: child.score,
                    type: child.type,
                    answers: child.answers,
                    action: Action.Add,
                    questions: []
                  });
                  id--;
                });
                parentQuestion.questions = subQuestions;
                questions.push(parentQuestion);
              }
            })
            this.testService.loadDataQuestions(questions);
            this.showListQuestion = true;
            this.fileQuestion.nativeElement.value = null;
          }
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err.response.data.errorDetail);
          this._learnMessageService.warToast(err.response.data.errorDetail);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }
  previewTest(): void {
    //this._learnMessageService.warToast("Tính năng đang phát triển!");
    this.dialog.open(QuestionsOverviewComponent, {
      data: this.testService.beauty(this.questions),
      width: '80%',
      height: '95vh'
    })
  }

  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.percent }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  public getLinkTemplate(cases: number) {
    if (cases == 1) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-mau.docx`;
    }
    else if (cases == 2) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/huong-dan-su-dung-tep-tin-mau.docx`;
    }
  }
}

import { LessonStatus } from './../../intefaces/lessonStatus';
import { Action } from 'src/app/shared/modules/question-banks/Interfaces/action';
import { MatSnackBar } from '@angular/material/snack-bar';
import { QuestionTest } from 'src/app/features/learn/intefaces/questionTest';
import { Component, ElementRef, HostListener, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { Option } from '../../../../shared/interface/Option';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { DetailContentRequests } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/detailContentRequests';
import { ContentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentEndpoint';
import { DetailContentResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/detailContentResponses';
import { ActivatedRoute, Router } from '@angular/router';

//qsb
import { Question } from 'sdk/cls-k12-sdk-js/src/services/question/models/Question';
import { TestService } from '../../Services/test.service';
import { CKEditorComponent } from 'ckeditor4-angular';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';

//grc
import { ReplaySubject, Subject, Observable, of } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { MatSelect } from '@angular/material/select';
import { ContentGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentGroupEndpoint';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { ContentGroupForCombobox, ContentGroupResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { QuestionConverterEndpoint } from 'sdk/cls-k12-sdk-js/src/services/question-converter/endpoints/question-converter-endpoint';
import { ProgressDialogComponent } from '../progress-dialog/progress-dialog.component';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { CLS } from 'sdk/cls-k12-sdk-js/src';
import { ConfigUploadCkeditor } from 'src/app/core/interfaces/app-config';
import { DialogUpdateScoreComponent } from '../dialog-update-score/dialog-update-score.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { QuestionsOverviewComponent } from '../questions-overview/questions-overview.component';
import { Configuration } from 'src/app/shared/configurations';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { EStatusConvert } from '../../intefaces/eStatusConvert';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';

@Component({
  selector: 'learn-add-document-content-body',
  templateUrl: './add-document-content-body.component.html',
  styleUrls: ['./add-document-content-body.component.scss']
})
export class AddDocumentContentBodyComponent implements OnInit {

  @Input() lessonName: string;
  tooltip :string = Configuration.MESSAGE_INFOR_DOCCUMENT
  baseApiUrl = 'http://localhost:58910';
  @ViewChild("file") file: ElementRef;
  group_content: ContentGroupForCombobox[] = [];
  currentQuestion: QuestionTest;
  progressData: Observable<any>;
  configUpload: ConfigUploadCkeditor;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }

  priFileId: string = null;
  documentStatus: string = ''
  numbers: 0;
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>;

  //grc
  public groupContentControl: FormControl = new FormControl();
  public groupContentFilterControl: FormControl = new FormControl();
  public filteredGroupContents: ReplaySubject<ContentGroupForCombobox[]> = new ReplaySubject<ContentGroupForCombobox[]>(0);
  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();

  contentGroup: Option = { id: undefined, name: undefined };

  @ViewChild("ck") ck: CKEditorComponent;
  //grc
  @ViewChild("singleSelect") singleSelect: MatSelect;
  nameFileDocument: "";
  showDocument = false;
  documentLink;
  contentGroupId: number;
  nameContent: string;
  conditionsForCompletion: string;
  minutes: number;
  seconds: number;
  @Input() lessonId: number;
  id: number;
  detailContentRequests: DetailContentRequests;
  detailContentResponses: DetailContentResponses;
  contentEndpoint: ContentEndpoint;
  contentGroupEndpoint: ContentGroupEndpoint;
  questionConverterEndpoint: QuestionConverterEndpoint;
  fileServer: FileServerEndpoint;
  detailContent: DetailContentRequests;
  isLoadDetailContent: boolean = false;
  //qsb
  @Input() contentId: number;
  questions: QuestionTest[];
  questionsShow: QuestionTest[] = [];
  allComplete: boolean = false;
  @ViewChild("fileQuestion") fileQuestion: ElementRef;
  showListQuestion = false;
  question: Question = null;
  documentFile: File;
  questionElement: QuestionTest | null;
  selectedRowIndex = -1;
  childIndex: number;
  totalScore: number;
  currentQuestionId: number;
  get questionFormat(): typeof FormatQuestion {
    return FormatQuestion;
  }
  get questionLevel(): typeof Level {
    return Level;
  }
  get questionType(): typeof Type {
    return Type;
  }
  get actionEnum(): typeof Action {
    return Action;
  }

   //#region Thiết lập câu hỏi
   isSetTestTime: boolean = false;
   isWriteLog: boolean = true;
   isGetRandomQuestion: boolean = false;
   totalQuestion: number;
   testTimeControl = new FormControl('', [
     Validators.required,
     Validators.pattern("^[0-9]*$"),
     Validators.min(5)
   ]);
   allowToReDoControl = new FormControl('', [
     Validators.required,
     Validators.pattern("^[0-9]*$")
   ]);
   itemPerPageCtrl = new FormControl('', [
     Validators.required,
     Validators.pattern("^[0-9]*$")
   ])
   //#endregion

  constructor(public dialog: MatDialog,
    private route: ActivatedRoute,
    private testService: TestService,
    private router: Router,
    private _learnMessageService: LearnMessageService,
    private _snackBar: MatSnackBar,
    private _authService: AuthService, private configService: AppConfigService,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.contentEndpoint = new ContentEndpoint({ baseUrl: this.baseApiUrl });
    this.fileServer = new FileServerEndpoint();
    this.detailContentRequests = new DetailContentRequests;
    this.detailContentRequests.testInfo = {
      viewCorrectResultAfterSubmittingTheTest : false,
      numberOfQuestionOnThePage: 0,
      testTime: null,
      allowToRedo: false,
      theMaximumNumberToRedo: null,
      isRandomizeQuestions: false
    };
    this.detailContentResponses = new DetailContentResponses;
    this.contentGroupEndpoint = new ContentGroupEndpoint;
    this.questionConverterEndpoint = new QuestionConverterEndpoint();
    this.minutes = 0;
    this.seconds = 0;
    this.lessonId = + this.route.snapshot.paramMap.get("id");
    this.detailContentRequests.lessonId = this.lessonId;
    this.contentGroupEndpoint.getListContentGroupForCombobox(this.lessonId).then((res) => {
      this.group_content = res;
      this.group_content.unshift({ id: 0, name: "Chọn tiết", order: -1 });
      console.log("contentGroups:------>>>>>>", res);
      this.filteredGroupContents.next(this.group_content);
    });
    this.testService.questions$.subscribe((listquestions) => {
      this.questions = JSON.parse(JSON.stringify(listquestions));
      this.questionsShow = [...this.questions.filter(x => x.action != Action.Delete)];
      this.questionsShow.map((qst) => {
        if (qst.format == FormatQuestion.group) {
          qst.questions = [...qst.questions.filter(x => x.action != Action.Delete)];
        }
      })
      this.itemPerPageCtrl = new FormControl('', [
        Validators.required,
        Validators.max(this.questionsShow.length)
      ])
      if (this.questionsShow.length > 0) {
        this.itemPerPageCtrl = new FormControl('', [
          Validators.min(1),
          Validators.required,
          Validators.max(this.questionsShow.length)
        ]);
      }
      this.detailContentRequests.testInfo.numberOfQuestionOnThePage = 0;
      this.detailContentRequests.testInfo.numberOfQuestionOnThePage = this.detailContentRequests.testInfo.numberOfQuestionOnThePage > this.questionsShow.length ?
      this.questionsShow.length : (this.detailContentRequests.testInfo.numberOfQuestionOnThePage == 0 && this.questionsShow.length > 5) ?
          5 : this.detailContentRequests.testInfo.numberOfQuestionOnThePage > 0 ?
            this.detailContentRequests.testInfo.numberOfQuestionOnThePage : this.questionsShow.length;
      this.itemPerPageCtrl.setValue(this.detailContentRequests.testInfo.numberOfQuestionOnThePage);
    });
    this.contentGroupEndpoint.getListContentGroupForCombobox(this.lessonId).then((res) => {
      this.group_content = res;
      this.group_content.unshift({ id: 0, name: "Chọn tiết", order: -1 });
      this.filteredGroupContents.next(this.group_content);
    });
    this.conditionsForCompletion = 'BUT';
    let baseUrl = CLS.getConfig().apiBaseUrls.fileServer ?? CLS.getConfig().apiBaseUrl;
    baseUrl += '/api';
    this.configUpload = {
      portalId: configService.getConfig().unit.portalId,
      token: _authService.getToken(),
      uploadUrl: baseUrl + "/file/upload"
    }
    this.testService.totalScore$.subscribe((totalScore) => {
      this.totalScore = totalScore;
    })

    this.baseApiUrl = CLS.getConfig().apiBaseUrl;
  }

  ngOnInit(): void {
    // set initial selection
    this.groupContentControl.setValue(this.group_content[-1]);
    // load the initial bank list
    this.filteredGroupContents.next(this.group_content.slice());
    // listen for search field value changes
    this.groupContentFilterControl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBanks();
      });
  }

  protected filterBanks() {
    if (!this.group_content) {
      return;
    }
    // get the search keyword
    let search = this.groupContentFilterControl.value;
    if (!search) {
      this.filteredGroupContents.next(this.group_content.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredGroupContents.next(
      this.group_content.filter(bank => bank.name.toLowerCase().indexOf(search) > -1)
    );
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
    this.testService.clear();
  }

  changControlInput(): void {
    this.isSetTestTime ? this.testTimeControl.enable() : this.testTimeControl.disable();
    this.detailContentRequests.testInfo?.allowToRedo ? this.allowToReDoControl.enable() : this.allowToReDoControl.disable();
    if (!this.isSetTestTime) {
      this.testTimeControl.reset();
      this.detailContentRequests.testInfo.testTime = null;
    } else {
      this.testTimeControl.markAsTouched();
    }
    if (!this.detailContentRequests.testInfo?.allowToRedo) {
      this.allowToReDoControl.reset();
      this.detailContentRequests.testInfo.theMaximumNumberToRedo = null;
    }
    else {
      this.allowToReDoControl.markAsTouched();
    }
    //this.isGetRandomQuestion ? this.questionsRandomCtrl.enable() : this.questionsRandomCtrl.disable();
  }

  addFromFile() {
    this.file.nativeElement.click();
  }

  //get file
  uploadFileDocument(event) {
    const file = event.target.files[0];
    if (!file) {
      return;
    }

    // Kiểm tra limit file
    let confirmLimit = Configuration.getLimitFileMessage(file);
    if (!confirmLimit.status) {
      this._learnMessageService.failToast(confirmLimit.message);
      return;
    }
    var type = file.name.substring(file.name.lastIndexOf(".") + 1).toLowerCase();
    if (["ppt", "pptx", "doc", "docx", "xls", "xlsx", "pdf"].includes(type)) {
      this.documentFile = file;
      Promise.resolve()
        .then(() => {
          this.openProgressDialog(null);
          //gửi video
          return this.documentFile ?
            this.fileServer.uploadFile({
              file: this.documentFile,
              isPrivate: false,
              moduleName: ServiceName.LEARN
            }, {
              onUploadProgress: (progressEvent) => {
                this.progressData = progressEvent;
                this.getprogressData().subscribe(value => {
                  if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                    this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                  }
                });
              }
            }) : null;
        })
        .then((res) => {

          this.priFileId && this.fileServer.skipFile(this.priFileId);
          this.documentLink = '';
          this.checkLink(type, res?.link);
          return res;
        })
        .then((res) => {
          console.log(`Hoàn thành`);
          this.showDocument = true;
          this.detailContentRequests.link = res?.link;
          this.priFileId = res.fileId;
          this.documentLink = res?.link;

          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });

      this.nameFileDocument = file.name;
      this.documentFile = file;

    }
    else {
      this._learnMessageService.warToast("Không đúng định dạng file, mời bạn nhập lại!");
      this.showDocument = false;
    }
  }
  onCancelClick(): void {
    this.getRequest();
    if (this.detailContentRequests.name != (this.detailContentResponses.name ? this.detailContentResponses.name : '')
      || this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId
      || this.detailContentRequests.link != (this.detailContentResponses.link ? this.detailContentResponses.link : '')
    ) {
      this.openConfirmDialog(Configuration.MESSAGE_EXIT_CONFIRM).subscribe((confirm) => {
        if (!confirm) {
          return;
        } this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
      })
    } else this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);

  }

  onSaveClick(): void {
    this.spinner.show();
    if (isNullOrWhiteSpace(this.nameContent) || isNullOrEmpty(this.nameContent)) {
      this._learnMessageService.warToast("Tên nội dung không được để trống!");
      this.spinner.hide();
      return;
    }
    else {
      this.detailContentRequests.name = this.nameContent;
    }

    this.detailContentRequests.typesOfContent = "DOC";

    if (this.conditionsForCompletion == null) {
      this.spinner.hide();
      this._learnMessageService.warToast("Chưa chọn điều kiện hoàn thành nội dung!");
      return;
    }
    else {
      this.detailContentRequests.conditionsForCompletion = this.conditionsForCompletion;
      if (this.conditionsForCompletion == 'INT') {
        if (this.minutes == null || this.seconds == null) {
          this.spinner.hide();
          this._learnMessageService.failToast("Thời gian hoàn thành không hợp lệ!");
          return;
        }
        else
          this.detailContentRequests.timeToComplete = this.minutes * 60 + this.seconds;
      }
      if (this.conditionsForCompletion == 'ANS') {
        this.detailContentRequests.listQuestion = this.questions;
        this.detailContentRequests.scoreAfterFinish = this.totalScore;
        this.detailContentRequests.totalScore = this.totalScore;
        this.detailContentRequests.totalEssayScore = 0;
        [...this.questionsShow].filter(x=>x.type == 'ESS').forEach(element => {
          this.detailContentRequests.totalEssayScore+= element.score;
        });
        this.detailContentRequests.totalQuizScore = this.totalScore - this.detailContentRequests.totalEssayScore;
      }
    }

    if (this.groupContentControl.value == undefined) {
      this.detailContentRequests.contentGroupId = null;
    }
    else {
      this.detailContentRequests.contentGroupId = this.groupContentControl.value.id;
    }

    this.detailContentRequests.linkId = this.priFileId;
    this.detailContentRequests.fileName = this.nameFileDocument;
    if (JSON.stringify(this.detailContentResponses) === JSON.stringify(this.detailContentRequests)) {
      this.spinner.hide();
      this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
    }

    this.saveDocumentFile();

  }

  private saveDocumentFile() {
    this.spinner.show();
    this.contentEndpoint.createContent(this.detailContentRequests).then((res) => {
      if (res) {
        this.detailContentResponses = { ...this.detailContentRequests };
        this.spinner.hide();
        this._learnMessageService.successToast();
        this.router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
      } else {
        this.spinner.hide();
        this._learnMessageService.failToast();
      }
    })
  }

  nameContentControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(255),
  ]);

  minutesControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
  ]);

  secondsControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
  ]);

  onGroupContentChanged(grc: Option) {
    this.contentGroupId = grc.id;
    console.log('tttt', grc.name);
  }
  highlight(row) {
    this.selectedRowIndex = row.id;
    this.questionElement = this.questionElement === row ? null : row
  }
  //qsb

  someComplete(): boolean {
    return this.testService.someChecked(this.allComplete);
  }

  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.testService.checkAll(completed);
  }

  updateAllComplete(isChecked: boolean, question: QuestionTest) {
    this.allComplete = this.testService.isCheckAll();
    if (question.questionGroupId == undefined || question.questionGroupId == null && question.format == FormatQuestion.group) {
      this.testService.checkGroupQuestion(question, isChecked);
    } else {
      this.testService.checkQuestion(question, isChecked);
    }
  }

  deleteQuestion(question: QuestionTest): void {
    this.testService.deleteQuestion(question);
    if (this.question != undefined && this.question != null) {
      if (question.id == this.question.id) {
        this.currentQuestionId = 0;
        this.question = undefined;
      }
    }
  }

  deleteSelected(): void {
    if (!this.someComplete() && !this.allComplete) {
      this._learnMessageService.failToast("Không có câu hỏi nào được chọn!");
      return;
    }
    else {
      this.openConfirmDialog("Bạn có chắc chắn muốn xóa các câu hỏi đã chọn?").subscribe((isConfirmed) => {
        if (isConfirmed) {
          if (this.question != undefined && this.question != null) {
            let currentQuestion = this.questions.find(x => x.id == this.question.id);
            if (currentQuestion.isChecked) {
              this.currentQuestionId = 0;
              this.question = undefined;
            }
          }
          this.testService.deleteSelected();
        }
      });
    }
  }
  addQuestionFromFile() {
    this.fileQuestion.nativeElement.click();
  }

  //get file
  handle(event) {
    const file = event.target.files;
    if (file) {
      Promise.resolve()
        .then(() => {
          this.openProgressDialog(null);
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.questionConverterEndpoint.uploadTestFiles(file, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              var subscription = this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          if (res) {
            let questions: QuestionTest[] = [];
            let result = res[0];
            let id = -1;
            console.log(res);
            result.questions.map((question) => {
              if (question.format == FormatQuestion.single) {
                questions.push({
                  id: id,
                  content: question.content,
                  level: question.level,
                  format: question.format,
                  questionGroupId: null,
                  isChecked: false,
                  score: question.score,
                  type: question.type,
                  unitId: question.unitId,
                  answers: question.answers,
                  action: Action.Add,
                  questions: []
                });
                id--;
              } else {
                let parentQuestion: QuestionTest = {
                  id: id,
                  content: question.content,
                  level: question.level,
                  format: question.format,
                  questionGroupId: null,
                  isChecked: false,
                  score: question.score,
                  type: question.type,
                  unitId: question.unitId,
                  answers: question.answers,
                  action: Action.Add,
                  questions: []
                };
                id--;
                let subQuestions: QuestionTest[] = [];
                question.questions.map((child) => {
                  subQuestions.push({
                    id: id,
                    content: child.content,
                    level: child.level,
                    format: child.format,
                    questionGroupId: parentQuestion.id,
                    isChecked: false,
                    score: child.score,
                    type: child.type,
                    unitId: child.unitId,
                    answers: child.answers,
                    action: Action.Add,
                    questions: []
                  });
                  id--;
                });
                parentQuestion.questions = subQuestions;
                questions.push(parentQuestion);
              }
            })
            this.testService.addListQuestion(questions);
            this.showListQuestion = true;
            this.fileQuestion.nativeElement.value = null;
          }
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err.response.data.errorDetail);
          this._learnMessageService.warToast(err.response.data.errorDetail);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }
  /**
       * Sets the initial value after the filteredBanks are loaded initially
       */
  protected setInitialValue() {
    this.filteredGroupContents
      .pipe(
        take(1),
        takeUntil(this._onDestroy)
      )
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        // triggers initializing the selection according to the initial value of
        // the form control (i.e. _initializeSelection())
        // this needs to be done after the filteredBanks are loaded initially
        // and after the mat-option elements are available
        this.singleSelect.compareWith = (a: ContentGroupResponses, b: ContentGroupResponses) =>
          a && b && a.id === b.id;
      });
  }
  ngAfterViewInit(): void {
    this.setInitialValue();

  }

  updateQuestion(qst: QuestionTest) {
    if (qst) {
      this._learnMessageService.successToast("Cập nhật thành công");
      this.testService.updateQuestion(qst, LessonStatus.Publish);
      this.currentQuestion = qst;
    } else {
      this._learnMessageService.failToast("Có lỗi xảy ra");
    }
  }

  chooseQuestion(questionSelected: QuestionTest, index?: number): void {
    if (index != undefined && this.childIndex == index + 1) {
      return
    }
    if (questionSelected.questionGroupId != null && questionSelected.questionGroupId != undefined) {
      let parent = this.questions.find(x => x.id == questionSelected.questionGroupId);
      this.question = JSON.parse(JSON.stringify(parent));
      this.currentQuestionId = parent.id;
    }
    else {
      this.question = JSON.parse(JSON.stringify(questionSelected));
      this.currentQuestionId = questionSelected.id;
    }
    this.isLoadDetailContent = true;
    if (index != null && index != undefined) {
      this.childIndex = index + 1;
    }
    else if (questionSelected.questionGroupId == null || questionSelected.questionGroupId == undefined) {
      this.childIndex = 1;
    }
  }
  previewTest(): void {
    //this._learnMessageService.warToast("Tính năng đang phát triển!");
    this.dialog.open(QuestionsOverviewComponent, {
      data: this.testService.beauty(this.questions),
      width: '80%',
      height: '95vh'
    })
  }
  /*
     * open progress dialog 
     */
  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.numbers }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  openUpdateScoreDialog(): void {
    if (!this.someComplete() && !this.allComplete) {
      this._learnMessageService.failToast("Không có câu hỏi nào được chọn!");
      return;
    }
    const dialogRef = this.dialog.open(DialogUpdateScoreComponent, {
      width: '377px',
      height: '234px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.setScoreQuestions(result);
      }
    });
  }
  setScoreQuestions(score: number): void {
    this.testService.updateScoreQuestions(score);
  }
  updateScore(id: number, score: number) {
    if (score == null) {
      this._learnMessageService.failToast("Điểm câu hỏi không được để trống!");
    }
    this.testService.updateScore(id, score);
  }
  updateScoreChild(parentId: number, childId: number, score: number) {
    if (score == null) {
      this._learnMessageService.failToast("Điểm câu hỏi không được để trống!");
    }
    this.testService.updateScoreChild(parentId, childId, score);
  }

  private openConfirmDialog(value: string): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      data: {
        message: `${value}`,
        buttonText: {
          ok: 'Có',
          cancel: 'Không'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }

  @HostListener('window:beforeunload', ['$event'])
  unloadHandler($event) {
    this.getRequest();
    if (this.detailContentRequests.name != (this.detailContentResponses.name ? this.detailContentResponses.name : '')
      || this.detailContentRequests.contentGroupId != this.detailContentResponses.contentGroupId
      || this.detailContentRequests.link != (this.detailContentResponses.link ? this.detailContentResponses.link : '')
    ) return false
  }

  private getRequest() {
    this.detailContentRequests.name = this.nameContent ? this.nameContent : '';
    this.detailContentRequests.contentGroupId = this.groupContentControl?.value?.id ?? null;
    this.detailContentRequests.link = this.documentLink ? this.documentLink : '';
    this.detailContentRequests.linkId = this.priFileId;
  }

  private checkLink(type: string, link: string) {
    this.documentStatus = '';
    // because mp3 it is not convert;
    if (['pdf'].includes(type) || type == '') {
      var jqxhr = $.get(link, () => {
      }).done(() => {
        this.documentStatus = 'COM';
      })
        .fail(() => {

          this.priFileId && this.fileServer.getStatusFile(this.priFileId).then(res => {
            switch (res) {
              case EStatusConvert.Waiting:
              case EStatusConvert.Inprogress:
                this.documentStatus = 'INP';
                break;

              case EStatusConvert.Failed:
              case EStatusConvert.Skip:
                this.documentStatus = 'FAI';
                break;
              case EStatusConvert.Completed:
                this.documentStatus = 'COM';
                break;
              default:
                break;
            }
          })
        })
    } else {
      this.documentStatus = 'INP';
    }
  }

  public getLinkTemplate(cases: number) {
    if (cases == 1) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-mau.docx`;
    }
    else if (cases == 2) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/huong-dan-su-dung-tep-tin-mau.docx`;
    }
  }
}

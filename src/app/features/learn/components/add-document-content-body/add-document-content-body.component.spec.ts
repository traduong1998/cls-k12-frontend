import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDocumentContentBodyComponent } from './add-document-content-body.component';

describe('AddDocumentContentBodyComponent', () => {
  let component: AddDocumentContentBodyComponent;
  let fixture: ComponentFixture<AddDocumentContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDocumentContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDocumentContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

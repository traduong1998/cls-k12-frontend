import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddWebContentBodyComponent } from './add-web-content-body.component';

describe('AddWebContentBodyComponent', () => {
  let component: AddWebContentBodyComponent;
  let fixture: ComponentFixture<AddWebContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddWebContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddWebContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

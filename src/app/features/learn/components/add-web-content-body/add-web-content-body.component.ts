import { QuestionTest } from 'src/app/features/learn/intefaces/questionTest';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { Option } from '../../../../shared/interface/Option';
import { MatDialog } from '@angular/material/dialog';
import { AddQuestionFromFileDialogComponent } from '../../components/add-question-from-file-dialog/add-question-from-file-dialog.component';
import { FormControl, Validators } from '@angular/forms';
import { DetailContentRequests } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/detailContentRequests';
import { ContentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentEndpoint';
import { DetailContentResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/detailContentResponses';
import { ActivatedRoute, Router } from '@angular/router';
//qsb
import { Question } from 'sdk/cls-k12-sdk-js/src/services/question/models/Question';
import { Observable } from 'rxjs';
import { DialogUpdateScoreComponent } from '../dialog-update-score/dialog-update-score.component';
import { DialogImportQuestionsFromFileComponent } from '../dialog-import-questions-from-file/dialog-import-questions-from-file.component';
import { TestService } from '../../Services/test.service';
import { CKEditor4, CKEditorComponent } from 'ckeditor4-angular';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';

//grc
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { MatSelect } from '@angular/material/select';
import { ContentGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentGroupEndpoint';
import { ContentGroupForCombobox, ContentGroupResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { CLS } from 'sdk/cls-k12-sdk-js/src';

@Component({
  selector: 'learn-add-web-content-body',
  templateUrl: './add-web-content-body.component.html',
  styleUrls: ['./add-web-content-body.component.scss']
})
export class AddWebContentBodyComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';

  @Input() lessonId: number;
  @Input() contentId: number;
  lessonName = "PHƯƠNG TRÌNH BẬC 2 VÀ ĐỊNH LÝ VIET";
  linkWeb = "";
  showWeb = false;
  contentGroupId: number;
  nameContent: string;
  conditionsForCompletion: string;
  minutes: number = 0;
  seconds: number = 0;
  contentCkEditor: string;
  detailContentRequests: DetailContentRequests;
  detailContentResponses: DetailContentResponses;
  contentEndpoint: ContentEndpoint;
  contentGroupEndpoint: ContentGroupEndpoint;
  detailContent: DetailContentRequests;
  questions$: Observable<QuestionTest[]>;
  totalScore$: Observable<number>;
  allComplete: boolean = false;
  showListQuestion = false;
  question: Question = null;
  data: Question[] = [];
  displayedColumns: string[] = ['check', 'content', 'delete'];
  configCkeditor = {
  };
  group_content: ContentGroupForCombobox[] = [];
  //grc
  public groupContentControl: FormControl = new FormControl();
  public groupContentFilterControl: FormControl = new FormControl();
  public filteredGroupContents: ReplaySubject<ContentGroupForCombobox[]> = new ReplaySubject<ContentGroupForCombobox[]>(0);
  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();
  @ViewChild("ck") ck: CKEditorComponent;
  //grc
  @ViewChild("singleSelect") singleSelect: MatSelect;
  @ViewChild("fileQuestion") fileQuestion: ElementRef;



  nameContentControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(255),
  ]);

  minutesControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
  ]);

  secondsControl = new FormControl('', [
    Validators.required,
    Validators.min(0),
  ]);

  onGroupContentChanged(grc: Option) {
    this.contentGroupId = grc.id;
    console.log('tttt', grc.name);
  }

  constructor(public dialog: MatDialog, private route: ActivatedRoute, private testService: TestService, private routerr: Router) {
    this.contentEndpoint = new ContentEndpoint();
    this.detailContentRequests = new DetailContentRequests;
    this.detailContentResponses = new DetailContentResponses;
    this.detailContent = new DetailContentRequests;
    this.lessonId = + this.route.snapshot.paramMap.get("id");
    console.log('lessonId', this.route.snapshot.paramMap.get("id"));
    console.log('lessonIdddddđ', this.lessonId);
    this.detailContentRequests.lessonId = this.lessonId;
    console.log('thislessonIdsssssssss', this.detailContentRequests.lessonId);
    this.contentGroupEndpoint = new ContentGroupEndpoint();
    this.contentGroupEndpoint.getListContentGroupForCombobox(this.lessonId).then((res) => {
      this.group_content = res;
      this.group_content.unshift({ id: 0, name: "Chọn tiết", order: -1 });
      console.log("contentGroups:------>>>>>>", res);
      this.filteredGroupContents.next(this.group_content);
    });
    //ckeditor
    this.configCkeditor = {
      height: 150,
      allowedContent: false,
      EditorType: CKEditor4.EditorType.CLASSIC
    };

    this.baseApiUrl = CLS.getConfig().apiBaseUrl;

  }

  ngOnInit(): void {
    //qsb
    this.testService.getListQuestion(this.contentId);
    this.questions$ = this.testService.questions$;
    this.totalScore$ = this.testService.totalScore$;
    console.log("c", this.questions$);
    // set initial selection
    this.groupContentControl.setValue(this.group_content[-1]);
    // load the initial bank list
    this.filteredGroupContents.next(this.group_content.slice());
    // listen for search field value changes
    this.groupContentFilterControl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterBanks();
      });
  }
  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  protected filterBanks() {
    if (!this.group_content) {
      return;
    }
    // get the search keyword
    let search = this.groupContentFilterControl.value;
    if (!search) {
      this.filteredGroupContents.next(this.group_content.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the banks
    this.filteredGroupContents.next(
      this.group_content.filter(bank => bank.name.toLowerCase().indexOf(search) > -1)
    );
  }
  validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
    return !!pattern.test(str);
  }
  addWeb(link: string) {
    if (this.validURL(link)) {
      this.showWeb = true;
      this.linkWeb = link;
    }
    else {
      alert("Không đúng định dạng URL, mời bạn nhập lại");
      this.showWeb = false;

    }
  }
  openDialog() {
    const dialogRef = this.dialog.open(AddQuestionFromFileDialogComponent, {
      width: '650px',
      height: 'auto',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.showListQuestion = result;
      console.log(`Dialog result: ${result}`);
      // this.animal = result;
    });
  }
  onCancelClick(): void {
  }
  onSaveClick(): void {
    if (isNullOrWhiteSpace(this.nameContent) || isNullOrEmpty(this.nameContent)) {
      alert('Chưa nhập tên nội dung');
      return;
    }
    else {
      this.detailContentRequests.name = this.nameContent;
    }
    this.detailContentRequests.typesOfContent = "WEB";
    if (this.conditionsForCompletion == null) {
      alert('Chưa chọn điều kiện hoàn thành nội dung');
      return;
    }
    else {
      this.detailContentRequests.conditionsForCompletion = this.conditionsForCompletion;
      if (this.conditionsForCompletion == 'INT') {
        if (this.minutes < 0) {
          alert('Phút không được nhỏ hơn 0');
          return;
        }
        if (this.seconds < 0) {
          alert('Giây không được nhỏ hơn 0');
          return;
        }
        if (this.minutes == null) {
          alert('Chưa nhập phút');
          return;
        }
        else {
          if (this.seconds == null) {
            alert('Chưa nhập giây');
            return;
          }
          else {
            this.detailContentRequests.timeToComplete = this.minutes * 60 + this.seconds;
          }
        }
      }
    }
    if (this.groupContentControl.value == undefined) {
      this.detailContentRequests.contentGroupId = null;
    }
    else {
      this.detailContentRequests.contentGroupId = this.groupContentControl.value.id;
    }
    this.detailContentRequests.link = this.linkWeb;
    this.detailContentRequests.detailsOfContent = this.contentCkEditor;
    if (JSON.stringify(this.detailContentResponses) === JSON.stringify(this.detailContentRequests)) {
      alert(`Dữ liệu không thay đổi`);
      return;
    }
    this.contentEndpoint.createContent(this.detailContentRequests).then((res) => {
      if (res) {
        this.detailContentResponses = { ...this.detailContentRequests };
        alert('Thêm thành công');
        this.routerr.navigateByUrl(`/learn/lesson/${this.lessonId}`);
      } else {
        alert('Thêm thất bại');
      }
    })
  }
  //qsb
  someComplete(): boolean {
    return this.testService.someChecked(this.allComplete);
  }

  setScoreQuestions(score: number): void {
    this.testService.updateScoreQuestions(score);
  }

  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.testService.checkAll(completed);
  }

  updateAllComplete() {
    this.allComplete = this.testService.isCheckAll();
  }

  deleteQuestion(id: number): void {
    console.log(id);
    //this.testService.deleteQuestion(id);
  }

  deleteSelected(): void {
    this.testService.deleteSelected();
  }
  openUpdateScoreDialog(): void {
    if (!this.someComplete() && !this.allComplete) {
      alert("Không có câu hỏi nào được chọn!");
      return;
    }
    const dialogRef = this.dialog.open(DialogUpdateScoreComponent, {
      width: '377px',
      height: '234px',
      data: this.totalScore$
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      if (result) {
        this.setScoreQuestions(result);
      }
    });
  }

  openImportQuestionsDialog(): void {
    const dialogRef = this.dialog.open(DialogImportQuestionsFromFileComponent, {
      width: '650px',
      //data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }

  addQuestionFromFile() {
    this.fileQuestion.nativeElement.click();
  }

  //get file
  handle(e) {
    const file = e.target.files[0];
    if (file.name != null) {
      this.showListQuestion = true;
    }
  }
  /**
       * Sets the initial value after the filteredBanks are loaded initially
       */
  protected setInitialValue() {
    this.filteredGroupContents
      .pipe(
        take(1),
        takeUntil(this._onDestroy)
      )
      .subscribe(() => {
        this.singleSelect.compareWith = (a: ContentGroupResponses, b: ContentGroupResponses) =>
          a && b && a.id === b.id;
      });
  }
  ngAfterViewInit(): void {
    //load data question
    this.loadData(this.data);
    this.setInitialValue();
  }

  updateQuestion(question) {
    //this.testService.updateQuestion(question);
    console.log("question edit", this.question);
  }

  loadData(data) {
    this.data = [];
    this.data = [...this.data, ...data];
  }

  delete(element) {
    this.testService.deleteQuestion(element.id);
  }

  chooseQuestion(item) {
    console.log("chosed", item);
    this.question = item;
    console.log(this.question);
  }

  getQuestionData() {
  
  }

  public getLinkTemplate(cases: number) {
    if (cases == 1) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-mau.docx`;
    }
    else if (cases == 2) {
      return `${this.baseApiUrl}/learn/public/files/cdn/template-file/huong-dan-su-dung-tep-tin-mau.docx`;
    }
  }
}

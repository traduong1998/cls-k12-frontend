import { UseCase } from './../../intefaces/useCase';
import { ContentStatus } from './../../intefaces/contentStatus';
import { Component, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { ContentGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentGroupEndpoint';
import { ContentGroupResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';

@Component({
  selector: 'learn-tab-content-lesson-part',
  templateUrl: './tab-content-lesson-part.component.html'
})
export class TabContentLessonPartComponent implements OnInit, OnChanges {

  @Input() lessonId: number;
  @Input() useCase: number;
  listContentGroup: ContentGroupResponses[]=[];
  
  displayedColumns: string[];
  panelOpenState = false;

  contentGroupEndpoint: ContentGroupEndpoint;


  public get useCaseEnum(): typeof UseCase {
    return UseCase;
  }
  public get contentStatusEnum(): typeof ContentStatus {
    return ContentStatus;
  }
  constructor() { 
    this.contentGroupEndpoint = new ContentGroupEndpoint();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if(this.useCase == this.useCaseEnum.approval){
      this.displayedColumns = ['name', 'status'];
    }
    else {
      this.displayedColumns = ['name', 'status','action'];
    }
    this.getListContentGroup();
  }

  ngOnInit(): void {
    this.getListContentGroup();
  }

  getListContentGroup(): void {
    this.contentGroupEndpoint.getListContentGroupByLessonId(this.lessonId).then((res)=>{
      this.listContentGroup = [...res];
      console.log(this.listContentGroup);
      console.log(res);
    })
  }
  

}

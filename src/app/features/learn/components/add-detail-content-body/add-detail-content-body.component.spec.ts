import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDetailContentBodyComponent } from './add-detail-content-body.component';

describe('AddDetailContentBodyComponent', () => {
  let component: AddDetailContentBodyComponent;
  let fixture: ComponentFixture<AddDetailContentBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDetailContentBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDetailContentBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

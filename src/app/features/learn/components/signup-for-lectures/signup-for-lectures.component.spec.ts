import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupForLecturesComponent } from './signup-for-lectures.component';

describe('SignupForLecturesComponent', () => {
  let component: SignupForLecturesComponent;
  let fixture: ComponentFixture<SignupForLecturesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignupForLecturesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupForLecturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { QuestionsOverviewComponent } from './components/questions-overview/questions-overview.component';
import { EditLessonConfigComponent } from './views/edit-lesson-config/edit-lesson-config.component';
import { FinishCreateLessonComponent } from './views/finish-create-lesson/finish-create-lesson.component';
import { ConfigLessonComponent } from './views/config-lesson/config-lesson.component';
import { CreateLessonComponent } from './views/create-lesson/create-lesson.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListLessonComponent } from './views/list-lesson/list-lesson.component';
import { ListLessonToBeApprovedComponent } from './views/list-lesson-to-be-approved/list-lesson-to-be-approved.component';
import { AddDetailContentComponent } from './views/add-detail-content/add-detail-content.component';
import { AddWebContentComponent } from './views/add-web-content/add-web-content.component';
import { AddDocumentContentComponent } from './views/add-document-content/add-document-content.component';
import { AddScormContentComponent } from './views/add-scorm-content/add-scorm-content.component';
import { AddIframeContentComponent } from './views/add-iframe-content/add-iframe-content.component';
import { AddContentLessonComponent } from './views/add-content-lesson/add-content-lesson.component';
import { AddInteractiveClassroomComponent } from './views/add-interactive-classroom/add-interactive-classroom.component';
import { AddTestLessonComponent } from './views/add-test-lesson/add-test-lesson.component';
import { EditLessonComponent } from './views/edit-lesson/edit-lesson.component';
import { EditInteractiveClassroomComponent } from './components/edit-interactive-classroom/edit-interactive-classroom.component';
import { EditContentLessonComponent } from './views/edit-content-lesson/edit-content-lesson.component';
import { AddVideoContentComponent } from './views/add-video-content/add-video-content.component';
import { AddSoundContentComponent } from './views/add-sound-content/add-sound-content.component';
import { AssignStudentForLessonComponent } from './views/assign-student-for-lesson/assign-student-for-lesson.component';
import { ApprovalLessonComponent } from './views/approval-lesson/approval-lesson.component';
import { UpdateContentComponent } from './views/update-content/update-content.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list-lesson'
  },
  {
    path: 'list-lesson',
    component: ListLessonComponent,
    data: {
      title: 'Danh sách bài giảng',
    }
  },
  {

    path: 'create-lesson',
    component: CreateLessonComponent,
    data: {
      title: 'Thêm bài giảng',
    }
  }
  ,
  {
    path: 'edit-lesson/:id',
    component: EditLessonComponent,
    data: {
      title: 'Chỉnh sửa bài giảng',
    }
  },
  {
    path: 'edit-content-lesson/:id',
    component: EditContentLessonComponent,
    data: {
      title: 'Sửa nội dung bài giảng',
    }
  },
  {
    path: 'list-lesson-to-be-approved',
    component: ListLessonToBeApprovedComponent,
    data: {
      title: 'Danh sách bài giảng cần duyệt',
    },
  },
  {
    path: '',
    children: [
      {
        path: 'lesson/:id/add-detail-content',
        component: AddDetailContentComponent,
        data: {
          title: 'Thêm nội dung thuần',
        },
      }
    ],
    data: {
      title: 'Dashbord'
    }
  },
  {
    path: '',
    children: [
      {
        path: 'lesson/:id/add-web-content',
        component: AddWebContentComponent,
        data: {
          title: 'Thêm nội dung web',
        },
      }
    ],
    data: {
      title: 'Dashbord'
    }
  },
  {
    path: '',
    children: [
      {
        path: 'lesson/:id/add-document-content',
        component: AddDocumentContentComponent,
        data: {
          title: 'Thêm nội dung tài liệu',
        },
      }
    ],
    data: {
      title: 'Dashbord'
    }
  },
  {
    path: '',
    children: [
      {
        path: 'lesson/:id/add-scorm-content',
        component: AddScormContentComponent,
        data: {
          title: 'Thêm nội dung scorm',
        },
      }
    ],
    data: {
      title: 'Dashbord'
    }
  },
  {
    path: '',
    children: [
      {
        path: 'lesson/:id/add-iframe-content',
        component: AddIframeContentComponent,
        data: {
          title: 'Thêm nội dung iframe',
        },
      }
    ],
    data: {
      title: 'Dashbord'
    }
  },
  {
    path: '',
    children: [
      {
        path: 'lesson/:id/add-video-content',
        component: AddVideoContentComponent,
        data: {
          title: 'Thêm nội dung video',
        },
      }
    ],
    data: {
      title: 'Dashbord'
    }
  },
  {
    path: '',
    children: [
      {
        path: 'lesson/:id/add-sound-content',
        component: AddSoundContentComponent,
        data: {
          title: 'Thêm nội dung âm thanh',
        },
      }
    ],
    data: {
      title: 'Dashbord'
    }
  },
  {
    path: 'add-content-lesson/:id',
    component: AddContentLessonComponent,
    data: {
      title: 'Thêm nội dung khóa học'
    }
  },
  {
    path: 'lesson/:id/add-interactive-classroom',
    component: AddInteractiveClassroomComponent,
    data: {
      title: 'Thêm lớp học tương tác'
    }
  },
  {
    path: 'edit-interactive-classroom/:id',
    component: EditInteractiveClassroomComponent,
    data: {
      title: 'Chỉnh sửa học tương tác'
    }
  },
  {
    path: 'config-lesson/:id',
    component: ConfigLessonComponent,
    data: {
      title: 'Thiết lập bài giảng',
    }
  },
  {
    path: 'signup-for-lectures-and-review-students/:id',
    component: AssignStudentForLessonComponent,
    data: {
      title: 'Ghi danh bài giảng',
    }

  }
  ,
  {
    path: 'finish-lesson/:id',
    component: FinishCreateLessonComponent,
    data: {
      title: 'Hoàn thành tạo bài giảng',
    }
  },
  {
    path: 'lesson/:id/add-test',
    component: AddTestLessonComponent,
    data: {
      title: 'Tạo bài kiểm tra',
    }
  }
  ,
  {
    path: 'approval-lesson/:id',
    component: ApprovalLessonComponent,
    data: {
      title: 'Duyệt bài giảng',
    }
  },
  {
    path: '',
    children: [
      {
        path: 'lesson/:idLesson/update-content/:id',
        component: UpdateContentComponent,
        data: {
          title: 'Cập nhật nội dung',
        },
      }
    ],
    data: {
      title: 'Dashbord'
    }
  },
  {
    path: 'edit-lesson-config/:id',
    component: EditLessonConfigComponent,
    data: {
      title: 'Thiết lập bài giảng',
    }
  },
  {
    path: 'lessons-to-approval',
    component: ListLessonToBeApprovedComponent,
    data: {
      title: 'Danh sách bài giảng cần duyệt',
    }
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LearnRoutingModule { }

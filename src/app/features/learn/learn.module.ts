import { ManageQuestionBankModule } from './../manage-question-bank/manage-question-bank.module';
import { AddIframeContentBodyComponent } from './components/add-iframe-content-body/add-iframe-content-body.component';
import { AddQuestionFromFileDialogComponent } from './components/add-question-from-file-dialog/add-question-from-file-dialog.component';
import { AddDocumentContentBodyComponent } from './components/add-document-content-body/add-document-content-body.component';
import { AddDocumentContentComponent } from './views/add-document-content/add-document-content.component';
import { DialogUpdateScoreComponent } from './components/dialog-update-score/dialog-update-score.component';
import { ContentAddTestComponent } from './components/content-add-test/content-add-test.component';
import { TabAddTestComponent } from './components/tab-add-test/tab-add-test.component';
import { AddTestLessonComponent } from './views/add-test-lesson/add-test-lesson.component';
import { FinishCreateLessonComponent } from './views/finish-create-lesson/finish-create-lesson.component';
import { TestService } from './Services/test.service';
import { AddPartDialogComponent } from './components/add-part-dialog/add-part-dialog.component';
import { StepHeaderLessonComponent } from './components/step-header-lesson/step-header-lesson.component';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { LearnRoutingModule } from './learn-routing.module';
import { ListLessonComponent } from './views/list-lesson/list-lesson.component';
import { BoxFilterBottomComponent } from './components/box-filter-bottom/box-filter-bottom.component';
import { AddDetailContentComponent } from './views/add-detail-content/add-detail-content.component';
import { CKEditorModule } from 'ckeditor4-angular';
import { FormsModule, NgControl } from '@angular/forms';
import { ListLessonToBeApprovedGridviewComponent } from './components/list-lesson-to-be-approved-gridview/list-lesson-to-be-approved-gridview.component';
import { AddDetailContentBodyComponent } from './components/add-detail-content-body/add-detail-content-body.component';
import { CreateLessonComponent } from './views/create-lesson/create-lesson.component';
import { AddContentComponent } from './views/add-content/add-content.component';
import { TabContentLessonTopComponent } from './components/tab-content-lesson-top/tab-content-lesson-top.component';
import { TabContentLessonFooterComponent } from './components/tab-content-lesson-footer/tab-content-lesson-footer.component';
import { ListLessonToBeApprovedComponent } from './views/list-lesson-to-be-approved/list-lesson-to-be-approved.component';
import { AddContentLessonComponent } from './views/add-content-lesson/add-content-lesson.component';
import { AddReferenceDialogComponent } from './components/add-reference-dialog/add-reference-dialog.component';
import { AddVideoContentComponent } from './views/add-video-content/add-video-content.component';
import { ConfigLessonComponent } from './views/config-lesson/config-lesson.component';
import { LessonNameHeaderComponent } from './components/lesson-name-header/lesson-name-header.component';
import { AddInteractiveClassroomComponent } from './views/add-interactive-classroom/add-interactive-classroom.component';
import { AddWebContentComponent } from './views/add-web-content/add-web-content.component';
import { AddWebContentBodyComponent } from './components/add-web-content-body/add-web-content-body.component';
import { SignupForLecturesComponent } from './components/signup-for-lectures/signup-for-lectures.component';
import { ReviewStudentsComponent } from './components/review-students/review-students.component';
import { AddScormContentComponent } from './views/add-scorm-content/add-scorm-content.component';
import { AddScormContentBodyComponent } from './components/add-scorm-content-body/add-scorm-content-body.component';
import { AddIframeContentComponent } from './views/add-iframe-content/add-iframe-content.component';
import { ApprovalLessonComponent } from './views/approval-lesson/approval-lesson.component';
import { LessonConfigBodyComponent } from './components/lesson-config-body/lesson-config-body.component';
import { SafeUrlPipe } from 'src/app/shared/pipes/SafeUrlPipe.pipe';
import { AddVideoContentBodyComponent } from './components/add-video-content-body/add-video-content-body.component';
import { AddSoundContentComponent } from './views/add-sound-content/add-sound-content.component';
import { AddSoundContentBodyComponent } from './components/add-sound-content-body/add-sound-content-body.component';
import { DialogImportQuestionsFromFileComponent } from './components/dialog-import-questions-from-file/dialog-import-questions-from-file.component';
import { SettingAddTestComponent } from './components/setting-add-test/setting-add-test.component';
import { UpdateDetailContentBodyComponent } from './components/update-detail-content-body/update-detail-content-body.component';
import { UpdateDocumentContentBodyComponent } from './components/update-document-content-body/update-document-content-body.component';
import { UpdateIframeContentBodyComponent } from './components/update-iframe-content-body/update-iframe-content-body.component';
import { UpdateSoundContentBodyComponent } from './components/update-sound-content-body/update-sound-content-body.component';
import { UpdateScormContentBodyComponent } from './components/update-scorm-content-body/update-scorm-content-body.component';
import { UpdateVideoContentBodyComponent } from './components/update-video-content-body/update-video-content-body.component';
import { UpdateWebContentBodyComponent } from './components/update-web-content-body/update-web-content-body.component';
import { EditTestLessonComponent } from './views/edit-test-lesson/edit-test-lesson.component';
import { EditLessonConfigComponent } from './views/edit-lesson-config/edit-lesson-config.component';
import { EditLessonComponent } from './views/edit-lesson/edit-lesson.component';
import { EditContentLessonComponent } from './views/edit-content-lesson/edit-content-lesson.component';
import { EditPartDialogComponent } from './components/edit-part-dialog/edit-part-dialog.component';
import { UpdateContentComponent } from './views/update-content/update-content.component';
import { HeaderQuestionBankComponent } from './components/header-question-bank/header-create-question.component';
import { TypeQuestionComponent } from './components/type-question/type-question.component';
import { GeneralQuestionComponent } from './components/general-question/general-question.component';
import { QuestionBanksModule } from 'src/app/shared/modules/question-banks/question-banks.module';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { AddEssayContentComponent } from './views/add-essay-content/add-essay-content.component';
import { EditInteractiveClassroomComponent } from './components/edit-interactive-classroom/edit-interactive-classroom.component';
import { AssignStudentForLessonComponent } from './views/assign-student-for-lesson/assign-student-for-lesson.component';
import { LearnMessageService } from './Services/learn-message-service.service';
import { ContextMenuModule } from 'ngx-contextmenu';
import { EditReferenceDialogComponent } from './components/edit-reference-dialog/edit-reference-dialog.component';
import { ProgressDialogComponent } from './components/progress-dialog/progress-dialog.component';
import { ProgressDialogService } from './Services/progress-dialog.service';
import { DateAdapter } from '@angular/material/core';
import { DateFormat } from 'src/app/shared/helpers/date-format.helper';
import { AssignUsergroupLessonTabComponent } from './components/assign-usergroup-lesson-tab/assign-usergroup-lesson-tab.component';
import { QuestionsOverviewComponent } from './components/questions-overview/questions-overview.component';
import { BackgroundProcessingComponent } from './components/background-processing/background-processing.component';
@NgModule({
  declarations: [
    ListLessonComponent,
    BoxFilterBottomComponent,
    CreateLessonComponent,
    StepHeaderLessonComponent,
    AddContentComponent,
    ListLessonToBeApprovedComponent,
    AddContentLessonComponent,
    TabContentLessonTopComponent,
    TabContentLessonFooterComponent,
    AddPartDialogComponent,
    ConfigLessonComponent,
    LessonNameHeaderComponent,
    AddReferenceDialogComponent,
    AddVideoContentComponent,
    AddDetailContentComponent,
    ListLessonToBeApprovedGridviewComponent,
    AddDetailContentBodyComponent,
    AddInteractiveClassroomComponent,
    AddWebContentComponent,
    AddWebContentBodyComponent,
    AssignStudentForLessonComponent,
    SignupForLecturesComponent,
    ReviewStudentsComponent,
    AddDocumentContentComponent,
    AddDocumentContentBodyComponent,
    AddScormContentComponent,
    AddScormContentBodyComponent,
    AddIframeContentComponent,
    AddIframeContentBodyComponent,
    ApprovalLessonComponent,
    LessonConfigBodyComponent,
    AddQuestionFromFileDialogComponent,
    //SafeUrlPipe,
    AddVideoContentBodyComponent,
    AddSoundContentComponent,
    AddSoundContentBodyComponent,
    FinishCreateLessonComponent,
    AddTestLessonComponent,
    TabAddTestComponent,
    ContentAddTestComponent,
    DialogUpdateScoreComponent,
    DialogImportQuestionsFromFileComponent,
    SettingAddTestComponent,
    UpdateDetailContentBodyComponent,
    UpdateDocumentContentBodyComponent,
    UpdateIframeContentBodyComponent,
    UpdateSoundContentBodyComponent,
    UpdateScormContentBodyComponent,
    UpdateVideoContentBodyComponent,
    UpdateWebContentBodyComponent,
    EditTestLessonComponent,
    EditLessonConfigComponent,
    AddEssayContentComponent,
    EditLessonComponent,
    EditContentLessonComponent,
    EditPartDialogComponent,
    UpdateContentComponent,
    HeaderQuestionBankComponent,
    TypeQuestionComponent,
    GeneralQuestionComponent,
    EditInteractiveClassroomComponent,
    EditReferenceDialogComponent,
    ProgressDialogComponent,
    AssignUsergroupLessonTabComponent,
    QuestionsOverviewComponent,
    BackgroundProcessingComponent,
  ],
  imports: [
    SharedModule,
    LearnRoutingModule,
    CKEditorModule,
    FormsModule,
    QuestionBanksModule,
    NgxMatSelectSearchModule,
    ContextMenuModule.forRoot(),
    ManageQuestionBankModule,
  ],
  providers: [
    TestService,
    LearnMessageService,
    ProgressDialogService,
    { provide: DateAdapter, useClass: DateFormat }
  ]
})
export class LearnModule {
  constructor(private dateAdapter: DateAdapter<Date>) {
    dateAdapter.setLocale("vi-VN");
  }
}

import { Option } from "src/app/shared/interface/Option";

export class GroupStudent implements Option {
    id: number;
    name: string;
    schoolId: number;
    gradeId: number
}
import { AnswerRequestOrResponse, QuestionRequestOrResponse } from './../../../shared/modules/question-banks/Interfaces/question-data';

export class QuestionTest implements QuestionRequestOrResponse {
    id?: number;
    content: string;
    level: string;
    format: string;
    questionGroupId?: number;
    score: number | undefined;
    type?: string;
    userId?: number;
    contentPanelId?: number;
    chapterId?: number;
    unitId?: number;
    answers: AnswerTest[] = [];
    questions?: QuestionTest[];
    isChecked: boolean = false;
    action?: string;
    //xác định câu hỏi đẩy lên API với mục đích gì (thêm, sửa, hay là xóa)
    /**
     *
     */
    constructor(question: QuestionTest) {
        this.id = question.id;
        this.content = question.content;
        this.level = question.level;
        this.format = question.format
        this.questionGroupId = question.questionGroupId;
        this.isChecked = false;
        this.score = question.score;
        this.type = question.type;
        this.chapterId = question.chapterId;
        this.unitId = question.unitId;
        this.answers = (question.answers != null && question.answers != undefined) ? question.answers : [];
        this.questions = (question.questions != null && question.questions != undefined) ? question.questions : [];
        this.action = (question.action != undefined) ? question.action : null;
    }
}
export class AnswerTest implements AnswerRequestOrResponse {
    id: number;
    questionId : number;
    content : string;
    trueAnswer : number;
    position? : boolean;
    groupOfFillBlank2? : number;
    isShuffler : boolean;
    action?: string;

    constructor(id: number, questionId: number, content: string, trueAnswer: number, position: boolean, groupOfFillBlank2: number, isShuffler: boolean, action: string) {
        this.id = id;
        this.questionId = questionId;
        this.content = content;
        this.trueAnswer = trueAnswer;
        this.position = position;
        this.groupOfFillBlank2 = groupOfFillBlank2;
        this.isShuffler = isShuffler;
        this.action = (action != undefined) ? action : null;
    }
}


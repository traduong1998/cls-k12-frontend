import { SchoolResponses } from "sdk/cls-k12-sdk-js/src/services/lesson/responses/schoolResponses";
import { Option } from "src/app/shared/interface/Option";

// Trường
export class School implements Option, SchoolResponses {
    id: number;
    name: string;
    divisionId?: number;
}
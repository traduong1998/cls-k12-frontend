import { Option } from "src/app/shared/interface/Option";

export class Chapter{
    id: number;
    curriculumName: string;
}
export interface QuestionType {
    value: string;
    viewValue: string;
  }
  
export interface QuestionTypeGroup {
    name: string;
    types: QuestionType[];
  }

  export interface Level {
    value: string;
    viewValue: string;
  }
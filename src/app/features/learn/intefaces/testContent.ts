import { QuestionTest } from "./questionTest";

export interface TestContent {
    name: string;
    contentGroupId?: number;
    lessonId: number;
    listQuestion?: QuestionTest[];
}
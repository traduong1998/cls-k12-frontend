export enum ShareSetting {
    register= 'REG',
    apprpval = 'APP',
    noApproval = 'NOA'
}
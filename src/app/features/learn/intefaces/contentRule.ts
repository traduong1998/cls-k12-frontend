export enum ContentRule {
    /** tuần tự*/
    sequentially = 'SEQ',

    /** tự do*/
    free = 'FRE'
}
export enum ContentType {
    text = 'TEX',
    video = 'VID',
    audio = 'SOU',
    scorm = 'SCO',
    iframe = 'IFR',
    test = 'TES',
    virtualClassRoom = 'VIR',
    document = 'DOC'
}
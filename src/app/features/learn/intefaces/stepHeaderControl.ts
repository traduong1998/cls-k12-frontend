export interface StepHeaderControl {
    currentPage: number;
    isCreatingContent?: boolean;
    lessonId?: number;
    step: number;
}
export interface StepHeaderControlExam {
    currentPage: number;
    isCreatingTest?: boolean;
    examId?: number;
    step: number;
}
export interface StepHeaderControlOrganize {
    currentPage: number;
    subjectId?: number;
    examId? : number;
}
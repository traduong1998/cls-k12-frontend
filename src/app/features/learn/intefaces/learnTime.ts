export enum LearnTime {
    // Vô thời hạn
    infinite = 'INF',
    //Xác định
    determined = 'DET'
}
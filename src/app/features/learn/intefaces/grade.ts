import { Option } from "src/app/shared/interface/Option";

export class Grade implements Option {
    id: number;
    name: string;
    schoolId: number;
}
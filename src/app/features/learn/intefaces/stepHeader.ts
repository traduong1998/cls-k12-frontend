export interface StepHeader {

    //Bước
    step: number,

    //tên bước
    label: string,

    type: number;

    //là bước hiện tại
    currentStep: boolean,

    //Đường dẫn khi click
    url: string,

    //Tên icon
    iconBasic: string,
    iconActive: string,
    iconDisabled: string
}

export enum StepType {
    basic = 1,
    active = 2,
    disabled = 3

}
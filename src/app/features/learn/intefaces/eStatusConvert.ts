export enum EStatusConvert {
  Waiting = "WAI",
  Inprogress = "INP",
  Completed = "COM",
  Failed = "FAI",
  Skip = "SKI"
}
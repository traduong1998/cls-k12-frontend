export enum StartTimeLine {
    start = 'STA',
    publish = 'PUB',
    register = 'REG'
}
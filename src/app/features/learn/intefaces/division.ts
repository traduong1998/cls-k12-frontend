import { DivisonResponses } from "sdk/cls-k12-sdk-js/src/services/lesson/responses/divisonResponses";
import { Option } from "src/app/shared/interface/Option";

// Phòng
export class Division implements Option, DivisonResponses {
    name: string;
    id: number;
    code: number;
}
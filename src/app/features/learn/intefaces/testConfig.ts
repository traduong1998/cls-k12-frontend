export interface TestConfig {
    viewCorrectResultAfterSubmittingTheTest: boolean | undefined;
    viewResultsAfterSubmitting: boolean | undefined;
    numberOfQuestionOnThePage: number | undefined;
    generateNumberRandomQuetions?: number;
    preserveTime: boolean | undefined;
    intervalWriteLog?: number;
    testTime?: number;
    allowToRedo: boolean | undefined;
    theMaximumNumberToRedo?: number;
    isRandomizeQuestions: boolean | undefined;
    numberOfQuestion: number | undefined;
    minimumScore?: number;
}
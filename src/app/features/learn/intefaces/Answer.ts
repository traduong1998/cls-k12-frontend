export interface Answer {
    //mã đáp án
    questionId?: number;
    //Nội dung đáp án
    content: string;
    //Đáp án đúng
    trueAnswer: number;
    // Vị trí trái phải đối với câu matching
    position?: boolean
    //Nhóm câu hỏi của điển khuyết loại 2
    groupOfFillBlank2?: number
    //Xáo trộn đáp án
    isShuffler: boolean;
}

export class AnswerCreateExamModel implements Answer {
    //mã đáp án
    answerId: number;
    //Nội dung đáp án
    content: string;
    //Đáp án đúng
    trueAnswer: number;
    // Vị trí trái phải đối với câu matching
    position: boolean;
    //Nhóm câu hỏi của điển khuyết loại 2
    groupOfFillBlank2: number;
    //Xáo trộn đáp án
    isShuffler: boolean;
}
import { Option } from "src/app/shared/interface/Option";

export class Teacher implements Option {
    id: number;
    name: string;
}

import { Option } from "src/app/shared/interface/Option";

export class SubjectOption implements Option {
    id: number;
    name: string;
    gradeId: number;
  
  }
import { Option } from "src/app/shared/interface/Option";

export class Curriculum implements Option {
    id: number;
    name: string;
    gradeId: number;
    subjectId: number;
  }
export enum ActionQuestion {
    Edit = "EDI",
    Create = "CRE",
    Delete = "DEL"
}
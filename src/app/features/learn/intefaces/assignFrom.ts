export enum AssignFrom {
    Registration = 'REG',
    Assign = 'ASS',
    Approval = 'APP'
}
export enum ConditionForCompletion {
    clickButton = 'BUT',
    answerQuestion = 'ANS',
    interval = 'INT',
    minimumScore = 'SCO'
}
export enum LessonStatus {
    Publish = 'PUB',
    Pending = 'PEN',
    Editting = 'EDI',
    Return = 'RET'
}
import { Option } from "src/app/shared/interface/Option";

export class Lesson implements Option {
    id: number;
    name: string;
    unitId: number;
}
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddScormContentComponent } from './add-scorm-content.component';

describe('AddScormContentComponent', () => {
  let component: AddScormContentComponent;
  let fixture: ComponentFixture<AddScormContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddScormContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddScormContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ContentGroupForCombobox } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { ContentGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentGroupEndpoint';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/errorResponse';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { ContentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentEndpoint';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { StepHeaderControl } from '../../intefaces/stepHeaderControl';
import { NgxSpinnerService } from 'ngx-spinner';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { InvalidDateTimeValidator } from 'src/app/shared/validations/learning-path-validation';
import { TeacherZoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/teacherZoomEndpoint';
import { STATUS_CODES } from 'http';
import { MatDialog } from '@angular/material/dialog';
import { VerifyAccountDialogComponent, VerifyAccountType } from 'src/app/shared/dialog/verify-account-dialog/verify-account-dialog.component';
import { Configuration } from 'src/app/shared/configurations';
import { VirtualClassRoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/virtual-classroom-endpoints';


export interface ContentGroup {
  id: number;
  name: string;
  order: number;
}

@Component({
  selector: 'learn-add-interactive-classroom',
  templateUrl: './add-interactive-classroom.component.html',
  styleUrls: ['./add-interactive-classroom.component.scss']
})

export class AddInteractiveClassroomComponent implements OnInit {
  @ViewChild('singleSelectContentGroup', { static: true }) singleSelectContentGroup: MatSelect;
  virtualClassroomEndpoint = new VirtualClassRoomEndpoint();
  endpointContent: ContentEndpoint;
  endpointLesson: LessonEndpoint;
  stepHeaderControl: StepHeaderControl;
  minDate: Date = new Date();
  public disableButtonSubmit = false;
  public isDisableSubmit: boolean;
  protected contentGroups: ContentGroupForCombobox[];
  public contentGroupsFilterCtrl: FormControl = new FormControl();
  public filteredGroupContents: ReplaySubject<ContentGroupForCombobox[]> = new ReplaySubject<ContentGroupForCombobox[]>(1);
  protected _onDestroy = new Subject<void>();
  public dateRangeInput: FormControl = new FormControl('', [Validators.required]);
  createInteractive = this.classRoomForm.group({
    lessonId: new FormControl(null, [Validators.required]),
    TypesOfContent: new FormControl('VIR', [Validators.required]),
    name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    contentGroupId: new FormControl(''),
    startedDate: new FormControl(new Date(), [Validators.required]),
    endDate: new FormControl(new Date(), [Validators.required]),
    description: new FormControl(''),
  });

  get name() { return this.createInteractive.get('name'); }
  get contentGroupId() { return this.createInteractive.get('contentGroupId'); }
  get startedDate() { return this.createInteractive.get('startedDate'); }
  get endDate() { return this.createInteractive.get('endDate'); }
  get description() { return this.createInteractive.get('description'); }
  lessonId: number;
  lesson: Lesson;
  lessonName: string;
  isReady: boolean = false;
  contentGroupEndpoint: ContentGroupEndpoint;
  constructor(private router: ActivatedRoute,
    public dialog: MatDialog,
    private classRoomForm: FormBuilder,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private spinner: NgxSpinnerService,
    private _learnMessageService: LearnMessageService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    let defaultEndDate = new Date();
    defaultEndDate.setHours(defaultEndDate.getHours() + 1);

    //set value form group
    this.createInteractive.controls['endDate'].setValue(defaultEndDate);

    this.endpointContent = new ContentEndpoint();
    this.endpointLesson = new LessonEndpoint();
    this.contentGroupEndpoint = new ContentGroupEndpoint();
    this.lessonId = +this.router.snapshot.paramMap.get("id");
    this.endpointLesson.getLessonById(this.lessonId).then(res => {
      if (res) {
        this.lesson = res;
      }
    });
    this.contentGroupEndpoint.getListContentGroupForCombobox(this.lessonId).then((res) => {
      this.contentGroups = res;
      this.contentGroups.unshift({ id: 0, name: "Chọn tiết", order: -1 });
      this.filteredGroupContents.next(this.contentGroups)
    });

  }

  ngOnInit(): void {

  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  getDisable() {
    return this.disableButtonSubmit;
  }
  protected filterContentGroups() {
    if (!this.contentGroups) {
      return;
    }
    // get the search keyword
    let search = this.contentGroupsFilterCtrl.value;
    if (!search) {
      this.filteredGroupContents.next(this.contentGroups.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupContents.next(
      this.contentGroups.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }
  ngAfterViewInit() {
    this.contentGroupsFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterContentGroups();
        if (this.contentGroupId.value < 0) {

        }
      });

    this.setInitialValue();

    this.spinner.show();
    this.virtualClassroomEndpoint.checkMeeting(this.lessonId).then(res => {
      if (res) {

        this.getContent();
        this.spinner.hide();
        // this.isDisableSubmit = false;
      }
    })
      .catch((error: ErrorResponse) => {
        if (error.statusCode == 403 || error.statusCode == 401) {
          switch (error.statusCode) {
            case 401:
              let data: VerifyAccountType = { title: error.errorDetail, detail: Configuration.MESSAGE_DONT_ZOOM_ROOM_DETAIL, buttonText: Configuration.BUTTONSETTINGTEXT }
              const verifyAccountDialog = this.dialog.open(VerifyAccountDialogComponent, {
                data: data
              });
              verifyAccountDialog.afterClosed().subscribe(result => {
                if (result) {
                  this._router.navigateByUrl(`learner/user-infor?tab=create-zoom-account`);
                }
                this.spinner.hide();
                this.disableButtonSubmit = true;
              });
              break;
            case 403:
              this.isDisableSubmit = true;
              console.log(this.isDisableSubmit);

              this._learnMessageService.failToast(Configuration.MESSAGE_FORBIDEN_DETAIL);
              // this._router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
              // document.getElementById('submit-button').setAttribute('disabled', true) =
              this.disableButtonSubmit = true;
              this.spinner.hide();
            default:
              this.spinner.hide();
              break;
          }

        } else {
          this._learnMessageService.failToast(error.errorDetail);
          this.isDisableSubmit = false;
          this.spinner.hide();
          this.disableButtonSubmit = true;
        }
      })
  }
  protected setInitialValue() {
    this.filteredGroupContents
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectContentGroup.compareWith = (a: ContentGroup, b: ContentGroup) => a && b && a.id === b.id;
      });
  }


  onSubmit() {
    console.log(this.isDisableSubmit);
    this.spinner.show();
    this.createInteractive.controls['lessonId'].setValue(this.lessonId);
    if (this.createInteractive.valid && !this.isDisableSubmit) {
      this.isDisableSubmit = true;

      this.endpointContent.createContent(this.createInteractive.value).then(res => {
        if (res) {
          this._learnMessageService.successToast(' Tạo thành công bài học tương tác ')
          this._router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
          this.isDisableSubmit = false;
        }

        this.spinner.hide();
      })
        .catch((err: ErrorResponse) => {
          if (err.statusCode == 404 || err.statusCode == 401) {
            let data: VerifyAccountType = { title: err.errorDetail, detail: Configuration.MESSAGE_DONT_ZOOM_ROOM_DETAIL, buttonText: Configuration.BUTTONSETTINGTEXT }
            const verifyAccountDialog = this.dialog.open(VerifyAccountDialogComponent, {
              data: data
            });
            verifyAccountDialog.afterClosed().subscribe(result => {
              if (result) {
                this._router.navigateByUrl(`learner/user-infor?tab=create-zoom-account`);
              }
            });
          } else {
            this._learnMessageService.failToast(err.errorDetail);
            this.isDisableSubmit = false;
          }

          this.spinner.hide();
        });
    } else {
      this.spinner.hide();
    }
  }

  onCancelButtonHandleClick() {
    this._router.navigateByUrl(`dashboard/learn/edit-content-lesson/${this.lessonId}`);
  }

  public onDateLearningPathChange(value: any) {
    let startDateControl = this.startedDate;
    let endDateControl = this.endDate;
    this.validateDateControl(startDateControl, endDateControl)
  }
  public getValidateStartDateErrors(): boolean {
    if (this.startedDate.errors?.invalidDateFromTo) {
      this.isDisableSubmit = true;
      return true;
    }

    this.isDisableSubmit = false;
    return false;
  }

  public getValidateEndDateErrors(): boolean {
    if (this.endDate.errors?.invalidDateFromTo) {
      this.isDisableSubmit = true;
      return true;
    }

    this.isDisableSubmit = false;
    return false;
  }

  public getValidateStartDateRequiredErrors() {
    if (this.startedDate.errors?.required) {
      this.isDisableSubmit = true;
      return true;
    }

    this.isDisableSubmit = false;
    return false;
  }

  public getValidateEndDateRequiredErrors() {
    if (this.endDate.errors?.required) {
      this.isDisableSubmit = true;
      return true;
    }

    this.isDisableSubmit = false;
    return false;
  }
  private validateDateControl(startDateControl: AbstractControl, endDateControl: AbstractControl) {
    let startDate = new Date(startDateControl.value?.toString())
    let endDate = new Date(endDateControl.value?.toString())
    if (startDateControl.value && endDateControl.value) {
      if (startDate.getTime() > endDate.getTime()) {
        startDateControl
          .setValidators(
            [
              InvalidDateTimeValidator(new Date(endDateControl.value))
            ]
          )
        this.isDisableSubmit = true;
        startDateControl.updateValueAndValidity();
        startDateControl.markAsTouched();
        startDateControl.markAsDirty();
      } else {
        this.isDisableSubmit = false;
        startDateControl.setErrors(null);
        startDateControl.clearValidators();
        startDateControl.updateValueAndValidity();
      }
    } else {
      startDateControl.setErrors(null);
      startDateControl.clearValidators();
      if (!startDateControl.value) {
        startDateControl
          .setValidators([Validators.required])
        startDateControl.updateValueAndValidity();
        startDateControl.markAsTouched();
        startDateControl.markAsDirty();
        this.isDisableSubmit = true;
      }
      if (!endDateControl.value) {
        endDateControl
          .setValidators([Validators.required])
        endDateControl.updateValueAndValidity();
        endDateControl.markAsTouched();
        endDateControl.markAsDirty();
        this.isDisableSubmit = false;
      }
    }
  }

  private getContent() {
    this.endpointLesson.getLessonById(this.lessonId).then((lesson) => {
      this.stepHeaderControl = {
        currentPage: 2,
        isCreatingContent: true,
        lessonId: lesson.id,
        step: lesson.step
      }
      this.lessonName = lesson.name;
      this.isReady = true;
    });
  }

}

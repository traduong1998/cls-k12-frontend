import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInteractiveClassroomComponent } from './add-interactive-classroom.component';

describe('AddInteractiveClassroomComponent', () => {
  let component: AddInteractiveClassroomComponent;
  let fixture: ComponentFixture<AddInteractiveClassroomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddInteractiveClassroomComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInteractiveClassroomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

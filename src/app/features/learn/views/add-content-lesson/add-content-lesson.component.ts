import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, of, Subject } from 'rxjs';
import { CLSModules, CLSPermissions, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/errorResponse';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { ContentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentEndpoint';
import { ContentGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentGroupEndpoint';
import { ReferencesEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/referencesEndpoint';
import { TeacherZoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/teacherZoomEndpoint';
import { VirtualClassRoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/virtual-classroom-endpoints';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { ReferenceRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/referenceRequest';
import { ContentGroupResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { ReferenceResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/referenceResponses';
import { AuthService } from 'src/app/core/services';
import { Configuration } from 'src/app/shared/configurations';
import { VerifyAccountDialogComponent, VerifyAccountType } from 'src/app/shared/dialog/verify-account-dialog/verify-account-dialog.component';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { AddPartDialogComponent } from '../../components/add-part-dialog/add-part-dialog.component';
import { AddReferenceDialogComponent } from '../../components/add-reference-dialog/add-reference-dialog.component';
import { EditPartDialogComponent } from '../../components/edit-part-dialog/edit-part-dialog.component';
import { EditReferenceDialogComponent } from '../../components/edit-reference-dialog/edit-reference-dialog.component';
import { ProgressDialogComponent } from '../../components/progress-dialog/progress-dialog.component';
import { UseCase } from '../../intefaces/useCase';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { ContentStatus } from './../../intefaces/contentStatus';
import { StepHeaderControl } from './../../intefaces/stepHeaderControl';

export class Button {
  public name: string;
  public color: string;
  public icon: string;
  constructor(private _name: string,
    private _color: string,
    private _icon: string) {
    this.name = _name;
    this.color = _color;
    this.icon = _icon;
  }
}

@Component({
  selector: 'learn-add-content-lesson',
  templateUrl: './add-content-lesson.component.html',
  styleUrls: ['./add-content-lesson.component.scss']
})

export class AddContentLessonComponent implements OnInit {
  public get contentStatusEnum(): typeof ContentStatus {
    return ContentStatus;
  }

  progressData: Observable<any>;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }
  numbers: 0;
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>
  lessonEndpoint: LessonEndpoint;
  contentEndpoint: ContentEndpoint;
  fileServer: FileServerEndpoint;
  referenceEndpoint: ReferencesEndpoint;
  virtualClassroomEndpoint = new VirtualClassRoomEndpoint();
  referenceData: ReferenceResponses[];
  isLoadingResults: boolean = false;
  stepHeaderControl: StepHeaderControl;
  displayedColumns: string[] = ['name', 'status', 'action'];
  displayedColumns2: string[] = ['position', 'name', 'action'];
  lessonName: string;
  teacherZoomEndpoint = new TeacherZoomEndpoint();
  hasAddLessonPermission: boolean;
  hasEditLessonPermission: boolean;
  hasDeleteLessonPermission: boolean;
  userIdentity: UserIdentity;
  isReady: boolean = false;
  constructor(public dialog: MatDialog,
    private _authService: AuthService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private _router: Router,
    private _learnMessageService: LearnMessageService) {
    this.lessonId = +this.route.snapshot.params.id;
    if (this.lessonId < 1) {
      return;
    }
    this.lessonEndpoint = new LessonEndpoint();
    this.contentEndpoint = new ContentEndpoint();
    this.fileServer = new FileServerEndpoint();
    this.referenceEndpoint = new ReferencesEndpoint();

    this.getLessonContentById();

    this.userIdentity = _authService.getTokenInfo();
    this.hasAddLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Add);
    this.hasEditLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Edit);
    this.hasDeleteLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Delete);


  }
  lessonId: number;
  lesson: Lesson = { name: "Phương trình bậc 2 và định lí viet ssssss " };
  isShowContent = false;
  useCase: UseCase = UseCase.creating
  listContentGroup: ContentGroupResponses;
  cgEndpoint = new ContentGroupEndpoint();

  ngOnInit(): void {
    this.getReferenceData();
  }
  getLessonContentById() {
    this.lessonEndpoint.getLessonContentsById(this.lessonId)
      .then(res => {
        if (res != null) {
          this.listContentGroup = res;
          console.log('this.listContentGroup', this.listContentGroup)
          this.listContentGroup.contentGroups.sort((a, b) => { return a.order - b.order })
          this.stepHeaderControl = {
            lessonId: this.lessonId,
            currentPage: 2,
            step: res.step
          };
          this.lessonName = res.name;
          this.isReady = true;
        };
      })
      .catch(err => {
        alert(" Có lỗi xảy ra vui lòng thử lại ")
      });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddReferenceDialogComponent, {
      width: '900px',
      maxHeight: '100vh',
      data: this.lessonId
    });

    dialogRef.afterClosed().subscribe((result: ReferenceRequest) => {
      if (result) {
        result.lessonId = this.lessonId;
        this.referenceEndpoint.addReference(result).then(res => {
          if (res) {
            this._learnMessageService.successToast();
            this.getReferenceData();
          }
        })
          .catch(err => {
            this._learnMessageService.failToast("Thêm thất bại");
          })
      }
    });
  }


  openPartDialog(): void {
    const dialogRef = this.dialog.open(AddPartDialogComponent, {
      width: "557px",
      height: "274px",
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      else {
        this.cgEndpoint.addContentGroup(this.lessonId, result).then(res => {
          if (res) {
            this._learnMessageService.successToast('Tạo thành công tiết học');
          }
        }).then(() => {
          this._router.navigate([`/dashboard/learn/edit-content-lesson/${this.lessonId}`]);
        }).catch((err: ErrorResponse) => {
          console.log(err);
        });
      }
    });
  }

  openEditPartDialog(id: number) {
    let data = this.listContentGroup.contentGroups.find(x => x.id == id);
    const dialogRef = this.dialog.open(EditPartDialogComponent, {
      width: "557px",
      height: "274px",
      data: data
    });

    dialogRef.afterClosed().subscribe((result: string) => {
      if (!result) {
        return;
      }
      else {
        this.cgEndpoint.editContentGroup(this.lessonId, data.id, result).then((res) => {
          alert("Đã cập nhật thành công");

        }).then(() => {
          this.getLessonContentById();
        }).catch()

      }
    });
  }

  handleShowHideAddContent() {
    this.isShowContent = !this.isShowContent;
  }

  /* Action Group content */
  editGroupContent(groupContentId: number) {
    this.openEditPartDialog(groupContentId)
  }

  deleteGroupContent(groupContentId: number) {
    this.openConfirmDialog(' Tiết học ').subscribe(res => {
      if (res) {
        this.cgEndpoint.deleteContentGroup(this.lessonId, groupContentId).then(res => console.log(res))
        this._learnMessageService.successToast('Xóa thành công tiết học');
        this.getLessonContentById();
      }
    })
  }

  /* Action content */
  editContent(value: number) {
    this._router.navigate([`/dashboard/learn/lesson/${this.lessonId}/update-content/${value}`]);
  }

  deleteContent(value: number) {
    this.openConfirmDialog("nội dung ").subscribe(res => {
      if (res) {
        this.contentEndpoint.deleteContent(this.lessonId, value).then(res => {
          this._learnMessageService.successToast('Xóa thành công');
          this.getLessonContentById();
        })
      }
    })
  }

  onNextButtonClick(): void {
    setTimeout(() => {
      if (this.stepHeaderControl.step < 3) {
        this._router.navigate([`/dashboard/learn/config-lesson/${this.lessonId}`]);
      } else {
        this._router.navigate([`/dashboard/learn/edit-lesson-config/${this.lessonId}`]);
      }
    }, 300)
  }

  openConfirmDialog(value: string): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      height: '160px',
      data: {
        message: ` Bạn có muốn xóa ${value} hay không `,
        buttonText: {
          ok: 'Có',
          cancel: 'Không'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }

  private getReferenceData() {
    this.referenceEndpoint.getListReferenceByLessonId(this.lessonId)
      .then(res => {
        this.referenceData = res;
      })
  }

  public onEditReferenceHandler(val: number) {
    this.referenceEndpoint.getReferenceById(val)
      .then(res => {
        const dialogRef = this.dialog.open(EditReferenceDialogComponent, {
          width: '860px',
          height: 'auto',
          data: res
        });
        dialogRef.afterClosed().subscribe((result: ReferenceRequest) => {
          if (result) {
            this.referenceEndpoint.editReference(result).then(res => {
              if (res) {
                this._learnMessageService.successToast('Sửa thành công');
                this.getReferenceData();
              }
            });
          }
        })
      })
  }

  onRemoveReferenceHandler(val: number) {
    this.openRemoveDialog().subscribe(res => {
      if (res) {
        this.referenceEndpoint.removeReference(0, val).then(res => {
          if (res) {
            this._learnMessageService.successToast(` Xóa thành công `);
            this.getReferenceData();
          } else {
            this._learnMessageService.failToast(` Có lỗi xảy ra vui lòng thử lại `);
          }
        })
          .catch((err) => {
            this._learnMessageService.failToast(` Xóa thất bại. Vui lòng thử lại `);
          })
      }
    })
  }

  openRemoveDialog(): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '300px',
      height: '160px',
      data: {
        message: ' Bạn có muốn xóa hay không ',
        buttonText: {
          ok: 'XÓA',
          cancel: 'HỦY'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }
  /*
   * open progress dialog 
   */
  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.numbers }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  /**24/08/2021 LAMNV Add check online class room  */
  public onAddOnlineClassroom() {
    this._router.navigateByUrl(`dashboard/learn/lesson/${this.lessonId}/add-interactive-classroom`);
    // this._router.navigate([`/dashboard/learn/lesson/',this.lessonId,'add-interactive-classroom`]);

  }
}
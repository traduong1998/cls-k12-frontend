import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddContentLessonComponent } from './add-content-lesson.component';

describe('AddContentLessonComponent', () => {
  let component: AddContentLessonComponent;
  let fixture: ComponentFixture<AddContentLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddContentLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddContentLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

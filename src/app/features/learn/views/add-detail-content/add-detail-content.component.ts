import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/lessonEndpoint';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { StepHeaderControl } from '../../intefaces/stepHeaderControl';

@Component({
  selector: 'app-add-detail-content',
  templateUrl: './add-detail-content.component.html',
  styleUrls: ['./add-detail-content.component.scss']
})
export class AddDetailContentComponent implements OnInit {

  stepHeaderControl: StepHeaderControl;
  lessonEnpoint: LessonEndpoint;
  lessonId: number;
  lessonName: string;
  isReady: boolean = false;

  constructor(private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.lessonId = + this.route.snapshot.paramMap.get("id");
    this.lessonEnpoint = new LessonEndpoint();
    this.lessonEnpoint.getLessonById(this.lessonId).then((lesson) => {
      this.stepHeaderControl = {
        currentPage: 2,
        isCreatingContent: true,
        lessonId: lesson.id,
        step: lesson.step
      }
      this.lessonName = lesson.name;
      this.isReady = true;
    });
  }

  ngOnInit(): void {
  }
}


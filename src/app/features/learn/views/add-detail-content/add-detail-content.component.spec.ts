import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDetailContentComponent } from './add-detail-content.component';

describe('AddDetailContentComponent', () => {
  let component: AddDetailContentComponent;
  let fixture: ComponentFixture<AddDetailContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDetailContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDetailContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

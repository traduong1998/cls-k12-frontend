import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { StepHeaderControl } from '../../intefaces/stepHeaderControl';

@Component({
  selector: 'app-add-video-content',
  templateUrl: './add-video-content.component.html',
  styleUrls: ['./add-video-content.component.scss']
})
export class AddVideoContentComponent implements OnInit {

  stepHeaderControl: StepHeaderControl;
  lessonEnpoint: LessonEndpoint;
  lessonId:number;
  lessonName: string;
  isReady: boolean = false;

  constructor(private route: ActivatedRoute) {
    this.lessonEnpoint = new LessonEndpoint();
    this.lessonId = + this.route.snapshot.paramMap.get("id");
    this.lessonEnpoint.getLessonById(this.lessonId).then((lesson)=>{
      this.stepHeaderControl = {
        currentPage: 2,
        isCreatingContent: true,
        lessonId: lesson.id,
        step: lesson.step
      }
      this.lessonName = lesson.name;
      this.isReady = true;
    });
  }
  ngOnInit(): void {
  }

}

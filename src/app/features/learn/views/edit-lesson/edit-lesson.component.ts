import { Component, ElementRef, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSelect } from '@angular/material/select';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { CKEditor4, CKEditorComponent } from 'ckeditor4-angular';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, of, ReplaySubject, Subject } from 'rxjs';
import { pairwise, startWith, take, takeUntil } from 'rxjs/operators';
import { CLSModules, CLSPermissions, GradeEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/errorResponse';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { GlobalEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { Curriculum } from 'sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum';
import { CurriculumsFilterRequests } from 'sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum-filter-request';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { ChaptersEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/chaptersEndpoint';
import { SubjectsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/subjectsEndpoint';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { AuthService } from 'src/app/core/services';
import { MimeTypes } from 'src/app/shared/enums/mime-type';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import videojs from 'video.js';
import { ProgressDialogComponent } from '../../components/progress-dialog/progress-dialog.component';
import { Chapter } from '../../intefaces/chapter';
import { StepHeaderControl } from '../../intefaces/stepHeaderControl';
import { ObserversModule } from '@angular/cdk/observers';
import { Unit } from '../../intefaces/unit';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { CurriculumEndpoint } from 'sdk/cls-k12-sdk-js/src/services/curriculum/endpoint/curriculum-endpoint';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { Configuration } from 'src/app/shared/configurations';
import { EStatusConvert } from '../../intefaces/eStatusConvert';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';

@Component({
  selector: 'learn-edit-lesson',
  templateUrl: './edit-lesson.component.html',
  styleUrls: ['./edit-lesson.component.scss']
})
export class EditLessonComponent implements OnInit {

  progressData: Observable<any>;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }

  private FILE_LIMIT: number = 500;
  numbers: 0;
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>
  public acceptImage = '.png,.jpg,.gif,.jpeg';
  baseApiUrl = 'http://localhost:58910';
  imageToolTip = Configuration.MESSAGE_INFOR_IMAGE;
  videoTooltip = Configuration.MESSAGE_INFOR_VIDEO;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  fileServer: FileServerEndpoint;
  playerLearn: videojs.Player;
  @ViewChild('videoLearn', { static: true }) videoLearn: ElementRef<HTMLVideoElement>;

  @ViewChild('imageContextMenu') public imageContextMenu: ContextMenuComponent;
  @ViewChild('videoContextMenu') public videoContextMenu: ContextMenuComponent;

  videoType: string = 'video';
  imageType: string = 'hình ảnh'
  statusVideo: 'INP' | 'COM' | 'NON' | 'FAI' | '';
  fileImageName: string;
  fileVideoName: string;
  isSaved: boolean = false;
  form: FormData = new FormData();
  isLoadingResults = false;
  lessonId: number;
  lesson: Lesson = undefined;
  lessonClone;
  lessonViewModel: Lesson;
  gradeIdSelected: number;
  subjectIdSelected: number;
  panelId: number;
  accessRange: number;
  imageLesson: File;
  videoLesson: File;
  introductionLesson: any;
  gradeEndpoint: GradeEndpoint;
  lessonEndpoint: LessonEndpoint;

  contextmenu = false;
  contextmenuX = 0;
  contextmenuY = 0;
  /* School */
  protected grades: GradeOption[];
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  /* Subject */
  protected subjects: SubjectOption[];
  public subjectFilterCtrl: FormControl = new FormControl();
  public filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  @ViewChild('singleSelectSubject', { static: true }) singleSelectSubject: MatSelect;

  /* Curriculum */
  protected contentPanels: Curriculum[];
  public contentPanelFilterCtrl: FormControl = new FormControl();
  public filteredContentPanels: ReplaySubject<Curriculum[]> = new ReplaySubject<Curriculum[]>(1);
  @ViewChild('singleSelectContentPanel', { static: true }) singleSelectContentPanel: MatSelect;

  /* Chapter */
  protected chapters: Chapter[];
  public chapterFilterCtrl: FormControl = new FormControl();
  public filteredChapters: ReplaySubject<Chapter[]> = new ReplaySubject<Chapter[]>(1);
  @ViewChild('singleSelectChapter', { static: true }) singleSelectChapter: MatSelect;
  isRequireChooseChaper: boolean = true;

  /* Unit */
  protected units: Unit[];
  public unitFilterCtrl: FormControl = new FormControl();
  public filteredUnits: ReplaySubject<Unit[]> = new ReplaySubject<Unit[]>(1);
  @ViewChild('singleSelectUnit', { static: true }) singleSelectUnit: MatSelect;

  protected _onDestroy = new Subject<void>();

  @ViewChild("inputImageFile") inputImageFile: ElementRef;
  @ViewChild("inputVideoFile") inputVideoFile: ElementRef;
  @ViewChild("ckIntroductionLesson") introductionVideo: CKEditorComponent;

  editLessonEndpoint: LessonEndpoint

  endpointGrade: GradeEndpoint;
  endpointSubject: SubjectsEndpoint;
  endpointContentPanel: GlobalEndpoint;
  curriculumEndpoint: CurriculumEndpoint;
  endpointChapter: ChaptersEndpoint;
  userIdentity: UserIdentity;
  stepHeaderControl: StepHeaderControl
  isReady: boolean = false;

  hasAddLessonPermission: boolean;
  hasEditLessonPermission: boolean;

  createLessonForm = this.fb.group({
    id: '',
    name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    gradeId: new FormControl('', [Validators.required]),
    subjectId: new FormControl('', [Validators.required]),
    contentPanelId: new FormControl(''),
    chapterId: new FormControl(''),
    unitId: new FormControl(''),
    imageURL: [''],
    fileImageURLName: [''],
    videoURL: [''],
    videoURLId: [''],
    fileVideoURLName: [''],
    introduction: ['']

  });
  get name() { return this.createLessonForm.get('name'); }

  get gradeId() { return this.createLessonForm.get('gradeId'); }
  isFirstLoadGrades: boolean = true;

  get subjectId() { return this.createLessonForm.get('subjectId'); }
  isFirstLoadSubjects: boolean = true;
  isDisableSubjectId: boolean = false;

  get contentPanelId() { return this.createLessonForm.get('contentPanelId'); }
  isFirstLoadContentPanels: boolean = true;
  isDisableContentPanelId: boolean = false;

  get chapterId() { return this.createLessonForm.get('chapterId'); }
  isFirstLoadChapters: boolean = true;
  isDisableChapterId: boolean = false;

  get unitId() { return this.createLessonForm.get('unitId'); }
  isFirstLoadUnits: boolean = true;
  isDisableUnitId: boolean = false;

  get introduction() { return this.createLessonForm.get('introduction'); }


  videoMimeTypeSupports = [MimeTypes.m3u8, MimeTypes.mp4];
  currentMimeType = 0;
  

  constructor(private router: Router,
    private fb: FormBuilder,
    private _learnMessageService: LearnMessageService,
    private _route: ActivatedRoute,
    private authService: AuthService,
    private dialog: MatDialog,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.isLoadingResults = false;
    this.lessonId = +this._route.snapshot.paramMap.get('id');
    this.editLessonEndpoint = new LessonEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubject = new SubjectsEndpoint({ baseUrl: this.baseApiUrl }) // TODO : system service is not have API
    this.endpointContentPanel = new GlobalEndpoint(); // TODO : system service is not have API
    this.curriculumEndpoint = new CurriculumEndpoint();
    this.endpointChapter = new ChaptersEndpoint({ baseUrl: this.baseApiUrl }) // TODO : system service is not have API
    this.lessonEndpoint = new LessonEndpoint({ baseUrl: this.baseApiUrl });
    this.fileServer = new FileServerEndpoint();
    this.userIdentity = authService.getTokenInfo();

    this.hasAddLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Add);
    this.hasEditLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Edit);
    this.statusVideo = '';

  }

  ngOnInit(): void {

  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  imageUrl: any = null;
  videoUrl: any = null;

  configCkeditor = {
    // startupFocus: true,
    height: 150,
    allowedContent: false,
    EditorType: CKEditor4.EditorType.CLASSIC,
    removeButtons: 'Image'
  };

  editorType: CKEditor4.EditorType.CLASSIC;

  onChangeImageInput(event): void {
    let file: File = null;
    if (event.target.files && event.target.files[0]) {
      file = event.target.files[0];

      // Kiểm tra limit file
      let confirmLimit = Configuration.getLimitFileMessage(file);
      if (!confirmLimit.status) {
        this._learnMessageService.failToast(confirmLimit.message);
        return;
      }

      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      Promise.resolve()
        .then(() => {
          console.log(`start process`);
          this.openProgressDialog(null);
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.fileServer.uploadFile({
            file: file,
            isPrivate: false,
            moduleName: ServiceName.LEARN
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              var subscription = this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          this.imageUrl = res ? res.link : null;
          this.fileImageName = res?.name;
          this.createLessonForm.controls['videoURLId'].setValue(res?.fileId);
          this.createLessonForm.controls['imageURL'].setValue(this.imageUrl);
          this.createLessonForm.controls['fileImageURLName'].setValue(res?.name);
        })
        .then((res) => {
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err);
          this.createLessonForm.controls['imageURL'].setValue("");
          this.createLessonForm.controls['fileImageURLName'].setValue("");
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: null, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }

  onChangeVideoInput(event): void {
    var file: File = null;
    if (event.target.files && event.target.files[0]) {
      file = event.target.files[0];
      // Kiểm tra limit file
      let confirmLimit = Configuration.getLimitFileMessage(file);
      if (!confirmLimit.status) {
        this._learnMessageService.failToast(confirmLimit.message);
        return;
      }

      Promise.resolve()
        .then(() => {
          console.log(`start process`);
          this.openProgressDialog(null);
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.fileServer.uploadFile({
            file: file,
            isPrivate: true,
            moduleName: ServiceName.LEARN
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          this.statusVideo = '';
          this.fileServer.skipFile(this.createLessonForm.controls['videoURLId'].value).then(resSkip => {
          });
          return res;

        })
        .then((res) => {
          this.videoUrl = res ? res.link : null;
          this.fileVideoName = res?.name;
          this.statusVideo = 'INP';
          this.createLessonForm.controls['videoURLId'].setValue(res.fileId);
          this.createLessonForm.controls['videoURL'].setValue(this.videoUrl);
          this.createLessonForm.controls['fileVideoURLName'].setValue(res?.name);

          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err);
          this.statusVideo = 'NON';
          this.videoUrl = null;
          this.createLessonForm.controls['videoURLId'].setValue("");
          this.createLessonForm.controls['imageURL'].setValue("");
          this.createLessonForm.controls['fileImageURLName'].setValue("");
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }

  uploadImage(): void {
    this.inputImageFile.nativeElement.click();
  }

  uploadVideo(): void {
    this.inputVideoFile.nativeElement.click();
  }

  onChangeImageHandleClick() {
    if (this.createLessonForm.controls['imageURL'].value) {
      return;
    }
    this.inputImageFile.nativeElement.click();
  }

  onChangeVideoHandleClick() {
    if (this.createLessonForm.controls['videoURL'].value) {
      return;
    }
    this.inputVideoFile.nativeElement.click();
  }

  submitForm() {

    let editor = this.introductionVideo.instance;
    // nLesson = editor.document.getBody().getText();
  }

  onSubmit() {
    // Kiểm tra quyền
    if (!this.hasEditLessonPermission) {
      console.error(" Does not have permision ");
      return;
    }

    // show spinner
    this.spinner.show();

    // check valid formcontrol
    this.createLessonForm.markAllAsTouched();
    if (this.createLessonForm.valid) {
      let current: Lesson = this.createLessonForm.value;
      if (JSON.stringify(this.lessonClone) === JSON.stringify(current)) {

        if (this.lesson.step < 2) {
          this.spinner.hide();
          this.router.navigate([`/dashboard/learn/add-content-lesson/${this.lessonId}`]);
        } else {
          this.router.navigate([`/dashboard/learn/edit-content-lesson/${this.lessonId}`]);
        }
      } else {
        this.lessonEndpoint.editInfomationLesson(this.createLessonForm.value).then(res => {
          if (res) {

            this._learnMessageService.successToast(" Đã chỉnh sửa")
            this.lessonClone = JSON.parse(JSON.stringify(current));
            setTimeout(() => {
              this.spinner.hide();
              if (this.lesson.step < 2) {
                this.router.navigate([`/dashboard/learn/add-content-lesson/${res.id}`]);
              } else {
                this.router.navigate([`/dashboard/learn/edit-content-lesson/${res.id}`]);
              }
            }, 2000)
          }
        })
          .then(() => {
          })
          .catch((err: ErrorResponse) => {
            this.spinner.hide();
            this._learnMessageService.failToast(" Có lỗi xảy ra")
            return;
          });
      }
    }
  }

  onSaveButtonHandleClick() {
    this.spinner.show();
    if (this.createLessonForm.valid) {
      let current: Lesson = this.createLessonForm.value;

      if (JSON.stringify(this.lessonClone) !== JSON.stringify(current)) {
        this.lessonEndpoint.editInfomationLesson(this.createLessonForm.value).then(res => {
          if (res) {
            this.spinner.hide();
            this._learnMessageService.successToast(" Đã chỉnh sửa")
            this.lessonClone = JSON.parse(JSON.stringify(current));
          }
        })
          .then(() => {
          })
          .catch((err: ErrorResponse) => {
            this.spinner.hide();
            this._learnMessageService.failToast(" Có lỗi xảy ra")
            return;
          });

      }
      else {
        this.spinner.hide();
        this._learnMessageService.failToast(" Bạn không chỉnh sửa gì ")
      }
    }
  }

  submitLesson(id: number) {
    this.router.navigate([`/dashboard/learn/edit-content-lesson/${id}`])
  }

  ngAfterViewInit() {

    videojs.Hls.xhr.beforeRequest = options => {
      options.headers = options.headers || {};
      var token = `Bearer ${this.authService.getToken()}`;
      options.headers["Authorization"] = token;
    }
    this.playerLearn = videojs(this.videoLearn.nativeElement, { autoplay: false, controls: true }, () => {
      if (this.lessonId) {
        this.editLessonEndpoint.getLessonById(this.lessonId).then(res => {
          this.lesson = res;
          this.imageUrl = this.lesson.avatar;
          this.fileImageName = this.lesson.fileAvatarName;
          this.createLessonForm.controls['fileImageURLName'].setValue(this.lesson.fileAvatarName);

          this.videoUrl = this.lesson.videoURL;
          this.fileVideoName = this.lesson.fileVideoName;
          this.createLessonForm.controls['fileVideoURLName'].setValue(this.lesson.fileVideoName);

          // set FileId
          this.createLessonForm.controls['videoURLId'].setValue(res.videoURLId);

          if (this.videoUrl) {
            let typeVideo = MimeTypes.m3u8;
            if(this.videoUrl.includes('.mp4')){
              typeVideo = MimeTypes.mp4
            }
            this.playerLearn.src({ src: this.videoUrl, type: typeVideo });

            this.playerLearn.on('loadedmetadata', () => {
              console.log("video loadedmetadata");
              this.statusVideo = 'COM';
            });

            this.playerLearn.on('error', () => {
              // Xử lý nếu file trả về là video mp4
              if(this.playerLearn.error().code === 4){
                this.currentMimeType++;
                if (this.currentMimeType > this.videoMimeTypeSupports.length) {
                  return false;
                }
                this.playerLearn.src({ src: this.videoUrl, type: this.videoMimeTypeSupports[this.currentMimeType] });
              }
              res.videoURLId && this.fileServer.getStatusFile(res.videoURLId).then(res => {
                switch (res) {
                  case EStatusConvert.Waiting:
                  case EStatusConvert.Inprogress:
                    this.statusVideo = 'INP';
                    break;
                  case EStatusConvert.Failed:
                  case EStatusConvert.Skip:
                    this.statusVideo = 'FAI';
                    break;
                  default:
                    break;
                }
              })
              
            });
          }

          this.stepHeaderControl = {
            currentPage: 1,
            lessonId: this.lessonId,
            step: this.lesson.step
          }

          this.createLessonForm.controls['id'].patchValue(this.lesson.id);
          this.createLessonForm.controls['name'].patchValue(this.lesson.name);
          this.createLessonForm.controls['gradeId'].patchValue(this.lesson.gradeId);
          this.createLessonForm.controls['subjectId'].patchValue(this.lesson.subjectId);
          this.createLessonForm.controls['contentPanelId'].patchValue(this.lesson.contentPanelId);
          this.createLessonForm.controls['chapterId'].patchValue(this.lesson.chapterId);

          this.createLessonForm.controls["unitId"].patchValue(this.lesson.unitId);

          this.createLessonForm.controls['imageURL'].patchValue(this.lesson.avatar);
          this.createLessonForm.controls['videoURL'].patchValue(this.lesson.videoURL);
          this.createLessonForm.controls['introduction'].patchValue(this.lesson.introduction);
          this.lessonClone = { ...this.createLessonForm.value };
          this.isReady = true;



        }).then(res => {

          this.onChapterSelectClicked()
          this.onContentPanelSelectClicked();
          this.onSubjectSelectClicked();
          this.onGradeSelectClicked();
          this.onUnitSelectClicked();
          this.isReady = true;
        })
      }
    });


    this.gradeId.valueChanges
      .pipe(startWith(this.gradeId.value),
        pairwise())
      .subscribe(

        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetSubjectSelect();
            this.onResetContentPanelSelect();
            this.onResetChapterSelect();
            this.onResetUnitSelect();

            if (value < 1) {

              this.isDisableSubjectId = true;
              this.isDisableContentPanelId = true;
              this.isDisableChapterId = true;
              this.isDisableUnitId = true;
            } else {

              this.isDisableSubjectId = false;
              if (this.subjectId.value > 0) {

                this.isDisableContentPanelId = false;
                this.isDisableChapterId = false;
                this.isDisableUnitId = false;
              }
              else {
                this.isDisableContentPanelId = true;
                this.isDisableChapterId = true;
                this.isDisableUnitId = true;
              }
            }
          }
        }
      )

    this.subjectId.valueChanges
      .pipe(startWith(this.subjectId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
            this.onResetContentPanelSelect();
            this.onResetChapterSelect();
            this.onResetUnitSelect();
            if (value < 1) {

              this.isDisableContentPanelId = true;
              this.isDisableChapterId = true;
              this.isDisableUnitId = true;
            } else {
              this.isDisableContentPanelId = false;
            }
          }
        });

    this.contentPanelId.valueChanges
      .pipe(startWith(this.contentPanelId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset changes
            this.onResetChapterSelect();
            this.onResetUnitSelect();

            if (value < 1) {
              this.isDisableChapterId = true;
              this.isDisableUnitId = true;
              this.createLessonForm.controls["chapterId"].setErrors(null);
              this.createLessonForm.controls["chapterId"].clearValidators();
              this.createLessonForm.controls["chapterId"].updateValueAndValidity();

              this.createLessonForm.controls["unitId"].setErrors(null);
              this.createLessonForm.controls["unitId"].clearValidators();
              this.createLessonForm.controls["unitId"].updateValueAndValidity();
            } else {
              this.isDisableChapterId = false;
              this.createLessonForm.controls["chapterId"].setValidators([Validators.required]);

              this.createLessonForm.controls["unitId"].setValidators([Validators.required]);

              this.createLessonForm.controls["unitId"].updateValueAndValidity();
              this.createLessonForm.controls["chapterId"].updateValueAndValidity();

              this.chapterId.markAsTouched();
              this.chapterId.markAsDirty();

              this.unitId.markAsTouched();
              this.unitId.markAsDirty();
            }
          }
        }
      );

    this.chapterId.valueChanges
      .pipe(startWith(this.chapterId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
            this.onResetUnitSelect();

            if (value < 1) {
              this.isDisableUnitId = true;

            } else {
              this.isDisableUnitId = false;

            }
          }
        }
      );

    this.unitId.valueChanges
      .pipe(startWith(this.unitId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {

        }
      );

    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    this.subjectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSubjects();
      });

    this.contentPanelFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterContentPanels();
      });

    this.chapterFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterChapters();
      });

    this.unitFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterUnits();
      });

    // this.setInitialValue();

    if (this.contentPanelId.value !== null || this.contentPanelId.value !== 0) {


    } else {
      this.chapterId.markAsTouched();
      this.chapterId.markAsDirty();

      this.unitId.markAsTouched();
      this.unitId.markAsDirty();
    }


    // this.confirmBeforLoader();
    // this.introductionVideo.on()
  }
  // ngAfterViewChecked() {
  //   let editor = this.introductionVideo.instance;
  //   editor.on('key', function (e) {
  //     console.log("key")
  //   });
  // }
  protected setInitialValue() {

    this.filteredSubjects
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSubject.compareWith = (a: SubjectOption, b: SubjectOption) => a && b && a.id === b.id;
      });

    this.filteredContentPanels
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectContentPanel.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
      });

    this.filteredChapters
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectChapter.compareWith = (a: Chapter, b: Chapter) => a && b && a.id === b.id;
      });
    this.isLoadingResults = false;
  }

  /* reset change Subject */
  private onResetSubjectSelect() {

    this.createLessonForm.controls['subjectId'].setValue(null);
    this.isFirstLoadSubjects = true;

    this.subjectId.markAsTouched();
    this.subjectId.markAsDirty();
  }

  /* reset change ContentPanel */
  private onResetContentPanelSelect() {

    this.createLessonForm.controls['contentPanelId'].setValue(null);
    this.isFirstLoadContentPanels = true;
  }

  /* reset change Chapter */
  private onResetChapterSelect() {

    this.createLessonForm.controls['chapterId'].setValue(null);
    this.isFirstLoadChapters = true;
  }

  /* reset change Unit*/
  private onResetUnitSelect() {
    this.createLessonForm.controls['unitId'].setValue(null);
    this.isFirstLoadUnits = true;
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSubjects() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.subjectFilterCtrl.value;
    if (!search) {
      this.filteredSubjects.next(this.subjects.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSubjects.next(
      this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterContentPanels() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.contentPanelFilterCtrl.value;
    if (!search) {
      this.filteredContentPanels.next(this.contentPanels.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredContentPanels.next(
      this.contentPanels.filter(contentPanel => contentPanel.curriculumName.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterChapters() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.chapterFilterCtrl.value;
    if (!search) {
      this.filteredChapters.next(this.chapters.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredChapters.next(
      this.chapters.filter(chapter => chapter.curriculumName.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterUnits() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.unitFilterCtrl.value;
    if (!search) {
      this.units && this.filteredUnits.next(this.units.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredUnits.next(
      this.units.filter(unit => unit.curriculumName.toLowerCase().indexOf(search) > -1)
    );
  }

  /* Get Grade */
  onGradeSelectClicked() {
    this.name.markAsTouched();
    this.name.markAsDirty();
    if (this.isFirstLoadGrades) {
      this.endpointGrade.getGradeOptions(this.userIdentity.schoolId)
        .then(res => {
          this.grades = res;

          if (this.grades && !this.grades.find(x => x.id == 0 || x.id == null)) {
            this.grades.unshift({ id: null, name: 'Chọn khối' });
          }

          this.filteredGrades.next(this.grades.slice());
          this.isFirstLoadGrades = false;
        })
        .catch(err => { console.log(err) }
        )
    }

  }

  /* Get Subject */
  onSubjectSelectClicked() { // TODO : system service was not have this API
    if (!this.isFirstLoadSubjects || this.isDisableSubjectId == true || this.gradeId.value == null || this.gradeId.value == 0) {
      return;
    } else {
      this.endpointSubject.getSubjectByGradeIdOptions(this.gradeId.value).then(res => {
        this.subjects = res;

        if (this.subjects && !this.subjects.find(x => x.id == 0 || x.id == null)) {
          this.subjects.unshift({ id: null, name: 'Chọn môn' });
        }
        this.filteredSubjects.next(this.subjects.slice());
        this.isFirstLoadSubjects = false;
      }).catch(err => {
        // sthis.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
    }
  }

  /* Get contentPanel */
  onContentPanelSelectClicked() { // TODO : system service was not have this API
    if (this.isDisableContentPanelId || !this.isFirstLoadContentPanels
      || this.gradeId.value == 0 || this.gradeId.value == null
      || this.subjectId.value == 0 || this.subjectId.value == null) {
      return;
    }
    this.curriculumEndpoint
      .getComboboxCurriculumCreateUpdate(this.userIdentity.divisionId, this.userIdentity.schoolId, this.gradeId.value, this.subjectId.value)
      .then(res => {
        this.contentPanels = res;

        if (this.contentPanels && !this.contentPanels.find(x => x.curriculumId == 0 || x.curriculumId == null)) {
          this.contentPanels.unshift({ curriculumId: 0, curriculumName: 'Chọn khung', subjectName: 'subjectName', gradeName: 'gradeName', isActived: true, getActiveStatus: '' });
        }
        this.filteredContentPanels.next(this.contentPanels.slice())
        this.isFirstLoadContentPanels = false;
      })
      .catch(err => {
        // s  this.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      })
  }

  /* Get Chapter */
  onChapterSelectClicked() { // TODO : system service was not have this 
    if (!this.isFirstLoadChapters || this.isDisableChapterId || this.contentPanelId.value == null || this.contentPanelId.value == 0) {
      return;
    }
    this.endpointContentPanel
      .getCurriculumChildrentNodesByPortalId(this.userIdentity.portalId, this.contentPanelId.value)
      .then((res) => {
        this.chapters = res;

        if (this.chapters && !this.chapters.find(x => x.id == 0 || x.id == null)) {
          this.chapters.unshift({ id: null, curriculumName: 'Chọn chương' });
        }
        this.filteredChapters.next(this.chapters.slice());
        this.isFirstLoadChapters = false;
      })
      .catch(err => {
        // s  this.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
  }

  /* Get Unit */
  onUnitSelectClicked() { // TODO : system service was not have this APId

    let valueTest = this.createLessonForm.controls["unitId"].value;
    let unitsDisplay = this.units;
    if (!this.isFirstLoadUnits || !this.onResetUnitSelect || this.isDisableUnitId) {
      return;
    }

    this.endpointContentPanel.
      getCurriculumChildrentNodesByPortalId(this.userIdentity.portalId, this.chapterId.value)
      .then(res => {
        this.units = res;
        if (this.units && !this.units.find(x => x.id == 0 || x.id == null)) {
          this.units.unshift({ id: null, curriculumName: 'Chọn bài học' });
        }
        this.filteredUnits.next(this.units.slice());
        this.isFirstLoadUnits = false;
      })
      .catch(err => {
        // s  this.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
  }

  /* context menu */
  onContextMenuRemoveImageHandler() {
    this.imageUrl = null;
    this.createLessonForm.controls['imageURL'].setValue(null);
    this.fileImageName = '';
    this.createLessonForm.controls['fileImageURLName'].setValue(null);
  }

  onContextMenuRemoveVideoHandler() {
    this.videoUrl = null;
    this.createLessonForm.controls['videoURL'].setValue(null);
    this.fileVideoName = '';
    this.createLessonForm.controls['fileVideoURLName'].setValue(null);
    // this.playerLearn.src({ src: this.videoUrl, type: MimeTypes.m3u8 });
  }
  /*
   * open progress dialog 
   */
  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.numbers }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  onClickBack() {
    let current: Lesson = this.createLessonForm.value;
    if (JSON.stringify(this.lessonClone) != JSON.stringify(current)) {
      this.openConfirmDialog(Configuration.MESSAGE_EXIT_CONFIRM).subscribe((confirm) => {
        if (!confirm) {
          return;
        } else {

          this.router.navigateByUrl('dashboard/learn/list-lesson');
        }
      })
    } else { this.router.navigateByUrl('dashboard/learn/list-lesson'); }
  }

  private openConfirmDialog(value: string): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: {
        message: `${value}`,
        buttonText: {
          ok: 'Có',
          cancel: 'Không'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }

  @HostListener('window:beforeunload', ['$event'])
  unloadHandler($event) {
    let current: Lesson = this.createLessonForm.value;
    if (JSON.stringify(this.lessonClone) === JSON.stringify(current)) {

    } else {

      return false
    }
  }
}

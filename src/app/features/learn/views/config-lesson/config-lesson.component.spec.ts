import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigLessonComponent } from './config-lesson.component';

describe('ConfigLessonComponent', () => {
  let component: ConfigLessonComponent;
  let fixture: ComponentFixture<ConfigLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

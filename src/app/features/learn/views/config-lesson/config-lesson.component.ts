import { StepHeaderControl } from './../../intefaces/stepHeaderControl';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfigLessonResponses } from './../../../../../../sdk/cls-k12-sdk-js/src/services/lesson/responses/configLessonResponses';
import { LessonEndpoint } from 'cls-k12-sdk-js/src/services/lesson/endpoints/lessonEndpoint';
import { Subject } from 'rxjs';
import { Component, Input, OnInit } from '@angular/core';
import { UseCase } from '../../intefaces/useCase';
import { AuthService } from 'src/app/core/services';
import { CLSModules, CLSPermissions, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-config-lesson',
  templateUrl: './config-lesson.component.html',
  styleUrls: ['./config-lesson.component.scss']
})
export class ConfigLessonComponent implements OnInit {

  eventsSubject: Subject<string> = new Subject<string>();
  events$ = this.eventsSubject.asObservable();
  lessonEndPoint: LessonEndpoint;
  lessonConfig: ConfigLessonResponses;
  hasAddLessonPermission: boolean;
  hasEditLessonPermission: boolean;
  userIdentity: UserIdentity;

  lessonId: number;

  isReady: boolean = false;
  stepHeaderControl: StepHeaderControl;

  public get useCaseEnum(): typeof UseCase {
    return UseCase;
  }


  constructor(private _router: Router, private route: ActivatedRoute, private _authService: AuthService, private spinner: NgxSpinnerService) {
    //this.spinner.show();
    this.lessonId = +this.route.snapshot.paramMap.get("id");
    this.lessonEndPoint = new LessonEndpoint();
    this.stepHeaderControl = {
      lessonId: this.lessonId,
      currentPage: 3,
      step: 3
    }

    this.userIdentity = _authService.getTokenInfo();
    this.hasAddLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Add);
    this.hasEditLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Edit);
  }

  ngOnInit(): void {
    this.getLessonConfig();
  }
  emitEventClick() {
    if (this.hasAddLessonPermission || this.hasEditLessonPermission) {

      this.eventsSubject.next("save");
    } else {
      console.warn('Does not have permission');
    }
  }
  backToContent(): void {
    this.eventsSubject.next("back");
  }
  continue(): void {
    this.eventsSubject.next("next");
  }

  getLessonConfig(): void {
    this.lessonEndPoint.getLessonConfig(this.lessonId).then((res) => {
      this.lessonConfig = res;
      this.isReady = true;
    }).catch(err => {
      console.error(err)
      //this.spinner.hide();
    })
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEssayContentComponent } from './add-essay-content.component';

describe('AddEssayContentComponent', () => {
  let component: AddEssayContentComponent;
  let fixture: ComponentFixture<AddEssayContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEssayContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEssayContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

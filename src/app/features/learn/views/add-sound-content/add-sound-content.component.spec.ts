import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSoundContentComponent } from './add-sound-content.component';

describe('AddSoundContentComponent', () => {
  let component: AddSoundContentComponent;
  let fixture: ComponentFixture<AddSoundContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSoundContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSoundContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { StepHeaderControl } from './../../intefaces/stepHeaderControl';
import { Subject } from 'rxjs';
import { ConfigLessonResponses } from './../../../../../../sdk/cls-k12-sdk-js/src/services/lesson/responses/configLessonResponses';
import { LessonEndpoint } from 'cls-k12-sdk-js/src/services/lesson/endpoints/lessonEndpoint';
import { Router, ActivatedRoute } from '@angular/router';
import { UseCase } from './../../intefaces/useCase';
import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-lesson-config',
  templateUrl: './edit-lesson-config.component.html',
  styleUrls: ['./edit-lesson-config.component.scss']
})
export class EditLessonConfigComponent implements OnInit {

  eventsSubject: Subject<string> = new Subject<string>();
  events$ = this.eventsSubject.asObservable();
  lessonEndPoint: LessonEndpoint;
  lessonConfig: ConfigLessonResponses;
  lessonId: number;
  stepHeaderControl: StepHeaderControl;
  isReady: boolean = false;
  public get useCaseEnum(): typeof UseCase {
    return UseCase;
  }
  constructor(private _router: Router, private route: ActivatedRoute) {
    this.lessonId = +this.route.snapshot.paramMap.get("id");
    this.lessonEndPoint = new LessonEndpoint();
    this.stepHeaderControl = {
      currentPage: 3,
      lessonId :this.lessonId,
      step: 0
    }
  }

  ngOnInit(): void {
    this.getLessonConfig();
  }
  emitEventClick() {
    this.eventsSubject.next("save");
  }

  backToContent(): void {
    this.eventsSubject.next("back");
  }
  continue(): void {
    this.eventsSubject.next("next");
  }
  getLessonConfig(): void {
    this.lessonEndPoint.getLessonConfig(this.lessonId).then((res) => {
      this.lessonConfig = res;
      this.stepHeaderControl.step = this.lessonConfig.step;
      this.isReady = true;
    })
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditLessonConfigComponent } from './edit-lesson-config.component';

describe('EditLessonConfigComponent', () => {
  let component: EditLessonConfigComponent;
  let fixture: ComponentFixture<EditLessonConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditLessonConfigComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditLessonConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

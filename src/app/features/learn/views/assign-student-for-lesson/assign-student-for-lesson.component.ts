import { NgxSpinnerService } from 'ngx-spinner';
import { AssignFrom } from './../../intefaces/assignFrom';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserIdentity } from 'cls-k12-sdk-js/src';
import { MatSelect } from '@angular/material/select';
import { AuthService } from './../../../../core/services/auth.service';
import { FormBuilder, FormControl } from '@angular/forms';
import { AssignAndApprovalStudentRequest } from 'cls-k12-sdk-js/src/services/lesson/requests/assignStudentsRequest';
import { AssignStudentsEndpoint, DivisionEndpoint, DivisionOption, GradeEndpoint, GroupStudentEndpoint, SchoolEndpoint } from 'cls-k12-sdk-js/src';
import { ListStudentRegisterResponse } from 'cls-k12-sdk-js/src/services/lesson/responses/listStudentRegisterResponse';
import { ListStudentForAssignedResponse } from 'cls-k12-sdk-js/src/services/lesson/responses/listStudentForAssignedResponse';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { Action } from '../../intefaces/action';
import { SelectionModel } from '@angular/cdk/collections';
import { pairwise, startWith, take, takeUntil } from 'rxjs/operators';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { Subject, ReplaySubject } from 'rxjs';
import { PaginatorResponse } from 'cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { StudentAssignPagingRequest } from 'cls-k12-sdk-js/src/services/lesson/models/StudentAssignPagingRequest';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { StudentRegistrationPagingRequest } from 'cls-k12-sdk-js/src/services/lesson/models/StudentRegistrationPagingRequest';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
export interface Status {
  id?: boolean,
  name: string;
}

@Component({
  selector: 'assign-student-for-lesson',
  templateUrl: './assign-student-for-lesson.component.html',
  styleUrls: ['./assign-student-for-lesson.component.scss']
})
export class AssignStudentForLessonComponent implements OnInit {
  baseApiUrl = '';

  listStudentForAssignedDefault: PaginatorResponse<ListStudentForAssignedResponse> = {
    items: [],
    totalItems: 0
  };

  listStudentForRegistrationDefault: PaginatorResponse<ListStudentRegisterResponse> = {
    items: [],
    totalItems: 0
  };

  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointGroupStudent: GroupStudentEndpoint
  userIdentity: UserIdentity;
  levelManageValue: number = 0;

  //Paging List Student Assign
  assignDataSource: PaginatorResponse<ListStudentForAssignedResponse> = this.listStudentForAssignedDefault;
  registrationDataSource: PaginatorResponse<ListStudentRegisterResponse> = this.listStudentForRegistrationDefault;


  @ViewChild('assignPaginator') assignPaginator: MatPaginator;
  @ViewChild('registerPaginator') registerPaginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  /*  //#region FromGroup  */
  filterSignupLecturesForm = this.fb.group({
    divisionId: new FormControl(''),
    schoolId: new FormControl(''),
    gradeId: new FormControl(''),
    groupStudentId: new FormControl(''),
  });

  filterReviewStudentsForm = this.fb.group({
    schoolIdReview: new FormControl(''),
    gradeIdReview: new FormControl(''),
    groupStudentIdReview: new FormControl(''),
    statusReview: new FormControl(''),
  });

  /* Public property */
  get divisionId() { return this.filterSignupLecturesForm.get('divisionId'); }
  isEnableDivision = true;
  isFirstLoadDivision = true;

  get schoolId() { return this.filterSignupLecturesForm.get('schoolId'); }
  isDisableSchoolId = false;
  isFirstLoadSchool = true;

  get gradeId() { return this.filterSignupLecturesForm.get('gradeId'); }
  isDisableGradeId = false;
  isFirstLoadGrade = true;

  get groupStudentId() { return this.filterSignupLecturesForm.get('groupStudentId'); }
  isDisableGroupStudentId = true;
  isFirstLoadGroupStudent = true;

  get schoolIdReview() { return this.filterReviewStudentsForm.get('schoolIdReview'); }
  isDisableSchoolIdReview = false;
  isFirstLoadSchoolReview = true;

  get gradeIdReview() { return this.filterReviewStudentsForm.get('gradeIdReview'); }
  isDisableGradeIdReview = false;
  isFirstLoadGradeReview = true;

  get groupStudentIdReview() { return this.filterReviewStudentsForm.get('groupStudentIdReview'); }
  isDisableGroupStudentIdReview = true;
  isFirstLoadGroupStudentsReview = true;

  get statusReview() { return this.filterReviewStudentsForm.get('statusReview'); }
  isDisableStatusReviewReview = false;
  isFirstLoadStatusReviewReview = true;

  //* Division */
  protected divisions: DivisionOption[];
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  /* School */
  protected schools: SchoolOption[];
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  /* grades */
  protected grades: GradeOption[];
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected groupStudents: GroupStudentOption[];
  public groupStudentCtrl: FormControl = new FormControl();
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;

  /* School */
  protected schoolsReview: SchoolOption[];
  public schoolFilterCtrlReview: FormControl = new FormControl();
  public filteredSchoolsReview: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchoolReview', { static: true }) singleSelectSchoolReview: MatSelect;

  /* grades */
  protected gradesReview: GradeOption[];
  public gradeFilterCtrlReview: FormControl = new FormControl();
  public filteredGradesReview: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGradeReview', { static: true }) singleSelectGradeReview: MatSelect;

  protected groupStudentsReview: GroupStudentOption[];
  public groupStudentCtrlReview: FormControl = new FormControl();
  public groupStudentFilterCtrlReview: FormControl = new FormControl();
  public filteredGroupStudentsReview: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectGroupStudentReview', { static: true }) singleSelectGroupStudentReview: MatSelect;

  public statusReviews: Status[] = [{ id: null, name: 'Chọn trạng thái' }, { id: true, name: 'Đã duyệt' }, { id: false, name: 'Chưa duyệt' }];
  public statusCtrlReview: FormControl = new FormControl();
  public statusFilterCtrlReview: FormControl = new FormControl();
  public filteredstatusReview: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectStatusReview', { static: true }) singleSelectStatusReview: MatSelect;

  protected _onDestroy = new Subject<void>();

  lessonId: number;
  lesson: Lesson;
  signUpforLecture: any;
  reviewStudent: any;
  isReady: boolean = false;
  assignDisplayedColumns: string[] = ['fullName', 'userName', 'className', 'action'];

  studentsAssignPaginationFilter: StudentAssignPagingRequest = {
    pageNumber: 1,
    sizeNumber: 10,
    getCount: true,
    LessonId: +this.router.snapshot.paramMap.get("id"),
    DivisionId: 0,
    SchoolId: 0,
    GradeId: 0,
    GroupStudentId: 0,
    schoolYear: 0,
    SortDirection: 'DESC',
    SortField: 'CRE'
  }

  studentsRegistrationPaginationFilter: StudentRegistrationPagingRequest = {
    pageNumber: 1,
    sizeNumber: 10,
    getCount: true,
    LessonId: +this.router.snapshot.paramMap.get("id"),
    GroupStudentId: 0,
    SchoolId: 0,
    GradeId: 0,
    Status: null,
    schoolYear: 0,
    SortDirection: 'DESC',
    SortField: 'CRE'
  }


  assignSelection = new SelectionModel<ListStudentForAssignedResponse>(true, []);
  registrationSelection = new SelectionModel<ListStudentRegisterResponse>(true, []);

  registerDisplayedColumns: string[] = ['fullName', 'gradeName', 'className', 'registrationDate', 'status', 'action'];

  assignStudentEndpoint: AssignStudentsEndpoint;
  lessonEndpoint: LessonEndpoint;
  //List gửi lên api để ghi danh học sinh vào bài giảng
  assignStudentRequest: AssignAndApprovalStudentRequest;

  //List gửi lên api để duyệt học sinh đăng ký
  approvalStudentRequest: AssignAndApprovalStudentRequest;

  //Danh sách học sinh đã được ghi danh
  listOldStudentForAssignId: number[];

  //Danh sách học sinh đã được duyệt(trạng thái đã duyệt)
  listOldStudentRegisterId: number[];

  isListStudentRegisterReady: boolean = false;
  isListStudentForAssignReady: boolean = false;
  isLessonInfoReady: boolean = false;
  isSingleAssignClick: boolean;
  isDisableAssignClick: boolean= false;
  isSingleApprovalClick: boolean;
  isDisableApprovalClick: boolean= false;
  constructor(private router: ActivatedRoute, private _route: Router, private fb: FormBuilder, private _authService: AuthService, private _snackBar: MatSnackBar, private spinner: NgxSpinnerService) {
    this.lessonEndpoint = new LessonEndpoint();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
    this.assignStudentEndpoint = new AssignStudentsEndpoint();
    this.lessonId = +this.router.snapshot.paramMap.get("id");
    this.assignStudentRequest = {
      lessonId: this.lessonId,
      lessonStatus: "",
      listUserLesson: [],
      assignFrom: AssignFrom.Assign
    };
    this.approvalStudentRequest = {
      lessonId: this.lessonId,
      lessonStatus: "",
      listUserLesson: [],
      assignFrom: AssignFrom.Approval
    };
    this.listOldStudentForAssignId = [];
    this.listOldStudentRegisterId = [];
    this.userIdentity = _authService.getTokenInfo();



    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
      // Tài khoản cấp Trường, schoolid fillter mặc định theo id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
      this.filterSignupLecturesForm.controls['schoolId'].setValue(this.userIdentity.schoolId);
      // this.usersPaginationFilter.schoolId = this.userIdentity.schoolId;
    }

    Promise.resolve()
    .then(()=>{
      return this.lessonEndpoint.getLessonById(this.lessonId);
    })
    .then((lesson)=>{
      this.lesson = { ...lesson };
      console.log("lessoninfo:", this.lesson);
      this.assignStudentRequest.lessonStatus = lesson.status;
      this.approvalStudentRequest.lessonStatus = lesson.status;
      this.studentsAssignPaginationFilter.DivisionId = this.lesson.divsionId;
      this.studentsAssignPaginationFilter.SchoolId = this.lesson.schoolId;
      this.studentsAssignPaginationFilter.GradeId = this.lesson.gradeId;

      this.getListStudentForAssign();
      this.getListStudentRegistration();
      this.isLessonInfoReady = true;
      return;
    })
    .then(()=>{
      if(this.lesson.divsionId != undefined && this.lesson.divsionId != null){
        this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivision = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
          this.divisionId.setValue(this.lesson.divsionId);
          return;
        })
      }else{
        return;
      }
    })
    .then(()=>{
      if(this.lesson.schoolId != undefined && this.lesson.schoolId != null){
        this.endpointSchool.getSchoolOptions()
        .then(res => {
          this.isFirstLoadSchool = false;
          this.schools = res;
          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.filteredSchools.next(this.schools.slice());
          this.schoolId.setValue(this.lesson.schoolId);
          return;
        })
      }else{
        return;
      }
    })
    .then(()=>{
      if(this.lesson.gradeId != undefined && this.lesson.gradeId != null){
        this.endpointGrade.getGradeOptions(this.lesson.schoolId)
        .then(res => {
          this.isFirstLoadGrade = false;
          this.grades = res;

          if (this.grades && !this.grades.find(x => x.id == 0)) {
            this.grades.unshift({ id: 0, name: 'Chọn khối' });
          }
          this.gradeFilter.schoolId = this.lesson.schoolId;
          this.filteredGrades.next(this.grades.slice());
          this.gradeId.setValue(this.lesson.gradeId);
          return;
        })
      }else{
        return;
      }
    })
  }

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  ngOnInit(): void {
  }


  pageChangeAssign(event?: PageEvent): void {
    this.assignSelection.clear();
    this.studentsAssignPaginationFilter.pageNumber = this.assignPaginator.pageIndex + 1;
    this.studentsAssignPaginationFilter.sizeNumber = this.assignPaginator.pageSize;
    this.getListStudentForAssign();
  }

  pageChangeApproval(event?: PageEvent): void {
    this.registrationSelection.clear();
    this.studentsRegistrationPaginationFilter.pageNumber = this.registerPaginator.pageIndex + 1;
    this.studentsRegistrationPaginationFilter.sizeNumber = this.registerPaginator.pageSize;
    this.getListStudentRegistration();
  }

  ngAfterViewInit() {

    
    this.schoolId.setValue(this.lesson.schoolId);
    this.gradeId.setValue(this.lesson.gradeId);
    //Assign
    this.assignPaginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.assignPaginator.page.subscribe(() => {
      this.assignSelection.clear();
      this.studentsAssignPaginationFilter.pageNumber = this.assignPaginator.pageIndex + 1;
      this.studentsAssignPaginationFilter.sizeNumber = this.assignPaginator.pageSize;
      this.getListStudentForAssign();
    });

    //Registration
    this.registerPaginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.registerPaginator.page.subscribe(() => {
      this.registrationSelection.clear();
      this.studentsRegistrationPaginationFilter.pageNumber = this.registerPaginator.pageIndex + 1;
      this.studentsRegistrationPaginationFilter.sizeNumber = this.registerPaginator.pageSize;
      this.getListStudentRegistration();
    });


    this.divisionId?.valueChanges
      .pipe(startWith(this.divisionId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            // reset change
            this.onResetSchoolSelect();
            this.onResetGradeSelect();
            this.onResetGroupStudentSelect();
          }
        }
      )

    this.schoolId?.valueChanges
      .pipe(startWith(this.schoolId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetGradeSelect();
            this.onResetGroupStudentSelect();
            if (value < 1) {
              this.isDisableGroupStudentId = true;
            } else {
              this.isDisableGroupStudentId = false;

            }
          }
        }
      )

    this.gradeId?.valueChanges
      .pipe(startWith(this.gradeId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            this.onResetGroupStudentSelect();
          }
        }
      )

    this.groupStudentId?.valueChanges
      .pipe(startWith(this.gradeId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
          }
        }
      )
    this.schoolIdReview?.valueChanges
      .pipe(startWith(this.schoolIdReview?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetGradeReviewSelect();
            this.onResetGroupStudentReviewSelect();
            if (value < 1) {
              this.isDisableGroupStudentIdReview = true;
            } else {
              this.isDisableGroupStudentIdReview = false;
            }
          }
        }
      )

    this.gradeIdReview?.valueChanges
      .pipe(startWith(this.gradeIdReview?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
            this.onResetGroupStudentReviewSelect();
          }
        }
      )

    this.groupStudentIdReview?.valueChanges
      .pipe(startWith(this.groupStudentIdReview?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
          }
        }
      )

    // listen for search field value changes 
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        console.log(this.divisionId.value);
        this.filterDivisions();
      });


    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    this.groupStudentFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudents();
      });

    this.schoolFilterCtrlReview.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchoolsReview();
      });

    this.gradeFilterCtrlReview.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGradesReview();
      });

    this.groupStudentFilterCtrlReview.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudentsReview();
      });

    this.setInitialValue();
  }
  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  private getLessonInfo(): void {

  }
  /* Get division */
  onDivisionSelectClicked() {
    if (this.isFirstLoadDivision) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivision = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {
    }
  }

  onSchoolSelectClicked() {
    debugger;
    var divisionIdSelected = this.divisionId?.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchool) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchool = false;
          this.schools = res;
          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {

    }
  }

  /* Get Grade */
  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolId.value == 0 ? null : this.schoolId.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrade || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrade = false;
          this.grades = res;

          if (this.grades && !this.grades.find(x => x.id == 0)) {
            this.grades.unshift({ id: 0, name: 'Chọn khối' });
          }
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());

        })
        .catch(err => { console.log(err) }
        )
    } else {

    }
  }

  /* Get GroupStudent */
  onGroupStudentSelectClicked() {

    var schoolIdSelected = this.schoolId?.value;
    var gradeIdSelected = this.gradeId?.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if ((this.isDisableGroupStudentId == true && this.levelManageValue !== 3) || !this.isFirstLoadGroupStudent) {
      return;
    } else {
      this.endpointGroupStudent.getGroupStudentOptions(schoolIdSelected, gradeIdSelected)
        .then(res => {
          this.isFirstLoadGroupStudent = false;
          this.groupStudents = res;
          this.filteredGroupStudents.next(this.groupStudents.slice());
        })
        .catch(

        )
    }
  }

  /* Get school Review */
  onSchoolReviewSelectClicked() {
    var divisionIdSelected = null;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchoolReview) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchoolReview = false;
          this.schoolsReview = res;

          if (this.schoolsReview && !this.schoolsReview.find(x => x.id == 0)) {
            this.schoolsReview.unshift({ id: null, name: 'Chọn trường' })
          }
          this.filteredSchoolsReview.next(this.schoolsReview.slice());
        })
        .catch(

        )
    } else {

    }
  }

  /* Get Grade */
  onGradeReviewSelectClicked() {
    var schoolIdReviewSelected = this.schoolIdReview.value == 0 ? null : this.schoolIdReview.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdReviewSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGradeReview) {
      this.endpointGrade.getGradeOptions(schoolIdReviewSelected)
        .then(res => {
          this.isFirstLoadGradeReview = false;
          this.gradesReview = res;

          if (this.gradesReview && !this.gradesReview.find(x => x.id == 0)) {
            this.gradesReview.unshift({ id: 0, name: 'Chọn khối' });
          }
          this.gradeFilter.schoolId = schoolIdReviewSelected;
          this.filteredGradesReview.next(this.gradesReview.slice());

        })
        .catch(err => { console.log(err) }
        )
    } else {

    }
  }

  /* Get GroupStudent */
  onGroupStudentReviewSelectClicked() {
    if (!this.isFirstLoadGroupStudentsReview || (this.isDisableGroupStudentIdReview && this.levelManageValue !== 3)) {

    } else {
      var schoolIdReviewSelected = this.schoolIdReview.value == 0 ? null : this.schoolIdReview.value;
      if (this.userIdentity.levelManage == "SCH") {
        schoolIdReviewSelected = this.userIdentity.schoolId;
      }
      this.endpointGroupStudent
        .getGroupStudentOptions(schoolIdReviewSelected
          , (this.filterReviewStudentsForm.controls['gradeIdReview'].value == 0 ? null : this.filterReviewStudentsForm.controls['gradeIdReview'].value))
        .then(res => {
          this.isFirstLoadGroupStudentsReview = false;
          this.groupStudentsReview = res;
          this.filteredGroupStudentsReview.next(this.groupStudentsReview.slice());
        })
        .catch(

        )
    }
  }



  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchoolsReview() {
    if (!this.schoolsReview) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrlReview.value;
    if (!search) {
      this.filteredSchoolsReview.next(this.schoolsReview.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchoolsReview.next(
      this.schoolsReview.filter(schoolReview => schoolReview.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGradesReview() {
    if (!this.gradesReview) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrlReview.value;
    if (!search) {
      this.filteredGradesReview.next(this.gradesReview.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGradesReview.next(
      this.gradesReview.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGroupStudentsReview() {
    if (!this.groupStudentsReview) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrlReview.value;
    if (!search) {
      this.filteredGroupStudentsReview.next(this.groupStudentsReview.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudentsReview.next(
      this.groupStudentsReview.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }

  private onResetSchoolSelect() {
    this.filterSignupLecturesForm.controls['schoolId'].setValue(0);
    this.isFirstLoadSchool = true;
    // this.filterLessonForm.controls['schoolFilterCtrl'].setValue(0);
  }

  /* reset change Grade */
  private onResetGradeSelect() {
    this.filterSignupLecturesForm.controls['gradeId'].setValue(0);
    // this.filterLessonForm.controls['gradeFilterCtrl'].setValue(null);
    this.isFirstLoadGrade = true;
  }

  private onResetGroupStudentSelect() {
    this.filterSignupLecturesForm.controls['groupStudentId'].setValue(null);
    // this.filterLessonForm.controls['gradeFilterCtrl'].setValue(null);
    this.isFirstLoadGroupStudent = true;
  }

  /* reset change Grade */
  private onResetGradeReviewSelect() {
    this.filterReviewStudentsForm.controls['gradeIdReview'].setValue(null);
    // this.filterLessonForm.controls['gradeFilterCtrl'].setValue(null);
    this.isFirstLoadGradeReview = true;
  }

  private onResetGroupStudentReviewSelect() {
    this.filterReviewStudentsForm.controls['groupStudentIdReview'].setValue(null);
    // this.filterLessonForm.controls['gradeFilterCtrl'].setValue(null);
    this.isFirstLoadGroupStudentsReview = true;
  }

  //Lấy danh sách học sinh cho 2 tab
  private getListStudentForAssign() {
    this.spinner.show('spinnerAsign');
    debugger;
    this.assignStudentEndpoint.getListStudentForAssigned(this.studentsAssignPaginationFilter)
      .then(res => {
        this.spinner.hide('spinnerAsign');
        if (res) {
          this.assignDataSource = res;
          //Gán những học sinh đã được ghi danh vào trong list này
          this.assignDataSource.items.forEach((x) => {
            if (x.isApproval) {
              this.listOldStudentForAssignId.push(x.id);
              this.assignSelection.select(x);
            } else if (this.assignStudentRequest.listUserLesson.find(y => y.id == x.id && y.action == 'ADD') != undefined) {
              this.assignSelection.select(x);
            }
          });
          this.isListStudentForAssignReady = true;
        }
      });
  }
  private getListStudentRegistration() {
    this.spinner.show('spinnerRegis');
    this.assignStudentEndpoint.getListStudentRegister(this.studentsRegistrationPaginationFilter)
      .then((res) => {
        this.spinner.hide('spinnerRegis');
        if (res) {
          this.registrationDataSource = res;
          //Gán những học sinh đã được duyệt vào trong list này
          this.registrationDataSource.items.forEach((x) => {
            if (x.isApproval) {
              this.listOldStudentRegisterId.push(x.id);
              this.registrationSelection.select(x);
            } else if (this.assignStudentRequest.listUserLesson.find(y => y.id == x.id && y.action == 'ADD') != undefined) {
              this.registrationSelection.select(x);
            }
          });
          this.isListStudentRegisterReady = true;
        }
      });
  }
  onCancelAssignStudentTabClick(): void {
    this._route.navigate(['dashboard/learn/list-lesson']);
  }

  onSaveAssignStudentClick(): void {
    this.isSingleAssignClick = true;
    this.isDisableAssignClick = true;
    setTimeout(() => {
      if(this.isSingleAssignClick){
        this.assignStudentEndpoint.assignStudentsForLesson(this.assignStudentRequest).then((res) => {
          this.isDisableAssignClick = false;
          if (res) {
            //Nếu thành công thì reset lại list request
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Bạn vừa mới thực hiện ghi danh/ gỡ ghi danh thành công',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
            // //Update list oldId
            this.assignStudentRequest.listUserLesson.forEach((std) => {
              if (std.action == Action.Add) {
                this.listOldStudentForAssignId.push(std.id);
                this.assignDataSource.items.map((x)=>{
                  if(x.id == std.id){
                    x.isApproval = true;
                  }
                })
              }
              else {
                let index = this.listOldStudentForAssignId.indexOf(std.id);
                this.listOldStudentForAssignId.splice(index, 1);
                this.assignDataSource.items.map((x)=>{
                  if(x.id == std.id){
                    x.isApproval = false;
                  }
                })
              }
            });
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Có lỗi xảy ra!',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
            //Reset lại combobox
            this.assignStudentRequest.listUserLesson.forEach((std) => {
              let row = this.listStudentForAssignedDefault.items.find(x => x.id == std.id);
              this.assignSelection.toggle(row);
            });
          }
          this.assignStudentRequest.listUserLesson = [];
        }).catch((err) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Có lỗi xảy ra!',
            duration: 3000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });
          //Reset lại combobox
          this.assignStudentRequest.listUserLesson.forEach((std) => {
            let row = this.listStudentForAssignedDefault.items.find(x => x.id == std.id);
            this.assignSelection.toggle(row);
          });
          this.assignStudentRequest.listUserLesson = [];
        });
      }
    }, 250);

  }

  onDoubleClickAssign(): void {
    this.isSingleAssignClick = false;
  }

  onSaveApprovalStudentClick(): void {
    this.isSingleApprovalClick = true;
    this.isDisableApprovalClick = true;
    setTimeout(() => {
      if(this.isSingleApprovalClick){
        this.assignStudentEndpoint.assignStudentsForLesson(this.approvalStudentRequest).then((res) => {
          this.isDisableApprovalClick = false;
          if (res) {
            //Nếu thành công thì reset lại list request
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Bạn vừa mới thực hiện duyệt/ bỏ duyệt thành công',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
            // //Update list oldId
            this.approvalStudentRequest.listUserLesson.forEach((std) => {
              if (std.action == Action.Add) {
                this.listOldStudentRegisterId.push(std.id);
                this.registrationDataSource.items.map((x)=>{
                  if(x.id == std.id){
                    x.isApproval = true;
                  }
                })
              }
              else {
                let index = this.listOldStudentRegisterId.indexOf(std.id);
                this.listOldStudentRegisterId.splice(index, 1);
                this.registrationDataSource.items.map((x)=>{
                  if(x.id == std.id){
                    x.isApproval = false;
                  }
                })
              }
            });
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Có lỗi xảy ra!',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          }
          this.approvalStudentRequest.listUserLesson = [];
        }).catch((err) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Có lỗi xảy ra!',
            duration: 3000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });
          this.approvalStudentRequest.listUserLesson = [];
        });
      }
    }, 250);
  }

  onDoubleClickApproval(): void {
    this.isSingleApprovalClick = false;
  }

  onChangeAssignCheckBox(checked, row: ListStudentForAssignedResponse): void {
    this.assignSelection.toggle(row);
    console.log(checked);
    // nếu chọn thì thêm vào list request với action là add
    if (checked) {
      //Kiểm tra xem nếu là chọn thằng cũ thì chắc là nãy nó bị bỏ chọn rồi giờ chọn lại nên remove khỏi list request đi
      if (this.listOldStudentForAssignId.includes(row.id)) {
        let std = this.assignStudentRequest.listUserLesson.find(x => x.id == row.id);
        let index = this.assignStudentRequest.listUserLesson.indexOf(std)
        this.assignStudentRequest.listUserLesson.splice(index, 1);
      }
      else {
        this.assignStudentRequest.listUserLesson.push({ id: row.id, action: Action.Add });
      }
    }
    // nếu bỏ chọn thì ...
    else {
      //Nếu bỏ chọn thằng đã được ghi danh thì thêm vào request để biết mà gỡ ghi danh
      if (this.listOldStudentForAssignId.includes(row.id)) {
        this.assignStudentRequest.listUserLesson.push({ id: row.id, action: Action.Delete });
      }
      //Bỏ chọn thằng mới thì gỡ ra luôn khỏi list, ko gửi lên
      else {
        let std = this.assignStudentRequest.listUserLesson.find(x => x.id == row.id);
        let index = this.assignStudentRequest.listUserLesson.indexOf(std);
        if (!(index < 0)) {
          this.assignStudentRequest.listUserLesson.splice(index, 1);
        }
      }
    }
    console.log("new", this.assignStudentRequest.listUserLesson);
    console.log("old", this.listOldStudentForAssignId);
  }
  onChangeApprovalCheckBox(checked, row: ListStudentRegisterResponse): void {
    this.registrationSelection.toggle(row);
    // nếu chọn thì thêm vào list request với action là add
    if (checked) {
      //Kiểm tra xem nếu là chọn thằng cũ thf chắc là nãy nó bị bỏ chọn rồi giờ chọn lại nên remove khỏi list request đi
      if (this.listOldStudentRegisterId.includes(row.id)) {
        let std = this.approvalStudentRequest.listUserLesson.find(x => x.id == row.id);
        let index = this.approvalStudentRequest.listUserLesson.indexOf(std);
        this.approvalStudentRequest.listUserLesson.splice(index, 1);
      }
      else {
        this.approvalStudentRequest.listUserLesson.push({ id: row.id, action: Action.Add });
      }
    }
    // nếu bỏ chọn thì ...
    else {
      //Nếu bổ chọn thằng đã được ghi danh thì thêm vào request để biết mà gỡ ghi danh
      if (this.listOldStudentRegisterId.includes(row.id)) {
        this.approvalStudentRequest.listUserLesson.push({ id: row.id, action: Action.Delete });
      }
      //Bỏ chọn thằng mới thì gỡ ra luôn khỏi list, ko gửi lên
      else {
        let std = this.approvalStudentRequest.listUserLesson.find(x => x.id == row.id);
        let index = this.approvalStudentRequest.listUserLesson.indexOf(std);
        if (!(index < 0)) {
          this.approvalStudentRequest.listUserLesson.splice(index, 1);
        }
      }
    }
    console.log("new", this.approvalStudentRequest.listUserLesson);
    console.log("old", this.listOldStudentRegisterId);
  }

  protected setInitialValue() {

    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectDivision && (this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id);
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool && (this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id);
      });

    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGrade && (this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id);
      });

    this.filteredGroupStudents
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGroupStudent && (this.singleSelectGroupStudent.compareWith = (a: GroupStudentOption, b: GroupStudentOption) => a && b && a.id === b.id);
      });

    this.filteredSchoolsReview
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchoolReview && (this.singleSelectSchoolReview.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id);
      });

    this.filteredGradesReview
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGradeReview && (this.singleSelectGradeReview.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id);
      });

    this.filteredGroupStudentsReview
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGroupStudentReview && (this.singleSelectGroupStudentReview.compareWith = (a: GroupStudentOption, b: GroupStudentOption) => a && b && a.id === b.id);
      });
  }

  /**Mấy hàm này danh cho check box bên tab ghi danh */
  /** Whether the number of selected elements matches the total number of rows. */
  isAssignAllSelected() {
    const numSelected = this.assignSelection.selected.length;
    const numRows = this.assignDataSource.items.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  assignMasterToggle() {
    if (this.isAssignAllSelected()) {
      //Nếu là bỏ chọn hết thì thêm những học sinh đã ghi danh vào model để gỡ ghi danh (action: Delete)
      this.assignDataSource.items.forEach((std) => {
        //Mấy thằng cũ đã được ghi danh thì kiểm tra xem nó đã được thêm vào trước đó với action delete chưa
        if (std.isApproval) {
          if (this.assignStudentRequest.listUserLesson.find(x => x.id == std.id && x.action == Action.Delete) == undefined) {
            this.assignStudentRequest.listUserLesson.push({ id: std.id, action: Action.Delete });
          }
        } else {
          let studentAdd = this.assignStudentRequest.listUserLesson.find(x => x.id == std.id && x.action == Action.Add);
          if (studentAdd != undefined) {
            let index = this.assignStudentRequest.listUserLesson.indexOf(studentAdd);
            this.assignStudentRequest.listUserLesson.splice(index, 1);
          }
        }
      });
      console.log(this.assignStudentRequest.listUserLesson);
      this.assignSelection.clear();
    } else {

      // Nếu chọn hết thì thêm những học sinh chưa ghi dahnh vào model để gửi lên(action: Add)
      this.assignDataSource.items.forEach((std) => {
        if (!std.isApproval) {
          if (this.assignStudentRequest.listUserLesson.find(x => x.id == std.id && x.action == Action.Add) == undefined) {
            this.assignStudentRequest.listUserLesson.push({ id: std.id, action: Action.Add });
          }
        }
        else {
          let studentDel = this.assignStudentRequest.listUserLesson.find(x => x.id == std.id && x.action == Action.Delete);
          if (studentDel != undefined) {
            let index = this.assignStudentRequest.listUserLesson.indexOf(studentDel);
            this.assignStudentRequest.listUserLesson.splice(index, 1);
          }
        }
      });
      console.log(this.assignStudentRequest.listUserLesson);
      this.assignDataSource.items.forEach(row => this.assignSelection.select(row));
    }
  }

  /** The label for the checkbox on the passed row */
  assignCheckboxLabel(row?: ListStudentForAssignedResponse): string {
    if (!row) {
      return `${this.isAssignAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.assignSelection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  /**Mấy hàm này danh cho check box bên tab đăng ký */
  /** Whether the number of selected elements matches the total number of rows. */
  isRegisAllSelected() {

    const numSelected = this.registrationSelection.selected.length;
    const numRows = this.registrationDataSource.items.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  regisMasterToggle() {
    if (this.isRegisAllSelected()) {
      //Nếu là bỏ chọn hết thì thêm những học sinh đã ghi danh vào model để gỡ ghi danh (action: Delete)
      this.registrationDataSource.items.forEach((std) => {
        //Mấy thằng cũ đã được ghi danh thì kiểm tra xem nó đã được thêm vào trước đó với action delete chưa
        if (std.isApproval) {
          if (this.approvalStudentRequest.listUserLesson.find(x => x.id == std.id && x.action == Action.Delete) == undefined) {
            this.approvalStudentRequest.listUserLesson.push({ id: std.id, action: Action.Delete });
          }
        } else {
          let studentAdd = this.approvalStudentRequest.listUserLesson.find(x => x.id == std.id && x.action == Action.Add);
          if (studentAdd != undefined) {
            let index = this.approvalStudentRequest.listUserLesson.indexOf(studentAdd);
            this.approvalStudentRequest.listUserLesson.splice(index, 1);
          }
        }
      });
      this.registrationSelection.clear();
    } else {
      // Nếu chọn hết thì thêm những học sinh chưa ghi dahnh vào model để gửi lên(action: Add)
      this.registrationDataSource.items.forEach((std) => {
        if (!std.isApproval) {
          if (this.approvalStudentRequest.listUserLesson.find(x => x.id == std.id && x.action == Action.Add) == undefined) {
            this.approvalStudentRequest.listUserLesson.push({ id: std.id, action: Action.Add });
          }
        }
        else {
          let studentDel = this.approvalStudentRequest.listUserLesson.find(x => x.id == std.id && x.action == Action.Delete);
          if (studentDel != undefined) {
            let index = this.approvalStudentRequest.listUserLesson.indexOf(studentDel);
            this.approvalStudentRequest.listUserLesson.splice(index, 1);
          }
        }
      });
      this.registrationDataSource.items.forEach(row => this.registrationSelection.select(row));
    }
  }
  /** The label for the checkbox on the passed row */
  regisCheckboxLabel(row?: ListStudentRegisterResponse): string {
    if (!row) {
      return `${this.isRegisAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.registrationSelection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  onSubmitSignLectures() {
    //reset lại list học sinh đã chọn và list học sinh đã ghi danh
    this.assignPaginator.pageIndex = 0;
    this.assignSelection.clear();
    this.assignStudentRequest.listUserLesson = [];
    this.listOldStudentForAssignId = [];
    this.studentsAssignPaginationFilter.pageNumber = 1;
    this.studentsAssignPaginationFilter.LessonId = this.lessonId
    this.studentsAssignPaginationFilter.DivisionId = this.filterSignupLecturesForm.controls['divisionId'].value
    this.studentsAssignPaginationFilter.SchoolId = this.filterSignupLecturesForm.controls['schoolId'].value
    this.studentsAssignPaginationFilter.GradeId = this.filterSignupLecturesForm.controls['gradeId'].value
    this.studentsAssignPaginationFilter.GroupStudentId = this.filterSignupLecturesForm.controls['groupStudentId'].value
    this.getListStudentForAssign();
  }

  onSubmitReviewStudent() {
    //reset lại list học sinh đã chọn và list học sinh đã ghi danh
    this.registerPaginator.pageIndex = 0;
    this.registrationSelection.clear();
    this.approvalStudentRequest.listUserLesson = [];
    this.listOldStudentRegisterId = [];
    this.studentsRegistrationPaginationFilter.pageNumber = 1;
    this.studentsRegistrationPaginationFilter.LessonId = this.lessonId
    this.studentsRegistrationPaginationFilter.SchoolId = this.filterReviewStudentsForm.controls['schoolIdReview'].value
    this.studentsRegistrationPaginationFilter.GradeId = this.filterReviewStudentsForm.controls['gradeIdReview'].value
    this.studentsRegistrationPaginationFilter.GroupStudentId = this.filterReviewStudentsForm.controls['groupStudentIdReview'].value
    this.studentsRegistrationPaginationFilter.Status = this.filterReviewStudentsForm.controls['statusReview'].value
    this.getListStudentRegistration();
  }
}

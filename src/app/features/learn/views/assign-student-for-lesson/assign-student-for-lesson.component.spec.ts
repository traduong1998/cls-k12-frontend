import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignStudentForLessonComponent } from './assign-student-for-lesson.component';

describe('SignupForLecturesAndReviewStudentsComponent', () => {
  let component: AssignStudentForLessonComponent;
  let fixture: ComponentFixture<AssignStudentForLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignStudentForLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignStudentForLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

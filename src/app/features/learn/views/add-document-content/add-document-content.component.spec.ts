import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDocumentContentComponent } from './add-document-content.component';

describe('AddDocumentContentComponent', () => {
  let component: AddDocumentContentComponent;
  let fixture: ComponentFixture<AddDocumentContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDocumentContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDocumentContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfigLessonResponses } from './../../../../../../sdk/cls-k12-sdk-js/src/services/lesson/responses/configLessonResponses';
import { LessonEndpoint } from 'cls-k12-sdk-js/src/services/lesson/endpoints/lessonEndpoint';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LessonInfoResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/lessonInfoResponses';
import { UserEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user/endpoints/UserEndpoint';

@Component({
  selector: 'app-approval-lesson',
  templateUrl: './approval-lesson.component.html',
  styleUrls: ['./approval-lesson.component.scss']
})
export class ApprovalLessonComponent implements OnInit {
  lessonName: string = "";
  lessonId: number;
  lessonEndpoint: LessonEndpoint;
  lessonConfig: ConfigLessonResponses;
  lessonInfo: LessonInfoResponses;
  isReady: boolean = false;
  teacherName = "Hoàng Trung";
  userEndpoint: UserEndpoint;


  constructor(private _router: Router, private route: ActivatedRoute, private _snackBar: MatSnackBar) {
    this.lessonEndpoint = new LessonEndpoint();
    this.lessonInfo = new LessonInfoResponses();
    this.userEndpoint = new UserEndpoint();
    this.lessonId = +this.route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.getLessonInfo();
  }

  getLessonConfig(): void {
    this.lessonEndpoint.getLessonConfig(this.lessonId).then((res) => {
      this.lessonConfig = res;
      this.lessonName = this.lessonConfig.name;
    })
  }
  getLessonInfo(): void {
    this.lessonEndpoint.getLessonById(this.lessonId).then((lesson) => {
      this.lessonInfo.id = lesson.id;
      this.lessonInfo.name = lesson.name;
      this.lessonInfo.userId = lesson.userId;
      this.lessonInfo.avatar = lesson.avatar;
      if(this.lessonInfo.description != null && this.lessonInfo.description != undefined && this.lessonInfo.description != ''){
        this.lessonInfo.description = this.convertHtmlToPlanText(lesson.introduction);
      }
      this.userEndpoint.GetListUserByIds([this.lessonInfo.userId]).then((res)=>{
        this.teacherName = res[0].name;
        this.isReady = true;
      });
    });
  }
  approvalLesson(): void {
    this.lessonEndpoint.approvalLesson(this.lessonId).then((res) => {
      if (res) {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: "Bài giảng đã được duyệt",
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
        this._router.navigate(['dashboard/learn/list-lesson-to-be-approved']);
      }
      else {
        this._snackBar.openFromComponent(ErrorSnackBarComponent,  {
          data: "Có lỗi xảy ra vui lòng thử lại!",
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
      }
    });
  }

  returnLesson(): void {
    this.lessonEndpoint.returnLesson(this.lessonId).then((res) => {
      if (res) {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: "Bài giảng đã được trả lại",
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
        this._router.navigate(['dashboard/learn/list-lesson-to-be-approved']);
      }
      else {
        this._snackBar.openFromComponent(ErrorSnackBarComponent,  {
          data: "Có lỗi xảy ra vui lòng thử lại!",
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
      }
    });
  }
  convertHtmlToPlanText(html: string): string {
    html = html.replace(/<style([\s\S]*?)<\/style>/gi, '');
    html = html.replace(/<script([\s\S]*?)<\/script>/gi, '');
    html = html.replace(/<\/div>/ig, '\n');
    html = html.replace(/<\/li>/ig, '\n');
    html = html.replace(/<li>/ig, '  *  ');
    html = html.replace(/<\/ul>/ig, '\n');
    html = html.replace(/<\/p>/ig, '\n');
    html = html.replace(/<br\s*[\/]?>/gi, "\n");
    html = html.replace(/<[^>]+>/ig, '');
    return html;
  }
  

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalLessonComponent } from './approval-lesson.component';

describe('ApprovalLessonComponent', () => {
  let component: ApprovalLessonComponent;
  let fixture: ComponentFixture<ApprovalLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApprovalLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

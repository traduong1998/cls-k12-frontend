import { LessonEndpoint } from 'cls-k12-sdk-js/src';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentEndpoint';
import { DetailContentResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/detailContentResponses';
import { StepHeaderControl } from '../../intefaces/stepHeaderControl';

@Component({
  selector: 'app-update-content',
  templateUrl: './update-content.component.html',
  styleUrls: ['./update-content.component.scss']
})
export class UpdateContentComponent implements OnInit {
  contentEndpoint: ContentEndpoint
  idContent: number
  idLesson: number;
  lessonName: string;
  lessonEndpoint: LessonEndpoint;
  typesOfContent: string;
  stepHeaderControl: StepHeaderControl;
  detailContentResponses: DetailContentResponses;
  isReady: boolean = false;
  lessonStatus: string;

  constructor(private route: ActivatedRoute) {
    this.contentEndpoint = new ContentEndpoint();
    this.lessonEndpoint = new LessonEndpoint();
    this.idLesson = + this.route.snapshot.paramMap.get("idLesson");
    this.idContent = + this.route.snapshot.paramMap.get("id");
  }

  ngOnInit(): void {
    this.lessonEndpoint.getLessonById(this.idLesson).then((lesson) => {
      this.stepHeaderControl = {
        isCreatingContent: true,
        currentPage: 2,
        lessonId: this.idLesson,
        step: lesson.step
      }
      this.lessonName = lesson.name;
      this.lessonStatus = lesson.status;
      this.getContent();
    });
  }

  getContent() {
    this.contentEndpoint.getContent(this.idLesson, this.idContent).then((res => {

      this.detailContentResponses = res;
      this.typesOfContent = res.typesOfContent?.toLocaleUpperCase();
    }));
    this.isReady = true;
  }

}
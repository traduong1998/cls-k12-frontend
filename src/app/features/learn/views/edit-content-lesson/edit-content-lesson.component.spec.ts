import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditContentLessonComponent } from './edit-content-lesson.component';

describe('EditContentLessonComponent', () => {
  let component: EditContentLessonComponent;
  let fixture: ComponentFixture<EditContentLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditContentLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditContentLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

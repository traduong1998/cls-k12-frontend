import { StepHeaderControl } from './../../intefaces/stepHeaderControl';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/errorResponse';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { ContentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentEndpoint';
import { ContentGroupEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/contentGroupEndpoint';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { ContentGroupResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/contentGroupResponses';
import { AddPartDialogComponent } from '../../components/add-part-dialog/add-part-dialog.component';
import { ContentStatus } from '../../intefaces/contentStatus';
import { UseCase } from '../../intefaces/useCase';
import { EditPartDialogComponent } from '../../components/edit-part-dialog/edit-part-dialog.component';
import { Observable, of, Subject } from 'rxjs';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { AddReferenceDialogComponent } from '../../components/add-reference-dialog/add-reference-dialog.component';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { ReferencesEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/referencesEndpoint';
import { ReferenceRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/referenceRequest';
import { ReferenceResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/referenceResponses';
import { EditReferenceDialogComponent } from '../../components/edit-reference-dialog/edit-reference-dialog.component';
import { CLSModules, CLSPermissions, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { AuthService } from 'src/app/core/services';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { TeacherZoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/teacherZoomEndpoint';
import { VerifyAccountDialogComponent, VerifyAccountType } from 'src/app/shared/dialog/verify-account-dialog/verify-account-dialog.component';
import { VirtualClassRoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/virtual-classroom-endpoints';
import { Configuration } from 'src/app/shared/configurations';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-edit-content-lesson',
  templateUrl: './edit-content-lesson.component.html',
  styleUrls: ['./edit-content-lesson.component.scss'],
})
export class EditContentLessonComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  state: string = 'default';
  currentlyOpenedItemIndex = -1;
  baseApiUrl = 'http://localhost:58910';
  lessonId: number = null;
  lesson: Lesson;
  isShowContent = false;
  useCase: UseCase = UseCase.creating
  listContentGroup: ContentGroupResponses;
  isLoadingResults: boolean = false;
  isReady: Boolean = false;
  progressData: Observable<any>;
  lessonName: string;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }
  numbers: 0;
  displayedColumns: string[] = ['name', 'status', 'action'];
  displayedColumns2: string[] = ['stt', 'name', 'function'];

  hasAddLessonPermission: boolean;
  hasEditLessonPermission: boolean;
  hasDeleteLessonPermission: boolean;
  userIdentity: UserIdentity;

  stepHeaderControl: StepHeaderControl
  fileServer: FileServerEndpoint;
  referenceEndpoint: ReferencesEndpoint;
  referenceData: ReferenceResponses[];
  lessonEndpoint = new LessonEndpoint({ baseUrl: this.baseApiUrl });
  teacherZoomEndpoint = new TeacherZoomEndpoint();
  virtualClassroomEndpoint = new VirtualClassRoomEndpoint();
  cgEndpoint = new ContentGroupEndpoint({ baseUrl: this.baseApiUrl });
  contentEndpoint = new ContentEndpoint({ baseUrl: this.baseApiUrl });
  public get useCaseEnum(): typeof UseCase {
    return UseCase;
  }
  constructor(public dialog: MatDialog,
    private _authService: AuthService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private _router: Router,
    private _learnMessageService: LearnMessageService,
    private _snackBar: MatSnackBar) {
    this.lessonId = +this.route.snapshot.paramMap.get("id");
    if (this.lessonId) {
      this.isReady = false;

      this.getListContentGroupByLessonId();
    }
    this.fileServer = new FileServerEndpoint();
    this.referenceEndpoint = new ReferencesEndpoint();
    ;
    this.userIdentity = _authService.getTokenInfo();
    this.hasAddLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Add);
    this.hasEditLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Edit);
    this.hasDeleteLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Delete);
  }

  public get contentStatusEnum(): typeof ContentStatus {
    return ContentStatus;
  }

  ngOnInit(): void {
    this.getReferenceData();
  }

  openPartDialog(): void {
    const dialogRef = this.dialog.open(AddPartDialogComponent, {
      width: "557px"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        return;
      }
      else {
        this.cgEndpoint.addContentGroup(this.lessonId, result).then(res => {
          if (res) {
            this._learnMessageService.successToast(" Tạo thành công tiết học ");

            this.getListContentGroupByLessonId();
          }
        }).catch((err: ErrorResponse) => {
          console.log(err);
        });
      }
    });
  }

  openAddReferenceDialog(): void {
    const dialogRef = this.dialog.open(AddReferenceDialogComponent, {
      width: '900px',
      maxHeight: '100vh',
      data: this.lessonId
    });

    dialogRef.afterClosed().subscribe((result: ReferenceRequest) => {
      result.lessonId = this.lessonId;
      if (result) {
        this.referenceEndpoint.addReference(result).then(res => {
          if (res) {
            this._learnMessageService.successToast(" Thêm thành công ");
            this.getReferenceData();
          }
        });
      }
    });
  }

  private getReferenceData() {
    this.referenceEndpoint.getListReferenceByLessonId(this.lessonId)
      .then(res => {
        this.referenceData = res;
      })
  }

  getLessonContentById() {
    this.lessonEndpoint.getLessonContentsById(this.lessonId)
      .then(res => {
        if (res != null) {
          this.listContentGroup = res
            ;
          this.listContentGroup.contentGroups.sort((a, b) => { return a.order - b.order })
          this.stepHeaderControl = {
            lessonId: this.lessonId,
            currentPage: 2,
            step: res.step
          };

          this.lessonName = res.name;
        };
      })
      .catch(err => {
      });
  }


  handleShowHideAddContent() {
    this.isShowContent = !this.isShowContent;
  }
  /**7/08/2021 Lamnv add check API key */
  editContent(value: number) {
    this._router.navigate([`/dashboard/learn/lesson/${this.lessonId}/update-content/${value}`]);
    // this.virtualClassroomEndpoint.getMeeting(value, this.lessonId)
    //   .then(res => {
    //     if (res) {

    //     }
    //   })
    //   .catch((err: ErrorResponse) => {
    //     if (err.statusCode = 404) {
    //       let data: VerifyAccountType = { title: err.errorDetail, detail: Configuration.MESSAGE_CHANGE_ZOOM_ROOM_DETAIL, buttonText: `Cập nhật liên kết` }
    //       const verifyAccountDialog = this.dialog.open(VerifyAccountDialogComponent, {
    //         data: data
    //       });
    //       verifyAccountDialog.afterClosed().subscribe(result => {
    //         if (result) {
    //           this.virtualClassroomEndpoint.createMeeting(value, this.lessonId)
    //             .then(res => {
    //               this._learnMessageService.successToast(` Đã liên kết thành công `);
    //               this._router.navigate([`/dashboard/learn/lesson/${this.lessonId}/update-content/${value}`]);
    //             })
    //             .catch((err: ErrorResponse) => {
    //               this._learnMessageService.failToast(` Liên kết thất bại. Hãy thử lại `);
    //             })
    //         } else {
    //           this.spinner.hide();
    //           return
    //         }
    //       })
    //     }

    //     this.spinner.hide();
    //   })
  }

  deleteContent(data) {
    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      disableClose: true,
      width: '444px',
      height: 'auto',
      data: {
        title: 'Xóa nội dung',
        name: data.name,
        message: ''
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
        this.contentEndpoint.deleteContent(this.lessonId, data.id).then((res) => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Xóa thành công nội dung',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
            this.getListContentGroupByLessonId();
          }
          else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Xóa thất bại',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          }
        })
          .catch((err) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Xóa thất bại',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          })
      }
      else {
        if (result != undefined) {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Bạn nhập chưa đúng, vui lòng thử lại',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      }
    });
  }

  private getListContentGroupByLessonId() {
    this.lessonEndpoint.getLessonContentsById(this.lessonId).then(res => {
      this.listContentGroup = res;
      this.listContentGroup.contentGroups.sort((a, b) => { return a.order - b.order });
      this.listContentGroup?.contentGroups && this.listContentGroup.contentGroups.forEach(contentgroup => {
        if (contentgroup && contentgroup.contents.length > 0) {
          contentgroup.contents.sort((a, b) => { return a.order - b.order });
        }
      });
      this.stepHeaderControl = {
        currentPage: 2,
        lessonId: res.id,
        step: res.step,
      }
      this.lessonName = res.name;
      this.isReady = true;
    })
  }

  openEditPartDialog(id: number) {
    let data = this.listContentGroup.contentGroups.find(x => x.id == id);
    const dialogRef = this.dialog.open(EditPartDialogComponent, {
      width: "557px",
      data: data
    });

    dialogRef.afterClosed().subscribe((result: string) => {

      if (!result) {
        return;
      }
      else {
        this.cgEndpoint.editContentGroup(this.lessonId, data.id, result).then((res) => {
          this._learnMessageService.successToast(" Đã cập nhật thành công ! ");

        }).then(() => {
          this.getListContentGroupByLessonId();
        }).catch()

      }
    });
  }

  onClickReturn(): void {
    setTimeout(() => {
      this._router.navigate([`/dashboard/learn/list-lesson`]);
      console.log(this.lessonId);
    }, 300)
  }

  onClickNext(): void {
    setTimeout(() => {
      if (this.stepHeaderControl.step < 3) {
        this._router.navigate([`/dashboard/learn/config-lesson/${this.lessonId}`]);
      } else {
        this._router.navigate([`/dashboard/learn/edit-lesson-config/${this.lessonId}`]);
      }
    }, 300)
  }

  deleteGroupContent(data) {
    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      disableClose: true,
      width: '444px',
      height: 'auto',
      data: {
        title: 'Xóa tiết học',
        name: data.name,
        message: ''
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
        this.cgEndpoint.deleteContentGroup(this.lessonId, data.id).then((res) => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Xóa thành công tiết học',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
            this.getListContentGroupByLessonId();
          }
          else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Xóa thất bại',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          }
        })
          .catch((err) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Xóa thất bại',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          })
      }
      else {
        if (result != undefined) {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Bạn nhập chưa đúng, vui lòng thử lại',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      }
    });
  }

  editGroupContent(groupContentId: number) {
    this.openEditPartDialog(groupContentId)
  }

  public onEditReferenceHandler(val: number) {
    this.referenceEndpoint.getReferenceById(val)
      .then(res => {
        const dialogRef = this.dialog.open(EditReferenceDialogComponent, {
          width: '860px',
          height: 'auto',
          data: res
        });
        dialogRef.afterClosed().subscribe((result: ReferenceRequest) => {
          result.lessonId = this.lessonId;
          if (result) {
            this.referenceEndpoint.editReference(result).then(res => {
              if (res) {
                this._learnMessageService.successToast('Sửa thành công');
                this.getReferenceData();
              }
            });
          }
        })
      })
  }

  onRemoveReferenceHandler(data) {

    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      disableClose: true,
      width: '444px',
      height: 'auto',
      data: {
        title: 'Xóa tài liệu tham khảo',
        name: data.name,
        message: ''
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
        this.referenceEndpoint.removeReference(0, data.id).then((res) => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Xóa thành công tài liệu tham khảo',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
            this.getReferenceData();
          }
          else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Xóa thất bại',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          }
        })
          .catch((err) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Xóa thất bại',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          })
      }
      else {
        if (result != undefined) {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Bạn nhập chưa đúng, vui lòng thử lại',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      }
    });
  }

  setOpened(itemIndex) {
    this.currentlyOpenedItemIndex = itemIndex;
  }

  setClosed(itemIndex) {
    if (this.currentlyOpenedItemIndex === itemIndex) {
      this.currentlyOpenedItemIndex = -1;
    }
  }

  rotate() {
    this.state = (this.state === 'default' ? 'rotated' : 'default');
  }

  /**24/08/2021 LAMNV Add check online class room  */
  public onAddOnlineClassroom() {
    this._router.navigateByUrl(`dashboard/learn/lesson/${this.lessonId}/add-interactive-classroom`);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishCreateLessonComponent } from './finish-create-lesson.component';

describe('FinishCreateLessonComponent', () => {
  let component: FinishCreateLessonComponent;
  let fixture: ComponentFixture<FinishCreateLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinishCreateLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishCreateLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgxSpinnerService } from 'ngx-spinner';
import { LessonStatus } from './../../intefaces/lessonStatus';
import { AuthService } from 'src/app/core/services';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/lessonEndpoint';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { StepHeaderControl } from '../../intefaces/stepHeaderControl';
import { LessonInfoResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/lessonInfoResponses';
import { InfoIdentity } from 'cls-k12-sdk-js/src';
import { UserEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user/endpoints/UserEndpoint';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { CLSPermissions } from 'cls-k12-sdk-js/src/core/authentication/identify/cls-permissions';
import { CLSModules } from 'cls-k12-sdk-js/src/core/authentication/identify/cls-modules';


@Component({
  selector: 'app-finish-create-lesson',
  templateUrl: './finish-create-lesson.component.html',
  styleUrls: ['./finish-create-lesson.component.scss']
})
export class FinishCreateLessonComponent implements OnInit {
  lessonId: number;
  lessonEndpoint: LessonEndpoint;
  lessonInfo: LessonInfoResponses;
  hasPermissionApproval = false;
  teacherName = "Hoàng Trung";
  isReady: boolean = false;
  stepHeaderControl: StepHeaderControl;
  userIdentity: InfoIdentity;
  userEndpoint: UserEndpoint;

  constructor(private _router: Router, private route: ActivatedRoute, private _snackBar: MatSnackBar, private _authService: AuthService, private _learnMessageService: LearnMessageService, private spinner: NgxSpinnerService) {
    this.spinner.show();
    this.lessonEndpoint = new LessonEndpoint();
    this.lessonInfo = new LessonInfoResponses();
    this.userEndpoint = new UserEndpoint();
    this.userIdentity = _authService.getTokenInfo();
    this.lessonId = +this.route.snapshot.paramMap.get("id");
    this.stepHeaderControl = {
      currentPage: 4,
      lessonId: this.lessonId,
      step: 4
    }
    
    this.hasPermissionApproval = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Approved);
  }

  ngOnInit(): void {
    this.getLessonInfo();
  }
  getLessonInfo(): void {
    this.lessonEndpoint.getLessonById(this.lessonId).then((lesson) => {
      this.lessonInfo.id = lesson.id;
      this.lessonInfo.name = lesson.name;
      this.lessonInfo.userId = lesson.userId;
      this.lessonInfo.avatar = lesson.avatar;
      this.lessonInfo.status = lesson.status;
      this.lessonInfo.description = lesson.introduction;
      this.userEndpoint.GetListUserByIds([this.lessonInfo.userId]).then((res) => {
        this.teacherName = res[0].name;
        this.isReady = true;
        this.spinner.hide();
      });
    });
  }

  backToConfig(): void {
    this._router.navigate([`/dashboard/learn/edit-lesson-config/${this.lessonId}`]);
  }

  submitLesson(): void {
    //Nếu có quyền duyệt
    if (this.hasPermissionApproval) {
      this.lessonEndpoint.approvalLesson(this.lessonId).then((res) => {
        if (res) {
          this._learnMessageService.successToast("Bài giảng đã được phát hành!");
          this.lessonInfo.status = LessonStatus.Publish;
        }
        else {
          this._learnMessageService.failToast("Có lỗi xảy ra!")
        }
      });
    }
    //Nếu ko có thì gửi duyệt
    else {
      this.lessonEndpoint.submitLesson(this.lessonId).then((res) => {
        if (res) {
          this._learnMessageService.successToast("Bài giảng đã được gửi duyệt!");
          this.lessonInfo.status = LessonStatus.Pending;
        }
        else {
          this._learnMessageService.failToast("Có lỗi xảy ra!")
        }
      });
    }
  }
  convertHtmlToPlanText(html: string): string {
    html = html.replace(/<style([\s\S]*?)<\/style>/gi, '');
    html = html.replace(/<script([\s\S]*?)<\/script>/gi, '');
    html = html.replace(/<\/div>/ig, '\n');
    html = html.replace(/<\/li>/ig, '\n');
    html = html.replace(/<li>/ig, '  *  ');
    html = html.replace(/<\/ul>/ig, '\n');
    html = html.replace(/<\/p>/ig, '\n');
    html = html.replace(/<br\s*[\/]?>/gi, "\n");
    html = html.replace(/<[^>]+>/ig, '');
    return html;
  }
}

import { LessonStatus } from './../../intefaces/lessonStatus';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { Chapter } from '../../intefaces/chapter';
import { Teacher } from '../../intefaces/teacher';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { pairwise, startWith, take, takeUntil } from 'rxjs/operators';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/PaginatorResponse';
import { Unit } from '../../intefaces/unit';
import { CLSModules, CLSPermissions, UserIdentity } from 'cls-k12-sdk-js/src';
import { AuthService } from 'src/app/core/services';
import { MatDialog } from '@angular/material/dialog';
import { SubjectsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/subjectsEndpoint';
import { CurriculumsFilterRequests } from 'sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum-filter-request';
import { UserTypeEndpoint } from 'sdk/cls-k12-sdk-js/src/services/usertype/endpoints/UserTypeEndpoint';
import { GradeEndpoint } from 'sdk/cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';
import { ChaptersEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/chaptersEndpoint';
import { DivisionOption } from 'sdk/cls-k12-sdk-js/src/services/division/models/Division';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { DivisionEndpoint, SchoolEndpoint, UserEndpoint, LevelManages } from 'cls-k12-sdk-js/src';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { GlobalEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { Currculum } from 'sdk/cls-k12-sdk-js/src/services/manage/models/curriculum';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';
import { CurriculumEndpoint } from 'sdk/cls-k12-sdk-js/src/services/curriculum/endpoint/curriculum-endpoint';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
const lessonsPaginationDefault: PaginatorResponse<Lesson> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'learn-list-lesson',
  templateUrl: './list-lesson.component.html',
  styleUrls: ['./list-lesson.component.scss']
})
export class ListLessonComponent implements OnInit {
  baseApiUrl = '';

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  @ViewChild(MatPaginator) paginator: MatPaginator;
  //lessonData: PaginatorResponse<Lesson>;
  lessonsPagination: PaginatorResponse<Lesson> = lessonsPaginationDefault;
  lessonDataMapTeacherName: PaginatorResponse<Lesson>;
  dataSource: Lesson[];
  /*  //#region Endpoint  */
  endpointLesson: LessonEndpoint;
  endpointUserType: UserTypeEndpoint;
  endpointUser: UserEndpoint;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointSubject: SubjectsEndpoint;
  endpointContentPanel: GlobalEndpoint;
  curriculumEndpoint: CurriculumEndpoint;
  endpointChapter: ChaptersEndpoint
  userIdentity: UserIdentity;
  levelManageValue: number = 0;
  hasApprovePermission: boolean;
  hasAddLessonPermission: boolean;
  hasCopyLessonPermission: boolean;
  hasEditLessonPermission: boolean;
  hasDeleteLessonPermission: boolean;
  hasPermissionAssign: boolean;
  flexValue: number = 0;
  flexfullRow: number = 0;

  /*  //#endregion  */

  /*  //#region FromGroup  */
  filterLessonForm = this.fb.group({
    divisionId: new FormControl(''),
    schoolId: new FormControl(''),
    gradeId: new FormControl(''),
    subjectId: new FormControl(''),
    contentPanelId: new FormControl(),
    chapterId: new FormControl(),
    unitId: new FormControl(),
    userId: new FormControl(),
    createdDate: new FormControl(),
    endDate: new FormControl(),
    lessonName: new FormControl(),
    page: 0,
    size: 10,
    getCount: true,
    sortDirection: 'DESC',
    sortField: 'CRE',
  });

  /* //#endregion */

  /* loading sniper */
  isLoadingResults = false;
  /* load teacher commbox */
  isShowteacherCombobox = false;

  get lessonStatusEnum(): typeof LessonStatus {
    return LessonStatus;
  }
  constructor(private _authService: AuthService
    , public dialog: MatDialog
    , private _learnMessageService: LearnMessageService
    , private _router: Router
    , private fb: FormBuilder
    , private spinner: NgxSpinnerService
    , @Inject(DEFAULT_NGXSPINNER) public spinnerConfig
    , private _snackBar: MatSnackBar) {

    this.endpointUserType = new UserTypeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUser = new UserEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointLesson = new LessonEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubject = new SubjectsEndpoint({ baseUrl: this.baseApiUrl })
    this.endpointContentPanel = new GlobalEndpoint();
    this.curriculumEndpoint = new CurriculumEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointChapter = new ChaptersEndpoint({ baseUrl: this.baseApiUrl })
    this.userIdentity = _authService.getTokenInfo();
    this.hasApprovePermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Approved);
    this.hasAddLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Add);
    this.hasCopyLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Clone);
    this.hasEditLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Edit);
    this.hasDeleteLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Delete);
    this.hasPermissionAssign = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Assign);

    this.isShowteacherCombobox = this.userIdentity.userTypeRole == 'SUPER'
    //console.log(this.userIdentity);
    if (this.userIdentity.levelManage == LevelManages.Department) {
      this.levelManageValue = 1;
      this.flexValue = 25;
    } else if (this.userIdentity.levelManage == LevelManages.Division) {
      this.levelManageValue = 2;
      this.flexValue = 50;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else {
      this.levelManageValue = 3;
      this.flexValue = -25;
      if (this.isShowteacherCombobox) {
        this.flexValue = 50;
      }
      // Tài khoản cấp Trường, schoolid fillter mặc định theo  id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
      this.filterLessonForm.controls['schoolId'].setValue(this.userIdentity.schoolId);
      // this.usersPaginationFilter.schoolId = this.userIdentity.schoolId;
    }
    if (!this.isShowteacherCombobox) {
      this.flexValue += 25;
    }
  }


  isShowFilter = true;

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  groupStudentFilter = {
    schoolId: null,
    gradeId: null
  }
  lessonClone;
  oldSchoolSelected: number;

  minDate: Date = new Date();

  displayedColumns: string[] = ['stt', 'name', 'weight', 'symbol', 'function'];

  //#endregion

  // mã phòng
  // bài học
  lessonId: number = 0;

  // #endregion

  /* Division */
  protected divisions: DivisionOption[];
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  /* School */
  protected schools: SchoolOption[];
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  /* School */
  protected grades: GradeOption[];
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  /* Subject */
  protected subjects: SubjectOption[];
  public subjectFilterCtrl: FormControl = new FormControl();
  public filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  @ViewChild('singleSelectSubject', { static: true }) singleSelectSubject: MatSelect;

  /* Curriculum */
  protected contentPanels: Currculum[];
  public contentPanelFilterCtrl: FormControl = new FormControl();
  public filteredContentPanels: ReplaySubject<Currculum[]> = new ReplaySubject<Currculum[]>(1);
  @ViewChild('singleSelectContentPanel', { static: true }) singleSelectContentPanel: MatSelect;

  /* Chapter */
  protected chapters: Chapter[];
  public chapterFilterCtrl: FormControl = new FormControl();
  public filteredChapters: ReplaySubject<Chapter[]> = new ReplaySubject<Chapter[]>(1);
  @ViewChild('singleSelectChapter', { static: true }) singleSelectChapter: MatSelect;

  /* Unit */
  protected units: Unit[];
  public unitFilterCtrl: FormControl = new FormControl();
  public filteredUnits: ReplaySubject<Unit[]> = new ReplaySubject<Unit[]>(1);
  @ViewChild('singleSelectUnit', { static: true }) singleSelectUnit: MatSelect;

  /* Teacher */
  protected teachers: Teacher[];
  public teacherFilterCtrl: FormControl = new FormControl();
  public filteredTeachers: ReplaySubject<Teacher[]> = new ReplaySubject<Teacher[]>(1);
  @ViewChild('singleSelectTeacher', { static: true }) singleSelectTeacher: MatSelect;

  protected _onDestroy = new Subject<void>();

  /* Public property */
  get divisionId() { return this.filterLessonForm.get('divisionId'); }
  isEnableDivision = true;
  isFirstLoadDivision = true;

  get schoolId() { return this.filterLessonForm.get('schoolId'); }
  isDisableSchoolId = false;
  isFirstLoadSchool = true;

  get gradeId() { return this.filterLessonForm.get('gradeId'); }
  isDisableGradeId = false;
  isFirstLoadGrade = true;

  get subjectId() { return this.filterLessonForm.get('subjectId'); }
  isDisableSubjectId = true;
  isFirstLoadSubject = true;

  get contentPanelId() { return this.filterLessonForm.get('contentPanelId'); }
  isDisableContentPanelId = true;
  isFirstLoadContentPanel = true;

  get chapterId() { return this.filterLessonForm.get('chapterId'); }
  isDisableChapterId = true;
  isFirstLoadChapter = true;

  get unitId() { return this.filterLessonForm.get('unitId'); }
  isDisableUnitId = true;
  isFirstLoadUnit = true;

  get createdDate() { return this.filterLessonForm.get('createdDate'); }
  get endDate() { return this.filterLessonForm.get('endDate'); }

  get userId() { return this.filterLessonForm.get('userId'); }
  isDisableUserId = true;

  get lessonName() { return this.filterLessonForm.get('lessonName'); }
  get page() { return this.filterLessonForm.get('page'); }
  get size() { return this.filterLessonForm.get('size'); }



  ngOnInit(): void {
    this.getLessonsFilter(this.filterLessonForm.value);
    this.getFilterParam()
    this.lessonClone = { ...this.filterLessonForm.value };
  }
  pageChange(event?: PageEvent) {
    this.filterLessonForm.controls['page'].setValue(this.paginator.pageIndex + 1);
    this.filterLessonForm.controls['size'].setValue(this.paginator.pageSize);
    this.getLessonsFilter(this.filterLessonForm.value);
  }

  ngAfterViewInit() {
    this.divisionId.valueChanges
      .pipe(startWith(this.divisionId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            // reset change
            this.onResetSchoolSelect();
            this.onResetGradeSelect();
            this.onResetSubjectSelect();
            this.onResetContentPanelSelect();
            this.onResetChapterSelect();
            this.onResetUnitSelect();
            this.onReseTeacherSelect();

            this.isDisableSubjectId = true;
            this.isDisableContentPanelId = true;
            this.isDisableChapterId = true;
            this.isDisableUnitId = true;
            this.isDisableUserId = true;
          }
        }
      )

    this.schoolId.valueChanges
      .pipe(startWith(this.schoolId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
            // reset change
            this.onResetGradeSelect();
            this.onResetSubjectSelect();
            this.onResetContentPanelSelect();
            this.onResetChapterSelect();
            this.onResetUnitSelect();
            this.onReseTeacherSelect();

            this.isDisableSubjectId = true;
            this.isDisableContentPanelId = true;
            this.isDisableChapterId = true;
            this.isDisableUnitId = true;
            if (value < 1) {
              this.isDisableUserId = true;
            } else {
              this.isDisableUserId = false;
            }
          }
        }
      )

    this.gradeId.valueChanges
      .pipe(startWith(this.gradeId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
            this.onResetSubjectSelect();
            this.onResetContentPanelSelect();
            this.onResetChapterSelect();
            this.onResetUnitSelect();
            this.onReseTeacherSelect();

            if (value < 1) {

              this.isDisableSubjectId = true;
              this.isDisableContentPanelId = true;
              this.isDisableChapterId = true;
              this.isDisableUnitId = true;
            } else {

              this.isDisableSubjectId = false;
            }
          }
        }
      )

    this.subjectId.valueChanges
      .pipe(startWith(this.subjectId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
            this.onResetContentPanelSelect();
            this.onResetChapterSelect();
            this.onResetUnitSelect();
            this.onReseTeacherSelect();
            if (value < 1) {
              this.isDisableContentPanelId = true;
              this.isDisableChapterId = true;
              this.isDisableUnitId = true;
            } else {
              this.isDisableContentPanelId = false;
            }
          }
        });

    this.contentPanelId.valueChanges
      .pipe(startWith(this.contentPanelId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
            this.onResetChapterSelect();
            this.onResetUnitSelect();
            this.onReseTeacherSelect();

            if (value < 1) {
              this.isDisableChapterId = true;
              this.isDisableUnitId = true;
            } else {
              this.isDisableChapterId = false;
            }
          }
        }
      );

    this.chapterId.valueChanges
      .pipe(startWith(this.chapterId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
            this.onResetUnitSelect();

            if (value < 1) {
              this.isDisableUnitId = true;
            } else {
              this.isDisableUnitId = false;
            }
          }
        }
      );

    // listen for search field value changes 
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        console.log(this.divisionId.value);
        this.filterDivisions();
      });


    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    this.subjectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSubjects();
      });

    this.contentPanelFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterContentPanels();
      });

    this.chapterFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterChapters();
      });

    this.unitFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterUnits();
        if (this.unitId.value < 0) {

        }
      });

    this.teacherFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterTeachers();
      });

    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected setInitialValue() {

    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectDivision && (this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id);
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool && (this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id);
      });

    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGrade && (this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id);
      });

    this.filteredSubjects
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSubject && (this.singleSelectSubject.compareWith = (a: SubjectOption, b: SubjectOption) => a && b && a.id === b.id);
      });

    this.filteredContentPanels
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectContentPanel && (this.singleSelectContentPanel.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id);
      });

    this.filteredChapters
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectChapter && (this.singleSelectChapter.compareWith = (a: Chapter, b: Chapter) => a && b && a.id === b.id);
      });

    this.filteredUnits
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectUnit && (this.singleSelectUnit.compareWith = (a: Unit, b: Unit) => a && b && a.id === b.id);
      });
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSubjects() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.subjectFilterCtrl.value;
    if (!search) {
      this.filteredSubjects.next(this.subjects.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSubjects.next(
      this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterContentPanels() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.contentPanelFilterCtrl.value;
    if (!search) {
      this.filteredContentPanels.next(this.contentPanels.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredContentPanels.next(
      this.contentPanels.filter(contentPanel => contentPanel.curriculumName.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterChapters() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.chapterFilterCtrl.value;
    if (!search) {
      this.filteredChapters.next(this.chapters.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredChapters.next(
      this.chapters.filter(chapter => chapter.curriculumName.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterUnits() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.unitFilterCtrl.value;
    if (!search) {
      this.filteredUnits.next(this.units.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredUnits.next(
      this.units.filter(unit => unit.curriculumName.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterTeachers() {

    // get the search keyword
    let search = this.teacherFilterCtrl.value;
    if (!search) {
      this.teachers && this.filteredTeachers.next(this.teachers.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredTeachers.next(
      this.teachers.filter(teacher => teacher.name.toLowerCase().indexOf(search) > -1)
    );
  }

  //#endregion


  /* Get division */
  onDivisionSelectClicked() {
    if (this.isFirstLoadDivision) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivision = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          console.log('dvsid', this.divisions);
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(err => {
          // s  console.log(err);
        })
    } else {

    }
  }

  /* Get school */
  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionId.value;
    if (this.userIdentity.levelManage == LevelManages.Division) {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchool) {
      this.endpointSchool.getSchoolOptions(this.divisionId.value)
        .then(res => {
          this.isFirstLoadSchool = false;
          this.schools = res;
          this.schoolFilter.divisionId = divisionIdSelected;

          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(

        )
    } else {

    }
  }

  /* Get Grade */
  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolId.value == 0 ? null : this.schoolId.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrade || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrade = false;
          this.grades = res;

          if (this.grades && !this.grades.find(x => x.id == 0)) {
            this.grades.unshift({ id: 0, name: 'Chọn khối' });
          }
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());

        })
        .catch(err => { console.log(err) }
        )
    } else {

    }
  }

  /* Get Subject */
  onSubjectSelectClicked() { // TODO : system service was not have this API
    if (this.isDisableSubjectId == true || this.gradeId.value == null || this.gradeId.value == 0 || !this.isFirstLoadSubject) {
      return;
    } else {
      this.endpointSubject.getSubjectByGradeIdOptions(this.gradeId.value).then(res => {
        this.subjects = res;
        this.isFirstLoadSubject = false;
        if (this.subjects && !this.subjects.find(x => x.id == 0)) {
          this.subjects.unshift({ id: 0, name: 'Chọn môn' });
        }
        this.filteredSubjects.next(this.subjects.slice());
      }).catch(err => {
        // sthis.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
    }
  }

  /* Get contentPanel */
  onContentPanelSelectClicked() {
    if (this.isDisableContentPanelId || !this.isFirstLoadContentPanel
      || this.gradeId.value == 0 || this.gradeId.value == null
      || this.subjectId.value == 0 || this.subjectId.value == null) {
      return;
    }
    let value = new CurriculumsFilterRequests();
    value.gradeId = this.gradeId.value;
    value.subjectId = this.subjectId.value;
    this.curriculumEndpoint
      .getComboboxCurriculumFilter(this.userIdentity.divisionId, this.userIdentity.schoolId, this.gradeId.value, this.subjectId.value)
      .then(res => {
        console.log(res)
        this.contentPanels = res;
        this.isFirstLoadContentPanel = false;
        if (this.contentPanels && !this.contentPanels.find(x => x.curriculumId == 0)) {
          this.contentPanels.unshift({ curriculumId: 0, curriculumName: 'Chọn khung', getActiveStatus: '', gradeName: '', isActived: true, parentId: 0, subjectName: '' });
        }
        this.filteredContentPanels.next(this.contentPanels.slice())

        // reset change
        this.onResetChapterSelect();
        this.onResetUnitSelect();
      })
      .catch(err => {
        // s  this.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      })
  }

  /* Get Chapter */
  onChapterSelectClicked() { // TODO : system service was not have this API
    if (this.isDisableChapterId == true || this.contentPanelId.value == null
      || this.contentPanelId.value == 0 || !this.isFirstLoadChapter) {
      return;
    }
    this.endpointContentPanel
      .getCurriculumChildrentNodesByPortalId(this.userIdentity.portalId, this.contentPanelId.value)
      .then((res) => {
        this.chapters = res;
        this.isFirstLoadChapter = false;
        if (this.chapters && !this.chapters.find(x => x.id == 0)) {
          this.chapters.unshift({ id: 0, curriculumName: 'Chọn chương' });
        }
        this.filteredChapters.next(this.chapters.slice());
      })
      .catch(err => {
        // s  this.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
  }

  /* Get Unit */
  onUnitSelectClicked() { // TODO : system service was not have this API
    if (this.isDisableUnitId == true || !this.isFirstLoadUnit) {
      return;
    }

    this.endpointContentPanel
      .getCurriculumChildrentNodesByPortalId(this.userIdentity.portalId, this.chapterId.value)
      .then((res) => {
        this.units = res;

        if (this.units && !this.units.find(x => x.id == 0)) {
          this.units.unshift({ id: 0, curriculumName: 'Chọn bài' });
        }
        this.filteredUnits.next(this.units.slice());
      })
      .catch(err => {
        // s  this.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
  }

  /* Get Teacher */
  onTeacherSelectClicked() { // TODO : system service was not have this API
    if (this.schoolId.value) {
      this.endpointUser
        .getTeacherOfSchoolOptions(this.schoolId.value)
        .then(res => {
          this.teachers = res;
          if (this.teachers && !this.teachers.find(x => x.id == 0)) {
            this.teachers.unshift({ id: 0, name: 'Chọn giáo viên' });
          }
          this.filteredTeachers.next(this.teachers.slice())
        })
        .catch(err => {
          // s  this.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
        });
    }
  }

  /* reset change School */
  private onResetSchoolSelect() {
    this.filterLessonForm.controls['schoolId'].setValue(0);
    this.isFirstLoadSchool = true;
  }

  /* reset change Grade */
  private onResetGradeSelect() {
    this.filterLessonForm.controls['gradeId'].setValue(0);
    this.isFirstLoadGrade = true;
  }

  /* reset change Subject */
  private onResetSubjectSelect() {
    this.filterLessonForm.controls['subjectId'].setValue(0);
    this.isFirstLoadSubject = true;
  }

  /* reset change ContentPanel */
  private onResetContentPanelSelect() {
    this.filterLessonForm.controls['contentPanelId'].setValue(0);
    this.isFirstLoadContentPanel = true;
  }

  /* reset change Chapter */
  private onResetChapterSelect() {
    this.filterLessonForm.controls['chapterId'].setValue(0);
    this.isFirstLoadChapter = true;
  }

  /* reset change Unit*/
  private onResetUnitSelect() {
    this.filterLessonForm.controls['unitId'].setValue(0);
    this.isFirstLoadUnit = true;
  }

  private onReseTeacherSelect() {
    this.filterLessonForm.controls['userId'].setValue(0);
  }

  /*Form submit */
  onSubmit() {
    if (compareTwoObject(this.lessonClone, this.filterLessonForm.value)) {
      console.warn(' Does not change request object ')
      return;
    }

    this.getFilterParam()
    this.getLessonsFilter(this.filterLessonForm.value);
  }

  /* Information lesson */
  getLessonsFilter(formvalue: FormGroup) {
    this.spinner.show();
    this.isLoadingResults = true;
    this.lessonClone = { ...this.filterLessonForm.value };
    this.endpointLesson.getLessonFilter(this.filterLessonForm.value)
      .then((res) => {
        this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
        this.filterLessonForm.controls['page'].setValue(this.paginator.pageIndex);
        this.filterLessonForm.controls['size'].setValue(this.paginator.pageSize);
        this.lessonsPagination = res;
        let userIdArr: Array<number> = [];
        this.lessonsPagination.items.forEach((value, index) => {
          userIdArr.push(value.userId);
        });

        // distinct arr user
        userIdArr = [...new Set(userIdArr)];

        // mapping name into list lesson
        this.endpointUser.GetListUserByIds(userIdArr).then(user => {
          this.lessonsPagination.items.forEach((value, index) => {
            value.userName = user.find(x => x.id == value.userId).name;
            this.isLoadingResults = false;
          })
          this.spinner.hide();

        }).catch(err => {
          console.log(err)
          this.isLoadingResults = false;
          this.spinner.hide();
        })
      })
      .catch((err) => {
        this.isLoadingResults = false;
        this.spinner.hide();
        this._learnMessageService.failToast(" Có lỗi xảy ra vui lòng thử lại ");
      });
  }



  getFilterParam() {
    this.paginator.pageIndex = 0
    this.filterLessonForm.controls['page'].setValue(this.paginator.pageIndex + 1);
    this.filterLessonForm.controls['size'].setValue(this.paginator.pageSize);
    if (this.userIdentity.levelManage == LevelManages.Division) {
      this.filterLessonForm.controls['divisionId'].setValue(this.userIdentity.divisionId);
    } else if (this.userIdentity.levelManage == LevelManages.School) {
      this.filterLessonForm.controls['divisionId'].setValue(this.userIdentity.divisionId);
      this.filterLessonForm.controls['schoolId'].setValue(this.userIdentity.schoolId);
    }
  }

  public addLesson() {
    if (this.hasAddLessonPermission) {

      this._router.navigate(['/dashboard/learn/create-lesson']);
    } else {
      console.error(' Does not have add permission ');
    }
  }

  registerLesson(lesson: Lesson): void {
    if (lesson.status == LessonStatus.Publish) {
      this._router.navigate([`/dashboard/learn/signup-for-lectures-and-review-students/${lesson.id}`])
    }
  }

  editLesson(id: number): void {
    if (this.hasEditLessonPermission) {

      this._router.navigate([`/dashboard/learn/edit-content-lesson/${id}`])
    } else {
      console.error(' Does not have edit permission ')
    }
  }

  copyLesson(id: number): void {
    this._learnMessageService.successToast(`Tính năng đang phát triển !`);
  }

  deleteLesson(data): void {
    if (!this.hasDeleteLessonPermission) {
      console.error(' Does not have edit permission ')
      return
    }

    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      disableClose: true,
      width: '444px',
      height: 'auto',
      //data: data
      data: {
        title: 'Xóa bài giảng',
        name: data.name,
        message: ''
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
        this.endpointLesson.deleteLessonById(data.id)
          .then(res => {
            if (res) {
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: 'Xóa bài giảng thành công',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });

              this.getLessonsFilter(this.filterLessonForm.value);
            } else {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Xóa bài giảng không thành công',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            }
          })
          .catch((err: ErrorResponse) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: err.errorCode,
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
      }
      else {
        if (result != undefined) {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Bạn nhập chưa đúng, vui lòng thử lại',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      }
    });
    // this.openDialog().subscribe(res => {
    //   if (res) {
    //     this.endpointLesson.deleteLessonById(id).then(res => {
    //       if (res) {
    //         this._learnMessageService.successToast(` Xóa thành công bài giảng`);
    //         this.getLessonsFilter(this.filterLessonForm.value);

    //       } else {
    //         this._learnMessageService.failToast(` Có lỗi xảy ra vui lòng thử lại `);
    //       }
    //     })
    //       .catch((err) => {
    //         this._learnMessageService.failToast(` Xóa thất bại. Vui lòng thử lại `);
    //       })
    //   }
    // })
  }

  clearDate(): void {
    this.createdDate.setValue('');
    this.endDate.setValue('');
  }

  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }

  getFirstName(fullName: string): string {
    fullName = fullName.trim();
    // let lastIndex = fullName.lastIndexOf(' ');
    // if (lastIndex != -1) {
    //   return fullName[lastIndex + 1];
    // }
    return fullName[0].toUpperCase();
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddWebContentComponent } from './add-web-content.component';

describe('AddWebContentComponent', () => {
  let component: AddWebContentComponent;
  let fixture: ComponentFixture<AddWebContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddWebContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddWebContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

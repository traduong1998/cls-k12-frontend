import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { StepHeaderControl } from '../../intefaces/stepHeaderControl';

@Component({
  selector: 'app-add-web-content',
  templateUrl: './add-web-content.component.html',
  styleUrls: ['./add-web-content.component.scss']
})
export class AddWebContentComponent implements OnInit {

  stepHeaderControl: StepHeaderControl;
  lessonEnpoint: LessonEndpoint;
  lessonId:number;
  lessonName: string;
  isReady: boolean = false;

  constructor(private route: ActivatedRoute) {
    this.lessonEnpoint = new LessonEndpoint();
    this.lessonId = + this.route.snapshot.paramMap.get("id");
    this.lessonEnpoint.getLessonById(this.lessonId).then((lesson)=>{
      this.stepHeaderControl = {
        currentPage: 2,
        isCreatingContent: true,
        step: lesson.step
      }
      this.lessonName = lesson.name;
      this.isReady = true;
    });
  }
  ngOnInit(): void {
  }

}

import { Component, ElementRef, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSelect } from '@angular/material/select';
import { Router } from '@angular/router';
import { CKEditor4, CKEditorComponent } from 'ckeditor4-angular';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable, of, ReplaySubject, Subject } from 'rxjs';
import 'rxjs/add/observable/interval';
import { pairwise, startWith, take, takeUntil } from 'rxjs/operators';
import { CLSModules, CLSPermissions, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/errorResponse';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { Curriculum } from 'sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum';
import { CurriculumsFilterRequests } from 'sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum-filter-request';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { GradeEndpoint } from 'sdk/cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { ChaptersEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/chaptersEndpoint';
import { SubjectsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/subjectsEndpoint';
import { AuthService } from 'src/app/core/services';
import { MimeTypes } from 'src/app/shared/enums/mime-type';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import videojs from 'video.js';
import { ProgressDialogComponent } from '../../components/progress-dialog/progress-dialog.component';
import { Chapter } from '../../intefaces/chapter';
import { StepHeaderControl } from '../../intefaces/stepHeaderControl';
import * as sj from '../../intefaces/subject';
import { Unit } from '../../intefaces/unit';
import { LearnMessageService } from '../../Services/learn-message-service.service';
import { ProgressDialogService } from './../../Services/progress-dialog.service';
import { GlobalEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { ConfirmationDialogComponent } from 'src/app/shared/material/confirmation-dialog/confirmation-dialog.component';
import { CurriculumEndpoint } from 'sdk/cls-k12-sdk-js/src/services/curriculum/endpoint/curriculum-endpoint';
import { Configuration } from 'src/app/shared/configurations';
import { Lesson } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/lesson';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';

@Component({
  selector: 'app-create-lesson',
  templateUrl: './create-lesson.component.html',
  styleUrls: ['./create-lesson.component.scss']
})
export class CreateLessonComponent implements OnInit {
  baseApiUrl = 'http://localhost:58910';
  isDisableSaveButton = false;
  imageToolTip = Configuration.MESSAGE_INFOR_IMAGE;
  videoTooltip = Configuration.MESSAGE_INFOR_VIDEO;
  form: FormData = new FormData();
  isLoadingResults: boolean;
  endpointGrade: GradeEndpoint;
  endpointSubject: SubjectsEndpoint
  curriculumEndpoint: CurriculumEndpoint;
  endpointContentPanel: GlobalEndpoint
  endpointChapter: ChaptersEndpoint
  fileServer: FileServerEndpoint;
  lessonId: number;
  stepHeaderControl: StepHeaderControl;
  fileImageName: string;
  fileVideoName: string;
  progressData: Observable<any>;
  hasAddLessonPermission: boolean;
  hasEditLessonPermission: boolean;
  hasDeleteLessonPermission: boolean;
  userIdentity: UserIdentity;
  statusVideo: 'INP' | 'COM' | 'NON';
  private serverFileId: string = null;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }

  private FILE_LIMIT: number = 500;
  numbers: 0;
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>

  playerLearn: videojs.Player;
  @ViewChild('videoLearn', { static: true }) videoLearn: ElementRef<HTMLVideoElement>;
  isSaved = false;

  public acceptImage = '.png,.jpg,.gif,.jpeg';
  /* School */
  protected grades: GradeOption[];
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  /* Subject */
  protected subjects: SubjectOption[];
  public subjectFilterCtrl: FormControl = new FormControl();
  public filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  @ViewChild('singleSelectSubject', { static: true }) singleSelectSubject: MatSelect;

  /* Curriculum */
  protected contentPanels: Curriculum[];
  public contentPanelFilterCtrl: FormControl = new FormControl();
  public filteredContentPanels: ReplaySubject<Curriculum[]> = new ReplaySubject<Curriculum[]>(1);
  @ViewChild('singleSelectContentPanel', { static: true }) singleSelectContentPanel: MatSelect;

  /* Chapter */
  protected chapters: Chapter[];
  public chapterFilterCtrl: FormControl = new FormControl();
  public filteredChapters: ReplaySubject<Chapter[]> = new ReplaySubject<Chapter[]>(1);
  @ViewChild('singleSelectChapter', { static: true }) singleSelectChapter: MatSelect;

  /* Unit */
  protected units: Unit[];
  public unitFilterCtrl: FormControl = new FormControl();
  public filteredUnits: ReplaySubject<Unit[]> = new ReplaySubject<Unit[]>(1);
  @ViewChild('singleSelectUnit', { static: true }) singleSelectUnit: MatSelect;

  protected _onDestroy = new Subject<void>();

  @ViewChild("inputImageFile") inputImageFile: ElementRef;
  @ViewChild("inputVideoFile") inputVideoFile: ElementRef;
  @ViewChild("ckIntroductionLesson") introductionVideo: CKEditorComponent;

  @ViewChild('imageContextMenu') public imageContextMenu: ContextMenuComponent;
  @ViewChild('videoContextMenu') public videoContextMenu: ContextMenuComponent;
  createLessonForm = this.fb.group({
    name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    gradeId: new FormControl('', [Validators.required]),
    subjectId: new FormControl('', [Validators.required]),
    contentPanelId: new FormControl(),
    chapterId: new FormControl('', [Validators.required]),
    unitId: new FormControl('', [Validators.required]),
    accessRange: 0,
    imageURL: [''],
    fileImageURLName: [''],
    videoURL: [''],
    videoURLId: [''],
    fileVideoURLName: [''],
    introduction: ['']
  });
  createLessonFormClone: any;

  get name() { return this.createLessonForm.get('name'); }
  get gradeId() { return this.createLessonForm.get('gradeId'); }
  isFirstLoadGrades: boolean = true;

  get subjectId() { return this.createLessonForm.get('subjectId'); }
  isFirstLoadSubjects: boolean = true;

  isDisableSubjectId: boolean = true;

  get contentPanelId() { return this.createLessonForm.get('contentPanelId'); }
  isFirstLoadContentPanels: boolean = true;

  isDisableContentPanelId: boolean = true;

  get chapterId() { return this.createLessonForm.get('chapterId'); }
  isFirstLoadChapters: boolean = true;

  isDisableChapterId: boolean = true;

  get unitId() { return this.createLessonForm.get('unitId'); }
  isFirstLoadUnits: boolean = true;

  isDisableUnitId: boolean = true;

  get introduction() { return this.createLessonForm.get('introduction'); }

  imageLesson: File;
  videoLesson: File;

  constructor(private router: Router,
    private fb: FormBuilder,
    private _learnMessageService: LearnMessageService,
    private authService: AuthService,
    public dialog: MatDialog,
    private _progressDialogService: ProgressDialogService,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig) {
    this.isLoadingResults = false;
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubject = new SubjectsEndpoint({ baseUrl: this.baseApiUrl })
    this.endpointContentPanel = new GlobalEndpoint()
    this.curriculumEndpoint = new CurriculumEndpoint();
    this.endpointChapter = new ChaptersEndpoint({ baseUrl: this.baseApiUrl })
    this.fileServer = new FileServerEndpoint();
    this.userIdentity = authService.getTokenInfo();
    this.stepHeaderControl = {
      currentPage: 1,
      lessonId: null,
      step: 0
    }
    this.isLoadingResults = false;
    this.statusVideo = 'NON';
    this.hasAddLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Add);
    this.hasEditLessonPermission = this.userIdentity.hasPermission(CLSModules.Lesson, CLSPermissions.Edit);
  }

  ngOnInit(): void {
  }
  ngAfterViewInit() {
    this.createLessonFormClone = { ...this.createLessonForm.value };
    this.gradeId.valueChanges
      .pipe(startWith(this.gradeId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
            this.onResetSubjectSelect();
            this.onResetContentPanelSelect();
            this.onResetChapterSelect();
            this.onResetUnitSelect();
            // this.onReseTeacherSelect();

            if (value < 1) {

              this.isDisableSubjectId = true;
              this.isDisableContentPanelId = true;
              this.isDisableChapterId = true;
              this.isDisableUnitId = true;
            } else {

              this.isDisableSubjectId = false;
            }
          }
        }
      )

    this.subjectId.valueChanges
      .pipe(startWith(this.subjectId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
            this.onResetContentPanelSelect();
            this.onResetChapterSelect();
            this.onResetUnitSelect();
            // this.onReseTeacherSelect();
            if (value < 1) {

              this.isDisableContentPanelId = true;
              this.isDisableChapterId = true;
              this.isDisableUnitId = true;
            } else {
              this.isDisableContentPanelId = false;
              // this.isDisableChapterId = false;
              // this.isDisableUnitId = false;
            }
          }
        });

    this.contentPanelId.valueChanges
      .pipe(startWith(this.contentPanelId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change 
            this.onResetChapterSelect();
            this.onResetUnitSelect();
            // this.onReseTeacherSelect();

            if (value < 1) {
              this.isDisableChapterId = true;
              this.isDisableUnitId = true;
              this.createLessonForm.controls["chapterId"].setErrors(null);
              this.createLessonForm.controls["chapterId"].clearValidators();
              this.createLessonForm.controls["chapterId"].updateValueAndValidity();

              this.createLessonForm.controls["unitId"].setErrors(null);
              this.createLessonForm.controls["unitId"].clearValidators();
              this.createLessonForm.controls["unitId"].updateValueAndValidity();
            } else {
              this.isDisableChapterId = false;
              this.createLessonForm.controls["chapterId"].setValidators([Validators.required]);

              this.createLessonForm.controls["unitId"].setValidators([Validators.required]);

              this.createLessonForm.controls["unitId"].updateValueAndValidity();
              this.createLessonForm.controls["chapterId"].updateValueAndValidity();

              this.chapterId.markAsTouched();
              this.chapterId.markAsDirty();

              this.unitId.markAsTouched();
              this.unitId.markAsDirty();
            }
          }
        }
      );

    this.chapterId.valueChanges
      .pipe(startWith(this.chapterId.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {

            // reset change
            this.onResetUnitSelect();

            if (value < 1) {
              this.isDisableUnitId = true;
            } else {
              this.isDisableUnitId = false;
            }
          }
        }
      );

    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    this.subjectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSubjects();
      });

    this.contentPanelFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterContentPanels();
      });

    this.chapterFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterChapters();
      });

    this.unitFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterUnits();
        if (this.unitId.value < 0) {
        }
      });

    console.log('this.createLessonForm', this.createLessonForm);

    // this.confirmBeforLoader()
  }

  protected setInitialValue() {
    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGrade && (this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id);
      });

    this.filteredSubjects
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSubject && (this.singleSelectSubject.compareWith = (a: sj.SubjectOption, b: sj.SubjectOption) => a && b && a.id === b.id);
      });

    this.filteredContentPanels
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectContentPanel && (this.singleSelectContentPanel.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id);
      });

    this.filteredChapters
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectChapter && (this.singleSelectChapter.compareWith = (a: Chapter, b: Chapter) => a && b && a.id === b.id);
      });

    this.filteredUnits
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectUnit && (this.singleSelectUnit.compareWith = (a: Unit, b: Unit) => a && b && a.id === b.id);
      });

  }

  /* reset change Subject */
  private onResetSubjectSelect() {
    this.createLessonForm.controls['subjectId'].setValue(null);
    this.isFirstLoadSubjects = true;

    this.subjectId.markAsTouched();
    this.subjectId.markAsDirty();
  }

  /* reset change ContentPanel */
  private onResetContentPanelSelect() {
    this.createLessonForm.controls['contentPanelId'].setValue(0);
    this.createLessonForm.controls['contentPanelId'].clearValidators();
    this.createLessonForm.controls['contentPanelId'].updateValueAndValidity();
    this.isFirstLoadContentPanels = true;
    // this.createLessonForm.controls['contentPanelFilterCtrl'].setValue(null);
  }

  /* reset change Chapter */
  private onResetChapterSelect() {
    this.createLessonForm.controls['chapterId'].setValue(null);
    this.isFirstLoadChapters = true;
    // this.createLessonForm.controls['chapterFilterCtrl'].setValue(null);
  }

  /* reset change Unit*/
  private onResetUnitSelect() {
    this.createLessonForm.controls['unitId'].setValue(null);
    this.isFirstLoadUnits = true;
    // this.createLessonForm.controls['unitFilterCtrl'].setValue(null);
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterSubjects() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.subjectFilterCtrl.value;
    if (!search) {
      this.filteredSubjects.next(this.subjects.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSubjects.next(
      this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterContentPanels() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.contentPanelFilterCtrl.value;
    if (!search) {
      this.filteredContentPanels.next(this.contentPanels.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredContentPanels.next(
      this.contentPanels.filter(contentPanel => contentPanel.curriculumName.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterChapters() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.chapterFilterCtrl.value;
    if (!search) {
      this.filteredChapters.next(this.chapters.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredChapters.next(
      this.chapters.filter(chapter => chapter.curriculumName.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterUnits() {
    if (!this.subjects) {
      return;
    }
    // get the search keyword
    let search = this.unitFilterCtrl.value;
    if (!search) {
      this.filteredUnits.next(this.units.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredUnits.next(
      this.units.filter(unit => unit.curriculumName.toLowerCase().indexOf(search) > -1)
    );
  }

  imageUrl: any = null;
  videoUrl: any = null;

  configCkeditor = {
    // startupFocus: true,
    height: 150,
    allowedContent: false,
    EditorType: CKEditor4.EditorType.CLASSIC,
    removeButtons: 'Image'
  };

  editorType: CKEditor4.EditorType.CLASSIC;

  onChangeImageInput(event): void {
    let file: File = null;
    if (event.target.files && event.target.files[0]) {
      file = event.target.files[0];

      // Kiểm tra limit file
      let confirmLimit = Configuration.getLimitFileMessage(file);
      if (!confirmLimit.status) {
        this._learnMessageService.failToast(confirmLimit.message);
        return;
      }

      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      Promise.resolve()
        .then(() => {
          console.log(`start process`);
          this.openProgressDialog();
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.fileServer.uploadFile({
            file: file,
            isPrivate: false,
            moduleName: ServiceName.LEARN
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          }) : null;
        })
        .then(res => {
          this.imageUrl = res ? res.link : null;
          this.fileImageName = res?.name;
          this.createLessonForm.controls['imageURL'].setValue(this.imageUrl);
          this.createLessonForm.controls['videoURLId'].setValue(res.fileId);
          this.createLessonForm.controls['fileImageURLName'].setValue(res?.name);
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }

  onChangeVideoInput(event): void {
    var file: File = null;
    if (event.target.files && event.target.files[0]) {
      file = event.target.files[0];
      let confirmLimit = Configuration.getLimitFileMessage(file);

      if (!confirmLimit.status) {
        this._learnMessageService.failToast(confirmLimit.message);
        return;
      }

      Promise.resolve()
        .then(() => {
          this.openProgressDialog(null);
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.fileServer.uploadFile({
            file: file,
            isPrivate: true,
            moduleName: ServiceName.LEARN
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              var subscription = this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          }) : null;
        })
        .then((res) => {

          this.serverFileId && this.fileServer.skipFile(this.serverFileId);

          return res;
        })
        .then((res) => {
          console.log(`Hoàn thành`);

          this.videoUrl = res ? res.link : null;
          this.serverFileId = res.fileId;
          this.createLessonForm.controls['videoURL'].setValue(this.videoUrl);
          this.createLessonForm.controls['videoURLId'].setValue(res.fileId);

          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {

          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }

  uploadImage(): void {
    this.inputImageFile.nativeElement.click();
  }

  uploadVideo(): void {
    this.inputVideoFile.nativeElement.click();
  }

  onChangeImageHandleClick() {
    if (this.createLessonForm.controls['imageURL'].value) {
      return;
    }
    this.inputImageFile.nativeElement.click();
  }

  onChangeVideoHandleClick() {
    if (this.createLessonForm.controls['videoURL'].value) {
      return;
    }
    this.inputVideoFile.nativeElement.click();
  }

  introductionLesson: any;
  onSubmit() {
    if (this.createLessonForm.valid) {
      this.spinner.show();
      if (JSON.stringify(this.createLessonFormClone) != JSON.stringify(this.createLessonForm.value)) {
        let lesson = this.createLessonForm.value;
        let lessonEndpoint = new LessonEndpoint();
        debugger;
        lessonEndpoint.createLesson(lesson).then(res => {
          if (res) {
            this.createLessonFormClone = { ...this.createLessonForm.value };
            this.lessonId = res.id;
            this._learnMessageService.successToast();
            this.router.navigate([`/dashboard/learn/add-content-lesson/${res.id}`]);
          } else {
            this._learnMessageService.failToast();
          }

          this.spinner.hide();
        })
          .catch((err: ErrorResponse) => {
            this.spinner.hide();
            this._learnMessageService.failToast(" Có lỗi hệ thống xảy ra");
          });
      } else {

        this.spinner.hide();
        this.router.navigate([`/dashboard/learn/add-content-lesson/${this.lessonId}`]);
      }
    }
  }

  onSaveButtonHandleClick() {
    this.isDisableSaveButton = true;
    this.spinner.show();
    if (this.createLessonForm.valid) {
      if (JSON.stringify(this.createLessonFormClone) == JSON.stringify(this.createLessonForm.value)) {
        this.spinner.hide();
        return;
      }
      let lesson = this.createLessonForm.value;
      let lessonEndpoint = new LessonEndpoint();
      lessonEndpoint.createLesson(lesson).then(res => {
        if (res) {
          this.createLessonFormClone = { ...this.createLessonForm.value };
          this.lessonId = res.id;
          this.isSaved = true;
          this._learnMessageService.successToast();
        } else {
          this._learnMessageService.failToast();
        }
        
        this.spinner.hide();
      })
        .catch((err: ErrorResponse) => {
          this.isDisableSaveButton = false;
          this.spinner.hide();
          this._learnMessageService.failToast(" Có lỗi hệ thống xảy ra");
        });
    }
  }

  /* Get Grade */
  onGradeSelectClicked() {
    if (this.isFirstLoadGrades) {
      this.endpointGrade.getGradeOptions(this.userIdentity.schoolId)
        .then(res => {
          this.grades = res;

          if (this.grades && !this.grades.find(x => x.id == 0 || x.id == null)) {
            this.grades.unshift({ id: null, name: 'Chọn khối' });
          }
          this.filteredGrades.next(this.grades.slice());
          this.isFirstLoadGrades = false;
        })
        .catch(err => { console.log(err) }
        )
    }

  }

  /* Get Subject */
  onSubjectSelectClicked() { // TODO : system service was not have this API
    if (!this.isFirstLoadSubjects || this.isDisableSubjectId == true || this.gradeId.value == null || this.gradeId.value == 0) {
      return;
    } else {
      this.endpointSubject.getSubjectByGradeIdOptions(this.gradeId.value).then(res => {
        this.subjects = res;

        if (this.subjects && !this.subjects.find(x => x.id == 0 || x.id == null)) {
          this.subjects.unshift({ id: null, name: 'Chọn môn' });
        }
        this.filteredSubjects.next(this.subjects.slice());
        this.isFirstLoadSubjects = false;
        console.log(this.createLessonForm);
      }).catch(err => {
        // sthis.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
    }
  }

  /* Get contentPanel */
  onContentPanelSelectClicked() { // TODO : system service was not have this API
    if (this.isDisableContentPanelId || !this.isFirstLoadContentPanels
      || this.gradeId.value == 0 || this.gradeId.value == null
      || this.subjectId.value == 0 || this.subjectId.value == null) {
      return;
    }

    let value = new CurriculumsFilterRequests();
    value.gradeId = this.gradeId.value;
    value.subjectId = this.subjectId.value;
    this.curriculumEndpoint
      .getComboboxCurriculumCreateUpdate(this.userIdentity.divisionId, this.userIdentity.schoolId, this.gradeId.value, this.subjectId.value)
      .then(res => {
        this.contentPanels = res

        if (this.contentPanels && !this.contentPanels.find(x => x.curriculumId == 0 || x.curriculumId == null)) {
          this.contentPanels.unshift({ curriculumId: 0, curriculumName: 'Chọn khung', subjectName: 'subjectName', gradeName: 'gradeName', isActived: true, getActiveStatus: '' });
        }
        this.filteredContentPanels.next(this.contentPanels.slice())

        // reset change
        this.onResetChapterSelect();
        this.onResetUnitSelect();
      })
      .catch(err => {
      })
  }

  /* Get Chapter */
  onChapterSelectClicked() { // TODO : system service was not have this API
    if (!this.isFirstLoadChapters || this.isDisableChapterId || this.contentPanelId.value == null || this.contentPanelId.value == 0) {
      return;
    }
    this.endpointContentPanel
      .getCurriculumChildrentNodesByPortalId(this.userIdentity.portalId, this.contentPanelId.value)
      .then((res) => {
        this.chapters = res;

        if (this.chapters && !this.chapters.find(x => x.id == 0 || x.id == null)) {
          this.chapters.unshift({ id: null, curriculumName: 'Chọn chương' });
        }
        this.filteredChapters.next(this.chapters.slice());
        this.isFirstLoadChapters = false;
      })
      .catch(err => {
        // s  this.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
  }

  /* Get Unit */
  onUnitSelectClicked() { // TODO : system service was not have this API
    if (!this.onResetUnitSelect || this.isDisableUnitId || !this.isFirstLoadUnits) {
      return;
    }

    this.endpointContentPanel
      .getCurriculumChildrentNodesByPortalId(this.userIdentity.portalId, this.chapterId.value)
      .then(res => {
        this.units = res;
        if (this.units && !this.units.find(x => x.id == 0 || x.id == null)) {
          this.units.unshift({ id: null, curriculumName: 'Chọn bài học' });
        }
        this.isFirstLoadUnits = false;
        this.filteredUnits.next(this.units.slice());
      })
      .catch(err => {
        // s  this.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
  }

  /* context menu */
  onContextMenuRemoveImageHandler() {
    this.imageUrl = null;
    this.fileImageName = '';
    this.createLessonForm.controls['imageURL'].setValue(null);
    this.createLessonForm.controls['fileImageURLName'].setValue(null);
  }

  onContextMenuRemoveVideoHandler() {
    this.videoUrl = null;
    this.fileVideoName = '';
    this.createLessonForm.controls['videoURL'].setValue(null);
    this.createLessonForm.controls['fileVideoURLName'].setValue('');
    // this.playerLearn.src({ src: this.videoUrl, type: MimeTypes.m3u8 });
  }

  /*
   * open progress dialog 
   */
  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.numbers }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  onClickBack() {
    let current: Lesson = this.createLessonForm.value;
    if (JSON.stringify(this.createLessonFormClone) !== JSON.stringify(current)) {
      this.openConfirmDialog(Configuration.MESSAGE_EXIT_CONFIRM).subscribe((confirm) => {
        if (!confirm) {
          return;
        } else {

          this.router.navigateByUrl('dashboard/learn/list-lesson');
        }
      })
    } else { this.router.navigateByUrl('dashboard/learn/list-lesson'); }
  }

  private openConfirmDialog(value: string): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject<boolean>();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: {
        message: `${value}`,
        buttonText: {
          ok: 'Có',
          cancel: 'Không'
        }
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      confirm.next(confirmed);
    });
    return confirm.asObservable();
  }


  @HostListener('window:beforeunload', ['$event'])
  unloadHandler($event) {
    let current: Lesson = this.createLessonForm.value;
    if (JSON.stringify(this.createLessonFormClone) === JSON.stringify(current)) {

    } else {
      return false
    }
  }
}

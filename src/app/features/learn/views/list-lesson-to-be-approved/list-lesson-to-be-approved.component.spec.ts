import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLessonToBeApprovedComponent } from './list-lesson-to-be-approved.component';

describe('ListLessonToBeApprovedComponent', () => {
  let component: ListLessonToBeApprovedComponent;
  let fixture: ComponentFixture<ListLessonToBeApprovedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListLessonToBeApprovedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLessonToBeApprovedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

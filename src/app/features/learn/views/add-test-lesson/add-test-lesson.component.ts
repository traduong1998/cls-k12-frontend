import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { TestService } from '../../Services/test.service';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services';
import { UseCase } from '../../intefaces/useCase';
import { StepHeaderControl } from '../../intefaces/stepHeaderControl';
import { LearnMessageService } from '../../Services/learn-message-service.service';

@Component({
  selector: 'app-add-test-lesson',
  templateUrl: './add-test-lesson.component.html',
  styleUrls: ['./add-test-lesson.component.scss']
})
export class AddTestLessonComponent implements OnInit, OnDestroy {

  lessonId: number;
  stepheaderCtrl: StepHeaderControl;
  submitEventsSubject: Subject<void> = new Subject<void>();
  submitEvents$ = this.submitEventsSubject.asObservable();
  lessonEndpoint: LessonEndpoint;
  lessonName: string;
  isReady: boolean = false;
  subcription: Subscription;
  invalidContentTest: boolean = false;
  invalidConfigTest: boolean = false;
  public get useCaseEnum(): typeof UseCase {
    return UseCase;
  }

  constructor(private _router: Router, private route: ActivatedRoute, private testService: TestService, private _learnMessageService: LearnMessageService, private spinner: NgxSpinnerService) {
    this.lessonEndpoint = new LessonEndpoint();

  }
  ngOnDestroy(): void {
    //this.subcription.unsubscribe();
  }

  ngOnInit(): void {
    this.lessonId = +this.route.snapshot.paramMap.get("id");
    this.lessonEndpoint.getLessonById(this.lessonId).then((lesson) => {
      this.stepheaderCtrl = {
        lessonId: this.lessonId,
        currentPage: 2,
        isCreatingContent: true,
        step: lesson.step
      };
      this.lessonName = lesson.name;
      this.isReady = true;
    });
  }

  onCancelClick(): void {
    if (this.stepheaderCtrl.step >= 2) {
      this._router.navigate([`/dashboard/learn/edit-content-lesson/${this.lessonId}`]);
    } else {
      this._router.navigate([`/dashboard/learn/add-content-lesson/${this.lessonId}`]);
    }
  }
  onSaveClick(): void {
    this.isSingleClick = true;
    setTimeout(() => {
      if (this.isSingleClick) {
        this.submitEventsSubject.next();
        this.spinner.show();
        if (this.invalidContentTest || this.invalidConfigTest) {
          this.spinner.hide();
          return;
        }
        this.testService.submitAddTest(this.lessonId).then((res) => {
          this.spinner.hide();
          this.testService.sub.unsubscribe();
          if (res) {
            this._learnMessageService.successToast("Tạo bài kiểm tra thành công!");
            this._router.navigate([`/dashboard/learn/edit-content-lesson/${this.lessonId}`]);
          }
          else {
            this._learnMessageService.failToast("Có lỗi xảy ra!");
          }
        });
      }
    }, 250);
  }
  onDoubleClick(): void {
    this.isSingleClick = false;
  }
  isSingleClick: Boolean = true;

  invalidContentEmit(invalidContent: boolean): void {
    this.invalidContentTest = invalidContent;
  };
  invalidConfigEmit(invalidConfig: boolean): void {
    this.invalidConfigTest = invalidConfig;
  };
}

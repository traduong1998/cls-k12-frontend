import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTestLessonComponent } from './add-test-lesson.component';

describe('AddTestLessonComponent', () => {
  let component: AddTestLessonComponent;
  let fixture: ComponentFixture<AddTestLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTestLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTestLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddIframeContentComponent } from './add-iframe-content.component';

describe('AddIframeContentComponent', () => {
  let component: AddIframeContentComponent;
  let fixture: ComponentFixture<AddIframeContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddIframeContentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddIframeContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

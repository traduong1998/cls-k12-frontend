import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StepHeaderControl } from './../../intefaces/stepHeaderControl';
import { ActivatedRoute, Router } from '@angular/router';
import { UseCase } from './../../intefaces/useCase';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { TestService } from '../../Services/test.service';
import { LessonEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/lessonEndpoint';
import { LearnMessageService } from '../../Services/learn-message-service.service';

@Component({
  selector: 'app-edit-test-lesson',
  templateUrl: './edit-test-lesson.component.html',
  styleUrls: ['./edit-test-lesson.component.scss']
})
export class EditTestLessonComponent implements OnInit {
  @Input() lessonStatus: string;
  submitEventsSubject: Subject<void> = new Subject<void>();
  submitEvents$ = this.submitEventsSubject.asObservable();
  contentId: number;
  lessonId: number;
  lessonName: string;
  step: number;
  stepHeaderCtrl: StepHeaderControl;
  lessonEndpoint: LessonEndpoint;
  subcription: Subscription;
  isReady: boolean = false;
  invalidContentTest: boolean = false;
  invalidConfigTest: boolean = false;

  public get useCaseEnum(): typeof UseCase {
    return UseCase;
  }
  constructor(private _router: Router, private route: ActivatedRoute, private testService: TestService, private _learnMessageService: LearnMessageService, private spinner: NgxSpinnerService) {
    this.lessonId = +this.route.snapshot.paramMap.get("idLesson");
    this.contentId = +this.route.snapshot.paramMap.get("id");
    this.lessonEndpoint = new LessonEndpoint();
    this.lessonEndpoint.getLessonById(this.lessonId).then((lesson) => {
      this.stepHeaderCtrl = {
        lessonId: this.lessonId,
        currentPage: 2,
        isCreatingContent: true,
        step: lesson.step
      };
      this.lessonName = lesson.name;
      this.isReady = true;
    });
    this.subcription = new Subscription();
  }

  ngOnInit(): void {
  }
  ngOnDestroy() {
    // this.subcription.unsubscribe();
  }

  onCancelClick(): void {
    this._router.navigate([`/dashboard/learn/edit-content-lesson/${this.lessonId}`]);
  }

  onSaveClick(): void {
    this.isSingleClick = true;
    setTimeout(() => {
      if(this.isSingleClick){
        this.submitEventsSubject.next();
        this.spinner.show();
        if (this.invalidContentTest || this.invalidConfigTest) {
          this.spinner.hide();
          return;
        }
        this.testService.submitEditTest(this.lessonId, this.contentId, this.lessonStatus).then((res) => {
          this.testService.editSub.unsubscribe();
          this.spinner.show();
          if (res != null) {
            if (res) {
              this._learnMessageService.successToast("Cập nhật thành công");
              this._router.navigate([`/dashboard/learn/edit-content-lesson/${this.lessonId}`]);
            } else {
              this._learnMessageService.failToast("Có lỗi xảy ra vui lòng thử lại!");
            }
          }
        });
      }
    }, 250);
  }

  onDoubleClick(): void {
    this.isSingleClick = false;
  }
  isSingleClick: Boolean = true;
  invalidContentEmit(invalidContent: boolean): void {
    this.invalidContentTest = invalidContent;
  };
  invalidConfigEmit(invalidConfig: boolean): void {
    this.invalidConfigTest = invalidConfig;
  };

}

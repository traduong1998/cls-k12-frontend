import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTestLessonComponent } from './edit-test-lesson.component';

describe('EditTestLessonComponent', () => {
  let component: EditTestLessonComponent;
  let fixture: ComponentFixture<EditTestLessonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditTestLessonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTestLessonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

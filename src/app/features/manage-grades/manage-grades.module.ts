import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { ManageGradesRoutingModule } from './manage-grades-routing.module';
import { ManageGradesComponent } from './manage-grades.component';
import { CreateGradeComponent } from './views/create-grade/create-grade.component';
import { UpdateGradeComponent } from './views/update-grade/update-grade.component';
import { ListGradesComponent } from './views/list-grades/list-grades.component';
import { ContentCreateGradeComponent } from './components/content-create-grade/content-create-grade.component';
import { ContentUpdateGradeComponent } from './components/content-update-grade/content-update-grade.component';
import { ContentListGradesComponent } from './components/content-list-grades/content-list-grades.component';
import { ListSubjectsComponent } from './views/list-subjects/list-subjects.component';
import { ContentListSubjectsOfGradeComponent } from './components/content-list-subjects/content-list-subject.component';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [ManageGradesComponent, CreateGradeComponent, UpdateGradeComponent, ListGradesComponent, ContentCreateGradeComponent, ContentUpdateGradeComponent, ContentListGradesComponent, ListSubjectsComponent, ContentListSubjectsOfGradeComponent],
  imports: [
    SharedModule,
    ManageGradesRoutingModule,
    NgxSpinnerModule
  ]
})
export class ManageGradesModule { }

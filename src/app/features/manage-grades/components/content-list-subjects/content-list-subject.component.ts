import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Grade } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition, MatSnackBar } from '@angular/material/snack-bar';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { SubjectEndpoint } from 'cls-k12-sdk-js/src/services/subject/endpoints/SubjectEndpoint';
import { AddSubjectToGradeRequest } from 'cls-k12-sdk-js/src/services/subject/requests/addSubjectToGradeRequest';
import { SubjectOfGrade } from 'cls-k12-sdk-js/src/services/subject/models/Subject';
import { SubjectOption } from 'cls-k12-sdk-js/src/services/subject/models/Subject';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { UserIdentity } from 'cls-k12-sdk-js/src/core/identity/models/user-identity-model';
import { CLSModules, CLSPermissions } from 'cls-k12-sdk-js/src';
import { AuthService } from 'src/app/core/services';


@Component({
  selector: 'app-content-list-subject',
  templateUrl: './content-list-subject.component.html',
  styleUrls: ['./content-list-subject.component.scss']
})
export class ContentListSubjectsOfGradeComponent implements OnInit {
  isLoadingResults = true;
  baseApiUrl = 'http://localhost:65000';
  subjectEndpoint: SubjectEndpoint;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  selection = new SelectionModel<Grade>(true, []);
  selectedSubjectIds = [];
  subjectListsOfGrade: SubjectOfGrade[] = [];
  subjectLists: SubjectOption[] = [];
  displayedColumns: string[] = ['id', 'name'];
  hasAddPermission: boolean;
  hasEditPermission: boolean;
  userIdentity: UserIdentity;

  constructor(private mdDialogRef: MatDialogRef<ContentListSubjectsOfGradeComponent>, @Inject(MAT_DIALOG_DATA) public gradeInfo: Grade, private _snackBar: MatSnackBar,private _authService: AuthService) {
    this.subjectEndpoint = new SubjectEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();
    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.Grade, CLSPermissions.Add);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.Grade, CLSPermissions.Edit);
  }

  grade: Grade = {
    id: this.gradeInfo.id,
    name: this.gradeInfo.name,
  }

  ngOnInit(): void {
    this.getGradesData();
  }

  failureCallback() {
    this.isLoadingResults = false;
    alert("hihi 400");
  }


  getGradesData() {
    // this.gradeEndpoint.getGradeOptions(null)
    // .then(res => {
    //   this.gradeLists = res;
    //   this.gradeEndpoint.getGradeOfSchool(this.school.id)
    //     .then(mes => {
    //       this.gradeListsOfSchool = mes;
    //       this.gradeLists.forEach((v, i) => {
    //         let item = this.gradeListsOfSchool.find(x => x.id == v.id);

    //         if (item != null) {
    //           this.selection.select(v);
    //           this.selectedGradeIds.push(v.id);
    //         }

    //       });

    //     })
    //   console.log(this.selectedGradeIds);

    // })
    // .catch(this.failureCallback);
    this.isLoadingResults = true;
    this.subjectEndpoint.getSubjectOptions(null)
      .then(res => {
        this.subjectLists = res;
        this.subjectEndpoint.getSubjectOfGrade(this.grade.id)
          .then(mes => {
            this.subjectListsOfGrade = mes;

            this.subjectListsOfGrade.forEach((v, i) => {
              this.subjectLists.forEach((u, j) => {
                if (v.id == u.id) {
                  this.selection.select(u);
                  this.selectedSubjectIds.push(u.id);
                }
              })
            });
          })

      })
      .catch(this.failureCallback);
  }

  submitForm() {
    let model = new AddSubjectToGradeRequest();

    model.gradeId = this.grade.id;
    model.subjectId = this.selectedSubjectIds
    this.subjectEndpoint.addSubjectToGrade(model)
      .then(res => {
        if (res) {
          this._snackBar.openFromComponent(SuccessSnackBarComponent, {
            data: 'Gán môn học vào khối thành công',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        } else {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Gán môn học vào khối không thành công',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      })
      .catch((err: ErrorResponse) => {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: err.errorDetail,
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
    this.mdDialogRef.close('submit')
  }

  changeCheck(event, row) {
    if (event.checked) {
      let index = this.selectedSubjectIds.indexOf(row.id);
      if (index == -1) {
        this.selectedSubjectIds.push(row.id);
      }
    } else {
      let index = this.selectedSubjectIds.indexOf(row.id);
      this.selectedSubjectIds.splice(index, 1);
    }
    console.log(this.selectedSubjectIds);
  }


}

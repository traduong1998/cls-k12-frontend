import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentListSubjectsOfGradeComponent } from './content-list-subject.component';

describe('ContentListSubjectsOfGradeComponent', () => {
  let component: ContentListSubjectsOfGradeComponent;
  let fixture: ComponentFixture<ContentListSubjectsOfGradeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentListSubjectsOfGradeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentListSubjectsOfGradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Grade } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { GradeEndpoint } from 'cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';
import { UpdateGradeRequest } from 'cls-k12-sdk-js/src/services/grade/requests/updateGradeRequest';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-content-update-grade',
  templateUrl: './content-update-grade.component.html',
  styleUrls: ['./content-update-grade.component.scss']
})
export class ContentUpdateGradeComponent implements OnInit {

  gradeEndpoint: GradeEndpoint;
  lblname = '';
  lblOrderNumber = '';
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  baseApiUrl = 'http://localhost:65000';

  grade: UpdateGradeRequest = {
    name: '',
  };

  constructor(private mdDialogRef: MatDialogRef<ContentUpdateGradeComponent>,
    @Inject(MAT_DIALOG_DATA) public gradeInfo: Grade,
    private _snackBar: MatSnackBar) {
    this.gradeEndpoint = new GradeEndpoint({ baseUrl: this.baseApiUrl });
  }

  formGrade: Grade = {
    name: this.gradeInfo.name,
    orderNumber: this.gradeInfo.orderNumber
  }

  ngOnInit(): void {
  }

  resetForm() {
    this.lblname = '';
    this.lblOrderNumber = '';
  }

  checkForm() {
    var result = true;

    if (this.formGrade.name === '') {
      this.lblname = 'Bạn chưa nhập tên Khối!';
      result = false;
    }
    // if (!this.formGrade.orderNumber) {
    //   this.lblOrderNumber = 'Bạn chưa nhập số thứ tự!';
    //   result = false;
    // } else if (!Number.isInteger(Number(this.formGrade.orderNumber))) {
    //   this.lblOrderNumber = 'Số thứ tự phải là kiểu số';
    //   result = false;
    // }

    return result;
  }


  EditDivision(regForm: NgForm) {
    this.resetForm();

    if (this.checkForm()) {
      this.gradeEndpoint.checkNameGrade(this.gradeInfo["id"], this.formGrade.name)
        .then(res => {

          //hàm này đặc thù trả về true, false nên sẽ vô then. nếu hàm bình thường thì thường backend sẽ thow exception nếu k thỏa điều kiện để update hoặc đầu vào bị validate
          if (res) {
            //this.lblname = 'Tên khối đã tồn tại trong hệ thống!';
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Tên khối đã tồn tại trong hệ thống!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });

            return;
          }

          this.grade["id"] = this.gradeInfo["id"];
          this.grade["name"] = this.formGrade.name;
          this.grade["orderNumber"] = this.formGrade.orderNumber;

          this.gradeEndpoint.updateGrade(this.grade)
            .then(res => {
              //ok. then nghĩa là response trả về 200

              if (res) {
                this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                  data: 'Sửa khối thành công',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
                this.mdDialogRef.close('submit')
              } else {
                this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                  data: 'Sửa khối không thành công',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            })
            .catch((err: ErrorResponse) => {

              //response khác 200 sẽ vào catch. nen trên backend cần trả cho đúng
              //catch thì k close mdDialogRef
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: err.errorDetail,
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            });
        })
        .catch((err: ErrorResponse) => {

          //response khác 200 sẽ vào catch. nen trên backend cần trả cho đúng
          //catch thì k close mdDialogRef
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });

    }
  }
}
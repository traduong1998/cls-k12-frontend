import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentUpdateGradeComponent } from './content-update-grade.component';

describe('ContentUpdateGradeComponent', () => {
  let component: ContentUpdateGradeComponent;
  let fixture: ComponentFixture<ContentUpdateGradeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentUpdateGradeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentUpdateGradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ContentCreateGradeComponent } from '../../components/content-create-grade/content-create-grade.component';
import { ContentUpdateGradeComponent } from '../../components/content-update-grade/content-update-grade.component';
import { Grade } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { GradeEndpoint } from 'cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';
import { ContentListSubjectsOfGradeComponent } from '../content-list-subjects/content-list-subject.component';
import { UserIdentity, CLSModules, CLSPermissions } from 'cls-k12-sdk-js/src';
import { AuthService } from 'src/app/core/services';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { DeleteGradeRequest } from 'sdk/cls-k12-sdk-js/src/services/grade/requests/deleteGradeRequest';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ListGradeVm } from './view-models/list-grade-VM';
import { ThemeConstant } from 'src/app/core/enums/theme.constant';
import { SubjectOfGrade } from 'sdk/cls-k12-sdk-js/src/services/grade/models/subject-of-grade';



@Component({
  selector: 'app-content-list-grades',
  templateUrl: './content-list-grades.component.html',
  styleUrls: ['./content-list-grades.component.scss']
})
export class ContentListGradesComponent implements OnInit {
  gradeEndpoint: GradeEndpoint;

  baseApiUrl = 'http://localhost:65000';

  displayedColumns: string[] = ['stt', 'name', 'orderNumber', 'listSubject', 'function'];

  //gradesPagination: Grade[]
  gradesPagination: ListGradeVm[] = [];

  subjectOfGrade: SubjectOfGrade[];
  selection = new SelectionModel<Grade>(true, []);
  selectedGradeIds = [];
  isLoadingResults = true;

  userIdentity: UserIdentity;
  hasAddPermission: boolean;
  hasDeletePermission: boolean;
  hasEditPermission: boolean;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(private _snackBar: MatSnackBar, public dialog: MatDialog, private _router: Router, private _authService: AuthService, private spinner: NgxSpinnerService,) {
    this.gradeEndpoint = new GradeEndpoint({ baseUrl: this.baseApiUrl });

    this.userIdentity = _authService.getTokenInfo();
    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.Grade, CLSPermissions.Add);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.Grade, CLSPermissions.Edit);
    this.hasDeletePermission = this.userIdentity.hasPermission(CLSModules.Grade, CLSPermissions.Delete);
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit(): void {
    this.getGradesData();
  }

  failureCallback() {
    this.isLoadingResults = false;
    alert("hihi 400");
  }
  isReady;
  getGradesData() {
    this.spinner.show();
    this.gradesPagination = [];
    this.isLoadingResults = true;
    Promise.resolve()
      .then(() => {

        this.isReady = false;
        return this.gradeEndpoint.getGradesPaging();
      })
      .then((res) => {

        let listGradeId = res.map(x => x.id);
        Promise.all([
          this.gradeEndpoint.getListSubjectOfGrade(listGradeId)
            .then(response => {
              this.spinner.hide();
              this.subjectOfGrade = response;
            })
        ])
          .then(() => {
            res.forEach((item) => {
              let name;
              let subjects = this.subjectOfGrade.find(x => x.gradeId == item.id);
              if (!subjects) {
                name = '';
              }
              else {
                name = subjects.subjectName;
              }
              this.gradesPagination.push(ListGradeVm.From(item, name));
            })
            this.isReady = true;
          })
      })
  }


  //constructor() { }
  openDialog() {
    this.selection.clear();
    const dialogRef = this.dialog.open(ContentCreateGradeComponent, {
      disableClose: true,
      width: '450px',
      height: 'auto',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getGradesData();
        }
      }
    });
  }

  ngAfterViewInit() {
  }

  /**Show dialog Update Grade */
  UpdateGrade(data) {
    this.selection.clear();
    //show dialog
    const dialogRef = this.dialog.open(ContentUpdateGradeComponent, {
      disableClose: true,
      width: '450px',
      height: 'auto',
      data: data
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getGradesData();
        }
      }
    });
  }

  /**Show dialog Delete Grade */
  DeleteGrade(data) {
    this.selection.clear();

    this.gradeEndpoint.countGradeOfSchool(data.id)
      .then(res => {
        if (res == 0) {
          //show dialog
          const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
            disableClose: true,
            width: '444px',
            height: 'auto',
            //data: data
            data: {
              title: 'Xóa khối',
              name: data.name,
              message: ''
            }
          });

          dialogRef.afterClosed().subscribe(result => {
            if (result == 'DONGY') {
              let model = new DeleteGradeRequest();
              model.id = data.id;
              this.gradeEndpoint.deleteGrade(model)
                .then(res => {
                  if (res) {
                    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                      data: 'Xóa khối thành công',
                      duration: 3000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                    });
                    this.getGradesData();
                  } else {
                    this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                      data: 'Xóa khối không thành công',
                      duration: 3000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                    });
                  }
                })
                .catch((err: ErrorResponse) => {
                  this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                    data: err.errorCode,
                    duration: 3000,
                    horizontalPosition: this.horizontalPosition,
                    verticalPosition: this.verticalPosition,
                  });
                });
            }
            else {
              if (result != undefined) {
                this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                  data: 'Bạn nhập chưa đúng, vui lòng thử lại',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            }
          });
        } else {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Khối đang thuộc trường, không được phép xóa!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      })
  }

  UpdateSubject(data) {
    this.selection.clear();
    //show dialog
    const dialogRef = this.dialog.open(ContentListSubjectsOfGradeComponent, {
      disableClose: true,
      width: '650px',
      height: 'auto',
      data: data
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getGradesData();
        }
      }
    });
  }
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.gradesPagination.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.selectedGradeIds = [];
    } else {
      this.gradesPagination.forEach(row => {
        this.selection.select(row);
        this.selectedGradeIds.push(row.id);
      })
    }
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Grade): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }

}

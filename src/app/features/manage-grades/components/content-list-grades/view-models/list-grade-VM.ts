import { Grade } from "sdk/cls-k12-sdk-js/src/services/grade/models/Grade";

export class ListGradeVm implements Grade {
    id?: number;
    name?: string;
    orderNumber?: number;
    schoolId?: number;
    portalId?: number;
    isDeleted?: number;
    createBy?: string;
    createdDate?: Date;

    subjectName: string;

    static From(grade: Grade, subjects: string) {
        var m: ListGradeVm = {
            id: grade.id,
            name: grade.name,
            orderNumber: grade.orderNumber,
            schoolId: grade.schoolId,
            portalId: grade.portalId,
            isDeleted: grade.isDeleted,
            createBy: grade.createBy,
            createdDate: grade.createdDate,
            subjectName: subjects
        }
        return m;
    }
}
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentCreateGradeComponent } from './content-create-grade.component';

describe('ContentCreateGradeComponent', () => {
  let component: ContentCreateGradeComponent;
  let fixture: ComponentFixture<ContentCreateGradeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentCreateGradeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentCreateGradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { Grade } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { GradeEndpoint } from 'cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';
import { CreateGradeRequest } from 'cls-k12-sdk-js/src/services/grade/requests/createGradeRequest';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';


@Component({
  selector: 'app-content-create-grade',
  templateUrl: './content-create-grade.component.html',
  styleUrls: ['./content-create-grade.component.scss']
})
export class ContentCreateGradeComponent implements OnInit {

  gradeEndpoint: GradeEndpoint;
  baseApiUrl = 'http://localhost:65000';
  lblname = ''
  lblOrderNumber = ''
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  grade: CreateGradeRequest = {
    name: '',
  };

  constructor(private mdDialogRef: MatDialogRef<ContentCreateGradeComponent>, private _snackBar: MatSnackBar) {
    this.gradeEndpoint = new GradeEndpoint({ baseUrl: this.baseApiUrl });
  }

  ngOnInit(): void {
  }

  resetFrom() {
    this.lblname = '';
    this.lblOrderNumber = '';
  }

  checkForm() {
    var result = true;

    if (this.grade.name === '') {
      this.lblname = 'Bạn chưa nhập tên Khối!';
      result = false;
    }

    // if (!this.grade.orderNumber) {
    //   this.lblOrderNumber = 'Bạn chưa nhập số thứ tự!';
    //   result = false;
    // } else if (!Number.isInteger(Number(this.grade.orderNumber))) {
    //   this.lblOrderNumber = 'Số thứ tự phải là kiểu số';
    //   result = false;
    // }

    return result;
  }

  submitForm(regForm: NgForm) {
    this.resetFrom();
    if (this.checkForm()) {
      this.gradeEndpoint.createGrade(this.grade)
        .then(res => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Thêm khối thành công!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          } else {

            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Xóa khối không thành công!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });

          }
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
      this.mdDialogRef.close('submit')
    }
  }
}


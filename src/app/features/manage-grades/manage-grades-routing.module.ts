import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards';
import { ManageGradesComponent } from './manage-grades.component';
import { CreateGradeComponent } from './views/create-grade/create-grade.component';
import { UpdateGradeComponent } from './views/update-grade/update-grade.component';
import { ListGradesComponent } from './views/list-grades/list-grades.component';
import { ContentListSubjectsOfGradeComponent } from './components/content-list-subjects/content-list-subject.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: ManageGradesComponent,
    children: [
      {
        path: '',
        component: ListGradesComponent,
        data: {
          title: 'Danh sách khối',
        }
      },
      {
        path: 'create',
        component: CreateGradeComponent,
        data: {
          title: 'Thêm mới khối',
        }
      },      
      {
        path: 'update',
        component: UpdateGradeComponent,
        data: {
          title: 'Chỉnh sửa khối',
        }
      },           
      {
        path: 'list-subject-of-grade',
        component: ContentListSubjectsOfGradeComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageGradesRoutingModule { }

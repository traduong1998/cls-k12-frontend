import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListLearningPathComponent } from './list-learning-path.component';

describe('ListLearningPathComponent', () => {
  let component: ListLearningPathComponent;
  let fixture: ComponentFixture<ListLearningPathComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListLearningPathComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListLearningPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

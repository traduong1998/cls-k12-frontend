import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { Router } from '@angular/router';
import { ReplaySubject, Subject } from 'rxjs';
import { pairwise, startWith, takeUntil } from 'rxjs/operators';
import { UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { ContentLearnerDetailEndpoint, LessonSelect } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/endpoints/content-learner-detail-endpoint';
import { MeetingDetailandSignature } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/models/meeting-detail-and-signature';
import { LearningPathStudentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/learning-path-student/endpoints/LearningPathStudentEndpoint';
import { LearningPathModel } from 'sdk/cls-k12-sdk-js/src/services/learning-path-student/models/LearningPathModel';
import { LearningPathOption } from 'sdk/cls-k12-sdk-js/src/services/learning-path-student/models/LearningPathOption';
import { TeacherLearningPathOption } from 'sdk/cls-k12-sdk-js/src/services/learning-path-student/models/TeacherLearningPathOption';
import { VirtualClassRoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/virtual-classroom-endpoints';
import { VirtualClassRoom } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/virtualClassRoom';
import { AuthService } from 'src/app/core/services';

@Component({
  selector: 'app-list-learning-path',
  templateUrl: './list-learning-path.component.html',
  styleUrls: ['./list-learning-path.component.scss']
})
export class ListLearningPathComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  userIdentity: UserIdentity;

  //#region INPUT OUTPUT PROPERTY
  //#endregion

  //#region ENDPOINT
  private virtualClassRoomEndpoint: VirtualClassRoomEndpoint;
  private contentLearnerDetailEndpoint: ContentLearnerDetailEndpoint;
  private learningPathStudentEndpoint: LearningPathStudentEndpoint;
  //#endregion

  //#region PRIVATE PROPERTY
  private learningPathDefault: LearningPathOption[] = [];
  private isFirstLoad = true;
  isReadyLearningPathSelect = false;
  //#endregion

  //#region PUBLIC PROPERTY
  public listLearningPath: LearningPathModel[] = [];
  public count = 0;
  public displayedColumns = ['position', 'learningPathName', 'teacherName', 'startDate', 'endDate', 'status', 'action'];
  public filterLearningPath = this.formGroup.group({
    learningPathId: new FormControl(''),
    teacherId: new FormControl(''),
    startDate: new FormControl(''),
    endDate: new FormControl(''),
    status: new FormControl(''),
    page: new FormControl(0),
    size: new FormControl(10),
  })

  protected learningPathSelect: LearningPathOption[] = [];
  protected teacherSelect: TeacherLearningPathOption[] = [];
  public learningPathSelectFilterCtrl: FormControl = new FormControl();
  public teacherSelectFilterCtrl: FormControl = new FormControl();
  public filteredLearningPath: ReplaySubject<LearningPathOption[]> = new ReplaySubject<LearningPathOption[]>(1);
  public filteredTeacher: ReplaySubject<TeacherLearningPathOption[]> = new ReplaySubject<TeacherLearningPathOption[]>(1);
  @ViewChild('singleSelectLesson', { static: true }) singleSelectDivision: MatSelect;

  public objectRequestClone = null;
  public isFirstLoadLesson: boolean = true;
  public request: any;

  protected _onDestroy = new Subject<void>();
  get learningPathId() { return this.filterLearningPath.get('learningPathId'); }
  get teacherId() { return this.filterLearningPath.get('teacherId'); }
  get startDate() { return this.filterLearningPath.get('startDate'); }
  get endDate() { return this.filterLearningPath.get('endDate'); }
  get status() { return this.filterLearningPath.get('status'); }
  //#endregion

  checkDateNull = new Date('0001-01-02T01:01:01.01');

  currentPage: number = 1;
  totalPage = 1;

  //#region CONSTRUCTOR
  constructor(private formGroup: FormBuilder, private _authService: AuthService, private router: Router) {
    this.virtualClassRoomEndpoint = new VirtualClassRoomEndpoint();
    this.contentLearnerDetailEndpoint = new ContentLearnerDetailEndpoint();
    this.learningPathStudentEndpoint = new LearningPathStudentEndpoint();
    this.userIdentity = _authService.getTokenInfo();


  }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {
    this.objectRequestClone = { ...this.request }
    this.loadData(this.isFirstLoad);
  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.filterLearningPath.controls['page'].setValue(this.paginator.pageIndex);
      this.filterLearningPath.controls['size'].setValue(this.paginator.pageSize);
      this.loadData(this.isFirstLoad);
    });

    this.learningPathId.valueChanges
      .pipe(startWith(this.learningPathId?.value),
        pairwise())
      .subscribe(() => {
      }
      )

    // listen for search field value changes 
    this.learningPathSelectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {

        this.filterLearningPaths();
      });

    this.teacherId.valueChanges
      .pipe(startWith(this.teacherId?.value),
        pairwise())
      .subscribe(() => {
      }
      )

    // listen for search field value changes 
    this.teacherSelectFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {

        this.filterTeachers();
      });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  //#endregion

  //#region PUBLIC METHOD

  public onSubmit() {
    this.isFirstLoad = false;
    this.loadData(this.isFirstLoad);
  }


  onLearningPathSelectHandleClick() {
    this.learningPathStudentEndpoint.getLearningPathOption(this.userIdentity.userId)
      .then(res => {
        this.learningPathSelect = [];
        this.learningPathSelect = res;
        if (this.learningPathSelect && !this.learningPathSelect.find(x => x.id == 0 || x.id == null)) {
          this.learningPathSelect.unshift({ id: null, name: 'Chọn lộ trình' })
        }
        this.filteredLearningPath.next(this.learningPathSelect.slice());
        this.isReadyLearningPathSelect = true;
      })
  }

  onTeacherSelectHandleClick() {

    this.learningPathStudentEndpoint.getTeacherLearningPathOption(this.userIdentity.userId)
      .then(res => {
        this.teacherSelect = [];
        this.teacherSelect = res;
        if (this.teacherSelect && !this.teacherSelect.find(x => x.id == 0 || x.id == null)) {
          this.teacherSelect.unshift({ id: null, name: 'Người tạo' })
        }
        this.filteredTeacher.next(this.teacherSelect.slice());
      })
  }

  protected filterLearningPaths() {
    if (!this.learningPathSelect) {
      return;
    }
    // get the search keyword
    let search = this.learningPathSelectFilterCtrl.value;
    if (!search) {
      this.filteredLearningPath.next(this.learningPathSelect.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredLearningPath.next(
      this.learningPathSelect.filter(learningPath => learningPath.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterTeachers() {
    if (!this.teacherSelect) {
      return;
    }
    // get the search keyword
    let search = this.teacherSelectFilterCtrl.value;
    if (!search) {
      this.filteredTeacher.next(this.teacherSelect.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredTeacher.next(
      this.teacherSelect.filter(teacher => teacher.name.toLowerCase().indexOf(search) > -1)
    );
  }
  //#endregion

  //#region PRIVATE METHOD
  private loadData(isFirstLoad: boolean) {
    var a = this.filterLearningPath.value;
    if (!isFirstLoad) {
      this.request = { ...this.filterLearningPath.value };
      if (this.compareObject(this.objectRequestClone, this.request)) {
        return;
      }
    }

    this.learningPathStudentEndpoint.getListLearningPath(this.request).then(res => {
      this.listLearningPath = res.items;
      this.count = res.totalItems;
    })

    this.objectRequestClone = { ...this.request }
    this.isFirstLoad = false;
  }

  private compareObject(objectfisrt: any, objectSecond: any) {
    if (JSON.stringify(objectfisrt) == JSON.stringify(objectSecond)) {
      return true;
    }
    return false;
  }

  onDetailClick(learningPathId: number) {
    this.router.navigateByUrl(`/learner/user-learning-path/${learningPathId}`);
  }

  isDateNull(date:Date):boolean{
    let dt = new Date(date);
    if(dt>this.checkDateNull){
      return false;
    }
    else{
      return true;
    }
  }
  handlePageEvent(event) {
    if (Number.isInteger(event)) {
      debugger
      this.filterLearningPath.controls['page'].setValue(event-1);
      // this.examReportRequest.page = event
      this.currentPage = event;
    } else {
      this.filterLearningPath.controls['page'].setValue(event.pageIndex);
    }

    this.loadData(this.isFirstLoad);

  }
}

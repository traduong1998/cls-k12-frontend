import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserLearningPathRoutingModule } from './user-learning-path-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from 'ckeditor4-angular';
import { ContextMenuModule } from 'ngx-contextmenu';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxPaginationModule } from 'ngx-pagination';
import { MathModule } from 'src/app/shared/math/math.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { UserLearnRoutingModule } from '../user-learn/user-learn-routing.module';
import { UserLearningPathComponent } from './user-learning-path.component';
import { DetailLearningPathStepComponent } from './views/detail-learning-path-step/detail-learning-path-step.component';
import { DetailLearningPathComponent } from './views/detail-learning-path/detail-learning-path.component';
import { ListLearningPathComponent } from './views/list-learning-path/list-learning-path.component';


@NgModule({
  declarations: [
    UserLearningPathComponent,
    DetailLearningPathComponent,
    DetailLearningPathStepComponent,
    ListLearningPathComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    UserLearningPathRoutingModule,
    MathModule,
    CKEditorModule,
    UserLearnRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ContextMenuModule.forRoot(),
    NgxMatSelectSearchModule,
    NgxPaginationModule
  ]
})
export class UserLearningPathModule { }

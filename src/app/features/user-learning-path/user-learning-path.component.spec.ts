import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLearningPathComponent } from './user-learning-path.component';

describe('UserLearningPathComponent', () => {
  let component: UserLearningPathComponent;
  let fixture: ComponentFixture<UserLearningPathComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserLearningPathComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLearningPathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserLearningPathComponent } from './user-learning-path.component';
import { DetailLearningPathStepComponent } from './views/detail-learning-path-step/detail-learning-path-step.component';
import { DetailLearningPathComponent } from './views/detail-learning-path/detail-learning-path.component';
import { ListLearningPathComponent } from './views/list-learning-path/list-learning-path.component';

const routes: Routes = [
  {
    path: '',
    component: UserLearningPathComponent,
    children: [
      {
        path: '',
        component: ListLearningPathComponent,
        data:{
          title: "Danh sách lộ trình học tập"
        }
      }
    ]
  },
  {
    path: ':learningPathId/detail-learning-path/:learningPathStepId',
    component: DetailLearningPathStepComponent,
    data:{
      title: "Danh sách bài giảng"
    }
  },
  {
    path: ':learningPathId',
    component: DetailLearningPathComponent,
    data:{
      title: "Chi tiết lộ trình học tập"
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserLearningPathRoutingModule { }

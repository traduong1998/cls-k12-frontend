import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TroubleshootingLearnerComponent } from './troubleshooting-learner.component';

describe('TroubleshootingLearnerComponent', () => {
  let component: TroubleshootingLearnerComponent;
  let fixture: ComponentFixture<TroubleshootingLearnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TroubleshootingLearnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TroubleshootingLearnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

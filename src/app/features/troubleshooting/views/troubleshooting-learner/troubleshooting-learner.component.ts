import { X } from '@angular/cdk/keycodes';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { NgxSpinnerService } from 'ngx-spinner';
import { ReplaySubject, Subject } from 'rxjs';
import { pairwise, startWith, take, takeUntil } from 'rxjs/operators';
import { UserEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { TroubleshootingEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/trouble-shooting-endpoint';
import { Troubleshooting } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/troubleshooting';
import { TroubleshootingFilterRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/troubleshooting-filter-request';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';
import { AnswerTroubleshootingDialogComponent } from '../../components/answer-troubleshooting-dialog/answer-troubleshooting-dialog.component';
export interface LessonOption {
  id: number;
  name: string;
}
@Component({
  selector: 'app-troubleshooting-learner',
  templateUrl: './troubleshooting-learner.component.html',
  styleUrls: ['./troubleshooting-learner.component.scss']
})
export class TroubleshootingLearnerComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('dpFromDate') dpFromDate;
  @ViewChild('dpToDate') dpToDate;
  /* Division */
  protected lessons: LessonOption[];
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<LessonOption[]> = new ReplaySubject<LessonOption[]>(1);
  @ViewChild('singleSelectLesson', { static: true }) singleSelectDivision: MatSelect;

  protected _onDestroy = new Subject<void>();
  private troubleshootingEndpoint: TroubleshootingEndpoint;
  private endpointUser: UserEndpoint;
  private listUserIds: number[] = [];

  filterTroubleShootingForm = this.formBuider.group({
    lessonId: new FormControl(''),
    status: new FormControl(''),
    startDate: new FormControl(''),
    endDate: new FormControl(''),
    page: 0,
    size: 10,
    getCount: true,
    sortDirection: 'DESC',
    sortField: 'CRE',
  });

  private request: TroubleshootingFilterRequest = null;
  private requestClone: TroubleshootingFilterRequest = null;
  public isFirstLoadData: boolean = true;
  public isFirstLoadDivision = true;
  public isLoadData = false;
  public troubleshootings: Troubleshooting[] = [];
  public displayedColumns = ['stt', 'name', 'lessonName', 'content', 'datetime', 'function'];
  public count = 0;
  public isShowFilter = true;
  get lessonId() { return this.filterTroubleShootingForm.get('lessonId'); }
  get status() { return this.filterTroubleShootingForm.get('status'); }
  get startDate() { return this.filterTroubleShootingForm.get('startDate'); }
  get endDate() { return this.filterTroubleShootingForm.get('endDate'); }
  get page() { return this.filterTroubleShootingForm.get('page'); }
  get size() { return this.filterTroubleShootingForm.get('size'); }
  get getCount() { return this.filterTroubleShootingForm.get('getCount'); }
  get sortDirection() { return this.filterTroubleShootingForm.get('sortDirection'); }
  get sortField() { return this.filterTroubleShootingForm.get('sortField'); }
  constructor(private formBuider: FormBuilder, public dialog: MatDialog,
    private spinner: NgxSpinnerService,
    @Inject(DEFAULT_NGXSPINNER) public spinnerConfig
  ) {
    this.troubleshootingEndpoint = new TroubleshootingEndpoint();
    this.endpointUser = new UserEndpoint();
  }

  ngOnInit(): void {
    this.requestClone = { ...this.request };
    this.loadData(this.isFirstLoadData);
  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.filterTroubleShootingForm.controls['page'].setValue(this.paginator.pageIndex);
      this.filterTroubleShootingForm.controls['size'].setValue(this.paginator.pageSize);
      this.loadData(this.isFirstLoadData);
    });
    this.lessonId.valueChanges
      .pipe(startWith(this.lessonId?.value),
        pairwise())
      .subscribe(
        ([old, value]) => {
          if (old != value) {
          }
        }
      )

    // listen for search field value changes 
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        console.log(this.lessonId.value);
        this.filterDivisions();
      });
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  /* Get division */
  onDivisionSelectClicked() {
    if (this.isFirstLoadDivision) {
      this.lessons = [];
      this.isFirstLoadDivision = false;
      this.troubleshootingEndpoint.getLessons()
        .then((res) => {
          this.lessons = res;
          if (this.lessons && !this.lessons.find(x => x.id == 0 || x.id == null)) {
            this.lessons.unshift({ id: null, name: 'Chọn bài giảng' })
          }
          this.isFirstLoadDivision = false;
          this.filteredDivisions.next(this.lessons.slice());
        })
    } else {
      this.isFirstLoadDivision = true;
    }
  }

  public onSubmit() {
    this.paginator.pageIndex = 0;
    this.filterTroubleShootingForm.controls['page'].setValue(this.paginator.pageIndex);
    this.filterTroubleShootingForm.controls['size'].setValue(this.paginator.pageSize);
    this.loadData(false);
  }
  public onReviewTroubleHandleClick(value?: number) {
    let data = this.troubleshootings.find(x => x.id === value);
    if (!data) {
      console.error(' can not find id in data onReviewTroubleHandleClick !');
      return;
    }
    this.openAnswerDialogHandleClick(data, 1)
  }

  public onReplyTroubleHandleClick(value?: number) {
    let data = this.troubleshootings.find(x => x.id === value);
    if (!data) {
      console.error(' can not find id in data onReplyTroubleHandleClick !');
      return;
    }
    this.openAnswerDialogHandleClick(data, 2)
  }

  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }

  openDpFromDate() {
    this.dpFromDate.open();
  }
  openDpToDate() {
    this.dpToDate.open();
  }

  clearDate(field: string) {
    if (field === 'from') {
      this.startDate.setValue('');
    } else {
      this.endDate.setValue('');
    }
  }

  protected filterDivisions() {
    if (!this.lessons) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.lessons.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.lessons.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectDivision && (this.singleSelectDivision.compareWith = (a: LessonOption, b: LessonOption) => a && b && a.id === b.id);
      });
  }

  private loadData(isFirstLoadData: boolean) {
    this.spinner.show();
    this.isLoadData = false;
    this.request = {
      lessonId: this.filterTroubleShootingForm.controls['lessonId'].value,
      status: this.filterTroubleShootingForm.controls['status'].value,
      startDate: this.filterTroubleShootingForm.controls['startDate'].value,
      endDate: this.filterTroubleShootingForm.controls['endDate'].value,
      page: this.filterTroubleShootingForm.controls['page'].value,
      size: this.filterTroubleShootingForm.controls['size'].value,
      getCount: this.filterTroubleShootingForm.controls['getCount'].value,
      sortDirection: this.filterTroubleShootingForm.controls['sortDirection'].value,
      sortField: this.filterTroubleShootingForm.controls['sortField'].value
    }

    //if (!compareTwoObject(this.requestClone, this.request)) {
    this.troubleshootings = [];
    this.listUserIds = [];
    this.troubleshootingEndpoint.getListTroubleshootingPaging(this.request).then(res => {
      if (res && res.items) {
        this.requestClone = { ...this.request };
        this.count = res.totalItems;
        // this.troubleshootings = res.items;
        return new Promise((resolve, reject) => {
          res.items.forEach(x => {
            this.listUserIds.push(x.senderId)
          });
          resolve(this.listUserIds);
        })
          .then(rp => {
            this.endpointUser.GetListUserByIds(this.listUserIds).then(users => {
              res.items.forEach(element => {
                let user = users.find(x => x.id === element.senderId)
                let studentFeedback: Troubleshooting = {
                  id: element.id,
                  fbId: element.fbId,
                  lessonId: element.lessonId,
                  lessonName: element.lessonName,
                  senderId: element.senderId,
                  senderName: user?.name,
                  senderAvatar: user?.avatar,
                  senderEmail: user?.emailAddress,
                  senderDate: element.senderDate,
                  content: element.content,
                  feedbackId: element.feedbackId,
                  feedbackContent: element.feedbackContent,
                  feedbackDate: element.feedbackDate,
                  status: element.status,
                  parentId: element.parentId,
                };
                this.troubleshootings.push(studentFeedback);
              });

            })
              .then(() => {
                console.log('success !')
                this.isLoadData = true;
                this.spinner.hide();
              })
              .catch(err => {
                console.log(err);
                this.spinner.hide();
              })
          })

      }
    })
    // } else {
    //   console.warn(' Does not change request object ');
    //   this.spinner.hide();
    //   this.isLoadData = true;
    //   return;
    // }

    this.isFirstLoadData = false;
  }

  private openAnswerDialogHandleClick(troubleshooting: Troubleshooting, typeShow: number) {
    const dialogRef = this.dialog.open(AnswerTroubleshootingDialogComponent, {
      height: 'auto',
      data: { troubleshooting, typeShow }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      if (result) {
        this.loadData(false);
      }
    });
  }

  getFirstName(fullName: string): string {
    fullName = fullName.trim();
    let lastIndex = fullName.lastIndexOf(' ');
    if (lastIndex != -1) {
      return fullName[lastIndex + 1];
    }
    return fullName[0];
  }
}

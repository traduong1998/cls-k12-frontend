import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TroubleshootingLearnerComponent } from './views/troubleshooting-learner/troubleshooting-learner.component';

const routes: Routes = [
  {
    path: '',
    component: TroubleshootingLearnerComponent,
    data: {
      title: 'Danh sách thắc mắc'
    }
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TroubleshootingRoutingModule { }

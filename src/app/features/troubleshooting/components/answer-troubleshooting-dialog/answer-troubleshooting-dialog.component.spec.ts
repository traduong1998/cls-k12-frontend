import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnswerTroubleshootingDialogComponent } from './answer-troubleshooting-dialog.component';

describe('AnswerTroubleshootingDialogComponent', () => {
  let component: AnswerTroubleshootingDialogComponent;
  let fixture: ComponentFixture<AnswerTroubleshootingDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnswerTroubleshootingDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnswerTroubleshootingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

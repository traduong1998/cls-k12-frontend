import { CdkTextareaAutosize } from '@angular/cdk/text-field';
import { Component, Inject, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxSpinnerService } from 'ngx-spinner';
import { take } from 'rxjs/operators';
import { TroubleshootingEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/trouble-shooting-endpoint';
import { Troubleshooting } from 'sdk/cls-k12-sdk-js/src/services/lesson/models/troubleshooting';
import { LearnMessageService } from 'src/app/features/learn/Services/learn-message-service.service';
import { compareTwoObject, countdown } from 'src/app/shared/helpers/cls.helper';
import { DEFAULT_NGXSPINNER } from 'src/app/shared/modules/spinner/config';

@Component({
  selector: 'app-answer-troubleshooting-dialog',
  templateUrl: './answer-troubleshooting-dialog.component.html',
  styleUrls: ['./answer-troubleshooting-dialog.component.scss']
})
export class AnswerTroubleshootingDialogComponent implements OnInit {

  private troubleshootingEndpoint: TroubleshootingEndpoint;
  private troubleFeedbackContentClone: Troubleshooting;
  public feedbackContent: string;
  @ViewChild('autosize') autosize: CdkTextareaAutosize;
  constructor(
    private _ngZone: NgZone,
    public dialogRef: MatDialogRef<AnswerTroubleshootingDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private _learnMessageService: LearnMessageService) {
    dialogRef.disableClose = true;
    this.troubleshootingEndpoint = new TroubleshootingEndpoint();
  }
  
  triggerResize() {
    // Wait for changes to be applied, then trigger textarea resize.
    this._ngZone.onStable.pipe(take(1))
        .subscribe(() => this.autosize.resizeToFitContent(true));
  }
  ngOnInit() {
    this.troubleFeedbackContentClone = { ...this.data.troubleshooting };
  }

  public onFeedbackHandleClick() {
    if(!this.feedbackContent){
      this._learnMessageService.failToast('Vui lòng nhập phản hồi');
      return;
    }
    if (compareTwoObject(this.feedbackContent, this.data.troubleshooting.feedbackContent)) {
      this._learnMessageService.successToast('Đã chỉnh sửa phản hồi');
      return;
    }
    let data = {};
    data = { id: this.data.troubleshooting.fbId, feedbackContent: this.feedbackContent }
    switch (this.data.typeShow) {
      case 1:
        this.troubleshootingEndpoint.updateFeedbackTroubleshooting(data).then(res => {
          if (res) {
            this._learnMessageService.successToast('Đã chỉnh sửa phản hồi');
            //this.dialogRef.close(true);
            this.data.troubleshooting.feedbackContent = this.feedbackContent;
            this.feedbackContent = "";
          }
        })
        break;
      case 2:
        data = { id: this.data.troubleshooting.id, feedbackContent: this.feedbackContent }
        this.troubleshootingEndpoint.createFeedbackTroubleshooting(data).then(res => {
          if (res) {
            this._learnMessageService.successToast('Đã thêm phản hồi');
            this.data.troubleshooting.feedbackContent = this.feedbackContent;
            this.feedbackContent = "";
            //this.dialogRef.close(true);
          }
        })
        break;

      default:
        break;
    }
  }
  public onCloseDialogHandleClick() {
    this.onNoClick();
  }

  private onNoClick(): void {
    this.dialogRef.close(true);
  }
}

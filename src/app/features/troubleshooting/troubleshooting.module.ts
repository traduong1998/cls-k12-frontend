import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TroubleshootingRoutingModule } from './troubleshooting-routing.module';
import { TroubleshootingLearnerComponent } from './views/troubleshooting-learner/troubleshooting-learner.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TroubleshootingComponent } from './troubleshooting.component';
import { DateAdapter } from '@angular/material/core';
import { DateFormat } from 'src/app/shared/helpers/date-format.helper';
import { AnswerTroubleshootingDialogComponent } from './components/answer-troubleshooting-dialog/answer-troubleshooting-dialog.component';
import { LearnMessageService } from '../learn/Services/learn-message-service.service';


@NgModule({
  declarations: [
    TroubleshootingComponent,
    TroubleshootingLearnerComponent,
    AnswerTroubleshootingDialogComponent
  ],
  imports: [
    SharedModule,
    TroubleshootingRoutingModule
  ],
  providers: [
    { provide: DateAdapter, useClass: DateFormat },
    LearnMessageService]
})
export class TroubleshootingModule {
  constructor(private dateAdapter: DateAdapter<Date>) {
    dateAdapter.setLocale("vi-VN");
  }
}

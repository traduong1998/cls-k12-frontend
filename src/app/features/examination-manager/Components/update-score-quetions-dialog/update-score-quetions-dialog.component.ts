
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-update-score-quetions-dialog',
  templateUrl: './update-score-quetions-dialog.component.html',
  styleUrls: ['./update-score-quetions-dialog.component.scss']
})
export class UpdateScoreQuetionsDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<UpdateScoreQuetionsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: number) { }
  onCancelClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
  }

}

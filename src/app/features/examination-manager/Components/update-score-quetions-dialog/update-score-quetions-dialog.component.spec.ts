import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateScoreQuetionsDialogComponent } from './update-score-quetions-dialog.component';

describe('UpdateScoreQuetionsDialogComponent', () => {
  let component: UpdateScoreQuetionsDialogComponent;
  let fixture: ComponentFixture<UpdateScoreQuetionsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateScoreQuetionsDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateScoreQuetionsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

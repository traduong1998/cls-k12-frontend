import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EstablishExamComponent } from './establish-exam.component';

describe('EstablishExamComponent', () => {
  let component: EstablishExamComponent;
  let fixture: ComponentFixture<EstablishExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EstablishExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EstablishExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

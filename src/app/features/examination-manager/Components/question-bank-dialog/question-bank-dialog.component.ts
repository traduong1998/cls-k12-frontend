import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { QuestionBankEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/errorResponse';
import { ListQuestionComponent } from 'src/app/features/manage-question-bank/views/list-question/list-question.component';

import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-question-bank-dialog',
  templateUrl: './question-bank-dialog.component.html',
  styleUrls: ['./question-bank-dialog.component.scss']
})
export class QuestionBankDialogComponent implements OnInit {

  @ViewChild(ListQuestionComponent) listQuestionComponent;
  errors = [];
  isValid: boolean = true;
  questionBankEnpoint: QuestionBankEndpoint;
  selection: any;
  selectedQuestionIds = []
  // component lấy danh sách câu hỏi từ ngân hàng câu hỏi để tạo đề thi
  constructor(private mdDialogRef: MatDialogRef<QuestionBankDialogComponent>,
    private _snackBar: MatSnackBar, private _messageService: MessageService,
    @Inject(MAT_DIALOG_DATA) public data: []) {
    this.questionBankEnpoint = new QuestionBankEndpoint();

  }

  ngOnInit(): void {
    this.selectedQuestionIds = this.data;
  }

  saveData() {
    this.selection = this.listQuestionComponent.selection;
    const arrMultipeId = [];
    if (this.selection.selected.length) {
      this.selection.selected.forEach(value => {
        arrMultipeId.push(value.id);
      });
    }
    if (arrMultipeId.length > 0) {
      this.questionBankEnpoint.getQuestionByIds(arrMultipeId)
        .then(
          res => {
            this.mdDialogRef.close(res);
          }
        )
        .catch((err: ErrorResponse) => {
          this._messageService.failureCallback(err.errorDetail, 1500)
        });
    }
    else {
      this.mdDialogRef.close('cancel');
    }
  }


  cancel() {
    this.mdDialogRef.close('cancel');
  }
}

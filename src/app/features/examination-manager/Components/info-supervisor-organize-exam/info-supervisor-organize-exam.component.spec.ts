import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoSupervisorOrganizeExamComponent } from './info-supervisor-organize-exam.component';

describe('InfoSupervisorOrganizeExamComponent', () => {
  let component: InfoSupervisorOrganizeExamComponent;
  let fixture: ComponentFixture<InfoSupervisorOrganizeExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoSupervisorOrganizeExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoSupervisorOrganizeExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

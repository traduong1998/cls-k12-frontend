
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { TeacherExam } from 'sdk/cls-k12-sdk-js/src/services/exam/models/TeacherExam';
import { ShiftInfoDTO } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/shift-info-dto';
import { ShiftInfo } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/ShiftInfo';
import { UserExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-exam/endpoints/user-exam-endpoint';
import { SupervisorOrganize } from 'sdk/cls-k12-sdk-js/src/services/user-exam/models/supervisor-exam';
import { MessageService } from 'src/app/shared/services/message.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-info-supervisor-organize-exam',
  templateUrl: './info-supervisor-organize-exam.component.html',
  styleUrls: ['./info-supervisor-organize-exam.component.scss']
})
export class InfoSupervisorOrganizeExamComponent implements OnInit {
  userExamEndpoint: UserExamEndpoint;
  supervisorExamSubject: TeacherExam[] = [];
  allComplete: boolean = false;
  dataSource = new MatTableDataSource<TeacherExam>([]);
  listSupervisorId: { examSubjectSupervisorId: number, action: string }[] = [];
  listSupervisorShiftExam: SupervisorOrganize[];
  isValid = true;
  shiftInfo: ShiftInfoDTO;
  displayedColumns: string[] = ['stt', 'name', 'email', 'dob', 'gender'];
  isLoadingResults = true;
  isFullScreen = true;
  constructor(private _httpClient: HttpClient, @Inject(MAT_DIALOG_DATA) public data: any, private messageService: MessageService, private spinner: NgxSpinnerService) {
    this.userExamEndpoint = new UserExamEndpoint();
    this.shiftInfo = data.shiftInfo;
  }
  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    this.isFullScreen = false;
    this.spinner.show("organizeSupervisorLoading");
    this.userExamEndpoint.getSupervisorExamOrganize({ keyword: "", organizeExamId: this.data.organizeExamId, organizeExamInfoId: this.shiftInfo.id }).then((res) => {
      this.supervisorExamSubject = res;
      this.isLoadingResults = false;
      this.spinner.hide("organizeSupervisorLoading");
    })
      .catch(() => {
        this.spinner.hide("organizeSupervisorLoading");
        this.isLoadingResults = false;
        this.messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại!")
      });
  }
  exportExcel() {
    this.isFullScreen = true;
    this.spinner.show("organizeSupervisorLoading");
    this.isLoadingResults = true;
    this.userExamEndpoint.exportExcelExamSubjectSupervisorOrganize(this.shiftInfo, { keyword: null, organizeExamId: this.data.organizeExamId, organizeExamInfoId: this.shiftInfo.id })
      .then((urlFile) => {
        this.isLoadingResults = false;
        this.spinner.hide("organizeSupervisorLoading");
        window.open(urlFile);
      })
      .catch(() => {
        this.isLoadingResults = false;
        this.spinner.hide("organizeSupervisorLoading");
        this.messageService.failureCallback("Có lỗi xảy ra trong quá trình tải file vui lòng thử lại!")
      });
  }
}
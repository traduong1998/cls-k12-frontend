import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ExamEnpoint } from 'sdk/cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { SubjectTestkitRequest } from 'sdk/cls-k12-sdk-js/src/services/exam/requests/update-subject-testkit-request';
import { QuestionConverterEndpoint } from 'sdk/cls-k12-sdk-js/src/services/question-converter/endpoints/question-converter-endpoint';
import { TestKitEnpoint } from 'sdk/cls-k12-sdk-js/src/services/testkit/enpoints/testkit-enpoint';
import { TestKit } from 'sdk/cls-k12-sdk-js/src/services/testkit/models/testkit';
import { MessageService } from 'src/app/shared/services/message.service';
import { DialogConfirm } from '../../interfaces/dialog-confirm';
import { DialogConfirmComponent } from '../dialog-confirm/dialog-confirm.component';
import { ToastComponent } from '../toast/toast.component';

@Component({
  selector: 'app-content-list-testkit',
  templateUrl: './content-list-testkit.component.html',
  styleUrls: ['./content-list-testkit.component.scss']
})
export class ContentListTestkitComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator = Object.create(null);
  @ViewChild(MatSort) sort: MatSort = Object.create(null);

  @ViewChild("file") file: ElementRef;
  routeSub: Subscription;
  //mã ki thi  môn thi
  subjectExamId;
  //ma mon thi
  subjectId;
  //examid
  examId: number;
  gradeId: number;
  testKitId: number;

  stepHeaderControl;
  displayedColumns: string[] = ['stt', 'name', 'createType', 'used', 'function'];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  testKit: TestKit[] = [];
  dialogConfirm: DialogConfirm;
  isPublished: boolean = true;
  private testkitEnpoint: TestKitEnpoint;
  private examApi: ExamEnpoint;

  private questionConverterEndpoint: QuestionConverterEndpoint;

  constructor(private _snackBar: MatSnackBar, private route: ActivatedRoute, private router: Router, private dialog: MatDialog,private _messageService:MessageService) {
    this.examApi = new ExamEnpoint();
    this.questionConverterEndpoint = new QuestionConverterEndpoint();

    this.routeSub = this.route.params.subscribe(params => {
      this.examId = +params['id'];
    });
    this.route.queryParams.subscribe(params => {
      this.gradeId = +params['gradeId'];

      this.subjectId = +params['subjectId'];

      this.subjectExamId = +params['examSubjectId'];

      this.testKitId = +params['testKitId'];
    });

    this.testkitEnpoint = new TestKitEnpoint();
    this.stepHeaderControl = {
      currentPage: 2,
      isCreatingTest: true,
      examId: this.examId,
      step: 2
    }
  }

  ngOnInit(): void {
    this.examApi.getExamStatus(this.examId)
      .then(res => {
        this.isPublished = res == 'PUB';
      })
      .catch();

    // this.testkitEnpoint.getTestKitPaging(this.gradeId, this.subjectId
    // ).then(res => {

    //   this.testKit = res
    // }).catch(err => {
    //   console.log(err);
    // });
  }

  //open file 
  openFile() {
    if (this.isPublished) {
      return;
    }
    this.file.nativeElement.click();
  }

  //get file
  filesSelectedOnChange(files: File[]) {
    if (this.isPublished) {
      return;
    }
    this.router.navigate(['dashboard/examination-manager/list-testkit/',
    this.examId, this.gradeId, this.subjectId, this.subjectExamId, 'view-test']);
    return;
    
    if (files != null) {
      this.questionConverterEndpoint.uploadTestFiles(files)
        .then(res => {
          console.log(res);
          this.router.navigate(['dashboard/examination-manager/list-testkit/',
            this.examId, this.gradeId, this.subjectId, this.subjectExamId, 'view-test']);
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.open(err.errorDetail, 'Ok', {
            duration: 5000,
            panelClass: ['red-snackbar'],
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });
        });
    }
  }

  //open question bank
  openQuestionBank() {
    if (this.isPublished) {
      return;
    }
    //this.router.navigate(['dashboard/examination-manager/list-testkit/', this.  subjectExamId
    this.router.navigate(['/dashboard/errors/feature-work-in-progress']);
  }

  //open matrix
  openMatrix() {
    if (this.isPublished) {
      return;
    }
    //this.router.navigate(['dashboard/examination-manager/list-testkit/', this.  subjectExamId
    this.router.navigate(['/dashboard/errors/feature-work-in-progress']);
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  chooseSubject(testKitId) {
    if (this.isPublished) {
      return;
    }
    let subjectTestkit: SubjectTestkitRequest = { id: this.subjectExamId, testKitId: testKitId }

    this.examApi.updateExamSubjectTestKit(this.subjectExamId, subjectTestkit)
      .then(() => {
        this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "choose-test"]);
      }).catch(err => {
        this._messageService.failureCallback("Thêm đề không thành công",1500);
      });

  }

  openSnackBar(message: string, type: 'error' | 'ok') {
    let panelClass = type == 'ok' ? 'green-snackbar' : 'red-snackbar';
    this._snackBar.open(message, 'Ok', {
      duration: 3000,
      panelClass: [panelClass],
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  deleteTestKit(testKitId) {
    if (this.isPublished) {
      return;
    }
    this.dialogConfirm = { title: 'XÓA BỘ ĐỀ', description: 'Bạn có chắc chắn muốn xóa bộ đề thi này?' }
    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.testkitEnpoint.deleteTestKit(testKitId)
          .then(res => {
            this._messageService.susccessCallback("Xóa bộ đề thành công",1500);
          })
          .then(() => {
            // this.testkitEnpoint.getTestKitPaging(this.gradeId, this.subjectExamId
            // ).then(res => {
            //   this.testKit = res
            // }).catch(err => {
            //   console.log(err);
            // });
          })
          .catch((err: ErrorResponse) => {
           this._messageService.failureCallback("Xóa bộ đề không thành công",1500);
          });
      }
    });
  }

  viewTest(testId) {
    if (this.isPublished) {
      return;
    }
    this.router.navigate(['dashboard/examination-manager/list-testkit/', this.examId, this.gradeId, this.subjectId, this.subjectExamId, 'view-test']);
  }
}
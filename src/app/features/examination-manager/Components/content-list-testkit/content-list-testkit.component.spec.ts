import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentListTestkitComponent } from './content-list-testkit.component';

describe('ContentListTestkitComponent', () => {
  let component: ContentListTestkitComponent;
  let fixture: ComponentFixture<ContentListTestkitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentListTestkitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentListTestkitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

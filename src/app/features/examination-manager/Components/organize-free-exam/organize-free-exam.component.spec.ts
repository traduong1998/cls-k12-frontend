import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizeFreeExamComponent } from './organize-free-exam.component';

describe('OrganizeFreeExamComponent', () => {
  let component: OrganizeFreeExamComponent;
  let fixture: ComponentFixture<OrganizeFreeExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizeFreeExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizeFreeExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

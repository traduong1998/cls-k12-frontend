import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { ExamSubject } from 'cls-k12-sdk-js/src/services/exam/models/ExamSubject';
import { Subscription } from 'rxjs';
import { OrganizeExamEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { OrganizeExamDetail } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/OrganizeExamDetail';
import { ShiftInfo } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/ShiftInfo';
import { UpdateOrganizeExamRequest } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/request/update-organize-exam-request';
import { UserExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-exam/endpoints/user-exam-endpoint';
import { SupervisorOrganize, SupervisorShiftExamEdit } from 'sdk/cls-k12-sdk-js/src/services/user-exam/models/supervisor-exam';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { MessageService } from 'src/app/shared/services/message.service';
import { RoomName } from '../../interfaces/roomName.model';
import { EditSupervisorOrganizeExamComponent } from '../edit-supervisor-organize-exam/edit-supervisor-organize-exam.component';
import { InfoStudentOrganizeExamComponent } from '../info-student-organize-exam/info-student-organize-exam.component';
import { InfoSupervisorOrganizeExamComponent } from '../info-supervisor-organize-exam/info-supervisor-organize-exam.component';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-organize-free-exam',
  templateUrl: './organize-free-exam.component.html',
  styleUrls: ['./organize-free-exam.component.scss']
})
export class OrganizeFreeExamComponent implements OnInit {
  @Input() organizeExamDetail: OrganizeExamDetail;
  public maxStudents = 10;
  hasShift = false;
  @ViewChild('picker') picker: any;
  displayedColumns: string[] = ['stt', 'shiftName', 'realStudent', 'startDate', 'endDate', 'supervision', 'student', 'function'];

  public date: moment.Moment;
  public minDate: moment.Moment;
  public maxDate: moment.Moment;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 60;
  public totalShifts = 1;
  organizeExamEndpoint: OrganizeExamEndpoint;
  userExamEndpoint: UserExamEndpoint;

  public formGroup = new FormGroup({
    date: new FormControl(null, [Validators.required]),
  })
  public roomStudent: RoomName[] = [{ name: 'Phòng 1', totalStudent: 1 }];
  public shiftExam: ShiftInfo[] = [];
  dataSource = new MatTableDataSource<ShiftInfo>([]);
  today = new Date();
  public startDate = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), this.today.getHours(), this.today.getMinutes(), 0);
  public startExamTime = new Date();
  public endExamTime = new Date(this.startExamTime);
  public shift = 1;
  public supervisiorShiftEdit: SupervisorShiftExamEdit;
  public supervisorShiftExam: SupervisorOrganize[] = [];
  routeSub: Subscription;
  examId;
  subjectId;
  gradeId;
  organizeId;
  isValidStudent = true;
  isValidShiftDate = true;
  listOldId: number[] = [];
  updateOrganizeExamRequest: UpdateOrganizeExamRequest;

  isFullScreen = true;
  constructor(public dialog: MatDialog, private router: Router, private route: ActivatedRoute, private _messageService: MessageService, private spinner: NgxSpinnerService) {
    this.organizeExamEndpoint = new OrganizeExamEndpoint();
    this.userExamEndpoint = new UserExamEndpoint();
  }
  closePicker() {
    this.picker.cancel();
  }

  ngOnInit(): void {
    this.updateOrganizeExamRequest = { id: this.organizeExamDetail.id, breakTime: this.organizeExamDetail.breakTime, totalShift: this.organizeExamDetail.totalShift, totalRoom: this.organizeExamDetail.totalRoom, startShiftDate: this.organizeExamDetail.startShiftDate }
    this.startDate = this.organizeExamDetail.startShiftDate;
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = params['id'];
      this.gradeId = params['gradeId'];
      this.subjectId = params['subjectId'];
      this.organizeId = params['organizeId'];
    });
    this.isFullScreen = true;
    this.spinner.show("organizeFreeLoading");
    this.organizeExamEndpoint.getListOrganizeExamInfo(this.organizeExamDetail.id)
      .then((res) => {
        this.shiftExam = res;
        this.dataSource.data = this.shiftExam;
        if (this.shiftExam.length > 0) {
          this.maxStudents = this.shiftExam[0].maximumStudent;
        }
        if (res.length > 0) {
          let lastShift = this.shiftExam.reduce((a, b) => {
            return new Date(a.startDate) > new Date(b.startDate) ? a : b;
          })
          this.startExamTime = new Date(new Date(lastShift.startDate).getTime() + this.updateOrganizeExamRequest.breakTime * 60000 + this.organizeExamDetail.testTime * 60000);
          this.endExamTime = new Date(this.startExamTime.getTime() + this.organizeExamDetail.testTime * 60000);
        }
        this.shift = [...new Set(this.shiftExam.map(item => item.shiftName))].length + 1;
        this.updateOrganizeExamRequest.totalShift = this.shift - 1;
        this.checkValidStudent(-1);
        this.checkValidShiftDate();
      })
      .catch((err) => {
        console.log(err);
        this._messageService.failureCallback("Có lỗi xảy ra lúc lấy danh sách ca thi");
      });
    this.userExamEndpoint.getSupervisorOfOrganize(this.organizeExamDetail.id)
      .then((res) => {
        this.supervisorShiftExam = res;
      })
      .catch(() => {
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại");
      });
    this.spinner.hide("organizeFreeLoading");
  }
  setDefaultValueShiftInfo() {
    this.isValidStudent = true;
    this.isValidShiftDate = true;
    this.listOldId = this.shiftExam.map(x => x.id);
    this.shiftExam = [];
    this.startExamTime = new Date(this.startDate);
    this.updateOrganizeExamRequest.startShiftDate = new Date(this.startDate);
    this.endExamTime = new Date(this.startExamTime.getTime() + this.organizeExamDetail.testTime * 60000);
    this.shift = 1;
  }
  createShift() {
    if(this.roomStudent.some(x=>x.totalStudent<=0 ||isNaN(x.totalStudent) ) || this.maxStudents<=0 || isNaN(this.maxStudents)||this.updateOrganizeExamRequest.breakTime<0 || isNaN(this.updateOrganizeExamRequest.breakTime) ){
      this._messageService.failureCallback("Thông tin không chính xác vui lòng thử lại");
      return;
    }
    if (this.shiftExam.length > 0) {
      const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
        width: '444px',
        data: { title: 'Tạo mới ca thi', message: 'các thiết lập về giám thị và học sinh của tổ chức thi cũ này sẽ bị xóa.', name: 'Danh sách ca thi cũ' }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result == 'DONGY') {
          this.generateShift()
        }
      });

    } else {
      this.generateShift()
    }
  }
  generateShift() {
    this.isFullScreen = false;
    this.spinner.show("organizeFreeLoading");
    let remainstudent = this.organizeExamDetail.totalStudent;
    this.setDefaultValueShiftInfo();
    do {

      for (let index = 0; index < this.roomStudent.length; index++) {
        if (remainstudent > this.maxStudents) {
          this.shiftExam.push({ id: generateRandomUniqueInteger(1000, 9999), shiftName: 'Ca ' + this.shift, roomName: this.roomStudent[index].name, realStudent: this.maxStudents, maximumStudent: this.maxStudents, startDate: this.startExamTime, endDate: this.endExamTime });
          remainstudent -= this.maxStudents;
        } else {
          this.shiftExam.push({ id: generateRandomUniqueInteger(1000, 9999), shiftName: 'Ca ' + this.shift, roomName: this.roomStudent[index].name, realStudent: remainstudent, maximumStudent: this.maxStudents, startDate: this.startExamTime, endDate: this.endExamTime });
          remainstudent = 0
        }
      }
      // sau khi thêm các phòng thì cộng thêm thời gian ca thi cho ca mới
      this.startExamTime = new Date(this.startExamTime.getTime() + this.updateOrganizeExamRequest.breakTime * 60000 + this.organizeExamDetail.testTime * 60000);
      this.endExamTime = new Date(this.startExamTime.getTime() + this.organizeExamDetail.testTime * 60000);

      this.shift++;
    } while (remainstudent > 0);
    this.organizeExamEndpoint.genarateListOrganizeExamInfo({ organizeExamId: this.organizeExamDetail.id, examId: this.examId, subjectId: this.subjectId, gradeId: this.gradeId, listOrganizeExamInfo: this.shiftExam, listOldId: this.listOldId })
      .then((res) => {
        this.spinner.hide("organizeFreeLoading");
        this._messageService.susccessCallback("Thêm ca thi thành công");
        this.shiftExam = res;
        this.dataSource.data = this.shiftExam;
        this.updateOrganizeExamRequest.totalShift = this.shift;
      }).catch(() => {
        this.spinner.hide("organizeFreeLoading");
        this._messageService.failureCallback("Thêm ca thi không thành công")
      })
    this.checkValidShiftDate();
    console.log(this.shift);
  }
  // thêm ca thi
  addShift() {
    let newShiftExam = [];

    newShiftExam.push({ id: generateRandomUniqueInteger(1000, 9999), shiftName: 'Ca ' + this.shift, roomName: 'Phòng 1', realStudent: 1, maximumStudent: this.maxStudents, startDate: this.startExamTime, endDate: this.endExamTime });
    this.isFullScreen = false;
    this.spinner.show("organizeFreeLoading");
    this.organizeExamEndpoint.addOrganizeExamInfo({ organizeExamId: this.organizeExamDetail.id, examId: this.examId, subjectId: this.subjectId, gradeId: this.gradeId, listOrganizeExamInfo: newShiftExam })
      .then((res) => {
        this.shiftExam = this.shiftExam.concat(res);
        this.dataSource.data = this.shiftExam;
        this.startExamTime = new Date(this.endExamTime.getTime() + this.updateOrganizeExamRequest.breakTime * 60000);
        this.endExamTime = new Date(this.startExamTime.getTime() + this.organizeExamDetail.testTime * 60000);
        this.shift++;
        this.updateOrganizeExamRequest.totalShift = this.shift - 1;
        this.dataSource.data = this.shiftExam;
        this.checkValidStudent(-1);
        this.spinner.hide("organizeFreeLoading");
        this._messageService.susccessCallback("Thêm ca thi thành công");
      }).catch((err) => {
        this.spinner.hide("organizeFreeLoading");
        this._messageService.failureCallback("Thêm ca thi không thành công")
      });
  }
  deleteShift(index: number) {
    let shiftInfo = this.shiftExam[index];
    this.listOldId = this.shiftExam.filter(x => x.shiftName == shiftInfo.shiftName).map(x => x.id);
    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      width: '444px',
      data: { title: 'Xoá ca thi', message: 'các thiết lập về giám thị và học sinh của ca thi này sẽ bị xóa.', name: shiftInfo.shiftName }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
        this.isFullScreen = false;
        this.spinner.show("organizeFreeLoading");
        this.organizeExamEndpoint.deleteOrganizeExamInfo(this.listOldId)
          .then((res) => {
            if (res) {
              this.shiftExam.splice(index, 1);
              this.dataSource.data = this.shiftExam;
              this.checkValidStudent(-1);
              this.shift--;
              this.spinner.hide("organizeFreeLoading");
              this._messageService.susccessCallback("Xóa ca thi thành công");
              if (this.shift > 1) {
                for (let index = 0; index < this.roomStudent.length; index++) {
                  for (let j = 1; j < this.shift; j++) {
                    let position = (j - 1) * this.roomStudent.length;
                    let supervisor = this.supervisorShiftExam.find(x => x.organizeExamInfoId == this.shiftExam[position + index].id);
                    if (supervisor) {
                      supervisor.shiftName = "Ca " + j
                    }
                    this.shiftExam[position + index].shiftName = "Ca " + j
                    this.shiftExam[position + index].action = "EDI";
                  }
                }
                this.organizeExamEndpoint.updateListOrganizeExamInfo(this.shiftExam)
                  .then((res) => {
                    if (res) {
                      this.shiftExam.forEach(element => {
                        element.action = "";
                      });
                    }
                  })
              }
            }
          })
          .catch((ex) => {
            console.warn(ex);
            this.spinner.hide("organizeFreeLoading");
            this._messageService.failureCallback("Xóa ca thi không thành công")
          })
        this.supervisorShiftExam = this.supervisorShiftExam.filter(x => this.listOldId.includes(x.organizeExamInfoId) == false);
      }
    });

  }

  ChangeShift(event) {
    if (event) {
      this.generateShift();
    }
  }


  sumRealStudents() {
    let sum = this.shiftExam.reduce((accumulator, current) => accumulator + current.realStudent, 0);
    return sum;
  }
  checkValidStudent(index: number) {
    if (index != -1) {
      this.shiftExam[index].action = "EDI";
    }
    let sum = this.shiftExam.reduce((accumulator, current) => accumulator + current.realStudent, 0);
    if (sum != this.organizeExamDetail.totalStudent) {
      this.isValidStudent = false;
    } else {
      this.isValidStudent = true;
    }
  }
  openEditSupervisorDialog(index: number) {
    this.supervisiorShiftEdit = { organizeExamId: this.organizeExamDetail.id, shiftInfo: this.shiftExam[index], listSupervisorShiftExam: this.supervisorShiftExam }
    const dialogRef = this.dialog.open(EditSupervisorOrganizeExamComponent, {
      width: '80%',
      maxHeight: '80%',
      data: this.supervisiorShiftEdit
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.listSupervisorId) {
        this.supervisorShiftExam = this.supervisorShiftExam.filter(x => x.organizeExamInfoId != this.shiftExam[index].id);

        let supervisor = result.listSupervisorId.filter(x => x.action != "");
        if (supervisor.length > 0) {
          this.isFullScreen = false;
          this.spinner.show("organizeFreeLoading");
          this.userExamEndpoint.addExamSupervisorOrganize(this.shiftExam[index].id, supervisor).then((res) => {
            if (res) {
              ///sau khi add vào db gán lại list ở local
              this.spinner.hide("organizeFreeLoading");
              this._messageService.susccessCallback("Gán giám thị thành công")
              result.listSupervisorId.forEach(x => {
                if (x.action != 'DEL') {
                  this.supervisorShiftExam.push({ organizeExamInfoId: result.id, shiftName: this.shiftExam[index].shiftName, roomName: this.shiftExam[index].roomName, examSubjectSupervisorId: x.examSubjectSupervisorId, action: "" });
                }
              });
            }
          })
            .catch(() => {
              this.spinner.hide("organizeFreeLoading");
              this._messageService.failureCallback("Có lỗi xảy ra khi gán danh sách giám thị")
            });
        }

      }
    });
  }
  openInfoSupervisorDialog(index: number) {
    const dialogRef = this.dialog.open(InfoSupervisorOrganizeExamComponent, {
      width: '80%',
      maxHeight: '80%',
      data: { organizeExamId: this.organizeExamDetail.id, shiftInfo: this.shiftExam[index] }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  openInfoStudentDialog(index: number) {
    let selectedShiftExam = this.shiftExam[index];
    let skipNumber = 0;
    if (index != 0) {
      this.shiftExam.filter((e, i) => {
        if (i < index) {
          skipNumber += e.realStudent;
        }
      })
    }

    let dataSendStudentDialog = {
      organizeExamId: this.organizeId,
      startDate: selectedShiftExam.startDate,
      endDate: selectedShiftExam.endDate,
      roomName: selectedShiftExam.roomName,
      shiftName: selectedShiftExam.shiftName,
      fromRecord: skipNumber,
      size: selectedShiftExam.realStudent
    }

    const dialogRef = this.dialog.open(InfoStudentOrganizeExamComponent, {
      width: '80%',
      height: 'auto',
      data: dataSendStudentDialog,
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  setUpdateRequest() {
    this.updateOrganizeExamRequest.startShiftDate = this.startDate;
  }
  changeStartDate(event, index) {
    this.shiftExam[index].action = "EDI";
    let newStartDate = new Date(event.value._d);
    this.shiftExam[index].startDate = newStartDate;
    this.shiftExam[index].endDate = new Date(newStartDate.getTime() + this.organizeExamDetail.testTime * 60000);
    this.checkValidShiftDate();
    this.dataSource.data = this.shiftExam;
  }
  checkValidShiftDate() {
    this.isValidShiftDate = !this.shiftExam.some((x, i) => new Date(x.endDate) > new Date(this.organizeExamDetail.endDate))
    if (this.isValidShiftDate == false) {
      return;
    }
    for (let index = 0; index < this.shiftExam.length; index++) {
      this.isValidShiftDate = !this.shiftExam.some((x, i) => Math.abs(new Date(this.shiftExam[index].startDate).getTime() - new Date(x.startDate).getTime()) < (this.organizeExamDetail.testTime + this.updateOrganizeExamRequest.breakTime) * 60000 && i != index);
      if (this.isValidShiftDate == false) {
        return;
      }
    }
    this.isValidShiftDate = true;
    return;
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizeOncentrationExamComponent } from './organize-oncentration-exam.component';

describe('OrganizeOncentrationExamComponent', () => {
  let component: OrganizeOncentrationExamComponent;
  let fixture: ComponentFixture<OrganizeOncentrationExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizeOncentrationExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizeOncentrationExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

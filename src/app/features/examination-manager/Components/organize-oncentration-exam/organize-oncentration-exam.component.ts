
import { AfterViewInit, Component, Input, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import * as moment from 'moment';
import { ExamSubject } from 'sdk/cls-k12-sdk-js/src/services/exam/models/ExamSubject';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { RoomName } from '../../interfaces/roomName.model';
import { EditSupervisorOrganizeExamComponent } from '../edit-supervisor-organize-exam/edit-supervisor-organize-exam.component';
import { InfoStudentOrganizeExamComponent } from '../info-student-organize-exam/info-student-organize-exam.component';
import { InfoSupervisorOrganizeExamComponent } from '../info-supervisor-organize-exam/info-supervisor-organize-exam.component';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { ShiftInfo } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/ShiftInfo';
import { OrganizeExamEndpoint } from 'cls-k12-sdk-js/src';
import { OrganizeExamDetail } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/OrganizeExamDetail';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SupervisorOrganize, SupervisorShiftExamEdit } from 'sdk/cls-k12-sdk-js/src/services/user-exam/models/supervisor-exam';
import { UserExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-exam/endpoints/user-exam-endpoint';
import { Action } from 'rxjs/internal/scheduler/Action';
import { UpdateOrganizeExamRequest } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/request/update-organize-exam-request';
import { MessageService } from 'src/app/shared/services/message.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-organize-oncentration-exam',
  templateUrl: './organize-oncentration-exam.component.html',
  styleUrls: ['./organize-oncentration-exam.component.scss']
})
export class OrganizeOncentrationExamComponent implements OnInit, AfterViewInit {
  organizeExamEndpoint: OrganizeExamEndpoint;
  userExamEndpoint: UserExamEndpoint;
  @Input() organizeExamDetail: OrganizeExamDetail;
  updateOrganizeExamRequest: OrganizeExamDetail;
  isLoading = false;
  @ViewChild('picker') picker: any;
  displayedColumns: string[] = ['stt', 'shiftName', 'roomName', 'realStudent', 'startDate', 'endDate', 'supervision', 'student', 'function'];
  displayedRoomColumns: string[] = ['stt', 'name', 'totalStudents'];
  public date: moment.Moment;
  public minDate: moment.Moment;
  public maxDate: moment.Moment;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 60;
  public totalRooms = 1;
  public isValidStudent = true;
  public isValidShiftDate = true;
  public supervisiorShiftEdit: SupervisorShiftExamEdit;
  public supervisorShiftExam: SupervisorOrganize[] = [];
  routeSub: Subscription;
  examId;
  subjectId;
  gradeId;
  organizeId;
  changedShiftExam: boolean = false;

  public formGroup = new FormGroup({
    date: new FormControl(null, [Validators.required]),
  })
  public roomStudent: RoomName[] = [{ name: 'Phòng 1', totalStudent: 10 }];
  public shiftExam: ShiftInfo[] = [];
  dataSource = new MatTableDataSource<ShiftInfo>([]);
  dataSourceRoom = new MatTableDataSource<RoomName>([]);
  today = new Date();
  listOldId: number[] = [];
  public startDate = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), this.today.getHours(), this.today.getMinutes(), 0);
  public startExamTime = new Date();
  public endExamTime = new Date(this.startExamTime);
  public shift = 1;
  public examInfo: ExamSubject;
  disableGenerate: boolean = false;
  isFullScreen = true;
  constructor(public dialog: MatDialog, private router: Router, private route: ActivatedRoute, private _messageService: MessageService, private spinner: NgxSpinnerService) {
    this.organizeExamEndpoint = new OrganizeExamEndpoint();
    this.userExamEndpoint = new UserExamEndpoint();
  }
  ngAfterViewInit(): void {

  }
  closePicker() {
    this.picker.cancel();
  }
  ngOnInit(): void {
    this.updateOrganizeExamRequest = { ...this.organizeExamDetail };
    this.totalRooms = this.updateOrganizeExamRequest.totalRoom;
    this.startDate = this.organizeExamDetail.startShiftDate;
    this.dataSourceRoom.data = this.roomStudent;
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = params['id'];
      this.gradeId = params['gradeId'];
      this.subjectId = params['subjectId'];
      this.organizeId = params['organizeId'];

    });
    this.isFullScreen = true;
    this.spinner.show("organizeConLoading");
    this.organizeExamEndpoint.getListOrganizeExamInfo(this.organizeExamDetail.id)
      .then((res) => {
        this.shiftExam = res;
        this.dataSource.data = this.shiftExam;
        let rooms = this.shiftExam.filter(x => x.shiftName == this.shiftExam[0].shiftName);
        this.shift = [...new Set(this.shiftExam.map(item => item.shiftName))].length + 1;
        this.updateOrganizeExamRequest.totalShift = this.shift - 1;

        if (res.length > 0) {
          this.roomStudent = [];
          rooms.forEach(room => {
            this.roomStudent.push({ name: room.roomName, totalStudent: room.maximumStudent });
          });
          this.dataSourceRoom.data = this.roomStudent;
          this.updateOrganizeExamRequest.totalRoom = this.roomStudent.length;
          let lastShift = this.shiftExam.reduce((a, b) => {
            return new Date(a.startDate) > new Date(b.startDate) ? a : b;
          })
          this.startExamTime = new Date(new Date(lastShift.startDate).getTime() + this.updateOrganizeExamRequest.breakTime * 60000 + this.organizeExamDetail.testTime * 60000);
          this.endExamTime = new Date(this.startExamTime.getTime() + this.organizeExamDetail.testTime * 60000);
        }
        this.checkValidStudent(-1);
        this.checkStartDate();
        this.checkValidShiftDate();
      })
      .catch(() => {
        this._messageService.failureCallback("Có lỗi xảy ra lúc khi danh sách ca thi");
      });

    // Rim Comment tạm để dev
    this.userExamEndpoint.getSupervisorOfOrganize(this.organizeExamDetail.id)
      .then((res) => {
        this.supervisorShiftExam = res;
      })
      .catch(() => {
        this._messageService.failureCallback("Có lỗi xảy ra khi lấy danh sách giám thị của ca thi.")
      });

    this.spinner.hide("organizeConLoading");
  }
  generateRoom() {
    debugger;
    if(this.totalRooms<=0 || isNaN(this.totalRooms)){
      this._messageService.failureCallback("Thông tin phòng thi không đúng");
      return;
    }
    console.log(event);
    if (this.totalRooms > this.roomStudent.length) {
      let rowAdd = this.totalRooms - this.roomStudent.length;
      for (let index = 0; index < rowAdd; index++) {
        this.roomStudent.push({ name: 'Phòng ' + (this.roomStudent.length + 1), totalStudent: 10 });
      }
    }
    else {
      this.roomStudent.splice(this.totalRooms);
    }
    this.dataSourceRoom.data = this.roomStudent;
    this.updateOrganizeExamRequest.totalRoom = this.roomStudent.length;
  }
  // set giá trị mặc định trước khi tạo ca thi
  setDefaultValueShiftInfo() {
    this.isValidStudent = true;
    this.isValidShiftDate = true;
    this.listOldId = this.shiftExam.map(x => x.id);
    this.shiftExam = [];
    this.startExamTime = new Date(this.startDate);
    this.updateOrganizeExamRequest.startDate = new Date(this.startDate);
    this.endExamTime = new Date(this.startExamTime.getTime() + this.organizeExamDetail.testTime * 60000);
    this.shift = 1;
  }
  createShift() {
    if(this.roomStudent.some(x=>x.totalStudent<=0 ||isNaN(x.totalStudent) )||this.updateOrganizeExamRequest.breakTime<0 || isNaN(this.updateOrganizeExamRequest.breakTime) ){
      this._messageService.failureCallback("Thông tin không chính xác vui lòng thử lại");
      return;
    }
    if (this.shiftExam.length > 0) {
      const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
        width: '444px',
        data: { title: 'Tạo mới ca thi', message: 'các thiết lập về giám thị và học sinh của tổ chức thi cũ này sẽ bị xóa.', name: 'Danh sách ca thi cũ' }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result == 'DONGY') {
          this.generateShift()
        }
      });

    } else {
      this.generateShift()
    }
  }
  // tạo ca thi
  generateShift() {
    this.isFullScreen = false;
    this.spinner.show("organizeConLoading");
    this.changedShiftExam = true;
    let remainstudent = this.organizeExamDetail.totalStudent;
    this.setDefaultValueShiftInfo();
    do {
      for (let index = 0; index < this.roomStudent.length; index++) {
        if (remainstudent > this.roomStudent[index].totalStudent) {
          this.shiftExam.push({ id: generateRandomUniqueInteger(1000, 9999), shiftName: 'Ca ' + this.shift, roomName: this.roomStudent[index].name, realStudent: this.roomStudent[index].totalStudent, maximumStudent: this.roomStudent[index].totalStudent, startDate: this.startExamTime, endDate: this.endExamTime, action: "" });
          remainstudent -= this.roomStudent[index].totalStudent;
        } else {
          this.shiftExam.push({ id: generateRandomUniqueInteger(1000, 9999), shiftName: 'Ca ' + this.shift, roomName: this.roomStudent[index].name, realStudent: remainstudent, maximumStudent: this.roomStudent[index].totalStudent, startDate: this.startExamTime, endDate: this.endExamTime, action: "" });
          remainstudent = 0
        }
      }
      // sau khi thêm các phòng thì cộng thêm thời gian ca thi cho ca mới
      this.startExamTime = new Date(this.startExamTime.getTime() + this.updateOrganizeExamRequest.breakTime * 60000 + this.organizeExamDetail.testTime * 60000);
      this.endExamTime = new Date(this.startExamTime.getTime() + this.organizeExamDetail.testTime * 60000);

      this.shift++;
    } while (remainstudent > 0);
    this.organizeExamEndpoint.genarateListOrganizeExamInfo({ organizeExamId: this.organizeExamDetail.id, examId: this.examId, subjectId: this.subjectId, gradeId: this.gradeId, listOrganizeExamInfo: this.shiftExam, listOldId: this.listOldId })
      .then((res) => {
        this.spinner.hide("organizeConLoading");
        this.shiftExam = res;
        this.dataSource.data = this.shiftExam;
        this.updateOrganizeExamRequest.totalShift = this.shift - 1;
        this._messageService.susccessCallback("Tạo ca thi thành công");
      })
      .catch(() => {
        this.spinner.hide("organizeConLoading");
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
      });
    this.checkValidShiftDate();
    this.supervisorShiftExam = [];
    this.spinner.hide("organizeConLoading");
  }
  // thêm ca thi
  addShift() {
    let newShiftExam = [];
    for (let index = 0; index < this.roomStudent.length; index++) {

      newShiftExam.push({ id: generateRandomUniqueInteger(1000, 9999), shiftName: 'Ca ' + this.shift, roomName: this.roomStudent[index].name, realStudent: 1, maximumStudent: this.roomStudent[index].totalStudent, startDate: this.startExamTime, endDate: this.endExamTime, action: "" });
    }
    this.isFullScreen = false;
    this.spinner.show("organizeConLoading");
    this.organizeExamEndpoint.addOrganizeExamInfo({ organizeExamId: this.organizeExamDetail.id, examId: this.examId, subjectId: this.subjectId, gradeId: this.gradeId, listOrganizeExamInfo: newShiftExam })
      .then((res) => {
        this.spinner.hide("organizeConLoading");
        this._messageService.susccessCallback("Thêm ca thi thành công");
        this.changedShiftExam = true;
        this.shiftExam = this.shiftExam.concat(res);
        this.startExamTime = new Date(this.endExamTime.getTime() + this.updateOrganizeExamRequest.breakTime * 60000);
        this.endExamTime = new Date(this.startExamTime.getTime() + this.organizeExamDetail.testTime * 60000);
        this.shift++;
        this.updateOrganizeExamRequest.totalShift = this.shift - 1;
        this.dataSource.data = this.shiftExam;
        this.checkValidStudent(-1);
      })
      .catch(() => {
        this.spinner.hide("organizeConLoading");
        this._messageService.failureCallback("Thêm ca thi không thành công")

      });
    this.checkValidShiftDate();
    this.spinner.hide("organizeConLoading");
  }
  deleteShift(index: number) {
    let shiftInfo = this.shiftExam[index];
    this.listOldId = this.shiftExam.filter(x => x.shiftName == shiftInfo.shiftName).map(x => x.id);
    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      width: '444px',
      data: { title: 'Xoá ca thi', message: 'các thiết lập về giám thị và học sinh của ca thi này sẽ bị xóa.', name: shiftInfo.shiftName }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
        this.isFullScreen = false;
        this.spinner.show("organizeConLoading");
        this.changedShiftExam = true;
        this.organizeExamEndpoint.deleteOrganizeExamInfo(this.listOldId)
          .then((res) => {
            if (res) {
              this.shiftExam = this.shiftExam.filter(x => x.shiftName != shiftInfo.shiftName);
              this.dataSource.data = this.shiftExam;
              this.shift--;
              this.updateOrganizeExamRequest.totalShift--;
              this.checkValidStudent(-1);
              this.spinner.hide("organizeConLoading");
              this._messageService.susccessCallback("Xóa ca thi thành công");
              // gen lại tên ca thi
              if (this.shift > 1) {
                for (let index = 0; index < this.roomStudent.length; index++) {
                  for (let j = 1; j < this.shift; j++) {
                    let position = (j - 1) * this.roomStudent.length;
                    let supervisor = this.supervisorShiftExam.find(x => x.organizeExamInfoId == this.shiftExam[position + index].id);
                    if (supervisor) {
                      supervisor.shiftName = "Ca " + j
                    }
                    this.shiftExam[position + index].shiftName = "Ca " + j
                    this.shiftExam[position + index].action = "EDI";
                  }
                }
                this.organizeExamEndpoint.updateListOrganizeExamInfo(this.shiftExam)
                  .then((res) => {
                    if (res) {
                      this.shiftExam.forEach(element => {
                        element.action = "";
                      });
                    }
                  })
              }
              this.supervisorShiftExam = this.supervisorShiftExam.filter(x => this.listOldId.includes(x.organizeExamInfoId) == false);
            }
          }).catch(() => { this._messageService.failureCallback("Xóa ca thi không thành công") });
      }
    });

  }
  sumRealStudents() {
    let sum = this.shiftExam.reduce((accumulator, current) => accumulator + current.realStudent, 0);
    return sum;
  }
  checkValidStudent(index: number) {
    if (index != -1) {
      this.shiftExam[index].action = "EDI";
    }
    let sum = this.shiftExam.reduce((accumulator, current) => accumulator + current.realStudent, 0);
    if (sum != this.organizeExamDetail.totalStudent) {
      this.isValidStudent = false;
    } else {
      this.isValidStudent = true;
    }
  }
  setUpdateRequest() {
    this.updateOrganizeExamRequest.startShiftDate = this.startDate;
  }
  openEditSupervisorDialog(index: number) {
    this.supervisiorShiftEdit = { organizeExamId: this.organizeExamDetail.id, shiftInfo: this.shiftExam[index], listSupervisorShiftExam: this.supervisorShiftExam }
    const dialogRef = this.dialog.open(EditSupervisorOrganizeExamComponent, {
      width: '80%',
      maxHeight: '80%',
      data: this.supervisiorShiftEdit
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.listSupervisorId) {
        this.supervisorShiftExam = this.supervisorShiftExam.filter(x => x.organizeExamInfoId != this.shiftExam[index].id);

        let supervisor = result.listSupervisorId.filter(x => x.action != "");
        if (supervisor.length > 0) {
          this.isFullScreen = false;
          this.spinner.show("organizeConLoading");
          this.userExamEndpoint.addExamSupervisorOrganize(this.shiftExam[index].id, supervisor).then((res) => {
            if (res) {
              ///sau khi add vào db gán lại list ở local
              this.spinner.hide("organizeConLoading");
              this._messageService.susccessCallback("Gán giám thị thành công")
              result.listSupervisorId.forEach(x => {
                if (x.action != 'DEL') {
                  this.supervisorShiftExam.push({ organizeExamInfoId: result.id, shiftName: this.shiftExam[index].shiftName, roomName: this.shiftExam[index].roomName, examSubjectSupervisorId: x.examSubjectSupervisorId, action: "" });
                }
              });
            }
          })
            .catch(() => {
              this.spinner.hide("organizeConLoading");
              this._messageService.failureCallback("Gán giám thị không thành công")
            });
        }

      }
    });
  }
  openInfoSupervisorDialog(index: number) {
    const dialogRef = this.dialog.open(InfoSupervisorOrganizeExamComponent, {
      width: '80%',
      maxHeight: '80%',
      data: { organizeExamId: this.organizeExamDetail.id, shiftInfo: this.shiftExam[index] }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  openInfoStudentDialog(index: number) {
    let selectedShiftExam = this.shiftExam[index];
    let skipNumber = 0;
    if (index != 0) {
      this.shiftExam.filter((e, i) => {
        if (i < index) {
          skipNumber += e.realStudent;
        }
      })
    }

    let dataSendStudentDialog = {
      organizeExamId: this.organizeId,
      startDate: selectedShiftExam.startDate,
      endDate: selectedShiftExam.endDate,
      roomName: selectedShiftExam.roomName,
      shiftName: selectedShiftExam.shiftName,
      fromRecord: skipNumber,
      size: selectedShiftExam.realStudent
    }

    const dialogRef = this.dialog.open(InfoStudentOrganizeExamComponent, {
      width: '80%',
      height: 'auto',
      data: dataSendStudentDialog,
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  changeStartDate(event, index) {
    this.shiftExam[index].action = "EDI";
    let newStartDate = new Date(event.value._d);
    this.shiftExam[index].startDate = newStartDate;
    this.shiftExam[index].endDate = new Date(newStartDate.getTime() + this.organizeExamDetail.testTime * 60000);
    this.checkValidShiftDate();
    this.dataSource.data = this.shiftExam;
  }
  checkValidShiftDate() {
    this.isValidShiftDate = !this.shiftExam.some((x, i) => new Date(x.endDate) > new Date(this.organizeExamDetail.endDate))
    if (this.isValidShiftDate == false) {
      return;
    }
    for (let index = 0; index < this.shiftExam.length; index++) {
      this.isValidShiftDate = !this.shiftExam.some((x, i) =>
        this.shiftExam[i].shiftName != this.shiftExam[index].shiftName && x.endDate < this.organizeExamDetail.endDate &&
        Math.abs(new Date(this.shiftExam[index].startDate).getTime() - new Date(x.startDate).getTime()) < (this.organizeExamDetail.testTime + this.updateOrganizeExamRequest.breakTime) * 60000);
      if (this.isValidShiftDate == false) {
        return;
      }
    }
    this.isValidShiftDate = true;
    return;
  }
  checkStartDate() {
    this.disableGenerate = this.shiftExam.some(x => this.today > x.startDate);
  }

  getDate(date: any) {
    return new Date(date);
  }
}

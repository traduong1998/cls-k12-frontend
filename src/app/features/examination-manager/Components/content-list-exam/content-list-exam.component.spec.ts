import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentListExamComponent } from './content-list-exam.component';

describe('ContentListExamComponent', () => {
  let component: ContentListExamComponent;
  let fixture: ComponentFixture<ContentListExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentListExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentListExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

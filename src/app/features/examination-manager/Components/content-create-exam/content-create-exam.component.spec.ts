import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentCreateExamComponent } from './content-create-exam.component';

describe('ContentCreateExamComponent', () => {
  let component: ContentCreateExamComponent;
  let fixture: ComponentFixture<ContentCreateExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentCreateExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentCreateExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

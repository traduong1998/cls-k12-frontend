import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-content-create-exam',
  templateUrl: './content-create-exam.component.html',
  styleUrls: ['./content-create-exam.component.scss']
})
export class ContentCreateExamComponent implements OnInit {
  isLinear = false;
  firstFormGroup: FormGroup = Object.create(null);
  secondFormGroup: FormGroup = Object.create(null);
  isOptional = false;
  isEditable = false;

  currentPage = 1;
  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  nextPage() {
    if(this.currentPage <5){
      this.currentPage = this.currentPage + 1;
    }
  }
  previousPage(){
    if(this.currentPage > 1){
      this.currentPage = this.currentPage - 1;
    }
  }
}

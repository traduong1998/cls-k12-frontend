import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateInfoExamComponent } from './create-info-exam.component';

describe('CreateInfoExamComponent', () => {
  let component: CreateInfoExamComponent;
  let fixture: ComponentFixture<CreateInfoExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateInfoExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateInfoExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Component, ViewChild, AfterViewInit, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { merge, Observable, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { ShiftInfoDTO } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/shift-info-dto';
import { ShiftInfo } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/ShiftInfo';
import { UserExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-exam/endpoints/user-exam-endpoint';
import { StudentExam } from 'sdk/cls-k12-sdk-js/src/services/user-exam/models/student-exam';
import { GetExamSubjectStudentOrganizeRequest } from 'sdk/cls-k12-sdk-js/src/services/user-exam/requests/get-examsubject-student-organize-request';
import { MessageService } from 'src/app/shared/services/message.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-info-student-organize-exam',
  templateUrl: './info-student-organize-exam.component.html',
  styleUrls: ['./info-student-organize-exam.component.scss']
})
export class InfoStudentOrganizeExamComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'name', 'email', 'class', 'dob', 'gender'];
  exampleDatabase: ExampleHttpDatabase | null = null;
  selection;
  data: GithubIssue[] = [];

  resultsLength = 0;
  isLoadingResults = false;
  isRateLimitReached = false;

  examId;
  gradeId;
  subjectId;
  organizeId;
  organizeExamInfoId;

  listStudent: StudentExam[] = [];
  studentExamSubjectFilter: GetExamSubjectStudentOrganizeRequest;
  userExamEndpoint: UserExamEndpoint;

  @ViewChild(MatPaginator) paginator: MatPaginator = Object.create(null);
  @ViewChild(MatSort) sort: MatSort = Object.create(null);
  isFullScreen = true;
  // tslint:disable-next-line - Disables all
  constructor(@Inject(MAT_DIALOG_DATA) public shiftInfo: ShiftInfoDTO, private route: ActivatedRoute, private messageService: MessageService, private spinner: NgxSpinnerService) {
    this.userExamEndpoint = new UserExamEndpoint();
  }
  ngOnInit(): void {

    this.studentExamSubjectFilter = {
      keyword: '',
      organizeExamId: +this.shiftInfo.organizeExamId,
      sortDirection: 'ASC',
      sortField: 'ALP',
      fromRecord: this.shiftInfo.fromRecord,
      size: this.shiftInfo.size
    }

    this.getStudentData();
  }

  ngAfterViewInit(): void {

  }
  getStudentData() {
    this.isFullScreen=false;
    this.spinner.show("organizeStudentLoading");
    if (this.studentExamSubjectFilter.size != 0) {
      this.isLoadingResults = true;
      this.userExamEndpoint.getExamSubjectStudentOrganize(this.studentExamSubjectFilter)
        .then(res => {
          this.isLoadingResults = false;
          this.listStudent = res;

          this.spinner.hide("organizeStudentLoading");
        })
        .catch(() => {
          this.isLoadingResults = false;
          this.spinner.hide("organizeStudentLoading");
          this.messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại!")
        });
    }
  }
  exportExcel() {
    if (this.studentExamSubjectFilter.size != 0) {
      this.isFullScreen=true;
      this.spinner.show("organizeStudentLoading");
      this.userExamEndpoint.exportExcelExamSubjectStudentOrganize(this.shiftInfo, this.studentExamSubjectFilter)
        .then((urlFile) => {
          this.isLoadingResults = false;
          window.open(urlFile);
          this.spinner.hide("organizeStudentLoading");
        })
        .catch(() => {
          this.isLoadingResults = false;
          this.spinner.hide("organizeStudentLoading");
          this.messageService.failureCallback("Có lỗi xảy ra trong quá trình tải file vui lòng thử lại!")
        });
    }
  }
}

export interface GithubApi {
  items: GithubIssue[];
  total_count: number;
}

export interface GithubIssue {
  created_at: string;
  number: string;
  state: string;
  title: string;
}

/** An example database that the data source uses to retrieve data for the table. */
export class ExampleHttpDatabase {
  allComplete: boolean = false;
  // tslint:disable-next-line - Disables all
  constructor(private _httpClient: HttpClient) { }

  getRepoIssues(sort: string, order: string, page: number): Observable<GithubApi> {
    const href = 'https://api.github.com/search/issues';
    const requestUrl =
      `${href}?q=repo:angular/components&sort=${sort}&order=${order}&page=${page + 1}`;

    return this._httpClient.get<GithubApi>(requestUrl);
  }


}
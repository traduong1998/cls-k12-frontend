import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoStudentOrganizeExamComponent } from './info-student-organize-exam.component';

describe('InfoStudentOrganizeExamComponent', () => {
  let component: InfoStudentOrganizeExamComponent;
  let fixture: ComponentFixture<InfoStudentOrganizeExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoStudentOrganizeExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoStudentOrganizeExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentCreateMatrixComponent } from './content-create-matrix.component';

describe('ContentCreateMatrixComponent', () => {
  let component: ContentCreateMatrixComponent;
  let fixture: ComponentFixture<ContentCreateMatrixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentCreateMatrixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentCreateMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

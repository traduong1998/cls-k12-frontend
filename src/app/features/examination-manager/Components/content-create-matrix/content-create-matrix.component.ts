import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { number } from 'ngx-custom-validators/src/app/number/validator';
import { Subscription } from 'rxjs';
import { CurriculumsFilterRequests, GlobalEndpoint, QuestionTest, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { CurriculumEndpoint } from 'sdk/cls-k12-sdk-js/src/services/curriculum/endpoint/curriculum-endpoint';
import { Curriculum } from 'sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum';
import { ChaptersEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/chaptersEndpoint';
import { MatrixEndpoint } from 'sdk/cls-k12-sdk-js/src/services/matrix/endpoints/matrix-endpoint';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { AuthService } from 'src/app/core/services';
import { Chapter } from 'src/app/features/learn/intefaces/chapter';
import { Unit } from 'src/app/features/learn/intefaces/unit';
import { Option } from 'src/app/shared/interface/Option';
import { Action } from 'src/app/shared/modules/question-banks/Interfaces/action';
import { QuestionLevelOption } from 'src/app/shared/modules/question-banks/Interfaces/question-level-option';
import { QuestionTypeOption } from 'src/app/shared/modules/question-banks/Interfaces/question-type-option';
import { Matrix } from '../../interface/Matrix';
import { MatrixDetailVM, MatrixInfo, MatrixVM } from '../../interfaces/matrix-info';
import { QuestionExam } from '../../interfaces/question-exam';

@Component({
  selector: 'app-content-create-matrix',
  templateUrl: './content-create-matrix.component.html',
  styleUrls: ['./content-create-matrix.component.scss']
})

export class ContentCreateMatrixComponent implements OnInit {
  @Input() matrixInfo: MatrixInfo
  currentMatrixData: MatrixVM;
  matrixDetails: MatrixDetailVM[];
  // lấy mã mô  tạo đề
  subjectId: number;
  endpointContentPanel: GlobalEndpoint;
  curriculumEndpoint: CurriculumEndpoint;
  endpointChapter: ChaptersEndpoint;
  endpointMatrix: MatrixEndpoint;
  userIdentity: UserIdentity;
  dataTestQuestion: QuestionTest[] = [];
  levelQuestions: QuestionTypeOption[] = [
    { id: Level.NB, name: "Nhận biết" },
    { id: Level.TH, name: "Thông hiểu" },
    { id: Level.VD, name: "Vận dụng" },
    { id: Level.VDC, name: "Vận dụng cao" }];
  levelGroupQuestions: QuestionTypeOption[] = [
    { id: '', name: "Đa mức độ" }];

  typeQuestions: QuestionLevelOption[] = [
    { id: Type.singlechoice, name: "Một lựa chọn" },
    { id: Type.multichoice, name: "Nhiều lựa chọn" },
    { id: Type.underline, name: "Gạch chân" },
    { id: Type.fillblank, name: "Điền khuyết" },
    { id: Type.fillblank2, name: "Điền khuyết loại 2" },
    { id: Type.matching, name: "Ghép đôi" },
    { id: Type.truefasle, name: "Đúng sai" },
    { id: Type.truefaslechause, name: "Mệnh đề đúng sai" },
    { id: Type.essay, name: "Tự luận" }];
  typeGroupQuestions: QuestionLevelOption[] = [
    { id: '', name: "Đa loại câu" }];

  curriculumsBySubjectGrade: Curriculum[];

  displayedColumns: string[] = ['stt', 'contentPanelId', 'chapterId', 'unitId', 'formatQuestion', 'typeOfQuestion', 'level', 'totalQuestion', 'totalScore', 'delete'];
  constructor(private _authService: AuthService) {
    this.userIdentity = _authService.getTokenInfo();
    this.curriculumEndpoint = new CurriculumEndpoint();
    this.endpointContentPanel = new GlobalEndpoint();
    this.endpointChapter = new ChaptersEndpoint();
    this.endpointMatrix = new MatrixEndpoint();
  }
  contentPanels: Curriculum[] = [];
  chapters: Chapter[] = [];
  units: Unit[] = [];

  ngOnInit(): void {
    //console.log(this.matrixInfo);

    this.currentMatrixData = {
      gradeId: this.matrixInfo.gradeId,
      gradeName: this.matrixInfo.gradeName,
      name: '',
      subjectId: this.matrixInfo.subjectId,
      subjectName: this.matrixInfo.subjectName,
      numberTest: 1,
      matrixDetails: []
    }

    if (this.matrixInfo.matrixId != null) {
      // Tối ưu hơn thì chỉ lấy danh sách theo khung, chương, bài có chọn trong từng cấu hình
      this.curriculumEndpoint.getCurriculumsBySubjectGrade(this.matrixInfo.subjectId, this.matrixInfo.gradeId)
        .then(res => {
          this.curriculumsBySubjectGrade = res;
          // lấy thông tin của ma trận hiện tại
          this.endpointMatrix.getMatrix(this.matrixInfo.matrixId)
            .then(res => {
              this.currentMatrixData.id = this.matrixInfo.matrixId;
              this.currentMatrixData.name = res.name;
              this.currentMatrixData.numberTest = res.numberTest;
              this.currentMatrixData.matrixDetails = [];

              res.matrixDetails.map(md => {
                // lấy object contentPanel, chapter, unit đang được chọn
                this.contentPanels = [];
                this.chapters = [];
                this.units = [];
                let contentPanelSelected = '';
                let chapterSelected = '';
                let unitSelected = '';
                let formatSelected = md.formatQuestion == "SIG" ? "Câu đơn" : "Câu chùm";
                let typeSelected = '';
                let levelSelected = '';
                let levelsMD = this.levelGroupQuestions;
                let typesMD = this.typeGroupQuestions;
                if (md.formatQuestion == 'SIG') {
                  typeSelected = this.typeQuestions.find(x => x.id == md.typeOfQuestion).name;
                  levelSelected = this.levelQuestions.find(x => x.id == md.level).name;
                  levelsMD = this.levelQuestions;
                  typesMD = this.typeQuestions;
                }

                if (md.contentPanelId != null) {
                  let contentPanel = this.curriculumsBySubjectGrade.find(c => c.curriculumId == md.contentPanelId);
                  if (contentPanel != undefined) {
                    this.contentPanels.push(contentPanel);
                    contentPanelSelected = contentPanel.curriculumName;
                  }
                }
                if (md.chapterId != null) {
                  let chapter = this.curriculumsBySubjectGrade.find(c => c.curriculumId == md.chapterId);
                  if (chapter != undefined) {
                    this.chapters.push({
                      id: chapter.curriculumId,
                      curriculumName: chapter.curriculumName
                    });
                    chapterSelected = chapter.curriculumName;
                  }
                }
                if (md.unitId != null) {
                  let unit = this.curriculumsBySubjectGrade.find(c => c.curriculumId == md.unitId);
                  if (unit != undefined) {
                    this.units.push({
                      id: unit.curriculumId,
                      curriculumName: unit.curriculumName
                    });
                    unitSelected = unit.curriculumName;
                  }
                }
                this.currentMatrixData.matrixDetails.push({
                  id: md.id,
                  matrixId: md.matrixId,
                  contentPanelId: md.contentPanelId,
                  chapterId: md.chapterId,
                  unitId: md.unitId,
                  formatQuestion: md.formatQuestion,
                  typeOfQuestion: md.typeOfQuestion,
                  level: md.level,
                  totalQuestion: md.totalQuestion,
                  totalScore: md.totalScore,
                  action: '',
                  contentPanels: this.contentPanels,
                  chapters: this.chapters,
                  units: this.units,
                  levels: levelsMD,
                  types: typesMD,
                  contentPanelLoaded: false,
                  chapterLoaded: false,
                  unitLoaded: false,
                  contentPanelSelected: contentPanelSelected,
                  chapterSelected: chapterSelected,
                  unitSelected: unitSelected,
                  typeQuestionSelected: typeSelected,
                  formatQuestionSelected: formatSelected,
                  levelSelected: levelSelected,
                });
              })

              //this.loadData(this.currentMatrixData.matrixDetails);
            })
            .catch()
        })
        .catch();
    }

  }

  newRow() {
    // matrix detail mới lấy Id theo số âm để xử lý cho khỏi trùng
    let nextId = -1;
    if (this.currentMatrixData.matrixDetails.length > 0) {
      // nếu last matrix detail đã tồn tại trong db thì sẽ có id > 0 => đổi sang số âm, để sinh ra nextId
      let lastId = this.currentMatrixData.matrixDetails[this.currentMatrixData.matrixDetails.length - 1].id;
      if (lastId > 0)
        lastId = -lastId;
      nextId = lastId - 1;
    }
    this.currentMatrixData.matrixDetails.push({
      id: nextId,
      matrixId: null,
      contentPanelId: 0,
      chapterId: 0,
      unitId: 0,
      formatQuestion: 'SIG',
      typeOfQuestion: 'SIG',
      level: 'NB',
      totalQuestion: 1,
      totalScore: 1,
      action: Action.Add,
      chapters: [],
      contentPanels: [],
      units: [],
      levels: this.levelQuestions,
      types: this.typeQuestions,
      typeQuestionSelected: "Một lựa chọn",
      formatQuestionSelected: "Câu đơn",
      levelSelected: "Nhận biết"
    });
    this.loadData(this.currentMatrixData.matrixDetails)
  }

  loadData(data) {
    this.currentMatrixData.matrixDetails = [];
    this.currentMatrixData.matrixDetails = [...this.currentMatrixData.matrixDetails, ...data];
  }

  deleteRow(item: MatrixDetailVM) {
    var index = this.currentMatrixData.matrixDetails.findIndex(x => x.id == item.id);

    if (item.action == Action.Add) {
      this.currentMatrixData.matrixDetails.splice(index, 1);
    } else {
      item.action = Action.Delete;
    }

    this.loadData(this.currentMatrixData.matrixDetails);
  }

  onChangeContentPanelSelected(value, id) {
    let matrixDetail = this.currentMatrixData.matrixDetails.find(x => x.id == id);
    matrixDetail.contentPanelId = +value;
    matrixDetail.contentPanelSelected = matrixDetail.contentPanels.find(x => x.curriculumId == value).curriculumName;
    // reset selected chương
    matrixDetail.chapterLoaded = false;
    matrixDetail.chapters = [];
    // reset selected bài giảng
    matrixDetail.unitLoaded = false;
    matrixDetail.units = [];
    if (matrixDetail.action != Action.Add) {
      matrixDetail.action = Action.EditAll;
    }
  }

  onChangeChapterSelected(value, id) {
    let matrixDetail = this.currentMatrixData.matrixDetails.find(x => x.id == id);
    matrixDetail.chapterId = +value;
    matrixDetail.chapterSelected = matrixDetail.chapters.find(x => x.id == value).curriculumName;
    // reset selected bài giảng
    matrixDetail.unitLoaded = false;
    matrixDetail.units = [];
    if (matrixDetail.action != Action.Add) {
      matrixDetail.action = Action.EditAll;
    }
  }

  onChangeUnitSelected(value, id) {
    let matrixDetail = this.currentMatrixData.matrixDetails.find(x => x.id == id);
    matrixDetail.unitId = +value;
    matrixDetail.unitSelected = matrixDetail.units.find(x => x.id == value).curriculumName;
    if (matrixDetail.action != Action.Add) {
      matrixDetail.action = Action.EditAll;
    }
  }

  onChangeQuestionTypeSelected(value, id) {
    let matrixDetail = this.currentMatrixData.matrixDetails.find(x => x.id == id);
    matrixDetail.typeOfQuestion = value;
    if (matrixDetail.action != Action.Add) {
      matrixDetail.action = Action.EditAll;
    }
  }
  onChangeQuestionLevelSelected(value, id) {
    let matrixDetail = this.currentMatrixData.matrixDetails.find(x => x.id == id);
    matrixDetail.level = value;
    if (matrixDetail.action != Action.Add) {
      matrixDetail.action = Action.EditAll;
    }
  }

  onChangeQuestionFormatSelected(value, id) {
    let matrixDetail = this.currentMatrixData.matrixDetails.find(x => x.id == id);
    matrixDetail.formatQuestion = value;
    if (matrixDetail.action != Action.Add) {
      matrixDetail.action = Action.EditAll;
    }
    if (value == 'SIG') {
      //reset level and type
      matrixDetail.levels = this.levelQuestions;
      matrixDetail.level = Level.NB;
      matrixDetail.types = this.typeQuestions;
      matrixDetail.typeOfQuestion = Type.singlechoice;

    } else if (value == 'GRO') {
      // Đối với câu chùm thì level mặc định là Đa mức độ, loại câu mặc định: Đa loại câu
      //reset level and type
      matrixDetail.levels = this.levelGroupQuestions;
      matrixDetail.level = '';
      matrixDetail.types = this.typeGroupQuestions;
      matrixDetail.typeOfQuestion = '';
    }
  }

  /* Get contentPanel */
  onContentPanelSelectClicked(id) {
    let matrixDetail = this.currentMatrixData.matrixDetails.find(x => x.id == id);

    if (matrixDetail.contentPanelLoaded) {
      return;
    }

    let value = new CurriculumsFilterRequests();
    value.gradeId = this.matrixInfo.gradeId;
    value.subjectId = this.matrixInfo.subjectId;
    value.isActive = true;
    value.pageNumber = 1;
    value.isActive = true;
    // Lấy hết khung chương trình
    value.sizeNumber = 10000;

    this.curriculumEndpoint
      .getComboboxCurriculumCreateUpdate(this.userIdentity.divisionId, this.userIdentity.schoolId, this.matrixInfo.gradeId, this.matrixInfo.subjectId)
      .then(res => {
        matrixDetail.contentPanels = res;
        matrixDetail.contentPanelLoaded = true;
      })
      .catch(err => {
        // s  this.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      })
  }

  /* Get Chapter */
  onChapterSelectClicked(id) {
    let matrixDetail = this.currentMatrixData.matrixDetails.find(x => x.id == id);
    if (matrixDetail.chapterLoaded || matrixDetail.contentPanelId == 0) {
      return;
    }
    this.endpointContentPanel
      .getCurriculumChildrentNodesByPortalId(this.userIdentity.portalId, matrixDetail.contentPanelId)
      .then((res) => {
        matrixDetail.chapters = res;
        matrixDetail.chapterLoaded = true;
      })
      .catch(err => {
        // s  this.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
  }

  /* Get Unit */
  onUnitSelectClicked(id) {
    let matrixDetail = this.currentMatrixData.matrixDetails.find(x => x.id == id);
    if (matrixDetail.unitLoaded || matrixDetail.chapterId == 0) {
      return;
    }

    this.endpointContentPanel.getCurriculumChildrentNodesByPortalId(this.userIdentity.portalId ,matrixDetail.chapterId)
      .then(res => {
        matrixDetail.units = res;
      })
      .catch(err => {
        // s  this.openSnarBar(" Có lỗi xảy ra vui lòng thử lại ", 'red-snackbar');
      });
  }
  disableDeleteRow() {
    return this.currentMatrixData.matrixDetails.filter(x => x.action != Action.Delete).length == 1;
  }

  preventNegativeNumber(element) {
    if (element.totalScore < 1) {
      element.totalScore = 1;
    }
    if (element.totalScore > 3) {
      element.totalScore = 3;
    }
  }

  changeTotalScoreMatrixDetail(element) {
    if (element.action != Action.Add) {
      element.action = Action.EditAll;
    }
    if (element.totalScore <= 0) {
      element.totalScore = 1;
    }
  }
  changeTotalQuestionMatrixDetail(element) {
    if (element.action != Action.Add) {
      element.action = Action.EditAll;
    }
    if (element.totalQuestion < 1) {
      element.totalQuestion = 1;
    }
  }
  changeNumberTest() {
    debugger
    if (this.currentMatrixData.numberTest > 3) {
      this.currentMatrixData.numberTest = 3;
    }
  }

  getActiveMatrixDetails() {
    return this.currentMatrixData.matrixDetails.filter(x => x.action != Action.Delete);
  }
}

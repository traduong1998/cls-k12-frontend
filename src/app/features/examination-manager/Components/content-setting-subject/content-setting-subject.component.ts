import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-content-setting-subject',
  templateUrl: './content-setting-subject.component.html',
  styleUrls: ['./content-setting-subject.component.scss']
})
export class ContentSettingSubjectComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  cancel(){
    this.router.navigate(["dashboard/examination-manager/create"]);
  }
}

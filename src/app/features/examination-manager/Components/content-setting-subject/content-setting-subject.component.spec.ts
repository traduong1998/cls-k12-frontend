import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentSettingSubjectComponent } from './content-setting-subject.component';

describe('ContentSettingSubjectComponent', () => {
  let component: ContentSettingSubjectComponent;
  let fixture: ComponentFixture<ContentSettingSubjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentSettingSubjectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentSettingSubjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

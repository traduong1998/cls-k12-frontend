import { Component, AfterViewInit, OnInit, Output, ViewChild, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject, Subject, Subscription } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { GroupStudentEndpoint, UserIdentity } from 'cls-k12-sdk-js/src';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { UserExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-exam/endpoints/user-exam-endpoint';
import { Action } from 'sdk/cls-k12-sdk-js/src/services/user-exam/enum/action';
import { StudentExam } from 'sdk/cls-k12-sdk-js/src/services/user-exam/models/student-exam';
import { StudentExamRequest } from 'sdk/cls-k12-sdk-js/src/services/user-exam/requests/student-exam-request';
import { UserAction } from 'sdk/cls-k12-sdk-js/src/services/user-exam/requests/user-action-request';
import { AuthService } from 'src/app/core/services';
import { MessageService } from 'src/app/shared/services/message.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';

@Component({
  selector: 'app-add-student-exam',
  templateUrl: './add-student-exam.component.html',
  styleUrls: ['./add-student-exam.component.scss']
})
export class AddStudentExamComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Output() valid;
  @Input() status: string;
  displayedColumns: string[] = ['stt', 'name', 'email', 'dob', 'class', 'gender', 'select'];
  listStudent: StudentExam[] = [];
  userAction: UserAction[] = [];
  userExamEndpoint: UserExamEndpoint;
  endpointGroupStudent: GroupStudentEndpoint;
  isLoadingResults = false;
  allComplete: boolean = false;

  isFirstLoadGroupStudents = true;

  examId;
  gradeId;
  subjectId;
  organizeId;
  totalItems;

  groupStudentFilter = {
    schoolId: null,
    gradeId: null
  }
  userIdentity: UserIdentity;

  public keyWordCtrl: FormControl = new FormControl();

  protected groupStudents: GroupStudentOption[];
  public groupStudentCtrl: FormControl = new FormControl();
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;

  protected _onDestroy = new Subject<void>();
  studentPagingFilterClone: StudentExamRequest;
  studentPagingFilter: StudentExamRequest;
  routeSub: Subscription;
  isFullScreen = true;
  constructor(private router: Router, private route: ActivatedRoute, private _authService: AuthService, private _messageService: MessageService, private spinner: NgxSpinnerService) {
    this.userExamEndpoint = new UserExamEndpoint();
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: '' });
    this.userIdentity = _authService.getTokenInfo();
  }
  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = params['id'];
      this.gradeId = params['gradeId'];
      this.subjectId = params['subjectId'];
      this.organizeId = params['organizeId'];

    });
    this.studentPagingFilter = {
      gradeId: this.gradeId,
      groupStudentId: 0,
      keyword: "",
      examId: this.examId,
      subjectId: this.subjectId,
      organizeExamId: this.organizeId,
      pageNumber: 1,
      pageSize: 10
    }

    this.getStudentData();

    // listen for search field value changes
    this.groupStudentFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudents();
      });
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.studentPagingFilter.pageNumber = this.paginator.pageIndex + 1;
      this.studentPagingFilter.pageSize = this.paginator.pageSize;
      this.getStudentData();
    });
    this.setInitialValue();
  }

  protected setInitialValue() {

    this.filteredGroupStudents
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGroupStudent.compareWith = (a: GroupStudentOption, b: GroupStudentOption) => a && b && a.id === b.id;
      });
  }

  ngOnDestroy(): void {
  }

  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onGroupStudentSelectClicked() {
    var schoolIdSelected = this.userIdentity.schoolId;;
    var gradeIdSelected = this.gradeId;
    if (this.isFirstLoadGroupStudents || this.groupStudentFilter.gradeId != gradeIdSelected) {
      this.endpointGroupStudent.getGroupStudentOptions(schoolIdSelected, gradeIdSelected)
        .then(res => {
          this.isFirstLoadGroupStudents = false;
          this.groupStudents = res;
          this.groupStudentFilter.gradeId = gradeIdSelected;
          this.groupStudentFilter.schoolId = schoolIdSelected;
          this.filteredGroupStudents.next(this.groupStudents.slice());
        })
        .catch(

        )
    } else {

    }
  }

  resetGroupStudentSelectCtrl() {
    this.groupStudents = [];
    this.isFirstLoadGroupStudents = true;
    this.groupStudentCtrl.setValue(undefined);
  }

  getStudentData() {
    this.isLoadingResults = true;
    this.isFullScreen = false;
    this.spinner.show("studentLoading");
    this.userExamEndpoint.getStudentExam(this.studentPagingFilter)
      .then(res => {
        this.isLoadingResults = false;
        this.listStudent = res.items;
        this.studentPagingFilterClone = { ...this.studentPagingFilter };
        this.listStudent.map(student => {
          if (student.isChoose == true) {
            var action = this.userAction.find(x => x.id == student.id);
            if (action) {
              if (action.action == 'DEL') {
                student.isChoose = false;
              }
            } else {
              this.userAction.push({ id: student.id, action: '' })
            }

          } else {
            // trường hợp chọn xong=>qua trang=>back lại
            // check xem có trong list đã chọn thì tick lại
            if (this.userAction.find(x => x.id == student.id && x.action == Action.Add) != null) {
              student.isChoose = true;
            }
          }
          return student;
        });
        this.allComplete = this.isCheckAll();
        this.totalItems = res.totalItems;
        this.spinner.hide("studentLoading");
      })
      .catch(() => {
        this.spinner.hide("studentLoading");
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
      });
  }
  getFilterParam() {
    this.studentPagingFilter.groupStudentId = this.groupStudentCtrl.value;
    this.studentPagingFilter.keyword = this.keyWordCtrl.value;
  }
  onSubmitFilter() {
    this.getFilterParam();
    if (!compareTwoObject(this.studentPagingFilter, this.studentPagingFilterClone)) {
      this.getStudentData();
    }
  }

  someComplete(): boolean {
    return this.someChecked(this.allComplete);
  }
  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.checkAll(completed);
    // if (!completed) {
    //   this.listStudent.forEach(student => {
    //     if (student.action == '' && student.isChoose == true) {
    //       student.action = Action.Delete;
    //     } else {
    //       student.action = "";
    //     }
    //   });
    // } else {
    //   this.listStudent.forEach(student => {
    //     if (student.action == Action.Delete && student.isChoose == true) {
    //       student.action = '';
    //     } else if (student.isChoose == false) {
    //       student.action = Action.Add;
    //     }
    //   });
    // }

  }
  checkAll(completed: boolean) {
    this.listStudent.map(x => x.isChoose = completed);
    this.listStudent.forEach(sch => {
      if (completed) {
        let action = this.userAction.find(x => x.id == sch.id);
        if (action) {
          if (action.action == Action.Delete) {
            action.action = "";
          }
        } else {
          this.userAction.push({ id: sch.id, action: Action.Add })
        }
      } else {
        let action = this.userAction.find(x => x.id == sch.id);
        if (action) {
          if (action.action == Action.Add) {
            this.userAction.splice(this.userAction.indexOf(action), 1)
          } else if (action.action == "") {
            action.action = Action.Delete
          }
        }
      }
    });
  }
  someChecked(allComplete: boolean): boolean {
    return this.listStudent.filter(x => x.isChoose).length > 0 && !allComplete;
  }
  updateAllComplete(event, id) {
    this.allComplete = this.isCheckAll();

    let student = this.userAction.find(x => x.id == id);
    //xử lý action khi check hoặc uncheck
    if (event) {
      if (student) {
        if (student.action == Action.Delete) {
          student.action = ""
        }
      } else {
        this.userAction.push({ id: id, action: Action.Add });
      }
    } else {
      if (student) {
        if (student.action == Action.Add) {
          this.userAction.splice(this.userAction.indexOf(student), 1);

        } else if (student.action == "") {
          student.action = Action.Delete
        }
      }
    }
  }
  isCheckAll(): boolean {
    return this.listStudent.every(x => x.isChoose);
  }
}


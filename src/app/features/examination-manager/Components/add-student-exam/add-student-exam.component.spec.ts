import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddStudentExamComponent } from './add-student-exam.component';

describe('AddStudentExamComponent', () => {
  let component: AddStudentExamComponent;
  let fixture: ComponentFixture<AddStudentExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddStudentExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddStudentExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

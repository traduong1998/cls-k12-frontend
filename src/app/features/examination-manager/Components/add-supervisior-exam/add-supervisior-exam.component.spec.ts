import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSupervisiorExamComponent } from './add-supervisior-exam.component';

describe('AddSupervisiorExamComponent', () => {
  let component: AddSupervisiorExamComponent;
  let fixture: ComponentFixture<AddSupervisiorExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSupervisiorExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSupervisiorExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

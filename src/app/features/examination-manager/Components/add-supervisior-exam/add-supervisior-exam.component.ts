import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DivisionEndpoint, DivisionOption, UserIdentity } from 'cls-k12-sdk-js/src';
import { TeacherExam } from 'sdk/cls-k12-sdk-js/src/services/exam/models/TeacherExam';
import { UserExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-exam/endpoints/user-exam-endpoint';
import { Action } from 'sdk/cls-k12-sdk-js/src/services/user-exam/enum/action';
import { TeacherExamPagingRequest } from 'sdk/cls-k12-sdk-js/src/services/user-exam/requests/teacher-exam-request';
import { UserAction } from 'sdk/cls-k12-sdk-js/src/services/user-exam/requests/user-action-request';
import { AuthService } from 'src/app/core/services';
import { MessageService } from 'src/app/shared/services/message.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';

@Component({
  selector: 'app-add-supervisior-exam',
  templateUrl: './add-supervisior-exam.component.html',
  styleUrls: ['./add-supervisior-exam.component.scss']
})
export class AddSupervisiorExamComponent implements OnInit {
  @Input() status: string;
  displayedColumns: string[] = ['stt', 'name', 'email', 'dob', 'gender', 'select'];
  baseApiUrl = 'http://localhost:65000';
  isLoadingResults = false;
  protected divisions: DivisionOption[];
  userIdentity: UserIdentity;
  levelManageValue: number = 0;
  endpointDivision: DivisionEndpoint;
  public divisionFilterCtrl: FormControl = new FormControl();
  public divisionCtrl: FormControl = new FormControl();
  public txtKeyword: FormControl = new FormControl('');
  userExamEndpoint: UserExamEndpoint;
  supervisorExamSubject: TeacherExam[] = [];
  supervisorAction: UserAction[] = [];

  allComplete: boolean = false;
  examId;
  gradeId;
  subjectId;
  organizeId;
  totalItems;
  supervisorPagingFilterClone: TeacherExamPagingRequest;
  supervisorPagingFilter: TeacherExamPagingRequest;
  routeSub: Subscription;
  @ViewChild(MatPaginator) paginator: MatPaginator = Object.create(null);
  @ViewChild(MatSort) sort: MatSort = Object.create(null);
  isFullScreen = true;
  constructor(private _authService: AuthService, private router: Router, private route: ActivatedRoute, private _messageService: MessageService, private spinner: NgxSpinnerService) {
    this.userExamEndpoint = new UserExamEndpoint();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();
    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    }
  }
  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.supervisorPagingFilter.pageNumber = this.paginator.pageIndex + 1;
      this.supervisorPagingFilter.pageSize = this.paginator.pageSize;
      this.getSupervisorData();
    });
  }
  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = params['id'];
      this.gradeId = params['gradeId'];
      this.subjectId = params['subjectId'];
      this.organizeId = params['organizeId'];
    });
    this.supervisorPagingFilter = {
      divisionId: 0,
      schoolId: 0,
      keyword: "",
      examId: this.examId,
      subjectId: this.subjectId,
      organizeExamId: this.organizeId,
      pageNumber: 1,
      pageSize: 10
    }
    this.getSupervisorData();

  }


  ngOnDestroy(): void {
  }

  getSupervisorData() {
    this.isFullScreen = false;
    this.spinner.show("supervisorLoading");
    this.userExamEndpoint.getSupervisorExam(this.supervisorPagingFilter)
      .then(res => {
        this.isLoadingResults = false;
        this.supervisorPagingFilterClone={...this.supervisorPagingFilter};
        this.supervisorExamSubject = res.items;
        this.supervisorExamSubject.map(supervisor => {
          if (supervisor.isChoose == true) {
            var action = this.supervisorAction.find(x => x.id == supervisor.id);
            if (action) {
              if (action.action == 'DEL') {
                supervisor.isChoose = false;
              }
            } else {
              this.supervisorAction.push({ id: supervisor.id, action: '' })
            }
          } else {
            // trường hợp chọn xong=>qua trang=>back lại
            // check xem có trong list đã chọn thì tick lại
            if (this.supervisorAction.find(x => x.id == supervisor.id && x.action == Action.Add) != null) {
              supervisor.isChoose = true;
            }
          }

          return supervisor;
        });
        this.allComplete = this.isCheckAll();
        this.totalItems = res.totalItems;
        this.spinner.hide("supervisorLoading");
      })
      .catch(() => {
        this.spinner.hide("supervisorLoading");
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
      });
  }
  getFilterParam() {

    this.supervisorPagingFilter.divisionId = this.divisionCtrl.value;

    this.supervisorPagingFilter.keyword = this.txtKeyword.value;
    this.supervisorPagingFilter.pageNumber = 1;
    this.supervisorPagingFilter.pageSize = 10;
  }
  onSubmitFilter() {
    this.getFilterParam();
    if (!compareTwoObject(this.supervisorPagingFilter, this.supervisorPagingFilterClone)) {
      this.getSupervisorData();
    }
  }

  someComplete(): boolean {
    return this.someChecked(this.allComplete);
  }
  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.checkAll(completed);
  }
  checkAll(completed: boolean) {
    this.supervisorExamSubject.map(x => x.isChoose = completed);
    this.supervisorExamSubject.forEach(sch => {
      if (completed) {
        let action = this.supervisorAction.find(x => x.id == sch.id);
        if (action) {
          if (action.action == Action.Delete) {
            action.action = "";
          }
        } else {
          this.supervisorAction.push({ id: sch.id, action: Action.Add })
        }
      } else {
        let action = this.supervisorAction.find(x => x.id == sch.id);
        if (action) {
          if (action.action == Action.Add) {
            this.supervisorAction.splice(this.supervisorAction.indexOf(action), 1)
          } else if (action.action == "") {
            action.action = Action.Delete
          }
        }
      }
    });
  }
  someChecked(allComplete: boolean): boolean {
    return this.supervisorExamSubject.filter(x => x.isChoose).length > 0 && !allComplete;
  }
  updateAllComplete(event, id) {
    this.allComplete = this.isCheckAll();
    let supervisor = this.supervisorAction.find(x => x.id == id);
    //xử lý action khi check hoặc uncheck
    if (event) {
      if (supervisor) {
        if (supervisor.action == Action.Delete) {
          supervisor.action = ""
        }
      } else {
        this.supervisorAction.push({ id: id, action: Action.Add });
      }
    } else {
      if (supervisor) {
        if (supervisor.action == Action.Add) {
          this.supervisorAction.splice(this.supervisorAction.indexOf(supervisor), 1);

        } else if (supervisor.action == "") {
          supervisor.action = Action.Delete
        }
      }
    }
  }
  isCheckAll(): boolean {
    return this.supervisorExamSubject.every(x => x.isChoose);
  }

}
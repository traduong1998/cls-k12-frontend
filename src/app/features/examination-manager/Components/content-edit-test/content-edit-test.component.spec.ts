import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentEditTestComponent } from './content-edit-test.component';

describe('ContentEditTestComponent', () => {
  let component: ContentEditTestComponent;
  let fixture: ComponentFixture<ContentEditTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentEditTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentEditTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

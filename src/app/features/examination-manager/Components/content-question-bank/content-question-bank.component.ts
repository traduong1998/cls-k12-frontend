import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Option } from 'src/app/shared/interface/Option';

@Component({
  selector: 'app-content-question-bank',
  templateUrl: './content-question-bank.component.html',
  styleUrls: ['./content-question-bank.component.scss']
})
export class ContentQuestionBankComponent implements OnInit {
  routeSub: Subscription;
  numberTest: number = 0;

  subjectId: number;
  grades: Option[] = [
    // { Id: 1, Value: 'Khối 1' }, { Id: 2, Value: 'Khối 2' }, { Id: 3, Value: 'Khối 3' },
  ];

  test: Option[] = [];

  constructor(private router: Router, private route: ActivatedRoute, private zone: NgZone) { }

  ngOnInit(): void {
    // lấy mã môn
    this.routeSub = this.route.params.subscribe(params => {
      this.subjectId = params['id'];
    });
  }
  cancel() {
    this.router.navigate(["dashboard/examination-manager/list-testkit/", this.subjectId]);
  }
  inputNumberTest(numberTest) {
    this.zone.run(() => {
      for (let i = 0; i < numberTest; i++) {
        this.test.push({ id: i, name: `Đề số ` + (i + 1) });
      }
    });
  }
}

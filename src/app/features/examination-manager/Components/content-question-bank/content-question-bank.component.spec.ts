import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentQuestionBankComponent } from './content-question-bank.component';

describe('ContentQuestionBankComponent', () => {
  let component: ContentQuestionBankComponent;
  let fixture: ComponentFixture<ContentQuestionBankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentQuestionBankComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentQuestionBankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

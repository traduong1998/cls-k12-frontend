import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoTeacherFinishDialogComponent } from './info-teacher-finish-dialog.component';

describe('InfoTeacherFinishDialogComponent', () => {
  let component: InfoTeacherFinishDialogComponent;
  let fixture: ComponentFixture<InfoTeacherFinishDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoTeacherFinishDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoTeacherFinishDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

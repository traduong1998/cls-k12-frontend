import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DivisionEndpoint, DivisionOption, UserIdentity } from 'cls-k12-sdk-js/src';
import { TeacherExam } from 'sdk/cls-k12-sdk-js/src/services/exam/models/TeacherExam';
import { UserExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-exam/endpoints/user-exam-endpoint';
import { TeacherExamPagingRequest } from 'sdk/cls-k12-sdk-js/src/services/user-exam/requests/teacher-exam-request';
import { AuthService } from 'src/app/core/services';
import { MessageService } from 'src/app/shared/services/message.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-info-teacher-finish-dialog',
  templateUrl: './info-teacher-finish-dialog.component.html',
  styleUrls: ['./info-teacher-finish-dialog.component.scss']
})
export class InfoTeacherFinishDialogComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'name', 'email', 'dob', 'gender'];
  baseApiUrl = 'http://localhost:65000';
  isLoadingResults = false;
  protected divisions: DivisionOption[];
  userIdentity: UserIdentity;
  levelManageValue: number = 0;
  endpointDivision: DivisionEndpoint;
  public divisionFilterCtrl: FormControl = new FormControl();
  public divisionCtrl: FormControl = new FormControl();
  public txtKeyword: FormControl = new FormControl('');
  userExamEndpoint: UserExamEndpoint;
  teacherExamSubject: TeacherExam[] = [];
  allComplete: boolean = false;
  examId;
  gradeId;
  subjectId;
  totalItems;
  teacherPagingFilter: TeacherExamPagingRequest;
  @ViewChild(MatPaginator) paginator: MatPaginator = Object.create(null);
  @ViewChild(MatSort) sort: MatSort = Object.create(null);

  isFullScreen = true;
  constructor(private _authService: AuthService, private _messageService: MessageService, @Inject(MAT_DIALOG_DATA) public organizeId: number, private spinner: NgxSpinnerService) {
    this.userExamEndpoint = new UserExamEndpoint();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();
    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    }
  }

  ngOnInit(): void {

    this.teacherPagingFilter = {

      divisionId: null,
      schoolId: null,
      keyword: "",
      examId: this.examId,
      subjectId: this.subjectId,
      organizeExamId: this.organizeId,
      pageNumber: 1,
      pageSize: 10
    }
    this.getTeacherData();
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.teacherPagingFilter.pageNumber = this.paginator.pageIndex + 1;
      this.teacherPagingFilter.pageSize = this.paginator.pageSize;
      this.getTeacherData();
    });
  }

  ngOnDestroy(): void {
  }
  onSubmitFilter() {
    this.getFilterParam();
    this.getTeacherData();
  }
  getTeacherData() {
    this.isLoadingResults = true;
    this.isFullScreen = false;
    this.spinner.show("finishTeacherLoading");
    this.userExamEndpoint.getTeacherExamFinish(this.teacherPagingFilter)
      .then(res => {
        this.isLoadingResults = false;
        this.teacherExamSubject = res.items;
        this.totalItems = res.totalItems;
        this.spinner.hide("finishTeacherLoading");
      })
      .catch(() => {
        this.spinner.hide("finishTeacherLoading");
        this.isLoadingResults = false;
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
      });
  }
  getFilterParam() {

    this.teacherPagingFilter.divisionId = this.divisionCtrl.value;

    this.teacherPagingFilter.keyword = this.txtKeyword.value;
    this.teacherPagingFilter.pageNumber = 1;
    this.teacherPagingFilter.pageSize = 10;
  }
  exportExcel() {
    this.isFullScreen = true;
    this.spinner.show("finishTeacherLoading");
    this.isLoadingResults = true;
    this.userExamEndpoint.exportExcelTeacherExamFinish(this.teacherPagingFilter)
      .then((urlFile) => {
        this.isLoadingResults = false;
        this.spinner.hide("finishTeacherLoading");
        window.open(urlFile);
      })
      .catch(() => {
        this.isLoadingResults = false;
        this.spinner.hide("finishTeacherLoading");
        this._messageService.failureCallback("Có lỗi xảy ra trong quá trình tải file vui lòng thử lại!")
      });
  }
}
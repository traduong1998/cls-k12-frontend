import { ExamSubject } from 'sdk/cls-k12-sdk-js/src/services/exam/models/ExamSubject';
import { ExamSubjectEndPoint } from 'sdk/cls-k12-sdk-js/src/services/exam/endpoints/ExamSubjectEndPoint';
import { Component, OnInit, OnChanges } from '@angular/core';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-organize-exam-info',
  templateUrl: './organize-exam-info.component.html',
  styleUrls: ['./organize-exam-info.component.scss']
})
export class OrganizeExamInfoComponent implements OnInit {
  examSubjectEndPoint: ExamSubjectEndPoint;
  examSubjectInfo: ExamSubject;
  isReady: boolean = false;
  isFullScreen = true;
  constructor( private _messageService: MessageService,private spinner: NgxSpinnerService) {
    this.examSubjectEndPoint = new ExamSubjectEndPoint();
  }
  ngOnInit(): void {
    this.isFullScreen = true;

    this.examSubjectEndPoint.getExamSubject(1)
      .then((res) => {
        debugger;
        this.spinner.hide();
        console.log(``);
        this.examSubjectInfo = { id: res.id, endDate: new Date(res.endDate), startDate: new Date(res.startDate), status: res.status, type: res.type, testTime: res.testTime, subjectName: res.subjectName };
      })
      .then(() => {
        debugger;
        this.spinner.hide();
        this.isReady = true;
      }).catch(() => {
        debugger;
        this.spinner.hide();
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
      })
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentListExamSubjectComponent } from './content-list-examsubject.component';

describe('ContentListExamsubjectComponent', () => {
  let component: ContentListExamSubjectComponent;
  let fixture: ComponentFixture<ContentListExamSubjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentListExamSubjectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentListExamSubjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

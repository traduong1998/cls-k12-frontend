
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CLSModules, CLSPermissions, ExamDetail, ExamEnpoint, OrganizeExamEndpoint, SubjectEndpoint } from 'cls-k12-sdk-js/src';
import { ExamSubjectOrganize } from 'cls-k12-sdk-js/src/services/organize-exam/models/ExamSubjectOrganize';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { MatDialog } from '@angular/material/dialog';
import { MessageService } from 'src/app/shared/services/message.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from 'src/app/core/services';
@Component({
  selector: 'app-content-list-examsubject',
  templateUrl: './content-list-examsubject.component.html',
  styleUrls: ['./content-list-examsubject.component.scss']
})
export class ContentListExamSubjectComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'subject', 'startDate', 'endDate', 'testTime', 'status', 'function'];
  data: ExamSubjectOrganize[] = [];
  organizeExamEndpoint: OrganizeExamEndpoint;
  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;
  examId;
  gradeId;
  routeSub: Subscription;
  isFirstLoadAllGrades: boolean = true;
  allSubject: SubjectOption[] = [];
  examInfo: ExamDetail;
  today = new Date();
  private endpointSubject: SubjectEndpoint;
  private examEndpoint: ExamEnpoint;
  isFullScreen = true;
  hasApprovePermission: any;
  // tslint:disable-next-line - Disables all
  constructor(private _httpClient: HttpClient, private _authService: AuthService, private router: Router, private route: ActivatedRoute, public dialog: MatDialog, private _messageService: MessageService, private spinner: NgxSpinnerService) {
    var userIdentity = this._authService.getTokenInfo();
    this.organizeExamEndpoint = new OrganizeExamEndpoint();
    this.endpointSubject = new SubjectEndpoint();
    this.examEndpoint = new ExamEnpoint();
    this.hasApprovePermission = userIdentity.hasPermission(CLSModules.OrganizeExam, CLSPermissions.Add);
  }
  ngOnInit(): void {

    console.log('Load danh sách môn thi')
    this.isLoadingResults = true;
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = params['id'];
      this.gradeId = params['gradeId'];
    });
    this.isFullScreen = false;
    this.spinner.show();
    this.examEndpoint.getInfoExam(this.examId)
      .then(res => {
        this.examInfo = res;
      })
    this.organizeExamEndpoint.getListSubjectOrganize(this.examId).then((res) => {
      this.isLoadingResults = false;
      this.data = res;
      this.organizeExamEndpoint.getListExamSubjectOrganize(this.examId)
        .then((res) => {
          if (res.length > 0) {
            res.forEach(element => {
              this.data.forEach(data => {
                if (data.subjectId == element.subjectId) {
                  data.id = element.id;
                  data.status = element.status;
                }
              })
            });
            console.log('data', this.data)
          }
        })
      if (this.isFirstLoadAllGrades) {
        this.endpointSubject.getSubjectOptions(null)
          .then(res => {
            this.isFirstLoadAllGrades = false;
            this.allSubject = res;

            this.data = this.data.map((exam) => {
              let subject = this.allSubject.find(x => x.id == exam.subjectId);
              if (subject != undefined) {
                exam.subjectName = subject.name;
              }

              return exam;
            });
          })
          .catch();
      }
      else {
        this.data = this.data.map((exam) => {
          let subject = this.allSubject.find(x => x.id == exam.subjectId);
          if (subject != undefined) {
            exam.subjectName = subject.name;
          }
          return exam;
        });
      }
      this.spinner.hide();
    })
      .catch(() => {
        this.spinner.hide();
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
      });
  }

  ngAfterViewInit(): void {

  }
  organize(id, subjectId, examType, startDate, endDate) {
    if (id != null) {
      this.router.navigate([this.router.url, subjectId, 'assignment', id]);
      return;
    }
    this.organizeExamEndpoint.createOrganizeExam({ subjectId: subjectId, gradeId: this.gradeId, examId: this.examId, examType: examType, startDate: startDate, endDate: endDate })
      .then(res => {
        this.router.navigate([this.router.url, subjectId, 'assignment', res]);
      })
      .catch(() => {
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
      });
  }
  view(id, subjectId) {
    this.router.navigate(['dashboard/examination-manager/organize/', this.examId, 'grade', this.gradeId, 'examsubject', subjectId, 'view', id])
  }
  deleteOrganize(id, subjectName, index) {
    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      width: '444px',
      data: { title: 'Xóa tổ chức thi', message: 'các thiết lập về giám thị và học sinh của tổ chức thi này sẽ bị xóa.', name: subjectName }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
        this.isFullScreen = true;
        this.spinner.show();
        this.organizeExamEndpoint.deleteOrganizeExam(id)
          .then(res => {
            this._messageService.susccessCallback('Xóa thành công');
            this.data[index].id = null;
            this.data[index].status = 'NOT';
            this.spinner.hide();
          })
          .catch(() => {
            this.spinner.hide();
            this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
          });
      }
    })
  }
  getDate(date: any) {
    return new Date(date);
  }
  previousPage() {
    this.router.navigate(['dashboard/examination-manager/organize']);
  }
}


import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderCreateExamComponent } from './header-create-exam.component';

describe('HeaderCreateExamComponent', () => {
  let component: HeaderCreateExamComponent;
  let fixture: ComponentFixture<HeaderCreateExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderCreateExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderCreateExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

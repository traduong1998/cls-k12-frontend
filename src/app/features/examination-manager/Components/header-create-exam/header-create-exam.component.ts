import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StepHeader, StepType } from 'src/app/features/learn/intefaces/stepHeader';
import { StepHeaderControlExam } from 'src/app/features/learn/intefaces/stepHeaderControl';
@Component({
  selector: 'app-header-create-exam',
  templateUrl: './header-create-exam.component.html',
  styleUrls: ['./header-create-exam.component.scss']
})
export class HeaderCreateExamComponent implements OnInit {
  @Input() stepHeaderCtrl: StepHeaderControlExam;
  steps: StepHeader[] = [];
  isReady: boolean = false;
  public get stepTypeEnum(): typeof StepType {
    return StepType;
  }
  constructor(private _router: Router, private route: ActivatedRoute) {
    //khởi tạo giá trị ban đầu cho các step
    this.steps = [
      { step: 1, label: "THÔNG TIN", type: StepType.basic, currentStep: false, url: "", iconBasic: "info", iconActive: "info", iconDisabled: "info-baned" },
      { step: 2, label: "CHỌN BỘ ĐỀ THI", type: StepType.basic, currentStep: false, url: "", iconBasic: "add", iconActive: "add-complete", iconDisabled: "" },
      { step: 3, label: "THIẾT LẬP", type: StepType.basic, currentStep: false, url: "", iconBasic: "setting", iconActive: "setting-completed", iconDisabled: "" },
      { step: 4, label: "HOÀN THÀNH", type: StepType.basic, currentStep: false, url: "", iconBasic: "finish", iconActive: "finish-completed", iconDisabled: "" }
    ];
  }
  ngOnChanges(changes: SimpleChanges): void {
    //Page hiện tại thì gán currentPage = true
    this.steps[this.stepHeaderCtrl.currentPage - 1].currentStep = true;
    //Set url cho các step
    this.setUrlAllStep();
    // Kiểm tra giá trị step truyền vào, đang ở bước nào thì active tất cả các bước trước nó
    this.steps.forEach(x => {
      if (x.step <= (this.stepHeaderCtrl.step)) {
        x.type = StepType.active
      } else {
        x.type = StepType.basic
      }
      //Nếu là đang tạo nội dung thì disable bước thông tin
      if (this.stepHeaderCtrl.isCreatingTest) {
        this.steps[0].type = StepType.disabled;
      };
    });
  }

  ngOnInit(): void {
    this.isReady = true;
  }

  setUrlAllStep(): void {
    this.steps[0].url = "/dashboard/examination-manager/exams/";
    this.steps[1].url = "/dashboard/examination-manager/exams/";
    this.steps[2].url = "/dashboard/examination-manager/exams";
    this.steps[3].url = "/dashboard/examination-manager/exams/";
  }

  //Hàm router ccho các button step
  onClickStep(item: StepHeader): void {
    console.log("item", this.stepHeaderCtrl)
    if (item.step == 1) {
      if (item.type == StepType.active) {
        this._router.navigate([item.url + `${this.stepHeaderCtrl.examId}`]);
      }
    }

    else if (item.step == 2) {
      if (item.type == StepType.active) {
        this._router.navigate([item.url + `${this.stepHeaderCtrl.examId}` + `/steps/choose-test`]);
      }
    } else if (item.step == 3) {
      if (item.type == StepType.active) {
        this._router.navigate([item.url + `${this.stepHeaderCtrl.examId}` + `/steps/setting`]);
      }

    } else if (item.step == 4) {
      if (item.type == StepType.active) {
        this._router.navigate([item.url + `${this.stepHeaderCtrl.examId}` + `/steps/finish`]);
      }
    }
  }

}

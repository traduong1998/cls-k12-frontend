import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.scss']
})
export class DialogConfirmComponent implements OnInit {
  //@Output() isConfirm = new EventEmitter();
  constructor(public dialogRef: MatDialogRef<DialogConfirmComponent>, @Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit(): void {
    if (this.data.labelYes == undefined) {
      this.data.labelYes = "Xóa"
    }
    if (this.data.labelNo == undefined) {
      this.data.labelNo = "Hủy"
    }
    if (this.data.type == undefined) {
      this.data.type = "delete"
    }
  }
  cancel() {
    this.dialogRef.close(false);
  }

  confirm() {
    this.dialogRef.close(true);
    //this.isConfirm.emit(true);
  }
}

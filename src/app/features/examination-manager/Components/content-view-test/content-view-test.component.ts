
import { QuestionTest } from './../../interface/question-test';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Answer } from 'sdk/cls-k12-sdk-js/src/services/question/models/Answer';
import { Question } from 'sdk/cls-k12-sdk-js/src/services/question/models/Question';
import { QuestionFileService } from 'src/app/core/services/question-file/question-file.service';
import { DialogStatisticQuestionComponent } from '../dialog-statistic-question/dialog-statistic-question.component'
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Test } from 'sdk/cls-k12-sdk-js/src/services/test/models/test';
import { QuestionModel } from 'sdk/cls-k12-sdk-js/src/services/test/requests/question-model';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { TestKit } from 'sdk/cls-k12-sdk-js/src/services/testkit/models/testkit';
import { TestKitEnpoint } from 'sdk/cls-k12-sdk-js/src/services/testkit/enpoints/testkit-enpoint';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ToastComponent } from '../toast/toast.component';
import { TestEnpoint } from 'sdk/cls-k12-sdk-js/src/services/test/enpoint/test-enpoint';
import { ExamEnpoint, GradeEndpoint, SubjectEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { GradeInfo } from 'sdk/cls-k12-sdk-js/src/services/grade/requests/grade-info-request';
import { SubjectInfo } from 'sdk/cls-k12-sdk-js/src/services/subject/models/subject-info-request';
import { SubjectTestkitRequest } from 'sdk/cls-k12-sdk-js/src/services/exam/requests/update-subject-testkit-request';
import { QuestionExam } from '../../interfaces/question-exam';
import { UpdateScoreQuetionsDialogComponent } from '../update-score-quetions-dialog/update-score-quetions-dialog.component';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { DialogConfirm } from '../../interfaces/dialog-confirm';
import { DialogConfirmComponent } from '../dialog-confirm/dialog-confirm.component';

@Component({
  selector: 'app-content-view-test',
  templateUrl: './content-view-test.component.html',
  styleUrls: ['./content-view-test.component.scss']
})
export class ContentViewTestComponent implements OnInit {
  //fake
  routeSub: Subscription;
  subjectId;
  examId;
  testKitId;
  subjectExamId;
  gradeId: number;
  dialogConfirm: DialogConfirm;

  private testEnpoint: TestEnpoint;
  private testKitEnpoint: TestKitEnpoint;
  private gradeEnpoit: GradeEndpoint;
  private subjectEndpoint: SubjectEndpoint;
  private examApi: ExamEnpoint;

  constructor(private _snackBar: MatSnackBar, private fb: FormBuilder, public dialog: MatDialog, private questionFilleService: QuestionFileService, private router: Router, private route: ActivatedRoute, private changeDetectorRefs: ChangeDetectorRef) {
    this.gradeEnpoit = new GradeEndpoint({ baseUrl: 'http://localhost:65000' });
    this.subjectEndpoint = new SubjectEndpoint({ baseUrl: 'http://localhost:65000' });
    this.testEnpoint = new TestEnpoint();
    this.testKitEnpoint = new TestKitEnpoint();
    this.examApi = new ExamEnpoint();
  }
  //dữ liệu truyền đi
  question: QuestionModel = null;

  totalScore = 1;
  //data questionModel
  dataQuestionModel: QuestionModel[] = [];
  questions: QuestionModel[] = [];
  allComplete: boolean = false;
  //index câu hỏi đã chọn
  index;
  grade?: GradeInfo = {
    gradeId: 0,
    gradeName: '',
    orderNumber: 0
  };
  subject?: SubjectInfo = {
    id: 0,
    name: '',
    orderNumber: 0
  };

  //danh sách câu hỏi đc thêm từ file
  data: QuestionExam[] = [];

  //hiên thị
  displayedColumns: string[] = ['stt', 'content', 'score', 'delete']
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  //bộ đề
  testKit: TestKit;
  //danh sách đề
  dataTest: Test[] = [];
  isPublished: boolean = true;

  testForm = this.fb.group({
    testName: new FormControl('', [Validators.required, Validators.maxLength(255)])
  })

  get name() { return this.testForm.get('testName'); }

  ngOnInit(): void {

    this.getQuestionData();
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = + params['examId']

      this.gradeId = +params['gradeId'];

      this.subjectId = +params['subjectId'];

      this.subjectExamId = +params['examSubject'];

    });
    this.examApi.getExamStatus(this.examId)
      .then(res => {
        this.isPublished = res == 'PUB';
        if(this.isPublished){
          this.router.navigate(["dashboard/examination-manager/list-testkit/", this.examId, this.gradeId, this.subjectId, this.subjectExamId]);
        }
      })
      .catch();
    //lấy thông tin khối
    this.gradeEnpoit.getGradeInfo(this.gradeId).then(res => {
      if (res) {
        this.grade = res;
      }
    })
    //lây thông tin môn
    this.subjectEndpoint.getSubjectInfo(this.subjectId).then(res => {
      if (res) {
        this.subject = res;
      }
    })
    console.log(this.question);
  }

  submitForm() {
    if (this.testForm.invalid) {
      //sai
    }
    else {
      this.testKit = { id: null, gradeId: this.gradeId, subjectId: this.subjectId, createType: 'FIL', matrixId: null, name: this.name.value, numberTest: 3 }
      this.testKitEnpoint.createTestkit(this.testKit).then(res => {
        if (res) {
          this.testKitId = res.id;
          this.createTest();
          // this._snackBar.openFromComponent(ToastComponent, {
          //   data: ['Thêm bộ đề thành công'],
          //   duration: 1000,
          //   horizontalPosition: 'center',
          //   verticalPosition: 'top',
          // });
        } else {
          this._snackBar.openFromComponent(ToastComponent, {
            data: ['Thêm thất bại'],
            duration: 1000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });
        }
      })
        .then(() => {
          let subjectTestkit: SubjectTestkitRequest = { id: this.subjectExamId, testKitId: this.testKitId }
          this.examApi.updateExamSubjectTestKit(this.subjectExamId, subjectTestkit)
            .then(() => {

              this.router.navigate(["dashboard/examination-manager/exams/" + this.examId + "/setting-subject/" + this.subjectId + "/" + this.subjectExamId]);

              // this._snackBar.openFromComponent(ToastComponent, {
              //   data: ['Thêm đề cho môn thành công'],
              //   duration: 2000,
              //   horizontalPosition: 'center',
              //   verticalPosition: 'top',
              // });
            }).catch(err => {
              this._snackBar.openFromComponent(ToastComponent, {
                data: ['Thêm đề môn thất bại'],
                duration: 2000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });
            });
        })
        .catch(err => {
          console.log(err);
        })
    }
  }

  createTest(): void {
    this.createQuestionRequest(this.data);

    this.dataTest.push({ name: "đề số 1", gradeId: this.gradeId, subjectId: this.subjectId, testKitId: this.testKitId, questions: this.dataQuestionModel })
    this.dataTest.forEach(object => {
      this.testEnpoint.createTest(object).then(res => {
        if (res) {
          console.log("okok")
        }
        else {
          console.log("fail")
        }
      })
    });

  }

  //lấy danh sách câu hỏi từ file
  getQuestionData() {
    this.questionFilleService.getQuestionData().subscribe(res => {
      this.data = res;
    });
  }

  // Khởi tạo những câu hỏi đc thêm vào
  createQuestionRequest(data) {
    data.forEach(obj => {
      if (obj.questions != null && obj.format == FormatQuestion.group) {
        obj.questions.forEach(obj => {
          this.questions.push(
            {
              score: obj.score,
              content: obj.content,
              type: obj.type,
              level: obj.level,
              format: FormatQuestion.group,
              contentPanelId: 0,
              chapterId: 0,
              unitId: 0,
              questions: [],
              answers: obj.answers
            });
        });

        this.dataQuestionModel.push(
          {
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: this.questions,
            answers: []
          });
      }
      else {
        this.dataQuestionModel.push(
          {
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: [],
            answers: obj.answers
          });
      }
    })

    //this.loadData(this.dataQuestionModel);
  }

  loadData(data) {
    this.data = [];
    this.data = [...this.data, ...data];

    this.changeDetectorRefs.detectChanges();
  }

  showStatisticQuestion(data): void {
    console.log(`data:99: `, data);
    let dataQuestions: QuestionModel[] = [];
    data.forEach(obj => {
      if (obj.questions != null && obj.format == FormatQuestion.group) {
        let questions: QuestionModel[] = [];
        obj.questions.forEach(obj => {
          questions.push(
            {
              score: obj.score,
              content: obj.content,
              type: obj.type,
              level: obj.level,
              format: FormatQuestion.group,
              contentPanelId: 0,
              chapterId: 0,
              unitId: 0,
              questions: [],
              answers: obj.answers
            });
        });

        dataQuestions.push(
          {
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: this.questions,
            answers: []
          });
      }
      else {
        dataQuestions.push(
          {
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: [],
            answers: obj.answers
          });
      }
    })

    const dialogRef = this.dialog.open(DialogStatisticQuestionComponent, {
      width: '800px',
      height: 'auto',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  delete(element) {
    this.dialogConfirm = { title: 'XÓA CÂU HỎI', description: 'Bạn có chắc chắn muốn xóa câu hỏi này?' }
    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let index = this.data.indexOf(element);
        if (index != -1) {
          this.data.splice(index, 1);
          this.loadData(this.data);
        }
      }
    });
  }

  chooseQuestion(item, i) {
    this.index = i;
    this.question = item;
    console.log("item", item)
  }

  updateQuestion(question) {
    debugger
    this.data[this.index] = question;
    this.loadData(this.data);
  }

  updateScore(i, score) {
    this.dataQuestionModel[i].score = Number.parseInt(score)
  }

  save() {
    //lưu danh sách đề thi
    this.submitForm();
    //router sang bên setting môn thi
    //this.router.navigate(["dashboard/examination-manager/setting-subject/", this.subjectId, this.examId]);
  }
  cancel() {
    //router về danh sách đề thi
    this.router.navigate(["dashboard/examination-manager/list-testkit/", this.examId, this.gradeId, this.subjectId, this.subjectExamId]);
  }

  deleteSelected() {
    this.dialogConfirm = { title: 'XÓA CÂU HỎI', description: 'Bạn có chắc chắn muốn xóa các câu hỏi đã chọn?' }
    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.data = this.data.filter(x => !x.isChecked);
      }
    });

  }

  someComplete() {
    if (this.data == undefined) return false;

    let somchecked = false;
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].isChecked) {
        somchecked = true;
        break;
      }
      if (this.data[i].questions != null) {
        for (let j = 0; j < this.data[i].questions.length; j++) {
          if (this.data[i].questions[j].isChecked) {
            somchecked = true;
            break;
          }
        }
      }
    }
    return somchecked;
  }

  updateAllComplete() {
    if (this.data == undefined) return false;
    let allChecked = true;
    for (let i = 0; i < this.data.length; i++) {
      if (!this.data[i].isChecked) {
        allChecked = false;
        break;
      }
      if (this.data[i].questions != null) {
        for (let j = 0; j < this.data[i].questions.length; j++) {
          if (!this.data[i].questions[j].isChecked) {
            allChecked = false;
            break;
          }
        }
      }
    }
    this.allComplete = allChecked;
  }
  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.data.map((question) => {
      if (question.format == FormatQuestion.group) {
        question.isChecked = completed;
        question.questions.forEach((child) => {
          child.isChecked = completed
        });
      } else {
        question.isChecked = completed
      }
    });
  }
  openUpdateScoreDialog() {
    console.log(`this.questions: `, this.data);
    if (!this.someComplete()) {
      alert("Không có câu hỏi nào được chọn!");
      return;
    }
    const dialogRef = this.dialog.open(UpdateScoreQuetionsDialogComponent, {
      width: '377px',
      height: '234px',
      data: this.totalScore
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      if (result) {
        this.updateScoreQuestions(result);
      }
    });
  }

  updateScoreQuestions(score: number): void {
    this.data.map((question) => {
      if (question.isChecked) {
        question.score = score;
        if (question.format == FormatQuestion.group) {
          question.questions.forEach((child) => {
            child.score = score;
          });
        }
      }
    });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentViewTestComponent } from './content-view-test.component';

describe('ContentViewTestComponent', () => {
  let component: ContentViewTestComponent;
  let fixture: ComponentFixture<ContentViewTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentViewTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentViewTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

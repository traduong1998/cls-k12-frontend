import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { QuestionBankEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/errorResponse';
import { MatrixEndpoint } from 'sdk/cls-k12-sdk-js/src/services/matrix/endpoints/matrix-endpoint';
import { Matrix, MatrixDetail } from 'sdk/cls-k12-sdk-js/src/services/matrix/models/matrix';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { Action } from 'src/app/shared/modules/question-banks/Interfaces/action';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { MatrixInfo, MatrixVM } from '../../interfaces/matrix-info';
import { ContentCreateMatrixComponent } from '../content-create-matrix/content-create-matrix.component';

@Component({
  selector: 'app-add-matrix-testkit-dialog',
  templateUrl: './add-matrix-testkit-dialog.component.html',
  styleUrls: ['./add-matrix-testkit-dialog.component.scss']
})
export class AddMatrixTestkitDialogComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  @ViewChild(ContentCreateMatrixComponent) contentCreateMatrixComponent;
  errors = [];
  isValid: boolean = true;
  endpointMatrix: MatrixEndpoint;
  questionBankEnpoint: QuestionBankEndpoint;
  // component thêm ma trận của 1 bộ đề thi
  constructor(@Inject(MAT_DIALOG_DATA) public matrix: MatrixInfo, private mdDialogRef: MatDialogRef<AddMatrixTestkitDialogComponent>,
    private _snackBar: MatSnackBar) {
    this.endpointMatrix = new MatrixEndpoint();
    this.questionBankEnpoint = new QuestionBankEndpoint();
  }

  ngOnInit(): void {

  }

  validData(currentMatrixData: MatrixVM) {
    this.isValid = true;
    this.errors = [];
    if (isNullOrEmpty(currentMatrixData.name) || isNullOrWhiteSpace(currentMatrixData.name)) {
      this.isValid = false;
      this.errors.push("Bạn cần nhập tên ma trận");
    }
    if (currentMatrixData.matrixDetails.filter(x => x.action != Action.Delete).length == 0) {
      this.isValid = false;
      this.errors.push("Bạn cần tạo ít nhất 1 chi tiết cấu hình");
    }
    // kiểm tra trùng cấu hình
    currentMatrixData.matrixDetails.map((x, i) => {
      if (x.totalQuestion < 1 || x.totalScore <= 0) {
        x.validCode = 2;
        this.isValid = false;
        this.errors.push("- Dòng số " + (i + 1) + ": Số lượng và điểm phải lớn hơn 0");
      }
      currentMatrixData.matrixDetails.map((y, j) => {
        if (x.id != y.id && j > i && x.action != Action.Delete && y.action != Action.Delete) {
          if (x.contentPanelId == y.contentPanelId && x.chapterId == y.chapterId && x.unitId == y.unitId) {
            if (x.formatQuestion == y.formatQuestion && x.typeOfQuestion == y.typeOfQuestion && x.level == y.level) {
              y.validCode = 1;
              this.isValid = false;
              this.errors.push("- Dòng số " + (j + 1) + ": Cấu hình bị trùng với dòng số " + (i + 1));
            }
          }
        }
      })
    });

    return this.isValid;
  }
  saveData() {
    let currentMatrixData: MatrixVM = this.contentCreateMatrixComponent.currentMatrixData;
    if (this.validData(currentMatrixData)) {
      let maxtrix: Matrix;
      maxtrix = {
        id: currentMatrixData.id,
        gradeId: currentMatrixData.gradeId,
        subjectId: currentMatrixData.subjectId,
        numberTest: currentMatrixData.numberTest,
        name: currentMatrixData.name,
        matrixDetails: []
      }
      let matrixDetails: MatrixDetail[] = [];

      currentMatrixData.matrixDetails.map(x => {
        maxtrix.matrixDetails.push({
          contentPanelId: x.contentPanelId,
          chapterId: x.chapterId,
          unitId: x.unitId,
          formatQuestion: x.formatQuestion,
          typeOfQuestion: x.typeOfQuestion,
          level: x.level,
          totalQuestion: x.totalQuestion,
          totalScore: x.totalScore,
          id: x.id,
          action: x.action,
          matrixId: maxtrix.id
        })
      });

      if (currentMatrixData.id != null) {
        // update matrix
        this.endpointMatrix.updateMatrix(maxtrix)
          .then(res => {
            //this.mdDialogRef.close(res);
            this.contentCreateMatrixComponent.currentMatrixData.id = res.id;
            this.contentCreateMatrixComponent.currentMatrixData.matrixDetails = this.contentCreateMatrixComponent.currentMatrixData.matrixDetails.filter(x => x.action != Action.Delete);
            this.contentCreateMatrixComponent.currentMatrixData.matrixDetails.map((x, i) => {
              x.id = res.matrixDetails[i].id;
              x.action = "";
              return x;
            })

            return this.getQuestionByMatrix(res);
          })
          .catch((err: ErrorResponse) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: err.errorDetail,
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
      } else {
        // add matrix
        this.endpointMatrix.createMatrix(maxtrix)
          .then(res => {
            this.contentCreateMatrixComponent.currentMatrixData.id = res.id;
            this.contentCreateMatrixComponent.currentMatrixData.matrixDetails.map((x, i) => {
              x.id = res.matrixDetails[i].id;
              x.action = "";
              return x;
            })
            return this.getQuestionByMatrix(res);
          })
          .catch((err: ErrorResponse) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: err.errorDetail,
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
      }
    }
  }

  getQuestionByMatrix(result: Matrix) {
    // Lấy đề theo cấu hình ma trận
    this.questionBankEnpoint.getTestsByMatrix(result)
      .then(
        res => {
          let dataResult = {
            matrixId: result.id,
            data: res
          };
          this.mdDialogRef.close(dataResult);
        }
      )
      .catch((err: ErrorResponse) => {
        if (err.errorCode == "notenough") {
          if (err.errorDetail != "") {
            let msg = err.errorDetail;
            let paths = msg.split("-");
            let id = 0;
            if (paths.length == 2) {
              id = +paths[1];
            }
            let index = result.matrixDetails.findIndex(x => x.id == id);
            let msgShow = paths[0] + ". " + "Cấu hình dòng số " + (index + 1);
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: msgShow,
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        } else {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      });
  }
  cancel() {
    this.mdDialogRef.close('cancel');
  }
}

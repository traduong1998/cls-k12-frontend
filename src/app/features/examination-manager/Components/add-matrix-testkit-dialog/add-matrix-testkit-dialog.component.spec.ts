import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMatrixTestkitDialogComponent } from './add-matrix-testkit-dialog.component';

describe('AddMatrixTestkitDialogComponent', () => {
  let component: AddMatrixTestkitDialogComponent;
  let fixture: ComponentFixture<AddMatrixTestkitDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddMatrixTestkitDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMatrixTestkitDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

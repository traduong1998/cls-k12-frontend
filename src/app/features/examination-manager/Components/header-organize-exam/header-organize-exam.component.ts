import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StepHeader, StepType } from 'src/app/features/learn/intefaces/stepHeader';
import { StepHeaderControlExam, StepHeaderControlOrganize } from 'src/app/features/learn/intefaces/stepHeaderControl';

@Component({
  selector: 'app-header-organize-exam',
  templateUrl: './header-organize-exam.component.html',
  styleUrls: ['./header-organize-exam.component.scss']
})
export class HeaderOrganizeExamComponent implements OnInit {
  @Input() stepHeaderCtrl: StepHeaderControlOrganize;
  steps: StepHeader[] = [];
  isReady: boolean = false;
  public get stepTypeEnum(): typeof StepType {
    return StepType;
  }
  constructor(private _router: Router, private route: ActivatedRoute) {
    //khởi tạo giá trị ban đầu cho các step
    this.steps = [
      { step: 1, label: "PHÂN CÔNG", type: StepType.basic, currentStep: false, url: "", iconBasic: "info", iconActive: "assignment-active", iconDisabled: "info-baned" },
      { step: 2, label: "TỔ CHỨC THI", type: StepType.basic, currentStep: false, url: "", iconBasic: "organize", iconActive: "organize-active", iconDisabled: "" },
      { step: 3, label: "HOÀN THÀNH", type: StepType.basic, currentStep: false, url: "", iconBasic: "finish", iconActive: "finish-completed", iconDisabled: "" }
    ];
  }
  ngOnChanges(changes: SimpleChanges): void {
    //Page hiện tại thì gán currentPage = true
    this.steps[this.stepHeaderCtrl.currentPage - 1].currentStep = true;
    // Kiểm tra giá trị step truyền vào, đang ở bước nào thì active tất cả các bước trước nó
    this.steps.forEach(x => {
      if (x.step <= (this.stepHeaderCtrl.currentPage)) {
        x.type = StepType.active
      } else {
        x.type = StepType.basic
      }
    });
  }

  ngOnInit(): void {
    this.isReady = true;
  }


  //Hàm router ccho các button step
  onClickStep(item: StepHeader): void {
    if(item.type == StepType.active)
      if(item.step == 0){
        this._router.navigate([item.url + `${this.stepHeaderCtrl.examId}`]);
      }
      if(item.step == 1){
        this._router.navigate([item.url + `${this.stepHeaderCtrl.examId}`]);

      }
      if(item.step == 3){
        this._router.navigate([item.url + `${this.stepHeaderCtrl.examId}`]);
      }

  }

}

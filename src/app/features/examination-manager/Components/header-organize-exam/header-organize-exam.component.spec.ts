import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderOrganizeExamComponent } from './header-organize-exam.component';

describe('HeaderOrganizeExamComponent', () => {
  let component: HeaderOrganizeExamComponent;
  let fixture: ComponentFixture<HeaderOrganizeExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderOrganizeExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderOrganizeExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

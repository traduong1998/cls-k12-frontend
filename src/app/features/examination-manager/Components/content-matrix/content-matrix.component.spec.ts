import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentMatrixComponent } from './content-matrix.component';

describe('ContentMatrixComponent', () => {
  let component: ContentMatrixComponent;
  let fixture: ComponentFixture<ContentMatrixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentMatrixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

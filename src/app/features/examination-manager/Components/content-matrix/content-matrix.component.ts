import { A } from '@angular/cdk/keycodes';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Matrix } from '../../interface/Matrix';

@Component({
  selector: 'app-content-matrix',
  templateUrl: './content-matrix.component.html',
  styleUrls: ['./content-matrix.component.scss']
})
export class ContentMatrixComponent implements OnInit {
  
  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  addNewMatrix(){
    this.router.navigate(["dashboard/examination-manager/list-testkit/1/matrix/addnew"])
  }
}

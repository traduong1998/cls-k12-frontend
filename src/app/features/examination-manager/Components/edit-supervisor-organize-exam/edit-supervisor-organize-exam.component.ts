
import { HttpClient } from '@angular/common/http';
import { Component, AfterViewInit, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { SupervisorShiftExamEdit } from 'sdk/cls-k12-sdk-js/src/services/user-exam/models/supervisor-exam';
import { TeacherExam } from 'sdk/cls-k12-sdk-js/src/services/exam/models/TeacherExam';
import { ShiftInfo } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/ShiftInfo';
import { UserExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-exam/endpoints/user-exam-endpoint';
import { SupervisorOrganize } from 'sdk/cls-k12-sdk-js/src/services/user-exam/models/supervisor-exam';
import { Action } from 'src/app/features/learn/intefaces/action';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-edit-supervisor-organize-exam',
  templateUrl: './edit-supervisor-organize-exam.component.html',
  styleUrls: ['./edit-supervisor-organize-exam.component.scss']
})

export class EditSupervisorOrganizeExamComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['stt', 'name', 'email', 'dob', 'gender', 'select'];
  userExamEndpoint: UserExamEndpoint;
  supervisorExamSubject: TeacherExam[] = [];
  allComplete: boolean = false;
  dataSource = new MatTableDataSource<TeacherExam>([]);
  shiftInfo: ShiftInfo;
  listSupervisorId: { examSubjectSupervisorId: number, action: string }[] = [];
  listSupervisorShiftExam: SupervisorOrganize[];
  isValid = true;
  ngOnInit(): void {

  }
  isLoadingResults = true;

  isFullScreen = true;
  constructor(private _httpClient: HttpClient, @Inject(MAT_DIALOG_DATA) public supervisorShiftExamEdit: SupervisorShiftExamEdit, private _messageService: MessageService, private spinner: NgxSpinnerService) {
    this.userExamEndpoint = new UserExamEndpoint();
    this.shiftInfo = supervisorShiftExamEdit.shiftInfo;
  }
  ngAfterViewInit(): void {

    this.listSupervisorShiftExam = this.supervisorShiftExamEdit.listSupervisorShiftExam;
    this.listSupervisorId = this.listSupervisorShiftExam
      .filter(x => x.organizeExamInfoId == this.shiftInfo.id)
      .map(x => { return { examSubjectSupervisorId: x.examSubjectSupervisorId, action: x.action ?? '' } });
    setTimeout(() => this.isFullScreen = false, 0)
    // this.isFullScreen = false;
    this.spinner.show("editSupervisorLoading");
    this.userExamEndpoint.getSupervisorExamOrganize({ keyword: "", organizeExamId: this.supervisorShiftExamEdit.organizeExamId })
      .then((res) => {
        this.supervisorExamSubject = res;
        this.isLoadingResults = false;
      })
      .then(() => {
        this.supervisorExamSubject.forEach((sup) => {
          let isChecked = this.supervisorShiftExamEdit.listSupervisorShiftExam
            .some(x => x.roomName == this.shiftInfo.roomName && x.shiftName == this.shiftInfo.shiftName && x.examSubjectSupervisorId == sup.id && x.action != "DEL");
          sup.isChoose = isChecked;
        })
      })
      .then(() => {
        this.dataSource.data = this.supervisorExamSubject
      })
      .catch(() => {
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại");
      });
    this.spinner.hide("editSupervisorLoading");
  }
  someComplete(): boolean {
    return this.someChecked(this.allComplete);
  }
  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.checkAll(completed);

    this.validateSuperVisor();
  }
  checkAll(completed: boolean) {

    this.supervisorExamSubject.forEach(sup => {
      if (completed) {
        let action = this.listSupervisorId.find(x => x.examSubjectSupervisorId == sup.id);
        if (action) {
          if (action.action == Action.Delete) {
            action.action = "";
          }
        } else {
          this.listSupervisorId.push({ examSubjectSupervisorId: sup.id, action: Action.Add })
        }
      } else {
        let action = this.listSupervisorId.find(x => x.examSubjectSupervisorId == sup.id);
        if (action) {
          if (action.action == Action.Add) {
            this.listSupervisorId.splice(this.listSupervisorId.indexOf(action), 1)
          } else if (action.action == "") {
            action.action = Action.Delete
          }
        }
      }
    });
    this.dataSource.data.map(x => x.isChoose = completed);
    this.validateSuperVisor();
  }
  someChecked(allComplete: boolean): boolean {
    return this.dataSource.data.filter(x => x.isChoose).length > 0 && !allComplete;
  }
  updateAllComplete(event, id) {
    this.allComplete = this.isCheckAll();
    let supervisorId = this.listSupervisorId.find(x => x.examSubjectSupervisorId == id);
    //xử lý action khi check hoặc uncheck
    if (event) {
      if (supervisorId) {
        if (supervisorId.action == Action.Delete) {
          supervisorId.action = ""
        }
      } else {
        this.listSupervisorId.push({ examSubjectSupervisorId: id, action: Action.Add });
      }
    } else {
      if (supervisorId) {
        if (supervisorId.action == Action.Add) {
          this.listSupervisorId.splice(this.listSupervisorId.indexOf(supervisorId), 1);

        } else if (supervisorId.action == "") {
          supervisorId.action = Action.Delete
        }
      }
    }
    this.validateSuperVisor();
  }
  isCheckAll(): boolean {
    return this.dataSource.data.every(x => x.isChoose);
  }
  validateSuperVisor() {
    debugger;
    if (this.listSupervisorId.length == 0) {
      this.isValid = true;
    } else {
      this.isValid = true;
      this.listSupervisorId.forEach((sup) => {
        let check = !this.listSupervisorShiftExam
          .some(x => x.roomName != this.shiftInfo.roomName && x.shiftName == this.shiftInfo.shiftName && x.examSubjectSupervisorId == sup.examSubjectSupervisorId && sup.action != "DEL");
          if(check==false){
            this.isValid=false;
          }
        })
    }
  }
} 
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSupervisorOrganizeExamComponent } from './edit-supervisor-organize-exam.component';

describe('EditSupervisorOrganizeExamComponent', () => {
  let component: EditSupervisorOrganizeExamComponent;
  let fixture: ComponentFixture<EditSupervisorOrganizeExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSupervisorOrganizeExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSupervisorOrganizeExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChosseTestComponent } from './chosse-test.component';

describe('ChosseTestComponent', () => {
  let component: ChosseTestComponent;
  let fixture: ComponentFixture<ChosseTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChosseTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChosseTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExaminationSubjectSettingDialogComponent } from './examination-subject-setting-dialog.component';

describe('ExaminationSubjectSettingDialogComponent', () => {
  let component: ExaminationSubjectSettingDialogComponent;
  let fixture: ComponentFixture<ExaminationSubjectSettingDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExaminationSubjectSettingDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExaminationSubjectSettingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ExamSubjectSettingReponse } from 'sdk/cls-k12-sdk-js/src/services/exam/response/exam-subject-setting-response';

@Component({
  selector: 'app-examination-subject-setting-dialog',
  templateUrl: './examination-subject-setting-dialog.component.html',
  styleUrls: ['./examination-subject-setting-dialog.component.scss']
})
export class ExaminationSubjectSettingDialogComponent implements OnInit {

  subjectSetting: ExamSubjectSettingReponse = {
    id: 0,
    displayPerPage: 0,
    testTime: 0,
    numberOfMediaOpening: 0,
    isLockTestScreen: false,
    isShuffler: false,
    maxTestTime:60,
  };

  constructor(public dialogRef: MatDialogRef<ExaminationSubjectSettingDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: ExamSubjectSettingReponse) {
    this.subjectSetting = this.data;
  }

  ngOnInit(): void {

  }
  cancel() {
    this.dialogRef.close();
  }
}


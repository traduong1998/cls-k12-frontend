import { SelectionModel } from '@angular/cdk/collections';
import { HttpClient } from '@angular/common/http';
import { Component, ViewChild, AfterViewInit, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { merge, Observable, of as observableOf, Subscription } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { DivisionEndpoint, DivisionOption, UserIdentity } from 'cls-k12-sdk-js/src';

import { TeacherExam } from 'sdk/cls-k12-sdk-js/src/services/exam/models/TeacherExam';
import { UserExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-exam/endpoints/user-exam-endpoint';
import { Action } from 'sdk/cls-k12-sdk-js/src/services/user-exam/enum/action';
import { TeacherExamPagingRequest } from 'sdk/cls-k12-sdk-js/src/services/user-exam/requests/teacher-exam-request';
import { UserAction } from 'sdk/cls-k12-sdk-js/src/services/user-exam/requests/user-action-request';
import { AuthService } from 'src/app/core/services';
import { MessageService } from 'src/app/shared/services/message.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';

@Component({
  selector: 'app-add-teacher-exam',
  templateUrl: './add-teacher-exam.component.html',
  styleUrls: ['./add-teacher-exam.component.scss']
})
export class AddTeacherExamComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'name', 'email', 'dob', 'gender', 'select'];
  baseApiUrl = 'http://localhost:65000';
  isLoadingResults = false;
  protected divisions: DivisionOption[];
  userIdentity: UserIdentity;
  levelManageValue: number = 0;
  endpointDivision: DivisionEndpoint;
  public divisionFilterCtrl: FormControl = new FormControl();
  public divisionCtrl: FormControl = new FormControl();
  public txtKeyword: FormControl = new FormControl('');
  userExamEndpoint: UserExamEndpoint;
  teacherExamSubject: TeacherExam[] = [];
  teacherAction: UserAction[] = [];
  allComplete: boolean = false;
  examId;
  gradeId;
  subjectId;
  organizeId;
  totalItems;
  teacherPagingFilterClone: TeacherExamPagingRequest;
  teacherPagingFilter: TeacherExamPagingRequest;
  routeSub: Subscription;
  @ViewChild(MatPaginator) paginator: MatPaginator = Object.create(null);
  @ViewChild(MatSort) sort: MatSort = Object.create(null);
  isFullScreen = true;

  constructor(private _authService: AuthService, private router: Router, private route: ActivatedRoute, private _messageService: MessageService, private spinner: NgxSpinnerService) {
    this.userExamEndpoint = new UserExamEndpoint();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();
    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    }
  }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = params['id'];
      this.gradeId = params['gradeId'];
      this.subjectId = params['subjectId'];
      this.organizeId = params['organizeId'];

    });
    this.teacherPagingFilter = {

      divisionId: null,
      schoolId: null,
      keyword: "",
      examId: this.examId,
      subjectId: this.subjectId,
      organizeExamId: this.organizeId,
      pageNumber: 1,
      pageSize: 10
    }
    this.getTeacherData();
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.teacherPagingFilter.pageNumber = this.paginator.pageIndex + 1;
      this.teacherPagingFilter.pageSize = this.paginator.pageSize;
      this.getTeacherData();
    });
  }

  ngOnDestroy(): void {
  }

  getTeacherData() {
    this.isFullScreen = false;
    this.spinner.show("teachearLoading");
    this.userExamEndpoint.getTeacherExam(this.teacherPagingFilter)
      .then(res => {
        this.isLoadingResults = false;
        this.teacherExamSubject = res.items;
        this.teacherPagingFilterClone = { ...this.teacherPagingFilter }
        this.teacherExamSubject.map(teacher => {
          if (teacher.isChoose == true) {
            var action = this.teacherAction.find(x => x.id == teacher.id);
            if (action) {
              if (action.action == 'DEL') {
                teacher.isChoose = false;
              }
            } else {
              this.teacherAction.push({ id: teacher.id, action: '' })
            }

          } else {
            // trường hợp chọn xong=>qua trang=>back lại
            // check xem có trong list đã chọn thì tick lại
            if (this.teacherAction.find(x => x.id == teacher.id && x.action == Action.Add) != null) {
              teacher.isChoose = true;
            }
          }

          return teacher;

        });
        this.allComplete = this.isCheckAll();
        this.totalItems = res.totalItems;

        this.spinner.hide("teachearLoading");
      })
      .catch(() => {

        this.spinner.hide("teachearLoading");
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
      });
  }
  getFilterParam() {

    this.teacherPagingFilter.divisionId = this.divisionCtrl.value;

    this.teacherPagingFilter.keyword = this.txtKeyword.value;
    this.teacherPagingFilter.pageNumber = 1;
    this.teacherPagingFilter.pageSize = 10;
  }
  onSubmitFilter() {
    this.getFilterParam();

    if (!compareTwoObject(this.teacherPagingFilter, this.teacherPagingFilterClone)) {
      this.getTeacherData();
    }
  }

  someComplete(): boolean {
    return this.someChecked(this.allComplete);
  }
  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.checkAll(completed);
  }
  checkAll(completed: boolean) {
    this.teacherExamSubject.map(x => x.isChoose = completed);
    this.teacherExamSubject.forEach(sch => {
      if (completed) {
        let action = this.teacherAction.find(x => x.id == sch.id);
        if (action) {
          if (action.action == Action.Delete) {
            action.action = "";
          }
        } else {
          this.teacherAction.push({ id: sch.id, action: Action.Add })
        }
      } else {
        let action = this.teacherAction.find(x => x.id == sch.id);
        if (action) {
          if (action.action == Action.Add) {
            this.teacherAction.splice(this.teacherAction.indexOf(action), 1)
          } else if (action.action == "") {
            action.action = Action.Delete
          }
        }
      }
    });
  }
  someChecked(allComplete: boolean): boolean {
    return this.teacherExamSubject.filter(x => x.isChoose).length > 0 && !allComplete;
  }
  updateAllComplete(event, id) {
    this.allComplete = this.isCheckAll();
    let teacher = this.teacherAction.find(x => x.id == id);
    //xử lý action khi check hoặc uncheck
    if (event) {
      if (teacher) {
        if (teacher.action == Action.Delete) {
          teacher.action = ""
        }
      } else {
        this.teacherAction.push({ id: id, action: Action.Add });
      }
    } else {
      if (teacher) {
        if (teacher.action == Action.Add) {
          this.teacherAction.splice(this.teacherAction.indexOf(teacher), 1);

        } else if (teacher.action == "") {
          teacher.action = Action.Delete
        }
      }
    }
  }
  isCheckAll(): boolean {
    return this.teacherExamSubject.every(x => x.isChoose);
  }

}
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTeacherExamComponent } from './add-teacher-exam.component';

describe('AddTeacherExamComponent', () => {
  let component: AddTeacherExamComponent;
  let fixture: ComponentFixture<AddTeacherExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTeacherExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTeacherExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

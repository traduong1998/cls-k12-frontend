import { Component, Inject, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { ReplaySubject, Subject, Subscription } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { GroupStudentEndpoint, UserIdentity } from 'cls-k12-sdk-js/src';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { UserExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-exam/endpoints/user-exam-endpoint';
import { StudentExam } from 'sdk/cls-k12-sdk-js/src/services/user-exam/models/student-exam';
import { StudentExamRequest } from 'sdk/cls-k12-sdk-js/src/services/user-exam/requests/student-exam-request';
import { AuthService } from 'src/app/core/services';
import { MessageService } from 'src/app/shared/services/message.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-info-student-finish-dialog',
  templateUrl: './info-student-finish-dialog.component.html',
  styleUrls: ['./info-student-finish-dialog.component.scss']
})
export class InfoStudentFinishDialogComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Output() valid;
  displayedColumns: string[] = ['stt', 'name', 'email', 'dob', 'class', 'gender'];
  listStudent: StudentExam[] = [];
  userExamEndpoint: UserExamEndpoint;
  endpointGroupStudent: GroupStudentEndpoint;
  isLoadingResults = false;
  allComplete: boolean = false;

  isFirstLoadGroupStudents = true;

  examId;
  gradeId;
  subjectId;
  totalItems;

  groupStudentFilter = {
    schoolId: null,
    gradeId: null
  }
  userIdentity: UserIdentity;
  routeSub: Subscription;
  public keyWordCtrl: FormControl = new FormControl();
  protected groupStudents: GroupStudentOption[];
  public groupStudentCtrl: FormControl = new FormControl();
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;

  protected _onDestroy = new Subject<void>();
  isFullScreen = true;
  studentPagingFilter: StudentExamRequest;
  constructor(private route: ActivatedRoute, private _authService: AuthService, private _snackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public organizeId: number, private _messageService: MessageService, private spinner: NgxSpinnerService) {
    this.userExamEndpoint = new UserExamEndpoint();
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: '' });
    this.userIdentity = _authService.getTokenInfo();
  }
  ngOnInit(): void {
    this.studentPagingFilter = {
      gradeId: this.gradeId,
      groupStudentId: null,
      keyword: "",
      organizeExamId: this.organizeId,
      pageNumber: 1,
      pageSize: 10
    }

    this.getStudentData();

    // listen for search field value changes
    this.groupStudentFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudents();
      });
  }

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.studentPagingFilter.pageNumber = this.paginator.pageIndex + 1;
      this.studentPagingFilter.pageSize = this.paginator.pageSize;
      this.getStudentData();
    });
    this.setInitialValue();
  }

  protected setInitialValue() {

    this.filteredGroupStudents
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGroupStudent.compareWith = (a: GroupStudentOption, b: GroupStudentOption) => a && b && a.id === b.id;
      });
  }

  ngOnDestroy(): void {
  }

  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onGroupStudentSelectClicked() {
    var schoolIdSelected = this.userIdentity.schoolId;;
    var gradeIdSelected = this.gradeId;
    if (this.isFirstLoadGroupStudents || this.groupStudentFilter.gradeId != gradeIdSelected) {
      this.endpointGroupStudent.getGroupStudentOptions(schoolIdSelected, gradeIdSelected)
        .then(res => {
          this.isFirstLoadGroupStudents = false;
          this.groupStudents = res;
          this.groupStudentFilter.gradeId = gradeIdSelected;
          this.groupStudentFilter.schoolId = schoolIdSelected;
          this.filteredGroupStudents.next(this.groupStudents.slice());
        })
        .catch(

        )
    } else {

    }
  }

  resetGroupStudentSelectCtrl() {
    this.groupStudents = [];
    this.isFirstLoadGroupStudents = true;
    this.groupStudentCtrl.setValue(undefined);
  }

  getStudentData() {
    this.isLoadingResults = true;
    this.isFullScreen = false;
    this.spinner.show("finishStudentLoading");
    this.userExamEndpoint.getStudentExamFinish(this.studentPagingFilter)
      .then(res => {
        this.isLoadingResults = false;
        this.listStudent = res.items;
        this.totalItems = res.totalItems;
        this.spinner.hide("finishStudentLoading");
      })
      .catch(() => {
        this.spinner.hide("finishStudentLoading");
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại!");
      });
  }
  getFilterParam() {
    this.studentPagingFilter.groupStudentId = this.groupStudentCtrl.value;
    this.studentPagingFilter.keyword = this.keyWordCtrl.value;
  }
  onSubmitFilter() {
    this.getFilterParam();
    this.getStudentData();
  }

  exportExcel() {
    this.isLoadingResults = true;
    this.isFullScreen = true;
    this.spinner.show("finishStudentLoading");
    this.userExamEndpoint.exportExcelStudentExamFinish(this.studentPagingFilter)
      .then((urlFile) => {
        this.isLoadingResults = false;
        this.spinner.hide("finishStudentLoading");
        window.open(urlFile);
      })
      .catch(() => {
        this.isLoadingResults = false;
        this.spinner.hide("finishStudentLoading");
        this._messageService.failureCallback("Có lỗi xảy ra trong quá trình tải file vui lòng thử lại!")
      });
  }
}


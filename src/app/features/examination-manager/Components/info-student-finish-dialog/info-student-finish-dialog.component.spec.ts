import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoStudentFinishDialogComponent } from './info-student-finish-dialog.component';

describe('InfoStudentFinishDialogComponent', () => {
  let component: InfoStudentFinishDialogComponent;
  let fixture: ComponentFixture<InfoStudentFinishDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoStudentFinishDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoStudentFinishDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

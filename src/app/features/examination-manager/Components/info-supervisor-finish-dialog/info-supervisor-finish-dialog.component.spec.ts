import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoSupervisorFinishDialogComponent } from './info-supervisor-finish-dialog.component';

describe('InfoSupervisorFinishDialogComponent', () => {
  let component: InfoSupervisorFinishDialogComponent;
  let fixture: ComponentFixture<InfoSupervisorFinishDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfoSupervisorFinishDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoSupervisorFinishDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

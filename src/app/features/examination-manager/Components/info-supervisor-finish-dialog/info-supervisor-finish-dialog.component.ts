import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DivisionEndpoint, DivisionOption, UserIdentity } from 'cls-k12-sdk-js/src';
import { TeacherExam } from 'sdk/cls-k12-sdk-js/src/services/exam/models/TeacherExam';
import { UserExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-exam/endpoints/user-exam-endpoint';
import { TeacherExamPagingRequest } from 'sdk/cls-k12-sdk-js/src/services/user-exam/requests/teacher-exam-request';
import { AuthService } from 'src/app/core/services';
import { MessageService } from 'src/app/shared/services/message.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-info-supervisor-finish-dialog',
  templateUrl: './info-supervisor-finish-dialog.component.html',
  styleUrls: ['./info-supervisor-finish-dialog.component.scss']
})
export class InfoSupervisorFinishDialogComponent implements OnInit {

  displayedColumns: string[] = ['stt', 'name', 'email', 'dob', 'gender'];
  baseApiUrl = 'http://localhost:65000';
  isLoadingResults = false;
  protected divisions: DivisionOption[];
  userIdentity: UserIdentity;
  levelManageValue: number = 0;
  endpointDivision: DivisionEndpoint;
  public divisionFilterCtrl: FormControl = new FormControl();
  public divisionCtrl: FormControl = new FormControl();
  public txtKeyword: FormControl = new FormControl('');
  userExamEndpoint: UserExamEndpoint;
  supervisorExamSubject: TeacherExam[] = [];
  allComplete: boolean = false;
  examId;
  gradeId;
  subjectId;

  totalItems;
  supervisorPagingFilter: TeacherExamPagingRequest;

  @ViewChild(MatPaginator) paginator: MatPaginator = Object.create(null);
  @ViewChild(MatSort) sort: MatSort = Object.create(null);
  isFullScreen = true;
  constructor(private _authService: AuthService, private _messageService: MessageService, @Inject(MAT_DIALOG_DATA) public organizeId: number, private spinner: NgxSpinnerService) {
    this.userExamEndpoint = new UserExamEndpoint();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();
    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    }
  }
  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.supervisorPagingFilter.pageNumber = this.paginator.pageIndex + 1;
      this.supervisorPagingFilter.pageSize = this.paginator.pageSize;
      this.getSupervisorData();
    });
  }
  ngOnInit(): void {
    this.supervisorPagingFilter = {
      divisionId: null,
      schoolId: null,
      keyword: "",
      examId: this.examId,
      subjectId: this.subjectId,
      organizeExamId: this.organizeId,
      pageNumber: 1,
      pageSize: 10
    }
    this.getSupervisorData();

  }


  ngOnDestroy(): void {
  }

  getSupervisorData() {
    this.isLoadingResults = true;
    this.isFullScreen = false;
    this.spinner.show("finishSupervisorLoading");
    this.userExamEndpoint.getSupervisorExamFinish(this.supervisorPagingFilter)
      .then(res => {
        this.isLoadingResults = false;
        this.supervisorExamSubject = res.items;

        this.totalItems = res.totalItems;
        this.spinner.hide("finishSupervisorLoading");
      })
      .catch(() => {
        this.isLoadingResults = false;
        this.spinner.hide("finishSupervisorLoading");
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại")
      });
  }
  getFilterParam() {

    this.supervisorPagingFilter.divisionId = this.divisionCtrl.value;

    this.supervisorPagingFilter.keyword = this.txtKeyword.value;
    this.supervisorPagingFilter.pageNumber = 1;
    this.supervisorPagingFilter.pageSize = 10;
  }
  onSubmitFilter() {
    this.getFilterParam();
    this.getSupervisorData();
  }
  exportExcel() {
    this.isLoadingResults = true;
    this.isFullScreen = true;
    this.spinner.show("finishSupervisorLoading");
    this.userExamEndpoint.exportExcelSupervisorExamFinish(this.supervisorPagingFilter)
      .then((urlFile) => {
        this.isLoadingResults = false;
        this.spinner.hide("finishSupervisorLoading");
        window.open(urlFile);
      })
      .catch(() => {
        this.isLoadingResults = false;
        this.spinner.hide("finishSupervisorLoading");
        this._messageService.failureCallback("Có lỗi xảy ra trong quá trình tải file vui lòng thử lại!")
      });
  }
}
import { QuestionTest } from './../../interface/question-test';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject, Type } from '@angular/core';
import { StatisticQuestion } from '../../interfaces/statistic-question';
import { Tyle } from '../../interfaces/enums/tyle';
import { Level } from '../../interfaces/enums/level';

@Component({
  selector: 'app-dialog-statistic-question',
  templateUrl: './dialog-statistic-question.component.html',
  styleUrls: ['./dialog-statistic-question.component.scss']
})
export class DialogStatisticQuestionComponent implements OnInit {

  totalQuestion: number;
  totalScore: number = 0;

  // biến đếm
  quantityGroup: number = 0;

  singleNB: number = 0;
  singleTH: number = 0;
  singleVD: number = 0;
  singleVDC: number = 0;

  scoreNB: number = 0;
  scoreTH: number = 0;
  scoreVD: number = 0;
  scoreVDC: number = 0;
  scoreGroup: number = 0;

  listStatisticQuestion: StatisticQuestion[] = [];
  displayedColumns: string[] = ['stt', 'type', 'level', 'numberQuestion', 'score'];
  constructor(@Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit(): void {
    console.log("data dia log", this.data)
    //tổng số câu hỏi
    this.totalQuestion = this.data.length;
    this.statisticQuestion();
  }


  statisticQuestion() {
    console.log(this.data);
    this.data.forEach(item => {
      //tổng điểm
      this.totalScore = this.totalScore + item.score;
      if (item.format == "SIG") {
        if (item.level == "NB") {
          this.scoreNB = this.scoreNB + item.score;
          this.singleNB++;
        } else if (item.level == "TH") {
          this.scoreTH = this.scoreTH + item.score;
          this.singleTH++
        } else if (item.level == "VD") {
          this.singleVD++;
          this.scoreVD = this.scoreVD + item.score;
        } else {
          this.singleVDC++;
          this.scoreVDC = this.scoreVDC + item.score;
        }
      } else {
        this.scoreGroup = this.scoreGroup + item.score;
        this.quantityGroup++;
      }
    });

    if (this.singleNB != 0) {
      this.scoreNB = Math.round(this.scoreNB * 100) / 100;
      this.listStatisticQuestion.push({ chapter: null, lesson: null, level: Level.NB, quantity: this.singleNB, score: this.scoreNB, type: Tyle.Single })
    }
    if (this.singleTH != 0) {
      this.scoreTH = Math.round(this.scoreTH * 100) / 100;
      this.listStatisticQuestion.push({ chapter: null, lesson: null, level: Level.TH, quantity: this.singleTH, score: this.scoreTH, type: Tyle.Single })
    }
    if (this.singleVD != 0) {
      this.scoreVD = Math.round(this.scoreVD * 100) / 100;
      this.listStatisticQuestion.push({ chapter: null, lesson: null, level: Level.VD, quantity: this.singleVD, score: this.scoreVD, type: Tyle.Single })
    }
    if (this.singleVDC != 0) {
      this.scoreVDC = Math.round(this.scoreVDC * 100) / 100;
      this.listStatisticQuestion.push({ chapter: null, lesson: null, level: Level.VDC, quantity: this.singleVDC, score: this.scoreVDC, type: Tyle.Single })
    }
    if (this.quantityGroup != 0) {
      this.scoreGroup = Math.round(this.scoreGroup * 100) / 100;
      this.listStatisticQuestion.push({ chapter: null, lesson: null, level: Level.DMD, quantity: this.quantityGroup, score: this.scoreGroup, type: Tyle.Group })
    }

    this.totalScore = Math.round(this.totalScore * 100) / 100;
    console.log(this.listStatisticQuestion);
  }
}

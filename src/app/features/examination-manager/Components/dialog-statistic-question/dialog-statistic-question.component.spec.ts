import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogStatisticQuestionComponent } from './dialog-statistic-question.component';

describe('DialogStatisticQuestionComponent', () => {
  let component: DialogStatisticQuestionComponent;
  let fixture: ComponentFixture<DialogStatisticQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogStatisticQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogStatisticQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

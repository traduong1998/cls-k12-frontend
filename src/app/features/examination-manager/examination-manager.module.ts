import { MathModule } from './../../shared/math/math.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExaminationManagerRoutingModule } from './examination-manager-routing.module';
import { ContextMenuModule } from 'ngx-contextmenu';
import { ListExaminationComponent } from './Views/list-examination/list-examination.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExaminationManagerComponent } from './examination-manager.component';
import { CreateExaminationComponent } from './Views/create-examination/create-examination.component';
import { HeaderCreateExamComponent } from './Components/header-create-exam/header-create-exam.component';
import { ListTestkitComponent } from './Views/list-testkit/list-testkit.component';
import { ContentListTestkitComponent } from './Components/content-list-testkit/content-list-testkit.component';
import { QuestionBankComponent } from './Views/question-bank/question-bank.component';
import { MaxtrixComponent } from './Views/maxtrix/maxtrix.component';
import { ContentQuestionBankComponent } from './Components/content-question-bank/content-question-bank.component';
import { ContentMatrixComponent } from './Components/content-matrix/content-matrix.component';
import { AddNewMatrixComponent } from './Views/add-new-matrix/add-new-matrix.component';
import { ContentCreateMatrixComponent } from './Components/content-create-matrix/content-create-matrix.component';
import { ViewTestComponent } from './Views/view-test/view-test.component';
import { ContentViewTestComponent } from './Components/content-view-test/content-view-test.component';
import { DialogStatisticQuestionComponent } from './Components/dialog-statistic-question/dialog-statistic-question.component';
import { SettingSubjectTestComponent } from './Views/setting-subject-test/setting-subject-test.component';
import { ContentSettingSubjectComponent } from './Components/content-setting-subject/content-setting-subject.component';
import { SettingExamComponent } from './Views/setting-exam/setting-exam.component';
import { ChooseTestComponent } from './Views/choose-test/choose-test.component';
import { FinshComponent } from './Views/finsh/finsh.component';
import { ToastComponent } from './Components/toast/toast.component';
import { CKEditorModule } from 'ckeditor4-angular';
import { QuestionBanksModule } from 'src/app/shared/modules/question-banks/question-banks.module';
import { ContentListExamSubjectComponent } from './Components/content-list-examsubject/content-list-examsubject.component';
import { ExamSubjectOrganizeExaminationComponent } from './Views/exam-subject-organize-examination/exam-subject-organize-examination.component';
import { HeaderOrganizeExamComponent } from './Components/header-organize-exam/header-organize-exam.component';
import { AddStudentExamComponent } from './Components/add-student-exam/add-student-exam.component';
import { AddSupervisiorExamComponent } from './Components/add-supervisior-exam/add-supervisior-exam.component';
import { AddTeacherExamComponent } from './Components/add-teacher-exam/add-teacher-exam.component';
import { OrganizeExamInfoComponent } from './Views/organize-exam-info/organize-exam-info.component';
import { OrganizeFreeExamComponent } from './Components/organize-free-exam/organize-free-exam.component';
import { InfoSupervisorOrganizeExamComponent } from './Components/info-supervisor-organize-exam/info-supervisor-organize-exam.component';
import { EditSupervisorOrganizeExamComponent } from './Components/edit-supervisor-organize-exam/edit-supervisor-organize-exam.component';
import { OrganizeOncentrationExamComponent } from './Components/organize-oncentration-exam/organize-oncentration-exam.component';
import { InfoStudentOrganizeExamComponent } from './Components/info-student-organize-exam/info-student-organize-exam.component';
import { FinishOrganizeComponent } from './Views/finish-organize/finish-organize.component';
import { DialogConfirmComponent } from './Components/dialog-confirm/dialog-confirm.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { DetailExamComponent } from './Views/detail-exam/detail-exam.component';
import { EditExaminationComponent } from './Views/edit-examination/edit-examination.component';
import { UpdateScoreQuetionsDialogComponent } from './Components/update-score-quetions-dialog/update-score-quetions-dialog.component';
import { EditTestComponent } from './Views/edit-test/edit-test.component';
import { ContentEditTestComponent } from './Components/content-edit-test/content-edit-test.component';
import { ContentListExamComponent } from './Components/content-list-exam/content-list-exam.component';
import { ListOganizeExamComponent } from './Views/list-oganize-exam/list-oganize-exam.component';
import { AssignmentSchoolComponent } from './Views/assignment-school/assignment-school.component';
import { ListExaminationApprovalComponent } from './Views/list-examination-approval/list-examination-approval.component';
import { ExaminationSummaryReviewComponent } from './Views/examination-summary-review/examination-summary-review.component';
import { ExaminationSubjectSettingDialogComponent } from './Components/examination-subject-setting-dialog/examination-subject-setting-dialog.component';

import { InfoStudentFinishDialogComponent } from './Components/info-student-finish-dialog/info-student-finish-dialog.component';
import { InfoTeacherFinishDialogComponent } from './Components/info-teacher-finish-dialog/info-teacher-finish-dialog.component';
import { InfoSupervisorFinishDialogComponent } from './Components/info-supervisor-finish-dialog/info-supervisor-finish-dialog.component';
import { EditTestkitComponent } from './Views/edit-testkit/edit-testkit.component';
import { AddTestkitFromFileComponent } from './Views/add-testkit-from-file/add-testkit-from-file.component';
import { ExaminationDetailViewComponent } from './Views/examination-detail-view/examination-detail-view.component';
import { QuestionBankDialogComponent } from './Components/question-bank-dialog/question-bank-dialog.component';
import { ManageQuestionBankModule } from '../manage-question-bank/manage-question-bank.module';
import { AddMatrixTestkitDialogComponent } from './Components/add-matrix-testkit-dialog/add-matrix-testkit-dialog.component';
import { ViewExamFinishComponent } from './Views/view-exam-finish/view-exam-finish.component';
@NgModule({
  declarations: [
    ExaminationManagerComponent,
    ListExaminationComponent,
    CreateExaminationComponent,
    EditExaminationComponent,
    HeaderCreateExamComponent,
    ListTestkitComponent,
    ContentListTestkitComponent,
    QuestionBankComponent,
    MaxtrixComponent,
    ContentQuestionBankComponent,
    ContentMatrixComponent,
    AddNewMatrixComponent,
    ContentCreateMatrixComponent,
    ViewTestComponent,
    ContentViewTestComponent,
    DialogStatisticQuestionComponent,
    SettingSubjectTestComponent,
    ContentSettingSubjectComponent,
    SettingExamComponent,
    ChooseTestComponent,
    FinshComponent,
    ToastComponent,
    ContentListExamSubjectComponent,
    ExamSubjectOrganizeExaminationComponent,
    HeaderOrganizeExamComponent,
    AddStudentExamComponent,
    AddSupervisiorExamComponent,
    AddTeacherExamComponent,
    OrganizeExamInfoComponent,
    OrganizeFreeExamComponent,
    InfoSupervisorOrganizeExamComponent,
    EditSupervisorOrganizeExamComponent,
    OrganizeOncentrationExamComponent,
    InfoStudentOrganizeExamComponent,
    FinishOrganizeComponent,
    DialogConfirmComponent,
    DetailExamComponent,
    UpdateScoreQuetionsDialogComponent,
    EditTestComponent,
    ContentEditTestComponent,
    ListOganizeExamComponent,
    AssignmentSchoolComponent,
    ContentListExamComponent,
    ListExaminationApprovalComponent,
    ExaminationSummaryReviewComponent,
    ExaminationSubjectSettingDialogComponent,
    InfoStudentFinishDialogComponent,
    InfoSupervisorFinishDialogComponent,
    InfoTeacherFinishDialogComponent,
    AddTestkitFromFileComponent,
    EditTestkitComponent,
    ExaminationDetailViewComponent,
    AddMatrixTestkitDialogComponent,
    QuestionBankDialogComponent,
    ViewExamFinishComponent,
  ],
  imports: [
    MathModule,
    SharedModule,
    CKEditorModule,
    ExaminationManagerRoutingModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    QuestionBanksModule,
    ContextMenuModule.forRoot(),
    NgxMatSelectSearchModule,
    ManageQuestionBankModule
  ],
  providers: [

  ]
})
export class ExaminationManagerModule { }

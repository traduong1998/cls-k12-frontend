/**Sử dụng cho các option đơn giản */
export class OptionSelect {
    /**giá trị */
    value: string;

    display: string;

    checked?: boolean;
}
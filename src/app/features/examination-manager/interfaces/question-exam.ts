import { AnswerRequestOrResponse, QuestionRequestOrResponse } from "src/app/shared/modules/question-banks/Interfaces/question-data";

export class QuestionExam implements QuestionRequestOrResponse {
    id?: number;
    type: string;
    questionGroupId?: number;
    level: string;
    content: string;
    format: string;
    isChecked: boolean = false;
    score: number | undefined;
    createDate?: Date;
    approvalDate?: Date;
    userId?: number;
    GradeId: number | undefined;
    SubjectId: number | undefined;
    ChapterId: number | undefined;
    UnitId: number | undefined;
    answers: AnswerRequestOrResponse[];
    questions?: QuestionExam[];
    action?: string;
}
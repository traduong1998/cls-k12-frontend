
export interface ShiftExam {
    id:number;
    shiftName: string;
    roomName: string;
    realStudent:number;
    maxStudent:number;
    startDate:Date;
    endDate:Date;

  }
  
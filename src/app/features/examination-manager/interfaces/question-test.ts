import { QuestionExam } from "./question-exam";

export class QuestionTest {
    id: number;
    questions?: QuestionExam[];
    action?: string;
}
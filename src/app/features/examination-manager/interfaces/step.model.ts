export interface Step {
    name: string;
    isDone: boolean;
  }
  
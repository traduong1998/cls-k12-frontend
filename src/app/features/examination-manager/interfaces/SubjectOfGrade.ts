import { SubjectResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/subjectResponses';

export interface SubjectOfGrade extends SubjectResponses{
    checked:boolean;
}
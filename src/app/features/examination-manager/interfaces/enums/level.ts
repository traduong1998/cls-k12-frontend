export enum Level {
    /**Nhận biết */
    NB = 'NB',
    VD = 'Vận dụng',
    VDC = 'Vận dụng cao',
    TH = 'Thông hiểu',
    DMD ='Đa mức độ'
}
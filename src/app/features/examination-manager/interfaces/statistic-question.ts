
import { Tyle } from "./enums/tyle";
import { Level } from "./enums/level";

export interface StatisticQuestion {
    chapter: string,
    lesson: string,
    type: Tyle,
    level: Level,
    quantity: number,
    score: number
}
import { SubjectOfExam } from "cls-k12-sdk-js/src/services/exam/models/subject-of-exam";

export class SubjectExam implements SubjectOfExam {
    examSubjectId: number;
    subjectId: number;
    testkitId: string;
    testKitname: string;
    startDate: Date;
    endDate: Date;
    gradeId: number;
    subjectName?: string;

    checked?: boolean;

    static From(data: SubjectOfExam) {
        var m = new SubjectExam();
        m.examSubjectId = data.examSubjectId;
        m.subjectId = data.subjectId;
        m.endDate = data.endDate;
        m.startDate = data.startDate;
        m.testKitname = data.testKitname;
        m.testkitId = data.testkitId;
        return m;
    }
}


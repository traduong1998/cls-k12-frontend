import { SubjectExam } from "sdk/cls-k12-sdk-js/src/services/exam/models/subject-exam"

export class SubjectOfExam implements SubjectExam {
    subjectId: number;
    testKitName: string;
    startDate: Date
	endDate: Date
    nameSubject: string
}
import { Curriculum } from "sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum";
import { QuestionLevelOption } from "src/app/shared/modules/question-banks/Interfaces/question-level-option";
import { QuestionTypeOption } from "src/app/shared/modules/question-banks/Interfaces/question-type-option";
import { Chapter } from "../../learn/intefaces/chapter";
import { Unit } from "../../learn/intefaces/unit";

export class MatrixInfo {
    gradeId: number;
    subjectId: number;
    subjectName: string;
    gradeName: string;
    matrixId?: number;
}

export class MatrixVM {
    id?: number;
    name: string;
    numberTest: number;
    gradeId: number;
    subjectId: number;
    subjectName: string;
    gradeName: string;
    matrixDetails: MatrixDetailVM[];
}

export class MatrixDetailVM {
    id?: number;
    contentPanelId: number;
    contentPanelName?: string;
    chapterId: number;
    chapterName?: string;
    unitId: number;
    unitName?: string;
    formatQuestion: string;
    typeOfQuestion: string;
    level: string;
    totalQuestion: number;
    totalScore: number;
    matrixId?: number;
    action: string;
    //value selected để hiển thị dropdown. nếu = 0 thì là không chọn
    chapterSelected?: string;
    contentPanelSelected?: string;
    unitSelected?: string;
    formatQuestionSelected?: string;
    levelSelected?: string;
    typeQuestionSelected?: string;
    

    // danh sách
    units?: Unit[];
    chapters?: Chapter[];
    contentPanels?: Curriculum[];
    levels?: QuestionLevelOption[];
    types?: QuestionTypeOption[];

    //xác định đã load danh sách chưa
    chapterLoaded?: boolean;
    contentPanelLoaded?: boolean;
    unitLoaded?: boolean;
    /// 1: trùng cấu hình
    /// 2: số lượng, điểm > 0
    validCode?: 1 | 2
}
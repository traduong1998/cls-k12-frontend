import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExaminationManagerComponent } from './examination-manager.component';

describe('ExaminationManagerComponent', () => {
  let component: ExaminationManagerComponent;
  let fixture: ComponentFixture<ExaminationManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExaminationManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExaminationManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishOrganizeComponent } from './finish-organize.component';

describe('FinishOrganizeComponent', () => {
  let component: FinishOrganizeComponent;
  let fixture: ComponentFixture<FinishOrganizeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinishOrganizeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishOrganizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

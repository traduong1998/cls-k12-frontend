import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { ExamEnpoint } from 'sdk/cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { OrganizeExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/endpoints/organize-exam-endpoint';
import { OrganizeExamFinish } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/organize-exam-finish';
import { ShiftInfoDTO } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/shift-info-dto';
import { ShiftInfo } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/ShiftInfo';
import { AssignStudentsToShiftExamsRequest } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/request/assign-students-shiftexams';
import { UserExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-exam/endpoints/user-exam-endpoint';
import { StepHeaderControlOrganize } from 'src/app/features/learn/intefaces/stepHeaderControl';
import { MessageService } from 'src/app/shared/services/message.service';
import { isDebuggerStatement } from 'typescript';
import { InfoStudentFinishDialogComponent } from '../../Components/info-student-finish-dialog/info-student-finish-dialog.component';
import { InfoStudentOrganizeExamComponent } from '../../Components/info-student-organize-exam/info-student-organize-exam.component';
import { InfoSupervisorFinishDialogComponent } from '../../Components/info-supervisor-finish-dialog/info-supervisor-finish-dialog.component';
import { InfoSupervisorOrganizeExamComponent } from '../../Components/info-supervisor-organize-exam/info-supervisor-organize-exam.component';
import { InfoTeacherFinishDialogComponent } from '../../Components/info-teacher-finish-dialog/info-teacher-finish-dialog.component';
import { RoomName } from '../../interfaces/roomName.model';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { OrganizeExamDetail } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/OrganizeExamDetail';

@Component({
  selector: 'app-finish-organize',
  templateUrl: './finish-organize.component.html',
  styleUrls: ['./finish-organize.component.scss']
})
export class FinishOrganizeComponent implements OnInit {

  routeSub: Subscription
  subjectId;
  examId;
  gradeId;
  organizeId;
  //fake
  public roomStudent: RoomName[] = [{ name: 'Phòng 1', totalStudent: 10 }];
  displayedColumns: string[] = ['stt', 'shiftName', 'roomName', 'realStudent', 'startDate', 'endDate', 'supervision', 'student'];
  displayedRoomColumns: string[] = ['stt', 'name', 'totalStudents'];
  listSubjectOfExam;
  stepHeaderCtrl: StepHeaderControlOrganize;
  currentPage: 3;
  organizeExamEndPoint: OrganizeExamEndpoint;
  examEndPoint: ExamEnpoint;
  userExamEndpoint: UserExamEndpoint;
  organizeExamFinish: OrganizeExamFinish;
  shiftExam: ShiftInfo[] = [];
  isValidStudent = true;
  isValidShiftDate = true;
  totalSubject = 0;
  isView: boolean = false;
  isReady: Observable<boolean>;
  organizeExamDetail: OrganizeExamDetail = {
    id: 0,
    testTime: 0,
    totalRoom: 1,
    totalShift: 1,
    startDate: new Date(),
    endDate: new Date(),
    startShiftDate: new Date(),
    totalStudent: 0,
    examType: 'CON',
    subjectName: '',
    breakTime: 3,
    status: "EDI"
  };
  errorDetail: string = "";
  shiftExamAssignStudents = [];
  constructor(private router: Router, private route: ActivatedRoute, private _messageService: MessageService, private dialog: MatDialog, private spinner: NgxSpinnerService) {
    this.stepHeaderCtrl = {
      currentPage: 3,
      //Truyền examid cho đúng nha a
      examId: 1,
      subjectId: 1
    };
    this.organizeExamEndPoint = new OrganizeExamEndpoint();
    this.examEndPoint = new ExamEnpoint();
    this.userExamEndpoint = new UserExamEndpoint();
  }

  ngOnInit(): void {
    let url = window.location.href;
    this.isView = url.includes('/view/');
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = params['id'];
      this.subjectId = params['subjectId'];
      this.gradeId = params['gradeId'];
      this.organizeId = params['organizeId'];
    });
    this.spinner.show();
    this.organizeExamEndPoint.getOrganizeExamFinish(this.organizeId).then((res) => {
      this.organizeExamFinish = res;
      if (res.status != 'PUB') {
        this.examEndPoint.getSubjectExam(this.examId)
          .then((res) => {
            this.totalSubject = res.length;
          })
          .catch(() => this._messageService.failureCallback('Có lỗi xảy ra vui lòng thử lại'));
      }
      return this.organizeExamEndPoint.getListOrganizeExamInfo(this.organizeId);
    })
      .then((res) => {
        this.shiftExam = res;
        let rooms = this.shiftExam.filter(x => x.shiftName == this.shiftExam[0].shiftName);
        if (res.length > 0) {
          this.roomStudent = [];
          rooms.forEach(room => {
            this.roomStudent.push({ name: room.roomName, totalStudent: room.maximumStudent });
          });
        }
        this.checkValidStudent();
      })
      .then(() => {
        this.spinner.hide();
        this.isReady = new Observable(ob => { ob.next(true); });
      })
      .catch(() => {
        this.spinner.hide();
        this._messageService.failureCallback('có lỗi xảy ra khi lấy thông tin ca kì tổ chức thi');
      });
    this.organizeExamEndPoint.getOrganizeExamDetail(this.organizeId)
      .then((res) => {
        this.organizeExamDetail = res;
      })
      .then(() => {
        this.examEndPoint.getExamSubjectSetting(this.examId, this.subjectId)
          .then(res => {

            this.organizeExamDetail.testTime = res.testTime;
          })
          .catch(err => {
            this._messageService.failureCallback("Có lỗi xảy ra Vui lòng thử lại");
            this.router.navigate(['dashboard/examination-manager/organize/', this.examId, 'grade', this.gradeId, 'examsubject', this.subjectId, 'assignment', this.organizeId]);
          })
      })
      .catch((err) => {
        this._messageService.failureCallback("có lỗi xảy ra vui lòng thử lại");
        this.router.navigate(['dashboard/examination-manager/organize/', this.examId, 'grade', this.gradeId, 'examsubject', this.subjectId, 'assignment', this.organizeId]);
      });
    this.checkValidShiftDate();
    this.checkValidStudent();
  }
  previousPage() {
    this.router.navigate(['dashboard/examination-manager/organize/', this.examId, 'grade', this.gradeId, 'examsubject', this.subjectId, 'setting', this.organizeId]);
  }

  save() {
    if (this.organizeExamFinish.status != 'PUB') {
      // gán thí sinh vào các ca thi
      this.spinner.show();
      this.errorDetail = "";
      this.shiftExamAssignStudents = [];
      let index = 0;
      this.shiftExam.forEach(e => {
        this.shiftExamAssignStudents.push({
          index: index,
          organizeExamInfoId: e.id,
          numberOfStudent: e.realStudent
        });
        index += 1;
      });


      let addRequest: AssignStudentsToShiftExamsRequest = {
        organizeExamId: parseInt(this.organizeId),
        shiftExams: this.shiftExamAssignStudents
      }
      let assignStudent = this.organizeExamEndPoint.assignStudentsToShiftExams(addRequest)
        .then(res => {
          if (res) {

          } else {
            this._messageService.failureCallback("Gán học sinh vào kỳ thi không thành công");
          }
        })
        .catch((err: ErrorResponse) => {
          this._messageService.failureCallback(err.errorDetail);
        });

      let updateStatus = this.organizeExamEndPoint.updateOrganizeExamStatus(this.organizeId, this.totalSubject)
        .then((res) => {
          if (res) {
            this._messageService.susccessCallback('Lưu thành công');
            this.organizeExamFinish.status = 'PUB';
            setTimeout(() => {
              this.router.navigate(['dashboard/examination-manager/organize/', this.examId, 'grade', this.gradeId, 'examsubject']);
            }, 5000);
          } else {
            this._messageService.failureCallback('có lỗi xảy ra khi lưu');
          }
        })
        .catch(() => this._messageService.failureCallback('có lỗi xảy ra khi lưu'));
      Promise.all(
        [
          assignStudent.catch((err: ErrorResponse) => {
            this.errorDetail += err.errorDetail + '. ';
          }),
          updateStatus.catch((err: ErrorResponse) => {
            this.errorDetail += err.errorDetail + '. ';
          }),
        ]
      ).then((values) => {
        if (this.errorDetail != '') {
          this.spinner.hide();
          this._messageService.failureCallback(this.errorDetail);
        } else {
          this.spinner.hide();
          this.router.navigate(['dashboard/examination-manager/organize/', this.examId, 'grade', this.gradeId, 'examsubject', this.subjectId, 'finish', this.organizeId]);
        }
      });
    } else {
      this._messageService.susccessCallback('Lưu thành công');
      this.organizeExamFinish.status = 'PUB';
      this.router.navigate(['dashboard/examination-manager/organize/', this.examId, 'grade', this.gradeId, 'examsubject']);
    }
  }

  openInfoSupervisorDialog(index: number) {
    const dialogRef = this.dialog.open(InfoSupervisorOrganizeExamComponent, {
      width: '80%',
      data: { organizeExamId: this.organizeId, shiftInfo: this.shiftExam[index], isFinish: true },
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  openInfoStudentDialog(index: number) {
    let selectedShiftExam = this.shiftExam[index];
    let skipNumber = 0;
    if (index != 0) {
      this.shiftExam.filter((e, i) => {
        if (i < index) {
          skipNumber += e.realStudent;
        }
      })
    }

    let dataSendStudentDialog = {
      organizeExamId: this.organizeId,
      startDate: selectedShiftExam.startDate,
      endDate: selectedShiftExam.endDate,
      roomName: selectedShiftExam.roomName,
      shiftName: selectedShiftExam.shiftName,
      fromRecord: skipNumber,
      size: selectedShiftExam.realStudent,
      isFinish: true
    }

    const dialogRef = this.dialog.open(InfoStudentOrganizeExamComponent, {
      width: '80%',
      data: dataSendStudentDialog,
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  openInfoStudentFinishDiagLog() {
    const dialogRef = this.dialog.open(InfoStudentFinishDialogComponent, {
      width: '80%',
      data: this.organizeId,
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  openInfoTeacherFinishDiagLog() {
    const dialogRef = this.dialog.open(InfoTeacherFinishDialogComponent, {
      width: '80%',
      data: this.organizeId,
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  openInfoSupervisorFinishDiagLog() {
    const dialogRef = this.dialog.open(InfoSupervisorFinishDialogComponent, {
      width: '80%',
      data: this.organizeId,
      autoFocus: false
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }
  checkValidStudent() {
    let sum = this.shiftExam.reduce((accumulator, current) => accumulator + current.realStudent, 0);
    if (sum != this.organizeExamFinish.totalStudent) {
      this.isValidStudent = false;
    } else {
      this.isValidStudent = true;
    }
  }
  checkValidShiftDate() {
    this.isValidShiftDate = !this.shiftExam.some((x, i) => new Date(x.endDate) > new Date(this.organizeExamDetail.endDate))
    if (this.isValidShiftDate == false) {
      return;
    }
    for (let index = 0; index < this.shiftExam.length; index++) {
      this.isValidShiftDate = !this.shiftExam.some((x, i) =>
        this.shiftExam[i].shiftName != this.shiftExam[index].shiftName && x.endDate < this.organizeExamDetail.endDate &&
        Math.abs(new Date(this.shiftExam[index].startDate).getTime() - new Date(x.startDate).getTime()) < (this.organizeExamDetail.testTime + this.organizeExamDetail.breakTime) * 60000);
      if (this.isValidShiftDate == false) {
        return;
      }
    }
    this.isValidShiftDate = true;
    return;
  }
  exportListStudent(index) {
    this.spinner.show();
    let selectedShiftExam = this.shiftExam[index];
    let skipNumber = 0;
    if (index != 0) {
      this.shiftExam.filter((e, i) => {
        if (i < index) {
          skipNumber += e.realStudent;
        }
      })
    }

    let shiftInfo: ShiftInfoDTO = {
      organizeExamId: this.organizeId,
      startDate: selectedShiftExam.startDate,
      endDate: selectedShiftExam.endDate,
      roomName: selectedShiftExam.roomName,
      shiftName: selectedShiftExam.shiftName,
      fromRecord: skipNumber,
      size: selectedShiftExam.realStudent,
      realStudent: selectedShiftExam.realStudent
    }
    this.userExamEndpoint.exportExcelExamSubjectStudentOrganize(shiftInfo, { organizeExamId: this.organizeId, keyword: null, fromRecord: shiftInfo.fromRecord, size: shiftInfo.realStudent })
      .then((urlFile) => {
        this.spinner.hide();
        window.open(urlFile);
      })
      .catch(() => {
        this.spinner.hide();
        this._messageService.failureCallback("Có lỗi xảy ra trong quá trình tải file vui lòng thử lại!")
      });
    this.spinner.hide();
  }
  exportListSupervisor(index) {
    this.spinner.show();
    let selectedShiftExam = this.shiftExam[index];
    let shiftInfo: ShiftInfoDTO = {
      organizeExamId: this.organizeId,
      startDate: selectedShiftExam.startDate,
      endDate: selectedShiftExam.endDate,
      roomName: selectedShiftExam.roomName,
      shiftName: selectedShiftExam.shiftName,
    }
    this.userExamEndpoint.exportExcelExamSubjectSupervisorOrganize(shiftInfo, { organizeExamId: this.organizeId, keyword: null, organizeExamInfoId: selectedShiftExam.id })
      .then((urlFile) => {
        this.spinner.hide();
        window.open(urlFile);
      })
      .catch(() => {
        this.spinner.hide();
        this._messageService.failureCallback("Có lỗi xảy ra trong quá trình tải file vui lòng thử lại!")
      });
    this.spinner.hide();
  }
  sumRealStudents() {
    let sum = this.shiftExam.reduce((accumulator, current) => accumulator + current.realStudent, 0);
    return sum;
  }
}

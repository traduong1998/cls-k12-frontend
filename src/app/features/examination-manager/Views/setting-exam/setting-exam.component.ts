import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ExamEnpoint } from 'sdk/cls-k12-sdk-js/src/services/exam/endpoints/exam-enpoint';
import { ExamSetting } from 'sdk/cls-k12-sdk-js/src/services/exam/requests/exam-setting';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { ToastComponent } from '../../Components/toast/toast.component';

@Component({
  selector: 'app-setting-exam',
  templateUrl: './setting-exam.component.html',
  styleUrls: ['./setting-exam.component.scss']
})
export class SettingExamComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  routeSub: Subscription;
  //mã kỳ thi
  examId: number;
  step;
  logTime = 5;
  isPublished: boolean = true;
  stepHeaderControl;
  disableContinue: boolean = true;
  // data setting exam
  settingData: ExamSetting = { id: 0, isPreservingTime: false, isPublicAnswer: false, isPublicScore: false, logTime: 5 };
  settingDataClone;
  private examApi: ExamEnpoint
  constructor(private route: ActivatedRoute, private router: Router, private _snackBar: MatSnackBar) {
    this.examApi = new ExamEnpoint();

    this.routeSub = this.route.params.subscribe(params => {
      this.examId = +params['id'];
      this.step = params['step'];
    });

    this.stepHeaderControl = {
      currentPage: 3,
      isCreatingTest: false,
      examId: +this.examId,
      step: 3
    }
    this.getSettingData();
  }

  ngOnInit(): void {


    this.examApi.getExamStatus(this.examId)
      .then(res => {
        this.isPublished = res == 'PUB';
      })
      .catch();
  }

  getSettingData() {
    this.examApi.getSettingExam(this.examId).then((res) => {
      if (res) {
        this.settingData.id = this.examId;
        this.settingData.isPreservingTime = res.isPreservingTime;
        this.settingData.isPublicScore = res.isPublicScore;
        this.settingData.isPublicAnswer = res.isPublicAnswer;
        this.settingData.logTime = res.logTime;

        this.settingDataClone = { ...this.settingData };
      }
      this.disableContinue = false;
    });
  }

  nextPage() {
    if (!this.isPublished) {
      this.stepHeaderControl = {
        currentPage: 4,
        isCreatingTest: false,
        examId: +this.examId,
        step: 4
      }

      if (this.logTime < 5) {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Thời gian ghi log ít nhất 5 phút',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      } else {
        if (!compareTwoObject(this.settingData, this.settingDataClone)) {
          this.examApi.createSettingExam(this.settingData, this.examId).then(res => {
            if (res) {
              // this._snackBar.open("Thành công", 'Ok', {
              //   duration: 1500,
              //   panelClass: ['green-snackbar'],
              //   horizontalPosition: 'center',
              //   verticalPosition: 'top',
              // });
              this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "finish"])
            }
            else {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'hất bại',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            }
          });
        } else {
          this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "finish"])
        }
      }
    }
    else {
      this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "finish"])
    }
  }

  backPage() {
    this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "choose-test"])
  }
  changeCheckbox(event, tag) {
    if (tag == 1) {
      if (event.checked) {
        this.settingData.isPreservingTime = true;
      }
      else {
        this.settingData.isPreservingTime = false;
      }
    }
    else if (tag == 2) {
      if (event.checked) {
        this.settingData.isPublicScore = true;
      }
      else {
        this.settingData.isPublicScore = false;
      }
    }
    else if (tag == 3) {
      if (event.checked) {
        this.settingData.isPublicAnswer = true;
      }
      else {
        this.settingData.isPublicAnswer = false;
      }
    }
  }
  changeInputLogTime() {
    if (this.logTime < 5) {
      this.logTime = 5;
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Thời gian ghi log ít nhất 5 phút',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    } else {
      this.settingData.logTime = this.logTime;
    }
  }
}

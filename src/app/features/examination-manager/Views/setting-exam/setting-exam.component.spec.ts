import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingExamComponent } from './setting-exam.component';

describe('SettingExamComponent', () => {
  let component: SettingExamComponent;
  let fixture: ComponentFixture<SettingExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

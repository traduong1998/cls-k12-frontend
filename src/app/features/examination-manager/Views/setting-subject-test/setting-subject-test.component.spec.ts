import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingSubjectTestComponent } from './setting-subject-test.component';

describe('SettingSubjectTestComponent', () => {
  let component: SettingSubjectTestComponent;
  let fixture: ComponentFixture<SettingSubjectTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingSubjectTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingSubjectTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ExamEnpoint } from 'sdk/cls-k12-sdk-js/src/services/exam/endpoints/exam-enpoint';
//import { SettingSubject } from 'sdk/cls-k12-sdk-js/src/services/exam/models/setting-subject';
import { ExamSubjectSetting } from 'sdk/cls-k12-sdk-js/src/services/exam/requests/exam-subject-setting';
import { ExamSubjectSettingReponse } from 'sdk/cls-k12-sdk-js/src/services/exam/response/exam-subject-setting-response';
import { MessageService } from 'src/app/shared/services/message.service';
import { ToastComponent } from '../../Components/toast/toast.component';

@Component({
  selector: 'app-setting-subject-test',
  templateUrl: './setting-subject-test.component.html',
  styleUrls: ['./setting-subject-test.component.scss']
})
export class SettingSubjectTestComponent implements OnInit {

  routeSub: Subscription;
  subjectExamId;
  examId;
  subjectId;
  step = 2
  disableContinue: boolean = true;
  //dataa lây thoogn tin lấy đc
  dataSettingSubject: ExamSubjectSetting;

  //testkitid
  testKitId: number;
  //reponse subject setting

  settingSubjectForm = this.fb.group({
    displayPerPage: new FormControl(5, [Validators.required]),
    numberOfMediaOpening: new FormControl(0, [Validators.required]),
    testTime: new FormControl(60, [Validators.required]),
    isShuffler: new FormControl(true),
    isLockTestScreen: new FormControl(false)
  });

  get displayPerPage() { return this.settingSubjectForm.get('displayPerPage'); }
  get numberOfMediaOpening() { return this.settingSubjectForm.get('numberOfMediaOpening'); }
  get testTime() { return this.settingSubjectForm.get('testTime'); }
  get isShuffler() { return this.settingSubjectForm.get('isShuffler') }
  get isLockTestScreen() { return this.settingSubjectForm.get('isLockTestScreen') }

  dataExamSubjectSettingReponse: ExamSubjectSettingReponse;
  private examApi: ExamEnpoint;

  constructor(private router: Router, private route: ActivatedRoute, private _snackBar: MatSnackBar, private fb: FormBuilder,private _messageService :MessageService) {
    this.testKitId = +this.route.snapshot.queryParamMap.get("testKitId");
    this.examApi = new ExamEnpoint();
    this.routeSub = this.route.params.subscribe(params => {
      this.subjectExamId = +params['id'];
      this.examId = +params['examId'];
      this.subjectId = +params['subjectId'];
    });

    this.examApi.getExamSubjectSetting(this.examId, this.subjectId)
      .then(res => {
        this.dataExamSubjectSettingReponse = res;

        this.displayPerPage.setValue(res.displayPerPage);
        this.isShuffler.setValue(res.isShuffler);
        this.testTime.setValue(res.testTime);
        this.numberOfMediaOpening.setValue(res.numberOfMediaOpening);
        this.isLockTestScreen.setValue(res.isLockTestScreen);
        this.disableContinue = false;
      }).catch(err => {
        console.log("err")
      })
  }
  ngOnInit(): void {
    this.examApi.getExamStatus(this.examId)
      .then(res => {
        if (res == 'PUB') {
          this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "choose-test"]);
        }
      })
      .catch();
  }

  cancel() {
    this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "choose-test"]);
  }
  save() {
    debugger
    this.dataSettingSubject = { id: this.subjectExamId, displayPerPage: this.displayPerPage.value, isLockTestScreen: this.isLockTestScreen.value, isShuffler: this.isShuffler.value, numberOfMediaOpening: this.numberOfMediaOpening.value, testTime: this.testTime.value };
    this.examApi.updateExamSubjectSetting(this.subjectExamId, this.dataSettingSubject).then(res => {
      // this._snackBar.openFromComponent(ToastComponent, {
      //   data: ['Thiết lập thành công'],
      //   duration: 1000,
      //   horizontalPosition: 'center',
      //   verticalPosition: 'top',
      // });

      // router cùng tên đề
      this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "choose-test"]);

    }).catch(err => {
      this._messageService.failureCallback("Thiết lập không thành công",1000);
    })
  }
}

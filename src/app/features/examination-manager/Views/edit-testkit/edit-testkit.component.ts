
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { QuestionFileService } from 'src/app/core/services/question-file/question-file.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Test } from 'sdk/cls-k12-sdk-js/src/services/test/models/test';
import { QuestionModel } from 'sdk/cls-k12-sdk-js/src/services/test/requests/question-model';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { TestKit } from 'sdk/cls-k12-sdk-js/src/services/testkit/models/testkit';
import { TestKitEnpoint } from 'sdk/cls-k12-sdk-js/src/services/testkit/enpoints/testkit-enpoint';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { TestEnpoint } from 'sdk/cls-k12-sdk-js/src/services/test/enpoint/test-enpoint';
import { CLS, ExamEnpoint, GradeEndpoint, SubjectEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { GradeInfo } from 'sdk/cls-k12-sdk-js/src/services/grade/requests/grade-info-request';
import { SubjectInfo } from 'sdk/cls-k12-sdk-js/src/services/subject/models/subject-info-request';
import { SubjectTestkitRequest } from 'sdk/cls-k12-sdk-js/src/services/exam/requests/update-subject-testkit-request';
import { QuestionExam } from '../../interfaces/question-exam';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { DialogConfirm } from '../../interfaces/dialog-confirm';
import { ToastComponent } from '../../Components/toast/toast.component';
import { DialogStatisticQuestionComponent } from '../../Components/dialog-statistic-question/dialog-statistic-question.component';
import { DialogConfirmComponent } from '../../Components/dialog-confirm/dialog-confirm.component';
import { UpdateScoreQuetionsDialogComponent } from '../../Components/update-score-quetions-dialog/update-score-quetions-dialog.component';
import { QuestionConverterEndpoint } from 'sdk/cls-k12-sdk-js/src/services/question-converter/endpoints/question-converter-endpoint';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/errorResponse';
import { UploadTestFilesReponse } from 'sdk/cls-k12-sdk-js/src/services/question-converter/responses/upload-test-files-response';
import { TestKitBankEnpoint } from 'sdk/cls-k12-sdk-js/src/services/testkit-bank/endpoints/testkit-bank-endpoint';
import { UpdateTestKit } from 'sdk/cls-k12-sdk-js/src/services/testkit/requests/update-testkit';
import { X } from '@angular/cdk/keycodes';
import { Action } from 'src/app/shared/modules/question-banks/Interfaces/action';
import { ConfigUploadCkeditor } from 'src/app/shared/modules/question-banks/Interfaces/config-upload-ckeditor';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { QuestionBankDialogComponent } from '../../Components/question-bank-dialog/question-bank-dialog.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ViewExamFinishComponent } from '../view-exam-finish/view-exam-finish.component';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';

@Component({
  selector: 'app-edit-testkit',
  templateUrl: './edit-testkit.component.html',
  styleUrls: ['./edit-testkit.component.scss']
})
export class EditTestkitComponent implements OnInit {

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  @ViewChild("file") file: ElementRef;

  routeSub: Subscription;
  subjectId;
  examId;
  testKitId;
  testId;
  subjectExamId;
  gradeId: number;
  dialogConfirm: DialogConfirm;
  matrixId?: number = null;

  private testEnpoint: TestEnpoint;
  private testKitEnpoint: TestKitEnpoint;
  private gradeEnpoit: GradeEndpoint;
  private subjectEndpoint: SubjectEndpoint;
  private examApi: ExamEnpoint;
  private questionConverterEndpoint: QuestionConverterEndpoint;
  private testKitBankEnpoint: TestKitBankEnpoint;

  //dữ liệu truyền đi
  question: QuestionModel = null;
  configUpload: ConfigUploadCkeditor;
  totalScore = 1;

  //data questionModel
  dataQuestionModel: QuestionModel[] = [];
  questions: QuestionModel[] = [];
  allComplete: boolean = false;
  //index câu hỏi đã chọn
  index;
  // index của câu hỏi con đã chọn
  childIndex;
  grade?: GradeInfo = {
    gradeId: 0,
    gradeName: '',
    orderNumber: 0
  };
  subject?: SubjectInfo = {
    id: 0,
    name: '',
    orderNumber: 0
  };
  // Danh sách câu hỏi trả về sau convert file
  dataResponse: UploadTestFilesReponse;
  //danh sách câu hỏi đc thêm từ file
  data: QuestionExam[] = [];

  // lưu lại danh sách các câu hỏi theo từng đề
  //hiên thị
  displayedColumns: string[] = ['stt', 'content', 'score', 'delete']
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  //bộ đề
  testKit: TestKit;
  updateTestKit: UpdateTestKit;
  //danh sách đề
  dataTest: Test[] = [];
  isPublished: boolean = true;
  createType: string;
  listTest = [
    { id: 1, name: "Đề số 1", data: [], filePath: '', action: 'Add' }
  ]

  disableAddNewTest: boolean = false;
  // Chỉ đề thi mới tạo thì được up file, không cho up file câu hỏi lên đề thi đã được tạo trước đó
  disableUploadFile: boolean = true;
  testForm = this.fb.group({
    testName: new FormControl('', [Validators.required, Validators.maxLength(255)]),

  })
  private dialogQuestionBankRef: MatDialogRef<QuestionBankDialogComponent>
  public testCtrl: FormControl = new FormControl();
  private dialogQuestionBankList: MatDialogRef<ViewExamFinishComponent>;
  totalCountCurrentTest: number = 0;
  totalScoreCurrentTest: number = 0;
  constructor(private _snackBar: MatSnackBar, private fb: FormBuilder, public dialog: MatDialog, private questionFilleService: QuestionFileService,
    private router: Router, private route: ActivatedRoute, private changeDetectorRefs: ChangeDetectorRef, private _authService: AuthService, private configService: AppConfigService) {
    this.gradeEnpoit = new GradeEndpoint({ baseUrl: 'http://localhost:65000' });
    this.subjectEndpoint = new SubjectEndpoint({ baseUrl: 'http://localhost:65000' });
    this.testEnpoint = new TestEnpoint();
    this.testKitEnpoint = new TestKitEnpoint();
    this.examApi = new ExamEnpoint();
    this.questionConverterEndpoint = new QuestionConverterEndpoint();
    this.testKitBankEnpoint = new TestKitBankEnpoint();

    this.routeSub = this.route.params.subscribe(params => {
      this.examId = + params['id']
    });
    this.route.queryParams.subscribe(params => {
      this.gradeId = +params['gradeId'];

      this.subjectId = +params['subjectId'];

      this.subjectExamId = +params['examSubjectId'];

      this.testKitId = +params['testKitId'];

      this.testId = +params['testId'];
      this.createType = params['type'];
    });

    let baseUrl = CLS.getConfig().apiBaseUrls.fileServer ?? CLS.getConfig().apiBaseUrl;
    baseUrl += '/api';

    this.configUpload = {
      portalId: configService.getConfig().unit.portalId,
      token: _authService.getToken(),
      uploadUrl: baseUrl + "/file/upload"
    }
  }

  get name() { return this.testForm.get('testName'); }

  ngOnInit(): void {
    this.examApi.getExamStatus(this.examId)
      .then(res => {
        this.isPublished = res == 'PUB';
        if (this.isPublished) {
          this.backToListTestKit();
        }
      })
      .catch();

    this.testKitEnpoint.getTestKit(this.testKitId)
      .then(res => {
        if (res.used > 0) {
          this.backToListTestKit();
        } else {
          this.testForm.get('testName').setValue(res.name);
        }
      })

    //lấy thông tin khối
    this.gradeEnpoit.getGradeInfo(this.gradeId).then(res => {
      if (res) {
        this.grade = res;
      }
    })

    //lây thông tin môn
    this.subjectEndpoint.getSubjectInfo(this.subjectId).then(res => {
      if (res) {
        this.subject = res;
      }
    })

    this.testCtrl.setValue(this.listTest[0].id);

    this.getQuestionData();
  }

  ngAfterViewInit() {

  }

  addNewTest() {
    if (this.listTest.length == 3) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Bộ đề được giới hạn tối đa 3 đề thi!',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });

      return;
    }

    if (this.disableAddNewTest == true) {
      return;
    }
    this.disableAddNewTest = true

    setTimeout(() => {
      this.disableAddNewTest = false;
    }, 3000);

    let currentTotalTest = this.listTest.length;
    let newTestId = currentTotalTest + 1;
    this.listTest.push({ id: newTestId, name: "Đề số " + newTestId, data: [], filePath: '', action: 'Add' });
    this.testCtrl.setValue(newTestId);
    this.disableUploadFile = false;
    this.data = [];
    this.question = null;
    this.setAll(false);

    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
      data: "Đã thêm đề mới Đề số " + newTestId + ". Thêm câu hỏi vào đề thi để hoàn thành việc tạo đề",
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  onChangeTestSelected(id) {
    this.setAll(false);
    let test = this.listTest.find(x => x.id == id);
    this.data = test.data;
    this.countQuestionTest();
    // Nếu chưa lấy danh sách câu hỏi của đề thì lấy
    if (this.data.length == 0) {
      if (test.filePath != '') {
        this.getFileTestData(test.filePath, test.id);
      }
    }

    this.question = null;

    this.disableUploadFile = test.action == Action.EditAll;
  }

  //open file 
  openFile() {
    if (this.isPublished) {
      return;
    }
    this.file.nativeElement.click();
  }

  filesSelectedOnChange(files: File[]) {
    if (this.isPublished) {
      return;
    }
    if (this.disableUploadFile) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Đề này đã lưu danh sách câu hỏi. Chỉ được phép up file cho đề mới tạo!',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (files.length != 0) {
      this.questionConverterEndpoint.uploadTestFiles(files, null, ServiceName.EXAM)
        .then(res => {
          this.file.nativeElement.value = "";
          this.dataResponse = res;
          this.data = res[0].questions;
          this.countQuestionTest();
          let currentTestSelected = this.testCtrl.value;

          this.listTest.map(test => {
            if (test.id == currentTestSelected) {
              test.data = this.data;
            }

            return test;
          });
        })
        .catch((err: ErrorResponse) => {
          this.file.nativeElement.value = "";
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
    }
  }

  submitForm() {
    if (this.testForm.invalid) {
      //sai
    }
    else {
      // valid xem các đề thi đã có danh sách câu hỏi chưa
      let errorDetail: string = '';
      this.listTest.forEach((e, i) => {
        if (e.data.length == 0) {
          errorDetail += "Đề số " + (i + 1) + ", ";
        }
      });

      if (errorDetail != '') {
        errorDetail = 'Bạn cần tải lên danh sách câu hỏi cho: ' + errorDetail.substring(0, errorDetail.length - 2);
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: errorDetail,
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        return;
      }
      // ---------------
      this.updateTestKit = {
        id: this.testKitId,
        name: this.testForm.get('testName').value
      }
      this.testKitEnpoint.updateTestkit(this.updateTestKit).then(res => {
        if (res) {
          // Update đề thi
          this.createTest();
        } else {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Thêm thất bại!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      })
        .then(() => {
          this._snackBar.openFromComponent(SuccessSnackBarComponent, {
            data: 'Cập nhật bộ đề thành công!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.backToListTestKit();
        })
        .catch((err: ErrorResponse) => {
          this.file.nativeElement.value = "";
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
    }
  }

  createTest(): void {

    this.listTest.forEach(x => {

      let dataParsing = this.createQuestionRequest(x.data);

      console.log(`dataParsing999: `, dataParsing);

      if (x.action == 'Add') {
        this.dataTest.push(
          {
            name: x.name, gradeId: this.gradeId, subjectId: this.subjectId,
            testKitId: this.testKitId, examId: this.examId, questions: dataParsing
          }
        );
      } else {
        this.dataTest.push(
          {
            id: x.id, name: x.name, gradeId: this.gradeId, subjectId: this.subjectId,
            testKitId: this.testKitId, examId: this.examId, questions: dataParsing
          }
        );
      }
    });

    this.dataTest.forEach(object => {
      if (object.id == null) {
        this.testEnpoint.createTest(object).then(res => {
          if (res) {
            console.log("okok")
          }
          else {
            console.log("fail")
          }
        })
          .catch((err: ErrorResponse) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: err.errorDetail,
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
      }
      else {
        this.testEnpoint.updateTest(object).then(res => {
          if (res) {
            console.log("okok")
          }
          else {
            console.log("fail")
          }
        })
          .catch((err: ErrorResponse) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: err.errorDetail,
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
      }
    });
  }

  //lấy danh sách câu hỏi từ file
  getQuestionData() {
    this.testEnpoint.getTests(this.testKitId).then(res => {
      if (res.length > 0) {
        this.listTest = [];
        // khởi tạo danh sách các đề thi
        res.forEach(test => {
          this.listTest.push({ id: test.id, name: test.name, data: [], filePath: test.urlJsonTest, action: 'EDI' });
        });

        for (var i = res.length - 1; i >= 0; i--) {
          let filePath = res[i].urlJsonTest;
          this.getFileTestData(filePath, res[i].id, true, res[0].id);
        }
      }
    });
  }

  getFileTestData(filePath: string, testId: number, setCurrent?: boolean, firstId?: number) {
    this.testEnpoint.getFileTest(filePath).then(
      res => {
        //console.log(`getFileTestData: 999 `, res.questions);
        //this.data = res.questions.filter(x => x.type == 'SIG' || x.type == 'MTC');
        this.data = res.questions;

        this.listTest.map(test => {
          if (test.id == testId) {
            test.data = this.data;
          }

          return test;
        });

        // trong lần đầu lấy ds đề thi sẽ luôn set đề thi đâu tiên là đề thi đang được hiển thị
        if (setCurrent) {
          this.testCtrl.setValue(firstId);
          this.data = this.listTest.find(x => x.id == firstId)?.data;
        } else {
          //  trường hợp chọn lại từ dropdown thì theo đề được chọn
          this.testCtrl.setValue(testId);
        }
        this.countQuestionTest();
      }
    )
      .catch((err: ErrorResponse) => {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: err.errorDetail,
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
  }


  // Khởi tạo những câu hỏi đc thêm vào
  createQuestionRequest(data) {

    let i = 0;
    this.dataQuestionModel = [];
    data.forEach(obj => {
      i += 1;
      if (obj.questions != null && obj.format == FormatQuestion.group) {
        let questions: QuestionModel[] = [];
        obj.questions.forEach(obj => {
          questions.push(
            {
              id: obj.id,
              score: obj.score,
              content: obj.content,
              type: obj.type,
              level: obj.level,
              format: FormatQuestion.group,
              contentPanelId: 0,
              chapterId: 0,
              unitId: 0,
              questions: [],
              answers: obj.answers,
              action: obj.action
            });
        });

        this.dataQuestionModel.push(
          {
            id: obj.id,
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: questions,
            answers: [],
            action: obj.action
          });
      }
      else {
        this.dataQuestionModel.push(
          {
            id: obj.id,
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: [],
            answers: obj.answers,
            action: obj.action
          });
      }
    })

    //console.log(`this.dataQuestionModel:99: `, this.dataQuestionModel);
    return this.dataQuestionModel;
  }

  loadData(data) {
    this.data = [];
    this.data = [...this.data, ...data];

    this.changeDetectorRefs.detectChanges();
  }

  showStatisticQuestion(data): void {
    let dataQuestions: QuestionModel[] = [];
    data.forEach(obj => {
      if (obj.questions != null && obj.format == "GRO" && obj.action != Action.Delete) {
        let questions: QuestionModel[] = [];
        obj.questions.forEach(obj => {
          if (obj.action != Action.Delete) {
            questions.push(
              {
                score: obj.score,
                content: obj.content,
                type: obj.type,
                level: obj.level,
                format: 'GRP',
                contentPanelId: 0,
                chapterId: 0,
                unitId: 0,
                questions: [],
                answers: obj.answers
              });
          }
        });

        dataQuestions.push(
          {
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: questions,
            answers: []
          });
      }
      else if (obj.action != Action.Delete) {
        dataQuestions.push(
          {
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: [],
            answers: obj.answers
          });
      }
    })

    const dialogRef = this.dialog.open(DialogStatisticQuestionComponent, {
      width: '800px',
      height: 'auto',
      data: dataQuestions
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  delete(question, i, ic) {
    this.dialogConfirm = { title: 'XÓA CÂU HỎI', description: 'Bạn có chắc chắn muốn xóa câu hỏi này?' }
    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // là câu chùm
        if (i != undefined) {
          if (this.data[i].questions.filter(x => x.action != Action.Delete).length == 1) {
            // cầu chùm phải có ít nhất 1 câu con
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: "Câu chùm phải có ít nhất 1 câu con.",
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            return;
          } else {
            // cập nhật lại điểm câu chùm
            // cập nhật lại điểm câu chùm
            let childId = this.data[i].questions[ic].id;
            let parentId = this.data[i].id;
            this.updateScore(childId, 0, parentId);

            if (question.action == Action.Add) {
              this.data[i].questions.splice(ic, 1);
            } else {
              question.action = Action.Delete;
              this.data[i].questions[ic] = question;
            }
          }

          // reset câu hỏi đã chọn
          if (this.index == i) {
            this.question = null;
          }
        }
        else {
          let index = this.data.indexOf(question);
          if (index != -1) {
            if (question.action == Action.Add) {
              this.data.splice(index, 1);
            } else {
              question.action = Action.Delete;
            }
          }

          // reset câu hỏi đã chọn
          if (this.index == index) {
            this.question = null;
          }
        }

        this.loadData(this.data);

        let currentTestSelected = this.testCtrl.value;

        this.listTest.map(test => {
          if (test.id == currentTestSelected) {
            test.data = this.data;
          }

          return test;
        });
        // thống kê lại số lượng câu hỏi, tổng điểm
        this.countQuestionTest();
      }
    });
  }

  chooseQuestion(item, i, ic) {
    this.index = i;
    this.question = item;
    if (ic == undefined) {
      this.childIndex = 1;
    } else {
      this.childIndex = ic + 1;
    }

    console.log("item", item)
  }

  updateQuestion(question) {
    let oldQuestion = this.data[this.index];

    // Lấy lại thông tin điểm
    if (oldQuestion.format == FormatQuestion.single) {
      question.score = oldQuestion.score;
    }
    else if (oldQuestion.format == FormatQuestion.group) {
      question.score = oldQuestion.score;
      //duyệt câu con
      question.questions.map(qs => {
        // không cho phép xóa, nên chắc chắn phải tìm ra được câu hỏi
        let oldChild = oldQuestion.questions.find(x => x.id == qs.id);
        if (oldChild) {
          qs.score = oldChild.score;
        }
        return qs;
      });
    }

    this.data[this.index] = question;
    this.loadData(this.data);

    let currentTestSelected = this.testCtrl.value;

    this.listTest.map(test => {
      if (test.id == currentTestSelected) {
        test.data = this.data;
      }

      return test;
    });

    console.log(`this.listTest:999999: `, this.listTest);
  }

  updateScore(id, score, parentId) {

    let currentTestId = this.testCtrl.value;
    //this.data: là data tạm để hiển thị
    //this.listTest[{..data}]: data trong listTest là data lưu trữ lại của từng đề thi

    this.listTest.map(test => {
      if (test.id == currentTestId) {
        //test.data = this.data
        test.data.map((qs, i) => {
          // Cập nhật điểm câu con của câu chùm
          if (parentId != null) {
            if (qs.id == parentId) {
              // lấy câu chùm của data tạm
              let question = this.data.find(x => x.id == parentId);
              let totalQuestionGroupScore = 0;

              // cập nhật điểm cho câu con của câu chùm
              question.questions.map((child, j) => {
                if (child.id == id) {
                  if (qs.questions[j].score != score) {
                    // Đang cập nhật điểm, nếu trc đó có cập nhật content thì action là EDIT
                    if (qs.questions[j].action == Action.EditContent) {
                      qs.questions[j].action = Action.EditAll;
                    } if (qs.action != Action.EditAll) {
                      qs.questions[j].action = Action.EditScore;
                    }
                    qs.questions[j].score = score;
                  }
                }

                totalQuestionGroupScore += child.score;

                return child;
              });

              question.score = totalQuestionGroupScore;
              qs.score = totalQuestionGroupScore;
            }
          } else {
            if (qs.id == id) {
              if (qs.score != score) {
                // Đang cập nhật điểm, nếu trc đó có cập nhật content thì action là EDIT
                if (qs.action == Action.EditContent) {
                  qs.action = Action.EditAll;
                } if (qs.action != Action.EditAll) {
                  qs.action = Action.EditScore;
                }
                qs.score = score;
              }
            }
          }

          return qs;
        });
        // console.log(`this.listTest999: `, test);
        // console.log(`this.data999: `, this.data);
      }

      return test;
    });

    this.countQuestionTest();
  }

  save() {
    //lưu danh sách đề thi
    this.submitForm();
    //router sang bên setting môn thi
    //this.router.navigate(["dashboard/examination-manager/setting-subject/", this.subjectId, this.examId]);
  }

  cancel() {
    this.backToListTestKit();
  }

  deleteSelected() {
    this.dialogConfirm = { title: 'XÓA CÂU HỎI', description: 'Bạn có chắc chắn muốn xóa các câu hỏi đã chọn?' }
    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // lọc bỏ những câu hỏi mới thêm
        this.data = this.data.filter(x => !(x.isChecked && x.action == Action.Add));
        this.data.forEach((qs, i) => {
          if (qs.isChecked) {
            qs.action = Action.Delete;
          }
          if (qs.format == FormatQuestion.group) {
            qs.questions.map((x, ic) => {
              if (x.isChecked) {
                // cập nhật lại điểm câu chùm
                this.updateScore(x.id, 0, qs.id);
              }
            });
            // lọc bỏ những câu hỏi mới thêm
            qs.questions = qs.questions.filter(x => !(x.isChecked && x.action == Action.Add));
            //duyệt câu con
            qs.questions.map(cqs => {

              if (cqs.isChecked) {
                cqs.action = Action.Delete;
              }
              return cqs;
            });
          }
          // reset câu hỏi đã chọn
          if (this.index == i) {
            this.question = null;
          }
        });
        this.countQuestionTest();
        this.loadData(this.data);

        let currentTestSelected = this.testCtrl.value;

        this.listTest.map(test => {
          if (test.id == currentTestSelected) {
            test.data = this.data;
          }

          return test;
        });

      }
    });

  }

  someComplete() {
    if (this.data == undefined) return false;

    let somchecked = false;
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].isChecked) {
        somchecked = true;
        break;
      }
      if (this.data[i].questions != null) {
        for (let j = 0; j < this.data[i].questions.length; j++) {
          if (this.data[i].questions[j].isChecked) {
            somchecked = true;
            break;
          }
        }
      }
    }
    return somchecked;
  }

  updateAllComplete() {
    if (this.data == undefined) return false;
    let allChecked = true;
    for (let i = 0; i < this.data.length; i++) {
      if (!this.data[i].isChecked) {
        allChecked = false;
        break;
      }
      if (this.data[i].questions != null) {
        for (let j = 0; j < this.data[i].questions.length; j++) {
          if (!this.data[i].questions[j].isChecked) {
            allChecked = false;
            break;
          }
        }
      }
    }
    this.allComplete = allChecked;
  }

  setAll(completed: boolean): void {
    this.allComplete = completed;
    if (this.data.length != 0) {
      this.data.map((question) => {
        if (question.format == FormatQuestion.group) {
          question.isChecked = completed;
          question.questions.forEach((child) => {
            child.isChecked = completed
          });
        } else {
          question.isChecked = completed
        }
      });
    }
  }

  openUpdateScoreDialog() {
    console.log(`this.questions: `, this.data);
    if (!this.someComplete()) {
      alert("Không có câu hỏi nào được chọn!");
      return;
    }
    const dialogRef = this.dialog.open(UpdateScoreQuetionsDialogComponent, {
      width: '377px',
      height: '234px',
      data: this.totalScore
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      if (result) {
        this.updateScoreQuestions(result);
      }
    });
  }

  updateScoreQuestions(score: number): void {
    let currentTestId = this.testCtrl.value;
    //this.data: là data tạm để hiển thị
    //this.listTest[{..data}]: data trong listTest là data lưu trữ lại của từng đề thi
    this.data.map((question) => {

      if (question.isChecked) {
        if (question.format != FormatQuestion.group) {
          question.score = score;
        }
        else if (question.format == FormatQuestion.group) {
          let totalQuestionGroupScore = 0;
          question.questions.forEach((child) => {
            child.score = score;
            totalQuestionGroupScore += score;
          });
          question.score = totalQuestionGroupScore;
        }
        question.isChecked = false;
      }

      return question;
    });

    this.listTest.map(test => {
      if (test.id == currentTestId) {
        //test.data = this.data
        test.data.map((qs, i) => {
          let question = this.data[i];
          // --- CÂU ĐƠN
          if (question.format != FormatQuestion.group) {
            if (qs.score != question.score) {
              // Đang cập nhật điểm, nếu trc đó có cập nhật content thì action là EDIT
              if (qs.action == 'CON') {
                qs.action = 'EDI';
              } if (qs.action != 'EDI') {
                qs.action = 'SCO';
              }
              qs.score = question.score;
            }
          }
          // --- CÂU CHÙM
          else if (question.format == FormatQuestion.group) {
            let totalQuestionGroupScore = 0;

            // cập nhật điểm cho câu con của câu chùm
            question.questions.map((child, j) => {
              totalQuestionGroupScore += child.score;
              // nếu câu con thay đổi Điểm
              if (qs.questions[j].score != child.score) {
                // Đang cập nhật điểm, nếu trc đó có cập nhật content thì action là EDIT
                if (qs.questions[j].action == 'CON') {
                  qs.questions[j].action = 'EDI';
                } if (qs.action != 'EDI') {
                  qs.questions[j].action = 'SCO';
                }
                qs.questions[j].score = child.score;
              }

              return child;
            });

            question.score = totalQuestionGroupScore;
            qs.score = totalQuestionGroupScore;
          }

          return qs;
        })
      }

      return test;
    });
    this.allComplete = false;
    this.setAll(false);
    this.countQuestionTest();
  }

  backToListTestKit() {
    this.router.navigate(['dashboard/examination-manager/exams/' + this.examId + '/steps/testkits'],
      { queryParams: { gradeId: this.gradeId, subjectId: this.subjectId, examSubjectId: this.subjectExamId, testKitId: this.testKitId } });
  }

  openQuestionBankDialog() {
    let currentTestSelected = this.testCtrl.value;
    let currentTest = this.listTest.find(x => x.id == currentTestSelected);
    let selectedQuestionIds = currentTest.data.map(x => x.id);

    this.dialogQuestionBankRef = this.dialog.open(QuestionBankDialogComponent, {
      width: '80vw',
      height: 'auto',
      data: selectedQuestionIds
    });

    this.dialogQuestionBankRef.afterClosed().subscribe((result: any) => {
      if (result != "cancel") {
        // lọc ra những questions đã chọn, vì chọn selectAll bên NHCH thì sẽ thêm vào luôn những question đã chọn
        if (selectedQuestionIds) {
          result = result.filter(x => selectedQuestionIds.indexOf(x.id) == -1);
        }
        result.map(x => {
          this.data.push(x);
        })

        let currentTestSelected = this.testCtrl.value;

        this.listTest.map(test => {
          if (test.id == currentTestSelected) {
            test.data = this.data;
          }

          return test;
        });
        this.countQuestionTest();
      }
    });
  }
  deleteTest() {
    let currentTestSelectedId = this.testCtrl.value;
    let test = this.listTest.find(x => x.id == currentTestSelectedId);

    this.dialogConfirm = { title: 'XÓA ĐỀ THI', description: 'Bạn có chắc chắn muốn xóa đề thi "' + test.name + '"?' }
    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.data = this.data.filter(x => !x.isChecked);

        this.listTest = this.listTest.filter(x => x.id != test.id);
        this.listTest.map(t => {

          if (t.id > test.id && t.action == Action.Add) {
            t.id--;
            t.name = "Đề số " + t.id;
          }
        })

        let currentTest = this.listTest[0];
        this.testCtrl.setValue(currentTest.id);
        this.data = currentTest.data;
        this.countQuestionTest();
        this.question = null;
        this.setAll(false);
      }
    });
  }

  hanleGetListQuestionBank() {
    this.showDialogListQuestion(this.listTest, "Xem trước bộ đề thi");
  }

  // show dialog
  showDialogListQuestion(listTest, testkitName) {
    this.dialogQuestionBankList = this.dialog.open(ViewExamFinishComponent, {
      width: '70vw',
      height: 'auto',
      data: {
        testkitName,
        listTest,
        preview: true,
        selectedTestId: this.testCtrl.value
      }
    });
  }
  countQuestionTest() {
    this.totalCountCurrentTest = this.data.filter(x => x.action != Action.Delete).length;
    this.totalScoreCurrentTest = 0;
    this.data.forEach(obj => {
      if (obj.action != Action.Delete) {
        this.totalScoreCurrentTest = Math.round((this.totalScoreCurrentTest + obj.score) * 100) / 100;
      }
    })
  }
}

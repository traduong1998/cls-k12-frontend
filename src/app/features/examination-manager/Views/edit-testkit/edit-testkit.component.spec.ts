import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTestkitComponent } from './edit-testkit.component';

describe('EditTestkitComponent', () => {
  let component: EditTestkitComponent;
  let fixture: ComponentFixture<EditTestkitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditTestkitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTestkitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
declare var CKEDITOR: any;
CKEDITOR.plugins.addExternal(
  'insertaudio',
  window.location.origin + '/assets/ckeditor/plugins/InsertAudio/',
  'InsertAudio.js');
CKEDITOR.plugins.addExternal(
  'insertimage',
  window.location.origin + '/assets/ckeditor/plugins/InsertImage/',
  'InsertImage.js');
CKEDITOR.plugins.addExternal(
  'insertvideo',
  window.location.origin + '/assets/ckeditor/plugins/InsertVideo/',
  'InsertVideo.js');
  
  CKEDITOR.plugins.addExternal(
    'createunderline',
    window.location.origin + '/assets/ckeditor/plugins/underline/',
    'plugin.js');
      
  CKEDITOR.plugins.addExternal(
    'fillblank1',
    window.location.origin + '/assets/ckeditor/plugins/fillblank1/',
    'plugin.js');
    CKEDITOR.plugins.addExternal(
      'fillblank2',
      window.location.origin + '/assets/ckeditor/plugins/fillblank2/',
      'plugin.js');
      CKEDITOR.plugins.addExternal('ckeditor_wiris',  window.location.origin + '/assets/ckeditor/plugins/mathtype/', 'plugin.js');



@Component({
  selector: 'app-view-test',
  templateUrl: './view-test.component.html',
  styleUrls: ['./view-test.component.scss']
})
export class ViewTestComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

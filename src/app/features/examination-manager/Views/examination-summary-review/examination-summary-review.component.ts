
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ExamEnpoint } from 'sdk/cls-k12-sdk-js/src/services/exam/endpoints/exam-enpoint';
import { ExamDetail, SubjectEndpoint } from 'cls-k12-sdk-js/src';
import { ToastComponent } from '../../Components/toast/toast.component';
import { AuthService } from 'src/app/core/services';
import { DialogConfirm } from '../../interfaces/dialog-confirm';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogConfirmComponent } from '../../Components/dialog-confirm/dialog-confirm.component';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { ExamSubjectSettingReponse } from 'sdk/cls-k12-sdk-js/src/services/exam/response/exam-subject-setting-response';
import { ExaminationSubjectSettingDialogComponent } from '../../Components/examination-subject-setting-dialog/examination-subject-setting-dialog.component';
import { TestEnpoint } from 'sdk/cls-k12-sdk-js/src/services/test/enpoint/test-enpoint';
import { ViewExamFinishComponent } from '../view-exam-finish/view-exam-finish.component';

@Component({
  selector: 'app-examination-summary-review',
  templateUrl: './examination-summary-review.component.html',
  styleUrls: ['./examination-summary-review.component.scss']
})
export class ExaminationSummaryReviewComponent implements OnInit {

  routeSub: Subscription;
  //mã kỳ thi
  examId: number;
  step;
  //info exam

  //authen
  authService

  //quyền
  subjectOfExam = [];

  private dialogQuestionBankList: MatDialogRef<ViewExamFinishComponent>

  examSubjectSettingSelected: ExamSubjectSettingReponse;
  dataInfoExam: ExamDetail = null;
  dialogConfirm: DialogConfirm;
  // Endpoint
  private testEnpoint: TestEnpoint;
  displayedColumns: string[] = ['STT', 'subject', 'startTime', 'endTime', 'testKit', 'config'];
  isPublished: boolean = true;
  private examApi: ExamEnpoint;
  subjectEndpoint: SubjectEndpoint;
  constructor(private route: ActivatedRoute, private router: Router, private _snackBar: MatSnackBar, private _authService: AuthService, private dialog: MatDialog,) {
    var a = this._authService.getTokenInfo();
    this.testEnpoint = new TestEnpoint();
    this.examApi = new ExamEnpoint();
    this.subjectEndpoint = new SubjectEndpoint({ baseUrl: '' });
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = +params['id'];

    });
  }

  ngOnInit(): void {
    //get data from api
    this.examApi.getInfoExam(this.examId)
      .then(res => {
        this.dataInfoExam = res;
        //bindding data
        if (this.dataInfoExam == null || this.dataInfoExam.status != 'WAI') {
          this.backListExams();
        }
        if (this.dataInfoExam != null) {
          //đô dữ liệu môn thi
          this.dataInfoExam.examSubjects.forEach(element => {
            this.subjectOfExam.push({
              id: element.subjectId,
              name: null,
              testKitName: element.testKitName,
              testKitId: element.testKitId,
              startDate: element.subjectStartDate,
              endDate: element.subjectEndDate,
            });
          });

          let subjectIds = this.subjectOfExam.map((x) => {
            return x.id
          });
          this.subjectEndpoint.getSubjectOptionsByIds(subjectIds)
            .then(res => {
              this.subjectOfExam = this.subjectOfExam.map((sj) => {
                let subject = res.find(x => x.id == sj.id);
                if (subject != undefined) {
                  sj.name = subject.name;
                }
                return sj;
              });
            });

        }
      })
  }

  approveExam() {
    this.dialogConfirm = {
      title: 'DUYỆT KỲ THI',
      description: 'Bạn có chắc chắn muốn DUYỆT kỳ thi này?',
      type: 'confirm',
      labelYes: 'Duyệt',
      labelNo: 'Hủy'
    }
    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.examApi.approveExam(this.examId, true)
          .then(
            res => {
              if (res) {
                this._snackBar.openFromComponent(ToastComponent, {
                  data: ['Duyệt thành công'],
                  duration: 1000,
                  panelClass: ['green-snackbar'],
                  horizontalPosition: 'center',
                  verticalPosition: 'top',
                });
                this.backListExams();
              } else {
                this._snackBar.openFromComponent(ToastComponent, {
                  data: ['Duyệt thất bại. Vui lòng thử lại sau.'],
                  duration: 1000,
                  panelClass: ['red-snackbar'],
                  horizontalPosition: 'center',
                  verticalPosition: 'top',
                });
              }
            }
          )
          .catch((err: ErrorResponse) => {
            this._snackBar.open(err.errorDetail, 'Ok', {
              duration: 5000,
              panelClass: ['red-snackbar'],
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          });
      }
    });
  }

  rejectExam() {
    this.dialogConfirm = {
      title: 'TRẢ LẠI KỲ THI',
      description: 'Bạn có chắc chắn muốn TRẢ LẠI kỳ thi này?',
      labelYes: 'Trả lại',
      labelNo: 'Hủy'
    }

    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.examApi.approveExam(this.examId, false)
          .then(
            res => {
              if (res) {
                this._snackBar.openFromComponent(ToastComponent, {
                  data: ['Trả lại thành công'],
                  duration: 1000,
                  panelClass: ['green-snackbar'],
                  horizontalPosition: 'center',
                  verticalPosition: 'top',
                });
                this.backListExams();
              } else {
                this._snackBar.openFromComponent(ToastComponent, {
                  data: ['Trả lại thất bại. Vui lòng thử lại sau.'],
                  duration: 1000,
                  panelClass: ['red-snackbar'],
                  horizontalPosition: 'center',
                  verticalPosition: 'top',
                });
              }
            }
          )
          .catch((err: ErrorResponse) => {
            this._snackBar.open(err.errorDetail, 'Ok', {
              duration: 5000,
              panelClass: ['red-snackbar'],
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          });
      }
    });
  }


  backListExams() {
    this.router.navigate(['dashboard/examination-manager/exams/approval']);
  }

  settingSubject(subjectId) {
    console.log(`subjectId: `, subjectId)
    this.examApi.getExamSubjectSetting(this.examId, subjectId)
      .then(res => {

        let subject = this.subjectOfExam.find(x => x.id == subjectId);
        if (subject != null) {
          res.subjectName = subject.name;
        }
        this.dialog.open(ExaminationSubjectSettingDialogComponent, {
          width: '500px',
          height: 'auto',
          disableClose: true,
          data: res
        });
      }).catch(err => {
        console.log("err")
      })
  }

  // get list question bank
  hanleGetListQuestionBank(idTestkit,testkitName) {
    this.testEnpoint.getTestsView(idTestkit).then(res => {
      if (res) {
        this.showDialogListQuestion(res,testkitName);
      }
    }).catch(err => console.log(err))
  }

  // show dialog
  showDialogListQuestion(listTest,testkitName) {
    this.dialogQuestionBankList = this.dialog.open(ViewExamFinishComponent, {
      width: '70vw',
      height: 'auto',
      data: {
        testkitName,
        listTest
      }
    });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExaminationSummaryReviewComponent } from './examination-summary-review.component';

describe('ExaminationSummaryReviewComponent', () => {
  let component: ExaminationSummaryReviewComponent;
  let fixture: ComponentFixture<ExaminationSummaryReviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExaminationSummaryReviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExaminationSummaryReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

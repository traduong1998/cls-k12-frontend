import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ExamEnpoint } from 'cls-k12-sdk-js/src/services/exam/endpoints/exam-enpoint';
import { ExamDetail, ExamScope, ExamType, SubjectEndpoint } from 'cls-k12-sdk-js/src';
import { ToastComponent } from '../../Components/toast/toast.component';
import { ReplaySubject, Subject as RxSubject, Subject, Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/services';
import { ListSubjectOfGrade, SubjectsEndpoint } from 'cls-k12-sdk-js/src/services/lesson/endpoints/subjectsEndpoint';
import { DivisionEndpoint, GradeEndpoint } from 'cls-k12-sdk-js/src';
import { MatSelect } from '@angular/material/select';
import { take, takeUntil } from 'rxjs/operators';
import { GradeOption } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { StepHeaderControlExam } from 'src/app/features/learn/intefaces/stepHeaderControl';
import { SubjectExam } from '../../interfaces/subject-exam';
import { OptionSelect } from '../../interfaces/option-select';
import { SubjectOfGradeVM } from './interfaces/subject-of-grade-vm';
import { EditExamRequest, SubjectEditRequest } from 'sdk/cls-k12-sdk-js/src/services/exam/requests/edit-exam-request';
import { TranslateCacheSettings } from 'ngx-translate-cache';
import { AssessmentLevelGroupOption } from 'sdk/cls-k12-sdk-js/src/services/exam/models/assessmen-level-group';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SubjectResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/subjectResponses';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/manage/models/subject';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';
//import { EditExamRequest, SubjectEditRequest } from 'cls-k12-sdk-js/src/services/exam/requests/edit-exam-request';
@Component({
  selector: 'exam-edit-examination',
  templateUrl: './edit-examination.component.html',
  styleUrls: ['./edit-examination.component.scss']
})
export class EditExaminationComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  @ViewChild('picker') drpFromToDate;
  @ViewChild('dpFromDate') dpFromDate;
  @ViewChild('dpToDate') dpToDate;

  eventsSubject: Subject<string> = new Subject<string>();
  events$ = this.eventsSubject.asObservable();

  baseApiUrl = 'http://localhost:58910';
  routeSub: Subscription
  examId = 0;
  //sttep
  step = 1;
  //mảng data chứa môn thi
  dataInfoExam: ExamDetail;
  //biến check trạng thái của vùng validate
  checkValidate: boolean = false;
  //biến check đã chọn môn đó
  displayedColumns: string[] = ['subjectName', 'checked'];
  displayedColumnsListSubject: string[] = ['select', 'stt', 'subjectName', 'startDate', 'endDate', 'delete'];
  // danh sach mon

  //danh danh hình thức thi
  types: OptionSelect[] = [{ value: ExamType.Free, display: 'Tự do', checked: true }, { value: ExamType.Concentrated, display: 'Tập trung', checked: false }]
  typeChoose: ExamType = ExamType.Concentrated;
  //danh sách môn chọn ra đề thi 
  listSubjectWasChoose: SubjectExam[] = [];
  // danh sach hom de thi
  subjectOfExam: SubjectOfGradeVM[] = [];
  subjectOfExamClone: SubjectOfGradeVM[] = [];
  // danh sách Ids môn học trước khi chỉnh sửa
  subjectIdsOfExamBackup: number[] = [];

  stepHeaderControl: StepHeaderControlExam;

  //danh sách môn
  listSubject: SubjectOfGradeVM[] = [];
  //data exam requesst
  subjectIdRemove: string[] = [];
  today = new Date();

  timeSubject = this.fb.group({
    startTime: new FormControl(new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0), [Validators.required]),
    endTime: new FormControl(new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 0), [Validators.required])
  });

  get startTime() { return this.timeSubject.get('startTime'); }
  get endTime() { return this.timeSubject.get('endTime'); }

  infoForm = this.fb.group({
    name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    start: new FormControl(new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0), [Validators.required]),
    end: new FormControl(new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 0), [Validators.required]),
    gradeId: new FormControl(null, [Validators.required]),
  });

  examTypeCtrl: FormControl = new FormControl('', Validators.required);

  get name() { return this.infoForm.get('name'); }
  get start() { return this.infoForm.get('start'); }
  get end() { return this.infoForm.get('end'); }
  get gradeId() { return this.infoForm.get('gradeId'); }

  isInvalidExamDate() {
    let sDate = new Date(this.infoForm.get('start').value).setHours(0, 0, 0);
    let eDate = new Date(this.infoForm.get('end').value).setHours(23, 59, 0);
    return sDate > eDate;
  }
  authService;
  portalId: number;
  schoolId: number;
  listGrade: GradeOption[] = [];
  private gradeEndpoint: GradeEndpoint;
  private subjectsEndpoint: SubjectsEndpoint;
  private subjectEndpoint: SubjectEndpoint;
  private examEndPoint: ExamEnpoint;
  endpointDivision: DivisionEndpoint;
  ////
  isFirstLoadDivisions = true;
  isFirstLoadAssesment = true;
  isPublished = true;
  //status exam
  status: string;
  dataExamInfoClone: ExamDetail;
  constructor(private _authService: AuthService, private router: Router, private route: ActivatedRoute, private fb: FormBuilder, private _snackBar: MatSnackBar) {
    this.gradeEndpoint = new GradeEndpoint({ baseUrl: 'http://localhost:65000' });
    this.subjectsEndpoint = new SubjectsEndpoint({ baseUrl: 'http://localhost:65000' });
    this.subjectEndpoint = new SubjectEndpoint({ baseUrl: 'http://localhost:65000' });
    this.examEndPoint = new ExamEnpoint();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: 'http://localhost:65000' });
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = params['id'];
    });
    this.stepHeaderControl = {
      currentPage: 1,
      isCreatingTest: false,
      examId: +this.examId,
      step: 1
    }
  }

  /** list of divisions */
  protected grades: GradeOption[];
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  protected assessmentLevelGroups: AssessmentLevelGroupOption[];
  public assessmentLevelGroupCtrl: FormControl = new FormControl();
  public assessmentLevelGroupFilterCtrl: FormControl = new FormControl();
  public filteredAssessmentLevelGroups: ReplaySubject<AssessmentLevelGroupOption[]> = new ReplaySubject<AssessmentLevelGroupOption[]>(1);
  @ViewChild('singleSelectAssessmentLevelGroup', { static: true }) singleSelectAssessmentLevelGroup: MatSelect;


  protected _onDestroy = new RxSubject<void>();

  ngOnInit(): void {
    // nhân lại status
    this.status = this.route.snapshot.queryParamMap.get("status");

    //get infor token
    this.authService = this._authService.getTokenInfo();
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = +params['id'];
    });

    //get data from api
    this.examEndPoint.getInfoExam(this.examId)
      .then(res => {

        this.infoForm.get('gradeId').setValue(res.gradeId);
        this.dataInfoExam = res;

        //bindding data
        if (this.dataInfoExam != null) {
          this.name.setValue(this.dataInfoExam.name);
          this.end.setValue(this.dataInfoExam.endDate);
          this.start.setValue(this.dataInfoExam.startDate);
          this.typeChoose = this.dataInfoExam.type;
          this.examTypeCtrl.setValue(this.dataInfoExam.type);
          this.isPublished = this.dataInfoExam.status == 'PUB';

          this.examEndPoint.getAssessmenLevelGroupOptions()
            .then(res => {
              this.isFirstLoadAssesment = false;
              this.assessmentLevelGroups = res;
              this.filteredAssessmentLevelGroups.next(this.assessmentLevelGroups.slice());
              this.assessmentLevelGroupCtrl.setValue(this.dataInfoExam.assessmentLevelGroupId);
            })
            .catch();

          //đô dữ liệu môn thi
          this.dataInfoExam.examSubjects.forEach(element => {
            this.subjectOfExam.push({
              id: element.subjectId,
              name: null,
              gradeId: this.gradeId.value,
              startDate: new Date(element.subjectStartDate),
              endDate: new Date(element.subjectEndDate),
              orderNumber: null,
              checked: false
            });
            this.subjectIdsOfExamBackup.push(element.subjectId);
          });

          let subjectIds = this.subjectOfExam.map((x) => {
            return x.id
          });

          this.subjectEndpoint.getSubjectOptionsByIds(subjectIds)
            .then(res => {
              this.subjectOfExam = this.subjectOfExam.map((sj) => {
                let subject = res.find(x => x.id == sj.id);
                if (subject != undefined) {
                  sj.name = subject.name;
                }

                return sj;
              });
              // Lưu lại thông tin chung của kỳ thi và danh sách thiết lập các môn thi
              this.subjectOfExamClone = this.subjectOfExam.map(x => ({ ...x }));// clone array
              this.dataExamInfoClone = { ...this.dataInfoExam }
            });

          console.log("data exx", this.dataInfoExam);

          this.subjectOfExam = this.reloadArray(this.subjectOfExam);
        }

        return this.gradeEndpoint.getGradeOptions(this.schoolId);
      })
      .then(res => {
        this.isFirstLoadDivisions = false;
        // lọc chỉ lấy 1 khối của kỳ thi
        this.grades = res.filter(x => x.id == this.gradeId.value);
        this.filteredGrades.next(this.grades.slice());

        return this.subjectsEndpoint.getSubjectByGradeIdOptions(this.gradeId.value);
      })
      .then((res: SubjectOption[]) => {
        res.forEach(element => {
          let subjectOfGrade: SubjectResponses = {
            id: element.id,
            name: element.name
          }
          this.listSubject.push(SubjectOfGradeVM.From(subjectOfGrade));
        });

        this.listSubject = this.reloadArray(this.listSubject);
      }).then(() => {
        this.listSubject.forEach(item => {
          let subject = this.subjectOfExam.find(x => x.id == item.id);
          if (subject != null) {
            this.listSubject.map(x => {
              if (x.id == subject.id) {
                x.chose = true;
                return x;
              } else {
                return x;
              }
            })
          }
        })
      });

    this.portalId = this.authService.portalId;
    this.schoolId = this.authService.schoolId;
    // get 
    console.log("authen", this.authService);

    // listen for search field value changes
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });

    // listen for search field value changes
    this.assessmentLevelGroupCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterAssessmentLevelGroups();
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  hasChangeData(dataExamInfoClone: ExamDetail, dataExamInfoRequest: EditExamRequest) {
    let objBeforeEdit = {
      name: dataExamInfoClone.name,
      assessmentLevelGroupId: dataExamInfoClone.assessmentLevelGroupId,
      scope: dataExamInfoClone.scope,
      type: dataExamInfoClone.type,
      startDate: new Date(dataExamInfoClone.startDate),
      endDate: new Date(dataExamInfoClone.endDate)
    }
    let objAfterEdit = {
      name: dataExamInfoRequest.name,
      assessmentLevelGroupId: dataExamInfoRequest.assessmentLevelGroupId,
      scope: dataExamInfoRequest.scope,
      type: dataExamInfoRequest.type,
      startDate: dataExamInfoRequest.startDate,
      endDate: dataExamInfoRequest.endDate
    }

    if (!compareTwoObject(objBeforeEdit, objAfterEdit)) {
      return true;
    }

    return !compareTwoObject(this.subjectOfExam, this.subjectOfExamClone)
  }

  submitForm() {
    if (this.isPublished) {
      //next step
      this.step = 2;
      this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "choose-test"], { queryParams: { gradeId: this.gradeId.value } })
    }
    else {
      this.infoForm.markAllAsTouched();
      this.infoForm.get('start').markAsTouched();
      this.infoForm.get('end').markAsTouched();

      if (this.isInvalidExamDate()) {
        return;
      }
      if (this.infoForm.invalid) {
        if (this.name.value == '') {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Bạn chưa nhập tên kỳ thi!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
        if (this.subjectOfExam) {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Bạn chưa chọn môn thi!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      }
      else {

        // chỉnh sửa thông tin kỳ thi
        let subjectRequest: SubjectEditRequest[] = [];
        this.subjectOfExam.forEach(element => {
          subjectRequest.push({
            subjectId: element.id,
            startDate: new Date(element.startDate),
            endDate: new Date(element.endDate),
            isDeleted: element.checked
          })
        });

        this.subjectIdsOfExamBackup.forEach(id => {
          if (this.subjectOfExam.findIndex(x => x.id == id) == -1) {
            // Đưa thêm lên các subject bị xóa khỏi list sau khi chỉnh sửa
            // isDeleted = true, startDate & endDate không quan trọng
            subjectRequest.push({
              subjectId: id,
              startDate: new Date(2021, 11, 11),
              endDate: new Date(2021, 12, 12),
              isDeleted: true
            })
          }
        })

        let sDate = new Date(this.start.value).setHours(0, 0, 0);
        let eDate = new Date(this.end.value).setHours(23, 59, 59);
        let dataExamInfoRequest: EditExamRequest = {
          id: this.examId,
          name: this.name.value,
          assessmentLevelGroupId: this.assessmentLevelGroupCtrl.value,
          scope: ExamScope.Private,
          type: this.typeChoose,
          startDate: new Date(sDate),
          endDate: new Date(eDate),
          examSubjects: subjectRequest
        }
        
        if (this.hasChangeData(this.dataExamInfoClone, dataExamInfoRequest)) {
          this.examEndPoint.updateExam(this.examId, dataExamInfoRequest)
            .then(res => {
              if (res) {
                // this._snackBar.openFromComponent(ToastComponent, {
                //   data: ['Thành công'],
                //   duration: 1000,
                //   horizontalPosition: 'center',
                //   verticalPosition: 'top',
                // });
                //next step
                this.step = 2;
                this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "choose-test"], { queryParams: { gradeId: this.gradeId.value } })

              } else {
                this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                  data: 'Lỗi!',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            });
        } else {
          this.step = 2;
          this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "choose-test"], { queryParams: { gradeId: this.gradeId.value } })
        }

      }
    }

  }
  // get value of value exam
  changeType(type) {
    this.typeChoose = type;
  }
  nextPage() {
    this.submitForm();
  }

  chose() {
    //xuất ra danh sách môn đã chọn qua table
    this.listSubject.filter(x => x.checked == true).forEach((obj) => {
      let index = this.subjectOfExam.findIndex(o => o.id == obj.id);
      if (index == -1) {
        let subChose: SubjectOfGradeVM = { ...obj };
        subChose.startDate = new Date(new Date(this.start.value).setHours(0, 0, 0));
        subChose.endDate = new Date(new Date(this.end.value).setHours(23, 59, 0));
        subChose.checked = false;
        this.subjectOfExam.push(subChose);
      }
      obj.checked = false;
      obj.chose = true;
    });

    this.subjectOfExam = this.reloadArray(this.subjectOfExam);
  }

  chooseSubject(element) {
    if (this.isPublished)
      return;
    let subject = this.listSubject.find(x => x.id == element.id);
    if (subject != null && !subject.chose) {
      subject.checked = !subject.checked;
      this.listSubject = this.reloadArray(this.listSubject);
    }
  }

  deleteSubject(element) {

    let index = this.subjectOfExam.findIndex(x => x.id == element.id);
    if (index != -1) {
      this.subjectOfExam.splice(index, 1);
    }

    //xoas trang thai checked ben khoi
    let i = this.listSubject.findIndex(x => x.id == element.id);
    if (i != -1) {
      this.listSubject[i].chose = false;
    }

    this.listSubject = this.reloadArray(this.listSubject);
    this.subjectOfExam = this.reloadArray(this.subjectOfExam);
  }

  back() {
    let subjectBacked = this.subjectOfExam.filter(x => x.checked);
    if (!subjectBacked) {
      return;
    }

    this.subjectOfExam = this.subjectOfExam.filter(x => !x.checked);
    this.subjectOfExam = this.reloadArray(this.subjectOfExam);

    //tìm khối đã bị bỏ bên danh sách khối
    this.listSubject.forEach((sub) => {
      if (subjectBacked.find(y => y.id == sub.id)) {
        sub.chose = false;
      }
    })

    this.listSubject = this.reloadArray(this.listSubject);
  }

  private reloadArray<T>(model: T[]) {
    return [...model];
  }

  //change date of subject 
  changeStartDate(event, subjectId) {
    let sub = this.subjectOfExam.find(o => o.id == subjectId);
    sub.startDate = event.value;
  }

  changeEndDate(event, subjectId) {
    let sub = this.subjectOfExam.find(o => o.id == subjectId);
    sub.endDate = event.value;

  }

  changeCheck(event, row) {
    if (event.checked) {
      let index = this.subjectIdRemove.indexOf(row.subjectId);
      if (index == -1) {
        this.subjectIdRemove.push(row.subjectId);
      }
    } else {
      let index = this.subjectIdRemove.indexOf(row.subjectId);
      if (index != -1) {
        this.subjectIdRemove.splice(index, 1);
      }
    }
  }

  protected setInitialValue() {
    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        if (this.singleSelectDivision != undefined) {
          this.singleSelectDivision.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
        }
      });
    this.filteredAssessmentLevelGroups
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectAssessmentLevelGroup != undefined) {
          this.singleSelectAssessmentLevelGroup.compareWith = (a: AssessmentLevelGroupOption, b: AssessmentLevelGroupOption) => a && b && a.id === b.id;
        }
      });
  }

  protected filterDivisions() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterAssessmentLevelGroups() {
    if (!this.assessmentLevelGroups) {
      return;
    }
    // get the search keyword
    let search = this.assessmentLevelGroupFilterCtrl.value;
    if (!search) {
      this.filteredAssessmentLevelGroups.next(this.assessmentLevelGroups.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredAssessmentLevelGroups.next(
      this.assessmentLevelGroups.filter(assessment => assessment.name.toLowerCase().indexOf(search) > -1)
    );
  }


  onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.gradeEndpoint.getGradeOptions(this.schoolId)
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.grades = res;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onAssessmentLevelGroupSelectClicked() {
    if (this.isFirstLoadAssesment) {
      this.examEndPoint.getAssessmenLevelGroupOptions()
        .then(res => {
          this.isFirstLoadAssesment = false;
          this.assessmentLevelGroups = res;
          this.filteredAssessmentLevelGroups.next(this.assessmentLevelGroups.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onChangeDivisionSelected(id: any) {
    this.listSubject = [];

    // this.resetSchoolSelectCtrl();
    // this.resetGradeSelectCtrl();
    // this.resetGroupStudentSelectCtrl();
    this.subjectsEndpoint.getSubjectByGradeIdOptions(id).then(res => {
      if (res) {
        //this.listSubject.push()

        res.forEach(element => {
          let subjectOfGrade: SubjectResponses = {
            id: element.id,
            name: element.name
          }
          this.listSubject.push(SubjectOfGradeVM.From(subjectOfGrade));
        });

        this.listSubject = [...this.listSubject];

        this.subjectOfExam = [];
        this.subjectOfExam = this.reloadArray(this.subjectOfExam)
      }
    })
  }
  openDateRangePicker() {
    this.drpFromToDate.open();
  }
  openDpFromDate() {
    if (!this.isPublished) {
      this.dpFromDate.open();
    }

  }
  openDpToDate() {
    if (!this.isPublished) {
      this.dpToDate.open();
    }
  }
  backToContent() {
    this.router.navigate([`dashboard/examination-manager`]);
  }
  examToDateChange(e) {
    // set lại cuối ngày cho thời gian kết thúc kỳ thi
    this.end.value.setHours(23, 59, 0);
  }
}

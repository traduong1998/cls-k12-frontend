import { SubjectResponses } from "cls-k12-sdk-js/src/services/lesson/responses/subjectResponses";

export class SubjectOfGradeVM implements SubjectResponses {
    id: number;
    name: string;
    gradeId: number;
    orderNumber: number;

    checked?: boolean;
    startDate: Date;
    endDate: Date;
    chose?: boolean;
    
    static From(data: SubjectResponses) {
        var m: SubjectOfGradeVM =
        {
            id: data.id,
            name: data.name,
            gradeId: data.gradeId,
            orderNumber: data.orderNumber,
            checked: false,
            startDate: null,
            endDate: null,
            chose: false,

        };

        return m;
    }
}
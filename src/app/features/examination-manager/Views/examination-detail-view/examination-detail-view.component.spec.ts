import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExaminationDetailViewComponent } from './examination-detail-view.component';

describe('ExaminationDetailViewComponent', () => {
  let component: ExaminationDetailViewComponent;
  let fixture: ComponentFixture<ExaminationDetailViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExaminationDetailViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExaminationDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

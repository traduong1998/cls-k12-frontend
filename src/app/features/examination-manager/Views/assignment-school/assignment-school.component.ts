import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { ReplaySubject, Subscription, Subject } from 'rxjs';
import { DivisionEndpoint, DivisionOption, UserIdentity } from 'cls-k12-sdk-js/src';
import { OrganizeExamEndpoint } from 'cls-k12-sdk-js/src';
import { School } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/School';
import { SchoolAction } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/request/assign-school-request';
import { GetSchoolRequest } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/request/get-school-request';
import { Action } from 'sdk/cls-k12-sdk-js/src/services/user-exam/enum/action';
import { AuthService } from 'src/app/core/services';
import { MessageService } from 'src/app/shared/services/message.service';
import { take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-assignment-school',
  templateUrl: './assignment-school.component.html',
  styleUrls: ['./assignment-school.component.scss']
})
export class AssignmentSchoolComponent implements OnInit, AfterViewInit {
  baseApiUrl = 'http://localhost:65000';
  isLoadingResults = false;
  protected divisions: DivisionOption[];
  userIdentity: UserIdentity;
  levelManageValue: number = 0;
  endpointDivision: DivisionEndpoint;
  endpoinOrganizeExam: OrganizeExamEndpoint
  public divisionFilterCtrl: FormControl = new FormControl();
  public divisionCtrl: FormControl = new FormControl();
  public txtKeyword: FormControl = new FormControl('');
  /** list of divisions filtered by search keyword */
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject < DivisionOption[] > (1);
  allComplete: boolean = false;
  //fake list school
  schoolAction: SchoolAction[] = [];
  examId;
  gradeId;
  routeSub: Subscription;
  listSchool: School[] = [];
  displayedColumns: string[] = ['stt', 'name', 'code', 'select'];
  SchoolPaginationFilter: GetSchoolRequest;
  totalItems: number;
  isFirstLoadDivisions = true;
  @ViewChild(MatPaginator) paginator: MatPaginator = Object.create(null);
  @ViewChild(MatSort) sort: MatSort = Object.create(null);
  selection = new SelectionModel < School > (true, []);
  isShowFilter = true;
  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject < void> ();
  // tslint:disable-next-line - Disables all
  constructor(private _authService: AuthService, private router: Router, private route: ActivatedRoute, private _messageService: MessageService) {
    this.endpoinOrganizeExam = new OrganizeExamEndpoint();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();
    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    }
  }
  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.SchoolPaginationFilter.page = this.paginator.pageIndex + 1;
      this.SchoolPaginationFilter.size = this.paginator.pageSize;
      this.getSchoolData();
    });
  }

  ngOnInit(): void {

    this.routeSub = this.route.params.subscribe(params => {
      this.examId = params['id'];
      this.gradeId = params['gradeId'];
    });
    this.SchoolPaginationFilter = {
      gradeId: this.gradeId,
      examId: this.examId,
      divisionId: null,
      keyWord: '',
      page: 1,
      size: 10,
    };
    this.getSchoolData();
    // listen for search field value changes
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });
  }


  someComplete(): boolean {
    return this.someChecked(this.allComplete);
  }
  setAll(completed: boolean): void {
    this.allComplete = completed;
    this.checkAll(completed);
  }
  checkAll(completed: boolean) {
    this.listSchool.map(x => x.isChoose = completed);
    this.listSchool.forEach(sch => {
      if (completed) {
        let action = this.schoolAction.find(x => x.schoolId == sch.id);
        if (action) {
          if (action.action == 'DEL') {
            action.action = "";
          }
        } else {
          this.schoolAction.push({ schoolId: sch.id, action: "ADD" })
        }
      } else {
        let action = this.schoolAction.find(x => x.schoolId == sch.id);
        if (action) {
          if (action.action == 'ADD') {
            this.schoolAction.splice(this.schoolAction.indexOf(action), 1)
          } else if (action.action == "") {
            action.action = "DEL"
          }
        }
      }
    });
  }
  someChecked(allComplete: boolean): boolean {
    return this.listSchool.filter(x => x.isChoose).length > 0 && !allComplete;
  }
  updateAllComplete(event, id) {
    this.allComplete = this.isCheckAll();

    let school = this.schoolAction.find(x => x.schoolId == id);
    //xử lý action khi check hoặc uncheck
    if (event) {
      if (school) {
        if (school.action == Action.Delete) {
          school.action = ""
        }
      } else {
        this.schoolAction.push({ schoolId: id, action: Action.Add });
      }
    } else {
      if (school) {
        if (school.action == Action.Add) {
          this.schoolAction.splice(this.schoolAction.indexOf(school), 1);

        } else if (school.action == "") {
          school.action = Action.Delete
        }
      }
    }
  }
  isCheckAll(): boolean {
    return this.listSchool.every(x => x.isChoose);
  }

  assiassignment() {
    let listSchool = this.schoolAction.filter(x => x.action != "");
    if (listSchool.length > 0) {
      this.isLoadingResults = true;
      this.endpoinOrganizeExam.assignExamToSchools({ examId: this.examId, listSchools: this.schoolAction.filter(x => x.action != "") })
        .then(res => {
          this.isLoadingResults = false;
          this.schoolAction.map(x => x.action = "");
          this._messageService.susccessCallback("Phân bố trường thành công")
          this.schoolAction = [];
          this.getSchoolData();
        })
        .catch(this.failureCallback);
    } else {
      this._messageService.susccessCallback("Phân bố trường thành công")
    }

  }
  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }
  onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.divisions = res;
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(

        )
    } else {

    }
  }
  getFilterParam() {

    this.SchoolPaginationFilter.divisionId = this.divisionCtrl.value;

    this.SchoolPaginationFilter.keyWord = this.txtKeyword.value;
    this.SchoolPaginationFilter.page = 1;
    this.SchoolPaginationFilter.size = 10;
  }
  onSubmitFilter() {
    this.getFilterParam();
    this.getSchoolData();
  }
  getSchoolData() {
    this.isLoadingResults = true;
    this.endpoinOrganizeExam.getSchools(this.SchoolPaginationFilter)
      .then(res => {
        this.isLoadingResults = false;
        this.listSchool = res.items;
        this.totalItems = res.totalItems;
        this.listSchool.forEach(shc => {
          if (shc.isChoose == true) {
            var action = this.schoolAction.find(x => x.schoolId == shc.id);
            if (action) {
              if (action.action == 'DEL') {
                shc.isChoose = false;
              }
            } else {
              this.schoolAction.push({ schoolId: shc.id, action: "" })
            }
          } else {
            var action = this.schoolAction.find(x => x.schoolId == shc.id);
            if (action) {
              if (action.action == 'ADD') {
                shc.isChoose = true;
              }
            }
          }
        });
        this.allComplete = this.isCheckAll();
      })
      .catch(this.failureCallback);
  }
  failureCallback() {
    this.isLoadingResults = false;
    this._messageService.failureCallback("Có lỗi xảy ra lúc phân bố ca thi");
  }

  backListExams() {
    this.router.navigate(['dashboard/examination-manager']);
  }
}


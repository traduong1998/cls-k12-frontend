import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentSchoolComponent } from './assignment-school.component';

describe('AssignmentSchoolComponent', () => {
  let component: AssignmentSchoolComponent;
  let fixture: ComponentFixture<AssignmentSchoolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignmentSchoolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentSchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

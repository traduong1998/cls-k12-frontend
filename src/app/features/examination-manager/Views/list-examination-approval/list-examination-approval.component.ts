import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { DivisionEndpoint, DivisionOption, ExamInfo, GetExamFilterRequests, GradeEndpoint, SchoolEndpoint, SubjectEndpoint, UserEndpoint, UserIdentity } from 'cls-k12-sdk-js/src';
import { ReplaySubject, Subject as RxSubject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/PaginatorResponse';
import { ExamEnpoint } from 'sdk/cls-k12-sdk-js/src/services/exam/endpoints/exam-enpoint';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { AuthService } from 'src/app/core/services';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { DialogConfirmComponent } from '../../Components/dialog-confirm/dialog-confirm.component';
import { ToastComponent } from '../../Components/toast/toast.component';
import { DialogConfirm } from '../../interfaces/dialog-confirm';

@Component({
  selector: 'app-list-examination-approval',
  templateUrl: './list-examination-approval.component.html',
  styleUrls: ['./list-examination-approval.component.scss']
})
export class ListExaminationApprovalComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('dpFromDate') dpFromDate;
  @ViewChild('dpToDate') dpToDate;

  startDate: FormControl = new FormControl('');

  baseApiUrl: string = "";
  protected allGrades: GradeOption[];

  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected divisions: DivisionOption[];
  public divisionCtrl: FormControl = new FormControl();
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  protected schools: SchoolOption[];
  public schoolCtrl: FormControl = new FormControl();
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  protected _onDestroy = new RxSubject<void>();

  public examTypeCtrl: FormControl = new FormControl();
  public fromDateCtrl: FormControl = new FormControl('');
  public toDateCtrl: FormControl = new FormControl('');

  startDateFilter: Date = null;
  endDateFilter: Date = null;

  filterForm = this.fb.group({
    fromDate: new FormControl(),
    toDate: '',
    search: new FormControl(''),
    gradeId: new FormControl(null, [Validators.required])
  });

  isFirstLoadDivisions: boolean = true;
  isFirstLoadSchools: boolean = true;
  isFirstLoadGrades: boolean = true;
  isFirstLoadAllGrades: boolean = true;

  levelManageValue: number = 0;

  dialogConfirm: DialogConfirm;
  listExamChoose: number[] = [];

  dataExamPagination: PaginatorResponse<ExamInfo> = {
    items: [],
    totalItems: 0
  };
  getExamFilterRequest: GetExamFilterRequests = {
    pageNumber: 1,
    pageSize: 10,
    divisionId: null,
    schoolId: null,
    gradeId: null,
    subjectId: null,

    fromDate: null,
    toDate: null,
    scope: null,
    type: null,
    statusEdit: null,
    getCount: true,

    filter: {
      sortDirection: 'DESC',
      sortField: 'CRE',
    },
  }

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  subjectFilter = {
    gradeId: null
  }
  displayedColumns: string[] = ['stt', 'name', 'grade', 'creator', 'scope', 'startTime', 'endTime', 'function'];

  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  private endpointGrade: GradeEndpoint;
  private examEndpoint: ExamEnpoint;
  private endpointSubject: SubjectEndpoint;
  private endpointUser: UserEndpoint;

  userIdentity: UserIdentity;

  constructor(private fb: FormBuilder, private router: Router, private dialog: MatDialog, private _snackBar: MatSnackBar, private _authService: AuthService,) {
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubject = new SubjectEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUser = new UserEndpoint({ baseUrl: this.baseApiUrl });

    this.examEndpoint = new ExamEnpoint();
    this.userIdentity = _authService.getTokenInfo();
    console.log(this.userIdentity);

    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    }
    else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.getExamFilterRequest.divisionId = this.userIdentity.divisionId;
    }
    else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
      this.getExamFilterRequest.schoolId = this.userIdentity.schoolId;
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
    }
  }

  ngOnInit(): void {
    this.getListExamData();
    // listen for search field value changes
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });

    // listen for search field value changes
    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });
  }
  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.getExamFilterRequest.pageNumber = this.paginator.pageIndex + 1;
      this.getExamFilterRequest.pageSize = this.paginator.pageSize;
      this.getListExamData();
    });

  }
  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  getListExamData() {
    this.examEndpoint.getExamsApproval(this.getExamFilterRequest)
      .then((res) => {
        this.dataExamPagination = res;
        if (this.isFirstLoadAllGrades) {
          this.endpointGrade.getGradeOptions(null)
            .then(res => {
              this.isFirstLoadAllGrades = false;
              this.allGrades = res;

              this.dataExamPagination.items = this.dataExamPagination.items.map((exam) => {
                let grade = res.find(x => x.id == exam.gradeId);
                if (grade != undefined) {
                  exam.gradeName = grade.name;
                }
                return exam;
              });

            })
            .catch();

          let userIds = this.dataExamPagination.items.map(x => { return x.creatorId });
          this.endpointUser.GetListUserByIds(userIds)
            .then(res => {
              this.dataExamPagination.items = this.dataExamPagination.items.map((exam) => {
                let user = res.find(x => x.id == exam.creatorId);
                if (user != undefined) {
                  exam.creatorName = user.name;
                }
                return exam;
              });

            })
            .catch();
        }
        else {
          this.dataExamPagination.items = this.dataExamPagination.items.map((exam) => {
            let grade = this.allGrades.find(x => x.id == exam.gradeId);
            if (grade != undefined) {
              exam.gradeName = grade.name;
            }
            return exam;
          });

          let userIds = this.dataExamPagination.items.map(x => { return x.creatorId });
          this.endpointUser.GetListUserByIds(userIds)
            .then(res => {
              this.dataExamPagination.items = this.dataExamPagination.items.map((exam) => {
                let user = res.find(x => x.id == exam.creatorId);
                if (user != undefined) {
                  exam.creatorName = user.name;
                }
                return exam;
              });

            })
            .catch();

        }

      })
  }

  approveExam(examId) {
    this.dialogConfirm = {
      title: 'DUYỆT KỲ THI',
      description: 'Bạn có chắc chắn muốn DUYỆT kỳ thi này?',
      type: 'confirm',
      labelYes: 'Duyệt',
      labelNo: 'Hủy'
    }
    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.examEndpoint.approveExam(examId, true)
          .then(
            res => {
              if (res) {
                this.dataExamPagination.items = this.dataExamPagination.items.filter(x => x.id != examId);
                this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                  data: 'Duyệt thành công!',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              } else {
                this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                  data: 'Duyệt thất bại. Vui lòng thử lại sau!',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            }
          )
          .catch((err: ErrorResponse) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: err.errorDetail,
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
      }
    });
  }

  rejectExam(examId) {
    this.dialogConfirm = {
      title: 'TRẢ LẠI KỲ THI',
      description: 'Bạn có chắc chắn muốn TRẢ LẠI kỳ thi này?',
      labelYes: 'Trả lại',
      labelNo: 'Hủy'
    }

    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.examEndpoint.approveExam(examId, false)
          .then(
            res => {
              if (res) {
                this.dataExamPagination.items = this.dataExamPagination.items.filter(x => x.id != examId);
                this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                  data: 'Trả lại thành công!',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              } else {
                this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                  data: 'Trả lại thất bại. Vui lòng thử lại sau!',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            }
          )
          .catch((err: ErrorResponse) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: err.errorDetail,
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
      }
    });
  }

  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id;
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id;
      });

    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectGrade != undefined) {
          this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
        }
      });
  }
  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.divisions = res;
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch()
    }
  }

  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionCtrl.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchools || this.schoolFilter.divisionId != divisionIdSelected) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchools = false;
          this.schools = res;
          this.schoolFilter.divisionId = divisionIdSelected;
          this.filteredSchools.next(this.schools.slice());
        })
        .catch()
    }
  }

  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolCtrl.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrades || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch()
    }
  }

  onChangeDivisionSelected(id: any) {
    this.resetSchoolSelectCtrl();
    this.resetGradeSelectCtrl();
  }
  onChangeSchoolSelected(id: any) {
    this.resetGradeSelectCtrl();
  }

  resetSchoolSelectCtrl() {
    this.schools = [];
    this.isFirstLoadSchools = true;
    this.schoolCtrl.setValue(undefined);
  }
  resetGradeSelectCtrl() {
    this.grades = [];
    this.isFirstLoadGrades = true;
    this.gradeCtrl.setValue(undefined);
  }

  getFilterParam() {
    this.getExamFilterRequest.divisionId = this.divisionCtrl.value;
    this.getExamFilterRequest.schoolId = this.schoolCtrl.value;

    if (this.userIdentity.levelManage == "DVS") {
      this.getExamFilterRequest.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.getExamFilterRequest.divisionId = this.userIdentity.divisionId;
      this.getExamFilterRequest.schoolId = this.userIdentity.schoolId;
    }

    this.getExamFilterRequest.gradeId = this.gradeCtrl.value;
    this.getExamFilterRequest.type = this.examTypeCtrl.value;
    this.getExamFilterRequest.pageNumber = 1;
    this.paginator.pageIndex = 0;
  }

  filterExam() {
    this.getFilterParam();
    this.getListExamData();
  }
  backToContent() {
    this.router.navigate([`dashboard/examination-manager`]);
  }
}

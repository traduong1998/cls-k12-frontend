import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListExaminationApprovalComponent } from './list-examination-approval.component';

describe('ListExaminationApprovalComponent', () => {
  let component: ListExaminationApprovalComponent;
  let fixture: ComponentFixture<ListExaminationApprovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListExaminationApprovalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListExaminationApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

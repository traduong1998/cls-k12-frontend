import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExamSubjectOrganizeExaminationComponent } from './exam-subject-organize-examination.component';

describe('ExamSubjectOrganizeExaminationComponent', () => {
  let component: ExamSubjectOrganizeExaminationComponent;
  let fixture: ComponentFixture<ExamSubjectOrganizeExaminationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExamSubjectOrganizeExaminationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExamSubjectOrganizeExaminationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

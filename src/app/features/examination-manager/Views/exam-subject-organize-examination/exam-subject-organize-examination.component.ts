import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/errorResponse';
import { OrganizeExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/endpoints/organize-exam-endpoint';
import { Action } from 'sdk/cls-k12-sdk-js/src/services/user-exam/enum/action';
import { StepHeaderControlExam, StepHeaderControlOrganize } from 'src/app/features/learn/intefaces/stepHeaderControl';
import { MessageService } from 'src/app/shared/services/message.service';
import { AddStudentExamComponent } from '../../Components/add-student-exam/add-student-exam.component';
import { AddSupervisiorExamComponent } from '../../Components/add-supervisior-exam/add-supervisior-exam.component';
import { AddTeacherExamComponent } from '../../Components/add-teacher-exam/add-teacher-exam.component';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { VariableAst } from '@angular/compiler';

@Component({
  selector: 'app-exam-subject-organize-examination',
  templateUrl: './exam-subject-organize-examination.component.html',
  styleUrls: ['./exam-subject-organize-examination.component.scss']
})
export class ExamSubjectOrganizeExaminationComponent implements OnInit, AfterViewInit {
  @ViewChild(AddStudentExamComponent) addStudentExamView;
  @ViewChild(AddSupervisiorExamComponent) addSupervisorExamView;
  @ViewChild(AddTeacherExamComponent) addTeacherExamView;

  studentAction = [];
  supervisorAction = [];
  teacherAction = [];

  isLinear = false;
  firstFormGroup: FormGroup = Object.create(null);
  secondFormGroup: FormGroup = Object.create(null);
  isOptional = false;
  isEditable = false;

  currentPage = 1;

  subjectId = 1;
  examId = 1;
  gradeId;
  organizeExamId;
  status: string = "";
  routeSub: Subscription;
  stepHeaderCtrl: StepHeaderControlOrganize;
  organizeExamEndpoint: OrganizeExamEndpoint;
  isFullScreen = true;
  isProcessing = false;
  constructor(private _formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private _snackBar: MatSnackBar, private _messageService: MessageService, private spinner: NgxSpinnerService) {
    this.stepHeaderCtrl = {
      currentPage: this.currentPage,
      //truyền id cho đúng để router
      examId: 1,
      subjectId: 1
    };

    this.organizeExamEndpoint = new OrganizeExamEndpoint();
  }
  ngAfterViewInit(): void {
    this.studentAction = this.addStudentExamView.userAction;
    this.supervisorAction = this.addSupervisorExamView.supervisorAction;
    this.teacherAction = this.addTeacherExamView.teacherAction;
  }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });

    //get id route

    this.routeSub = this.route.params.subscribe(params => {
      this.examId = params['id'];
      this.subjectId = params['subjectId'];
      this.gradeId = params['gradeId'];
      this.organizeExamId = params['organizeId'];
    });
    this.organizeExamEndpoint.getOrganizeExamStatus(this.organizeExamId).then((res) => { this.status = res })
      .catch(() => {
        this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại");
      })
  }

  // Không thay đổi gì
  createPromiseOk(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      resolve(true);
    })
  }

  nextPage() {
    this.isProcessing = true;
    this.studentAction = this.addStudentExamView.userAction;
    this.supervisorAction = this.addSupervisorExamView.supervisorAction;
    this.teacherAction = this.addTeacherExamView.teacherAction;
    if (this.studentAction.filter(x => x.action != 'DEL').length == 0) {
      this._messageService.failureCallback("Chưa có thí sinh nào được chọn. Vui lòng chọn ít nhất 1 thí sinh để tiếp tục", 5000);
      this.isProcessing = false;
    }
    else if (this.supervisorAction.filter(x => x.action != 'DEL').length == 0) {
      this._messageService.failureCallback("Chưa có giám thị nào được chọn. Vui lòng chọn ít nhất 1 giám thị để tiếp tục", 5000)
      this.isProcessing = false;
    }
    else {
      this.isFullScreen = false;
      this.spinner.show("totalLoading");
      let assignStudentResponse = this.createPromiseOk();

      if (this.studentAction.some(x => x.action == Action.Add || x.action == Action.Delete)) {
        assignStudentResponse = this.organizeExamEndpoint.assignStudentsToExam(
          { examId: this.examId, subjectId: this.subjectId, gradeId: this.gradeId, users: this.studentAction, organizeExamId: this.organizeExamId }
        );
      }

      let assignSupervisorResponse = this.createPromiseOk();
      if (this.supervisorAction.some(x => x.action == Action.Add || x.action == Action.Delete)) {
        assignSupervisorResponse = this.organizeExamEndpoint.assignSupervisorsToExam(
          { examId: this.examId, subjectId: this.subjectId, gradeId: this.gradeId, users: this.supervisorAction, organizeExamId: this.organizeExamId }
        );
      }

      let assignTeacherResponse = this.createPromiseOk();

      if (this.teacherAction.some(x => x.action == Action.Add || x.action == Action.Delete)) {
        assignTeacherResponse = this.organizeExamEndpoint.assignTeachersToExam(
          { examId: this.examId, subjectId: this.subjectId, gradeId: this.gradeId, listExamSubjectTeacher: this.teacherAction, organizeExamId: this.organizeExamId }
        );
      }

      let errorDetail = '';
      const bar = Promise.all([
        assignStudentResponse.catch((err: ErrorResponse) => {
          errorDetail += err.errorDetail + '. ';
        }),
        assignSupervisorResponse.catch((err: ErrorResponse) => {
          errorDetail += err.errorDetail + '. ';
        }),
        assignTeacherResponse.catch((err: ErrorResponse) => {
          errorDetail += err.errorDetail + '. ';
        })
      ])
        .then((values) => {
          if (errorDetail != '') {
            this._messageService.failureCallback("Có lỗi xảy ra vui lòng thử lại", 5000);
            this.spinner.hide("totalLoading");

          } else {
            this.spinner.hide("totalLoading");
            this.router.navigate(['dashboard/examination-manager/organize/' + this.examId + '/grade/' + this.gradeId + '/examsubject/' + this.subjectId + '/setting/' + this.organizeExamId]);
          }
          this.isProcessing = false;
        });
    }
  }
  previousPage() {
    this.router.navigate(['dashboard/examination-manager/organize/' + this.examId + '/grade/' + this.gradeId + '/examsubject']);
  }
}

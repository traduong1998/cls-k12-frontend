import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ExamEnpoint } from 'sdk/cls-k12-sdk-js/src/services/exam/endpoints/exam-enpoint';
import { ExamDetail, ExamScope, ExamType, UserIdentity } from 'cls-k12-sdk-js/src';
import { ToastComponent } from '../../Components/toast/toast.component';
import { ReplaySubject, Subject as RxSubject, Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/services';
import { SubjectsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/subjectsEndpoint';
import { DivisionEndpoint, GradeEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { MatSelect } from '@angular/material/select';
import { take, takeUntil } from 'rxjs/operators';
import { GradeOption } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { StepHeaderControlExam } from 'src/app/features/learn/intefaces/stepHeaderControl';
import { SubjectExam } from '../../interfaces/subject-exam';
import { OptionSelect } from '../../interfaces/option-select';
import { SubjectOfGradeVM } from './interfaces/subject-of-grade-vm';
import { CreateExamRequest, SubjectCreateRequest } from 'cls-k12-sdk-js/src/services/exam/requests/create-exam-request';
import { ExamStep } from 'sdk/cls-k12-sdk-js/src/services/exam/requests/exam-step-request';
import { validateBasis } from '@angular/flex-layout';
import { AssessmentLevelGroupOption } from 'sdk/cls-k12-sdk-js/src/services/exam/models/assessmen-level-group';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { SubjectResponses } from 'sdk/cls-k12-sdk-js/src/services/lesson/responses/subjectResponses';

@Component({
  selector: 'app-create-examination',
  templateUrl: './create-examination.component.html',
  styleUrls: ['./create-examination.component.scss']
})
export class CreateExaminationComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  @ViewChild('picker') drpFromToDate;
  @ViewChild('dpFromDate') dpFromDate;
  @ViewChild('dpToDate') dpToDate;

  today = new Date();

  timeSubject = this.fb.group({
    startTime: new FormControl(new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0), [Validators.required]),
    endTime: new FormControl(new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 0), [Validators.required])
  });
  infoForm = this.fb.group({
    name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    startDate: new FormControl(new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0), [Validators.required]),
    endDate: new FormControl(new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 23, 59, 0), [Validators.required]),
    gradeId: new FormControl(null, [Validators.required])
  });

  examTypeCtrl: FormControl = new FormControl('', Validators.required);

  public minDate: Date = new Date();

  routeSub: Subscription
  examId = 0;
  //sttep
  step = 1;
  //mảng data chứa môn thi
  dataInfoExam: ExamDetail;
  //biến check trạng thái của vùng validate
  checkValidate: boolean = false;
  //biến check đã chọn môn đó
  displayedColumns: string[] = ['subjectName', 'checked'];
  displayedColumnsListSubject: string[] = ['select', 'stt', 'subjectName', 'startDate', 'endDate', 'delete'];
  //danh danh hình thức thi
  types: OptionSelect[] = [{ value: ExamType.Free, display: 'Tự do', checked: true }, { value: ExamType.Concentrated, display: 'Tập trung', checked: false }]
  typeChoose: ExamType = ExamType.Concentrated;
  //danh sách môn chọn ra đề thi 
  listSubjectWasChoose: SubjectExam[] = [];
  // danh sach hom de thi
  subjectOfExam: SubjectOfGradeVM[] = []
  stepHeaderControl: StepHeaderControlExam;

  //danh sách môn
  listSubject: SubjectOfGradeVM[] = [];
  //data exam requesst
  subjectIdRemove: string[] = [];


  get startTime() { return this.timeSubject.get('startTime'); }
  get endTime() { return this.timeSubject.get('endTime'); }

  get name() { return this.infoForm.get('name'); }
  get startDate() { return this.infoForm.get('startDate'); }
  get endDate() { return this.infoForm.get('endDate'); }
  get gradeId() { return this.infoForm.get('gradeId'); }

  userIdentity: UserIdentity;
  portalId: number;
  schoolId: number;


  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected assessmentLevelGroups: AssessmentLevelGroupOption[];
  public assessmentLevelGroupCtrl: FormControl = new FormControl('', Validators.required);
  public assessmentLevelGroupFilterCtrl: FormControl = new FormControl();
  public filteredAssessmentLevelGroups: ReplaySubject<AssessmentLevelGroupOption[]> = new ReplaySubject<AssessmentLevelGroupOption[]>(1);
  @ViewChild('singleSelectAssessmentLevelGroup', { static: true }) singleSelectAssessmentLevelGroup: MatSelect;


  protected _onDestroy = new RxSubject<void>();

  listGrade: GradeOption[] = [];
  private gradeEndpoint: GradeEndpoint;
  private subjectsEndpoint: SubjectsEndpoint;
  private examEndPoint: ExamEnpoint;
  endpointDivision: DivisionEndpoint;
  ////
  isFirstLoadGrades = true;
  isFirstLoadAssesment = true;
  isProcessing = false;
  constructor(private _authService: AuthService, private router: Router, private route: ActivatedRoute, private fb: FormBuilder, private _snackBar: MatSnackBar) {
    this.gradeEndpoint = new GradeEndpoint({ baseUrl: 'http://localhost:65000' });
    this.subjectsEndpoint = new SubjectsEndpoint({ baseUrl: 'http://localhost:65000' });
    this.examEndPoint = new ExamEnpoint();
    this.endpointDivision = new DivisionEndpoint({ baseUrl: 'http://localhost:65000' });
    this.stepHeaderControl = {
      currentPage: 1,
      isCreatingTest: false,
      examId: null,
      step: 1
    }
  }



  ngOnInit(): void {
    //get infor token
    this.userIdentity = this._authService.getTokenInfo();

    this.routeSub = this.route.params.subscribe(params => {
      this.examId = +params['id'];
    });

    this.portalId = this.userIdentity.portalId;
    this.schoolId = this.userIdentity.schoolId;
    // get 
    console.log("authen", this.userIdentity);

    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });
    // listen for search field value changes
    this.assessmentLevelGroupCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterAssessmentLevelGroups();
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  submitForm() {
    this.isProcessing = true;
    this.infoForm.markAllAsTouched();
    this.assessmentLevelGroupCtrl.markAsTouched();
    this.examTypeCtrl.markAsTouched();

    let sDate = this.startDate.value.setHours(0, 0, 0);
    let eDate = this.endDate.value.setHours(23, 59, 59);

    if (sDate > eDate || this.assessmentLevelGroupCtrl.invalid || this.examTypeCtrl.invalid) {
      this.isProcessing = false;
      return;
    }
    if (this.infoForm.invalid) {
      if (this.name.value == '') {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: 'Bạn chưa nhập tên kỳ thi!',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
      if (this.subjectOfExam) {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Bạn chưa chọn môn thi!',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
      this.isProcessing = false;
    }
    else {
      // tạo mới thông tin
      let subjectRequest: SubjectCreateRequest[] = [];
      this.subjectOfExam.forEach(element => {
        let sDate = element.startDate;
        let eDate = element.endDate;
        subjectRequest.push({
          subjectId: element.id,
          startDate: new Date(sDate),
          endDate: new Date(eDate)
        })
      });

      let createRequest: CreateExamRequest = {
        name: this.name.value, gradeId: this.gradeId.value, assessmentLevelGroupId: parseInt(this.assessmentLevelGroupCtrl.value),
        scope: ExamScope.Private, type: this.examTypeCtrl.value, startDate: new Date(sDate),
        endDate: new Date(eDate),
        examSubjects: subjectRequest
      };

      this.examEndPoint.createInfoExam(createRequest).then((res) => {
        if (res) {
          this.examId = res.id;
          // this._snackBar.openFromComponent(ToastComponent, {
          //   data: ['Thành công'],
          //   duration: 1000,
          //   horizontalPosition: 'center',
          //   verticalPosition: 'top',
          // });
        }
        else {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Thất bại!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.isProcessing = false;
        }
      }).then(() => {
        //step model
        let examStep: ExamStep = { id: this.examId, step: 2 }
        this.examEndPoint.updateStepExam(this.examId, examStep).then(res => {
          if (res) {
            //next step
            this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "choose-test"], { queryParams: { gradeId: this.gradeId.value } })
          } else {
            console.log("error")
          }
          this.isProcessing = false;
        })
      })
        .catch(error => {
          this.isProcessing = false;
          alert("Lỗi")
        })
    }
  }
  // get value of value exam
  changeType(type: ExamType) {
    this.typeChoose = type;
  }

  nextPage() {
    this.submitForm();
  }

  chose() {
    //xuất ra danh sách môn đã chọn qua table
    this.listSubject.filter(x => x.checked == true).forEach((obj) => {
      let index = this.subjectOfExam.findIndex(o => o.id == obj.id);
      if (index == -1) {
        let subChose: SubjectOfGradeVM = { ...obj };
        subChose.startDate = new Date(new Date(this.startDate.value).setHours(0, 0, 0));
        subChose.endDate = new Date(new Date(this.endDate.value).setHours(23, 59, 0));
        subChose.checked = false;
        this.subjectOfExam.push(subChose);
      }

      obj.checked = false;
      obj.chose = true;
    });

    this.subjectOfExam = this.reloadArray(this.subjectOfExam);
  }

  back() {
    let subjectBacked = this.subjectOfExam.filter(x => x.checked);
    if (!subjectBacked) {
      return;
    }

    this.subjectOfExam = this.subjectOfExam.filter(x => !x.checked);
    this.subjectOfExam = this.reloadArray(this.subjectOfExam);

    //tìm khối đã bị bỏ bên danh sách khối
    this.listSubject.forEach((sub) => {
      if (subjectBacked.find(y => y.id == sub.id)) {
        sub.chose = false;
      }
    })

    this.listSubject = this.reloadArray(this.listSubject);
  }

  private reloadArray<T>(model: T[]) {
    return [...model];
  }

  //change date of subject 
  changeStartDate(event: MatDatepickerInputEvent<Date>, subjectId) {
    let sub = this.subjectOfExam.find(o => o.id == subjectId);
    sub.startDate = event.value;
    // let index = this.listSubjectOfExam.findIndex(o => o.subjectId == subjectId);
    // if (index != -1) {
    //   this.listSubjectOfExam[index].startDate = event.value;
    // }
  }

  changeEndDate(event: MatDatepickerInputEvent<Date>, subjectId) {
    let sub = this.subjectOfExam.find(o => o.id == subjectId);
    sub.endDate = event.value;
  }
  chooseSubject(element) {
    let subject = this.listSubject.find(x => x.id == element.id);
    if (subject != null && !subject.chose) {
      subject.checked = !subject.checked;
      this.listSubject = this.reloadArray(this.listSubject);
    }
  }

  deleteSubject(element) {

    //kiểm tra tồn tại trong danh sách table
    let index = this.subjectOfExam.findIndex(x => x.id == element.id);
    if (index != -1) {
      this.subjectOfExam.splice(index, 1);

      //relaod
      this.subjectOfExam = this.reloadArray(this.subjectOfExam);

      //unchose subject from subjects
      let subjectUnchose = this.listSubject.find(x => x.id == element.id);
      subjectUnchose.chose = false;
    }
  }

  changeCheck(event, row) {
    if (event.checked) {
      let index = this.subjectIdRemove.indexOf(row.subjectId);
      if (index == -1) {
        this.subjectIdRemove.push(row.subjectId);
      }
    } else {
      let index = this.subjectIdRemove.indexOf(row.subjectId);
      if (index != -1) {
        this.subjectIdRemove.splice(index, 1);
      }
    }
  }

  protected setInitialValue() {
    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectGrade != undefined) {
          this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
        }
      });
    this.filteredAssessmentLevelGroups
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectAssessmentLevelGroup != undefined) {
          this.singleSelectAssessmentLevelGroup.compareWith = (a: AssessmentLevelGroupOption, b: AssessmentLevelGroupOption) => a && b && a.id === b.id;
        }
      });
  }

  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterAssessmentLevelGroups() {
    if (!this.assessmentLevelGroups) {
      return;
    }
    // get the search keyword
    let search = this.assessmentLevelGroupFilterCtrl.value;
    if (!search) {
      this.filteredAssessmentLevelGroups.next(this.assessmentLevelGroups.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredAssessmentLevelGroups.next(
      this.assessmentLevelGroups.filter(assessment => assessment.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onGradeSelectClicked() {
    if (this.isFirstLoadGrades) {
      this.gradeEndpoint.getGradeOptions(this.schoolId)
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch(

        )
    } else {

    }
  }
  onAssessmentLevelGroupSelectClicked() {
    if (this.isFirstLoadAssesment) {
      this.examEndPoint.getAssessmenLevelGroupOptions()
        .then(res => {
          this.isFirstLoadAssesment = false;
          this.assessmentLevelGroups = res;
          this.filteredAssessmentLevelGroups.next(this.assessmentLevelGroups.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onChangeGradeSelected(id: any) {

    this.listSubject = [];
    this.subjectsEndpoint.getSubjectByGradeIdOptions(id).then(res => {
      res.forEach(element => {
        let subjectOfGrade: SubjectResponses = {
          id: element.id,
          name: element.name
        }
        this.listSubject.push(SubjectOfGradeVM.From(subjectOfGrade));
      });


      this.listSubject = [...this.listSubject];
      // reset môn đã chọn
      this.subjectOfExam = []
      this.subjectOfExam = this.reloadArray(this.subjectOfExam);
    })
  }

  openDateRangePicker() {
    this.drpFromToDate.open();
  }
  openDpFromDate() {
    this.dpFromDate.open();
  }
  openDpToDate() {
    this.dpToDate.open();
  }
  backToContent() {
    this.router.navigate([`dashboard/examination-manager`]);
  }
  examToDateChange(e) {
    // set lại cuối ngày cho thời gian kết thúc kỳ thi
    this.endDate.value.setHours(23, 59, 0);
  }
}

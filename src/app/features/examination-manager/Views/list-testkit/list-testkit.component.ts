import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ExamEnpoint } from 'sdk/cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { SubjectTestkitRequest } from 'sdk/cls-k12-sdk-js/src/services/exam/requests/update-subject-testkit-request';
import { QuestionConverterEndpoint } from 'sdk/cls-k12-sdk-js/src/services/question-converter/endpoints/question-converter-endpoint';
import { TestKitEnpoint } from 'sdk/cls-k12-sdk-js/src/services/testkit/enpoints/testkit-enpoint';
import { TestKit } from 'sdk/cls-k12-sdk-js/src/services/testkit/models/testkit';
import { TestkitsPagingRequest } from 'sdk/cls-k12-sdk-js/src/services/testkit/requests/testkits-paging-request';
import { DialogConfirmComponent } from '../../Components/dialog-confirm/dialog-confirm.component';
import { DialogConfirm } from '../../interfaces/dialog-confirm';
import { TestEnpoint } from 'sdk/cls-k12-sdk-js/src/services/test/enpoint/test-enpoint';
import { ViewExamFinishComponent } from '../view-exam-finish/view-exam-finish.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';

@Component({
  selector: 'app-list-testkit',
  templateUrl: './list-testkit.component.html',
  styleUrls: ['./list-testkit.component.scss']
})
export class ListTestkitComponent implements OnInit {
  
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort = Object.create(null);

  @ViewChild("file") file: ElementRef;
  routeSub: Subscription;
  //mã ki thi  môn thi
  subjectExamId;
  //ma mon thi
  subjectId;
  //examid
  examId: number;
  gradeId: number;
  testKitId: number;

  stepHeaderControl;
  displayedColumns: string[] = ['stt', 'name', 'createType', 'used', 'function'];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  // Endpoint
  private testEnpoint: TestEnpoint;

  private dialogQuestionBankList: MatDialogRef<ViewExamFinishComponent>

  testKit: TestKit[] = [];
  totalTestKit: number = 0;
  dialogConfirm: DialogConfirm;
  isPublished: boolean = true;
  testKitPagingRequest: TestkitsPagingRequest;
  private testkitEnpoint: TestKitEnpoint;
  private examApi: ExamEnpoint;

  private questionConverterEndpoint: QuestionConverterEndpoint;

  constructor(private _snackBar: MatSnackBar, private route: ActivatedRoute, private router: Router, private dialog: MatDialog) {
    this.examApi = new ExamEnpoint();
    this.questionConverterEndpoint = new QuestionConverterEndpoint();

    this.routeSub = this.route.params.subscribe(params => {
      this.examId = +params['id'];
    });
    this.testEnpoint = new TestEnpoint();
    this.route.queryParams.subscribe(params => {
      this.gradeId = +params['gradeId'];

      this.subjectId = +params['subjectId'];

      this.subjectExamId = +params['examSubjectId'];

      this.testKitId = +params['testKitId'];

      if (isNaN(this.testKitId)) {
        this.testKitId = 0;
      }
    });

    this.testkitEnpoint = new TestKitEnpoint();
    this.stepHeaderControl = {
      currentPage: 2,
      isCreatingTest: true,
      examId: this.examId,
      step: 2
    }
  }

  ngOnInit(): void {
    this.examApi.getExamStatus(this.examId)
      .then(res => {
        this.isPublished = res == 'PUB';
      })
      .catch();
    this.testKitPagingRequest = {
      gradeId: this.gradeId,
      subjectId: this.subjectId,
      testKitSelectedId: this.testKitId,
      page: 1,
      size: 10,
      getCount: true
    }
    this.getTestKitsData();
  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.testKitPagingRequest.page = this.paginator.pageIndex + 1;
      this.testKitPagingRequest.size = this.paginator.pageSize;
      this.getTestKitsData();
    });
  }

  getTestKitsData() {
    this.isLoadingResults = true;
    this.testkitEnpoint.getTestKitPaging(this.testKitPagingRequest).then(res => {
      this.isLoadingResults = false;
      this.testKit = res.items;
      this.totalTestKit = res.totalItems;
    }).catch(err => {
      console.log(err);
    });
  }
  //open file
  openFile() {
    if (this.isPublished) {
      return;
    }
    this.file.nativeElement.click();
  }

  //get file
  filesSelectedOnChange(files: File[]) {
    if (this.isPublished) {
      return;
    }
    this.router.navigate(['dashboard/examination-manager/exams/' + this.examId + '/steps/testkits/add-testkit-from-file'],
      { queryParams: { gradeId: this.gradeId, subjectId: this.subjectId, examSubjectId: this.subjectExamId, testKitId: this.testKitId } });

    return;
  }

  //open question bank
  openQuestionBank() {
    if (this.isPublished) {
      return;
    }
    this.router.navigate(['/dashboard/errors/feature-work-in-progress']);
  }

  //open matrix
  openMatrix() {
    if (this.isPublished) {
      return;
    }
    this.router.navigate(['/dashboard/errors/feature-work-in-progress']);
  }

  ngOnDestroy(): void {
    this.routeSub.unsubscribe();
  }

  chooseSubject(testKitId) {
    if (this.isPublished) {
      return;
    }

    if (this.testKitId == testKitId) {
      this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "choose-test"]);
    }

    let subjectTestkit: SubjectTestkitRequest = { id: this.subjectExamId, testKitId: testKitId }

    this.examApi.updateExamSubjectTestKit(this.subjectExamId, subjectTestkit)
      .then(() => {
        this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "choose-test"]);
      }).catch(err => {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Thêm đề thất bại!',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });

  }

  openSnackBar(message: string, type: 'error' | 'ok') {
    let panelClass = type == 'ok' ? 'green-snackbar' : 'red-snackbar';
    this._snackBar.open(message, 'Ok', {
      duration: 3000,
      panelClass: [panelClass],
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  deleteTestKit(data) {
    if (this.isPublished) {
      return;
    }

    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      disableClose: true,
      width: '444px',
      height: 'auto',
      //data: data
      data: {
        title: 'Xóa bộ đề',
        name: data.name,
        message: ''
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
            this.testkitEnpoint.deleteTestKit(data.id)
          .then(res => {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Xóa thành công!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          })
          .then(() => {
            this.getTestKitsData();
          })
          .catch((err: ErrorResponse) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Có lỗi xảy ra!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
      }
      else {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Bạn nhập chưa đúng',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    });


    // this.dialogConfirm = { title: 'XÓA BỘ ĐỀ', description: 'Bạn có chắc chắn muốn xóa bộ đề thi này?' }
    // const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result) {
    //     this.testkitEnpoint.deleteTestKit(testKitId)
    //       .then(res => {
    //         this._snackBar.openFromComponent(SuccessSnackBarComponent, {
    //           data: 'Xóa thành công!',
    //           duration: 3000,
    //           horizontalPosition: this.horizontalPosition,
    //           verticalPosition: this.verticalPosition,
    //         });
    //       })
    //       .then(() => {
    //         this.getTestKitsData();
    //       })
    //       .catch((err: ErrorResponse) => {
    //         this._snackBar.openFromComponent(ErrorSnackBarComponent, {
    //           data: 'Có lỗi xảy ra!',
    //           duration: 3000,
    //           horizontalPosition: this.horizontalPosition,
    //           verticalPosition: this.verticalPosition,
    //         });
    //       });
    //   }
    // });
  }

  editTest(testKitId) {
    if (this.isPublished) {
      return;
    }
    let testKitEdit = this.testKit.find(x => x.id == testKitId);
    this.router.navigate(['dashboard/examination-manager/exams/' + this.examId + '/steps/testkits/edit-testkit'],
      { queryParams: { gradeId: this.gradeId, subjectId: this.subjectId, examSubjectId: this.subjectExamId, testKitId: testKitId, type: testKitEdit.createType } });

  }

  viewTest(testId) {
    if (this.isPublished) {
      return;
    }
    this.router.navigate(['dashboard/examination-manager/list-testkit/', this.examId, this.gradeId, this.subjectId, this.subjectExamId, 'view-test']);
  }

  // get list question bank
  hanleGetListQuestionBank(idTestkit, testkitName) {
    this.testEnpoint.getTestsView(idTestkit).then(res => {
      if (res) {
        this.showDialogListQuestion(res, testkitName);
      }
    }).catch(err => console.log(err))
  }

  // show dialog
  showDialogListQuestion(listTest, testkitName) {
    this.dialogQuestionBankList = this.dialog.open(ViewExamFinishComponent, {
      width: '70vw',
      height: 'auto',
      data: {
        testkitName,
        listTest
      }
    });
  }

  backToContent() {
    this.router.navigate(['dashboard/examination-manager/exams/' + this.examId + '/steps/choose-test']);
  }
}

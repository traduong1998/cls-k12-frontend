import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTestkitComponent } from './list-testkit.component';

describe('ListTestkitComponent', () => {
  let component: ListTestkitComponent;
  let fixture: ComponentFixture<ListTestkitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListTestkitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTestkitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

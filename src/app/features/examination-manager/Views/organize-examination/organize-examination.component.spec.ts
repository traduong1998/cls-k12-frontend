import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizeExaminationComponent } from './organize-examination.component';

describe('OrganizeExaminationComponent', () => {
  let component: OrganizeExaminationComponent;
  let fixture: ComponentFixture<OrganizeExaminationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizeExaminationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizeExaminationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

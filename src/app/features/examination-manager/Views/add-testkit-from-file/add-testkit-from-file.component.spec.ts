import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTestkitFromFileComponent } from './add-testkit-from-file.component';

describe('AddTestkitFromFileComponent', () => {
  let component: AddTestkitFromFileComponent;
  let fixture: ComponentFixture<AddTestkitFromFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTestkitFromFileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTestkitFromFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

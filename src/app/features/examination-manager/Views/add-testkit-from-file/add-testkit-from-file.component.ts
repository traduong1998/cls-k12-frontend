import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { QuestionFileService } from 'src/app/core/services/question-file/question-file.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, of, Subscription } from 'rxjs';
import { Test } from 'sdk/cls-k12-sdk-js/src/services/test/models/test';
import { QuestionModel } from 'sdk/cls-k12-sdk-js/src/services/test/requests/question-model';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { TestKit } from 'sdk/cls-k12-sdk-js/src/services/testkit/models/testkit';
import { TestKitEnpoint } from 'sdk/cls-k12-sdk-js/src/services/testkit/enpoints/testkit-enpoint';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { TestEnpoint } from 'sdk/cls-k12-sdk-js/src/services/test/enpoint/test-enpoint';
import { CLS, ExamEnpoint, GradeEndpoint, QuestionBankEndpoint, SubjectEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { GradeInfo } from 'sdk/cls-k12-sdk-js/src/services/grade/requests/grade-info-request';
import { SubjectInfo } from 'sdk/cls-k12-sdk-js/src/services/subject/models/subject-info-request';
import { SubjectTestkitRequest } from 'sdk/cls-k12-sdk-js/src/services/exam/requests/update-subject-testkit-request';
import { QuestionExam } from '../../interfaces/question-exam';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { DialogConfirm } from '../../interfaces/dialog-confirm';
import { ToastComponent } from '../../Components/toast/toast.component';
import { DialogStatisticQuestionComponent } from '../../Components/dialog-statistic-question/dialog-statistic-question.component';
import { DialogConfirmComponent } from '../../Components/dialog-confirm/dialog-confirm.component';
import { UpdateScoreQuetionsDialogComponent } from '../../Components/update-score-quetions-dialog/update-score-quetions-dialog.component';
import { QuestionConverterEndpoint } from 'sdk/cls-k12-sdk-js/src/services/question-converter/endpoints/question-converter-endpoint';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/errorResponse';
import { UploadTestFilesReponse } from 'sdk/cls-k12-sdk-js/src/services/question-converter/responses/upload-test-files-response';
import { TestKitBankEnpoint } from 'sdk/cls-k12-sdk-js/src/services/testkit-bank/endpoints/testkit-bank-endpoint';
import { ProgressDialogComponent } from 'src/app/shared/progress-dialog/progress-dialog.component';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { ConfigUploadCkeditor } from 'src/app/core/interfaces/app-config';
import { AddMatrixTestkitDialogComponent } from '../../Components/add-matrix-testkit-dialog/add-matrix-testkit-dialog.component';
import { MatrixInfo, MatrixVM } from '../../interfaces/matrix-info';
import { QuestionBankDialogComponent } from '../../Components/question-bank-dialog/question-bank-dialog.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ViewExamFinishComponent } from '../view-exam-finish/view-exam-finish.component';
import { delay } from 'rxjs/operators';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';


@Component({
  selector: 'app-add-testkit-from-file',
  templateUrl: './add-testkit-from-file.component.html',
  styleUrls: ['./add-testkit-from-file.component.scss']
})
export class AddTestkitFromFileComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  @ViewChild('file') file: ElementRef;

  routeSub: Subscription;
  subjectId;
  examId;
  testKitId;
  subjectExamId;
  gradeId: number;
  matrixId?: number = null;
  createType: string;
  dialogConfirm: DialogConfirm;

  private testEnpoint: TestEnpoint;
  private testKitEnpoint: TestKitEnpoint;
  private gradeEnpoit: GradeEndpoint;
  private subjectEndpoint: SubjectEndpoint;
  private examApi: ExamEnpoint;
  private questionConverterEndpoint: QuestionConverterEndpoint;
  private testKitBankEnpoint: TestKitBankEnpoint;
  private questionBankEnpoint: QuestionBankEndpoint;
  //dữ liệu truyền đi
  question: QuestionModel = null;

  totalScore = 1;
  //data questionModel
  dataQuestionModel: QuestionModel[] = [];
  questions: QuestionModel[] = [];
  configUpload: ConfigUploadCkeditor;
  allComplete: boolean = false;
  // index của câu hỏi con đã chọn
  childIndex;
  //index câu hỏi đã chọn
  index;
  grade?: GradeInfo = {
    gradeId: 0,
    gradeName: '',
    orderNumber: 0,
  };
  subject?: SubjectInfo = {
    id: 0,
    name: '',
    orderNumber: 0
  };
  showInner = false;

  // Danh sách câu hỏi trả về sau convert file
  dataResponse: UploadTestFilesReponse;
  //danh sách câu hỏi đc thêm từ file
  data: QuestionExam[] = [];

  // lưu lại danh sách các câu hỏi theo từng đề
  //hiên thị
  displayedColumns: string[] = ['stt', 'content', 'score', 'delete']
  alphabet: string[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'V', 'X', 'Y', 'Z'];
  //bộ đề
  testKit: TestKit;
  //danh sách đề
  dataTest: Test[] = [];
  isPublished: boolean = true;

  listTest = [
    { id: 1, name: 'Đề số 1', data: [] }
  ]

  disableAddNewTest: boolean = false;

  testForm = this.fb.group({
    testName: new FormControl('', [Validators.required, Validators.maxLength(255)]),

  })
  submiting: boolean = false;
  public testCtrl: FormControl = new FormControl();
  progressData: Observable<any>;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>
  private dialogMatrixRef: MatDialogRef<AddMatrixTestkitDialogComponent>
  private dialogQuestionBankRef: MatDialogRef<QuestionBankDialogComponent>
  percent: number = 0;
  private dialogQuestionBankList: MatDialogRef<ViewExamFinishComponent>;
  totalCountCurrentTest: number = 0;
  totalScoreCurrentTest: number = 0;
  constructor(private _snackBar: MatSnackBar, private fb: FormBuilder, public dialog: MatDialog, private questionFilleService: QuestionFileService, private router: Router,
    private route: ActivatedRoute, private changeDetectorRefs: ChangeDetectorRef, private _authService: AuthService, private configService: AppConfigService) {
    this.gradeEnpoit = new GradeEndpoint({ baseUrl: 'http://localhost:65000' });
    this.subjectEndpoint = new SubjectEndpoint({ baseUrl: 'http://localhost:65000' });
    this.testEnpoint = new TestEnpoint();
    this.testKitEnpoint = new TestKitEnpoint();
    this.examApi = new ExamEnpoint();
    this.questionConverterEndpoint = new QuestionConverterEndpoint();
    this.testKitBankEnpoint = new TestKitBankEnpoint();
    this.questionBankEnpoint = new QuestionBankEndpoint();

    this.routeSub = this.route.params.subscribe(params => {
      this.examId = + params['id']
    });
    this.route.queryParams.subscribe(params => {
      this.gradeId = +params['gradeId'];

      this.subjectId = +params['subjectId'];

      this.subjectExamId = +params['examSubjectId'];

      this.testKitId = +params['testKitId'];

      this.createType = params['type'];
    });

    let baseUrl = CLS.getConfig().apiBaseUrls.fileServer ?? CLS.getConfig().apiBaseUrl;
    baseUrl += '/api';

    this.configUpload = {
      portalId: configService.getConfig().unit.portalId,
      token: _authService.getToken(),
      uploadUrl: baseUrl + '/file/upload'
    }

    this.baseApiUrl = CLS.getConfig().apiBaseUrl;

  }

  get name() { return this.testForm.get('testName'); }

  ngOnInit(): void {
    this.examApi.getExamStatus(this.examId)
      .then(res => {
        this.isPublished = res == 'PUB';
        if (this.isPublished) {
          this.router.navigate(['dashboard/examination-manager/exams/' + this.examId + '/steps/testkits'],
            { queryParams: { gradeId: this.gradeId, subjectId: this.subjectId, examSubjectId: this.subjectExamId, testKitId: this.testKitId } });
        }
      })
      .catch();

    //lấy thông tin khối
    this.gradeEnpoit.getGradeInfo(this.gradeId).then(res => {
      if (res) {
        this.grade = res;
      }
    })

    //lây thông tin môn
    this.subjectEndpoint.getSubjectInfo(this.subjectId).then(res => {
      if (res) {
        this.subject = res;
      }
    })

    this.testCtrl.setValue(this.listTest[0].id);

    //this.matrixId = 6;
  }

  ngAfterViewInit() {

  }

  addNewTest() {
    if (this.listTest.length == 3) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Bộ đề được giới hạn tối đa 3 đề thi',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });

      return;
    }

    if (this.disableAddNewTest == true) {
      return;
    }
    this.disableAddNewTest = true

    setTimeout(() => {
      this.disableAddNewTest = false;
    }, 3000);

    let currentTotalTest = this.listTest.length;
    let newTestId = currentTotalTest + 1;
    this.listTest.push({ id: newTestId, name: 'Đề số ' + newTestId, data: [] });
    this.testCtrl.setValue(newTestId);

    this.data = [];
    this.countQuestionTest();
    this.question = null;
    this.setAll(false);

    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
      data: 'Đã thêm đề mới Đề số ' + newTestId + '. Thêm câu hỏi vào đề thi để hoàn thành việc tạo đề',
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }

  onChangeTestSelected(id) {
    let test = this.listTest.find(x => x.id == id);
    this.data = test.data;
    this.countQuestionTest();
    this.question = null;
    this.setAll(false);
  }

  //open file 
  openFile() {
    if (this.isPublished) {
      return;
    }
    this.file.nativeElement.click();
  }

  filesSelectedOnChange(files: File[]) {
    if (this.isPublished) {
      return;
    }

    if (files.length != 0) {
      this.openProgressDialog(null);
      this.questionConverterEndpoint.uploadTestFiles(files,
        {
          onUploadProgress: (progressEvent) => {
            this.progressData = progressEvent;
            var subscription = this.getprogressData().subscribe(value => {
              if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: files[0].name };
              }
            })
          }
        }, ServiceName.EXAM)
        .then(res => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: files[0].name };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
          console.log(res);
          this.file.nativeElement.value = '';
          this.dataResponse = res;
          this.data = res[0].questions;
          this.countQuestionTest();
          let currentTestSelected = this.testCtrl.value;

          this.listTest.map(test => {
            if (test.id == currentTestSelected) {
              test.data = this.data;
            }

            return test;
          });
        })
        .catch((err: ErrorResponse) => {
          this.file.nativeElement.value = '';
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: files[0].name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }
  submitForm() {
    if (this.testForm.invalid) {
      //sai
    }
    else {
      // valid xem các đề thi đã có danh sách câu hỏi chưa
      let errorDetail: string = '';
      this.listTest.forEach((e, i) => {
        if (e.data.length == 0) {
          errorDetail += 'Đề số ' + (i + 1) + ', ';
        }
      });
      this.submiting = true;
      if (errorDetail != '') {
        errorDetail = 'Bạn cần tải lên danh sách câu hỏi cho: ' + errorDetail.substring(0, errorDetail.length - 2);
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: errorDetail,
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        this.submiting = false;
        return;
      }
      // ---------------

      this.testKit = {
        id: null, gradeId: this.gradeId, subjectId: this.subjectId,
        createType: this.createType, matrixId: this.matrixId, name: this.name.value, numberTest: this.listTest.length
      }
      this.testKitEnpoint.createTestkit(this.testKit).then(res => {
        if (res) {
          this.testKitId = res.id;
          this.createTest();
        } else {
          this.submiting = false;
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Thêm thất bại!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      })
        .then(() => {
          let subjectTestkit: SubjectTestkitRequest = { id: this.subjectExamId, testKitId: this.testKitId }
          this.examApi.updateExamSubjectTestKit(this.subjectExamId, subjectTestkit)
            .then(() => {
              this.submiting = false;
              this.router.navigate(['dashboard/examination-manager/exams/' + this.examId + '/steps/testkits'],
                { queryParams: { gradeId: this.gradeId, subjectId: this.subjectId, examSubjectId: this.subjectExamId, testKitId: this.testKitId } });

            }).catch(err => {
              this.submiting = false;

              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Thêm đề môn thất bại!',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            });
        })
        .catch(err => {
          this.submiting = false;
          console.log(err);
        })
    }
  }

  async createTest() {

    let testData = [];
    console.log(`listTest:9999999999 `, this.listTest);
    this.listTest.forEach(x => {

      let dataParsing = this.createQuestionRequest(x.data);

      console.log(dataParsing);
      // data lưu bộ đề file
      testData.push({ id: null, config: null, questions: dataParsing });

      // data lưu cơ sở dữ liệu
      this.dataTest.push(
        {
          name: x.name, gradeId: this.gradeId, subjectId: this.subjectId, testKitId: this.testKitId, examId: this.examId,
          questions: dataParsing
        }
      );
    })
    let index = 0;
    //this.dataTest.forEach(object => {
    for (const object of this.dataTest) {
      await this.testEnpoint.createTest(object).then(res => {
        if (res) {
          testData[index].id = res.id;
          console.log('okok')
        }
        else {
          console.log('fail')
        }
      })
        .catch((err: ErrorResponse) => {
          this.submiting = false;
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
    };
  }

  //lấy danh sách câu hỏi từ file
  getQuestionData() {
    // this.testEnpoint.getTest(112).then(res => {
    //   let filePath = res.urlJsonTest;
    //   this.testEnpoint.getFileTest(filePath).then(
    //     res => {
    //       this.data = res.questions;
    //     }
    //   )
    //     .catch((err: ErrorResponse) => {
    //       this._snackBar.open(err.errorDetail, 'Ok', {
    //         duration: 5000,
    //         panelClass: ['red-snackbar'],
    //         horizontalPosition: 'center',
    //         verticalPosition: 'top',
    //       });
    //     });

    // });
  }

  // Khởi tạo những câu hỏi đc thêm vào
  createQuestionRequest(data) {

    let i = 0;
    this.dataQuestionModel = [];
    data.forEach(obj => {
      i += 1;
      if (obj.questions != null && obj.format == FormatQuestion.group) {
        let questions: QuestionModel[] = [];
        obj.questions.forEach(obj => {
          questions.push(
            {
              score: obj.score,
              content: obj.content,
              type: obj.type,
              level: obj.level,
              format: FormatQuestion.group,
              contentPanelId: 0,
              chapterId: 0,
              unitId: 0,
              questions: [],
              answers: obj.answers
            });
        });

        this.dataQuestionModel.push(
          {
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: questions,
            answers: []
          });
      }
      else {
        this.dataQuestionModel.push(
          {
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: [],
            answers: obj.answers
          });
      }
    })

    console.log(`this.dataQuestionModel:99: `, this.dataQuestionModel);
    return this.dataQuestionModel;
  }

  loadData(data) {
    this.data = [];
    this.data = [...this.data, ...data];
    this.countQuestionTest();
    this.changeDetectorRefs.detectChanges();
  }

  showStatisticQuestion(data): void {
    console.log(`data:99: `, data);
    let dataQuestions: QuestionModel[] = [];
    data.forEach(obj => {
      if (obj.questions != null && obj.format == FormatQuestion.group) {
        let questions: QuestionModel[] = [];
        obj.questions.forEach(obj => {
          questions.push(
            {
              score: obj.score,
              content: obj.content,
              type: obj.type,
              level: obj.level,
              format: FormatQuestion.group,
              contentPanelId: 0,
              chapterId: 0,
              unitId: 0,
              questions: [],
              answers: obj.answers
            });
        });

        dataQuestions.push(
          {
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: questions,
            answers: []
          });
      }
      else {
        dataQuestions.push(
          {
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: [],
            answers: obj.answers
          });
      }
    })

    const dialogRef = this.dialog.open(DialogStatisticQuestionComponent, {
      width: '800px',
      height: 'auto',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  delete(element, i, ic) {
    this.dialogConfirm = { title: 'XÓA CÂU HỎI', description: 'Bạn có chắc chắn muốn xóa câu hỏi này?' }
    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

        // là câu chùm, đang xóa câu con của câu chùm
        if (i != undefined) {
          if (this.data[i].questions.length == 1) {
            // cầu chùm phải có ít nhất 1 câu con
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Câu chùm phải có ít nhất 1 câu con!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            return;
          } else {
            // cập nhật lại điểm câu chùm
            this.updateScore(ic, 0, i);
            this.data[i].questions.splice(ic, 1);
          }

          // reset câu hỏi đã chọn
          if (this.index == i) {
            this.question = null;
          }
        }
        else {
          let index = this.data.indexOf(element);
          if (index != -1) {
            this.data.splice(index, 1);
          }

          // reset câu hỏi đã chọn
          if (this.index == index) {
            this.question = null;
          }
        }

        this.loadData(this.data);

        let currentTestSelected = this.testCtrl.value;

        this.listTest.map(test => {
          if (test.id == currentTestSelected) {
            test.data = this.data;
          }

          return test;
        });

        // thống kê lại số lượng câu hỏi, tổng điểm
        this.countQuestionTest();
      }
    });
  }

  chooseQuestion(item, i, ic) {
    this.index = i;
    this.question = item;

    if (ic == undefined) {
      this.childIndex = 1;
    } else {
      this.childIndex = ic + 1;
    }

    console.log('item', item)
  }

  updateQuestion(question) {
    this.data[this.index] = question;
    this.loadData(this.data);

    let currentTestSelected = this.testCtrl.value;

    this.listTest.map(test => {
      if (test.id == currentTestSelected) {
        //test.data = this.data;
        test.data.map((qs, i) => {
          if (i != this.index) {
            return qs;
          } else {
            qs = question;
          }
        })
      }

      return test;
    });

    console.log(`this.listTest:999999: `, this.listTest);
  }

  updateScore(index, score, parentIndex) {
    let currentTestId = this.testCtrl.value;
    // --- CẬP NHẬT ĐIỂM CÂU ĐƠN
    if (parentIndex == null) {
      this.data.map((question, i) => {
        if (i == index) {
          if (question.format != FormatQuestion.group) {
            question.score = score;
          }
        }

        return question;
      });
    }
    else {
      this.data.map((question, i) => {
        // --- CẬP NHẬT ĐIỂM CÂU CON CỦA CÂU CHÙM
        if (i == parentIndex) {
          if (question.format == FormatQuestion.group) {
            let totalQuestionGroupScore = 0;
            question.questions.map((child, j) => {
              if (j == index) {
                child.score = score;
              }
              totalQuestionGroupScore += child.score;

              return child;
            });
            debugger;
            question.score = parseFloat(totalQuestionGroupScore.toFixed(2));
          }
        }
      });
    }


    this.listTest.map(test => {
      if (test.id == currentTestId) {
        test.data = this.data
        console.log(`this.listTest999: `, test);
      }

      return test;
    });
    this.countQuestionTest();
  }

  save() {
    //lưu danh sách đề thi
    this.submitForm();
  }
  cancel() {
    this.router.navigate(['dashboard/examination-manager/exams/' + this.examId + '/steps/testkits'],
      { queryParams: { gradeId: this.gradeId, subjectId: this.subjectId, examSubjectId: this.subjectExamId, testKitId: this.testKitId } });
  }

  deleteSelected() {
    this.dialogConfirm = { title: 'XÓA CÂU HỎI', description: 'Bạn có chắc chắn muốn xóa các câu hỏi đã chọn?' }
    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // Xóa câu con trước
        this.data.forEach((qs, i) => {
          if (qs.format == FormatQuestion.group) {
            // update la
            // lọc bỏ những câu hỏi con
            qs.questions.map((x, ic) => {
              if (x.isChecked) {
                // cập nhật lại điểm câu chùm
                this.updateScore(ic, 0, i);
              }
            });
            qs.questions = qs.questions.filter(x => !x.isChecked);
          }
          // reset câu hỏi đã chọn
          if (this.index == i) {
            this.question = null;
          }
        });

        this.data = this.data.filter(x => !x.isChecked);
        this.countQuestionTest();
        this.loadData(this.data);
        let currentTestSelected = this.testCtrl.value;

        this.listTest.map(test => {
          if (test.id == currentTestSelected) {
            test.data = this.data;
          }

          return test;
        });
      }
    });

  }

  deleteTest() {
    let currentTestSelectedId = this.testCtrl.value;
    let test = this.listTest.find(x => x.id == currentTestSelectedId);

    this.dialogConfirm = { title: 'XÓA ĐỀ THI', description: 'Bạn có chắc chắn muốn xóa đề thi "' + test.name + '"?' }
    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.data = this.data.filter(x => !x.isChecked);
        if (this.listTest.length <= 1) {
          this.listTest = [
            { id: 1, name: 'Đề số 1', data: [] }
          ]
        }
        else {
          this.listTest = this.listTest.filter(x => x.id != test.id);
          this.listTest.map(t => {
            if (t.id > test.id) {
              t.id--;
              t.name = 'Đề số ' + t.id;
            }
          })
        }

        let currentTest = this.listTest[0];
        this.testCtrl.setValue(currentTest.id);
        this.data = currentTest.data;
        this.countQuestionTest();
        this.question = null;
        this.setAll(false);
      }
    });
  }
  someComplete() {
    if (this.data == undefined) return false;

    let somchecked = false;
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].isChecked) {
        somchecked = true;
        break;
      }
      if (this.data[i].questions != null) {
        for (let j = 0; j < this.data[i].questions.length; j++) {
          if (this.data[i].questions[j].isChecked) {
            somchecked = true;
            break;
          }
        }
      }
    }
    return somchecked;
  }

  updateAllComplete() {
    if (this.data == undefined) return false;
    let allChecked = true;
    for (let i = 0; i < this.data.length; i++) {
      if (!this.data[i].isChecked) {
        allChecked = false;
        break;
      }
      if (this.data[i].questions != null) {
        for (let j = 0; j < this.data[i].questions.length; j++) {
          if (!this.data[i].questions[j].isChecked) {
            allChecked = false;
            break;
          }
        }
      }
    }
    this.allComplete = allChecked;
  }

  setAll(completed: boolean): void {
    this.allComplete = completed;
    if (this.data != null) {
      this.data.map((question) => {
        if (question.format == FormatQuestion.group) {
          question.isChecked = completed;
          question.questions.forEach((child) => {
            child.isChecked = completed
          });
        } else {
          question.isChecked = completed
        }
      });
    }
  }

  openUpdateScoreDialog() {
    console.log(`this.questions: `, this.data);
    if (!this.someComplete()) {
      alert('Không có câu hỏi nào được chọn!');
      return;
    }
    const dialogRef = this.dialog.open(UpdateScoreQuetionsDialogComponent, {
      width: '377px',
      height: '234px',
      data: this.totalScore
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      if (result) {
        this.updateScoreQuestions(result);
      }
    });
  }

  updateScoreQuestions(score: number): void {
    let currentTestId = this.testCtrl.value;
    this.data.map((question) => {
      if (question.isChecked) {
        if (question.format != FormatQuestion.group) {
          question.score = score;
        }

        if (question.format == FormatQuestion.group) {
          let totalQuestionGroupScore = 0;
          question.questions.forEach((child) => {
            child.score = score;
            totalQuestionGroupScore += score;
          });
          question.score = Math.round((totalQuestionGroupScore + Number.EPSILON) * 100) / 100
        }
        question.isChecked = false;
      }
    });

    this.listTest.map(test => {
      if (test.id == currentTestId) {
        test.data = this.data
      }

      return test;
    });
    this.allComplete = false;
    this.setAll(false);
    this.countQuestionTest();
  }
  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.percent }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openMatrixDialog() {
    let matrixInfo: MatrixInfo = {
      gradeId: this.gradeId,
      subjectId: this.subjectId,
      subjectName: this.subject.name,
      gradeName: this.grade.gradeName,
      matrixId: this.matrixId
    }

    this.dialogMatrixRef = this.dialog.open(AddMatrixTestkitDialogComponent, {
      width: '80vw',
      height: 'auto',
      data: matrixInfo
    });

    this.dialogMatrixRef.afterClosed().subscribe((result: any) => {
      if (result != 'cancel') {
        //result = {matrixId: 0, data: []}
        this.matrixId = result.matrixId;
        if (result.data.length > 0) {
          this.listTest = [];
          // khởi tạo danh sách các đề thi
          result.data.forEach((test, i) => {
            this.listTest.push({ id: (i + 1), name: 'Đề số ' + (i + 1), data: test.questions });
          });

          this.testCtrl.setValue(this.listTest[0].id);
          this.data = this.listTest[0].data;
          this.countQuestionTest();
          this.question = null;
        }
      }
    });

  }
  openQuestionBankDialog() {
    let currentTestSelected = this.testCtrl.value;
    let currentTest = this.listTest.find(x => x.id == currentTestSelected);
    let selectedQuestionIds = currentTest.data.map(x => x.id);

    this.dialogQuestionBankRef = this.dialog.open(QuestionBankDialogComponent, {
      width: '80vw',
      height: 'auto',
      // danh sách các id các câu hỏi đã chọn của đề hiện tại, disabled để tránh chọn lại trong danh sách câu hỏi từ ngân hàng
      data: selectedQuestionIds
    });

    this.dialogQuestionBankRef.afterClosed().subscribe((result: any) => {
      if (result != 'cancel') {
        // lọc ra những questions đã chọn, vì chọn selectAll bên NHCH thì sẽ thêm vào luôn những question đã chọn
        if (selectedQuestionIds) {
          result = result.filter(x => selectedQuestionIds.indexOf(x.id) == -1);
        }
        result.map(x => {
          this.data.push(x);
        })

        let currentTestSelected = this.testCtrl.value;

        this.listTest.map(test => {
          if (test.id == currentTestSelected) {
            test.data = this.data;
          }

          return test;
        });
        this.countQuestionTest();
      }
    });
  }
  hanleGetListQuestionBank() {
    this.showDialogListQuestion(this.listTest, 'Xem trước bộ đề thi');
  }

  // show dialog
  showDialogListQuestion(listTest, testkitName) {
    this.dialogQuestionBankList = this.dialog.open(ViewExamFinishComponent, {
      width: '70vw',
      height: 'auto',
      data: {
        testkitName,
        listTest,
        preview: true,
        selectedTestId: this.testCtrl.value
      }
    });
    this.showInner = true;
    this.dialog.afterAllClosed.subscribe(() => this.showInner = false)
  }

  countQuestionTest() {
    this.totalCountCurrentTest = this.data.length;
    this.totalScoreCurrentTest = 0;
    this.data.forEach(obj => {
      this.totalScoreCurrentTest = Math.round((this.totalScoreCurrentTest + obj.score) * 100) / 100;
    })

  }

  public getLinkTemplate(cases: number) {
    if (cases == 1) {
      return `${this.baseApiUrl}/exam/public/files/cdn/template-file/tep-tin-mau.docx`;
    }
    else if (cases == 2) {
      return `${this.baseApiUrl}/exam/public/files/cdn/template-file/huong-dan-su-dung-tep-tin-mau.docx`;
    }
  }
}

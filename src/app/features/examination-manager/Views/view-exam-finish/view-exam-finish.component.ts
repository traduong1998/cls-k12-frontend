import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TestEnpoint } from 'sdk/cls-k12-sdk-js/src/services/test/enpoint/test-enpoint';

@Component({
  selector: 'app-view-exam-finish',
  templateUrl: './view-exam-finish.component.html',
  styleUrls: ['./view-exam-finish.component.scss']
})
export class ViewExamFinishComponent implements OnInit {
  questionField;
  listQuestion = [];
  private testEnpoint: TestEnpoint;
  listQuestionFromExam;
  totalQuestion: number = 0;
  totalScore: number = 0;
  preview: boolean = false;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.testEnpoint = new TestEnpoint();
  }

  ngOnInit(): void {
    if (this.data) {
      if (this.data.preview != undefined) {
        this.preview = true;
      }
      this.listQuestion = this.data.listTest;
      let selectedTestIndex = 0;
      if (this.data.selectedTestId != undefined) {
        selectedTestIndex = this.listQuestion.findIndex(x => x.id == this.data.selectedTestId);
      }
      if (this.listQuestion.length > 0) {
        this.questionField = this.listQuestion[selectedTestIndex].id;
        this.onQuestionFieldClicked(selectedTestIndex)
      } else {
        this.listQuestionFromExam = [];
        this.totalQuestion = 0;
      }
    }
  }

  onQuestionFieldClicked(index) {
    if (this.preview) {
      this.listQuestionFromExam = { questions: this.listQuestion[index].data };
      this.totalQuestion = this.listQuestionFromExam.questions.length;
      let score = 0;
      this.listQuestionFromExam.questions.forEach((sc) => {
        score = Math.round((score +  sc.score) * 100) / 100;
      });
      this.totalScore = score
    }
    else {
      let urlFile = this.listQuestion[index].urlJsonTest;
      this.listQuestionFromExam = [];
      this.testEnpoint.getFileTest(urlFile).then(list => {
        if (list) {
          this.listQuestionFromExam = list;
          this.totalQuestion = this.listQuestionFromExam.questions.length;
          let score = 0;
          this.listQuestionFromExam.questions.forEach((sc) => {
             score = Math.round((score +  sc.score) * 100) / 100;
          });
          this.totalScore = score
        }
      });
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewExamFinishComponent } from './view-exam-finish.component';

describe('ViewExamFinishComponent', () => {
  let component: ViewExamFinishComponent;
  let fixture: ComponentFixture<ViewExamFinishComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewExamFinishComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewExamFinishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

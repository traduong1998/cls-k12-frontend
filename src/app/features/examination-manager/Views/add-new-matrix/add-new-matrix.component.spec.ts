import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewMatrixComponent } from './add-new-matrix.component';

describe('AddNewMatrixComponent', () => {
  let component: AddNewMatrixComponent;
  let fixture: ComponentFixture<AddNewMatrixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddNewMatrixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-new-matrix',
  templateUrl: './add-new-matrix.component.html',
  styleUrls: ['./add-new-matrix.component.scss']
})
export class AddNewMatrixComponent implements OnInit {
  subjectId;
  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.subjectId = params['id'];
      console.log(`this.subjectId:999`,this.subjectId)
    });
  }
  saveData() {
    this.router.navigate(["dashboard/examination-manager/list-testkit/", this.subjectId, "matrix", "view-test"])
  }
  cancel() {
    this.router.navigate(["/dashboard/examination-manager/list-testkit/", this.subjectId, "matrix"])
  }
}

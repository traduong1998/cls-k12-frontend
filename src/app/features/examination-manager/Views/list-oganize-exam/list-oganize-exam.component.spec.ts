import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOganizeExamComponent } from './list-oganize-exam.component';

describe('ListOganizeExamComponent', () => {
  let component: ListOganizeExamComponent;
  let fixture: ComponentFixture<ListOganizeExamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListOganizeExamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOganizeExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

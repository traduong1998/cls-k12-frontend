import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { ExamEnpoint, ExamInfo, GradeEndpoint, UserIdentity } from 'cls-k12-sdk-js/src';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { OrganizeExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/endpoints/organize-exam-endpoint';
import { ExamSchool } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/ExamSchool';
import { ExamSchoolInfo } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/ExamSchoolInfo';
import { GetListExamSchoolPagingRequest } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/request/get-list-exam-school-paging-request';
import { AuthService } from 'src/app/core/services';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'src/app/shared/services/message.service';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';
@Component({
  selector: 'app-list-oganize-exam',
  templateUrl: './list-oganize-exam.component.html',
  styleUrls: ['./list-oganize-exam.component.scss']
})
export class ListOganizeExamComponent implements OnInit {
  displayedColumns: string[] = ['stt', 'name', 'grade', 'status', 'type', 'startTime', 'endTime', 'function'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  public statusCtrl: FormControl = new FormControl();
  public examTypeCtrl: FormControl = new FormControl();

  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected _onDestroy = new Subject<void>();
  examSchoolPaginationFilterClone: GetListExamSchoolPagingRequest;
  examSchoolPaginationFilter: GetListExamSchoolPagingRequest = {
    keyword: "",
    type: "",
    fromDate: null,
    toDate: null,
    createLevel: "",
    gradeId: 0,
    pageNumber: 1,
    pageSize: 10,
    sortField: "NAME",
    sortDirection: "DESC",
    listIds: []
  };
  isFirstLoadAllGrades: boolean = true;
  allGrades: GradeOption[] = []

  userIdentity: UserIdentity;
  organizeExamEndpoint: OrganizeExamEndpoint;
  dataExamReponse: ExamSchool[] = [];
  schoolInfo: ExamSchoolInfo[];
  listExamChoose: number[] = [];
  isFirstLoadGrades: boolean = true;
  totalItems = 0;
  isShowFilter = true;
  private examApi: ExamEnpoint;
  private endpointGrade: GradeEndpoint;
  isFullScreen: boolean = true;
  constructor(private router: Router, private dialog: MatDialog, private _snackBar: MatSnackBar, private _authService: AuthService, private spinner: NgxSpinnerService, private _messageService: MessageService) {

    this.organizeExamEndpoint = new OrganizeExamEndpoint();
    this.endpointGrade = new GradeEndpoint();
    this.userIdentity = _authService.getTokenInfo();
  }

  ngOnInit(): void {
    this.getListExamApi();
    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });
  }
  ngAfterViewInit() {
    this.setInitialValue();
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.examSchoolPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.examSchoolPaginationFilter.pageSize = this.paginator.pageSize;
      this.getListExamApi();
    });
  }
  getListExamApi() {
    this.isFullScreen = false;
    this.spinner.show();
    this.organizeExamEndpoint.getListExamSchoolInfo().then((res) => {
      this.schoolInfo = res;
      if (res.length > 0) {
        let listIds = this.schoolInfo.map(x => x.examId);
        this.examSchoolPaginationFilter.listIds = listIds;
      }
      if (res.length > 0) {
        this.organizeExamEndpoint.getListExamSchoolPaging(this.examSchoolPaginationFilter)
          .then((res) => {
            if (res) {
              this.examSchoolPaginationFilterClone={...this.examSchoolPaginationFilter};
              this.dataExamReponse = res.items;
              this.totalItems = res.totalItems;
              if (this.isFirstLoadAllGrades) {
                this.endpointGrade.getGradeOptions(null)
                  .then(res => {
                    this.isFirstLoadAllGrades = false;
                    this.allGrades = res;

                    this.dataExamReponse = this.dataExamReponse.map((exam) => {
                      let grade = this.allGrades.find(x => x.id == exam.gradeId);
                      if (grade != undefined) {
                        exam.gradeName = grade.name;
                      }

                      let school = this.schoolInfo.find(x => x.examId == exam.id);
                      exam.status = school.status;
                      if (exam.type == 'CON') {
                        exam.type = 'Tập trung';
                      } else {
                        exam.type = 'Tự do';
                      }

                      return exam;
                    });

                  })
                  .catch();
              }
              else {
                this.dataExamReponse = this.dataExamReponse.map((exam) => {
                  let grade = this.allGrades.find(x => x.id == exam.gradeId);
                  if (grade != undefined) {
                    exam.gradeName = grade.name;
                  }

                  let school = this.schoolInfo.find(x => x.examId == exam.id);
                  exam.status = school.status;
                  if (exam.type == 'CON') {
                    exam.type = 'Tập trung';
                  } else {
                    exam.type = 'Tự do';
                  }

                  return exam;
                });
              }
              this.spinner.hide();
              this.loadData(this.dataExamReponse);
            }
            else {
              this.spinner.hide();
              //to do
            }
            this.spinner.hide();
          });

          this.spinner.hide();
      }
      this.spinner.hide();
    }).catch((ex) => {
      console.log(ex);
      this.spinner.hide();
      this._messageService.failureCallback("Có lỗi xảy ra khi vui lòng thử lại");
    });
    this.spinner.hide();
  }

  loadData(data) {
    this.dataExamReponse = []
    this.dataExamReponse = [...this.dataExamReponse, ...data]
  }



  organization(examId, gradeId) {
    this.router.navigate(['dashboard/examination-manager/organize/', examId, 'grade', gradeId, 'examsubject']);
  }


  chooseExam(event, examId) {
    if (event.checked) {
      this.listExamChoose.push(examId);
    } else {
      let index = this.listExamChoose.findIndex(element => element == examId);
      if (index != -1) {
        this.listExamChoose.splice(index, 1);
      }
    }
  }

  onGradeSelectClicked() {
    let schoolIdSelected = 0;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrades) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch()
    }
  }

  protected setInitialValue() {
    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectGrade != undefined) {
          this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
        }
      });
  }
  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onSubmitFilter() {
    this.getFilterParam();
    if (!compareTwoObject(this.examSchoolPaginationFilter, this.examSchoolPaginationFilterClone)) {
      this.getListExamApi();
    }
  }

  getFilterParam() {
    this.examSchoolPaginationFilter.gradeId = +this.gradeCtrl.value;
    this.examSchoolPaginationFilter.type = this.examTypeCtrl.value;
  }
  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { CLSModules, CLSPermissions, DivisionEndpoint, DivisionOption, ExamInfo, GetExamFilterRequests, GradeEndpoint, SchoolEndpoint, SubjectEndpoint, UserEndpoint, UserIdentity } from 'cls-k12-sdk-js/src';
import { ReplaySubject, Subject as RxSubject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/PaginatorResponse';
import { ExamEnpoint } from 'sdk/cls-k12-sdk-js/src/services/exam/endpoints/exam-enpoint';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { SubjectOption } from 'sdk/cls-k12-sdk-js/src/services/subject/models/Subject';
import { AuthService } from 'src/app/core/services';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { DialogConfirmComponent } from '../../Components/dialog-confirm/dialog-confirm.component';
import { ToastComponent } from '../../Components/toast/toast.component';
import { DialogConfirm } from '../../interfaces/dialog-confirm';

@Component({
  selector: 'app-list-examination',
  templateUrl: './list-examination.component.html',
  styleUrls: ['./list-examination.component.scss']
})
export class ListExaminationComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('dpFromDate') dpFromDate;
  @ViewChild('dpToDate') dpToDate;

  isShowFilter = true;

  startDate: FormControl = new FormControl('');

  baseApiUrl: string = "";
  protected allGrades: GradeOption[];

  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected divisions: DivisionOption[];
  public divisionCtrl: FormControl = new FormControl();
  public divisionFilterCtrl: FormControl = new FormControl();
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  protected schools: SchoolOption[];
  public schoolCtrl: FormControl = new FormControl();
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  // protected subjects: SubjectOption[];
  // public subjectCtrl: FormControl = new FormControl();
  // public subjectFilterCtrl: FormControl = new FormControl();
  // public filteredSubjects: ReplaySubject<SubjectOption[]> = new ReplaySubject<SubjectOption[]>(1);
  // @ViewChild('singleSelectSubject', { static: true }) singleSelectSubject: MatSelect;

  protected _onDestroy = new RxSubject<void>();

  public statusCtrl: FormControl = new FormControl();
  public statusExamCtrl: FormControl = new FormControl();
  public examTypeCtrl: FormControl = new FormControl();
  public fromDateCtrl: FormControl = new FormControl('');
  public toDateCtrl: FormControl = new FormControl('');

  startDateFilter: Date = null;
  endDateFilter: Date = null;

  filterForm = this.fb.group({
    fromDate: new FormControl(),
    toDate: '',
    search: new FormControl(''),
    gradeId: new FormControl(null, [Validators.required])
  });

  isFirstLoadDivisions: boolean = true;
  isFirstLoadSchools: boolean = true;
  isFirstLoadGrades: boolean = true;
  //isFirstLoadSubjects: boolean = true;
  isFirstLoadAllGrades: boolean = true;

  levelManageValue: number = 0;

  dialogConfirm: DialogConfirm;
  listExamChoose: number[] = [];

  dataExamPagination: PaginatorResponse<ExamInfo> = {
    items: [],
    totalItems: 0
  };
  getExamFilterRequest: GetExamFilterRequests = {
    pageNumber: 1,
    pageSize: 10,
    divisionId: null,
    schoolId: null,
    gradeId: null,
    subjectId: null,
    fromDate: null,
    toDate: null,
    scope: null,
    type: null,
    statusEdit: null,
    statusExam: null,
    getCount: true,

    filter: {
      sortDirection: 'DESC',
      sortField: 'CRE',
    },
  }

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  subjectFilter = {
    gradeId: null
  }
  displayedColumns: string[] = ['select', 'stt', 'name', 'donviName', 'grade', 'creator', 'status', 'scope', 'startTime', 'endTime', 'function'];

  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  private endpointGrade: GradeEndpoint;
  private examEndpoint: ExamEnpoint;
  private endpointSubject: SubjectEndpoint;
  private endpointUser: UserEndpoint;

  userIdentity: UserIdentity;
  hasApprovePermission: boolean;
  hasAddPermission: boolean;
  hasDeletePermission: boolean;
  hasEditPermission: boolean;
  hasAssignPermission: boolean;
  examFilterClone;
  isCheckAll: boolean = false;
  constructor(private fb: FormBuilder, private router: Router, private dialog: MatDialog, private _snackBar: MatSnackBar, private _authService: AuthService,) {
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubject = new SubjectEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUser = new UserEndpoint({ baseUrl: this.baseApiUrl });

    this.examEndpoint = new ExamEnpoint();
    this.userIdentity = _authService.getTokenInfo();
    console.log(this.userIdentity);
    this.hasApprovePermission = this.userIdentity.hasPermission(CLSModules.Exam, CLSPermissions.Approved);
    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.Exam, CLSPermissions.Add);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.Exam, CLSPermissions.Edit);
    this.hasDeletePermission = this.userIdentity.hasPermission(CLSModules.Exam, CLSPermissions.Delete);
    this.hasAssignPermission = this.userIdentity.hasPermission(CLSModules.Exam, CLSPermissions.Assign);

    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    }
    else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.getExamFilterRequest.divisionId = this.userIdentity.divisionId;
    }
    else if (this.userIdentity.levelManage == "SCH") {
      this.levelManageValue = 3;
      this.getExamFilterRequest.schoolId = this.userIdentity.schoolId;
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
    }
  }

  ngOnInit(): void {
    this.getListExamData();
    // listen for search field value changes
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });

    // listen for search field value changes
    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });
    // // listen for search field value changes
    // this.subjectFilterCtrl.valueChanges
    //   .pipe(takeUntil(this._onDestroy))
    //   .subscribe(() => {
    //     this.filterSubjects();
    //   });
  }
  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.getExamFilterRequest.pageNumber = this.paginator.pageIndex + 1;
      this.getExamFilterRequest.pageSize = this.paginator.pageSize;
      this.getListExamData();
    });

  }
  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  getListExamData() {
    this.examEndpoint.getExams(this.getExamFilterRequest)
      .then((res) => {
        this.examFilterClone = { ...this.getExamFilterRequest };
        // Lấy danh sách kỳ thi thì reset các kỳ thi đã chọn
        this.isCheckAll = false;
        this.listExamChoose = [];
        this.dataExamPagination = res;

        debugger;
        let listCreateLevel = res.items.map(x => x.createLevel);
        let listDonviId = res.items.map(x => x.donviId);

        Promise.resolve()
          .then(() => {
            debugger
            return this.examEndpoint.GetDonViNameExam(listCreateLevel, listDonviId)
          })
          .then((response) => {
            res.items.forEach((item) => {
              let donVi = response.find(x => x.donviId == item.donviId && x.createLevel == item.createLevel);
              if (donVi) {
                item.donviName = donVi.donviName;
              }
              else {
                item.donviName = ''
              }
            })
          })


        if (this.isFirstLoadAllGrades) {

          // let gradeAll = this.endpointGrade.getGradeOptions(null);
          // let users = this.
          this.endpointGrade.getGradeOptions(null)
            .then(res => {
              this.isFirstLoadAllGrades = false;
              this.allGrades = res;

              this.dataExamPagination.items = this.dataExamPagination.items.map((exam) => {
                let grade = res.find(x => x.id == exam.gradeId);
                if (grade != undefined) {
                  exam.gradeName = grade.name;
                }
                return exam;
              });

            })
            .catch();

          let userIds = this.dataExamPagination.items.map(x => { return x.creatorId });
          this.endpointUser.GetListUserByIds(userIds)
            .then(res => {
              this.dataExamPagination.items = this.dataExamPagination.items.map((exam) => {
                let user = res.find(x => x.id == exam.creatorId);
                if (user != undefined) {
                  exam.creatorName = user.name;
                }
                return exam;
              });

            })
            .catch();
        }
        else {
          this.dataExamPagination.items = this.dataExamPagination.items.map((exam) => {
            let grade = this.allGrades.find(x => x.id == exam.gradeId);
            if (grade != undefined) {
              exam.gradeName = grade.name;
            }
            return exam;
          });

          let userIds = this.dataExamPagination.items.map(x => { return x.creatorId });
          this.endpointUser.GetListUserByIds(userIds)
            .then(res => {
              this.dataExamPagination.items = this.dataExamPagination.items.map((exam) => {
                let user = res.find(x => x.id == exam.creatorId);
                if (user != undefined) {
                  exam.creatorName = user.name;
                }
                return exam;
              });

            })
            .catch();

        }

      })
  }


  editExam(examId, status) {
    this.router.navigate(["dashboard/examination-manager/exams/", examId], { queryParams: { status: status } });
  }

  organization(examId) {
    this.router.navigate(['dashboard/examination-manager/organize'])
  }

  assignmentSchool(examId, gradeId) {
    this.router.navigate(["dashboard/examination-manager/exams/", examId, 'grade', gradeId, "assignment-school"])
  }

  deleteExam(data) {
    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      disableClose: true,
      width: '444px',
      height: 'auto',
      data: {
        title: 'Xóa kỳ thi',
        name: data.name,
        message: ''
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
        this.examEndpoint.deleteExam(data.id).then((res) => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Xóa kì thi thành công',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
            this.getListExamData();
          }
          else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Xóa thất bại',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          }
        })
          .catch((err) => {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Xóa thất bại',
              duration: 3000,
              horizontalPosition: 'center',
              verticalPosition: 'top',
            });
          })
      }
      else {
        if (result != undefined) {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Bạn nhập chưa đúng, vui lòng thử lại',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      }
    });
  }

  deleteMultiExam() {
    let deleteIds = this.listExamChoose.filter(exam => {
      if (this.dataExamPagination.items.find(x => x.id == exam && x.status == 'PUB') == null) {
        return exam;
      }
    });
    if (deleteIds.length != 0) {
      const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
        disableClose: true,
        width: '444px',
        height: 'auto',
        data: {
          title: 'Xóa kỳ thi',
          name: 'Kỳ thi đã chọn',
          message: ''
        }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result == 'DONGY') {
          this.examEndpoint.deleteExams(deleteIds).then((res) => {
            if (res) {
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: 'Xóa kì thi thành công',
                duration: 3000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });
              this.getListExamData();
              this.isCheckAll = false;
              this.listExamChoose = [];
            }
            else {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Xóa thất bại',
                duration: 3000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });
            }
          })
            .catch((err) => {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Xóa thất bại',
                duration: 3000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });
            })
        }
        else {
          if (result != undefined) {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Bạn nhập chưa đúng, vui lòng thử lại',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        }
      });
    }
    else {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Chỉ cho phép xóa Kỳ thi chưa được duyệt. Vui lòng chọn 1 kỳ thi để xóa!',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    }
  }

  chooseExam(isChecked, examId) {

    this.isCheckAll = this.isSelectedAll();
    if (isChecked) {
      this.listExamChoose.push(examId);
    } else {
      let index = this.listExamChoose.findIndex(element => element == examId);
      if (index != -1) {
        this.listExamChoose.splice(index, 1);
      }
    }
  }

  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id;
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id;
      });

    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        if (this.singleSelectGrade != undefined) {
          this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
        }
      });
    // this.filteredSubjects
    //   .pipe(take(1), takeUntil(this._onDestroy))
    //   .subscribe(() => {
    //     // setting the compareWith property to a comparison function
    //     if (this.singleSelectSubject != undefined) {
    //       this.singleSelectSubject.compareWith = (a: SubjectOption, b: SubjectOption) => a && b && a.id === b.id;
    //     }
    //   });
  }
  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the grades
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }
  // protected filterSubjects() {
  //   if (!this.subjects) {
  //     return;
  //   }
  //   // get the search keyword
  //   let search = this.subjectFilterCtrl.value;
  //   if (!search) {
  //     this.filteredSubjects.next(this.subjects.slice());
  //     return;
  //   } else {
  //     search = search.toLowerCase();
  //   }
  //   // filter the grades
  //   this.filteredSubjects.next(
  //     this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
  //   );
  // }

  onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.divisions = res;
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch()
    }
  }

  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionCtrl.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchools || this.schoolFilter.divisionId != divisionIdSelected) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchools = false;
          this.schools = res;
          this.schoolFilter.divisionId = divisionIdSelected;
          this.filteredSchools.next(this.schools.slice());
        })
        .catch()
    }
  }

  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolCtrl.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrades || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch()
    }
  }

  // onSubjectSelectClicked() {
  //   let gradeIdSelected = this.gradeCtrl.value;
  //   if (this.isFirstLoadSubjects || this.subjectFilter.gradeId != gradeIdSelected) {
  //     this.endpointSubject.getSubjectOptions(gradeIdSelected)
  //       .then(res => {
  //         this.isFirstLoadSubjects = false;
  //         this.subjects = res;
  //         this.subjectFilter.gradeId = gradeIdSelected;
  //         this.filteredSubjects.next(this.subjects.slice());
  //       })
  //       .catch()
  //   }
  // }

  onChangeDivisionSelected(id: any) {
    this.resetSchoolSelectCtrl();
    this.resetGradeSelectCtrl();
  }
  onChangeSchoolSelected(id: any) {
    this.resetGradeSelectCtrl();
  }
  onChangeGradeSelected(id: any) {
    // this.subjects = [];
    //this.isFirstLoadSubjects = true;
    //this.subjectCtrl.setValue(undefined);
  }

  resetSchoolSelectCtrl() {
    this.schools = [];
    this.isFirstLoadSchools = true;
    this.schoolCtrl.setValue(undefined);
  }
  resetGradeSelectCtrl() {
    this.grades = [];
    this.isFirstLoadGrades = true;
    this.gradeCtrl.setValue(undefined);
  }
  openDpFromDate() {
    this.dpFromDate.open();
  }
  openDpToDate() {
    this.dpToDate.open();
  }
  getFilterParam() {
    this.getExamFilterRequest.divisionId = this.divisionCtrl.value;
    this.getExamFilterRequest.schoolId = this.schoolCtrl.value;

    if (this.userIdentity.levelManage == "DVS") {
      this.getExamFilterRequest.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      this.getExamFilterRequest.divisionId = this.userIdentity.divisionId;
      this.getExamFilterRequest.schoolId = this.userIdentity.schoolId;
    }

    this.getExamFilterRequest.gradeId = this.gradeCtrl.value;
    //this.getExamFilterRequest.subjectId = this.subjectCtrl.value;

    this.getExamFilterRequest.type = this.examTypeCtrl.value;
    this.getExamFilterRequest.statusEdit = this.statusCtrl.value;
    this.getExamFilterRequest.statusExam = this.statusExamCtrl.value;
    this.getExamFilterRequest.pageNumber = 1;
    this.paginator.pageIndex = 0;
  }

  filterExam() {
    this.getFilterParam();
    if (this.objHasChanged(this.examFilterClone, this.getExamFilterRequest)) {
      this.getListExamData();
    }
  }
  objHasChanged(obj, obj2) {
    return JSON.stringify(obj) != JSON.stringify(obj2);
  }
  setAllSelected(isChecked) {

    if (isChecked) {
      this.isCheckAll = true;
      this.listExamChoose = [];
      this.dataExamPagination.items.map(exam => {
        this.listExamChoose.push(exam.id);
        exam.checked = true;
      });
    } else {
      this.isCheckAll = false;
      this.listExamChoose = [];
      this.dataExamPagination.items.map(exam => {
        exam.checked = false;
        return exam;
      });
    }
  }
  isSelectedAll(): boolean {
    return this.dataExamPagination.items.every(x => x.checked);
  }
  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }
}

import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ExamEnpoint } from 'cls-k12-sdk-js/src/services/exam/endpoints/exam-enpoint';
import { SubjectOfExam } from 'cls-k12-sdk-js/src/services/exam/models/subject-of-exam';
import { SubjectEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { ViewExamFinishComponent } from '../view-exam-finish/view-exam-finish.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { TestEnpoint } from 'sdk/cls-k12-sdk-js/src/services/test/enpoint/test-enpoint';

@Component({
  selector: 'app-choose-test',
  templateUrl: './choose-test.component.html',
  styleUrls: ['./choose-test.component.scss']
})
export class ChooseTestComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  routeSub: Subscription;
  //mã kỳ thi
  examId;
  step;
  gradeId;
  testKitId: number;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  stepHeaderControl;
  //list subject of exam
  subjectOfExam: SubjectOfExam[];
  selection = new SelectionModel<SubjectOfExam>(true, []);
  private examApi: ExamEnpoint;
  private subjectApi: SubjectEndpoint;
  isPublished: boolean = true;
  private dialogQuestionBankList: MatDialogRef<ViewExamFinishComponent>;
  private testEnpoint: TestEnpoint;
  
  // displayedColumns: string[] = ['select', 'stt', 'name', 'startTime', 'endTime', 'testName', 'chosse-test', 'establish'];
  displayedColumns: string[] = ['stt', 'name', 'startTime', 'endTime', 'testName', 'chosse-test', 'establish'];
  constructor(private route: ActivatedRoute, private router: Router, private _snackBar: MatSnackBar,private dialog: MatDialog) {
    this.gradeId = +this.route.snapshot.queryParamMap.get('gradeId');
    this.testKitId = +this.route.snapshot.queryParamMap.get("testKitId");
    this.examApi = new ExamEnpoint();
    this.subjectApi = new SubjectEndpoint({ baseUrl: '' });
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = params['id'];
      this.step = params['step'];
    });


    this.stepHeaderControl = {
      currentPage: 2,
      isCreatingTest: false,
      examId: +this.examId,
      step: 2
    }
    this.testEnpoint = new TestEnpoint();
  }

  ngOnInit() {
    this.examApi.getExamStatus(this.examId)
      .then(res => {
        this.isPublished = res == 'PUB';
      })
      .catch();

    this.getDataTest();
  }

  getDataTest() {
    this.examApi.getSubjectExam(this.examId).then(res => {
      if (res) {
        this.subjectOfExam = res;
        this.gradeId = res[0].gradeId;
        let subjectIds = this.subjectOfExam.map((x) => {
          return x.subjectId
        });
        this.subjectApi.getSubjectOptionsByIds(subjectIds)
          .then(res => {
            this.subjectOfExam = this.subjectOfExam.map((sj) => {
              let subject = res.find(x => x.id == sj.subjectId);
              if (subject != undefined) {
                sj.subjectName = subject.name;
              }
              return sj;
            });
          });
      }
    });
  }

  chosseTest(examSubjectId, subjectId, testKitId) {
    if (this.isPublished) {
      return;
    }

    //?gradeId=' + this.gradeId + '&&subjectId=' + subjectId + '&&examSubjectId=' + examSubjectId + '&&testKitId=' + testkitId
    this.router.navigate(['dashboard/examination-manager/exams/' + this.examId + '/steps/testkits'],
    { queryParams: { gradeId: this.gradeId, subjectId: subjectId, examSubjectId, testKitId } });


  }

  //setting subject test
  settingSubject(examSubjectId, subjectId) {
    console.log(subjectId);
    const sbId = examSubjectId ? examSubjectId : null;
    this.router.navigate(["dashboard/examination-manager/exams/" + this.examId + "/setting-subject/" + subjectId + "/", sbId]);
  }

  nextPage() {

    if (!this.isPublished) {
      var subjectWithoutTest = this.subjectOfExam.filter(x => x.testkitId == null);
      if (subjectWithoutTest != null && subjectWithoutTest.length > 0) {
        let listSubjectName = '';
        subjectWithoutTest.forEach(x => { listSubjectName += x.subjectName + ', ' });
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: "Vui lòng chọn đề thi cho môn thi: " + listSubjectName.substring(0, listSubjectName.length - 2),
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });

        return;
      }
      this.stepHeaderControl = {
        currentPage: 3,
        isCreatingTest: false,
        examId: +this.examId,
        step: 3
      }
    }

    this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "setting"])
  }
  backPage() {
    this.router.navigate(["dashboard/examination-manager/exams", this.examId])
  }


  // /** Whether the number of selected elements matches the total number of rows. */
  // isAllSelected() {
  //   const numSelected = this.selection.selected.length;
  //   const numRows = this.subjectOfExam.length;
  //   return numSelected === numRows;
  // }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  // masterToggle() {
  //   this.isAllSelected() ?
  //     this.selection.clear() :
  //     this.subjectOfExam.forEach(row => this.selection.select(row));
  // }

  /** The label for the checkbox on the passed row */
  // checkboxLabel(row?: SubjectOfExam): string {
  //   if (!row) {
  //     return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
  //   }
  //   return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.subjectId + 1}`;
  // }
  // get list question bank
  hanleGetListQuestionBank(idTestkit,testkitName) {
    this.testEnpoint.getTestsView(idTestkit).then(res => {
      if (res) {
        this.showDialogListQuestion(res,testkitName);
      }
    }).catch(err => console.log(err))
  }

  // show dialog
  showDialogListQuestion(listTest,testkitName) {
    this.dialogQuestionBankList = this.dialog.open(ViewExamFinishComponent, {
      width: '70vw',
      height: 'auto',
      data: {
        testkitName,
        listTest
      }
    });
  }
}

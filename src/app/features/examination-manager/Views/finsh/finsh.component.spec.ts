import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FinshComponent } from './finsh.component';

describe('FinshComponent', () => {
  let component: FinshComponent;
  let fixture: ComponentFixture<FinshComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FinshComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinshComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

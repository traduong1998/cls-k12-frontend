import { ViewExamFinishComponent } from './../view-exam-finish/view-exam-finish.component';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ExamEnpoint } from 'sdk/cls-k12-sdk-js/src/services/exam/endpoints/exam-enpoint';
import { CLSModules, CLSPermissions, ExamDetail, SubjectEndpoint } from 'cls-k12-sdk-js/src';
import { ToastComponent } from '../../Components/toast/toast.component';
import { AuthService } from 'src/app/core/services';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ExaminationSubjectSettingDialogComponent } from '../../Components/examination-subject-setting-dialog/examination-subject-setting-dialog.component';
import { TestEnpoint } from 'sdk/cls-k12-sdk-js/src/services/test/enpoint/test-enpoint';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';

@Component({
  selector: 'app-finsh',
  templateUrl: './finsh.component.html',
  styleUrls: ['./finsh.component.scss']
})
export class FinshComponent implements OnInit {

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  routeSub: Subscription;
  //mã kỳ thi
  examId: number;
  step;
  //info exam

  //authen
  authService

  //quyền
  subjectOfExam = [];

  private dialogQuestionBankList: MatDialogRef<ViewExamFinishComponent>


  dataInfoExam: ExamDetail = null;
  stepHeaderControl
  displayedColumns: string[] = ['STT', 'subject', 'startTime', 'endTime', 'testKit', 'config'];
  isPublished: boolean = true;
  private examApi: ExamEnpoint;
  subjectEndpoint: SubjectEndpoint;
  private testEnpoint: TestEnpoint;
  hasApprovePermission: boolean;
  constructor(private route: ActivatedRoute, private router: Router, private _snackBar: MatSnackBar, private _authService: AuthService, private dialog: MatDialog) {
    var userIdentity = this._authService.getTokenInfo();
    this.examApi = new ExamEnpoint();
    this.subjectEndpoint = new SubjectEndpoint({ baseUrl: '' });
    this.testEnpoint = new TestEnpoint();
    this.routeSub = this.route.params.subscribe(params => {
      this.examId = +params['id'];
      this.step = params['step']
    });

    this.stepHeaderControl = {
      currentPage: 1,
      isCreatingTest: false,
      examId: +this.examId,
      step: 4
    }

    // debugger;
    //hasPermission(module: CLSModules, permission: CLSPermissions): boolean
    this.hasApprovePermission = userIdentity.hasPermission(CLSModules.Exam, CLSPermissions.Approved);
  }

  ngOnInit(): void {
    //get quyền
    // this.portalId = this.authService.portalId;
    // this.schoolId = this.authService.schoolId;

    //get data from api
    this.examApi.getInfoExam(this.examId)
      .then(res => {
        this.dataInfoExam = res;
        //bindding data
        if (this.dataInfoExam != null) {
          this.isPublished = this.dataInfoExam.status == 'PUB';
          //đô dữ liệu môn thi
          this.dataInfoExam.examSubjects.forEach(element => {
            this.subjectOfExam.push({
              id: element.subjectId,
              name: null,
              testKitName: element.testKitName,
              testKitId: element.testKitId,
              startDate: element.subjectStartDate,
              endDate: element.subjectEndDate,
            });
          });

          let subjectIds = this.subjectOfExam.map((x) => {
            return x.id
          });
          this.subjectEndpoint.getSubjectOptionsByIds(subjectIds)
            .then(res => {
              this.subjectOfExam = this.subjectOfExam.map((sj) => {
                let subject = res.find(x => x.id == sj.id);
                if (subject != undefined) {
                  sj.name = subject.name;
                }
                return sj;
              });
            });

        }
      })
  }

  backPage() {
    this.router.navigate(["dashboard/examination-manager/exams", this.examId, "steps", "setting"])
  }

  openSnackBar(message: string, type: 'error' | 'ok') {
    let panelClass = type == 'ok' ? 'green-snackbar' : 'red-snackbar';
    this._snackBar.open(message, 'Ok', {
      duration: 3000,
      panelClass: [panelClass],
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

  save() {
    // gửi có quyển duyêt
    this.examApi.updateExamFinish(this.examId).then(res => {
      if (res) {
        this.router.navigate(['dashboard/examination-manager']);
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: 'Bạn đã hoàn thành kỳ thi!',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      } else {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Có lỗi xảy ra!',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    })

  }
  //lưu với quyền duyêt
  saveApp() {
    this.examApi.updateExamFinishAndapproval(this.examId).then(res => {
      if (res) {
        this.router.navigate(['dashboard/examination-manager']);
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: 'Bạn đã hoàn thành kỳ thi!',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      } else {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Có lỗi xảy ra!',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    })
  }

  backListExams() {
    this.router.navigate(['dashboard/examination-manager']);
  }
  settingSubject(subjectId) {
    console.log(`subjectId: `, subjectId)
    this.examApi.getExamSubjectSetting(this.examId, subjectId)
      .then(res => {

        let subject = this.subjectOfExam.find(x => x.id == subjectId);
        if (subject != null) {
          res.subjectName = subject.name;
        }
        this.dialog.open(ExaminationSubjectSettingDialogComponent, {
          width: '420px',
          height: 'auto',
          disableClose: true,
          data: res
        });
      }).catch(err => {
        console.log("err")
      })
  }

  // get list question bank
  hanleGetListQuestionBank(idTestkit, testkitName) {
    this.testEnpoint.getTestsView(idTestkit).then(res => {
      if (res) {
        this.showDialogListQuestion(res, testkitName);
      }
    }).catch(err => console.log(err))
  }

  // show dialog
  showDialogListQuestion(listTest, testkitName) {
    this.dialogQuestionBankList = this.dialog.open(ViewExamFinishComponent, {
      width: '70vw',
      height: 'auto',
      data: {
        testkitName,
        listTest
      }
    });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizeExamInfoComponent } from './organize-exam-info.component';

describe('OrganizeExamInfoComponent', () => {
  let component: OrganizeExamInfoComponent;
  let fixture: ComponentFixture<OrganizeExamInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrganizeExamInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizeExamInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

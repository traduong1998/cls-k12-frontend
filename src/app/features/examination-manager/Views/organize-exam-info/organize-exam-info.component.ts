import { ExamSubject } from 'sdk/cls-k12-sdk-js/src/services/exam/models/ExamSubject';
import { ExamSubjectEndPoint } from 'sdk/cls-k12-sdk-js/src/services/exam/endpoints/ExamSubjectEndPoint';
import { Component, OnInit, OnChanges, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { StepHeaderControlExam, StepHeaderControlOrganize } from 'src/app/features/learn/intefaces/stepHeaderControl';
import { ExamEnpoint, OrganizeExamEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { OrganizeExamDetail } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/OrganizeExamDetail';
import { OrganizeOncentrationExamComponent } from '../../Components/organize-oncentration-exam/organize-oncentration-exam.component';
import { OrganizeFreeExamComponent } from '../../Components/organize-free-exam/organize-free-exam.component';
import { UserExamEndpoint } from 'sdk/cls-k12-sdk-js/src/services/user-exam/endpoints/user-exam-endpoint';
import { AssignStudentsToShiftExamsRequest, ShiftExamAssignStudentInfo } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/request/assign-students-shiftexams';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ShiftInfo } from 'sdk/cls-k12-sdk-js/src/services/organize-exam/models/ShiftInfo';
import { MessageService } from 'src/app/shared/services/message.service';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-organize-exam-info',
  templateUrl: './organize-exam-info.component.html',
  styleUrls: ['./organize-exam-info.component.scss']
})
export class OrganizeExamInfoComponent implements OnInit {
  @ViewChild(OrganizeOncentrationExamComponent) childCon;
  @ViewChild(OrganizeFreeExamComponent) childFree;
  organizeExamDetail: OrganizeExamDetail = {
    id: 0,
    testTime: 0,
    totalRoom: 1,
    totalShift: 1,
    startDate: new Date(),
    endDate: new Date(),
    startShiftDate: new Date(),
    totalStudent: 0,
    examType: 'CON',
    subjectName: '',
    breakTime: 3,
    status: "EDI"
  };
  shiftExamsInfo: ShiftInfo[];
  shiftExamAssignStudents: ShiftExamAssignStudentInfo[] = [];
  changedShiftExam: boolean = false;
  isReady: boolean = false;
  gradeId;
  subjectId;
  examId;
  organizeId;
  routeSub: Subscription;
  stepHeaderCtrl: StepHeaderControlOrganize;
  currentPage = 2;
  organizeExamEndPoint: OrganizeExamEndpoint;
  userExamEndpoint: UserExamEndpoint;
  private examApi: ExamEnpoint;
  constructor(private router: Router, private route: ActivatedRoute, private _messageService: MessageService, private spinner: NgxSpinnerService) {
    this.examApi = new ExamEnpoint();
    this.organizeExamEndPoint = new OrganizeExamEndpoint();
    this.userExamEndpoint = new UserExamEndpoint();
    this.stepHeaderCtrl = {
      currentPage: this.currentPage,
      //Nhớ truyền exam id cho đúng của a
      examId: 1,
      subjectId: 1
    };
  }
  ngOnInit(): void {

    this.routeSub = this.route.params.subscribe(params => {
      this.examId = params['id'];
      this.subjectId = params['subjectId'];
      this.gradeId = params['gradeId'];
      this.organizeId = params['organizeId'];
    });
    this.spinner.show();
    this.organizeExamEndPoint.getOrganizeExamDetail(this.organizeId)
      .then((res) => {
        this.organizeExamDetail = res;
      })
      .then(() => {
        this.examApi.getExamSubjectSetting(this.examId, this.subjectId)
          .then(res => {

            this.organizeExamDetail.testTime = res.testTime;
            this.isReady = true;
          })
          .catch(err => {
            this._messageService.failureCallback("Có lỗi xảy ra Vui lòng thử lại");
            this.router.navigate(['dashboard/examination-manager/organize/', this.examId, 'grade', this.gradeId, 'examsubject', this.subjectId, 'assignment', this.organizeId]);
          })
      })
      .catch((err) => {
        this._messageService.failureCallback("có lỗi xảy ra vui lòng thử lại");
        this.router.navigate(['dashboard/examination-manager/organize/', this.examId, 'grade', this.gradeId, 'examsubject', this.subjectId, 'assignment', this.organizeId]);
      });

    this.spinner.hide();
  }
  nextPage() {
    if (this.organizeExamDetail.status == "PUB") {
      this.router.navigate(['dashboard/examination-manager/organize/', this.examId, 'grade', this.gradeId, 'examsubject', this.subjectId, 'finish', this.organizeId]);
      return;
    }
    let errorDetail = "";
    if (this.organizeExamDetail.examType == 'CON') {
      if (this.childCon.shiftExam.length == 0) {
        this._messageService.failureCallback('Bạn chưa tạo ca thi');
        return;
      }
      else if (!this.childCon.isValidStudent || !this.childCon.isValidShiftDate) {
        this._messageService.failureCallback('Vui lòng kiểm tra lại thông tin thiết lập');
        return;
      }

      let listEdit = this.childCon.shiftExam.filter(x => x.action == 'EDI');
      let updateListOrganizeExamrs = new Promise((resolve, reject) => {
        resolve(1);
      });
      let updateOrganizeExamrs = new Promise((resolve, reject) => {
        resolve(1);
      });
      if (listEdit.length > 0) {
        updateListOrganizeExamrs = this.organizeExamEndPoint.updateListOrganizeExamInfo(listEdit)

      }
      if (JSON.stringify(this.organizeExamDetail) !== JSON.stringify(this.childCon.updateOrganizeExamRequest)) {
        updateOrganizeExamrs = this.organizeExamEndPoint.updateOrganizeExam(this.childCon.updateOrganizeExamRequest)
      }
      this.spinner.show();
      Promise.all(
        [
          updateListOrganizeExamrs.catch((err: ErrorResponse) => {
            errorDetail += err.errorDetail + '. ';
          }),
          updateOrganizeExamrs.catch((err: ErrorResponse) => {
            errorDetail += err.errorDetail + '. ';
          }),
        ]
      ).then((values) => {
        if (errorDetail != '') {
          this._messageService.failureCallback(errorDetail);
        } else {
          this.router.navigate(['dashboard/examination-manager/organize/', this.examId, 'grade', this.gradeId, 'examsubject', this.subjectId, 'finish', this.organizeId]);
        }
      });
      this.spinner.hide();
    }

    if (this.organizeExamDetail.examType == 'FRE') {
      if (this.childFree.shiftExam.length == 0) {
        this._messageService.failureCallback('Bạn chưa tạo ca thi');
        return;
      }
      else if (!this.childFree.isValidStudent || !this.childFree.isValidShiftDate) {
        this._messageService.failureCallback('Vui lòng kiểm tra lại thông tin thiết lập');
        return;
      }

      let listEdit = this.childFree.shiftExam.filter(x => x.action == 'EDI');
      let updateListOrganizeExamrs = new Promise((resolve, reject) => {
        resolve(1);
      });
      let updateOrganizeExamrs = new Promise((resolve, reject) => {
        resolve(1);
      });
      if (listEdit.length > 0) {
        updateListOrganizeExamrs = this.organizeExamEndPoint.updateListOrganizeExamInfo(listEdit)

      }
      if (JSON.stringify(this.organizeExamDetail) !== JSON.stringify(this.childFree.updateOrganizeExamRequest)) {
        updateOrganizeExamrs = this.organizeExamEndPoint.updateOrganizeExam(this.childFree.updateOrganizeExamRequest)
      }
      Promise.all(
        [
          updateListOrganizeExamrs.catch((err: ErrorResponse) => {
            errorDetail += err.errorDetail + '. ';
          }),
          updateOrganizeExamrs.catch((err: ErrorResponse) => {
            errorDetail += err.errorDetail + '. ';
          }),
        ]
      ).then((values) => {
        if (errorDetail != '') {
          this._messageService.failureCallback(errorDetail);
        } else {
          this.router.navigate(['dashboard/examination-manager/organize/', this.examId, 'grade', this.gradeId, 'examsubject', this.subjectId, 'finish', this.organizeId]);
        }
      });
    }
    this.spinner.hide();
  }
  previousPage() {
    this.router.navigate(['dashboard/examination-manager/organize/', this.examId, 'grade', this.gradeId, 'examsubject', this.subjectId, 'assignment', this.organizeId]);
  }
}

import { ShiftExam } from './../interfaces/shiftExam.model';
import { ExamSubjectEndPoint } from './../../../../../sdk/cls-k12-sdk-js/src/services/exam/endpoints/ExamSubjectEndPoint';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { TeacherExam } from 'sdk/cls-k12-sdk-js/src/services/exam/models/TeacherExam';
import { StudentExam } from 'sdk/cls-k12-sdk-js/src/services/user-exam/models/student-exam';

@Injectable()
export class organizeExamService {
  private listStudent: StudentExam[];
  private listTeacher: TeacherExam[];
  private listSupervisor: TeacherExam[];
  //private listSupervisorShiftExam:SupervisorShiftExam[];
  private listStudentSubject: BehaviorSubject<StudentExam[]> = new BehaviorSubject<StudentExam[]>([]);
  private listTeacherSubject: BehaviorSubject<TeacherExam[]> = new BehaviorSubject<TeacherExam[]>([]);
  private listSupervisorSubject: BehaviorSubject<TeacherExam[]> = new BehaviorSubject<TeacherExam[]>([]);
  //private listSupervisorShiftExamSubject:BehaviorSubject<SupervisorShiftExam[]> = new BehaviorSubject<SupervisorShiftExam[]>([]);
  listStudent$ = this.listStudentSubject.asObservable();
  listTeacher$ = this.listTeacherSubject.asObservable();
  listSupervisor$ = this.listSupervisorSubject.asObservable();
  //listSupervisorShiftExam$=this.listSupervisorShiftExamSubject.asObservable();
  constructor(private examSubjectEndpoint: ExamSubjectEndPoint) {

  }
  getListExamSubject(examId:number){
    return this.examSubjectEndpoint.getListExamSubject(examId);
  }
  getExamSubject(id:number){
    return this.examSubjectEndpoint.getExamSubject(id);
  }
  updateStudentExam(listStudentId: number[]) {
    this.listStudent.map((x) => {
      if (listStudentId.includes(x.id)) {
        x.isChoose = true;
      } else {
        x.isChoose = false;
      }
    });
    this.updateStudentData();
  }
  updateTeacherExam(listTeacherId: number[]) {
    this.listTeacher.map((x) => {
      if (listTeacherId.includes(x.id)) {
        x.isChoose = true;
      } else {
        x.isChoose = false;
      }
    });
    this.updateTeacherData();
  }

  updateSupervisorExam(listSupervisorId: number[]) {
    this.listSupervisor.map((x) => {
      if (listSupervisorId.includes(x.id)) {
        x.isChoose = true;
      } else {
        x.isChoose = false;
      }
    });
    this.updateSupervisorData();
  }
  updateTeacherData() {
    this.listTeacherSubject.next(this.listTeacher);
  }
  updateSupervisorData() {
    this.listSupervisorSubject.next(this.listSupervisor);
  }
  private updateStudentData(): void {
    this.listStudentSubject.next(this.listStudent);
  }

}

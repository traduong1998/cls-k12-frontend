import { Question } from './../../../../../sdk/cls-k12-sdk-js/src/services/question/models/Question';
export interface QuestionTest{
    question:Question,
    score:number
}
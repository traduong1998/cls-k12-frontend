export interface Matrix{
    stt:number,
    chapter:string,
    lesson:string,
    formatQuestion:string
    typeQuestion:string,
    level:string,
    quantity:number,
    score:number
}
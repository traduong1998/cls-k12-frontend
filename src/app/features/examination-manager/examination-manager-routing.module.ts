import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards';
import { ContentListExamSubjectComponent } from './Components/content-list-examsubject/content-list-examsubject.component';
import { ExaminationManagerComponent } from './examination-manager.component';
import { AddNewMatrixComponent } from './Views/add-new-matrix/add-new-matrix.component';
import { AddTestkitFromFileComponent } from './Views/add-testkit-from-file/add-testkit-from-file.component';
import { AssignmentSchoolComponent } from './Views/assignment-school/assignment-school.component';
import { ChooseTestComponent } from './Views/choose-test/choose-test.component';
import { CreateExaminationComponent } from './Views/create-examination/create-examination.component';
import { EditExaminationComponent } from './Views/edit-examination/edit-examination.component';
import { EditTestComponent } from './Views/edit-test/edit-test.component';
import { EditTestkitComponent } from './Views/edit-testkit/edit-testkit.component';
import { ExamSubjectOrganizeExaminationComponent } from './Views/exam-subject-organize-examination/exam-subject-organize-examination.component';
import { ExaminationDetailViewComponent } from './Views/examination-detail-view/examination-detail-view.component';
import { ExaminationSummaryReviewComponent } from './Views/examination-summary-review/examination-summary-review.component';
import { FinishOrganizeComponent } from './Views/finish-organize/finish-organize.component';
import { FinshComponent } from './Views/finsh/finsh.component';
import { ListExaminationApprovalComponent } from './Views/list-examination-approval/list-examination-approval.component';
import { ListExaminationComponent } from './Views/list-examination/list-examination.component';
import { ListOganizeExamComponent } from './Views/list-oganize-exam/list-oganize-exam.component';
import { ListTestkitComponent } from './Views/list-testkit/list-testkit.component';
import { MaxtrixComponent } from './Views/maxtrix/maxtrix.component';
import { OrganizeExamInfoComponent } from './Views/organize-exam-info/organize-exam-info.component';
import { QuestionBankComponent } from './Views/question-bank/question-bank.component';
import { SettingExamComponent } from './Views/setting-exam/setting-exam.component';
import { SettingSubjectTestComponent } from './Views/setting-subject-test/setting-subject-test.component';
import { ViewTestComponent } from './Views/view-test/view-test.component';

const routes: Routes = [
  {
    path: '', component: ExaminationManagerComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: ListExaminationComponent,
        data: {
          title: 'Danh sách kỳ thi'
        }
      },
      {
        path: 'exams',
        //component: CreateExaminationComponent,
        children: [
          {
            path: 'create',
            component: CreateExaminationComponent,
            data: {
              title: 'Tạo kỳ thi'
            }
          },
          {
            path: 'approval',
            component: ListExaminationApprovalComponent,
            data: {
              title: 'Danh sách duyệt kỳ thi'
            }
          },
          {
            path: 'approval/:id',
            component: ExaminationSummaryReviewComponent,
            data: {
              title: 'Xét duyệt kỳ thi'
            }
          },
          {
            path: 'review/:id',
            component: ExaminationDetailViewComponent,
            data: {
              title: 'Thông tin kỳ thi'
            }
          },
          //
          //
          // {
          //   path: ':id/:step/setting',
          //   component: SettingExamComponent
          // },
          // {
          //   path: ':id/:step/choose-test',
          //   component: ChooseTestComponent
          // },
          // {
          //   path: ':id/:step/finish',
          //   component: FinshComponent
          // },
          {
            path: ':id/grade/:gradeId/assignment-school',
            component: AssignmentSchoolComponent,
            data: {
              title: 'Phân bổ kỳ thi cho trường'
            }
          },
          // {
          //   // return page if null
          // }
        ]
      },
      {
        path: 'exams/:id',
        children: [
          {
            path: '',
            component: EditExaminationComponent,
            data: {
              title: 'Chỉnh sửa kỳ thi'
            }
          },
          {
            path: 'steps/choose-test',
            component: ChooseTestComponent,
            data: {
              title: 'Thiết lập đề thi'
            }
          },
          {
            path: 'steps/setting',
            component: SettingExamComponent,
            data: {
              title: 'Thiết lập kỳ thi'
            }
          },
          {
            path: 'steps/finish',
            component: FinshComponent,
            data: {
              title: 'Hoàn thành kỳ thi'
            }
          },
          {
            path: 'steps/testkits',
            component: ListTestkitComponent,
            data: {
              title: 'Chọn đề thi'
            }
          },
          {
            path: 'steps/testkits/add-testkit-from-file',
            component: AddTestkitFromFileComponent,
            data: {
              title: 'Thêm bộ đề'
            }
          },
          {
            path: 'steps/testkits/edit-testkit',
            component: EditTestkitComponent,
            data: {
              title: 'Chỉnh sửa bộ đề'
            }
          }
        ]
      },

      //id = subject id
      {
        path: 'list-testkit/:examId/:gradeId/:subjectId/:examSubject',
        children: [
          {
            path: '',
            component: ListTestkitComponent
          },
          {
            path: 'view-test',
            component: ViewTestComponent
          },

          {
            path: 'edit-test/:testId',
            component: EditTestComponent
          },
          {
            path: 'question-bank',
            component: QuestionBankComponent
          },
          {
            path: 'matrix',
            //component: MaxtrixComponent,
            children: [
              {
                path: '',
                component: MaxtrixComponent
              },
              {
                path: 'addnew',
                component: AddNewMatrixComponent
              },
              {
                path: 'view-test',
                component: ViewTestComponent
              }
            ]

          }
        ],
        data: {
          title: 'Chọn đề thi'
        }
      },

      {
        path: 'exams/:examId/setting-subject/:subjectId/:id',
        component: SettingSubjectTestComponent
      },
      {
        path: 'organize',
        children: [
          {
            path: '',
            component: ListOganizeExamComponent,
            data: {
              title: 'Danh sách kỳ thi của đơn vị'
            }
          },
          {
            path: ':id/grade/:gradeId/examsubject',
            component: ContentListExamSubjectComponent,
            data: {
              title: 'Danh sách môn thi'
            }
          },
          {
            // rout phân công
            path: ':id/grade/:gradeId/examsubject/:subjectId/assignment/:organizeId',
            component: ExamSubjectOrganizeExaminationComponent,
            data: {
              title: 'Phân công kỳ thi'
            }
          },

          {
            path: ':id/grade/:gradeId/examsubject/:subjectId/setting/:organizeId',
            component: OrganizeExamInfoComponent,
            data: {
              title: 'Thông tin thiết lập'
            }
          },

          {
            path: ':id/grade/:gradeId/examsubject/:subjectId/finish/:organizeId',
            component: FinishOrganizeComponent,
            data: {
              title: 'Hoàn thành tổ chức thi'
            }
          },
          {
            path: ':id/grade/:gradeId/examsubject/:subjectId/view/:organizeId',
            component: FinishOrganizeComponent,
            data: {
              title: 'Thông tin tổ chức thi'
            }
          }
        ]
      },
      // sửa tổ chức thi
      {
        path: 'organize/',
        children: [
          {
            // rout phân công
            path: 'edit/:id',
            component: ExamSubjectOrganizeExaminationComponent,
          },
          {
            path: 'edit/:id/organize-exam',
            component: OrganizeExamInfoComponent
          },
          {
            path: 'edit/:id/finish',
            component: FinishOrganizeComponent
          }
        ]
      },
    ]
  }
]
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExaminationManagerRoutingModule { }

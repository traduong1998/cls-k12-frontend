import { stringify } from '@angular/compiler/src/util';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';

import { CLS, UserEndpoint, UserIdentity } from 'cls-k12-sdk-js/src';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { PasswordRequest } from 'sdk/cls-k12-sdk-js/src/services/user/requests/password-user-request';
import { UserInforRequest } from 'sdk/cls-k12-sdk-js/src/services/user/requests/user-info-request';
import { GetUserInfoResponse } from 'sdk/cls-k12-sdk-js/src/services/user/responses/get-user-info-response';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { MustMatch } from '../../helpers/must-match.validators';
import { ProgressDialogComponent } from 'src/app/features/learn/components/progress-dialog/progress-dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';
import { TeacherZoomEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/teacherZoomEndpoint';
import { TeacherZoomRequest } from 'sdk/cls-k12-sdk-js/src/services/lesson/requests/teacherZoomRequest';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { RulesConfirmDialogComponent } from 'src/app/shared/dialog/rules-confirm-dialog/rules-confirm-dialog.component';
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  authService: UserIdentity;
  fullname: string;
  userId: number;
  divisionId: number;
  isFirstLoadSchools: boolean = true;
  isFirstLoadGrades: boolean = true;
  isFirstLoadGroupStudents: boolean = true;
  public selectedTabIndex = 0;
  userInfo: GetUserInfoResponse;
  private userEndpoint: UserEndpoint;
  private fileServer: FileServerEndpoint;
  private teacherZoomEndPoint: TeacherZoomEndpoint;

  progressData: Observable<any>;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }
  numbers: 0;
  changePasswordForm = this.fb.group({

  });

  personalForm = this.fb.group({

    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    newPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),
    confirmPassword: new FormControl('', [Validators.required, Validators.minLength(6)]),

    publicKeyZoom: new FormControl('', [Validators.required, Validators.pattern(/^[^'" ]+$/)]),
    privateKeyZoom: new FormControl('', [Validators.required, Validators.pattern(/^[^'" ]+$/)]),
    emailZoom: new FormControl('', [Validators.required, Validators.email]),
  }, {
    validator: MustMatch('newPassword', 'confirmPassword')
  });
  get password() { return this.personalForm.get('password'); }
  get newPassword() { return this.personalForm.get('newPassword') }
  get confirmPassword() { return this.personalForm.get('confirmPassword') }

  get publicKeyZoom() { return this.personalForm.get('publicKeyZoom') }
  get privateKeyZoom() { return this.personalForm.get('privateKeyZoom') }
  get emailZoom() { return this.personalForm.get('emailZoom') }


  protected _onDestroy = new Subject<void>();
  //filter compobox
  public schoolYearCtrl: FormControl = new FormControl('');
  public schoolCtrl: FormControl = new FormControl('', Validators.required);

  public gradeCtrl: FormControl = new FormControl('', Validators.required);

  protected groupStudents: GroupStudentOption[];
  public groupStudentCtrl: FormControl = new FormControl('', Validators.required);
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public userRole;

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  groupStudentFilter = {
    schoolId: null,
    gradeId: null
  }
  imageUrl;
  newAccZoom: boolean = true;
  accZoom: TeacherZoomRequest;
  currentSchoolYear: number;
  baseApiUrl = 'http://localhost:65000';
  @ViewChild("inputImageFile") inputImageFile: ElementRef;
  constructor(private route: ActivatedRoute, private _authService: AuthService, private fb: FormBuilder, private router: Router, private _snackBar: MatSnackBar, public dialog: MatDialog, private configService: AppConfigService) {
    this.userEndpoint = new UserEndpoint();
    this.fileServer = new FileServerEndpoint();
    this.teacherZoomEndPoint = new TeacherZoomEndpoint();
    //get token to get user id
    this.authService = this._authService.getTokenInfo();
    this.userRole = this.authService.userTypeRole;
    this.userId = +this.authService.userId;
    this.userEndpoint.getUserInfor().then(res => {
      this.fullname = (isNullOrEmpty(res.lastName) ? '' : res.lastName) + ' ' + (isNullOrEmpty(res.firstName) ? '' : res.firstName)
      this.divisionId = res.divisionId;
      this.userInfo = res;
      console.log(res)

      if (res.avatar) {
        this.imageUrl = res.avatar;
      } else {
        this.imageUrl = '/assets/images/users/user-avartar.png'
      }
    })

    this.teacherZoomEndPoint.getInformationZoomAPI().then(res => {
      if (res.userId > 0) {
        this.newAccZoom = false;
        this.publicKeyZoom.setValue(res.publicKey);
        this.privateKeyZoom.setValue(res.privateKey);
        this.emailZoom.setValue(res.email);

        this.accZoom = {
          publicKey: res.publicKey,
          privateKey: res.privateKey,
          email: res.email
        }
      }
    })
    this.baseApiUrl = CLS.getConfig().apiBaseUrl;
  }

  ngOnInit(): void {
    let param = this.route.snapshot.queryParamMap.get("tab");
    if (param) {
      switch (param.toLocaleLowerCase()) {
        case 'create-zoom-account':

          this.selectedTabIndex = 2;
          break;

        default:
          break;
      }
    }
  }

  onChangeImageInput(event): void {
    var reader = new FileReader();
    let file: File = null;
    if (event.target.files && event.target.files[0]) {
      file = event.target.files[0];
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      Promise.resolve()
        .then(() => {
          console.log(`start process`);
          this.openProgressDialog();
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.fileServer.uploadFile({
            file: file,
            isPrivate: false,
            moduleName: ServiceName.OTHER
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          this.imageUrl = res ? res.link : null;
        })
        .then(() => {
          let editUserRequest: UserInforRequest = {
            userId: this.userId,
            username: this.userInfo.username,
            firstName: this.userInfo.firstName,
            lastName: this.userInfo.lastName,
            email: this.userInfo.email,
            gender: this.userInfo.gender,
            dayOfBirth: this.userInfo.dayOfBirth,
            address: this.userInfo.address,
            phoneNumber: this.userInfo.phoneNumber,
            avatar: this.imageUrl
          };
          this.userEndpoint.updateUserInfo(editUserRequest).then(res => {

            this.getprogressData().subscribe(value => {
              if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
              }
            });
            setTimeout(() => {
              this.dialogProgressRef.close();
            }, 1000);
          })
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }

  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.numbers }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  uploadImage(): void {
    this.inputImageFile.nativeElement.click();
  }

  // save() {
  //   if (this.firstname.value == '') {
  //     this._snackBar.open("Tên của bạn không được trống", 'Ok', {
  //       duration: 5000,
  //       panelClass: ['red-snackbar'],
  //       horizontalPosition: this.horizontalPosition,
  //       verticalPosition: this.verticalPosition,
  //     });
  //   }
  //   else if (this.username.value == '') {
  //     this._snackBar.open("Mã định danh của bạn không được trống", 'Ok', {
  //       duration: 5000,
  //       panelClass: ['red-snackbar'],
  //       horizontalPosition: this.horizontalPosition,
  //       verticalPosition: this.verticalPosition,
  //     });
  //   } else {
  //     if (this.personalForm.invalid) {
  //       let editUserRequest: UserInforRequest = { userId: this.userId, username: this.username.value, firstName: this.firstname.value, lastName: this.lastname.value, email: this.email.value, gender: this.gender.value, dayOfBirth: this.birthday.value, address: this.address.value, phoneNumber: this.phone.value, avatar: this.imageUrl };
  //       this.userEndpoint.updateUserInfo(editUserRequest).then(res => {
  //         this._snackBar.open("Cập nhật thành công", 'Ok', {
  //           duration: 5000,
  //           panelClass: ['green-snackbar'],
  //           horizontalPosition: this.horizontalPosition,
  //           verticalPosition: this.verticalPosition,
  //         });
  //         this.userEndpoint.getUserById(this.userId).then(res => {
  //           console.log("ress", res);
  //           this.userInfo = res;
  //           this.divisionId = res.divisionId;
  //           this.userInfo = res;

  //           this.imageUrl = res.avatar;
  //           // gán thông tin
  //           this.firstname.setValue(res.firstName);

  //           if (!this.lastname) {
  //             this.lastname.setValue('');
  //           } else {
  //             this.lastname.setValue(res.lastName);
  //           }

  //           this.username.setValue(res.username);
  //           this.email.setValue(res.email);
  //           this.phone.setValue(res.phoneNumber);
  //           this.address.setValue(res.address);
  //           this.gender.setValue(res.gender);
  //           this.birthday.setValue(res.dayOfBirth);
  //           this.fullname.setValue(res.lastName + ' ' + res.firstName);

  //           this.schoolCtrl.setValue(res.schoolName);
  //           this.gradeCtrl.setValue(res.gradeName);
  //           this.groupStudentCtrl.setValue(res.groupStudentName);
  //         })

  //       }).catch(err => {
  //         this._snackBar.openFromComponent(ErrorSnackBarComponent, {
  //           data: "Cập nhật thất bại",
  //           duration: 3000,
  //           horizontalPosition: this.horizontalPosition,
  //           verticalPosition: this.verticalPosition,
  //         });
  //       });
  //     }
  //   }

  // }

  // cancel() {
  //   this.userEndpoint.getUserById(this.userId).then(res => {
  //     this.userInfo = res;
  //     this.divisionId = res.divisionId;
  //     this.userInfo = res;

  //     this.imageUrl = res.avatar;
  //     // gán thông tin
  //     this.firstname.setValue(res.firstName);
  //     if (!this.lastname) {
  //       this.lastname.setValue('');
  //     } else {
  //       this.lastname.setValue(res.lastName);
  //     }

  //     this.username.setValue(res.username);
  //     this.email.setValue(res.email);
  //     this.phone.setValue(res.phoneNumber);
  //     this.address.setValue(res.address);
  //     this.gender.setValue(res.gender);
  //     this.birthday.setValue(res.dayOfBirth);
  //     this.fullname.setValue(res.lastName + ' ' + res.firstName);

  //     this.schoolCtrl.setValue(res.schoolName);
  //     this.gradeCtrl.setValue(res.gradeName);
  //     this.groupStudentCtrl.setValue(res.groupStudentName);
  //   })
  // }

  changePassword() {
    if (this.password.value == '' && this.password.value.length < 6) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Bạn chưa nhập mật khẩu hiện tại",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    }
    else if (this.newPassword.value == '' && this.newPassword.value.length < 6) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Bạn chưa nhập mật khẩu mới",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    } else if (this.confirmPassword.value == '' && this.confirmPassword.value.length < 6) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Xác nhận chưa chính xác",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    } else {
      let passwordRequest: PasswordRequest = { password: this.password.value, newPassword: this.newPassword.value }
      this.userEndpoint.updatePasworUser(passwordRequest).then(res => {
        if (res) {
          this._snackBar.openFromComponent(SuccessSnackBarComponent, {
            data: "Thay đổi mật khẩu thành công",
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.resetForm();
        }

      }).catch(err => {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: "Thay đổi mật khẩu thất bại",
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      })
    }

  }

  resetForm() {
    this.password.reset();
    this.newPassword.reset();
    this.confirmPassword.reset();
  }

  getLinkTemplate() {
    return `${this.baseApiUrl}/learn/public/files/cdn/template-file/tep-tin-huong-dan-tao-tai-khoan-lien-ket-zoom.pdf`;
    // return `http://localhost:58910/learn/public/files/cdn/template-file/tep-tin-huong-dan-tao-tai-khoan-lien-ket-zoom.pdf`;
  }

  insertZoomAccount() {
    if (this.publicKeyZoom.errors?.required) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Bạn chưa nhập PublicKey",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (this.publicKeyZoom.errors?.pattern) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "PublicKey chứa ký tự đặc biệt",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (this.privateKeyZoom.errors?.required) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Bạn chưa nhập PrivateKey",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (this.privateKeyZoom.errors?.pattern) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "PrivateKey chứa ký tự đặc biệt",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (this.emailZoom.errors?.required) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Bạn chưa nhập Email",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else if (this.emailZoom.errors?.email) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Email không hợp lệ",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }
    else {
      if (this.newAccZoom) {
        const dialogRef = this.dialog.open(RulesConfirmDialogComponent, {
          disableClose: true,
          width: '396px',
          height: 'auto',
          data: {
            // title: 'Một số quy định khi tạo tài khoản liên kết Zoom', // 
            mode: 'create'  //nội dung message chi tiết
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          console.log(`result`, result);
          if (result === 'Save') {
            let teacherZoom: TeacherZoomRequest = {
              publicKey: this.publicKeyZoom.value,
              privateKey: this.privateKeyZoom.value,
              email: this.emailZoom.value
            }
            this.teacherZoomEndPoint.createInformationZoomAPI(teacherZoom).then(res => {
              if (res) {
                this.newAccZoom = false;
                this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                  data: "Tạo tài khoản liên kết Zoom thành công",
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
                this.accZoom = teacherZoom;
              }
              else {
                this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                  data: "Tạo tài khoản liên kết Zoom thất bại",
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            })
          }
        });

      }
      else {
        const dialogRef = this.dialog.open(RulesConfirmDialogComponent, {
          disableClose: true,
          width: '396px',
          height: 'auto',
          data: {
            // title: 'Một số yêu cầu khi cập nhật tài khoản liên kết Zoom ', // 
            mode: 'update'  //nội dung message chi tiết
          }
        });
        dialogRef.afterClosed().subscribe(result => {
          console.log(`result`, result);
          if (result === 'Save') {
            let teacherZoom: TeacherZoomRequest = {
              publicKey: this.publicKeyZoom.value,
              privateKey: this.privateKeyZoom.value,
              email: this.emailZoom.value
            }
            if (JSON.stringify(this.accZoom) == JSON.stringify(teacherZoom)) {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: "Không có gì thay đổi",
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
              return;
            }
            this.teacherZoomEndPoint.updateInformationZoomAPI(teacherZoom).then(res => {
              if (res) {
                this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                  data: "Cập nhật tài khoản liên kết Zoom thành công",
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
                this.accZoom = teacherZoom;
              }
              else {
                this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                  data: "Cập nhật tài khoản liên kết Zoom thất bại",
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            })
          }
        });
      }
    }
  }
}

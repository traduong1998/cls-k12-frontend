import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserInformationRoutingModule } from './user-information-routing.module';
import { UserInfoComponent } from './views/user-info/user-info.component';
import { UserInformationComponent } from './user-information.component';
import { MathModule } from 'src/app/shared/math/math.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContextMenuModule } from 'ngx-contextmenu';
import { ProfileComponent } from './views/profile/profile.component';
import { InfomationTabComponent } from './components/infomation-tab/infomation-tab.component';


@NgModule({
  declarations: [
    UserInformationComponent,
    UserInfoComponent,
    ProfileComponent,
    InfomationTabComponent],
  imports: [
    CommonModule,
    UserInformationRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ContextMenuModule.forRoot(),
  ]
})
export class UserInformationModule { }

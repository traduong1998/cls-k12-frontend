import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfomationTabComponent } from './infomation-tab.component';

describe('InfomationTabComponent', () => {
  let component: InfomationTabComponent;
  let fixture: ComponentFixture<InfomationTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfomationTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfomationTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

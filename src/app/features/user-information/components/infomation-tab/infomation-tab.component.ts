import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { GetUserInfoResponse } from 'sdk/cls-k12-sdk-js/src/services/user/responses/get-user-info-response';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { compareTwoObject } from 'src/app/shared/helpers/cls.helper';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { Logger, UserEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
@Component({
  selector: 'user-infomation-tab',
  templateUrl: './infomation-tab.component.html',
  styleUrls: ['./infomation-tab.component.scss']
})
export class InfomationTabComponent implements OnInit {

  //#region INPUT OUTPUT PROPERTY
  @Input() userInfo: GetUserInfoResponse;
  //#endregion

  //#region ENDPOINT
  private userEndpoint: UserEndpoint
  logger: Logger = Logger.getInstance();

  //#endregion

  //#region PRIVATE PROPERTY

  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  private userIdentity: UserIdentity;
  private personalFormClone;
  //#endregion

  //#region PUBLIC PROPERTY
  public managaName: string;
  currentSchoolYear: number;
  public defaultValue: string = '---';
  personalForm = this.fb.group({
    email: new FormControl('', [Validators.maxLength(100), Validators.pattern(this.emailPattern)]),
    phone: new FormControl(),
    address: new FormControl(),
  });
  get email() { return this.personalForm.get('email'); }
  get phone() { return this.personalForm.get('phone'); }
  get address() { return this.personalForm.get('address'); }

  //#endregion

  //#region CONSTRUCTOR
  constructor(
    private fb: FormBuilder,
    private configService: AppConfigService,
    private _snackBar: MatSnackBar,
    private _authService: AuthService
  ) { }
  //#endregion

  //#region LIFECYCLE
  ngOnInit(): void {
    // gán thông tin
    this.email.setValue(this.userInfo.email);
    this.phone.setValue(this.userInfo.phoneNumber);
    this.address.setValue(this.userInfo.address);
    this.currentSchoolYear = this.configService.getConfig().unit.currentSchoolYear;
    console.log(this.userInfo);
    this.userEndpoint = new UserEndpoint();
    this.userIdentity = this._authService.getTokenInfo();
    if (this.userIdentity.levelManage == 'DPM') {
      this.managaName = this.configService.getConfig().unit.name
    }
    if (this.userIdentity.levelManage == 'DVS') {

      this.managaName = this.userInfo.divisionName
    }
    if (this.userIdentity.levelManage == 'SCH')
      this.managaName = this.userInfo.schoolName
  }

  ngAfterContentInit() {
    this.CloneContentUpdate();
  }
  //#endregion

  //#region PUBLIC METHOD

  /** save other information form */
  public onSubmit() {
    console.log(this.personalForm.value);
    if (this.personalForm.valid) {
      if (compareTwoObject(this.personalFormClone, this.personalForm.value)) {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: "Dữ liệu không thay đổi",
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      } else {
        this.userEndpoint.UpdateProfile(this.personalForm.value).then(res => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: "Chỉnh sửa thành công",
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            this.CloneContentUpdate();
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: "Chỉnh sửa thất bại",
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        }).catch(err => {
          console.warn(err);
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        })
      }
    }

  }

  public isDisableSaveButton() {
    if (compareTwoObject(this.personalFormClone, this.personalForm.value))
      return true
    else return false
  }
  //#endregion

  //#region PRIVATE METHOD
  private CloneContentUpdate() {
    this.personalFormClone = JSON.parse(JSON.stringify(this.personalForm.value));
  }
  //#endregion
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards';
import { UserInformationComponent } from './user-information.component';
import { ProfileComponent } from './views/profile/profile.component';
import { UserInfoComponent } from './views/user-info/user-info.component';
const routes: Routes = [{
  path: '', component: UserInformationComponent,
  canActivate: [AuthGuard],
  children: [{
    path: '',
    component: ProfileComponent,
    data: {
      title: 'Thông tin cá nhân'
    }
  }]
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserInformationRoutingModule { }

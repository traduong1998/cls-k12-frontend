import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SettingSystemComponent } from './views/setting-system/setting-system.component';


const routes: Routes = [{
  path: '',
  component: SettingSystemComponent,
  data: {
    title: 'Cài đặt hệ thống',
  }
}]


@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SettingSystemRoutingModule { }

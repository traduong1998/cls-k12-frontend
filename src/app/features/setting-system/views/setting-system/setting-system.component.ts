import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-setting-system',
  templateUrl: './setting-system.component.html',
  styleUrls: ['./setting-system.component.scss']
})
export class SettingSystemComponent implements OnInit {

  currentTab: string = 'info'
  constructor() { }

  ngOnInit(): void {
  }

  changeTab(tabNumber) {
    if (tabNumber.index == 0) {
      this.currentTab = 'info';
    }
    else if (tabNumber.index == 1) {
      this.currentTab = 'home';
    }
    else if (tabNumber.index == 2) {
      this.currentTab = 'evaluate';
    }
    else if (tabNumber.index == 3) {
      this.currentTab = 'gmail';
    }
  }
}

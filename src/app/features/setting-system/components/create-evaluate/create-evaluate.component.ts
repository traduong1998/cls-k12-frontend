import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { AssessmentLevelGroupEnpoint } from 'sdk/cls-k12-sdk-js/src/services/assessment-level-group/assessment-level-group-enpoint';
import { AssessmentLevelGroupRequest } from 'sdk/cls-k12-sdk-js/src/services/assessment-level-group/requests/assessment-level-group-request';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'app-create-evaluate',
  templateUrl: './create-evaluate.component.html',
  styleUrls: ['./create-evaluate.component.scss']
})
export class CreateEvaluateComponent implements OnInit {

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  assessmentLevelGroupEnpoint: AssessmentLevelGroupEnpoint;
  assessmentLevelGroupRequest: AssessmentLevelGroupRequest = {
    name: '',
    highestScore: 0,
    passedScore: 0,
    isActived: true
  }

  constructor(private mdDialogRef: MatDialogRef<CreateEvaluateComponent>, private _snackBar: MatSnackBar) {
    this.assessmentLevelGroupEnpoint = new AssessmentLevelGroupEnpoint();
  }

  ngOnInit(): void {
  }

  submitForm() {
    if (this.checkForm()) {
      this.assessmentLevelGroupEnpoint.addAssessmentLevelGroup(this.assessmentLevelGroupRequest)
        .then(res => {
          this._snackBar.openFromComponent(SuccessSnackBarComponent, {
            data: 'Thêm thành công!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        })
        .catch(err => {
          debugger
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Thêm thất bại',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        })

      this.mdDialogRef.close('submit');
    }
  }

  checkForm() {

    if (!this.assessmentLevelGroupRequest.name) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: `Bạn chưa nhập tên thang đánh giá`,
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
      return false
    }
    else if (this.assessmentLevelGroupRequest.highestScore == 0) {

      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: `Điểm cao nhất phải khác điểm 0`,
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });

      return false;
    }
    else if (this.assessmentLevelGroupRequest.passedScore == 0) {

      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: `Điểm thấp nhất phải khác điểm 0`,
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
      return false;
    }
    return true

  }

  isActivedChange(event) {
    this.assessmentLevelGroupRequest.isActived = event.checked;
  }
}

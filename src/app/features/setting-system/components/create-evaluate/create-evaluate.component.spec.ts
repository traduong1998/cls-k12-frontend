import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEvaluateComponent } from './create-evaluate.component';

describe('CreateEvaluateComponent', () => {
  let component: CreateEvaluateComponent;
  let fixture: ComponentFixture<CreateEvaluateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateEvaluateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEvaluateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

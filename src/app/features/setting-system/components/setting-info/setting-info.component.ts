import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { InforUnit } from 'sdk/cls-k12-sdk-js/src/services/setting-system/models/infor-unit';
import { UnitConfigRequest } from 'sdk/cls-k12-sdk-js/src/services/setting-system/requests/unit-config-request';
import { SettingSystemEnpoint } from 'sdk/cls-k12-sdk-js/src/services/setting-system/setting-system-enpoint';

import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'app-setting-info',
  templateUrl: './setting-info.component.html',
  styleUrls: ['./setting-info.component.scss']
})
export class SettingInfoComponent implements OnInit {

  emailPattern = '[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}';
  private _currentTab: string;
  @Input() set currentTab(value: string) {
    this._currentTab = value;
    if (this._currentTab === 'info') {
      this.getUnitConfigInfor();
    }
  };

  inforUnit: InforUnit;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  settingSystemEnpoint: SettingSystemEnpoint;

  //form
  unitInfoForm = this.fb.group({
    unitName: new FormControl('', [Validators.required]),
    phoneNumber: new FormControl(''),
    email: new FormControl('', [Validators.required,Validators.pattern(this.emailPattern)]),
    address: new FormControl(''),
    imageBanerDesktopUrl: [''],
    imageBanerMobileUrl: [''],
    imageLogoUrl: [''],
    imageFaviconUrl: ['']
  });

  get unitName() { return this.unitInfoForm.get('unitName') }
  get phoneNumber() { return this.unitInfoForm.get('phoneNumber') }
  get email() { return this.unitInfoForm.get('email') }
  get address() { return this.unitInfoForm.get('address') }

  constructor(private fb: FormBuilder, private _snackBar: MatSnackBar, public dialog: MatDialog) {
    this.settingSystemEnpoint = new SettingSystemEnpoint();
  }

  ngOnInit(): void {
  }

  getUnitConfigInfor() {
    this.settingSystemEnpoint.getUnitConfig().then((res) => {
      this.inforUnit = res;
      this.unitName.setValue(res.unitName);
      this.phoneNumber.setValue(res.phoneNumber);
      this.email.setValue(res.email);
      this.address.setValue(res.address);
    })
  }

  save() {
    if (this.isChange()) {
      let unitConfigRequest: UnitConfigRequest =
      {
        unitName: this.unitName.value, phoneNumber: this.phoneNumber.value, email: this.email.value, address: this.address.value
      }
      this.settingSystemEnpoint.createUnitConfig(unitConfigRequest).then(res => {
        console.log("res", res);

        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: "Thành công",
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });


      }).catch(() => {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: "Thất bại",
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      })
    }
    else {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Bạn chưa thay đổi thông tin",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    }
  }
  cancel() {
    if (this.isChange()) {
      this.getUnitConfigInfor();
    }
  }

  isChange() {
    let isChange = false;
    if (this.inforUnit.unitName != this.unitName.value) {
      isChange = true;
    }
    if (this.inforUnit.phoneNumber != this.phoneNumber.value) {
      isChange = true;
    }
    if (this.inforUnit.email != this.email.value) {
      isChange = true;
    }
    if (this.inforUnit.address != this.address.value) {
      isChange = true;
    }
    return isChange;
  }
}

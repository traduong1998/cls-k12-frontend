import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingConfigGmailComponent } from './setting-config-gmail.component';

describe('SettingConfigGmailComponent', () => {
  let component: SettingConfigGmailComponent;
  let fixture: ComponentFixture<SettingConfigGmailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingConfigGmailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingConfigGmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { CLS } from 'sdk/cls-k12-sdk-js/src';
import { InforEmailConfig } from 'sdk/cls-k12-sdk-js/src/services/setting-system/models/infor-email-config';
import { EmailConfigRequest } from 'sdk/cls-k12-sdk-js/src/services/setting-system/requests/email-config-request';
import { SettingSystemEnpoint } from 'sdk/cls-k12-sdk-js/src/services/setting-system/setting-system-enpoint';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
//import { SMTPClient } from 'emailjs';

@Component({
  selector: 'app-setting-config-gmail',
  templateUrl: './setting-config-gmail.component.html',
  styleUrls: ['./setting-config-gmail.component.scss']
})
export class SettingConfigGmailComponent implements OnInit {
  private _currentTab: string;
  @Input() set currentTab(value: string) {
    this._currentTab = value;
    if (this._currentTab === 'gmail') {
      this.getEmailConfig();
    }
  };

  inforEmailConfig: InforEmailConfig;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  isDisable = true;
  enableSSl = false;
  isEnableSsl = false;
  email = "";

  emailConfigForm = this.fb.group({
    from: new FormControl('', [Validators.required]),
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    host: new FormControl('', [Validators.required]),
    port: new FormControl('', [Validators.required]),
  });

  get from() { return this.emailConfigForm.get('from') }
  get username() { return this.emailConfigForm.get('username') }
  get password() { return this.emailConfigForm.get('password') }
  get host() { return this.emailConfigForm.get('host') }
  get port() { return this.emailConfigForm.get('port') }

  baseApiUrl;

  settingSystemEnpoint: SettingSystemEnpoint;
  constructor(private fb: FormBuilder, private _snackBar: MatSnackBar) {
    this.settingSystemEnpoint = new SettingSystemEnpoint();

    this.baseApiUrl = CLS.getConfig().apiBaseUrl;
  }

  ngOnInit(): void {
    //this.getEmailConfig();
  }
  changeConfig(event) {
    console.log(event)
    this.isDisable = event.checked;
  }

  enableSSL(event) {
    this.enableSSl = event.checked;
  }

  getEmailConfig() {
    this.settingSystemEnpoint.getEmailConfig()
      .then(res => {
        this.inforEmailConfig = res;
        this.from.setValue(res.from)
        this.username.setValue(res.userName)
        this.password.setValue(res.password)
        this.host.setValue(res.host)
        this.port.setValue(res.port)
        this.enableSSl = res.enableSsl;
        this.isEnableSsl = res.enableSsl;
      })
  }

  save() {
    if (this.isChange()) {
      let emailConfig: EmailConfigRequest = {
        from: this.from.value,
        userName: this.username.value,
        password: this.password.value,
        host: this.host.value,
        port: this.port.value,
        enableSsl: this.enableSSl,
        kichHoat: true
      }
      this.settingSystemEnpoint.createEmailConfig(emailConfig)
        .then(res => {
          this._snackBar.openFromComponent(SuccessSnackBarComponent, {
            data: "Thành công",
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.getEmailConfig();

        })
        .catch(() => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: "Thất bại",
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        })
    } else {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Bạn chưa thay đổi thông tin",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    }
  }

  cancel() {
    if (this.isChange()) {
      this.getEmailConfig();
    }
  }
  downLoadFile() {
    return `${this.baseApiUrl}/system/public/files/cdn/docs/huong-dan-cau-hinh-mail.pdf`
  }
  sendMail() {
    if (this.email) {
      this.settingSystemEnpoint.sendMail(this.email)
        .then((res => {

          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: "Gửi mail xác nhận thành công",
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: "Gửi mail xác nhận thất bại",
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }

        }))
    } else {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: "Bạn chưa nhập email gửi xác nhận",
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    }
  }

  isChange() {
    let isChange = false;
    if (this.inforEmailConfig.from != this.from.value) {
      isChange = true;
    }
    if (this.inforEmailConfig.userName != this.username.value) {
      isChange = true;
    }
    if (this.inforEmailConfig.password != this.password.value) {
      isChange = true;
    }
    if (this.inforEmailConfig.host != this.host.value) {
      isChange = true;
    }
    if (this.inforEmailConfig.port != this.port.value) {
      isChange = true;
    }
    if (this.enableSSl != this.isEnableSsl) {
      isChange = true;
    }

    return isChange;
  }
}

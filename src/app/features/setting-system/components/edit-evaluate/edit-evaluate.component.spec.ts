import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEvaluateComponent } from './edit-evaluate.component';

describe('EditEvaluateComponent', () => {
  let component: EditEvaluateComponent;
  let fixture: ComponentFixture<EditEvaluateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditEvaluateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEvaluateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

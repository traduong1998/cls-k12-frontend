import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { AssessmentLevelGroupEnpoint } from 'sdk/cls-k12-sdk-js/src/services/assessment-level-group/assessment-level-group-enpoint';
import { AssessmentLevelGroupUpdateRequest } from 'sdk/cls-k12-sdk-js/src/services/assessment-level-group/requests/assessment-level-group-update-request';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'app-edit-evaluate',
  templateUrl: './edit-evaluate.component.html',
  styleUrls: ['./edit-evaluate.component.scss']
})
export class EditEvaluateComponent implements OnInit {

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  assessmentLevelGroupEnpoint: AssessmentLevelGroupEnpoint;
  assessmentLevelGroupUpdateRequest: AssessmentLevelGroupUpdateRequest = {
    id: null,
    name: '',
    highestScore: 0,
    passedScore: 0,
    isActived: true
  }

  assessmentParentId: number;
  constructor(private mdDialogRef: MatDialogRef<EditEvaluateComponent>, private _snackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data) {
    this.assessmentLevelGroupEnpoint = new AssessmentLevelGroupEnpoint();
    this.assessmentParentId = data;
  }



  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.assessmentLevelGroupEnpoint.getAssessmentLevelGroupById(this.data)
      .then(res => {
        this.assessmentLevelGroupUpdateRequest.id = this.data
        this.assessmentLevelGroupUpdateRequest.name = res.name
        this.assessmentLevelGroupUpdateRequest.highestScore = res.highestScore
        this.assessmentLevelGroupUpdateRequest.passedScore = res.passedScore
        this.assessmentLevelGroupUpdateRequest.isActived = res.isActived

        console.log("12", res);
      })
  }

  submitForm() {
    if (this.checkForm()) {
      debugger
      this.assessmentLevelGroupEnpoint.updateAssessmentLevelGroup(this.assessmentLevelGroupUpdateRequest)
        .then(res => {
          this._snackBar.openFromComponent(SuccessSnackBarComponent, {
            data: 'Cập nhật thành công!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        })
        .catch(err => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Cập nhật thất bại',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        })

      this.mdDialogRef.close('submit');
    }
  }

  checkForm() {
    if (!this.assessmentLevelGroupUpdateRequest.name) {
      return false
    }
    else if (this.assessmentLevelGroupUpdateRequest.highestScore == 0) {
      return false;
    }
    else if (this.assessmentLevelGroupUpdateRequest.passedScore == 0) {
      return false;
    }
    return true
  }

  isActivedChange(event) {
    this.assessmentLevelGroupUpdateRequest.isActived = event.checked;
  }
}

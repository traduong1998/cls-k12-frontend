import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { AssessmentLevelGroupEnpoint } from 'sdk/cls-k12-sdk-js/src/services/assessment-level-group/assessment-level-group-enpoint';
import { AssessmentLevelRequest } from 'sdk/cls-k12-sdk-js/src/services/assessment-level-group/requests/assessment-level-request';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'app-create-level-evaluate',
  templateUrl: './create-level-evaluate.component.html',
  styleUrls: ['./create-level-evaluate.component.scss']
})
export class CreateLevelEvaluateComponent implements OnInit {

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  assessmentLevelRequest: AssessmentLevelRequest = {
    name: '',
    lowestCoefficient: 0,
    assessmentLevelGroupId: 0
  }

  assessmentLevelGroupEnpoint: AssessmentLevelGroupEnpoint;
  constructor(private mdDialogRef: MatDialogRef<CreateLevelEvaluateComponent>, private _snackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data) {
    this.assessmentLevelGroupEnpoint = new AssessmentLevelGroupEnpoint();
  }

  ngOnInit(): void {
  }

  checkForm() {
    if (!this.assessmentLevelRequest.name) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: `Bạn chưa nhập tên mức đánh giá`,
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
      return false;
    }
    else if (this.assessmentLevelRequest.lowestCoefficient == 0) {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: `Điểm mức đánh giá phải khác điểm 0`,
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
      return false;
    }
    return true;
  }

  onSubmit() {
    if (this.checkForm()) {
      this.assessmentLevelRequest.assessmentLevelGroupId = this.data
      this.assessmentLevelGroupEnpoint.addAssessmentLevel(this.assessmentLevelRequest)
        .then(res => {
          this._snackBar.openFromComponent(SuccessSnackBarComponent, {
            data: 'Thêm thành công!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }).catch(err => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Thêm thất bại',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        })
      this.mdDialogRef.close('submit')
    }
  }

}

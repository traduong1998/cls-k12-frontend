import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLevelEvaluateComponent } from './create-level-evaluate.component';

describe('CreateLevelEvaluateComponent', () => {
  let component: CreateLevelEvaluateComponent;
  let fixture: ComponentFixture<CreateLevelEvaluateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateLevelEvaluateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLevelEvaluateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

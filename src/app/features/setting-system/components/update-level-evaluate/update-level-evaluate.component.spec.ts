import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateLevelEvaluateComponent } from './update-level-evaluate.component';

describe('UpdateLevelEvaluateComponent', () => {
  let component: UpdateLevelEvaluateComponent;
  let fixture: ComponentFixture<UpdateLevelEvaluateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateLevelEvaluateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateLevelEvaluateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { AssessmentLevelGroupEnpoint } from 'sdk/cls-k12-sdk-js/src/services/assessment-level-group/assessment-level-group-enpoint';
import { DetailAssessmentLevelGroup } from 'sdk/cls-k12-sdk-js/src/services/assessment-level-group/models/detail-assessment-level-group';
import { AssessmentLevelUpdateRequest } from 'sdk/cls-k12-sdk-js/src/services/assessment-level-group/requests/assessment-level-update-request';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'app-update-level-evaluate',
  templateUrl: './update-level-evaluate.component.html',
  styleUrls: ['./update-level-evaluate.component.scss']
})
export class UpdateLevelEvaluateComponent implements OnInit {

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  assessmentLevelGroupEnpoint: AssessmentLevelGroupEnpoint;
  detailAssessmentLevelGroup: DetailAssessmentLevelGroup;

  assessmentLevelUpdateRequest: AssessmentLevelUpdateRequest = {
    id: null,
    lowestCoefficient: null,
    name: ''
  }
  constructor(private mdDialogRef: MatDialogRef<UpdateLevelEvaluateComponent>, private _snackBar: MatSnackBar, @Inject(MAT_DIALOG_DATA) public data) {
    this.assessmentLevelGroupEnpoint = new AssessmentLevelGroupEnpoint();
  }

  ngOnInit(): void {
    this.getDataChild();
  }


  getDataChild() {
    this.assessmentLevelGroupEnpoint.getDetailAssessmentLevelGroupById(this.data.value)
      .then(res => {
        this.assessmentLevelUpdateRequest.lowestCoefficient = res.lowestCoefficient
        this.assessmentLevelUpdateRequest.name = res.name

        console.log("sasasaS", res)
      })
  }

  checkForm() {
    if (!this.assessmentLevelUpdateRequest.name) {
      return false
    }
    if (!this.assessmentLevelUpdateRequest.lowestCoefficient && !this.data.isWeak) {
      return false
    }
    return true
  }

  onSubmit() {
    if (this.checkForm()) {
      this.assessmentLevelUpdateRequest.id = this.data.value
      this.assessmentLevelGroupEnpoint.updateAssessmentLevel(this.assessmentLevelUpdateRequest)
        .then(res => {
          this._snackBar.openFromComponent(SuccessSnackBarComponent, {
            data: 'Cập nhật thành công!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }).catch(err => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Cập nhật thất bại',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        })
      this.mdDialogRef.close('submit')
    }
  }

}

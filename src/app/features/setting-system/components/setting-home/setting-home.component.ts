import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Observable, of } from 'rxjs';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';
import { InforUnit } from 'sdk/cls-k12-sdk-js/src/services/setting-system/models/infor-unit';
import { ThemeConfig } from 'sdk/cls-k12-sdk-js/src/services/setting-system/models/theme-config';
import { SettingSystemEnpoint } from 'sdk/cls-k12-sdk-js/src/services/setting-system/setting-system-enpoint';
import { ProgressDialogComponent } from 'src/app/shared/progress-dialog/progress-dialog.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'app-setting-home',
  templateUrl: './setting-home.component.html',
  styleUrls: ['./setting-home.component.scss']
})
export class SettingHomeComponent implements OnInit {

  private _currentTab: string;
  @Input() set currentTab(value: string) {
    this._currentTab = value;
    if (this._currentTab === 'home') {
      this.getUnitConfigInfor();
    }
  };
  //
  typeDisplayCtrl: FormControl = new FormControl();



  inforUnit: InforUnit;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>
  numbers: 0;
  progressData: Observable<any>;
  getprogressData(): Observable<any> {
    return of(this.progressData);
  }

  settingSystemEnpoint: SettingSystemEnpoint;
  private fileServer: FileServerEndpoint;

  imageBanerDesktopUrl: any = null;
  imageBanerMobileUrl: any;
  imageLogoMobileUrl: any;
  imageLogoUrl: any;
  imageFaviconUrl: any;

  //
  @ViewChild("inputBannerFile") inputBannerFile: ElementRef;
  @ViewChild("inputLogoFile") inputLogoFile: ElementRef;
  @ViewChild("inputLogoMobileFile") inputLogoMobileFile: ElementRef;
  @ViewChild("inputFaviconFile") inputFaviconFile: ElementRef;
  @ViewChild("inputBannerMobileFile") inputBannerMobileFile: ElementRef;

  //form
  unitInfoForm = this.fb.group({
    imageBanerDesktopUrl: [''],
    imageBanerMobileUrl: [''],
    imageLogoUrl: [''],
    imageLogoMobileUrl: [''],
    imageFaviconUrl: ['']
  });

  get unitName() { return this.unitInfoForm.get('unitName') }
  get phoneNumber() { return this.unitInfoForm.get('phoneNumber') }
  get email() { return this.unitInfoForm.get('email') }
  get address() { return this.unitInfoForm.get('address') }

  //
  current = true;
  constructor(private fb: FormBuilder, private _snackBar: MatSnackBar, public dialog: MatDialog) {
    this.settingSystemEnpoint = new SettingSystemEnpoint();
    this.fileServer = new FileServerEndpoint();
  }

  ngOnInit(): void {
  }

  activeDesktop() {
    top
    this.current = true;
  }

  activeMobile() {
    this.current = false;
  }

  getUnitConfigInfor() {
    this.settingSystemEnpoint.getInforTheme().then((res) => {
      this.imageBanerDesktopUrl = res.desktopBanner;
      this.imageBanerMobileUrl = res.mobileBanner;
      this.imageLogoUrl = res.desktopLogo;
      this.imageLogoMobileUrl = res.mobileLogo;
      this.imageFaviconUrl = res.favicon;
    })
  }

  //common
  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.numbers }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  onChangeImageBannerHandleClick() {
    if (this.unitInfoForm.controls['imageBanerDesktopUrl'].value) {
      return;
    }
    this.inputBannerFile.nativeElement.click();
  }

  onChangeImageBanerInput(event): void {
    var reader = new FileReader();
    let file: File = null;
    if (event.target.files && event.target.files[0]) {
      file = event.target.files[0];
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      Promise.resolve()
        .then(() => {
          console.log(`start process`);
          this.openProgressDialog();
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.fileServer.uploadFile({
            file: file,
            isPrivate: false,
            moduleName: ServiceName.OTHER
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          this.imageBanerDesktopUrl = res ? res.link : null;
        })
        .then((res) => {
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }
  //desktop log
  onChangeImageLogoHandleClick() {
    if (this.unitInfoForm.controls['imageLogoUrl'].value) {
      return;
    }
    this.inputLogoFile.nativeElement.click();
  }

  onChangeImageLogoInput(event) {
    debugger
    var reader = new FileReader();

    reader.onload = () => { // when file has loaded

      debugger
      var img = new Image();
      img.onload = () => {
        debugger
        console.log(img.width + ' vad ' + img.height);
      };

    };


    let file: File = null;
    if (event.target.files && event.target.files[0]) {
      file = event.target.files[0];
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      Promise.resolve()
        .then(() => {
          console.log(`start process`);
          this.openProgressDialog();
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.fileServer.uploadFile({
            file: file,
            isPrivate: false,
            moduleName: ServiceName.OTHER
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          this.imageLogoUrl = res ? res.link : null;
        })
        .then((res) => {
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }

  //mobile logo
  onChangeImageLogoMobileHandleClick() {
    if (this.unitInfoForm.controls['imageLogoMobileUrl'].value) {
      return;
    }
    this.inputLogoMobileFile.nativeElement.click();
  }

  onChangeImageLogoMobileInput(event) {
    var reader = new FileReader();
    let file: File = null;
    if (event.target.files && event.target.files[0]) {
      file = event.target.files[0];
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      Promise.resolve()
        .then(() => {
          console.log(`start process`);
          this.openProgressDialog();
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.fileServer.uploadFile({
            file: file,
            isPrivate: false,
            moduleName: ServiceName.OTHER
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          this.imageLogoMobileUrl = res ? res.link : null;
        })
        .then((res) => {
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }

  //favicon
  onChangeImageFaviconHandleClick() {
    if (this.unitInfoForm.controls['imageFaviconUrl'].value) {
      return;
    }
    this.inputFaviconFile.nativeElement.click();
  }
  onChangeImageFaviconInput(event) {
    var reader = new FileReader();
    let file: File = null;
    if (event.target.files && event.target.files[0]) {
      file = event.target.files[0];
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      Promise.resolve()
        .then(() => {
          console.log(`start process`);
          this.openProgressDialog();
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.fileServer.uploadFile({
            file: file,
            isPrivate: false,
            moduleName: ServiceName.OTHER
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          this.imageFaviconUrl = res ? res.link : null;
        })
        .then((res) => {
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }
  //banner mobile
  onChangeImageBannerMobileHandleClick() {
    if (this.unitInfoForm.controls['imageBanerMobileUrl'].value) {
      return;
    }
    this.inputBannerMobileFile.nativeElement.click();
  }

  onChangeImageBanerMoblieInput(event) {
    var reader = new FileReader();
    let file: File = null;
    if (event.target.files && event.target.files[0]) {
      file = event.target.files[0];
      var mimeType = file.type;
      if (mimeType.match(/image\/*/) == null) {
        return;
      }
      Promise.resolve()
        .then(() => {
          console.log(`start process`);
          this.openProgressDialog();
          //gửi ảnh
          //nếu không chọn ảnh thì khỏi gửi (return null)
          return file ? this.fileServer.uploadFile({
            file: file,
            isPrivate: false,
            moduleName: ServiceName.OTHER
          }, {
            onUploadProgress: (progressEvent) => {
              this.progressData = progressEvent;
              this.getprogressData().subscribe(value => {
                if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                  this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: file.name, catch: false };
                }
              });
            }
          }) : null;
        })
        .then((res) => {
          this.imageBanerMobileUrl = res ? res.link : null;
        })
        .then((res) => {
        })
        .then(() => {
          console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: file.name, catch: false };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        })
        .catch((err) => {
          console.error(`Lỗi trong quá trình xử lý`, err);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: false, fileName: file.name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }

  save() {
    let themeConfig: ThemeConfig =
    {
      desktopBanner: this.imageBanerDesktopUrl,
      mobileBanner: this.imageBanerMobileUrl,
      favicon: this.imageFaviconUrl,
      desktopLogo: this.imageLogoUrl,
      mobileLogo: this.imageLogoMobileUrl
    }


    this.settingSystemEnpoint.createThemeConfig(themeConfig)
      .then(res => {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: "Thành công",
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      })
      .catch(err => {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: "Thất bại",
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      })


    // if (this.isChange()) {
    //   let unitConfigRequest:
    //   //  UnitConfigRequest =
    //   // {
    //   //   unitName: this.unitName.value, phoneNumber: this.phoneNumber.value, email: this.email.value, address: this.address.value, mobileBanner: this.imageBanerMobileUrl, desktopBanner: this.imageBanerDesktopUrl, logo: this.imageLogoUrl, favicon: this.imageFaviconUrl
    //   // }
    //   this.settingSystemEnpoint.createUnitConfig(unitConfigRequest).then(res => {
    //     console.log("res", res);

    //     this._snackBar.openFromComponent(SuccessSnackBarComponent, {
    //       data: "Thành công",
    //       duration: 3000,
    //       horizontalPosition: this.horizontalPosition,
    //       verticalPosition: this.verticalPosition,
    //     });


    //   }).catch(() => {
    //     this._snackBar.openFromComponent(ErrorSnackBarComponent, {
    //       data: "Thất bại",
    //       duration: 3000,
    //       horizontalPosition: this.horizontalPosition,
    //       verticalPosition: this.verticalPosition,
    //     });
    //   })
    // }
    // else {
    //   this._snackBar.openFromComponent(ErrorSnackBarComponent, {
    //     data: "Bạn chưa thay đổi thông tin",
    //     duration: 3000,
    //     horizontalPosition: this.horizontalPosition,
    //     verticalPosition: this.verticalPosition,
    //   });
    // }
  }
  cancel() {
    this.getUnitConfigInfor();
  }

  // isChange() {
  //   let isChange = false;
  //   if (this.inforUnit.unitName != this.unitName.value) {
  //     isChange = true;
  //   }

  //   if (this.inforUnit.phoneNumber != this.phoneNumber.value) {
  //     isChange = true;
  //   }
  //   if (this.inforUnit.email != this.email.value) {
  //     isChange = true;
  //   }
  //   if (this.inforUnit.address != this.address.value) {
  //     isChange = true;
  //   }
  //   // if (this.inforUnit.desktopBanner != this.imageBanerDesktopUrl) {
  //   //   isChange = true;
  //   // }
  //   // if (this.inforUnit.mobileBanner != this.imageBanerMobileUrl) {
  //   //   isChange = true;
  //   // }
  //   // if (this.inforUnit.logo != this.imageLogoUrl) {
  //   //   isChange = true;
  //   // }
  //   // if (this.inforUnit.favicon != this.imageFaviconUrl) {
  //   //   isChange = true;
  //   // }
  //   return isChange;
  // }

  onContextMenuRemoveImageHandler() {
    this.imageLogoUrl = "";
  }

  onContextMenuRemoveFaviconHandler() {
    this.imageFaviconUrl = "";
  }

  onContextMenuRemoveBannerHandler() {
    this.imageBanerDesktopUrl = "";
  }

  onContextMenuRemoveLogoMobileHandler() {
    this.imageBanerDesktopUrl = "";
  }

  onContextMenuRemoveBannerMobileHandler() {
    this.imageBanerMobileUrl = '';
  }
}

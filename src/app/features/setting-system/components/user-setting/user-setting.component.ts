import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { UserSetting } from './interface/user-setting';

@Component({
  selector: 'app-user-setting',
  templateUrl: './user-setting.component.html',
  styleUrls: ['./user-setting.component.scss']
})
export class UserSettingComponent implements OnInit {


  // giá trị cho đăng kí
  typeRegistry = 'wait'
  //kiểu người dùng mặc đinh
  typeUser = 'student'
  // nhóm mặc đinh
  group = '1'

  isEdit = false
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  constructor(private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    let object = JSON.parse(localStorage.getItem('dataSettingUser'));
    console.log(object)
    this.isEdit = object.isEditInfo
    this.typeUser = object.typeUser
    this.typeRegistry = object.typeRegistry
    this.group = object.groupDefault
  }

  registerChange(value) {
    this.typeRegistry = value.value;
  }

  userTypeChange(value) {
    this.typeUser = value.value
  }
  groupChange(value) {
    this.group = value.value;
  }

  isEditChange(event) {
    this.isEdit = event.checked;
  }
  save() {
    if (localStorage.getItem('dataSettingUser')) {
      localStorage.removeItem('dataSettingUser');
    }

    let dataSettingUser: UserSetting = { typeRegistry: this.typeRegistry, typeUser: this.typeUser, groupDefault: this.group, isEditInfo: this.isEdit }

    localStorage.setItem('dataSettingUser', JSON.stringify(dataSettingUser));
    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
      data: "Thành công",
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });

  }
}

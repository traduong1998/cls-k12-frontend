import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingEvaluateComponent } from './setting-evaluate.component';

describe('SettingEvaluateComponent', () => {
  let component: SettingEvaluateComponent;
  let fixture: ComponentFixture<SettingEvaluateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingEvaluateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingEvaluateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

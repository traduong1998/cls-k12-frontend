import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { AssessmentLevelGroupEnpoint } from 'sdk/cls-k12-sdk-js/src/services/assessment-level-group/assessment-level-group-enpoint';
import { AddassessmentLevelGroup } from 'sdk/cls-k12-sdk-js/src/services/assessment-level-group/models/assessment-level-group';
import { DetailAssessmentLevelGroup } from 'sdk/cls-k12-sdk-js/src/services/assessment-level-group/models/detail-assessment-level-group';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { CreateEvaluateComponent } from '../create-evaluate/create-evaluate.component';
import { CreateLevelEvaluateComponent } from '../create-level-evaluate/create-level-evaluate.component';
import { DeleteConfirmComponent } from '../delete-confirm/delete-confirm.component';
import { EditEvaluateComponent } from '../edit-evaluate/edit-evaluate.component';
import { UpdateLevelEvaluateComponent } from '../update-level-evaluate/update-level-evaluate.component';

@Component({
  selector: 'app-setting-evaluate',
  templateUrl: './setting-evaluate.component.html',
  styleUrls: ['./setting-evaluate.component.scss']
})
export class SettingEvaluateComponent implements OnInit {

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  postion: number;
  percent: number;

  private _currentTab: string;
  @Input() set currentTab(value: string) {
    this._currentTab = value;
    if (this._currentTab === 'evaluate') {
      this.getAssessentLevelGroup();
    }
  };

  @ViewChild('singleSelectAssessment', { static: true }) singleSelectAssessment: MatSelect;
  public assessmentCtrl: FormControl = new FormControl();
  protected _onDestroy = new Subject<void>();


  assessmentLevelGroupEnpoint: AssessmentLevelGroupEnpoint;
  addassessmentLevelGroup: AddassessmentLevelGroup[] = []

  selectedItem: number;
  passedScore: number;
  detailAssessmentLevelGroup: DetailAssessmentLevelGroup;
  constructor(public dialog: MatDialog, private _snackBar: MatSnackBar) {
    this.assessmentLevelGroupEnpoint = new AssessmentLevelGroupEnpoint()
  }

  ngOnInit(): void {
  }

  getAssessentLevelGroup() {
    // lấy thông tin thang đánh giá
    let id: number;
    Promise.resolve()
      .then(() => {
        return this.assessmentLevelGroupEnpoint.getAssessmentLevelGroup();
      })
      .then((res) => {
        if (res) {
          this.assessmentCtrl.setValue(res[0].id);
          this.addassessmentLevelGroup = res;
          id = res[0].id;
          this.selectedItem = res[0].id;
          this.passedScore = res[0].passedScore;
          return this.assessmentLevelGroupEnpoint.getDetailAssessmentLevelGroup(id);
        }
      })
      .then(res => {
        this.detailAssessmentLevelGroup = res;
        this.setPositionPassScore();
      })
  }
  // // tính vị trí điểm đạt tai compont nào
  setPositionPassScore() {

    var i;
    if (this.detailAssessmentLevelGroup.listAssessmentLevelDTO.length == 1) {

      this.postion = this.detailAssessmentLevelGroup.listAssessmentLevelDTO[0].id;
      let kc = this.detailAssessmentLevelGroup.highestScore - this.detailAssessmentLevelGroup.listAssessmentLevelDTO[0].lowestCoefficient;

      let vitri = this.passedScore - this.detailAssessmentLevelGroup.listAssessmentLevelDTO[0].lowestCoefficient;

      this.percent = (vitri / kc) * 100;

    } else {

      for (i = 0; i < this.detailAssessmentLevelGroup.listAssessmentLevelDTO.length; i++) {
        if (i < this.detailAssessmentLevelGroup.listAssessmentLevelDTO.length) {
          if (this.passedScore >= this.detailAssessmentLevelGroup.listAssessmentLevelDTO[i + 1]?.lowestCoefficient &&
            this.passedScore <= this.detailAssessmentLevelGroup.listAssessmentLevelDTO[i].lowestCoefficient) {
            //vị trí nằm đây
            this.postion = this.detailAssessmentLevelGroup.listAssessmentLevelDTO[i + 1].id
            // tính khoảng cách bao nhiêu
            let kc = this.detailAssessmentLevelGroup.listAssessmentLevelDTO[i].lowestCoefficient - this.detailAssessmentLevelGroup.listAssessmentLevelDTO[i + 1].lowestCoefficient

            let a = this.passedScore - this.detailAssessmentLevelGroup.listAssessmentLevelDTO[i + 1].lowestCoefficient
            //phần trăm+
            this.percent = (a / kc) * 100;
          }
        }
      }


      if (this.passedScore <= this.detailAssessmentLevelGroup.highestScore && this.passedScore >= this.detailAssessmentLevelGroup.listAssessmentLevelDTO[0].lowestCoefficient) {
        this.postion = this.detailAssessmentLevelGroup.listAssessmentLevelDTO[0].id;
        let kc = this.detailAssessmentLevelGroup.highestScore - this.detailAssessmentLevelGroup.listAssessmentLevelDTO[0].lowestCoefficient;

        let vitri = this.passedScore - this.detailAssessmentLevelGroup.listAssessmentLevelDTO[0].lowestCoefficient;

        this.percent = (vitri / kc) * 100;
      }
    }


  }

  assessmentChange(event) {
    this.detailAssessmentLevelGroup = null
    this.selectedItem = event.value;
    Promise.resolve()
      .then(() => {
        return this.assessmentLevelGroupEnpoint.getAssessmentLevelGroupById(event.value);
      })
      .then((res) => {
        this.passedScore = res.passedScore;
        return this.assessmentLevelGroupEnpoint.getDetailAssessmentLevelGroup(event.value)
      })
      .then((res) => {
        this.detailAssessmentLevelGroup = res;
        this.setPositionPassScore();
      })
  }

  addEvaluate() {
    const dialogRef = this.dialog.open(CreateEvaluateComponent, {
      disableClose: true,
      minWidth: '550px',
      height: 'auto',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getAssessentLevelGroup();
        }
      }
    });
  }

  updateEvaluate() {
    const dialogRef = this.dialog.open(EditEvaluateComponent, {
      disableClose: true,
      minWidth: '550px',
      height: 'auto',
      data: this.assessmentCtrl.value
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */

          this.getListChild(this.selectedItem);
        }
      }
    });
  }

  deleteEvaluate() {
    const dialogRef = this.dialog.open(DeleteConfirmComponent, {
      disableClose: true,
      minWidth: '303px',
      height: 'auto',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === true) {
          this.assessmentLevelGroupEnpoint.deleteAssessmentLevelGroup(this.assessmentCtrl.value)
            .then(res => {
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: 'Xóa thành công',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });

              this.getAssessentLevelGroup();
            })
            .catch(err => {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Xóa thất bại',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            })
        }
      }
    });
  }

  //child
  getListChild(parentId: number) {
    this.detailAssessmentLevelGroup = null
    Promise.resolve()
      .then(() => {
        return this.assessmentLevelGroupEnpoint.getDetailAssessmentLevelGroup(parentId);
      }).then((res) => {
        this.passedScore = res.passedScore;
        this.detailAssessmentLevelGroup = res;

        this.setPositionPassScore();
      })

  }

  addAssessmentLevel() {

    const dialogRef = this.dialog.open(CreateLevelEvaluateComponent, {
      disableClose: true,
      minWidth: '550px',
      height: 'auto',
      data: this.selectedItem
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          //
          /**Nếu là submit sẽ load lại data */
          this.getListChild(this.selectedItem);
        }
      }
    });
  }

  // kiểm tra thèn này có phải là thèn yêu k
  checkIsWeak(id: number) {
    if (id == this.detailAssessmentLevelGroup.listAssessmentLevelDTO[this.detailAssessmentLevelGroup.listAssessmentLevelDTO.length - 1].id) {
      return true;
    }
    return false;
  }

  updateAssessmentLevel(value) {
    debugger
    let isWeak = this.checkIsWeak(value);

    const dialogRef = this.dialog.open(UpdateLevelEvaluateComponent, {
      disableClose: true,
      minWidth: '550px',
      height: 'auto',
      data: { value: value, isWeak: isWeak }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          //this.getSchoolsData();
          this.getListChild(this.selectedItem);
        }
      }
    });
  }

  deleteAssessmentLevel(value) {
    const dialogRef = this.dialog.open(DeleteConfirmComponent, {
      disableClose: true,
      minWidth: '303px',
      height: 'auto',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === true) {
          /**Nếu là submit sẽ load lại data */
          this.assessmentLevelGroupEnpoint.deleteAssessmentLevel(value)
            .then(res => {
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: 'Xóa thành công',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
              this.getListChild(this.selectedItem);
            })
            .catch(err => {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Xóa thất bại',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            })
        }
      }
    });
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { AssessmentLevelGroupEnpoint } from 'sdk/cls-k12-sdk-js/src/services/assessment-level-group/assessment-level-group-enpoint';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { CreateLevelEvaluateComponent } from '../create-level-evaluate/create-level-evaluate.component';
import { DeleteConfirmComponent } from '../delete-confirm/delete-confirm.component';
import { UpdateLevelEvaluateComponent } from '../update-level-evaluate/update-level-evaluate.component';

@Component({
  selector: 'app-item-evaluate',
  templateUrl: './item-evaluate.component.html',
  styleUrls: ['./item-evaluate.component.scss']
})
export class ItemEvaluateComponent implements OnInit {

  @Output() addAssessmentLevel = new EventEmitter();
  @Output() updateAssessmentLevel = new EventEmitter();
  @Output() deleteAssessmentLevel = new EventEmitter();


  @Input() data;
  @Input() parentId: number;
  @Input() postion: number;
  @Input() percent: number;
  @Input() passedScore: number;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  assessmentLevelGroupEnpoint: AssessmentLevelGroupEnpoint;
  constructor(public dialog: MatDialog, private _snackBar: MatSnackBar) {
    this.assessmentLevelGroupEnpoint = new AssessmentLevelGroupEnpoint();
  }

  ngOnInit(): void {
    console.log("percent", this.percent)
  }

  addLevelEvaluate() {
    let a = '111';
    this.addAssessmentLevel.emit(a);
  }
  updateLevelEvaluate(id) {
    this.updateAssessmentLevel.emit(id);
  }

  deleteLevelEvaluate(id) {
    this.deleteAssessmentLevel.emit(id)
  }
}

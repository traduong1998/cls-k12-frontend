import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemEvaluateComponent } from './item-evaluate.component';

describe('ItemEvaluateComponent', () => {
  let component: ItemEvaluateComponent;
  let fixture: ComponentFixture<ItemEvaluateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemEvaluateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemEvaluateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

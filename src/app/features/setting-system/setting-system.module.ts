import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingSystemComponent } from './views/setting-system/setting-system.component';
import { SettingSystemRoutingModule } from './setting-system-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { SettingEvaluateComponent } from './components/setting-evaluate/setting-evaluate.component';
import { SettingConfigGmailComponent } from './components/setting-config-gmail/setting-config-gmail.component';
import { SettingInfoComponent } from './components/setting-info/setting-info.component';
import { ContextMenuModule } from 'ngx-contextmenu';
import { SettingHomeComponent } from './components/setting-home/setting-home.component';
import { CreateEvaluateComponent } from './components/create-evaluate/create-evaluate.component';
import { EditEvaluateComponent } from './components/edit-evaluate/edit-evaluate.component';
import { UpdateLevelEvaluateComponent } from './components/update-level-evaluate/update-level-evaluate.component';
import { DeleteConfirmComponent } from './components/delete-confirm/delete-confirm.component';
import { CreateLevelEvaluateComponent } from './components/create-level-evaluate/create-level-evaluate.component';
import { ItemEvaluateComponent } from './components/item-evaluate/item-evaluate.component';
import { UserSettingComponent } from './components/user-setting/user-setting.component';



@NgModule({
  declarations: [
    SettingSystemComponent,
    SettingInfoComponent,
    SettingEvaluateComponent,
    SettingConfigGmailComponent, SettingHomeComponent, ItemEvaluateComponent, CreateEvaluateComponent, EditEvaluateComponent, UpdateLevelEvaluateComponent, DeleteConfirmComponent, CreateLevelEvaluateComponent, UserSettingComponent],
  imports: [
    CommonModule,
    SettingSystemRoutingModule,
    SharedModule,
    ContextMenuModule.forRoot()
  ]
})
export class SettingSystemModule { }

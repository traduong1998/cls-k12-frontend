import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentUpdateSubjectComponent } from './content-update-subject.component';

describe('ContentUpdateSubjectComponent', () => {
  let component: ContentUpdateSubjectComponent;
  let fixture: ComponentFixture<ContentUpdateSubjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentUpdateSubjectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentUpdateSubjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

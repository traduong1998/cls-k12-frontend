import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SubjectEndpoint } from 'cls-k12-sdk-js/src/services/subject/endpoints/SubjectEndpoint';
import { UpdateSubjectRequest } from 'cls-k12-sdk-js/src/services/subject/requests/updateSubjectRequest';
import { Subject } from 'cls-k12-sdk-js/src/services/subject/models/Subject';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { SubjectIcon } from 'src/app/shared/classes/subject-icon';


@Component({
  selector: 'app-content-update-subject',
  templateUrl: './content-update-subject.component.html',
  styleUrls: ['./content-update-subject.component.scss']
})
export class ContentUpdateSubjectComponent implements OnInit {

  subjectEndpoint: SubjectEndpoint;
  lblname = '';
  lblOrderNumber = '';
  baseApiUrl = 'http://localhost:65000';

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  subject: UpdateSubjectRequest = {
    name: '',
    icon: ''
  };

  selected: string;
  subjectIcon: SubjectIcon[] = [
    { name: 'Toán', link: 'math' },
    { name: 'Ngoại ngữ', link: 'English' },
    { name: 'Âm nhạc', link: 'music' },
    { name: 'Mỹ thuật', link: 'art' },
    { name: 'Địa lý', link: 'Geography' },
    { name: 'Lịch sử', link: 'History' },
    { name: 'Vật lý', link: 'physical' },
    { name: 'Hóa học', link: 'chemistry' },
    { name: 'Công nghệ', link: 'NaturalSciences' },
    { name: 'Tin học', link: 'InformationTechnology' },
    { name: 'Sinh học', link: 'biological' },
    { name: 'Đạo đức', link: 'morality' },
    { name: 'Khác', link: 'elective' },
    { name: 'Khác', link: 'economic' },
    { name: 'Giáo dục công dân', link: 'civiceducation' },
    { name: 'Thể dục', link: 'PhysicalEducation' }
  ];

  constructor(private mdDialogRef: MatDialogRef<ContentUpdateSubjectComponent>, @Inject(MAT_DIALOG_DATA) public subjectInfo: Subject, private _snackBar: MatSnackBar) {
    this.subjectEndpoint = new SubjectEndpoint({ baseUrl: this.baseApiUrl });


    this.selected = this.subjectInfo.icon;
    console.log("data", subjectInfo)
  }


  formSubject: Subject = {
    name: this.subjectInfo.name,
    orderNumber: this.subjectInfo.orderNumber
  }

  ngOnInit(): void {
  }

  @ViewChild('regForm') myForm: NgForm;


  choseIcon(icon) {
    this.subject.icon = icon.link;
    this.selected = icon.link;
  }
  temp = this.subjectIcon;
  findIcon() {
    debugger
    let tempFunc = [];
    if (this.formSubject.name) {
      this.subjectIcon = this.temp;
      this.subjectIcon.forEach((item) => {
        if (item.name.toLowerCase().includes(this.formSubject.name.toLowerCase())) {
          tempFunc.push(item);
        }
      })
      if (tempFunc.length == 0) {
        this.subjectIcon = this.temp;
      } else {
        this.subjectIcon = tempFunc;
      }

      this.selected = this.subjectIcon[0].link;

      this.subject.icon = this.subjectIcon[0].link;
    } else {
      this.subjectIcon = this.temp;
      this.selected = this.subjectIcon[0].link;
      this.subject.icon = this.subjectIcon[0].link;
    }
  }

  resetForm() {
    this.lblname = '';
    this.lblOrderNumber = '';
  }

  checkForm() {
    var result = true;

    if (this.formSubject.name === '') {
      this.lblname = 'Bạn chưa nhập tên môn học!';
      result = false;
    }
    if (!this.formSubject.orderNumber) {
      this.lblOrderNumber = 'Bạn chưa nhập số thứ tự!';
      result = false;
    } else if (!Number.isInteger(Number(this.formSubject.orderNumber))) {
      this.lblOrderNumber = 'Số thứ tự phải là kiểu số';
      result = false;
    }

    return result;
  }

  EditDivision(regForm: NgForm) {

    this.resetForm();

    if (this.checkForm()) {
      this.subjectEndpoint.checkNameSubject(this.subjectInfo["id"], this.formSubject.name)
        .then(res => {
          //hàm này đặc thù trả về true, false nên sẽ vô then. nếu hàm bình thường thì thường backend sẽ thow exception nếu k thỏa điều kiện để update hoặc đầu vào bị validate
          if (res) {
            this.lblname = 'Tên môn học đã tồn tại trong hệ thống!';
            return;
          }

          this.subject["id"] = this.subjectInfo["id"];
          this.subject["name"] = this.formSubject.name;
          this.subject["orderNumber"] = this.formSubject.orderNumber;
          debugger
          this.subjectEndpoint.updateSubject(this.subject)
            .then(res => {
              this.mdDialogRef.close('submit')
              if (res) {
                this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                  data: 'Sửa môn thành công',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              } else {
                this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                  data: 'Sửa môn không thành công',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }
            })
            .catch((err: ErrorResponse) => {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: err.errorDetail,
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            });
        })
        .catch((err: ErrorResponse) => {
          //response khác 200 sẽ vào catch. nen trên backend cần trả cho đúng
          //catch thì k close mdDialogRef
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });


    }
  }

}

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { Subject } from 'cls-k12-sdk-js/src/services/subject/models/Subject';
import { CreateSubjectRequest } from 'cls-k12-sdk-js/src/services/subject/requests/createSubjectRequest';
import { SubjectEndpoint } from 'cls-k12-sdk-js/src/services/subject/endpoints/SubjectEndpoint';
import { SubjectFilterRequests } from 'cls-k12-sdk-js/src/services/subject/models/SubjectPagingRequest';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';
import { MatDialogRef } from '@angular/material/dialog';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { SubjectIcon } from 'src/app/shared/classes/subject-icon';

const subjectsPaginationDefault: PaginatorResponse<Subject> = {
  items: [],
  totalItems: 0
};


@Component({
  selector: 'app-content-create-subject',
  templateUrl: './content-create-subject.component.html',
  styleUrls: ['./content-create-subject.component.scss']
})
export class ContentCreateSubjectComponent implements OnInit {

  subjectEndpoint: SubjectEndpoint;
  baseApiUrl = 'http://localhost:65000';
  lblname = '';
  lblOrderNumber = '';
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  subject: CreateSubjectRequest = {
    name: '',
    icon: 'elective'
  };
  subjectsPagination: PaginatorResponse<Subject> = subjectsPaginationDefault;
  subjectsPaginationFilter: SubjectFilterRequests = {
    pageNumber: 0,
    sizeNumber: 10,
    getCount: true,
    sortDirection: 'DESC',
    sortField: 'CRE',
  }

  subjectIcon: SubjectIcon[] = [
    { name: 'Toán', link: 'math' },
    { name: 'Ngoại ngữ', link: 'English' },
    { name: 'Âm nhạc', link: 'music' },
    { name: 'Mỹ thuật', link: 'art' },
    { name: 'Địa lý', link: 'Geography' },
    { name: 'Lịch sử', link: 'History' },
    { name: 'Vật lý', link: 'physical' },
    { name: 'Hóa học', link: 'chemistry' },
    { name: 'Công nghệ', link: 'NaturalSciences' },
    { name: 'Tin học', link: 'InformationTechnology' },
    { name: 'Sinh học', link: 'biological' },
    { name: 'Đạo đức', link: 'morality' },
    { name: 'Khác', link: 'elective' },
    { name: 'Khác', link: 'economic' },
    { name: 'Giáo dục công dân', link: 'civiceducation' },
    { name: 'Thể dục', link: 'PhysicalEducation' }
  ];

  constructor(private mdDialogRef: MatDialogRef<ContentCreateSubjectComponent>, private _snackBar: MatSnackBar) {
    this.subjectEndpoint = new SubjectEndpoint({ baseUrl: this.baseApiUrl });
    mdDialogRef.disableClose = true;
  }

  ngOnInit(): void {
  }

  failureCallback() {
    alert("hihi 400");
  }

  getSubjectsData() {
    this.subjectEndpoint.getSubjectsPaging(this.subjectsPaginationFilter)
      .then(res => {
        this.subjectsPagination = res;
      })
      .catch(this.failureCallback);
  }

  resetForm() {
    this.lblname = '';
    this.lblOrderNumber = '';
  }

  checkForm() {

    var result = true;
    if (this.subject.name === '') {
      this.lblname = 'Bạn chưa nhập tên môn học!';
      result = false;
    }
    if (!this.subject.orderNumber) {
      this.lblOrderNumber = 'Bạn chưa nhập số thứ tự!';
      result = false;
    } else if (!Number.isInteger(Number(this.subject.orderNumber))) {
      this.lblOrderNumber = 'Số thứ tự phải là kiểu số';
      result = false;
    }

    return result;
  }

  selected = 'math';
  choseIcon(icon) {
    this.subject.icon = icon.link;
    this.selected = icon.link;
  }
  temp = this.subjectIcon;
  findIcon() {
    let tempFunc = [];
    if (this.subject.name) {
      this.subjectIcon = this.temp;
      this.subjectIcon.forEach((item) => {
        if (item.name.toLowerCase().includes(this.subject.name.toLowerCase())) {
          tempFunc.push(item);
        }
      })
      if (tempFunc.length == 0) {
        this.subjectIcon = this.temp;
      } else {
        this.subjectIcon = tempFunc;
      }

      this.selected = this.subjectIcon[0].link;

      this.subject.icon = this.subjectIcon[0].link;
    } else {
      this.subjectIcon = this.temp;
      this.selected = this.subjectIcon[0].link;
      this.subject.icon = this.subjectIcon[0].link;
    }
  }

  submitForm(regForm: NgForm) {
    this.resetForm();

    if (this.checkForm()) {
      //this.subject.orderNumber
      this.subjectEndpoint.createSubject(this.subject)

        .then(res => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Thêm môn thành công',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            this.mdDialogRef.close('submit');
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Thêm môn không thành công',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
    }
  }

}



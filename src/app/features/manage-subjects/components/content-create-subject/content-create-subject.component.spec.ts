import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentCreateSubjectComponent } from './content-create-subject.component';

describe('ContentCreateSubjectComponent', () => {
  let component: ContentCreateSubjectComponent;
  let fixture: ComponentFixture<ContentCreateSubjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentCreateSubjectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentCreateSubjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

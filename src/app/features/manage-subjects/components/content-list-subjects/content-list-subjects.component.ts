import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { Subject } from 'cls-k12-sdk-js/src/services/subject/models/Subject';
import { SubjectEndpoint } from 'cls-k12-sdk-js/src/services/subject/endpoints/SubjectEndpoint';
import { SubjectFilterRequests } from 'cls-k12-sdk-js/src/services/subject/models/SubjectPagingRequest';
import { ContentCreateSubjectComponent } from '../content-create-subject/content-create-subject.component';
import { ContentUpdateSubjectComponent } from '../content-update-subject/content-update-subject.component';
import { AuthService } from 'src/app/core/services';
import { UserIdentity, CLSModules, CLSPermissions } from 'cls-k12-sdk-js/src';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { DeleteSubjectRequest } from 'sdk/cls-k12-sdk-js/src/services/subject/requests/deleteSubjectRequest';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

const subjectsPaginationDefault: PaginatorResponse<Subject> = {
  items: [],
  totalItems: 0
};


@Component({
  selector: 'app-content-list-subjects',
  templateUrl: './content-list-subjects.component.html',
  styleUrls: ['./content-list-subjects.component.scss']
})
export class ContentListSubjectsComponent implements OnInit {

  subjectEndpoint: SubjectEndpoint;

  baseApiUrl = 'http://localhost:65000';

  // displayedColumns: string[] = ['select', 'id', 'name', 'orderNumber', 'function'];
  displayedColumns: string[] = ['stt', 'name', 'orderNumber', 'function'];

  subjectsPagination: PaginatorResponse<Subject> = subjectsPaginationDefault;
  subjectsPaginationFilter: SubjectFilterRequests = {
    pageNumber: 0,
    sizeNumber: 10,
    getCount: true,
    sortDirection: 'DESC',
    sortField: 'CRE',
  }

  selection = new SelectionModel<Subject>(true, []);
  selectedSubjectIds = [];
  isLoadingResults = true;

  userIdentity: UserIdentity;
  hasAddPermission: boolean;
  hasDeletePermission: boolean;
  hasEditPermission: boolean;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(public dialog: MatDialog, private _router: Router, private _authService: AuthService, private spinner: NgxSpinnerService, private _snackBar: MatSnackBar) {
    this.subjectEndpoint = new SubjectEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();
    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.Subject, CLSPermissions.Add);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.Subject, CLSPermissions.Edit);
    this.hasDeletePermission = this.userIdentity.hasPermission(CLSModules.Subject, CLSPermissions.Delete);
    this.spinner.show();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit(): void {
    this.getSubjectsData();
  }

  failureCallback() {
    this.isLoadingResults = false;
    alert("hihi 400");
  }

  getSubjectsData() {
    this.isLoadingResults = true;
    this.subjectEndpoint.getSubjectsPaging(this.subjectsPaginationFilter)
      .then(res => {
        debugger
        this.spinner.hide();
        this.isLoadingResults = false;
        this.subjectsPagination = res;
      })
      .catch(this.failureCallback);
  }


  //constructor() { }
  openDialog() {
    debugger;
    const dialogRef = this.dialog.open(ContentCreateSubjectComponent, {
      width: '450px',
      height: '416px',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getSubjectsData();
        }
      }
    });
  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.subjectsPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.subjectsPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.getSubjectsData();
    });

  }

  /**Show dialog Update Subject */
  UpdateSubject(data) {
    this.selection.clear();
    //show dialog
    const dialogRef = this.dialog.open(ContentUpdateSubjectComponent, {
      disableClose: true,
      width: '450px',
      height: 'auto',
      data: data
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getSubjectsData();
        }
      }
    });
  }

  /**Show dialog Delete Subject */
  DeleteSubject(data) {
    this.selection.clear();
    const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
      disableClose: true,
      width: '444px',
      height: 'auto',
      //data: data
      data: {
        title: 'Xóa môn học',
        name: data.name,
        message: ''
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 'DONGY') {
        let model = new DeleteSubjectRequest();
        model.id = data.id;
        this.subjectEndpoint.deleteSubject(model)
          .then(res => {
            if (res) {
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: 'Xóa môn học thành công',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });

              this.getSubjectsData();
            } else {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Xóa môn học không thành công',
                duration: 3000,
                horizontalPosition: this.horizontalPosition,
                verticalPosition: this.verticalPosition,
              });
            }
          })
          .catch((err: ErrorResponse) => {
            this._snackBar.open(err.errorDetail, 'Ok', {
              duration: 5000,
              panelClass: ['red-snackbar'],
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          });
      }
      else {
        if (result != undefined) {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Bạn nhập chưa đúng, vui lòng thử lại',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      }
    });
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.subjectsPagination.items.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.selectedSubjectIds = [];
    } else {
      this.subjectsPagination.items.forEach(row => {
        this.selection.select(row);
        this.selectedSubjectIds.push(row.id);
      })
    }
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Subject): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }

}

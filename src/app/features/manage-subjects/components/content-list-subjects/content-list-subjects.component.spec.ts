import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentListSubjectsComponent } from './content-list-subjects.component';

describe('ContentListSubjectsComponent', () => {
  let component: ContentListSubjectsComponent;
  let fixture: ComponentFixture<ContentListSubjectsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentListSubjectsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentListSubjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

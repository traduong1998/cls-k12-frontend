import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { ManageSubjectsRoutingModule } from './manage-subjects-routing.module';
import { ManageSubjectsComponent } from './manage-subjects.component';
import { CreateSubjectComponent } from './views/create-subject/create-subject.component';
import { UpdateSubjectComponent } from './views/update-subject/update-subject.component';
import { ListSubjectsComponent } from './views/list-subjects/list-subjects.component';
import { ContentCreateSubjectComponent } from './components/content-create-subject/content-create-subject.component';
import { ContentUpdateSubjectComponent } from './components/content-update-subject/content-update-subject.component';

import { ContentListSubjectsComponent } from './components/content-list-subjects/content-list-subjects.component';
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [ManageSubjectsComponent, CreateSubjectComponent, UpdateSubjectComponent, ListSubjectsComponent, ContentCreateSubjectComponent, ContentUpdateSubjectComponent, ContentListSubjectsComponent],
  imports: [
    SharedModule,
    ManageSubjectsRoutingModule,
    NgxSpinnerModule
  ]
})
export class ManageSubjectsModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards';
import { ManageSubjectsComponent } from './manage-subjects.component';
import { CreateSubjectComponent } from './views/create-subject/create-subject.component';
import { UpdateSubjectComponent } from './views/update-subject/update-subject.component';
import { ListSubjectsComponent } from './views/list-subjects/list-subjects.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: ManageSubjectsComponent,
    children: [
      {
        path: '',
        component: ListSubjectsComponent,
        data: {
          title: 'Danh sách môn học',
        }
      },
      {
        path: 'create',
        component: CreateSubjectComponent,
        data: {
          title: 'Thêm mới môn học',
        }
      },
      {
        path: 'update',
        component: UpdateSubjectComponent,
        data: {
          title: 'Chỉnh sửa môn học',
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageSubjectsRoutingModule { }

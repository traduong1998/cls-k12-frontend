import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { GroupStudentEndpoint, UserEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { take, takeUntil } from 'rxjs/operators';
import { TeacherOption } from 'sdk/cls-k12-sdk-js/src/services/user/models/teacher';
import { AssignTeacherGroupStudent } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/assign-teacher-groupstudent';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';


@Component({
  selector: 'app-assign-teacher-dialog',
  templateUrl: './assign-teacher-dialog.component.html',
  styleUrls: ['./assign-teacher-dialog.component.scss']
})
export class AssignTeacherDialogComponent implements OnInit {
  groupStudentEndpoint: GroupStudentEndpoint;
  userEndpoint: UserEndpoint;
  baseApiUrl = 'http://localhost:65000';

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  groupStudentId: number;
  schoolId: number;
  schoolYear: number;
  isFirstLoadTeachers: boolean = true;
  constructor(@Inject(MAT_DIALOG_DATA) public groupStudent: AssignTeacherGroupStudent, private _snackBar: MatSnackBar, private mdDialogRef: MatDialogRef<AssignTeacherDialogComponent>) {
    this.groupStudentEndpoint = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
    this.userEndpoint = new UserEndpoint({ baseUrl: this.baseApiUrl });

    this.groupStudentId = groupStudent.groupStudentId;
    this.schoolId = groupStudent.schoolId;
    this.schoolYear = groupStudent.schoolYear;
  }



  protected teachers: TeacherOption[];
  public teacherCtrl: FormControl = new FormControl(null, Validators.required);

  public teacherFilterCtrl: FormControl = new FormControl();
  public filteredTeachers: ReplaySubject<TeacherOption[]> = new ReplaySubject<TeacherOption[]>(1);
  @ViewChild('singleSelectTeacher', { static: true }) singleSelectTeacher: MatSelect;

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();

  ngOnInit(): void {
    // listen for search field value changes
    this.teacherFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterTeachers();
      });
  }
  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }
  protected setInitialValue() {
    this.filteredTeachers
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectTeacher.compareWith = (a: TeacherOption, b: TeacherOption) => a && b && a.id === b.id;
      });
  }
  protected filterTeachers() {
    if (!this.teachers) {
      return;
    }
    // get the search keyword
    let search = this.teacherFilterCtrl.value;
    if (!search) {
      this.filteredTeachers.next(this.teachers.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredTeachers.next(
      this.teachers.filter(teacher => teacher.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onTeacherSelectClicked() {
    if (this.isFirstLoadTeachers) {
      this.userEndpoint.getTeacherOfSchoolOptions(this.schoolId)
        .then(res => {
          this.isFirstLoadTeachers = false;
          this.teachers = res;
          this.filteredTeachers.next(this.teachers.slice());
        })
        .catch(

        )
    } else {

    }
  }

  submitForm() {
    if (this.teacherCtrl.valid) {
      this.groupStudentEndpoint.assignTeacherToGroupStudent(this.groupStudentId, this.teacherCtrl.value, this.schoolYear)
        .then(res => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Phân công giáo viên thành công',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Phân công giáo viên không thành công',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
      this.mdDialogRef.close('submit')
    }
  }

}



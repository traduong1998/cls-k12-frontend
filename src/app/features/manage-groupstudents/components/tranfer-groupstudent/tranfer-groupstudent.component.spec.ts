import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranferGroupstudentComponent } from './tranfer-groupstudent.component';

describe('TranferGroupstudentComponent', () => {
  let component: TranferGroupstudentComponent;
  let fixture: ComponentFixture<TranferGroupstudentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TranferGroupstudentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TranferGroupstudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

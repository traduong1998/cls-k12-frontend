import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { GradeEndpoint, GroupStudentEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { AssignStudentsToGroupStudentSameGrade } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/AssignStudentsToGroupStudentSameGrade';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'app-tranfer-groupstudent',
  templateUrl: './tranfer-groupstudent.component.html',
  styleUrls: ['./tranfer-groupstudent.component.scss']
})
export class TranferGroupstudentComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';
  selectedGrade: number = 0;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  protected groupStudents: GroupStudentOption[];
  public groupStudentCtrl: FormControl = new FormControl('', Validators.required);
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);

  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;
  protected _onDestroy = new Subject<void>();

  gradeEndpoint: GradeEndpoint;
  endpointGroupStudent: GroupStudentEndpoint;
  constructor(@Inject(MAT_DIALOG_DATA) public data, private _snackBar: MatSnackBar, public dialogRef: MatDialogRef<TranferGroupstudentComponent>) {
    this.gradeEndpoint = new GradeEndpoint();
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
  }

  ngOnInit(): void {
    this.getGradeName();

    this.getGroupStudentName();

    this.groupStudentFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudents();
      });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  gradeName: string;
  getGradeName() {
    this.gradeEndpoint.getGradeInfo(this.data.gradeId).then((res) => {
      this.gradeName = res.gradeName;
    })
  }
  className
  getGroupStudentName() {
    this.endpointGroupStudent.getGroupStudentDetail(this.data.classId).then(res => {
      this.className = res.groupStudentName;
    })
  }


  submitForm() {
    if(this.groupStudentCtrl.value.id){
      let assignStudentsToGroupStudentSameGrade: AssignStudentsToGroupStudentSameGrade = { groupStudentId: this.groupStudentCtrl.value.id, schoolYear: this.data.schoolYear, userIds: this.data.userId };
      this.endpointGroupStudent.assignStudentsToGroupStudentSameGrade(assignStudentsToGroupStudentSameGrade).then(res => {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: 'Chuyển lớp thành công',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        this.dialogRef.close(true);
      }).catch((err) => {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Chuyển lớp thất bại',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      })
    }
    else{
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Bạn chưa chọn lớp để chuyển',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    }
  }

  onGroupStudentSelectClicked() {
    this.endpointGroupStudent.getGroupStudentOptions(this.data.schoolId, this.data.gradeId)
      .then(res => {
        let index = res.findIndex(x => x.id == this.data.classId);
        if (index >= 0) {
          res.splice(index,1);
        }

        this.groupStudents = res;
        this.filteredGroupStudents.next(this.groupStudents.slice());
      });
  }

  onChangeGroupStudentSelected(e) {

  }

  protected setInitialValue() {
    this.filteredGroupStudents
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGroupStudent.compareWith = (a: GroupStudentOption, b: GroupStudentOption) => a && b && a.id === b.id;
      });
  }

  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }

}

import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { CLSModules, CLSPermissions, DivisionOption } from 'cls-k12-sdk-js/src';
import { UserIdentity } from 'cls-k12-sdk-js/src/core/identity/models/user-identity-model';
import { UserEndpoint, DivisionEndpoint, SchoolEndpoint, GradeEndpoint } from 'cls-k12-sdk-js/src';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { StudentAssign } from 'cls-k12-sdk-js/src/services/groupstudent/models/Student';
import { StudentEndpoint } from 'cls-k12-sdk-js/src';
import { StudentFilterRequests } from 'cls-k12-sdk-js/src/services/groupstudent/models/StudentPagingRequest';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ExportStudentRequest } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/requests/ExportStudentRequest';
import { TranferGroupstudentComponent } from '../../components/tranfer-groupstudent/tranfer-groupstudent.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';

const studentsPaginationDefault: PaginatorResponse<StudentAssign> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-list-students',
  templateUrl: './list-students.component.html',
  styleUrls: ['./list-students.component.scss']
})
export class ListStudentsComponent implements OnInit {

  studentEndpoint: StudentEndpoint;
  public userTypeFilterCtrl: FormControl = new FormControl();
  protected _onDestroy = new Subject < void> ();
  userIdentity: UserIdentity;
  endpointUser: UserEndpoint;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointStudent: StudentEndpoint;
  levelManageValue: number = 0;
  isShowFilter = true;

  mdivisionId: number
  mschoolId: number
  mgradeId: number
  mgroupStudentId: number
  mschoolYear: number

  public schoolYearCtrl: FormControl = new FormControl('');
  public keywordCtrl: FormControl = new FormControl('');
  baseApiUrl = 'http://localhost:65000';

  displayedColumns: string[] = ['select', 'stt', 'username', 'fullName', 'dayOfBirth', 'gender', 'function'];

  studentsPagination: PaginatorResponse<StudentAssign> = studentsPaginationDefault;
  studentsPaginationFilter: StudentFilterRequests = {
    pageNumber: 0,
    sizeNumber: 10,
    schoolYear: null,
    keyWord: "",
    getCount: true,
    groupStudentId: null,
    sortDirection: 'ASC',
    sortField: 'ALP'
  }
  studentsPaginationFilterClone;

  /** list of divisions */
  protected divisions: DivisionOption[];
  selection = new SelectionModel < StudentAssign > (true, []);
  selectedStudentIds = [];
  isLoadingResults = true;
  currentSchoolYear: number;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  hasAssignPermission: boolean;
  constructor(private _snackBar: MatSnackBar, private _authService: AuthService, public dialog: MatDialog, private route: ActivatedRoute, private configService: AppConfigService, private spinner: NgxSpinnerService, private router: Router) {
    this.studentEndpoint = new StudentEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();

    this.route.queryParams
      .subscribe(params => {
        this.studentsPaginationFilter.groupStudentId = params.id;
        this.studentsPaginationFilter.schoolYear = params.schoolYear;
        this.mschoolId = params.schoolId;
        this.mschoolYear = params.schoolYear;
        this.mgradeId = params.gradeId;
        this.mdivisionId = params.divisionId;
        this.mgroupStudentId = params.id;
      });

    this.currentSchoolYear = this.configService.getConfig().unit.currentSchoolYear;

    this.spinner.show();
    this.hasAssignPermission = this.userIdentity.hasPermission(CLSModules.Class, CLSPermissions.Assign);
  }


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit(): void {
    if (this.hasAssignPermission) {
      this.schoolYearCtrl.setValue(this.studentsPaginationFilter.schoolYear + '');
      this.getStudentsData(true);
    } else {
      this.back();
    }
  }

  failureCallback() {
    this.isLoadingResults = false;
  }


  getStudentsData(isFirstLoad?: boolean) {

    this.isLoadingResults = true;
    // lúc mới load trang thì lấy niên khóa theo schoolYear trên param
    if (!isFirstLoad) {
      this.studentsPaginationFilter.schoolYear = this.schoolYearCtrl.value;

      // cập nhật lại router của schoolYear
      const queryParams: Params = { schoolYear: this.schoolYearCtrl.value };
      this.router.navigate(
        [],
        {
          relativeTo: this.route,
          queryParams: queryParams,
          queryParamsHandling: 'merge', // remove to replace all query params by provided
        });
    }


    this.studentEndpoint.getStudentsPaging(this.studentsPaginationFilter)
      .then(res => {
        this.spinner.hide();
        this.isLoadingResults = false;
        this.studentsPagination = res;
        this.studentsPaginationFilterClone = { ...this.studentsPaginationFilter }
        res.items.forEach((item) => {
          if (new Date(item.dayOfBirth).getFullYear() == 1) {
            item.dayOfBirth = null;
          }
        })
      })
      .catch(this.failureCallback);
  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.studentsPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.studentsPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.getStudentsData();
    });
  }

  exportExcel() {
    let getStudentReportFilterRequests: ExportStudentRequest =
    {
      groupStudentId: this.mgroupStudentId,
      schoolYear: this.schoolYearCtrl.value,
      keyWord: this.keywordCtrl.value
    }

    this.studentEndpoint.exportStudentReport(getStudentReportFilterRequests)
      .then((urlFile) => {
        window.open(urlFile);
      })
  }
  tranferGroupstudent(data) {
    let userId;
    if (data == null) {
      if (this.selectedStudentIds.length > 0) {
        userId = this.selectedStudentIds;
      }
      else {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Bạn chưa chọn học sinh để chuyển lớp',
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }
    } else {
      userId = [data.userID];
    }

    if (userId) {
      const dialogRef = this.dialog.open(TranferGroupstudentComponent, {
        disableClose: true,
        width: '444px',
        height: 'auto',
        data: { schoolId: this.mschoolId, gradeId: this.mgradeId, classId: this.mgroupStudentId, schoolYear: this.schoolYearCtrl.value, userId: userId }
      });

      dialogRef.afterClosed().subscribe(result => {
        debugger
        if (result) {
          this.getStudentsData(true);
        }
      });
    }


  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.studentsPagination.items.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.selectedStudentIds = [];
    } else {
      this.studentsPagination.items.forEach(row => {
        this.selection.select(row);
        this.selectedStudentIds.push(row.userID);
      })
    }
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: StudentAssign): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.userID}`;
  }

  changeCheck(event, row) {
    if (event.checked) {
      let index = this.selectedStudentIds.indexOf(row.userID);
      if (index == -1) {
        this.selectedStudentIds.push(row.userID);
      }
    } else {
      let index = this.selectedStudentIds.indexOf(row.userID);
      this.selectedStudentIds.splice(index, 1);
    }
  }
  onSubmitFilter() {
    this.studentsPaginationFilter.schoolYear = this.schoolYearCtrl.value;
    this.studentsPaginationFilter.keyWord = this.keywordCtrl.value;
    this.studentsPaginationFilter.pageNumber = 1;
    this.paginator.pageIndex = 0;
    if (this.objHasChanged(this.studentsPaginationFilterClone, this.studentsPaginationFilter)) {
      this.getStudentsData();
    }
  }
  objHasChanged(obj, obj2) {
    return JSON.stringify(obj) != JSON.stringify(obj2);
  }
  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }

  back() {
    this.router.navigate(['/dashboard/manage-groupstudents'])
  }
}

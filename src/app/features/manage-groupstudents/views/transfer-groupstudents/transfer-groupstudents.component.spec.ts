import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferGroupstudentsComponent } from './transfer-groupstudents.component';

describe('TransferGroupstudentsComponent', () => {
  let component: TransferGroupstudentsComponent;
  let fixture: ComponentFixture<TransferGroupstudentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransferGroupstudentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferGroupstudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

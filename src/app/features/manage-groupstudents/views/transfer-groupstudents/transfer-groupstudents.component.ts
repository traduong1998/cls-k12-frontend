import { Component, Inject, OnInit, ViewChild } from '@angular/core';

import { FormControl, NgForm, Validators } from '@angular/forms';
import { GradeEndpoint, GroupStudentEndpoint, TransferGroupStudentRequest, UserEndpoint, UserIdentity } from 'sdk/cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { take, takeUntil } from 'rxjs/operators';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'app-transfer-groupstudents',
  templateUrl: './transfer-groupstudents.component.html',
  styleUrls: ['./transfer-groupstudents.component.scss']
})
export class TransferGroupstudentsComponent implements OnInit {
  baseApiUrl = 'http://localhost:65000';
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  endpointGrade: GradeEndpoint;
  endpointGroupStudent: GroupStudentEndpoint;

  gradeId: number;
  constructor(@Inject(MAT_DIALOG_DATA) public data, private _snackBar: MatSnackBar, private route: ActivatedRoute, private router: Router,) {
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
  }

  isFirstLoadGrades = true;
  isFirstLoadGroupStudents = true;

  currentGroupStudent = {
    schoolYear: 0,
    groupStudentId: 0,
    schoolId: 0,
    groupStudentName: "",
    schoolName: "",
    gradeName: ""
  }
  nextGroupStudent = {
    schoolYear: 0,
    groupStudentName: "",
    gradeName: "",
    groupStudentId: 0,
    gradeId: 0
  }

  transferGroupStudentRequest: TransferGroupStudentRequest = {
    currentGroupStudentId: 0,
    nextGroupStudentId: 0,
    schoolYearFrom: 0,
    schoolYearTo: 0
  }

  gradeFilter = {
    schoolId: null
  }
  groupStudentFilter = {
    schoolId: null,
    gradeId: null
  }
  public schoolYearCtrl: FormControl = new FormControl('2021');

  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl('', Validators.required);
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected groupStudents: GroupStudentOption[];
  public groupStudentCtrl: FormControl = new FormControl('', Validators.required);
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject<GroupStudentOption[]>(1);
  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;

  protected _onDestroy = new Subject<void>();

  ngOnInit(): void {
    Promise.resolve()
      .then(() => {
        return this.endpointGroupStudent.getGroupStudentDetail(this.data.id);
      })
      .then((res) => {
        if (res) {
          this.currentGroupStudent.schoolId = res.schoolId;
          this.currentGroupStudent.schoolName = res.schoolName;
          this.currentGroupStudent.gradeName = res.gradeName;
          this.currentGroupStudent.groupStudentName = res.groupStudentName;
        }
      })
      .then(() => {
        this.onGradeSelectClicked();
      })

    // listen for search field value changes
    this.groupStudentFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudents();
      });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected setInitialValue() {
    this.filteredGroupStudents
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGroupStudent.compareWith = (a: GroupStudentOption, b: GroupStudentOption) => a && b && a.id === b.id;
      });
  }

  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }
  selectedGrade: number = 0;
  onGradeSelectClicked() {
    var schoolIdSelected = this.currentGroupStudent.schoolId;
    this.endpointGrade.getGradeOptions(schoolIdSelected)
      .then(res => {
        this.isFirstLoadGrades = false;
        this.grades = res;
        //lấy khối tiếp theo
        res.forEach((item) => {
          if (this.data.gradeId == item.id) {
            let nextOrderNumber = item.orderNumber + 1;
            let newValue = res.find(x => x.orderNumber == nextOrderNumber)
            this.selectedGrade = newValue.id;
          }
        })
        this.filteredGrades.next(this.grades.slice());
      })
  }
  onGroupStudentSelectClicked() {
    var schoolIdSelected = this.currentGroupStudent.schoolId;
    this.endpointGroupStudent.getGroupStudentOptions(schoolIdSelected, this.selectedGrade)
      .then(res => {
        this.isFirstLoadGroupStudents = false;
        this.groupStudents = res;
        this.groupStudentFilter.schoolId = schoolIdSelected;
        this.filteredGroupStudents.next(this.groupStudents.slice());
        this.currentGroupStudent = {
          gradeName: this.grades.find(x => x.id == this.selectedGrade).name,
          schoolYear: this.data.schoolYear,
          groupStudentId: this.data.id,
          groupStudentName: '',
          schoolId: schoolIdSelected,
          schoolName: ''
        }
      });
  }
  onChangeGroupStudentSelected(groupStudent: GroupStudentOption) {
    if (groupStudent) {
      this.nextGroupStudent.groupStudentName = groupStudent.name;
      this.nextGroupStudent.groupStudentId = groupStudent.id;
    } else {
      this.nextGroupStudent.groupStudentName = '';
      this.nextGroupStudent.groupStudentId = null;
    }
  }
  submitForm() {
    this.transferGroupStudentRequest = {
      currentGroupStudentId: this.data.id,
      nextGroupStudentId: this.nextGroupStudent.groupStudentId,
      schoolYearFrom: this.data.schoolYear,
      schoolYearTo: this.schoolYearCtrl.value
    }
    if (this.transferGroupStudentRequest.schoolYearTo <= this.transferGroupStudentRequest.schoolYearFrom) {

      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Bạn chỉ chuyển lớp từ niên khóa ' + this.transferGroupStudentRequest.schoolYearTo + '-'
          + this.transferGroupStudentRequest.schoolYearTo,
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      return;
    }

    this.endpointGroupStudent.transferGroupStudentRequest(this.transferGroupStudentRequest)
      .then(res => {
        console.log("sssss", res)
        if (res != null && res.isSuccess) {
          if (res.resultCode == 1) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Chuyển lớp thành công',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });

            this.gradeCtrl.reset();
            this.gradeCtrl.markAsUntouched();
            this.groupStudentCtrl.reset();
            this.groupStudentCtrl.markAsUntouched();
          } else {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Chuyển thành công ',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        } else {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Chuyển lớp không thành công',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      })
      .catch((err: ErrorResponse) => {
        this._snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: err.errorDetail,
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      });
  }
}


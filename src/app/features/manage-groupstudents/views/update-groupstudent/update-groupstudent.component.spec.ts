import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateGroupStudentComponent } from './update-groupstudent.component';

describe('UpdateGroupStudentComponent', () => {
  let component: UpdateGroupStudentComponent;
  let fixture: ComponentFixture<UpdateGroupStudentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateGroupStudentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGroupStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

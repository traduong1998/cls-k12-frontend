import { Component, OnInit, ViewChild, Input, Inject } from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';
import { GroupStudent, FormGroupStudent } from 'cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { GroupStudentEndpoint } from 'cls-k12-sdk-js/src/services/groupstudent/endpoints/GroupStudentEndpoint';
import { CreateGroupStudentRequest } from 'cls-k12-sdk-js/src/services/groupstudent/requests/createGroupStudentRequest';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ErrorResponse } from 'cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { UserIdentity } from 'cls-k12-sdk-js/src/core/identity/models/user-identity-model';
import { SchoolEndpoint } from 'cls-k12-sdk-js/src/services/school/endpoints/SchoolEndpoint';
import { SchoolOption } from 'cls-k12-sdk-js/src/services/school/models/School';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { GradeOption } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { GradeEndpoint } from 'cls-k12-sdk-js/src/services/grade/endpoints/GradeEndpoint';
import { AuthService } from 'src/app/core/services';
import { DivisionOption } from 'cls-k12-sdk-js/src/services/division/models/Division';
import { DivisionEndpoint } from 'cls-k12-sdk-js/src';
import { takeUntil, take } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { UpdateGroupStudentRequest } from 'cls-k12-sdk-js/src/services/groupstudent/requests/updateGroupStudentRequest';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';

@Component({
  selector: 'app-update-groupstudent',
  templateUrl: './update-groupstudent.component.html',
  styleUrls: ['./update-groupstudent.component.scss']
})
export class UpdateGroupStudentComponent implements OnInit {


  groupstudentEndpoint: GroupStudentEndpoint;
  endpointDivision: DivisionEndpoint
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  baseApiUrl = 'http://localhost:65000';
  lblname = ''
  lblOrderNumber = ''
  lblschool = ''
  lblgrade = ''
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  userIdentity: UserIdentity;
  public divisionCtrl: FormControl = new FormControl();
  protected _onDestroy = new Subject<void>();

  /** list of divisions */
  protected divisions: DivisionOption[];
  selection = new SelectionModel<GroupStudent>(true, []);
  selectedGroupStudentIds = [];
  isLoadingResults = true;

  /** control for the selected division */
  public divisionFilterCtrl: FormControl = new FormControl();

  isShowFilter = true;
  isFirstLoadDivisions = true;
  isFirstLoadSchools = true;
  isFirstLoadGrades = true;

  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  protected schools: SchoolOption[];
  public schoolCtrl: FormControl = new FormControl();
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;

  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }

  constructor(private _authService: AuthService, private mdDialogRef: MatDialogRef<UpdateGroupStudentComponent>,
    @Inject(MAT_DIALOG_DATA) public groupStudentInfo: GroupStudent,
    private _snackBar: MatSnackBar) {
    this.groupstudentEndpoint = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();

    console.log(`groupStudentInfo: `, groupStudentInfo)
  }


  formGroupStudent: UpdateGroupStudentRequest = {
    groupStudentName: this.groupStudentInfo.groupStudentName,
    orderNumber: this.groupStudentInfo.orderNumber,
    groupStudentId: this.groupStudentInfo.groupStudentId,
    gradeName: this.groupStudentInfo.gradeName,
    schoolName: this.groupStudentInfo.schoolName,
    divisionName: this.groupStudentInfo.divisionName
  }

  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionCtrl.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchools || this.schoolFilter.divisionId != divisionIdSelected) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchools = false;
          this.schools = res;
          this.schoolFilter.divisionId = divisionIdSelected;
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolCtrl.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrades || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.divisions = res;
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onChangeDivisionSelected(id: any) {
    this.resetSchoolSelectCtrl();
    this.resetGradeSelectCtrl();
  }

  onChangeSchoolSelected(id: any) {
    this.resetGradeSelectCtrl();
  }
  onChangeGradeSelected(id: any) {
  }

  resetSchoolSelectCtrl() {
    this.schools = [];
    this.isFirstLoadSchools = true;
    this.schoolCtrl.setValue(undefined);
  }

  resetGradeSelectCtrl() {
    this.grades = [];
    this.isFirstLoadGrades = true;
    this.gradeCtrl.setValue(undefined);
  }

  ngOnInit(): void {
    // this.divisionCtrl.setValue(this.formGroupStudent[0]);
    // this.filteredDivisions.next(this.divisions.slice());
    // this.schoolCtrl.setValue(this.formGroupStudent.schoolId);
    // this.gradeCtrl.setValue(this.formGroupStudent.gradeId);


    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });

    // listen for search field value changes
    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });
  }

  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        // triggers initializing the selection according to the initial value of
        // the form control (i.e. _initializeSelection())
        // this needs to be done after the filteredDivisions are loaded initially
        // and after the mat-option elements are available
        this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id;
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id;
      });

    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
      });
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  resetFrom() {
    this.lblname = '';
    this.lblOrderNumber = '';
  }

  checkForm() {
    var result = true;

    if (this.formGroupStudent.groupStudentName === '') {
      this.lblname = 'Bạn chưa nhập tên Lớp!';
      result = false;
    }

    if (this.formGroupStudent.groupStudentName.length > 30) {
      this.lblname = 'Tên lớp không được vượt quá 30 ký tự!';
      result = false;
    }

    if (!this.formGroupStudent.orderNumber) {
      this.lblOrderNumber = 'Bạn chưa nhập số thứ tự!';
      result = false;
    } else if (!Number.isInteger(Number(this.formGroupStudent.orderNumber))) {
      this.lblOrderNumber = 'Số thứ tự phải là kiểu số';
      result = false;
    }

    return result;
  }

  submitForm(regForm: NgForm) {
    this.resetFrom();

    if (this.checkForm()) {
      console.log(`this.formGroupStudent: `, this.formGroupStudent);
      this.groupstudentEndpoint.updateGroupStudent(this.formGroupStudent)
        .then(res => {
          if (res) {
            this._snackBar.openFromComponent(SuccessSnackBarComponent, {
              data: 'Cập nhật lớp thành công',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          } else {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Cập nhật lớp không thành công',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        })
        .catch((err: ErrorResponse) => {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        });
      this.mdDialogRef.close('submit')

    }

  }

}

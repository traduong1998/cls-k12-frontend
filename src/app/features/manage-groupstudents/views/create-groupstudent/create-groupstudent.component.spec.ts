import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateGroupStudentComponent } from './create-groupstudent.component';

describe('CreateGroupstudentComponent', () => {
  let component: CreateGroupStudentComponent;
  let fixture: ComponentFixture<CreateGroupStudentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateGroupStudentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGroupStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

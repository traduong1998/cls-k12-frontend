import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListGroupStudentsComponent } from './list-groupstudents.component';

describe('ListGroupStudentsComponent', () => {
  let component: ListGroupStudentsComponent;
  let fixture: ComponentFixture<ListGroupStudentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListGroupStudentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListGroupStudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

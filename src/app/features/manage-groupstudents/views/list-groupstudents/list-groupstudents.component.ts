import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/core/api/responses/PaginatorResponse';
import { GroupStudent, GroupStudentOption } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/GroupStudent';
import { GroupStudentEndpoint } from 'cls-k12-sdk-js/src/services/groupstudent/endpoints/GroupStudentEndpoint';
import { GroupStudentFilterRequests } from 'cls-k12-sdk-js/src/services/groupstudent/models/GroupStudentPagingRequest';
import { CreateGroupStudentComponent } from '../create-groupstudent/create-groupstudent.component';
import { UpdateGroupStudentComponent } from '../update-groupstudent/update-groupstudent.component';
import { FormControl } from '@angular/forms';
import { takeUntil, take } from 'rxjs/operators';
import { Subject, ReplaySubject } from 'rxjs';
import { DivisionOption } from 'cls-k12-sdk-js/src/services/division/models/Division';
import { SchoolOption } from 'cls-k12-sdk-js/src/services/school/models/School';
import { GradeOption } from 'cls-k12-sdk-js/src/services/grade/models/Grade';
import { MatSelect } from '@angular/material/select';
import { UserEndpoint, DivisionEndpoint, SchoolEndpoint, GradeEndpoint, UserIdentity, CLSModules, CLSPermissions } from 'cls-k12-sdk-js/src';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { AssignTeacherDialogComponent } from '../../components/assign-teacher-dialog/assign-teacher-dialog.component';
import { GetGroupStudentInfo } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/responses/get-groupstudent-info';
import { AssignTeacherGroupStudent } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/models/assign-teacher-groupstudent';
import { SpinnerType } from 'src/app/shared/enums/spinner-type';
import { NgxSpinnerService } from 'ngx-spinner';
import { DeleteGroupStudentRequest } from 'sdk/cls-k12-sdk-js/src/services/groupstudent/requests/deleteGroupStudentRequest';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { TransferGroupstudentsComponent } from '../transfer-groupstudents/transfer-groupstudents.component';

const groupstudentsPaginationDefault: PaginatorResponse<GroupStudent> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-list-groupstudents',
  templateUrl: './list-groupstudents.component.html',
  styleUrls: ['./list-groupstudents.component.scss']
})
export class ListGroupStudentsComponent implements OnInit {

  groupstudentEndpoint: GroupStudentEndpoint;
  public userTypeFilterCtrl: FormControl = new FormControl();
  protected _onDestroy = new Subject < void> ();
  userIdentity: UserIdentity;
  endpointUser: UserEndpoint;
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointGroupStudent: GroupStudentEndpoint;
  levelManageValue: number = 0;
  /** control for the selected division */
  public divisionCtrl: FormControl = new FormControl();
  public divisionFilterCtrl: FormControl = new FormControl();

  isShowFilter = true;
  isFirstLoadUserTypes = true;
  isFirstLoadDivisions = true;
  isFirstLoadSchools = true;
  isFirstLoadGrades = true;
  isFirstLoadGroupStudents = true;

  schoolFilter = {
    divisionId: null
  }
  gradeFilter = {
    schoolId: null
  }
  groupStudentFilter = {
    schoolId: null,
    gradeId: null
  }
  dataAssignTeacherGroupStudent: AssignTeacherGroupStudent;

  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject < DivisionOption[] > (1);
  @ViewChild('singleSelectDivision', { static: true }) singleSelectDivision: MatSelect;

  protected schools: SchoolOption[];
  public schoolCtrl: FormControl = new FormControl();
  public schoolFilterCtrl: FormControl = new FormControl();
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject < SchoolOption[] > (1);
  @ViewChild('singleSelectSchool', { static: true }) singleSelectSchool: MatSelect;



  protected grades: GradeOption[];
  public gradeCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public filteredGrades: ReplaySubject<GradeOption[]> = new ReplaySubject < GradeOption[] > (1);
  @ViewChild('singleSelectGrade', { static: true }) singleSelectGrade: MatSelect;

  protected groupStudents: GroupStudentOption[];
  public groupStudentCtrl: FormControl = new FormControl();
  public groupStudentFilterCtrl: FormControl = new FormControl();
  public filteredGroupStudents: ReplaySubject<GroupStudentOption[]> = new ReplaySubject < GroupStudentOption[] > (1);
  @ViewChild('singleSelectGroupStudent', { static: true }) singleSelectGroupStudent: MatSelect;

  public schoolYearCtrl: FormControl = new FormControl('2020');
  public txtFullName: FormControl = new FormControl('');

  baseApiUrl = 'http://localhost:65000';

  displayedColumns: string[] = ['stt', 'groupStudentName', 'teacherName', 'schoolName', 'gradeName', 'orderNumber', 'function'];

  groupstudentsPagination: PaginatorResponse<GroupStudent> = groupstudentsPaginationDefault;
  groupstudentsPaginationFilter: GroupStudentFilterRequests = {
    pageNumber: 1,
    sizeNumber: 10,
    divisionId: null,
    schoolYear: null,
    getCount: true,
    gradeId: null,
    groupStudentId: null,
    schoolId: null,
    sortDirection: 'DESC',
    sortField: 'CRE'
  }

  /** list of divisions */
  protected divisions: DivisionOption[];
  selection = new SelectionModel < GroupStudent > (true, []);
  selectedGroupStudentIds = [];
  isLoadingResults = true;
  hasAddPermission: boolean;
  hasDeletePermission: boolean;
  hasEditPermission: boolean;
  hasAssignPermission: boolean;
  currentSchoolYear: number;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  groupStudentFilterClone;
  constructor(private _snackBar: MatSnackBar, private _authService: AuthService, public dialog: MatDialog, private _router: Router, private spinner: NgxSpinnerService, private configService: AppConfigService) {
    this.groupstudentEndpoint = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointUser = new UserEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGroupStudent = new GroupStudentEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();

    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.Class, CLSPermissions.Add);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.Class, CLSPermissions.Edit);
    this.hasDeletePermission = this.userIdentity.hasPermission(CLSModules.Class, CLSPermissions.Delete);
    this.hasAssignPermission = this.userIdentity.hasPermission(CLSModules.Class, CLSPermissions.Assign);

    if (this.userIdentity.levelManage == "DPM") {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == "DVS") {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.schoolFilter.divisionId = this.userIdentity.divisionId;
    } else if (this.userIdentity.levelManage == "SCH") {
      if (this.userIdentity.divisionId != null) {
        this.schoolFilter.divisionId = this.userIdentity.divisionId;
      }
      this.levelManageValue = 3;
      // Tài khoản cấp Trường, schoolid fillter mặc định theo id trường của tài khoản hiện tại
      this.gradeFilter.schoolId = this.userIdentity.schoolId;
      this.groupStudentFilter.schoolId = this.userIdentity.schoolId;
      this.groupstudentsPaginationFilter.schoolId = this.userIdentity.schoolId;
    }
    this.currentSchoolYear = this.configService.getConfig().unit.currentSchoolYear;
    this.spinner.show();
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  ngOnInit(): void {
    this.getGroupStudentsData();

    // listen for search field value changes
    this.divisionFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterDivisions();
      });

    // listen for search field value changes
    this.schoolFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterSchools();
      });

    // listen for search field value changes
    this.gradeFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGrades();
      });

    // listen for search field value changes
    this.groupStudentFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroupStudents();
      });
    this.schoolYearCtrl.setValue(this.currentSchoolYear + '');
  }

  failureCallback() {
    this.isLoadingResults = false;
    alert("hihi 400");
  }

  protected filterDivisions() {
    if (!this.divisions) {
      return;
    }
    // get the search keyword
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterSchools() {
    if (!this.schools) {
      return;
    }
    // get the search keyword
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterGrades() {
    if (!this.grades) {
      return;
    }
    // get the search keyword
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrades.next(this.grades.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGrades.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }
  protected filterGroupStudents() {
    if (!this.groupStudents) {
      return;
    }
    // get the search keyword
    let search = this.groupStudentFilterCtrl.value;
    if (!search) {
      this.filteredGroupStudents.next(this.groupStudents.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the divisions
    this.filteredGroupStudents.next(
      this.groupStudents.filter(groupStudent => groupStudent.name.toLowerCase().indexOf(search) > -1)
    );
  }

  getGroupStudentsData() {

    if (this.levelManageValue == 2) {
      this.groupstudentsPaginationFilter.divisionId = this.schoolFilter.divisionId;
    }
    if (this.levelManageValue == 3) {
      if (this.schoolFilter.divisionId != null) {
        this.groupstudentsPaginationFilter.divisionId = this.schoolFilter.divisionId;
      }
      this.groupstudentsPaginationFilter.schoolId = this.gradeFilter.schoolId;
    }

    this.isLoadingResults = true;
    this.groupstudentEndpoint.getGroupStudentsPaging(this.groupstudentsPaginationFilter)
      .then(res => {
        this.spinner.hide();
        this.isLoadingResults = false;
        this.groupstudentsPagination = res;
        this.groupStudentFilterClone = { ...this.groupstudentsPaginationFilter };
      })
      .catch(this.failureCallback);
  }

  addGroupstudentFromFile() {

  }
  //constructor() { }
  openDialog() {
    const dialogRef = this.dialog.open(CreateGroupStudentComponent, {
      disableClose: true,
      minWidth: '491px',
      maxWidth: '100%',
      height: 'auto',
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getGroupStudentsData()
        }
      }
    });
  }

  openDialogAssignTeacher(groupStudent: GetGroupStudentInfo) {
    this.selection.clear();
    this.dataAssignTeacherGroupStudent = {
      groupStudentId: groupStudent.groupStudentId,
      schoolId: groupStudent.schoolId,
      schoolYear: this.schoolYearCtrl.value
    }
    const dialogRef = this.dialog.open(AssignTeacherDialogComponent, {
      disableClose: true,
      width: '491px',
      height: 'auto',
      data: this.dataAssignTeacherGroupStudent
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          this.getGroupStudentsData();
        }
      }
    });
  }

  ngAfterViewInit() {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.groupstudentsPaginationFilter.pageNumber = this.paginator.pageIndex + 1;
      this.groupstudentsPaginationFilter.sizeNumber = this.paginator.pageSize;
      this.getGroupStudentsData();
    });
    this.setInitialValue();
  }

  protected setInitialValue() {
    this.filteredDivisions
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        // triggers initializing the selection according to the initial value of
        // the form control (i.e. _initializeSelection())
        // this needs to be done after the filteredDivisions are loaded initially
        // and after the mat-option elements are available
        this.singleSelectDivision.compareWith = (a: DivisionOption, b: DivisionOption) => a && b && a.id === b.id;
      });

    this.filteredSchools
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectSchool.compareWith = (a: SchoolOption, b: SchoolOption) => a && b && a.id === b.id;
      });

    this.filteredGrades
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGrade.compareWith = (a: GradeOption, b: GradeOption) => a && b && a.id === b.id;
      });

    this.filteredGroupStudents
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelectGroupStudent.compareWith = (a: GroupStudentOption, b: GroupStudentOption) => a && b && a.id === b.id;
      });
  }

  transferStudent(schoolId, groupStudentId, gradeId) {
    Promise.resolve()
      .then(() => {
        return this.endpointGrade.getGradeOptions(schoolId);
      }).then(res => {
        let currentGrade;
        let maxOder = 1;;
        res.forEach((item) => {
          if (item.orderNumber > maxOder) {
            maxOder = item.orderNumber;
          }
          if (gradeId == item.id) {
            currentGrade = item.orderNumber;
          }
        })
        if (currentGrade == maxOder) {
          //showw thông báo
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Đây là khối cuối cùng, không thể chuyển lên lớp',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
        else {

          const dialogRef = this.dialog.open(TransferGroupstudentsComponent, {
            disableClose: true,
            width: '550px',
            height: 'auto',
            data: { id: groupStudentId, schoolYear: this.schoolYearCtrl.value, gradeId: gradeId }
          });

          dialogRef.afterClosed().subscribe(result => {
            if (result !== undefined) {

            }
          });
        }
      })
  }

  /**Show dialog Update GroupStudent */
  UpdateGroupStudent(data) {
    //show dialog
    this.selection.clear();
    const dialogRef = this.dialog.open(UpdateGroupStudentComponent, {
      disableClose: true,
      width: '550px',
      height: 'auto',
      data: data
      // data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        if (result === 'submit') {
          /**Nếu là submit sẽ load lại data */
          this.getGroupStudentsData()
        }
      }
    });
  }

  ListStudent(data) {
    data.schoolYearCtrl = this.schoolYearCtrl.value
    this._router.navigate(['/dashboard/manage-groupstudents/list-students'], data);
  }

  /**Show dialog Update GroupStudent */
  DeleteGroupStudent(data) {
    //show dialog
    this.selection.clear();

    this.groupstudentEndpoint.countStudentOfGroupStudent(data.groupStudentId)
      .then(res => {
        if (res == 0) {
          //show dialog
          const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
            disableClose: true,
            width: '444px',
            height: 'auto',
            //data: data
            data: {
              title: 'Xóa lớp học',
              name: data.groupStudentName,
              message: ''
            }
          });

          dialogRef.afterClosed().subscribe(result => {
            if (result == 'DONGY') {
              let model = new DeleteGroupStudentRequest();
              model.id = data.groupStudentId;
              this.groupstudentEndpoint.deleteGroupStudent(model)
                .then(res => {
                  if (res) {
                    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                      data: 'Xóa lớp học thành công',
                      duration: 3000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                    });
                    this.getGroupStudentsData();
                  } else {
                    this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                      data: 'Xóa lớp học không thành công',
                      duration: 3000,
                      horizontalPosition: this.horizontalPosition,
                      verticalPosition: this.verticalPosition,
                    });
                  }
                })
                .catch((err: ErrorResponse) => {

                  this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                    data: err.errorDetail,
                    duration: 3000,
                    horizontalPosition: this.horizontalPosition,
                    verticalPosition: this.verticalPosition,
                  });
                });
            }
            else {
              if (result != undefined) {
                this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                  data: 'Bạn nhập chưa đúng, vui lòng thử lại',
                  duration: 3000,
                  horizontalPosition: this.horizontalPosition,
                  verticalPosition: this.verticalPosition,
                });
              }

            }
          });
        } else {
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Lớp học đang có học sinh. Không được phép xóa!',
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
        }
      })
  }

  public addGroupStudentFromFile() {
    this._router.navigate(['/dashboard/manage-groupstudents/create-from-file']);
    console.log("🚀 ~ file: box-filter-bottom.component.ts ~ line 46 ~ BoxFilterBottomComponent ~ addLesson ~ _router", this._router)

  }

  onDivisionSelectClicked() {
    if (this.isFirstLoadDivisions) {
      this.endpointDivision.getDivisionOptions()
        .then(res => {
          this.isFirstLoadDivisions = false;
          this.divisions = res;
          if (this.divisions && !this.divisions.find(x => x.id == 0)) {
            this.divisions.unshift({ id: null, name: 'Chọn phòng' })
          }
          this.filteredDivisions.next(this.divisions.slice());
        })
        .catch(

        )
    } else {

    }
  }


  onSchoolSelectClicked() {
    var divisionIdSelected = this.divisionCtrl.value;
    if (this.userIdentity.levelManage == "DVS") {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    console.log(divisionIdSelected);
    if (this.isFirstLoadSchools || this.schoolFilter.divisionId != divisionIdSelected) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected)
        .then(res => {
          this.isFirstLoadSchools = false;
          this.schools = res;
          if (this.schools && !this.schools.find(x => x.id == 0)) {
            this.schools.unshift({ id: null, name: 'Chọn trường' })
          }
          this.schoolFilter.divisionId = divisionIdSelected;
          this.filteredSchools.next(this.schools.slice());
        })
        .catch(

        )
    } else {

    }
  }
  onGradeSelectClicked() {
    var schoolIdSelected = this.schoolCtrl.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrades || this.gradeFilter.schoolId != schoolIdSelected) {
      this.endpointGrade.getGradeOptions(schoolIdSelected)
        .then(res => {
          this.isFirstLoadGrades = false;
          this.grades = res;
          if (this.grades && !this.grades.find(x => x.id == 0)) {
            this.grades.unshift({ id: null, name: 'Chọn khối' })
          }
          this.gradeFilter.schoolId = schoolIdSelected;
          this.filteredGrades.next(this.grades.slice());
        })
        .catch(

        )
    } else {

    }
  }

  onChangeDivisionSelected(id: any) {
    this.resetSchoolSelectCtrl();
    this.resetGradeSelectCtrl();
    this.resetGroupStudentSelectCtrl();
  }
  onChangeSchoolSelected(id: any) {
    this.resetGradeSelectCtrl();
    this.resetGroupStudentSelectCtrl();
  }
  onChangeGradeSelected(id: any) {
    this.resetGroupStudentSelectCtrl();
  }

  resetSchoolSelectCtrl() {
    this.schools = [];
    this.isFirstLoadSchools = true;
    this.schoolCtrl.setValue(undefined);
  }
  resetGradeSelectCtrl() {
    this.grades = [];
    this.isFirstLoadGrades = true;
    this.gradeCtrl.setValue(undefined);
  }
  resetGroupStudentSelectCtrl() {
    this.groupStudents = [];
    this.isFirstLoadGroupStudents = true;
    this.groupStudentCtrl.setValue(undefined);
  }

  onSubmitFilter() {
    this.getFilterParam();
    debugger;
    if (this.objHasChanged(this.groupStudentFilterClone, this.groupstudentsPaginationFilter)) {
      this.getGroupStudentsData();
    }
  }
  objHasChanged(obj, obj2) {
    return JSON.stringify(obj) != JSON.stringify(obj2);
  }
  getFilterParam() {
    this.groupstudentsPaginationFilter.schoolYear = this.schoolYearCtrl.value;
    this.groupstudentsPaginationFilter.divisionId = this.divisionCtrl.value;
    this.groupstudentsPaginationFilter.schoolId = this.schoolCtrl.value;
    this.groupstudentsPaginationFilter.gradeId = this.gradeCtrl.value;
    this.groupstudentsPaginationFilter.groupStudentId = this.groupStudentCtrl.value;
    this.groupstudentsPaginationFilter.pageNumber = 1;
    this.paginator.pageIndex = 0;

    console.log(this.groupstudentsPaginationFilter);
  }

  onClickToggleShowFilter() {
    this.isShowFilter = !this.isShowFilter;
  }
}

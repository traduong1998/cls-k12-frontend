import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageGroupstudentsComponent } from './manage-groupstudents.component';

describe('ManageGroupstudentsComponent', () => {
  let component: ManageGroupstudentsComponent;
  let fixture: ComponentFixture<ManageGroupstudentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageGroupstudentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageGroupstudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

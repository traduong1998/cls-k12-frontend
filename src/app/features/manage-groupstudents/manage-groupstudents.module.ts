import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageGroupstudentsRoutingModule } from './manage-groupstudents-routing.module';
import { ManageGroupstudentsComponent } from './manage-groupstudents.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { UpdateGroupStudentComponent } from './views/update-groupstudent/update-groupstudent.component';
import { CreateGroupStudentComponent } from './views/create-groupstudent/create-groupstudent.component';
import { ListGroupStudentsComponent } from './views/list-groupstudents/list-groupstudents.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { FormsModule } from '@angular/forms';
import { ListStudentsComponent } from './views/list-students/list-students.component';

import { TransferGroupstudentsComponent } from './views/transfer-groupstudents/transfer-groupstudents.component';
import { AssignTeacherDialogComponent } from './components/assign-teacher-dialog/assign-teacher-dialog.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TranferGroupstudentComponent } from './components/tranfer-groupstudent/tranfer-groupstudent.component';



@NgModule({
  declarations: [
    ManageGroupstudentsComponent,
    ListGroupStudentsComponent,
    CreateGroupStudentComponent,
    UpdateGroupStudentComponent,
    ListStudentsComponent,
    TransferGroupstudentsComponent,
    AssignTeacherDialogComponent,
    TranferGroupstudentComponent],
  imports: [
    SharedModule,
    ManageGroupstudentsRoutingModule,
    NgxMatSelectSearchModule,
    FormsModule,
    NgxSpinnerModule
  ]
})
export class ManageGroupstudentsModule { }

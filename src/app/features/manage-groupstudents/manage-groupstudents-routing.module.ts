import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards';
import { ManageGroupstudentsComponent } from './manage-groupstudents.component';
import { UpdateGroupStudentComponent } from './views/update-groupstudent/update-groupstudent.component';
import { CreateGroupStudentComponent } from './views/create-groupstudent/create-groupstudent.component';
import { ListGroupStudentsComponent } from './views/list-groupstudents/list-groupstudents.component';
import { ListStudentsComponent } from './views/list-students/list-students.component';
import { TransferGroupstudentsComponent } from './views/transfer-groupstudents/transfer-groupstudents.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: ManageGroupstudentsComponent,
    children: [
      {
        path: '',
        component: ListGroupStudentsComponent,
        data: {
          title: 'Danh sách lớp học',
        }
      },
      {
        path: 'create',
        component: CreateGroupStudentComponent,
        data: {
          title: 'Thêm mới lớp học',
        }
      },
      {
        path: 'update',
        component: UpdateGroupStudentComponent,
        data: {
          title: 'Chỉnh sửa lớp học',
        }
      },
      {
        path: 'list-students',
        component: ListStudentsComponent,
        data: {
          title: 'Danh sách học sinh',
        }
      },
      {
        path: 'transfer-groupstudents',
        component: TransferGroupstudentsComponent,
        data: {
          title: 'Chuyển học sinh lên lớp',
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageGroupstudentsRoutingModule { }

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { DataRequestCreateQuestion } from '../../../../../sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/requests/data-request-create-question';
import { ListQuestionBankResponse } from '../../../../../sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/responses/question-bank-response';

@Injectable({
  providedIn: 'root'
})
export class ListQuestionService {
  // emit data from filter
  emitDataFilter = new Subject();
  dataFilter$ = this.emitDataFilter.asObservable();

  emitDataFilterCreate = new Subject();
  dataFilterCreate$ = this.emitDataFilterCreate.asObservable();

  emitDisableInputFilter = new Subject();
  isDableInputFilter$ = this.emitDisableInputFilter.asObservable();

  emitDataQuestionInfoImport = new Subject();
  dataQuestionInfoImport$ = this.emitDataQuestionInfoImport.asObservable();
  constructor() { }

  public beauty(questions: DataRequestCreateQuestion[]): DataRequestCreateQuestion[] {

    let beautyQuestions = [...questions];
    beautyQuestions.forEach((qst) => {
      //Remove trong nội dung câu hỏi
      qst.content = qst.content.replace(/text-indent:36pt;|&#xa0;|\u0000|<(html|head|\/head|body|\/body|\/html|\?xml|\!DOCTYPE|meta|title|\/title)[^>]{0,}>|<(span)[^>]{0,}>&#xa0;<\/span>/g, "");
      //Remove trong nội dung đáp án
      if (qst.answers != null) {
        qst.answers.forEach((answer) => {
          answer.content = answer.content.replace(/text-indent:36pt;|&#xa0;|\u0000|<(html|head|\/head|body|\/body|\/html|\?xml|\!DOCTYPE|meta|title|\/title)[^>]{0,}>|<(span)[^>]{0,}>&#xa0;<\/span>/g, "");
        });
      }
      //Câu chùm
      if (qst.questions != null) {
        qst.questions.forEach((child) => {
          //Remove trong nội dung câu con
          child.content = child.content.replace(/text-indent:36pt;|&#xa0;|\u0000|<(html|head|\/head|body|\/body|\/html|\?xml|\!DOCTYPE|meta|title|\/title)[^>]{0,}>|<(span)[^>]{0,}>&#xa0;<\/span>/g, "");

          //Remove trong đáp án câu con
          child.answers.forEach((answerChild) => {
            answerChild.content = answerChild.content.replace(/text-indent:36pt;|&#xa0;|\u0000|<(html|head|\/head|body|\/body|\/html|\?xml|\!DOCTYPE|meta|title|\/title)[^>]{0,}>|<(span)[^>]{0,}>&#xa0;<\/span>/g, "");
          });
        });
      }
    })
    return beautyQuestions;
  }

  public beautyAnswer(questions: ListQuestionBankResponse): ListQuestionBankResponse {
    if (questions.content) {
      questions.content = questions.content.replace(/text-indent:36pt;|&#xa0;|\u0000|<(html|head|\/head|body|\/body|\/html|\?xml|\!DOCTYPE|meta|title|\/title)[^>]{0,}>|<(span)[^>]{0,}>&#xa0;<\/span>/g, "");
    }
    if (questions.answers != null) {
      questions.answers.forEach((answer) => {
        answer.content = answer.content.replace(/text-indent:36pt;|&#xa0;|\u0000|<(html|head|\/head|body|\/body|\/html|\?xml|\!DOCTYPE|meta|title|\/title)[^>]{0,}>|<(span)[^>]{0,}>&#xa0;<\/span>/g, "");
      });
    }
    return questions;
  }

  // download file word
  downloadFileWord(idDocument: string) {
    var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' " +
      "xmlns:w='urn:schemas-microsoft-com:office:word' " +
      "xmlns='http://www.w3.org/1998/Math/MathML' " +
      "<?xml version='1.0'?>" +
      "xmlns='http://www.w3.org/TR/REC-html40'>" +
      "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var footer = "</body></html>";
    var test = document.getElementById(idDocument).innerHTML;
    var sourceHTML = header + document.getElementById(idDocument).innerHTML + footer;
    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'danhsachcauhoi.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
  }

}

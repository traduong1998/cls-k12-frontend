import { TestBed } from '@angular/core/testing';

import { ListQuestionService } from './list-question.service';

describe('ListQuestionService', () => {
  let service: ListQuestionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListQuestionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

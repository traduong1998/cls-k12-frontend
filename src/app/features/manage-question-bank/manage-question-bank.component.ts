import { ListQuestionService } from './services/list-question.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services';
import { CLSModules, CLSPermissions, UserIdentity } from 'sdk/cls-k12-sdk-js/src';

@Component({
  selector: 'app-manage-question-bank',
  templateUrl: './manage-question-bank.component.html',
  styleUrls: ['./manage-question-bank.component.scss']
})
export class ManageQuestionBankComponent implements OnInit {
  userIdentity: UserIdentity;
  hasAddPermission: boolean;
  hasDeletePermission: boolean;
  hasEditPermission: boolean;
  hasApprovePermission: boolean;

  constructor(private router: Router, private questionService: ListQuestionService, private _authService: AuthService) {
    this.userIdentity = _authService.getTokenInfo();
    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.QuestionBank, CLSPermissions.Add);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.QuestionBank, CLSPermissions.Edit);
    this.hasDeletePermission = this.userIdentity.hasPermission(CLSModules.QuestionBank, CLSPermissions.Delete);
    this.hasApprovePermission = this.userIdentity.hasPermission(CLSModules.QuestionBank, CLSPermissions.Approved);
  }
  ngOnInit(): void {

  }

}

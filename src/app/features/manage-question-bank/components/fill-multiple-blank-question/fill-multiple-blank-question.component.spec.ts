import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillMultipleBlankQuestionComponent } from './fill-multiple-blank-question.component';

describe('FillMultipleBlankQuestionComponent', () => {
  let component: FillMultipleBlankQuestionComponent;
  let fixture: ComponentFixture<FillMultipleBlankQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillMultipleBlankQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillMultipleBlankQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

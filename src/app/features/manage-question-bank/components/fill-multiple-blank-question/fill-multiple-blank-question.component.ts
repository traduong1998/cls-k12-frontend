import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { AnswerOfUser } from 'src/app/shared/modules/userexam/UserAnswer';
import { ListQuestionBankResponse } from 'sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/responses/question-bank-response';

@Component({
  selector: 'app-fill-multiple-blank-question',
  templateUrl: './fill-multiple-blank-question.component.html',
  styleUrls: ['./fill-multiple-blank-question.component.scss']
})
export class FillMultipleBlankQuestionComponent implements OnInit, OnChanges {
  @Input() dataQuestion: ListQuestionBankResponse;
  @Input() userAnswer;
  @Input() isResult;
  @Input() isPublicAnswer: boolean;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  dataAnswers;
  isFlag = false;
  isTrueQuestion;
  answerOfStudent: number[] = [];
  AnswerOfUser: AnswerOfUser[] = [];
  constructor() { }

  ngOnInit(): void {
    this.dataAnswers = this.dataQuestion.answers.reduce((r, a) => {
      r[a.groupOfFillBlank2] = [...r[a.groupOfFillBlank2] || [], a];
      return r;
    }, {});
    this.dataAnswers = Object.values(this.dataAnswers);
  }

  ngOnChanges(changes: SimpleChanges): void {
  }

  isMark() { }

  SortedAnswers() {
    return this.dataQuestion.answers.sort((a, b) => a.groupOfFillBlank2 > b.groupOfFillBlank2 ? 1 : a.groupOfFillBlank2 < b.groupOfFillBlank2 ? -1 : 0);
  }

  groupAnswers() {
    return [...new Set(this.SortedAnswers().map(item => item.groupOfFillBlank2))]
  }

  answersOfGroup(index) { }

  changeSelectAnswer(index) { }

  updateFillBlankQuestion() { }

  isAnswerTrue() { }

}

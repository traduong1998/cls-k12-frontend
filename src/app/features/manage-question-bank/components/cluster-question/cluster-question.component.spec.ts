import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClusterQuestionComponent } from './cluster-question.component';

describe('ClusterQuestionComponent', () => {
  let component: ClusterQuestionComponent;
  let fixture: ComponentFixture<ClusterQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClusterQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClusterQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

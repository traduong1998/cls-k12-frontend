import { ListQuestionBankResponse } from 'sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/responses/question-bank-response';
import { Component, OnInit, Input } from '@angular/core';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { AnswerOfUser } from 'src/app/shared/modules/userexam/UserAnswer';

@Component({
  selector: 'app-cluster-question',
  templateUrl: './cluster-question.component.html',
  styleUrls: ['./cluster-question.component.scss']
})
export class ClusterQuestionComponent implements OnInit {
  @Input() dataQuestion: ListQuestionBankResponse;
  @Input() isResult;
  @Input() order: number;
  @Input() isPublicAnswer: boolean;
  userAnswer = [];
  type = Type;
  isFlag = false;
  isGroup = FormatQuestion.group;
  AnswerOfUser: AnswerOfUser[] = [];
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { ListQuestionBankResponse } from 'sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/responses/question-bank-response';
import { ListQuestionService } from '../../services/list-question.service';

@Component({
  selector: 'app-multi-choice-question',
  templateUrl: './multi-choice-question.component.html',
  styleUrls: ['./multi-choice-question.component.scss']
})
export class MultiChoiceQuestionComponent implements OnInit {
  @Input() dataQuestion: ListQuestionBankResponse;
  @Input() isTypeNull:boolean;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  constructor(
    private questionService: ListQuestionService
  ) { }

  ngOnInit(): void {
    // format text
    this.questionService.beautyAnswer(this.dataQuestion);
  }


}

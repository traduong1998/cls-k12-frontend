import { Component, OnInit, Input } from '@angular/core';
import { ListQuestionService } from '../../services/list-question.service';

@Component({
  selector: 'app-status-question',
  templateUrl: './status-question.component.html',
  styleUrls: ['./status-question.component.scss']
})
export class StatusQuestionComponent implements OnInit {
  @Input() data;
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  constructor(
    private questionService: ListQuestionService
  ) { }

  ngOnInit(): void {
    this.data = this.questionService.beautyAnswer(this.data);
  }
}
import { Component, OnInit, Input } from '@angular/core';
import { ListQuestionBankResponse } from 'sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/responses/question-bank-response';
import { ListQuestionService } from '../../services/list-question.service';

@Component({
  selector: 'app-matching-question',
  templateUrl: './matching-question.component.html',
  styleUrls: ['./matching-question.component.scss']
})
export class MatchingQuestionComponent implements OnInit {
  @Input() dataQuestion: ListQuestionBankResponse;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  answers;
  defaultMath = `http://www.w3.org/1998/Math/MathML`;

  constructor(
    private questionService: ListQuestionService
  ) { }

  ngOnInit(): void {
    // format text
    this.questionService.beautyAnswer(this.dataQuestion)
    if (this.dataQuestion.answers) {
      this.dataQuestion.answers.forEach(it=>{it.content.replace(/<[^>]*>/g, "");})
      this.answers = this.dataQuestion.answers.reduce((r, a) => {
        r[a.trueAnswer] = [...r[a.trueAnswer] || [], a];
        return r;
      }, {});
      this.answers = Object.values(this.answers);
    }
  }
}

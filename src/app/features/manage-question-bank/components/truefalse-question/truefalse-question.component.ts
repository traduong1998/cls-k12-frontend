import { ListQuestionBankResponse } from 'sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/responses/question-bank-response';
import { Component, OnInit, Input } from '@angular/core';
import { ListQuestionService } from '../../services/list-question.service';

@Component({
  selector: 'app-truefalse-question',
  templateUrl: './truefalse-question.component.html',
  styleUrls: ['./truefalse-question.component.scss']
})
export class TruefalseQuestionComponent implements OnInit {
  @Input() dataQuestion: ListQuestionBankResponse;
  selectedAnswer: number;
  @Input() isTypeNull:boolean;
  defaultMath = `http://www.w3.org/1998/Math/MathML`;

  constructor(
    private questionService: ListQuestionService
  ) { }

  ngOnInit(): void {
    // format text
    this.questionService.beautyAnswer(this.dataQuestion);
  }

  changeSelectAnswer(e, id) {}

}

import { Component, OnInit, HostListener, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { QuestionTest } from 'sdk/cls-k12-sdk-js/src/services/question/models/question-test';

@Component({
  selector: 'app-display-question',
  templateUrl: './display-question.component.html',
  styleUrls: ['./display-question.component.scss']
})
export class DisplayQuestionComponent implements OnInit, OnChanges {
  @Input() questions;
  screenWidth;
  userAnswer = [];
  questionsResult: QuestionTest[] = [];


  get questionType(): typeof Type {
    return Type;
  }
  get questionFormat(): typeof FormatQuestion {
    return FormatQuestion;
  }
  constructor() {
    this.onResize();
  }
  ngOnChanges(changes: SimpleChanges): void {
    // console.log("display", this.questions);
  }

  ngOnInit(): void {
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenWidth = window.innerWidth;
  }

  getQuestionBaseById(questionId: number) {
    let qst = this.questions.find(x => x.id == questionId);
    let baseQuestion = this.questionsResult.find(x => x.id == questionId);
    console.log(baseQuestion)
    // baseQuestion.order = qst.order;
    // return baseQuestion;
  }

  userAnswerById(questionId) {
    let answerQuestion = this.userAnswer.find(x => x.questionId == questionId);
    if (answerQuestion) {
      if (answerQuestion.userAnswer) {
        return answerQuestion.userAnswer;
      } else {
        return answerQuestion
      }
    } else return [];
  }

}

import { ListQuestionBankResponse } from 'sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/responses/question-bank-response';
import { Component, Input, OnInit} from '@angular/core';
import { ListQuestionService } from '../../services/list-question.service';

@Component({
  selector: 'app-essay-question',
  templateUrl: './essay-question.component.html',
  styleUrls: ['./essay-question.component.scss']
})
export class EssayQuestionComponent implements OnInit {
  @Input() dataQuestion:ListQuestionBankResponse;

  constructor(
    private questionService: ListQuestionService
   ) {}

  ngOnInit(){
    // format text
    this.questionService.beautyAnswer(this.dataQuestion);
  }
}

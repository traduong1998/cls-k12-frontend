import { Component, OnInit, Input } from '@angular/core';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { ListQuestionBankResponse } from 'sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/responses/question-bank-response';
import { ListQuestionService } from '../../services/list-question.service';

@Component({
  selector: 'app-underline-question',
  templateUrl: './underline-question.component.html',
  styleUrls: ['./underline-question.component.scss']
})
export class UnderlineQuestionComponent implements OnInit {
  @Input() dataQuestion: ListQuestionBankResponse;
  @Input() isTypeNull:boolean;
  @Input() isResult;
  @Input() isGroup;
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  constructor(private questionService: ListQuestionService) { }

  ngOnInit(): void {
   // format text
   this.questionService.beautyAnswer(this.dataQuestion);
  }
}

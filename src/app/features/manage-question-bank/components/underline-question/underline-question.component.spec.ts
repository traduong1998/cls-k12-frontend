import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnderlineQuestionComponent } from './underline-question.component';

describe('UnderlineQuestionComponent', () => {
  let component: UnderlineQuestionComponent;
  let fixture: ComponentFixture<UnderlineQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnderlineQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnderlineQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

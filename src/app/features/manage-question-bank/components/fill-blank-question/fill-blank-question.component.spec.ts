import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillBlankQuestionComponent } from './fill-blank-question.component';

describe('FillBlankQuestionComponent', () => {
  let component: FillBlankQuestionComponent;
  let fixture: ComponentFixture<FillBlankQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillBlankQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillBlankQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

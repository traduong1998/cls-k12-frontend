import { Component, OnInit, Input } from '@angular/core';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { ListQuestionBankResponse } from 'sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/responses/question-bank-response';
import { ListQuestionService } from '../../services/list-question.service';

@Component({
  selector: 'app-fill-blank-question',
  templateUrl: './fill-blank-question.component.html',
  styleUrls: ['./fill-blank-question.component.scss']
})
export class FillBlankQuestionComponent implements OnInit {
  @Input() dataQuestion: ListQuestionBankResponse;
  @Input() isTypeNull:boolean;
  @Input() userAnswer;
  @Input() isResult;
  @Input() parentId;
  @Input() isPublicAnswer:boolean;
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  @Input() isGroup;
  format = FormatQuestion;
  constructor(
    private questionService: ListQuestionService
  ) { }

  ngOnInit(): void {
     // format text
    this.questionService.beautyAnswer(this.dataQuestion);
  }

}

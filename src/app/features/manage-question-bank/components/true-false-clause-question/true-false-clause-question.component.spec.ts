import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrueFalseClauseQuestionComponent } from './true-false-clause-question.component';

describe('TrueFalseClauseQuestionComponent', () => {
  let component: TrueFalseClauseQuestionComponent;
  let fixture: ComponentFixture<TrueFalseClauseQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrueFalseClauseQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrueFalseClauseQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

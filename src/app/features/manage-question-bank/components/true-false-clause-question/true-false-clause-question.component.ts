import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { ListQuestionBankResponse } from 'sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/responses/question-bank-response';
import { ListQuestionService } from '../../services/list-question.service';

@Component({
  selector: 'app-true-false-clause-question',
  templateUrl: './true-false-clause-question.component.html',
  styleUrls: ['./true-false-clause-question.component.scss']
})
export class TrueFalseClauseQuestionComponent implements OnInit, OnChanges {
  defaultMath = `http://www.w3.org/1998/Math/MathML`;
  @Input() dataQuestion: ListQuestionBankResponse;
  isReady: boolean = false;
  @Input() isTypeNull:boolean;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  constructor(
    private questionService: ListQuestionService
  ) { }

  ngOnInit(): void {
    // format text
    this.questionService.beautyAnswer(this.dataQuestion)
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.isReady = true;
  }

}

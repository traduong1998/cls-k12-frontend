import { ListQuestionService } from './../../services/list-question.service';
import { Level } from '../../intefaces/level';
import { CurriculumEndpoint } from 'sdk/cls-k12-sdk-js/src/services/curriculum/endpoint/curriculum-endpoint';
import { SubjectsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/subjectsEndpoint';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { DivisionEndpoint, SchoolEndpoint, GradeEndpoint, UserIdentity, LevelManages, QuestionBankEndpoint } from 'cls-k12-sdk-js/src';
import { FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild, AfterViewInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { DivisionOption } from 'sdk/cls-k12-sdk-js/src/services/division/models/Division';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { Subject, ReplaySubject } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { SubjectFilter } from '../../intefaces/subject';
import { pairwise, startWith, takeUntil } from 'rxjs/operators';
import { CurriculumsFilterRequests } from 'sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum-filter-request';
import { Curriculum } from 'sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum';
import { Chapter } from '../../intefaces/chapter';
import { Exerciser } from '../../intefaces/exerciser';
import { QuestionType } from '../../intefaces/question-type';
import { AuthService } from 'src/app/core/services';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';

@Component({
  selector: 'app-filter-question',
  templateUrl: './filter-question.component.html',
  styleUrls: ['./filter-question.component.scss']
})
export class FilterQuestionComponent implements OnInit, AfterViewInit, OnChanges {
  // init form
  filterQuestionForm = this.fb.group({
    DivisionId: new FormControl(''),
    SchoolId: new FormControl(''),
    GradeId: new FormControl(''),
    SubjectId: new FormControl(''),
    ContentPanelId: new FormControl(),
    ChapterId: new FormControl(),
    UnitId: new FormControl(),
    CreatorId: new FormControl(),
    Status: new FormControl(''),
    Format: new FormControl(''),
    Type: new FormControl(''),
    Level: new FormControl(),
    CreateDate: new FormControl(),
    ModifiedDate: new FormControl(),
    Page: 1,
    Size: 10,
    GetCount: true,
  });
  // dùng modal chọn câu hỏi từ ngân hàng thêm vào đề thi
  @Input() getQuestionsToTest = false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Input() hideStatusField = false;

  /* loading sniper */
  isLoadingResults = false;
  // emit form change
  isValueFormChange = false;

  // variable common
  levelManageValue = 0;
  baseApiUrl = '';
  minDate: Date = new Date();
  listCreator: any;
  cloneValueForm: any;
  togleField = false;

  // List opitons of select option
  public filteredDivisions: ReplaySubject<DivisionOption[]> = new ReplaySubject<DivisionOption[]>(1);
  public filteredSchools: ReplaySubject<SchoolOption[]> = new ReplaySubject<SchoolOption[]>(1);
  public filteredGrade: ReplaySubject<GradeOption[]> = new ReplaySubject<GradeOption[]>(1);
  public filteredSubjects: ReplaySubject<SubjectFilter[]> = new ReplaySubject<SubjectFilter[]>(1);
  public filteredChapters: ReplaySubject<Chapter[]> = new ReplaySubject<Chapter[]>(1);
  public filteredFrames: ReplaySubject<Curriculum[]> = new ReplaySubject<Curriculum[]>(1);
  public filteredExerciser: ReplaySubject<Exerciser[]> = new ReplaySubject<Exerciser[]>(1);
  public filteredCreator: ReplaySubject<any> = new ReplaySubject<any>(1);
  public filteredQuestionType: QuestionType[] = [
    { id: null, name: 'Chọn loại câu hỏi' },
        { id: Type.singlechoice, name: 'Trắc nghiệm một lựa chọn' },
        { id: Type.multichoice, name: 'Trắc nghiệm nhiều lựa chọn' },
        { id: Type.truefasle, name: 'Lựa chọn đúng sai' },
        { id: Type.truefaslechause, name: 'Mệnh đề đúng sai' },
        { id: Type.fillblank, name: 'Điền khuyết' },
        { id: Type.fillblank2, name: 'Điền khuyết loại 2' },
        { id: Type.underline, name: 'Gạch chân' },
        { id: Type.matching, name: 'Ghép đôi' },
        { id: Type.essay, name: 'Tự luận' }
  ];
  public filteredLevels: Level[] = [
    { id: null, name: 'Chọn mức độ' },
    { id: 'NB', name: 'Nhận biết' },
    { id: 'TH', name: 'Thông hiểu' },
    { id: 'VD', name: 'Vận dụng' },
    { id: 'VDC', name: 'Vận dụng cao' },
    { id: 'DMD', name: 'Đa mức độ' }
  ];
  public filteredQuestionFormat: QuestionType[] = [
    { id: null, name: 'Chọn dạng câu hỏi' },
      { id: 'SIG', name: 'Câu đơn' },
      { id: 'GRO', name: 'Câu chùm' }
  ];
  public filteredStatusFormat: QuestionType[] = [
    { id: null, name: 'Chọn trạng thái' },
      { id: 'EDI', name: 'Đang chỉnh sửa' },
      { id: 'WAI', name: 'Chờ duyệt' },
      { id: 'PUB', name: 'Đã duyệt' },
      { id: 'REF', name: 'Trả lại' }
  ];


  filteredLevelsClone = JSON.parse(JSON.stringify(this.filteredLevels));

  protected divisions: DivisionOption[];
  protected schools: SchoolOption[];
  protected grades: GradeOption[];
  protected subjects: SubjectFilter[];
  protected questionTypes: QuestionType[];
  protected questionFormat: QuestionType[];
  protected statusFormat: QuestionType[];
  protected frames: Curriculum[];
  protected chapters: Chapter[];
  protected exercisers: Exerciser[];

  // First load variable
  isFirstLoadDivision = true;
  isFirstLoadSchool = true;
  isFirstLoadGrade = true;
  isFirstLoadSubject = true;
  isFirstLoadFrames = true;
  isFirstLoadChapter = true;
  isFirstLoadExerciser = true;

  // Endpoint
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointSubject: SubjectsEndpoint;
  endpointContentPanel: CurriculumEndpoint;
  userIdentity: UserIdentity;
  endpointQuestionBank: QuestionBankEndpoint;

  // Get value of field in form
  get divisionId() { return this.filterQuestionForm.get('DivisionId'); }
  get schoolId() { return this.filterQuestionForm.get('SchoolId'); }
  get gradeId() { return this.filterQuestionForm.get('GradeId'); }
  get subjectId() { return this.filterQuestionForm.get('SubjectId'); }
  get framesId() { return this.filterQuestionForm.get('ContentPanelId'); }
  get chapterId() { return this.filterQuestionForm.get('ChapterId'); }
  get exerciseId() { return this.filterQuestionForm.get('UnitId'); }
  get statusId() { return this.filterQuestionForm.get('Status'); }
  get levelId() { return this.filterQuestionForm.get('Level'); }
  get questionFormatId() { return this.filterQuestionForm.get('Format'); }
  get userCreatedId() { return this.filterQuestionForm.get('CreatorId'); }
  get createdDate() { return this.filterQuestionForm.get('CreateDate'); }
  get modifiedDate() { return this.filterQuestionForm.get('ModifiedDate'); }
  get typeId() { return this.filterQuestionForm.get('Type'); }
  get page() { return this.filterQuestionForm.get('page'); }
  get size() { return this.filterQuestionForm.get('size'); }

  // form control
  public divisionFilterCtrl: FormControl = new FormControl();
  public schoolFilterCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public subjectFilterCtrl: FormControl = new FormControl();
  public framesFilterCtrl: FormControl = new FormControl();
  public questionFormatFilterCtrl: FormControl = new FormControl();
  public questionTypeFilterCtrl: FormControl = new FormControl();
  public chapterFilterCtrl: FormControl = new FormControl();
  public exerciseFilterCtrl: FormControl = new FormControl();
  public statusFilterCtrl: FormControl = new FormControl();
  public levelFilterCtrl: FormControl = new FormControl();
  public creatorFilterCtrl: FormControl = new FormControl();

  protected _onDestroy = new Subject<void>();

  /* Disable property */
  isDisableSchoolId = false;
  isDisableGradeId = false;
  isDisableSubjecId = false;
  isDisableSubjectId = true;
  isDisablequestionTypeId = true;
  isDisableFramesId = true;
  isDisableChapterId = true;
  isDisableExerciseId = true;

  constructor(private fb: FormBuilder,
    private _authService: AuthService, private questionService: ListQuestionService) {
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubject = new SubjectsEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointContentPanel = new CurriculumEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointQuestionBank = new QuestionBankEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();

    if (this.userIdentity.levelManage == LevelManages.Department) {
      this.levelManageValue = 1;
    } else if (this.userIdentity.levelManage == LevelManages.Division) {
      this.levelManageValue = 2;
      // Tài khoản cấp Phòng
      // divisionId fillter mặc định theo id phòng của tài khoản hiện tại
      this.filterQuestionForm.controls['SchoolId'].setValue(this.userIdentity.schoolId);
    } else {
      this.levelManageValue = 3;
      // Tài khoản cấp Trường, schoolid fillter mặc định theo  id trường của tài khoản hiện tại
      this.filterQuestionForm.controls['SchoolId'].setValue(this.userIdentity.schoolId);
      // this.filterQuestionForm.controls['SchoolId'].setValue(this.userIdentity.schoolId);
    }
  }

  ngOnInit(): void {
    this.divisionFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => this.filterDivisions());
    this.schoolFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => this.filterSchools());
    this.gradeFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => this.filterGrades());
    this.subjectFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => this.filterSubjects());
    this.framesFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => this.filterFrames());
    // this.questionTypeFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => this.filterQuestionTypes());
    this.exerciseFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => this.filterExercises());
    this.chapterFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => this.filterChapters());
    this.creatorFilterCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe(() => this.filterCreators());

    // Nếu mở modal để lấy câu hỏi vào đề thi thì chỉ xét câu hỏi đã được duyệt
    if (this.getQuestionsToTest) {
      this.filteredStatusFormat = [{ id: 'PUB', name: 'Đã duyệt' }];
      this.filterQuestionForm.controls['Status'].setValue('PUB');
    }
    this.filterQuestionForm.valueChanges.subscribe(() => this.isValueFormChange = true);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.hideStatusField?.currentValue) {
      this.filterQuestionForm.controls['Status'].setValue(null);
      this.togleField = false;
    } else {
      this.togleField = true;
      this.filteredStatusFormat = [{ id: 'WAI', name: 'Chờ duyệt' }];
      this.filterQuestionForm.controls['Status'].setValue('WAI');
    }
  }

  onSubmitFilter(): void {
    // Nếu mở modal để lấy câu hỏi vào đề thi thì chỉ xét câu hỏi đã được duyệt
    if (this.getQuestionsToTest) {
      this.filterQuestionForm.controls['Status'].setValue('PUB');
    }
    // Check không có request liên tục
    if ((JSON.stringify(this.cloneValueForm) !== JSON.stringify(this.filterQuestionForm.value)) && this.isValueFormChange) {
      this.questionService.emitDataFilter.next(this.filterQuestionForm.value);
      this.cloneValueForm = this.filterQuestionForm.value;
    }
  }

  ngAfterViewInit(): void {
    this.divisionId.valueChanges.pipe(startWith(this.divisionId?.value), pairwise()).subscribe(
      ([old, value]) => {
        if (old !== value) {
          this.onResetSchoolSelect();
          this.onResetGradeSelect();
          this.onResetSubjectSelect();
          this.onResetStatusSelect();
          this.onResetFramesSelect();
          this.onResetChapterSelect();
          this.onResetQuestionFormatSelect();
          this.onResetExerciseSelect();
          this.onResetQuestionTypeSelect();
          this.onResetLevelSelect();
          this.isDisableSubjectId = true;
        }
      }
    );
    this.schoolId.valueChanges.pipe(startWith(this.schoolId.value), pairwise()).subscribe(
      ([old, value]) => {
        if (old !== value) {
          this.onResetGradeSelect();
          this.isDisableSubjectId = true;
        }
      }
    );
    this.gradeId.valueChanges.pipe(startWith(this.gradeId.value), pairwise()).subscribe(
      ([old, value]) => {
        if (old !== value) {
          this.onResetSubjectSelect();
          this.onResetFramesSelect();
          this.onResetChapterSelect();
          if (value < 1) {
            this.isDisableSubjectId = true;
            this.isDisableFramesId = true;
            this.isDisableChapterId = true;
            this.isDisableExerciseId = true;
          } else {
            this.isDisableSubjectId = false;
          }
        }
      }
    );
    this.subjectId.valueChanges.pipe(startWith(this.subjectId.value), pairwise()).subscribe(
      ([old, value]) => {
        if (old !== value) {
          this.onResetFramesSelect();
          if (value < 1) {
            this.isDisableFramesId = true;
          } else {
            this.isDisableFramesId = false;
          }
        }
      }
    );
    this.framesId.valueChanges.pipe(startWith(this.framesId.value), pairwise()).subscribe(
      ([old, value]) => {
        if (old !== value) {
          this.onResetChapterSelect();
          if (value < 1) {
            this.isDisableChapterId = true;
          } else {
            this.isDisableChapterId = false;
          }
        }
      }
    );
    this.chapterId.valueChanges.pipe(startWith(this.chapterId.value), pairwise()).subscribe(
      ([old, value]) => {
        if (old !== value) {
          this.onResetExerciseSelect();
          if (value < 1) {
            this.isDisableExerciseId = true;
          } else {
            this.isDisableExerciseId = false;
          }
        }
      }
    );
    this.questionFormatId.valueChanges.pipe(startWith(this.questionFormatId.value), pairwise()).subscribe(
      ([old, value]) => {
        if (old !== value) {
          this.onResetQuestionTypeSelect();
          if (value === 'GRO') {
            this.isDisablequestionTypeId = true;
            this.filteredLevels = [{ id: 'DMD', name: 'Đa mức độ' }]
            this.filterQuestionForm.controls['Level'].setValue('DMD');
          } else {
            if (value) {
              this.filteredLevels = this.filteredLevelsClone.filter(item => item.id !== 'DMD');
              this.isDisablequestionTypeId = false;
            } else {
              this.filteredLevels = this.filteredLevelsClone;
            }
            this.filterQuestionForm.controls['Level'].setValue(null);
          }
        }
      }
    );
  }

  // Tìm kiếm phòng
  filterDivisions(): void {
    if (!this.divisions) { return; }
    let search = this.divisionFilterCtrl.value;
    if (!search) {
      this.filteredDivisions.next(this.divisions.slice());
      return;
    } else { search = search.toLowerCase(); }
    this.filteredDivisions.next(
      this.divisions.filter(division => division.name.toLowerCase().indexOf(search) > -1)
    );
  }

  // Tìm kiếm trường
  filterSchools(): void {
    if (!this.schools) { return; }
    let search = this.schoolFilterCtrl.value;
    if (!search) {
      this.filteredSchools.next(this.schools.slice());
      return;
    } else { search = search.toLowerCase(); }
    this.filteredSchools.next(
      this.schools.filter(school => school.name.toLowerCase().indexOf(search) > -1)
    );
  }

  // Tìm kiếm khối
  filterGrades(): void {
    if (!this.grades) { return; }
    let search = this.gradeFilterCtrl.value;
    if (!search) {
      this.filteredGrade.next(this.grades.slice());
      return;
    } else { search = search.toLowerCase(); }
    this.filteredGrade.next(
      this.grades.filter(grade => grade.name.toLowerCase().indexOf(search) > -1)
    );
  }

  // Tìm kiếm môn học
  filterSubjects(): void {
    if (!this.subjects) { return; }
    let search = this.subjectFilterCtrl.value;
    if (!search) {
      this.filteredSubjects.next(this.subjects.slice());
      return;
    } else { search = search.toLowerCase(); }
    this.filteredSubjects.next(
      this.subjects.filter(subject => subject.name.toLowerCase().indexOf(search) > -1)
    );
  }

   // Tìm kiếm khung chương trình
   filterFrames(): void {
    if (!this.frames) { return; }
    let search = this.framesFilterCtrl.value;
    if (!search) {
      this.filteredFrames.next(this.frames.slice());
      return;
    } else { search = search.toLowerCase(); }
    this.filteredFrames.next(
      this.frames.filter(frame => frame.curriculumName.toLowerCase().indexOf(search) > -1)
    );
  }

   // Tìm kiếm bài
   filterExercises(): void {
    if (!this.exercisers) { return; }
    let search = this.exerciseFilterCtrl.value;
    if (!search) {
      this.filteredExerciser.next(this.exercisers.slice());
      return;
    } else { search = search.toLowerCase(); }
    this.filteredExerciser.next(
      this.exercisers.filter(type => type.curriculumName.toLowerCase().indexOf(search) > -1)
    );
  }

  // Tìm kiếm chương câu hỏi
  filterChapters(): void {
    if (!this.chapters) { return; }
    let search = this.chapterFilterCtrl.value;
    if (!search) {
      this.filteredChapters.next(this.chapters.slice());
      return;
    } else { search = search.toLowerCase(); }
    this.filteredChapters.next(
      this.chapters.filter(chapter => chapter.curriculumName.toLowerCase().indexOf(search) > -1)
    );
  }

  // Tìm kiếm trạng thái câu hỏi
  filterCreators(): void {
    if (!this.listCreator) { return; }
    let search = this.creatorFilterCtrl.value;
    if (!search) {
      this.filteredCreator.next(this.listCreator.slice());
      return;
    } else { search = search.toLowerCase(); }
    this.filteredCreator.next(
      this.listCreator.filter(creator => creator.name.toLowerCase().indexOf(search) > -1)
    );
  }

  /* Get division */
  onDivisionSelectClicked(): void {
    if (this.isFirstLoadDivision) {
      this.endpointDivision.getDivisionOptions().then(res => {
        this.isFirstLoadDivision = false;
        this.divisions = [{ id: null, name: 'Chọn phòng' }, ...res];
        this.filteredDivisions.next(this.divisions.slice());
      }).catch(err => console.log(err));
    } else { }
  }

  /* Get school */
  onSchoolSelectClicked(): void {
    let divisionIdSelected = this.divisionId.value;
    if (this.userIdentity.levelManage === LevelManages.Division) {
      divisionIdSelected = this.userIdentity.divisionId;
    }
    if (this.isFirstLoadSchool) {
      this.endpointSchool.getSchoolOptions(divisionIdSelected ? divisionIdSelected : null).then(res => {
        if (res) {
          this.isFirstLoadSchool = false;
          this.schools = [{ id: null, name: 'Chọn trường' }, ...res];
          this.filteredSchools.next(this.schools.slice());
        }
      }).catch(err => console.log(err));
    } else { }
  }

  /* Get grade */
  onGradeSelectClicked(): void {
    const schoolIdSelected = this.schoolId.value;
    if (this.isFirstLoadGrade) {
      this.endpointGrade.getGradeOptions(schoolIdSelected ? schoolIdSelected : null).then(res => {
        this.isFirstLoadGrade = false;
        this.isDisableSubjectId = false;
        this.grades = [{ id: null, name: 'Chọn khối' }, ...res];
        this.filteredGrade.next(this.grades.slice());
      }).catch(err => console.log(err));
    } else { }
  }

  /* Get subject */
  onSubjectSelectClicked(): void {
    const gradeSelected = this.gradeId.value;
    if (gradeSelected && this.isFirstLoadSubject) {
      this.endpointSubject.getSubjectByGradeId(gradeSelected ? gradeSelected : null).then(res => {
        this.isFirstLoadSubject = false;
        this.isDisableFramesId = false;
        this.subjects = [{ id: null, name: 'Chọn môn' }, ...res.listSubjectOfGrade];
        this.filteredSubjects.next(this.subjects.slice());
      }).catch(err => console.log(err));
    }
  }

  /* Get Frames */
  onFramesSelectClicked(): void {
    const value = new CurriculumsFilterRequests();
    value.gradeId = this.gradeId.value;
    value.subjectId = this.subjectId.value;
    // value.isActive = true;
    if (value.gradeId && value.subjectId && this.isFirstLoadFrames) {
      // this.endpointContentPanel.getCurriculumsPaging(value).then(res => {
      this.endpointContentPanel.getCurriculumsFilterQuestionBank(value, false, null, null).then(res => {
        this.isFirstLoadFrames = false;
        this.frames = [{
          curriculumId: null, curriculumName: 'Chọn khung', gradeName: '',
          isActived: false, subjectName: '', getActiveStatus: '', levelManageName: ''
        }, ...res];
        this.filteredFrames.next(this.frames.slice());
        if (this.frames.length > 1) {
          this.isDisableChapterId = false;
        }
      }).catch(err => console.log(err));
    }
  }

  /* Get Chapters */
  onChapterSelectClicked(): void {
    if (this.framesId.value && this.isFirstLoadChapter) {
      this.endpointContentPanel.getCurriculumChildrentNodes(this.framesId.value).then((res) => {
        this.isFirstLoadChapter = false;
        this.chapters = [{ id: null, curriculumName: 'Chọn chương' }, ...res];
        this.filteredChapters.next(this.chapters.slice());
        if (this.chapters.length > 1) {
          this.isDisableExerciseId = false;
        }
      }).catch(err => console.log(err));
    }
  }

  /* Get Exercise */
  onExerciseSelectClicked(): void {
    if (this.isFirstLoadExerciser) {
      this.endpointContentPanel.getCurriculumChildrentNodes(this.chapterId.value).then((res) => {
        this.isFirstLoadExerciser = false;
        this.exercisers = [{ id: null, curriculumName: 'Chọn bài' }, ...res];
        this.filteredExerciser.next(this.exercisers.slice());
      }).catch(err => console.log(err));
    }
  }

  /* Get status filter */
  onStatusSelectClicked(): void {
    // Nếu mở modal để lấy câu hỏi vào đề thi thì chỉ xét câu hỏi đã được duyệt
    if (this.getQuestionsToTest) {
      this.filteredStatusFormat = [{ id: 'PUB', name: 'Đã duyệt' }];
    }
  }

  onCreatorSelectClicked(): void {
    this.endpointQuestionBank.getAdminAndTeachers('').then((res: any) => {
      this.listCreator = [{ id: null, name: 'Chọn người tạo' }, ...res];
      this.filteredCreator.next(this.listCreator.slice());
    }).catch(err => {
      console.log(err);
    });
  }

  clearDate(field: string): void {
    if (field === 'create') {
      this.filterQuestionForm.controls.CreateDate.setValue('');
    } else {
      this.filterQuestionForm.controls.ModifiedDate.setValue('');
    }
  }

  /* reset change School */
  private onResetSchoolSelect(): void {
    this.filterQuestionForm.controls['SchoolId'].setValue('');
    this.isFirstLoadSchool = true;
  }

  /* reset change Grade */
  private onResetGradeSelect(): void {
    this.filterQuestionForm.controls['GradeId'].setValue('');
    this.isFirstLoadGrade = true;
  }

  /* reset change Subject */
  private onResetSubjectSelect(): void {
    this.filterQuestionForm.controls['SubjectId'].setValue('');
    this.isFirstLoadSubject = true;
    this.isDisableSubjectId = true;
  }

  /* reset change Frames */
  private onResetFramesSelect(): void {
    this.filterQuestionForm.controls['ContentPanelId'].setValue('');
    this.isFirstLoadFrames = true;
  }

  /* reset change Chapter */
  private onResetChapterSelect(): void {
    this.filterQuestionForm.controls['ChapterId'].setValue('');
    this.isFirstLoadChapter = true;
  }

  /* reset change Chapter */
  private onResetExerciseSelect(): void {
    this.filterQuestionForm.controls['UnitId'].setValue('');
    this.isFirstLoadExerciser = true;
  }

  /* reset change Subject */
  private onResetStatusSelect(): void {
    this.filterQuestionForm.controls['Status'].setValue('');
    this.isDisableFramesId = true;
  }

  /* reset change Question type */
  private onResetQuestionTypeSelect(): void {
    this.filterQuestionForm.controls['Type'].setValue('');
    this.isDisablequestionTypeId = true;
  }

  /* reset change Question format */
  private onResetQuestionFormatSelect(): void {
    this.filterQuestionForm.controls['Format'].setValue('');
    this.isDisablequestionTypeId = true;
  }
  /* reset change Level */
  private onResetLevelSelect(): void {
    this.filterQuestionForm.controls['Level'].setValue('');
    this.isDisablequestionTypeId = true;
  }

}

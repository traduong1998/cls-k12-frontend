import { EditQuestionComponent } from './views/edit-question/edit-question.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageQuestionBankComponent } from './manage-question-bank.component';
import { CreateQuestionComponent } from './views/create-question/create-question.component';
import { LayoutComponent } from './views/layout/layout.component';
import { ImportQuestionsFromFileComponent } from './views/import-questions-from-file/import-questions-from-file.component';

const routes: Routes = [
  {
    path: '',
    component: ManageQuestionBankComponent,
    children: [
      {
        path: '',
        component: LayoutComponent,
        data: {
          title: 'Ngân hàng câu hỏi'
        }
      },
      {
        path: 'create-question',
        component: CreateQuestionComponent,
        data: {
          title: 'Tạo câu hỏi'
        }
      },
      {
        path: 'edit-question/:id',
        component: EditQuestionComponent,
        data: {
          title: 'Chỉnh sửa câu hỏi'
        }
      },
      {
        path: 'import-questions-from-file',
        component: ImportQuestionsFromFileComponent,
        data: {
          title: 'Thêm câu hỏi từ file'
        }
      },
    ],
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: []
})
export class ManageQuestionBankRoutingModule { }

export interface QuestionType {
    id: string;
    name: string;
}

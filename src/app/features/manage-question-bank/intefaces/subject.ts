export interface SubjectFilter {
    id?: number;
    name?: string;
}

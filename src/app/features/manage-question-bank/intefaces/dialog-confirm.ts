export interface DialogConfirm{
    title?:string
    description?:string,    
    labelYes?:string,    
    labelNo?:string,    
    type?:string,
    labelConfirm?:string,    
}
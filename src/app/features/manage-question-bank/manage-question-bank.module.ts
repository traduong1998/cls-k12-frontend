import { RouterModule } from '@angular/router';
import { QuestionBanksModule } from 'src/app/shared/modules/question-banks/question-banks.module';
import { CKEditorModule } from 'ckeditor4-angular';
import { NgModule } from '@angular/core';
import { ManageQuestionBankRoutingModule } from './manage-question-bank-routing.module';
import { ManageQuestionBankComponent } from '../manage-question-bank/manage-question-bank.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { QuestionBankComponent } from './views/question-bank/question-bank.component';
import { FilterQuestionComponent } from './components/filter-question/filter-question.component';
import { ListQuestionComponent } from './views/list-question/list-question.component';
import { CdkTableModule } from '@angular/cdk/table';
import { SingleChoiceQuestionComponent } from './components/single-choice-question/single-choice-question.component';
import { DisplayQuestionComponent } from './components/display-question/display-question.component';
import { MultiChoiceQuestionComponent } from './components/multi-choice-question/multi-choice-question.component';
import { TruefalseQuestionComponent } from './components/truefalse-question/truefalse-question.component';
import { TrueFalseClauseQuestionComponent } from './components/true-false-clause-question/true-false-clause-question.component';
import { MatchingQuestionComponent } from './components/matching-question/matching-question.component';
import { FillBlankQuestionComponent } from './components/fill-blank-question/fill-blank-question.component';
import { FillMultipleBlankQuestionComponent } from './components/fill-multiple-blank-question/fill-multiple-blank-question.component';
import { UnderlineQuestionComponent } from './components/underline-question/underline-question.component';
import { ClusterQuestionComponent } from './components/cluster-question/cluster-question.component';
import { EssayQuestionComponent } from './components/essay-question/essay-question.component';
import { CreateQuestionComponent } from './views/create-question/create-question.component';
import { DialogNoteComponent } from './views/list-question/dialog-note/dialog-note.component';
import { QuestionFilterActionComponent } from './views/create-question/views/question-filter-action/question-filter-action.component';
import { EditQuestionComponent } from './views/edit-question/edit-question.component';
import { StatusQuestionComponent } from './components/status-question/status-question.component';
import { LayoutComponent } from './views/layout/layout.component';
import { ImportQuestionsFromFileComponent } from './views/import-questions-from-file/import-questions-from-file.component';
import { QuestionInfoSelectedComponent } from './views/import-questions-from-file/components/question-info-selected/question-info-selected.component';
import { DialogPreviewQuestionsImportComponent } from './views/import-questions-from-file/components/dialog-preview-questions-import/dialog-preview-questions-import.component';
import { DialogStatisticQuestionsImportComponent } from './views/import-questions-from-file/components/dialog-statistic-questions-import/dialog-statistic-questions-import.component';

@NgModule({
  declarations: [ManageQuestionBankComponent,
    QuestionBankComponent, FilterQuestionComponent,
    ListQuestionComponent, SingleChoiceQuestionComponent,
    DisplayQuestionComponent, MultiChoiceQuestionComponent,
    TruefalseQuestionComponent, TrueFalseClauseQuestionComponent,
    MatchingQuestionComponent, FillBlankQuestionComponent,
    FillMultipleBlankQuestionComponent, UnderlineQuestionComponent,
    ClusterQuestionComponent, EssayQuestionComponent, CreateQuestionComponent,
    DialogNoteComponent, QuestionFilterActionComponent, EditQuestionComponent,
    StatusQuestionComponent, LayoutComponent, ImportQuestionsFromFileComponent,
    QuestionInfoSelectedComponent, DialogPreviewQuestionsImportComponent,DialogStatisticQuestionsImportComponent],
  imports: [
    SharedModule,
    CdkTableModule,
    CKEditorModule,
    ManageQuestionBankRoutingModule,
    QuestionBanksModule,
    RouterModule
  ],
  exports:[
    ListQuestionComponent,
    FilterQuestionComponent,
    DisplayQuestionComponent
  ]
})
export class ManageQuestionBankModule { }
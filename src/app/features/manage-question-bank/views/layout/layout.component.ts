import { Component, OnInit } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { AuthService } from 'src/app/core/services';
import { CLSModules, CLSPermissions, UserIdentity } from 'sdk/cls-k12-sdk-js/src';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  statusWaiting = false;
  hasApprovePermission: boolean;
  userIdentity: UserIdentity;
  listAll = false;
  hideStatus = false;

  constructor(
    private _authService: AuthService
  ) {
    this.userIdentity = this._authService.getTokenInfo();
    this.hasApprovePermission = this.userIdentity.hasPermission(CLSModules.QuestionBank, CLSPermissions.Approved);
  }

  ngOnInit(): void { }

  // Check index tab for toggle filter
  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    if (tabChangeEvent.index === 1) {
      this.statusWaiting = true;
      this.listAll = false;
      this.hideStatus = true;
    } else {
      this.listAll = true;
      this.statusWaiting = false;
      this.hideStatus = false;
    }
  }
}

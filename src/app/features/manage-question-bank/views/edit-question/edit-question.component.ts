import { ListQuestionService } from './../../services/list-question.service';
import { QuestionBankEndpoint } from 'cls-k12-sdk-js/src';
import { QuestionViewData } from './../../../../shared/modules/question-banks/Interfaces/question-data';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { ConfigUploadCkeditor } from 'src/app/shared/modules/question-banks/Interfaces/config-upload-ckeditor';
import { ActivatedRoute, Router } from '@angular/router';
import { EditSingleChoiceQuestionComponent } from 'src/app/shared/modules/question-banks/components/edit-single-choice-question/edit-single-choice-question.component';
import { EditMultipleChoiceQuestionComponent } from 'src/app/shared/modules/question-banks/components/edit-multiple-choice-question/edit-multiple-choice-question.component';
import { EditTrueFalseQuestionComponent } from 'src/app/shared/modules/question-banks/components/edit-true-false-question/edit-true-false-question.component';
import { EditTrueFalseClauseQuestionComponent } from 'src/app/shared/modules/question-banks/components/edit-true-false-clause-question/edit-true-false-clause-question.component';
import { EditUnderLineQuestionComponent } from 'src/app/shared/modules/question-banks/components/edit-under-line-question/edit-under-line-question.component';
import { EditEssayQuestionComponent } from 'src/app/shared/modules/question-banks/components/edit-essay-question/edit-essay-question.component';
import { EditMatchingQuestionComponent } from 'src/app/shared/modules/question-banks/components/edit-matching-question/edit-matching-question.component';
import { EditMissingWord1QuestionComponent } from 'src/app/shared/modules/question-banks/components/edit-missing-word1-question/edit-missing-word1-question.component';
import { EditMissingWord2QuestionComponent } from 'src/app/shared/modules/question-banks/components/edit-missing-word2-question/edit-missing-word2-question.component';
import { EditGroupQuestionComponent } from 'src/app/shared/modules/question-banks/components/edit-group-question/edit-group-question.component';
import { DataRequestCreateQuestion } from 'sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/requests/data-request-create-question';
import { Type } from '../../../../../../sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

declare var CKEDITOR: any;
CKEDITOR.plugins.addExternal(
  'insertaudio',
  window.location.origin + '/assets/ckeditor/plugins/InsertAudio/',
  'InsertAudio.js');
CKEDITOR.plugins.addExternal(
  'insertimage',
  window.location.origin + '/assets/ckeditor/plugins/InsertImage/',
  'InsertImage.js');
CKEDITOR.plugins.addExternal(
  'insertvideo',
  window.location.origin + '/assets/ckeditor/plugins/InsertVideo/',
  'InsertVideo.js');

CKEDITOR.plugins.addExternal(
  'createunderline',
  window.location.origin + '/assets/ckeditor/plugins/underline/',
  'plugin.js');

CKEDITOR.plugins.addExternal(
  'fillblank1',
  window.location.origin + '/assets/ckeditor/plugins/fillblank1/',
  'plugin.js');
CKEDITOR.plugins.addExternal(
  'fillblank2',
  window.location.origin + '/assets/ckeditor/plugins/fillblank2/',
  'plugin.js');
CKEDITOR.plugins.addExternal('ckeditor_wiris', window.location.origin + '/assets/ckeditor/plugins/mathtype/', 'plugin.js');
@Component({
  selector: 'app-edit-question',
  templateUrl: './edit-question.component.html',
  styleUrls: ['./edit-question.component.scss']
})
export class EditQuestionComponent implements OnInit {
  // EndPoint
  endpointQuestionBank: QuestionBankEndpoint;
  dataQuestionView: DataRequestCreateQuestion;

  @Input() configUpload: ConfigUploadCkeditor;
  @ViewChild(EditSingleChoiceQuestionComponent) singleChoiceQuestionUpdate;
  @ViewChild(EditMultipleChoiceQuestionComponent) multipleChoiceQuestionUpdate;
  @ViewChild(EditTrueFalseQuestionComponent) trueFalseQuestionUpdate;
  @ViewChild(EditTrueFalseClauseQuestionComponent) trueFalseClauseQuestionUpdate;
  @ViewChild(EditUnderLineQuestionComponent) underlineQuestionUpdate;
  @ViewChild(EditEssayQuestionComponent) easayQuestionUpdate;
  @ViewChild(EditMatchingQuestionComponent) matchingQuestionUpdate;
  @ViewChild(EditMissingWord1QuestionComponent) missingWord1QuestionUpdate;
  @ViewChild(EditMissingWord2QuestionComponent) missingWord2QuestionUpdate;
  @ViewChild(EditGroupQuestionComponent) groupQuestionUpdate;
  @ViewChild("ck") ck: CKEditorComponent;

  baseApiUrl = '';
  isLoadingResults = false;
  isLevel = '';
  isAutoApproveQuestion = false;
  subjetFilter;

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private route: ActivatedRoute,
    private questionService: ListQuestionService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {
    this.endpointQuestionBank = new QuestionBankEndpoint({ baseUrl: this.baseApiUrl });
  }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.getDetailQuestion(id);
    this.subjetFilter = this.questionService.dataFilterCreate$.subscribe(response => {
      if (response['autoRequestApproval'] !== undefined) {
        this.isAutoApproveQuestion = response['autoRequestApproval'];
      } else {
        if (response['Level']) {
          this.isLevel = response['Level'];
        }
        Object.keys(response).forEach(res => {
          Object.keys(this.dataQuestionView).forEach(list => {
            if (list.toLowerCase() === res.toLowerCase()) {
              this.dataQuestionView[list] = response[res];
            }
          })
        });
      }
    });
  }

  // Get detail question
  getDetailQuestion(id): void {
    this.isLoadingResults = true;
    this.endpointQuestionBank.getQuestionDetail(id).then((response: QuestionViewData) => {
      this.dataQuestionView = response;
      this.dataQuestionView['action'] = 'EDI';
      this.isLoadingResults = false;
    }).catch(err => {
      this.isLoadingResults = false;
    });
  }

  // Emit from output and handle call api
  handleData(): void {
    if (this.checkRequireField()) {
      this.endpointQuestionBank.updateQuestion(this.dataQuestionView, this.isAutoApproveQuestion).then(() => {
        this._snackBar.openFromComponent(SuccessSnackBarComponent, {
          data: `Cập nhật thành công`,
          duration: 3000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
        this.router.navigate(['/dashboard/question-bank']);
      }).catch((err) => console.log(err));
    }
  }

  checkRequireField() {
    if (!this.dataQuestionView.gradeId) {
      alert('Cần chọn khối để sửa');
      return null;
    } else if (!this.dataQuestionView.subjectId) {
      alert('Cần chọn môn học để sửa');
      return null;
    } else {
      if (this.dataQuestionView.contentPanelId) {
        if (!this.dataQuestionView.chapterId || !this.dataQuestionView.unitId) {
          alert('Nếu đã chọn khung thì phải chọn chương và chọn bài');
          return null;
        } else {
          this.dataQuestionView['level'] = this.isLevel
          return this.dataQuestionView;
        }
      } else {
        this.dataQuestionView['level'] = this.isLevel
        return this.dataQuestionView;
      }
    }
  }

  getvalue(): void {
    if (this.dataQuestionView.type === Type.singlechoice && this.dataQuestionView.format !== FormatQuestion.group) {
      this.singleChoiceQuestionUpdate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.multichoice) {
      this.multipleChoiceQuestionUpdate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.truefasle) {
      this.trueFalseQuestionUpdate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.truefaslechause) {
      this.trueFalseClauseQuestionUpdate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.fillblank) {
      this.missingWord1QuestionUpdate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.fillblank2) {
      this.missingWord2QuestionUpdate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.essay) {
      this.easayQuestionUpdate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.underline) {
      this.underlineQuestionUpdate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.matching) {
      this.matchingQuestionUpdate.updateQuestion();
    } else if (this.dataQuestionView.format === FormatQuestion.group) {
      this.groupQuestionUpdate.updateQuestion();
    } else { }
  }

  ngOnDestroy(): void {
    if(this.subjetFilter){
      this.subjetFilter.unsubscribe();
    }
  }

}

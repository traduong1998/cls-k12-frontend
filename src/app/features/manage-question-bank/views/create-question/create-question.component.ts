import { Router } from '@angular/router';
import { CreateGroupQuestionComponent } from './../../../../shared/modules/question-banks/components/create-group-question/create-group-question.component';
import { CreateMissingWord2QuestionComponent } from './../../../../shared/modules/question-banks/components/create-missing-word2-question/create-missing-word2-question.component';
import { CreateMissingWord1QuestionComponent } from './../../../../shared/modules/question-banks/components/create-missing-word1-question/create-missing-word1-question.component';
import { CreateMatchingQuestionComponent } from './../../../../shared/modules/question-banks/components/create-matching-question/create-matching-question.component';
import { CreateEasayQuestionComponent } from './../../../../shared/modules/question-banks/components/create-easay-question/create-easay-question.component';
import { CreateUnderLineQuestionComponent } from './../../../../shared/modules/question-banks/components/create-under-line-question/create-under-line-question.component';
import { CreateTrueFalseClauseQuestionComponent } from './../../../../shared/modules/question-banks/components/create-true-false-clause-question/create-true-false-clause-question.component';
import { CreateTrueFalseQuestionComponent } from './../../../../shared/modules/question-banks/components/create-true-false-question/create-true-false-question.component';
import { CreateMultipleChoiceQuestionComponent } from './../../../../shared/modules/question-banks/components/create-multiple-choice-question/create-multiple-choice-question.component';
import { CreateSingleChoiceQuestionComponent } from './../../../../shared/modules/question-banks/components/create-single-choice-question/create-single-choice-question.component';
import { QuestionBankEndpoint } from 'cls-k12-sdk-js/src';
import { ListQuestionService } from './../../services/list-question.service';
import { ConfigUploadCkeditor } from 'src/app/shared/modules/question-banks/Interfaces/config-upload-ckeditor';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Type } from '../../../../../../sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { Level } from 'cls-k12-sdk-js/src/services/question/enums/Level';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { DataRequestCreateQuestion } from '../../../../../../sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/requests/data-request-create-question';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';

declare var CKEDITOR: any;
CKEDITOR.plugins.addExternal(
  'insertaudio',
  window.location.origin + '/assets/ckeditor/plugins/InsertAudio/',
  'InsertAudio.js');
CKEDITOR.plugins.addExternal(
  'insertimage',
  window.location.origin + '/assets/ckeditor/plugins/InsertImage/',
  'InsertImage.js');
CKEDITOR.plugins.addExternal(
  'insertvideo',
  window.location.origin + '/assets/ckeditor/plugins/InsertVideo/',
  'InsertVideo.js');

CKEDITOR.plugins.addExternal(
  'createunderline',
  window.location.origin + '/assets/ckeditor/plugins/underline/',
  'plugin.js');

CKEDITOR.plugins.addExternal(
  'fillblank1',
  window.location.origin + '/assets/ckeditor/plugins/fillblank1/',
  'plugin.js');
CKEDITOR.plugins.addExternal(
  'fillblank2',
  window.location.origin + '/assets/ckeditor/plugins/fillblank2/',
  'plugin.js');
CKEDITOR.plugins.addExternal('ckeditor_wiris', window.location.origin + '/assets/ckeditor/plugins/mathtype/', 'plugin.js');

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.scss']
})
export class CreateQuestionComponent implements OnInit {
  @Input() configUpload: ConfigUploadCkeditor;
  @ViewChild(CreateSingleChoiceQuestionComponent) singleChoiceQuestionCreate;
  @ViewChild(CreateMultipleChoiceQuestionComponent) multipleChoiceQuestionCreate;
  @ViewChild(CreateTrueFalseQuestionComponent) trueFalseQuestionCreate;
  @ViewChild(CreateTrueFalseClauseQuestionComponent) trueFalseClauseQuestionCreate;
  @ViewChild(CreateUnderLineQuestionComponent) underlineQuestionCreate;
  @ViewChild(CreateEasayQuestionComponent) easayQuestionCreate;
  @ViewChild(CreateMatchingQuestionComponent) matchingQuestionCreate;
  @ViewChild(CreateMissingWord1QuestionComponent) missingWord1QuestionCreate;
  @ViewChild(CreateMissingWord2QuestionComponent) missingWord2QuestionCreate;
  @ViewChild(CreateGroupQuestionComponent) groupQuestionCreate;

  // EndPoint
  endpointQuestionBank: QuestionBankEndpoint;
  dataQuestionView: DataRequestCreateQuestion;
  baseApiUrl = '';
  saveAndAddNew = false;
  isAutoApproveQuestion = false;
  typeClone = '';

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    private questionService: ListQuestionService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {
    // Set initial question
    this.dataQuestionView = {
      type: Type.singlechoice,
      level: Level.NB,
      content: '',
      format: FormatQuestion.single,
      answers: [],
      divisionId: null,
      schoolId: null,
      gradeId: null,
      subjectId: null,
      contentPanelId: null,
      chapterId: null,
      unitId: null
    };
    this.endpointQuestionBank = new QuestionBankEndpoint({ baseUrl: this.baseApiUrl });
  }

  ngOnInit(): void {
    this.questionService.dataFilterCreate$.subscribe(response => {
      if (response['autoRequestApproval'] !== undefined) {
        this.isAutoApproveQuestion = response['autoRequestApproval'];
      } else {
        if (this.dataQuestionView.type != response['Type'] || this.dataQuestionView.format != response['Format']) {
          this.dataQuestionView.content = '';
        }
        Object.keys(response).forEach(res => {
          Object.keys(this.dataQuestionView).forEach(list => {
            if (list.toLowerCase() === res.toLowerCase()) {
              this.dataQuestionView[list] = response[res];
            }
          })
        });
        if (response['Level']) {
          this.typeClone = response['Level'];
        }
        if (this.dataQuestionView.type === 'TFQ') {
          this.dataQuestionView.answers = [{
            content: "ans-true"
          }];
        } else {
          this.dataQuestionView.answers = [];
        }
        if (response['Format'] === 'GRO') {
          this.dataQuestionView.questions = [
            {
              content: "",
              type: 'SIG', level: 'NB', action: 'ADD',
              answers: []
            }
          ];
        } else {
          this.dataQuestionView.questions = null;
        }
      }
    });
  }

  getvalue(isAddNew: boolean) {
    this.saveAndAddNew = isAddNew;
    if (this.dataQuestionView.type === Type.singlechoice && this.dataQuestionView.format !== FormatQuestion.group) {
      this.singleChoiceQuestionCreate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.multichoice) {
      this.multipleChoiceQuestionCreate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.truefasle) {
      this.trueFalseQuestionCreate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.truefaslechause) {
      this.trueFalseClauseQuestionCreate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.fillblank) {
      this.missingWord1QuestionCreate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.fillblank2) {
      this.missingWord2QuestionCreate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.essay) {
      this.easayQuestionCreate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.underline) {
      this.underlineQuestionCreate.updateQuestion();
    } else if (this.dataQuestionView.type === Type.matching) {
      this.matchingQuestionCreate.updateQuestion();
    } else if (this.dataQuestionView.format === FormatQuestion.group) {
      this.dataQuestionView.answers = null;
      this.dataQuestionView.type = null;
      if (this.dataQuestionView.questions) {
        this.dataQuestionView.questions.forEach(val => {
          val['format'] = 'GRO';
        });
      }
      this.groupQuestionCreate.updateQuestion();
    } else { }
  }

  // Check Grade field and Subject field
  handleData(response): void {
    if (this.checkRequireField()) {
      this.addQuestion();
    }
  }

  // Emit from output and handle call api
  addQuestion(): void {
    this.endpointQuestionBank.createQuestion(this.dataQuestionView, this.isAutoApproveQuestion).then(() => {
      this._snackBar.openFromComponent(SuccessSnackBarComponent, {
        data: this.saveAndAddNew ? 'Lưu và tạo thành công' : 'Tạo thành công',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      // Nếu không phải lưu và tạo mới
      if (!this.saveAndAddNew) {
        this.router.navigate(['/dashboard/question-bank']);
      } else {
        window.location.reload();
      }
    }).catch((err) => console.log(err));
  }

  // Tạo câu hỏi cần chọn khối và môn
  checkRequireField() {
    if (this.dataQuestionView.questions) {
      this.dataQuestionView.questions.map(val => {
        val['format'] = 'GRO';
      });
    }
    if (!this.dataQuestionView.gradeId) {
      alert('Cần chọn khối để tạo');
      return null;
    } else if (!this.dataQuestionView.subjectId) {
      alert('Cần chọn môn học để tạo');
      return null;
    } else {
      // Nếu đã chọn khung chương trình thì phải chọn bài
      if (this.dataQuestionView.contentPanelId) {
        if (!this.dataQuestionView.chapterId || !this.dataQuestionView.unitId) {
          alert('Nếu đã chọn khung thì phải chọn chương và chọn bài');
          return null;
        } else {
          this.dataQuestionView.level = this.typeClone;
          return this.dataQuestionView;
        }
      } else {
        this.dataQuestionView.level = this.typeClone;
        return this.dataQuestionView;
      }
    }
  }

}

export interface QuestionTypeOption {
    id: string;
    name: string;
}
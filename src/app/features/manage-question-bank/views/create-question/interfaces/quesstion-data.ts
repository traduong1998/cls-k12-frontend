import { RSA_NO_PADDING } from "constants";

/**Dữ liệu câu hỏi cơ bản */
export interface QuestionCoreData {
    type?: string;
    questionGroupId?: number;
    level: string;
    content: string;
    format: string;
    answers: AnswerCoreData[];
    questions?: QuestionCoreData[];
}
export interface AnswerCoreData {
    content: string;
    trueAnswer: number;
    position?: boolean;
    groupOfFillBlank2?: number;
}

/**input yêu cầu, output module đáp ứng*/
export interface QuestionRequestOrResponse extends QuestionCoreData {
    id?: number;
    type?: string;
    questionGroupId?: number;
    level: string;
    content: string;
    format: string;
    answers: AnswerRequestOrResponse[];
    questions?: QuestionRequestOrResponse[];
    action?: string;
}

export interface AnswerRequestOrResponse extends AnswerCoreData {
    id: number;
    questionId: number;
    content: string;
    trueAnswer: number;
    position?: boolean;
    groupOfFillBlank2?: number;
    isShuffler: boolean;
    action?: string;
    // order dùng cho câu gạch chân
    order?: number;
}

/**Model dành cho hoạt động xử lý câu hỏi trong module */
export class QuestionViewData implements QuestionCoreData {
    id?: number;
    type?: string;
    questionGroupId?: number;
    level: string;
    content: string;
    format: string;
    answers: AnswerViewData[];
    questions?: QuestionViewData[];
    action?: string;
    requestStatus?: string;
    
    constructor() {
        this.requestStatus = this.action;
    }

    static FromQuestionResponse(data: QuestionRequestOrResponse): QuestionViewData {
        var model = {
            id: data.id,
            content: data.content,
            type: data.type,
            questionGroupId: data.questionGroupId,
            level: data.level,
            format: data.format,
            answers: data.answers,
            questions: data.questions,
            action: data.action
        };

        // Clone phương án của câu hỏi đơn
        if (data.answers) {
            model.answers = data.answers.map(ans => ({ ...ans }));
        }

        if (data.questions) {
            model.questions = JSON.parse(JSON.stringify(data.questions));
        }

        return model;
    }
}

export class AnswerViewData implements AnswerCoreData {
    id?: number;
    //Số thứ tự dùng cho xử lý câu gạch chân
    order?: number;
    questionId: number;
    content: string;
    trueAnswer: number;
    position?: boolean;
    groupOfFillBlank2?: number;
    isShuffler: boolean;
    action?: string;
    requestStatus?: string;
    constructor() {
        this.requestStatus = this.action;
    }
}

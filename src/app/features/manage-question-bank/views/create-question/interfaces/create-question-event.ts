import { Question } from "cls-k12-sdk-js/src/services/question/models/Question";

/**
 * Các sự kiện trong tạo câu hỏi
 */
export interface CreateQuestionEvent {
    updateQuestion(): void;
}
export interface EditQuestionEvent {
    updateQuestion(question): void;
}
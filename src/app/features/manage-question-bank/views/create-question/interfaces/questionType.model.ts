export interface QuestionType {
    value: string;
    viewValue: string;
  }


  export interface Level {
    value: string;
    viewValue: string;
  }
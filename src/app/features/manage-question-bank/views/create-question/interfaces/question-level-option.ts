export interface QuestionLevelOption {
    id: string;
    name: string;
}
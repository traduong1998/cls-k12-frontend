import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionFilterActionComponent } from './question-filter-action.component';

describe('QuestionFilterActionComponent', () => {
  let component: QuestionFilterActionComponent;
  let fixture: ComponentFixture<QuestionFilterActionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionFilterActionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionFilterActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

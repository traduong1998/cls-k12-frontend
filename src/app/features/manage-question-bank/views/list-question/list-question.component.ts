import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ListQuestionService } from './../../services/list-question.service';
import { Component, OnInit, AfterViewInit, ViewChild, Input, SimpleChanges, OnChanges } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { CLSModules, CLSPermissions, QuestionBankEndpoint, UserIdentity } from 'cls-k12-sdk-js/src';
import { ListQuestionBankResponse } from 'sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/responses/question-bank-response';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/PaginatorResponse';
import { MatPaginator } from '@angular/material/paginator';
import { DialogNoteComponent } from './dialog-note/dialog-note.component';
import { AuthService } from 'src/app/core/services';
import { Type } from '../../../../../../sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConfirmationDialogComponent } from '../../../../shared/material/confirmation-dialog/confirmation-dialog.component';
import { Observable, Subject } from 'rxjs';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { DeleteDiaLogComponent } from 'src/app/shared/dialog/delete-dia-log/delete-dia-log.component';

const questionPaginationDefault: PaginatorResponse<ListQuestionBankResponse> = {
  items: [],
  totalItems: 0
};

@Component({
  selector: 'app-list-question',
  templateUrl: './list-question.component.html',
  styleUrls: ['./list-question.component.scss']
})
export class ListQuestionComponent implements OnInit, AfterViewInit, OnChanges {
  questionPagination: PaginatorResponse<ListQuestionBankResponse> = questionPaginationDefault;
  displayedColumns: string[] = ['select', 'question', 'type', 'function'];
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  selection = new SelectionModel < ListQuestionBankResponse > (true, []);
  isLoadingResults = false;
  baseApiUrl = '';
  @Input() confirmQuestion = false;
  // Input dùng để lấy toàn bộ câu hỏi: status = '';
  @Input() listAll = false;
  @Input() listQuestionFromExam = [];
  @Input() isExamFinish = false;

  // Danh sách câu hỏi, truyền qua từ preview câu hỏi import bên NHCH
  @Input() listQuestionFromQuestionBank = [];
  @Input() fromQuestionBank = false;
  // showFileDownload
  @Input() showFileDownload = true;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dialogConfirm;
  // array xoá 1 hay nhiều câu hỏi
  arrayIdQuestionRemove: number[] = [];
  // array gửi duyệt 1 hay nhiều câu hỏi
  arrayIdQuestionApprove: number[] = [];
  // Hide checkbox column
  objDataApprove;
  objDataRefuse;
  hideButton = false;
  objCloneResponse;
  // dùng modal chọn câu hỏi từ ngân hàng thêm vào đề thi
  @Input() getQuestionsToTest = false;
  // dùng modal chọn câu hỏi từ ngân hàng thêm vào đề thi, disabled câu hỏi đã chọn để tránh chọn lại trong danh sách câu hỏi từ ngân hàng
  @Input() disableQuestionIds = [];
  // Parmas request list question
  paramRequest = {
    Page: 1,
    Size: 10,
    GetCount: true,
    keyword: ''
  };
  isFixedTable = false;
  isDisableScrollTable = true;
  // subject lắng nghe param từ bộ lọc
  subjectFilter;
  // check list all or confirm list
  isAllQuestion = true;
  // EndPoint
  endpointQuestionBank: QuestionBankEndpoint;
  // check quyền user
  userIdentity: UserIdentity;
  hasAddPermission: boolean;
  hasDeletePermission: boolean;
  hasEditPermission: boolean;
  hasApprovePermission: boolean;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.confirmQuestion?.currentValue === true) {
      // hiển thị câu hỏi ở trạng thái chờ duyệt
      this.paramRequest['Status'] = 'WAI';
      this.hideButton = true;
      this.isAllQuestion = false;
      this.getListQuestion(this.paramRequest);
    }
    if (changes.listAll?.currentValue === true) {
      this.paramRequest['Status'] = '';
      this.getListQuestion(this.paramRequest);
    }
    else if (changes.getQuestionsToTest?.currentValue === true) {
      // Chỉ hiện thị câu hỏi đã duyệt, dùng modal chọn câu hỏi từ ngân hàng thêm vào đề thi
      this.paramRequest['Status'] = 'PUB';
      this.getListQuestion(this.paramRequest);
    }
    if (changes.listQuestionFromExam?.currentValue) {
      // hiển thị danh sách câu hỏi ở kì thi
      this.questionPagination.items = [];
      this.spinner.show();
      this.questionPagination.items = changes.listQuestionFromExam.currentValue.questions;
      this.spinner.hide();
    }
    if (this.isExamFinish) {
      // chỉ hiển thị 4 trường ở finish exam component
      this.displayedColumns = ['select', 'question', 'score', 'type'];
    }

    if (changes.listQuestionFromQuestionBank?.currentValue) {
      // hiển thị danh sách câu hỏi xem trước ở NHCH
      this.questionPagination.items = [];
      this.spinner.show();
      this.questionPagination.items = changes.listQuestionFromQuestionBank.currentValue.questions;
      this.spinner.hide();
    }
    if (this.fromQuestionBank) {
      // chỉ hiển thị 3 trường ở preview NHCH
      this.displayedColumns = ['select', 'question', 'type'];
    }
  }

  constructor(
    private questionService: ListQuestionService,
    public dialog: MatDialog,
    private router: Router,
    private _authService: AuthService,
    private _snackBar: MatSnackBar,
    private spinner: NgxSpinnerService
  ) {
    this.endpointQuestionBank = new QuestionBankEndpoint({ baseUrl: this.baseApiUrl });
    if (this.getQuestionsToTest) {
      this.displayedColumns = ['select', 'question', 'type'];
    }
    this.userIdentity = _authService.getTokenInfo();
    this.hasAddPermission = this.userIdentity.hasPermission(CLSModules.QuestionBank, CLSPermissions.Add);
    this.hasEditPermission = this.userIdentity.hasPermission(CLSModules.QuestionBank, CLSPermissions.Edit);
    this.hasDeletePermission = this.userIdentity.hasPermission(CLSModules.QuestionBank, CLSPermissions.Delete);
    this.hasApprovePermission = this.userIdentity.hasPermission(CLSModules.QuestionBank, CLSPermissions.Approved);
    // if (this.router.url === '/dashboard/question-bank') {
    //   window.addEventListener('scroll', this.scroll, true);
    // }
  }

  scroll = (event): void => {
    // Nếu vị trí hiện tại lớn hơn hoặc bằng vị trí của table thì fixed header table
    let element = document.getElementById('table_view')?.offsetTop;
    if (event.target.scrollTop > element && this.isDisableScrollTable) {
      this.isFixedTable = true;
      this.isDisableScrollTable = false;
    } else {
    }
  };

  ngAfterViewInit(): void {
    this.paginator._intl.itemsPerPageLabel = "Số dòng trên trang";
    this.paginator.page.subscribe(() => {
      this.spinner.show();
      if (this.objCloneResponse) {
        this.paramRequest = this.objCloneResponse;
      }
      this.paramRequest.Page = this.paginator.pageIndex + 1;
      this.paramRequest.Size = this.paginator.pageSize;
      this.getListQuestion(this.paramRequest);
      this.scrollToView();
      this.spinner.hide();
    });
  }

  navigateToEditQuestion(value: number): void {
    this.router.navigate([`/dashboard/question-bank/edit-question/${value}`]);
  }

  ngOnInit(): void {
    if (!("Status" in this.paramRequest) && !this.isExamFinish && !this.fromQuestionBank) {
      this.getListQuestion(this.paramRequest);
    }
    this.subjectFilter = this.questionService.dataFilter$.subscribe(response => {
      this.objCloneResponse = response;
      this.objCloneResponse.Page = 1;
      this.objCloneResponse.Size = 10;
      this.getListQuestion(response);
    });
    if (this.getQuestionsToTest) {
      this.displayedColumns = ['select', 'question', 'type'];
    }
  }

  scrollToView() {
    const elementToScroll = document.getElementById('table_view');
    elementToScroll.scrollIntoView({ behavior: "smooth" });
  }

  onSearchQuestion(value): void {
    if (this.paramRequest.keyword !== value) {
      this.paramRequest.keyword = value;
      this.getListQuestion(this.paramRequest);
    }
  }

  getListQuestion(response?): void {
    this.spinner.show();
    this.endpointQuestionBank.getListQuestionFilter(response).then(res => {
      this.questionPagination = res;
      
      // console.log(res);
      const value = this.questionPagination.items;
      this.questionPagination.items = this.questionService.beauty(value);
      this.spinner.hide();
    }).catch(() => {
      this.showDialogAction('err', 'Có lỗi xảy ra');
      this.spinner.hide();
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.questionPagination.items.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }
    this.selection.select(...this.questionPagination.items);
  }

  // Check type question in table
  checkTypeQuestionTable(type): string {
    switch (type) {
      case Type.singlechoice:
        return 'Trắc nghiệm một lựa chọn';
      case Type.multichoice:
        return 'Trắc nghiệm nhiều lựa chọn';
      case Type.truefasle:
        return 'Đúng sai';
      case Type.truefaslechause:
        return 'Mệnh đề đúng sai';
      case Type.fillblank:
        return 'Điền khuyết';
      case Type.fillblank2:
        return 'Câu điền khuyết loại 2';
      case Type.underline:
        return 'Gạch chân';
      case Type.matching:
        return 'Câu ghép đôi';
      case Type.essay:
        return 'Câu tự luận';
      default:
        return '';
    }
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: ListQuestionBankResponse): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row}`;
  }

  // watch note question
  watchNoteQuestion(note): void {
    this.dialog.open(DialogNoteComponent, {
      disableClose: false,
      width: '540px',
      height: '200px',
      data: note
    });
  }

  // Add not for approve or refuse question
  handleNoteQuestion(value, element) {
    if (value) {
      element.note = value;
    }
  }

  // request approve question
  requestApproveQuestion(ids: number, multiple: boolean): void {
    let checkRequest = true;
    let notShowDialog = false;
    if (!multiple) {
      // Gửi duyệt 1 câu hỏi
      this.arrayIdQuestionApprove = [];
      this.arrayIdQuestionApprove.push(ids);
    } else {
      // Gửi duyệt nhiều câu hỏi
      let arrMultipeId = [];
      if (this.selection.selected.length) {
        this.selection.selected.map(value => {
          arrMultipeId.push(value.id);
        });
      }
      // Check status = 'WAIT' in arr of question
      const arrIdCheck = [];
      if (this.selection.selected) {
        this.selection.selected.map(question => {
          if (question.status === 'WAI' || question.status === 'PUB') {
            arrIdCheck.push(question.id);
          }
        });
      }
      // Show modal khi tồn tại câu đã duyệt
      if (arrIdCheck.length > 0) {
        checkRequest = false;
        const data = {
          message: 'Đã tồn tại những câu hỏi đã duyệt, Bạn có muốn tiếp tục gửi duyệt ?',
          buttonText: { ok: 'Tiếp tục', cancel: 'Huỷ' }
        };
        // Chỉ lọc những câu hỏi không ở trạng thái chờ duyệt và đã duyệt
        this.openConfirmDialog(data).subscribe(check => {
          if (check) {
            arrMultipeId = this.selection.selected.filter(x => x.status != 'WAI' && x.status != 'PUB')?.map(x => x.id);
            if (arrMultipeId) {
              this.handleApproveQuestion(arrMultipeId)
            }
          }
        });
      } else {
        this.arrayIdQuestionApprove = [...new Set(arrMultipeId)];
      }
    }
    if (this.arrayIdQuestionApprove.length > 0 && checkRequest) {
      if (!notShowDialog) {
        const data = {
          message: `Bạn có chắc chắn muốn gửi duyệt ${multiple ? 'các' : ''} câu hỏi này?`,
          buttonText: { ok: 'Gửi duyệt', cancel: 'Huỷ' }
        }
        this.openConfirmDialog(data).subscribe(result => {
          if (result) {
            this.handleApproveQuestion(this.arrayIdQuestionApprove);
          }
        });
      } else {
        this.handleApproveQuestion(this.arrayIdQuestionApprove);
      }
    }
  }

  handleApproveQuestion(data?) {
    if (data.length) {
      this.endpointQuestionBank.requestApproveQuestions(data).then(() => {
        this.showDialogAction('success', 'Gửi duyệt thành công');
        this.getListQuestion(this.paramRequest);
      }).catch(() => this.showDialogAction('err', 'Có lỗi xảy ra'));
    }
  }

  // remove question
  removeQuestion(ids: number, multiple: boolean): void {
    if (!multiple) {
      // Xoá 1 câu hỏi
      this.arrayIdQuestionRemove = [];
      this.arrayIdQuestionRemove.push(ids);
    } else {
      // Xoá nhiều câu hỏi
      let arrMultipeId = [];
      if (this.selection.selected.length) {
        this.selection.selected.forEach(value => {
          arrMultipeId.push(value.id);
        });
      }
      if (this.userIdentity.userTypeRole !== 'SUPER') {
        arrMultipeId = arrMultipeId.filter(res => res.creatorId === this.userIdentity.userId)?.map(x => x.id);
      }
      this.arrayIdQuestionRemove = [...new Set(arrMultipeId)];
    }
    // Xác nhận xoá
    if (this.arrayIdQuestionRemove.length > 0) {

      const dialogRef = this.dialog.open(DeleteDiaLogComponent, {
        disableClose: true,
        width: '444px',
        height: 'auto',
        data: {
          title: 'Xóa câu hỏi',
          name: `${multiple ? 'các' : ''} câu hỏi này`,
          message: ''
        }
      });
  
      dialogRef.afterClosed().subscribe(result => {
        if (result == 'DONGY') {
          this.endpointQuestionBank.deleteQuestion(this.arrayIdQuestionRemove).then((res) => {
            if (res) {
              this._snackBar.openFromComponent(SuccessSnackBarComponent, {
                data: 'Xóa câu hỏi thành công',
                duration: 3000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });
              this.getListQuestion(this.paramRequest);
            this.scrollToView();
            }
            else {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Xóa thất bại',
                duration: 3000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });
            }
          })
            .catch((err) => {
              this._snackBar.openFromComponent(ErrorSnackBarComponent, {
                data: 'Xóa thất bại',
                duration: 3000,
                horizontalPosition: 'center',
                verticalPosition: 'top',
              });
            })
        }
        else {
          if (result != undefined) {
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Bạn nhập chưa đúng, vui lòng thử lại',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
          }
        }
      });

    } else {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Vui lòng chọn câu hỏi để xóa',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    }
  }

  // refuse question
  refuseQuestion(element, multiple: boolean): void {
    if (!multiple) {
      // Chọn một câu hỏi
      this.objDataRefuse = [];
      const data = { id: element.id, approvalNote: element.note ? element.note : '' };
      this.objDataRefuse.push(data);
    } else {
      // Chọn nhiều câu hỏi
      const arrMultipeId = [];
      if (this.selection.selected.length) {
        this.selection.selected.forEach(value => {
          arrMultipeId.push({ id: value.id, approvalNote: value['note'] ? value['note'] : '' });
        });
      }
      this.objDataRefuse = [...new Set(arrMultipeId)];
    }
    // Xác nhận trả lại
    if (this.objDataRefuse.length > 0) {
      const data = {
        message: `Bạn có chắc chắn muốn trả lại ${multiple ? 'các' : ''} câu hỏi này?`,
        buttonText: { ok: 'Trả lại', cancel: 'Huỷ', colorButtonSubmit: 'warn' }
      }
      this.openConfirmDialog(data).subscribe(result => {
        if (result) {
          this.endpointQuestionBank.refuseQuestion(this.objDataRefuse).then(() => {
            let messageConfirm = !multiple ? 'Câu hỏi này đã được trả lại' : 'Đã trả lại';
            this.showDialogAction('success', messageConfirm);
            this.getListQuestion(this.paramRequest);
            this.scrollToView();
          }).catch(() => this.showDialogAction('err', 'Có lỗi xảy ra'));
        }
      });
    } else {
      alert('Vui lòng chọn câu hỏi để trả lại');
    }
  }

  // approve question
  approveQuestion(element, multiple: boolean): void {
    if (!multiple) {
      // Chọn 1 câu hỏi
      this.objDataApprove = [];
      const data = { id: element.id, approvalNote: element.note ? element.note : '' };
      this.objDataApprove.push(data);
    } else {
      // Chọn nhiều câu hỏi
      const arrMultipeId = [];
      if (this.selection.selected.length) {
        this.selection.selected.forEach(value => {
          arrMultipeId.push({ id: value.id, approvalNote: value['note'] ? value['note'] : '' });
        });
      }
      this.objDataApprove = [...new Set(arrMultipeId)];
    }
    // Xác nhận gửi duyệt
    if (this.objDataApprove.length > 0) {
      const data = {
        message: `Bạn có chắc chắn muốn duyệt ${multiple ? 'các' : ''} câu hỏi này?`,
        buttonText: { ok: 'Duyệt', cancel: 'Huỷ' }
      }
      this.openConfirmDialog(data).subscribe(result => {
        if (result) {
          this.endpointQuestionBank.approveQuestions(this.objDataApprove).then(() => {
            this.showDialogAction('success', 'Duyệt thành công');
            this.getListQuestion(this.paramRequest);
          }).catch(() => this.showDialogAction('err', 'Có lỗi xảy ra'));
        }
      });
    } else {
      alert('Vui lòng chọn câu hỏi để duyệt');
    }
  }

  // Dialog xác nhận
  openConfirmDialog(data): Observable<boolean> {
    let confirm: Subject<boolean> = new Subject < boolean > ();
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: data
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        confirm.next(confirmed);
      }
    });
    return confirm.asObservable();
  }

  // Dialog thành công hay thất bại
  showDialogAction(action: string, message: string) {
    const objMessage = {
      data: message,
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    }
    if (action === 'success') {
      this._snackBar.openFromComponent(SuccessSnackBarComponent, objMessage);
    } else {
      this._snackBar.openFromComponent(ErrorSnackBarComponent, objMessage);
    }
  }

  // download file word
  downloadFileWord() {
    this.questionService.downloadFileWord('question_hide');
  }

  createQuestionFromFile(){
    this.dialog.open(ConfirmationDialogComponent, {
      width: '474px',
      data: {
        message: `Tính năng đang được phát triển`,
        buttonText: {
          ok: 'Yes',
          cancel: 'Đóng'
        }
      },
    });
  }

  ngOnDestroy(): void {
    if (this.subjectFilter) {
      this.subjectFilter.unsubscribe();
    }
  }
}

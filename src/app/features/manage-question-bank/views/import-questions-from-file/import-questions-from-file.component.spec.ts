import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportQuestionsFromFileComponent } from './import-questions-from-file.component';

describe('ImportQuestionsFromFileComponent', () => {
  let component: ImportQuestionsFromFileComponent;
  let fixture: ComponentFixture<ImportQuestionsFromFileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImportQuestionsFromFileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportQuestionsFromFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

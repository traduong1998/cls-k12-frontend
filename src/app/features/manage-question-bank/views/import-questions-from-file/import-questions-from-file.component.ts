import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { CLS, QuestionBankEndpoint } from 'sdk/cls-k12-sdk-js/src';
import { ErrorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/ErrorResponse ';
import { ServiceName } from 'sdk/cls-k12-sdk-js/src/services/file-server/requests/module-name';
import { DataQuestionInfoImport } from 'sdk/cls-k12-sdk-js/src/services/question-bank/endpoint/requests/data-question-info-import';
import { QuestionConverterEndpoint } from 'sdk/cls-k12-sdk-js/src/services/question-converter/endpoints/question-converter-endpoint';
import { UploadTestFilesReponse } from 'sdk/cls-k12-sdk-js/src/services/question-converter/responses/upload-test-files-response';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { QuestionModel } from 'sdk/cls-k12-sdk-js/src/services/test/requests/question-model';
import { ConfigUploadCkeditor } from 'src/app/core/interfaces/app-config';
import { AppConfigService, AuthService } from 'src/app/core/services';
import { DialogConfirmComponent } from 'src/app/features/examination-manager/Components/dialog-confirm/dialog-confirm.component';
import { DialogConfirm } from 'src/app/features/examination-manager/interfaces/dialog-confirm';
import { QuestionExam } from 'src/app/features/examination-manager/interfaces/question-exam';
import { ProgressDialogComponent } from 'src/app/shared/progress-dialog/progress-dialog.component';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { ListQuestionService } from '../../services/list-question.service';
import { DialogPreviewQuestionsImportComponent } from './components/dialog-preview-questions-import/dialog-preview-questions-import.component';
import { DialogStatisticQuestionsImportComponent } from './components/dialog-statistic-questions-import/dialog-statistic-questions-import.component';

@Component({
  selector: 'app-import-questions-from-file',
  templateUrl: './import-questions-from-file.component.html',
  styleUrls: ['./import-questions-from-file.component.scss']
})
export class ImportQuestionsFromFileComponent implements OnInit {
  dataQuestionInfoImport: DataQuestionInfoImport;
  // Danh sách câu hỏi trả về sau convert file
  dataResponse: UploadTestFilesReponse;
  //danh sách câu hỏi đc thêm từ file
  data: QuestionExam[] = [];
  isAutoApproveQuestion = false;
  totalCountCurrentTest: number = 0;
  percent: number = 0;
  showInner = false;
  progressData: Observable<any>;
  @ViewChild('file') file: ElementRef;
  //dữ liệu truyền đi
  question: QuestionModel = null;
  // index của câu hỏi con đã chọn
  childIndex;
  //index câu hỏi đã chọn
  index;
  allComplete: boolean = false;
  baseApiUrl = 'http://localhost:65000';
  configUpload: ConfigUploadCkeditor;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  dialogConfirm: DialogConfirm;

  private dialogQuestionBankList: MatDialogRef<DialogPreviewQuestionsImportComponent>;
  private dialogProgressRef: MatDialogRef<ProgressDialogComponent>
  private questionConverterEndpoint: QuestionConverterEndpoint;
  private questionBankEnpoint: QuestionBankEndpoint;

  constructor(private router: Router, private questionService: ListQuestionService, public dialog: MatDialog, private _snackBar: MatSnackBar, private _authService: AuthService, private configService: AppConfigService, private changeDetectorRefs: ChangeDetectorRef) {
    // Set initial question
    this.dataQuestionInfoImport = {
      divisionId: null,
      schoolId: null,
      gradeId: null,
      subjectId: null,
      contentPanelId: null,
      chapterId: null,
      unitId: null
    };
    this.questionConverterEndpoint = new QuestionConverterEndpoint();
    this.questionBankEnpoint = new QuestionBankEndpoint();
    let baseUrl = CLS.getConfig().apiBaseUrls.fileServer ?? CLS.getConfig().apiBaseUrl;
    baseUrl += '/api';

    this.configUpload = {
      portalId: configService.getConfig().unit.portalId,
      token: _authService.getToken(),
      uploadUrl: baseUrl + '/file/upload'
    }

    this.baseApiUrl = CLS.getConfig().apiBaseUrl;
  }

  ngOnInit(): void {
    this.questionService.dataQuestionInfoImport$.subscribe(response => {
      //console.log(response);
      if (response['autoRequestApproval'] !== undefined) {
        this.isAutoApproveQuestion = response['autoRequestApproval'];
      } else {

        Object.keys(response).forEach(res => {
          Object.keys(this.dataQuestionInfoImport).forEach(list => {
            if (list.toLowerCase() === res.toLowerCase()) {
              this.dataQuestionInfoImport[list] = response[res];
            }
          })
        });
      }
    });
  }
  //open file 
  openFile() {
    this.file.nativeElement.click();
  }

  filesSelectedOnChange(files: File[]) {

    if (files.length != 0) {
      this.openProgressDialog(null);
      this.questionConverterEndpoint.uploadTestFiles(files,
        {
          onUploadProgress: (progressEvent) => {
            this.progressData = progressEvent;
            var subscription = this.getprogressData().subscribe(value => {
              if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
                this.dialogProgressRef.componentInstance.data = { numbers: value, isSuccess: false, fileName: files[0].name };
              }
            })
          }
        }, ServiceName.EXAM)
        .then(res => {
          //console.log(`Hoàn thành`);
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: files[0].name };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);

          this.file.nativeElement.value = '';
          this.dataResponse = res;
          this.data = res[0].questions;
          this.countQuestionTest();
        })
        .catch((err) => {
          this.file.nativeElement.value = '';
          this._snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: err.response.data.errorDetail,
            duration: 3000,
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
          });
          this.getprogressData().subscribe(value => {
            if (this.dialogProgressRef && this.dialogProgressRef.componentInstance) {
              this.dialogProgressRef.componentInstance.data = { numbers: 100, isSuccess: true, fileName: files[0].name, catch: true };
            }
          });
          setTimeout(() => {
            this.dialogProgressRef.close();
          }, 2000);
        });
    }
  }

  showStatisticQuestion(data): void {
    let dataQuestions: QuestionModel[] = [];
    data.forEach(obj => {
      if (obj.questions != null && obj.format == FormatQuestion.group) {
        let questions: QuestionModel[] = [];
        obj.questions.forEach(obj => {
          questions.push(
            {
              score: obj.score,
              content: obj.content,
              type: obj.type,
              level: obj.level,
              format: FormatQuestion.group,
              contentPanelId: 0,
              chapterId: 0,
              unitId: 0,
              questions: [],
              answers: obj.answers
            });
        });

        dataQuestions.push(
          {
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: questions,
            answers: []
          });
      }
      else {
        dataQuestions.push(
          {
            score: obj.score,
            content: obj.content,
            type: obj.type,
            level: obj.level,
            format: obj.format,

            contentPanelId: 0,
            chapterId: 0,
            unitId: 0,
            questions: [],
            answers: obj.answers
          });
      }
    })

    const dialogRef = this.dialog.open(DialogStatisticQuestionsImportComponent, {
      width: '800px',
      height: 'auto',
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      //console.log(`Dialog result: ${result}`);
    });
  }

  public getLinkTemplate(cases: number) {
    if (cases == 1) {
      return `${this.baseApiUrl}/exam/public/files/cdn/template-file/tep-tin-mau.docx`;
    }
    else if (cases == 2) {
      return `${this.baseApiUrl}/exam/public/files/cdn/template-file/huong-dan-su-dung-tep-tin-mau.docx`;
    }
  }

  showPreviewQuestions() {
    this.showDialogListQuestion(this.data, 'Xem trước danh sách câu hỏi ');
  }

  // show dialog
  showDialogListQuestion(questions, title) {
    this.dialogQuestionBankList = this.dialog.open(DialogPreviewQuestionsImportComponent, {
      width: '70vw',
      height: 'auto',
      data: {
        title,
        questions
      }
    });
    this.showInner = true;
    this.dialog.afterAllClosed.subscribe(() => this.showInner = false)
  }

  openProgressDialog(value?: any): void {
    this.dialogProgressRef = this.dialog.open(ProgressDialogComponent, {
      width: '340px',
      height: '130px',
      data: { numbers: this.percent }
    });

    this.dialogProgressRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
    });
  }

  getprogressData(): Observable<any> {
    return of(this.progressData);
  }

  countQuestionTest() {
    this.totalCountCurrentTest = this.data.length;
  }

  chooseQuestion(item, i, ic) {
    this.index = i;
    this.question = item;

    if (ic == undefined) {
      this.childIndex = 1;
    } else {
      this.childIndex = ic + 1;
    }

    //console.log('item', item)
  }

  someComplete() {
    if (this.data == undefined) return false;

    let somchecked = false;
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].isChecked) {
        somchecked = true;
        break;
      }
      if (this.data[i].questions != null) {
        for (let j = 0; j < this.data[i].questions.length; j++) {
          if (this.data[i].questions[j].isChecked) {
            somchecked = true;
            break;
          }
        }
      }
    }
    return somchecked;
  }

  updateAllComplete() {
    if (this.data == undefined) return false;
    let allChecked = true;
    for (let i = 0; i < this.data.length; i++) {
      if (!this.data[i].isChecked) {
        allChecked = false;
        break;
      }
      if (this.data[i].questions != null) {
        for (let j = 0; j < this.data[i].questions.length; j++) {
          if (!this.data[i].questions[j].isChecked) {
            allChecked = false;
            break;
          }
        }
      }
    }
    this.allComplete = allChecked;
  }

  setAll(completed: boolean): void {
    this.allComplete = completed;
    if (this.data != null) {
      this.data.map((question) => {
        if (question.format == FormatQuestion.group) {
          question.isChecked = completed;
          question.questions.forEach((child) => {
            child.isChecked = completed
          });
        } else {
          question.isChecked = completed
        }
      });
    }
  }

  deleteSelected() {
    this.dialogConfirm = { title: 'XÓA CÂU HỎI', description: 'Bạn có chắc chắn muốn xóa các câu hỏi đã chọn?' }
    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        // Xóa câu con trước
        this.data.forEach((qs, i) => {
          if (qs.format == FormatQuestion.group) {
            // update la
            // lọc bỏ những câu hỏi con
            qs.questions = qs.questions.filter(x => !x.isChecked);
          }
          // reset câu hỏi đã chọn
          if (this.index == i) {
            this.question = null;
          }
        });

        this.data = this.data.filter(x => !x.isChecked);
        this.countQuestionTest();
        this.loadData(this.data);
      }
    });

  }

  delete(element, i, ic) {
    this.dialogConfirm = { title: 'XÓA CÂU HỎI', description: 'Bạn có chắc chắn muốn xóa câu hỏi này?' }
    const dialogRef = this.dialog.open(DialogConfirmComponent, { data: this.dialogConfirm });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {

        // là câu chùm, đang xóa câu con của câu chùm
        if (i != undefined) {
          if (this.data[i].questions.length == 1) {
            // cầu chùm phải có ít nhất 1 câu con
            this._snackBar.openFromComponent(ErrorSnackBarComponent, {
              data: 'Câu chùm phải có ít nhất 1 câu con!',
              duration: 3000,
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
            });
            return;
          } else {
            this.data[i].questions.splice(ic, 1);
          }

          // reset câu hỏi đã chọn
          if (this.index == i) {
            this.question = null;
          }
        }
        else {
          let index = this.data.indexOf(element);
          if (index != -1) {
            this.data.splice(index, 1);
          }

          // reset câu hỏi đã chọn
          if (this.index == index) {
            this.question = null;
          }
        }

        this.loadData(this.data);

        // thống kê lại số lượng câu hỏi, tổng điểm
        this.countQuestionTest();
      }
    });
  }

  loadData(data) {
    this.data = [];
    this.data = [...this.data, ...data];
    this.countQuestionTest();
    this.changeDetectorRefs.detectChanges();
  }
  updateQuestion(question) {
    this.data[this.index] = question;
    this.loadData(this.data);
  }
  // Check Grade field and Subject field
  onAddQuestionsClicked(): void {
    if (this.checkRequireField()) {
      this.addQuestions();
    }
  }
  // Tạo câu hỏi cần chọn khối và môn
  checkRequireField() {
    if (this.data.length == 0) {
      return;
    }
    if (!this.dataQuestionInfoImport.gradeId) {
      alert('Cần chọn khối để tạo');
      return null;
    } else if (!this.dataQuestionInfoImport.subjectId) {
      alert('Cần chọn môn học để tạo');
      return null;
    } else {
      // Nếu đã chọn khung chương trình thì phải chọn bài
      if (this.dataQuestionInfoImport.contentPanelId) {
        if (!this.dataQuestionInfoImport.chapterId || !this.dataQuestionInfoImport.unitId) {
          alert('Nếu đã chọn khung thì phải chọn chương và chọn bài');
          return null;
        } else {
          return this.dataQuestionInfoImport;
        }
      } else {
        return this.dataQuestionInfoImport;
      }
    }
  }
  // Emit from output and handle call api
  addQuestions(): void {
    let dataRequest = {
      questions: this.data,
      autoRequestApproval: this.isAutoApproveQuestion,
      divisionId: this.dataQuestionInfoImport.divisionId,
      schoolId: this.dataQuestionInfoImport.schoolId,
      gradeId: this.dataQuestionInfoImport.gradeId,
      subjectId: this.dataQuestionInfoImport.subjectId,
      contentPanelId: this.dataQuestionInfoImport.contentPanelId,
      chapterId: this.dataQuestionInfoImport.chapterId,
      unitId: this.dataQuestionInfoImport.unitId,
    }
    this.questionBankEnpoint.importQuestions(dataRequest).then(() => {
      this._snackBar.openFromComponent(SuccessSnackBarComponent, {
        data: 'Thêm câu hỏi thành công',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
      this.router.navigate(['/dashboard/question-bank']);
    }).catch((err) => console.log(err));
  }
}

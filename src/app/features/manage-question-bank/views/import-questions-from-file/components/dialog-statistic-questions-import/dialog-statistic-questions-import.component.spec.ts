import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogStatisticQuestionsImportComponent } from './dialog-statistic-questions-import.component';

describe('DialogStatisticQuestionsImportComponent', () => {
  let component: DialogStatisticQuestionsImportComponent;
  let fixture: ComponentFixture<DialogStatisticQuestionsImportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogStatisticQuestionsImportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogStatisticQuestionsImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

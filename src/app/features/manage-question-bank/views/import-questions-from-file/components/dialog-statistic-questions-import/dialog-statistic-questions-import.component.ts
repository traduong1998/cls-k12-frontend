import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, Inject, Type } from '@angular/core';
import { StatisticQuestion } from '../../interfaces/statistic-question';
import { Tyle } from '../../interfaces/enums/tyle';
import { Level } from '../../interfaces/enums/level';

@Component({
  selector: 'app-dialog-statistic-questions-import',
  templateUrl: './dialog-statistic-questions-import.component.html',
  styleUrls: ['./dialog-statistic-questions-import.component.scss']
})
export class DialogStatisticQuestionsImportComponent implements OnInit {

  totalQuestion: number;

  // biến đếm
  quantityGroup: number = 0;

  singleNB: number = 0;
  singleTH: number = 0;
  singleVD: number = 0;
  singleVDC: number = 0;

  listStatisticQuestion: StatisticQuestion[] = [];
  displayedColumns: string[] = ['stt', 'type', 'level', 'numberQuestion'];
  constructor(@Inject(MAT_DIALOG_DATA) public data) { }

  ngOnInit(): void {
    //console.log("data dia log", this.data)
    //tổng số câu hỏi
    this.totalQuestion = this.data.length;
    this.statisticQuestion();
  }


  statisticQuestion() {
    //    console.log(this.data);
    this.data.forEach(item => {
      //tổng điểm
      if (item.format == "SIG") {
        if (item.level == "NB") {
          this.singleNB++;
        } else if (item.level == "TH") {
          this.singleTH++
        } else if (item.level == "VD") {
          this.singleVD++;
        } else {
          this.singleVDC++;
        }
      } else {
        this.quantityGroup++;
      }
    });

    if (this.singleNB != 0) {
      this.listStatisticQuestion.push({ chapter: null, lesson: null, level: Level.NB, quantity: this.singleNB, type: Tyle.Single })
    }
    if (this.singleTH != 0) {
      this.listStatisticQuestion.push({ chapter: null, lesson: null, level: Level.TH, quantity: this.singleTH, type: Tyle.Single })
    }
    if (this.singleVD != 0) {
      this.listStatisticQuestion.push({ chapter: null, lesson: null, level: Level.VD, quantity: this.singleVD, type: Tyle.Single })
    }
    if (this.singleVDC != 0) {
      this.listStatisticQuestion.push({ chapter: null, lesson: null, level: Level.VDC, quantity: this.singleVDC, type: Tyle.Single })
    }
    if (this.quantityGroup != 0) {
      this.listStatisticQuestion.push({ chapter: null, lesson: null, level: Level.DMD, quantity: this.quantityGroup, type: Tyle.Group })
    }

    //console.log(this.listStatisticQuestion);
  }
}

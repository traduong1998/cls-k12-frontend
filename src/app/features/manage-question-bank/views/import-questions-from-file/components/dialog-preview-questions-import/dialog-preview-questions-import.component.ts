import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TestEnpoint } from 'sdk/cls-k12-sdk-js/src/services/test/enpoint/test-enpoint';

@Component({
  selector: 'app-dialog-preview-questions-import',
  templateUrl: './dialog-preview-questions-import.component.html',
  styleUrls: ['./dialog-preview-questions-import.component.scss']
})
export class DialogPreviewQuestionsImportComponent implements OnInit {
  private testEnpoint: TestEnpoint;
  listQuestionFromQuestionBank;
  totalQuestion: number = 0;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.testEnpoint = new TestEnpoint();
  }

  ngOnInit(): void {
    this.listQuestionFromQuestionBank = { questions: this.data.questions };
    this.totalQuestion = this.listQuestionFromQuestionBank.questions.length;
  }

}

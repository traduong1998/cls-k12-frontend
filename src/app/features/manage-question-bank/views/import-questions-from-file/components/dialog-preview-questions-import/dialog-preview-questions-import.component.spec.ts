import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPreviewQuestionsImportComponent } from './dialog-preview-questions-import.component';

describe('DialogPreviewQuestionsImportComponent', () => {
  let component: DialogPreviewQuestionsImportComponent;
  let fixture: ComponentFixture<DialogPreviewQuestionsImportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogPreviewQuestionsImportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPreviewQuestionsImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

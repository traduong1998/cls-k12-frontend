import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionInfoSelectedComponent } from './question-info-selected.component';

describe('QuestionInfoSelectedComponent', () => {
  let component: QuestionInfoSelectedComponent;
  let fixture: ComponentFixture<QuestionInfoSelectedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionInfoSelectedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionInfoSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

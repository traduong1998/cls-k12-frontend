
import { MatCheckboxChange } from '@angular/material/checkbox';
import { ListQuestionService } from '../../../../services/list-question.service';
import { CurriculumEndpoint } from 'sdk/cls-k12-sdk-js/src/services/curriculum/endpoint/curriculum-endpoint';
import { SubjectsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/lesson/endpoints/subjectsEndpoint';
import { GradeOption } from 'sdk/cls-k12-sdk-js/src/services/grade/models/Grade';
import { DivisionEndpoint, SchoolEndpoint, GradeEndpoint, UserIdentity, LevelManages, QuestionBankEndpoint, SubjectEndpoint } from 'cls-k12-sdk-js/src';
import { FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DivisionOption } from 'sdk/cls-k12-sdk-js/src/services/division/models/Division';
import { SchoolOption } from 'sdk/cls-k12-sdk-js/src/services/school/models/School';
import { Subject } from 'rxjs';
import { SubjectFilter } from '../../../../intefaces/subject';
import { pairwise, startWith } from 'rxjs/operators';
import { CurriculumsFilterRequests } from 'sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum-filter-request';
import { Curriculum } from 'sdk/cls-k12-sdk-js/src/services/curriculum/models/curriculum';
import { Chapter } from '../../../../intefaces/chapter';
import { Exerciser } from '../../../../intefaces/exerciser';
import { AuthService } from 'src/app/core/services';


@Component({
  selector: 'app-question-info-selected',
  templateUrl: './question-info-selected.component.html',
  styleUrls: ['./question-info-selected.component.scss']
})
export class QuestionInfoSelectedComponent implements OnInit {

  // Input
  @Input() dataEdit = {};
  @Input() dataCreate = {};

  // init form
  filterQuestionForm = this.fb.group({
    GradeId: new FormControl(''),
    SchoolId: new FormControl(''),
    SubjectId: new FormControl(''),
    ContentPanelId: new FormControl(),
    ChapterId: new FormControl(),
    UnitId: new FormControl()
  });

  /* loading sniper */
  isLoadingResults = false;

  baseApiUrl = '';
  autoConfirm;
  checkedAuto = false;

  // List opitons of select option
  public filteredDivisions: DivisionOption[];
  public filteredSchools: SchoolOption[];
  public filteredGrade: GradeOption[];
  public filteredSubjects: SubjectFilter[];

  public filteredFrames: Curriculum[];
  public filteredChapters: Chapter[];
  public filteredExerciser: Exerciser[];

  // First load variable
  isFirstLoadDivision = true;
  isFirstLoadSchool = true;
  isFirstLoadGrade = true;
  isFirstLoadSubject = true;
  isFirstLoadFrames = true;
  isFirstLoadChapter = true;
  isFirstLoadExerciser = true;

  // Endpoint
  endpointDivision: DivisionEndpoint;
  endpointSchool: SchoolEndpoint;
  endpointGrade: GradeEndpoint;
  endpointSubject: SubjectsEndpoint;
  endpointContentPanel: CurriculumEndpoint;
  userIdentity: UserIdentity;
  endpointQuestionBank: QuestionBankEndpoint;
  private subjectEndpoint: SubjectEndpoint;


  // Get value of field in form
  get schoolId() { return this.filterQuestionForm.get('SchoolId'); }
  get gradeId() { return this.filterQuestionForm.get('GradeId'); }
  get subjectId() { return this.filterQuestionForm.get('SubjectId'); }
  get framesId() { return this.filterQuestionForm.get('ContentPanelId'); }
  get chapterId() { return this.filterQuestionForm.get('ChapterId'); }
  get exerciseId() { return this.filterQuestionForm.get('UnitId'); }

  // form control
  public schoolFilterCtrl: FormControl = new FormControl();
  public gradeFilterCtrl: FormControl = new FormControl();
  public subjectFilterCtrl: FormControl = new FormControl();
  public framesFilterCtrl: FormControl = new FormControl();
  public chapterFilterCtrl: FormControl = new FormControl();
  public exerciseFilterCtrl: FormControl = new FormControl();

  protected _onDestroy = new Subject < void> ();

  /* Disable property */
  isDisableSchoolId = false;
  isDisableGradeId = false;
  isDisableSubjecId = false;
  isDisableSubjectId = true;
  isDisableFramesId = true;
  isDisableChapterId = true;
  isDisableExerciseId = true;


  constructor(private fb: FormBuilder,
    private _authService: AuthService, private questionService: ListQuestionService) {
    this.endpointDivision = new DivisionEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSchool = new SchoolEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointGrade = new GradeEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointSubject = new SubjectsEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointContentPanel = new CurriculumEndpoint({ baseUrl: this.baseApiUrl });
    this.endpointQuestionBank = new QuestionBankEndpoint({ baseUrl: this.baseApiUrl });
    this.userIdentity = _authService.getTokenInfo();
    this.subjectEndpoint = new SubjectEndpoint({ baseUrl: 'http://localhost:65000' });

    if (this.userIdentity.levelManage == LevelManages.School) {
      // Tài khoản cấp Trường, schoolid fillter mặc định theo  id trường của tài khoản hiện tại
      this.filterQuestionForm.controls['SchoolId'].setValue(this.userIdentity.schoolId);
    }
  }

  ngOnInit(): void {

  }

  // Emit input change
  ngOnChanges(changes: SimpleChanges): void {
    // Edit question
    if (changes.dataEdit?.currentValue) {
      const listDataFilter = changes.dataEdit.currentValue;
      if (listDataFilter.gradeId) {
        this.getNameGradeField(listDataFilter.gradeId)
      }
      // Nếu status của câu hỏi là đang chờ duyệt thì checked tự động gửi duyệt
      if (listDataFilter.status === 'WAI') {
        this.checkedAuto = true
      }
      if (listDataFilter.subjectId) {
        this.getNameSubjecField(listDataFilter.subjectId)
      }
      if (listDataFilter.gradeId && listDataFilter.subjectId) {
        this.getContentPanelName(listDataFilter.subjectId, listDataFilter.gradeId);
      }
      const valueOfDataFilter = Object.values(listDataFilter);
      Object.keys(this.filterQuestionForm.value).forEach(res => {
        Object.keys(listDataFilter).forEach((list, index) => {
          if (list.toLowerCase() === res.toLowerCase()) {
            this.filterQuestionForm.controls[res].setValue(valueOfDataFilter[index]);
          }
        })
      });

    }
    // Create question
    if (changes.dataCreate?.currentValue) {
      const listDataCreate = changes.dataCreate.currentValue;
      const valueOfDataFilter = Object.values(listDataCreate);
      Object.keys(this.filterQuestionForm.value).forEach(res => {
        Object.keys(listDataCreate).forEach((list, index) => {
          if (list.toLowerCase() === res.toLowerCase()) {
            this.filterQuestionForm.controls[res].setValue(valueOfDataFilter[index]);
          }
        })
      });
    }
  }

  getContentPanelName(subjectId, gradeId) {
    this.endpointContentPanel.getCurriculumsBySubjectGrade(subjectId, gradeId).then(res => {
      if (res) {
        const contenPanel = this.filterQuestionForm.value.ContentPanelId;
        const chapter = this.filterQuestionForm.value.ChapterId;
        const unit = this.filterQuestionForm.value.UnitId;
        if (contenPanel) {
          const filterNameContentPanel = res.filter(name => name.curriculumId === contenPanel);
          this.filteredFrames = [{
            curriculumId: filterNameContentPanel[0]['curriculumId'],
            curriculumName: filterNameContentPanel[0]['curriculumName'],
            gradeName: '', subjectName: '',
            isActived: true, getActiveStatus: '',
            levelManageName: ''
          }]
        }
        if (chapter) {
          const filterNameChapter = res.filter(name => name.curriculumId === chapter);
          this.filteredChapters = [{ id: filterNameChapter[0]['curriculumId'], curriculumName: filterNameChapter[0]['curriculumName'] }];
        }
        if (unit) {
          const filterNameUnit = res.filter(name => name.curriculumId === unit);
          this.filteredExerciser = [{ id: filterNameUnit[0]['curriculumId'], curriculumName: filterNameUnit[0]['curriculumName'] }];
        }
      }
    }).catch(err => console.log(err));
  }

  // Get grade name
  getNameGradeField(id) {
    this.endpointGrade.getGradeInfo(id).then(res => {
      if (res) {
        this.filteredGrade = [{ id: id, name: res['gradeName'] }];
      }
    })
  }

  // Get subject name
  getNameSubjecField(id) {
    this.subjectEndpoint.getSubjectInfo(id).then(res => {
      this.filteredSubjects = [{ id: id, name: res.name }];
    })
  }

  ngAfterViewInit() {
    this.gradeId.valueChanges.pipe(startWith(this.gradeId.value), pairwise()).subscribe(
      ([old, value]) => {
        if (old !== value && !(old == null && value == "")) {
          this.onResetSubjectSelect();
          this.onResetFramesSelect();
          this.onResetChapterSelect();
          if (value < 1) {
            this.isDisableSubjectId = true;
            this.isDisableFramesId = true;
            this.isDisableChapterId = true;
            this.isDisableExerciseId = true;
          } else {
            this.isDisableSubjectId = false;
          }
          this.emitDataFormEdit();
        }
      }
    );
    this.subjectId.valueChanges.pipe(startWith(this.subjectId.value), pairwise()).subscribe(
      ([old, value]) => {
        if (old !== value && !(old == null && value == "")) {
          this.onResetFramesSelect();
          if (value < 1) {
            this.isDisableFramesId = true;
          } else {
            this.isDisableFramesId = false;
          }
          this.emitDataFormEdit();
          this.isDisableChapterId = true;
        }
      }
    );
    this.framesId.valueChanges.pipe(startWith(this.framesId.value), pairwise()).subscribe(
      ([old, value]) => {
        if (old !== value && !(old == null && value == "")) {
          this.onResetChapterSelect();
          if (value < 1) {
            this.isDisableChapterId = true;
          } else {
            this.isDisableChapterId = false;
          }
          this.emitDataFormEdit();
        }
      }
    );
    this.chapterId.valueChanges.pipe(startWith(this.chapterId.value), pairwise()).subscribe(
      ([old, value]) => {
        if (old !== value && !(old == null && value == "")) {
          this.onResetExerciseSelect();
          if (value < 1) {
            this.isDisableExerciseId = true;
          } else {
            this.isDisableExerciseId = false;
          }
          this.emitDataFormEdit();
        }
      }
    );

    this.exerciseId.valueChanges.pipe(startWith(this.exerciseId.value), pairwise()).subscribe(
      ([old, value]) => {
        if (old !== value && !(old == null && value == "")) {
          this.questionService.emitDataQuestionInfoImport.next(this.filterQuestionForm.value);
          this.emitDataFormEdit();
        }
      }
    );
  }

  /* Get grade */
  onGradeSelectClicked(): void {
    var schoolIdSelected = this.schoolId.value;
    if (this.userIdentity.levelManage == "SCH") {
      schoolIdSelected = this.userIdentity.schoolId;
    }
    if (this.isFirstLoadGrade) {
      this.endpointGrade.getGradeOptions(schoolIdSelected).then(res => {
        this.isFirstLoadGrade = false;
        this.filteredGrade = [{ id: null, name: 'Chọn khối' }, ...res];
      }).catch(err => console.log(err));
    } else { }
  }

  /* Get subject */
  onSubjectSelectClicked(): void {
    const gradeSelected = this.gradeId.value;
    if (gradeSelected && this.isFirstLoadSubject) {
      this.endpointSubject.getSubjectByGradeId(gradeSelected ? gradeSelected : null).then(res => {
        this.isFirstLoadSubject = false;
        this.isDisableFramesId = false;
        this.filteredSubjects = [{ id: null, name: 'Chọn môn' }, ...res.listSubjectOfGrade];
      }).catch(err => console.log(err));
    }
  }

  /* Get Frames */
  onFramesSelectClicked(): void {
    let value = new CurriculumsFilterRequests();
    value.gradeId = this.gradeId.value;
    value.subjectId = this.subjectId.value;
    // value.isActive = true;
    value.divisionId = null;
    value.schoolId = null;
    if (value.gradeId && value.subjectId && this.isFirstLoadFrames) {
      // this.endpointContentPanel.getCurriculumsPaging(value).then(res => {
      this.endpointContentPanel.getCurriculumsFilterQuestionBank(value, true, null, null).then(res => {
        this.isFirstLoadFrames = false;
        this.filteredFrames = [{
          curriculumId: null, curriculumName: 'Chọn khung', gradeName: '',
          isActived: false, subjectName: '', getActiveStatus: '',
          levelManageName: ''
        }, ...res];
        if (this.filteredFrames.length > 1) {
          this.isDisableChapterId = false;
        }
      }).catch(err => console.log(err));
    }
  }

  /* Get Chapters */
  onChapterSelectClicked(): void {
    if (this.framesId.value > 0 && this.isFirstLoadChapter) {
      this.endpointContentPanel.getCurriculumChildrentNodes(this.framesId.value).then((res) => {
        this.isFirstLoadChapter = false;
        this.filteredChapters = [{ id: null, curriculumName: 'Chọn chương' }, ...res];
        if (this.filteredChapters.length > 1) {
          this.isDisableExerciseId = false;
        }
      }).catch(err => console.log(err));
    }
  }

  /* Get Exercise */
  onExerciseSelectClicked(): void {
    if (this.isFirstLoadExerciser && !this.isDisableExerciseId) {
      this.endpointContentPanel.getCurriculumChildrentNodes(this.chapterId.value).then((res) => {
        this.isFirstLoadExerciser = false;
        this.filteredExerciser = [{ id: null, curriculumName: 'Chọn bài' }, ...res];
      }).catch(err => console.log(err));
    }
  }

  // Emit value from checkbox auto confirm question
  changeAutoConfirm(value: MatCheckboxChange): void {
    this.questionService.emitDataQuestionInfoImport.next({ autoRequestApproval: value.checked });
  }

  /* reset change Subject */
  private onResetSubjectSelect(): void {
    this.filterQuestionForm.controls['SubjectId'].setValue('');
    this.isFirstLoadSubject = true;
    this.isDisableSubjectId = true;
  }

  /* reset change Frames */
  private onResetFramesSelect(): void {
    this.filterQuestionForm.controls['ContentPanelId'].setValue('');
    this.isFirstLoadFrames = true;
  }

  /* reset change Chapter */
  private onResetChapterSelect(): void {
    this.filterQuestionForm.controls['ChapterId'].setValue('');
    this.isFirstLoadChapter = true;
  }

  /* reset change Chapter */
  private onResetExerciseSelect(): void {
    this.filterQuestionForm.controls['UnitId'].setValue('');
    this.isFirstLoadExerciser = true;
  }

  emitDataFormEdit() {
    setTimeout(() => {
      this.questionService.emitDataQuestionInfoImport.next(this.filterQuestionForm.value);
    }, 0);
  }
}

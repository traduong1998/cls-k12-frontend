import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { Lesson } from './interfaces/lesson';
import { RegistStudentEndpoint } from 'sdk/cls-k12-sdk-js/src/services/regist-student-class/endpoints/regist-student-class-endpoint';
import { PaginatorResponse } from 'sdk/cls-k12-sdk-js/src/core/api/responses/PaginatorResponse';
import { RegistStudentResponse } from 'sdk/cls-k12-sdk-js/src/services/regist-student-class/models/regist-student';
import { SuccessSnackBarComponent } from 'src/app/shared/snack-bar/success-snack-bar/success-snack-bar.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as moment from 'moment';
import { MeetingRoomStudentLogsEndpoint } from 'sdk/cls-k12-sdk-js/src/services/meeting-room-student-logs/end-points/meeting-room-student-logs-endpoint';
import { SETTING_LOG } from 'src/app/shared/configurations';
import { ErrorSnackBarComponent } from 'src/app/shared/snack-bar/error-snack-bar/error-snack-bar.component';

export interface ClickedRoom {
  id: number
  lastClicked: number
}

@Component({
  selector: 'app-interactive-class',
  templateUrl: './interactive-class.component.html',
  styleUrls: ['./interactive-class.component.scss']
})
export class InteractiveClassComponent implements OnInit {
  registStudentEndpoint: RegistStudentEndpoint;
  studentLogsEndpoint: MeetingRoomStudentLogsEndpoint
  baseApiUrl = '';
  isShowFilter = true;
  isFirstLog = true;
  examFilter = this.fb.group({
    TypeClass: new FormControl(''),
    Status: new FormControl(''),
    TimeStart: new FormControl(''),
    TimeEnd: new FormControl(''),
    Keyword: new FormControl(''),
    Page: 1,
    Size: 10,
    getCount: true
  })
  clickedRooms: ClickedRoom[] = [];
  prevMeetingRoomId: number = 0;
  startLog: number = 0;
  totalLength: number = 0;
  classPagination: PaginatorResponse<RegistStudentResponse>;

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {
    this.registStudentEndpoint = new RegistStudentEndpoint({ baseUrl: this.baseApiUrl });
    this.studentLogsEndpoint = new MeetingRoomStudentLogsEndpoint();
  }

  ngOnInit(): void {
    this.getListClass();
  }

  getListClass(data?) {
    this.registStudentEndpoint.getListMeetingRoomsUser(this.examFilter.value).then(res => {
      console.log(res);
      this.classPagination = res;
      this.classPagination.items.forEach(value => {
        value.startTimeConvert = moment.utc(value.startTime).local().lang("vi");
        value.endTimeConvert = moment.utc(value.endTime).local().lang("vi");
      });
      this.totalLength = res.totalItems;
    }, err => console.log(err))
  }

  toggleFilter(): void {
    this.isShowFilter = !this.isShowFilter;
  }

  onSubmit(): void {
    this.getListClass();
  }

  clearDate(field: string): void {
    if (field === 'create') {
      this.examFilter.controls.CreateDate.setValue('');
    } else {
      this.examFilter.controls.ModifiedDate.setValue('');
    }
  }

  onCopy(text): void {
    navigator.clipboard.writeText(text).then(() => {
      this.snackBar.openFromComponent(SuccessSnackBarComponent, {
        data: 'Đã sao chép',
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
    }, function (err) {
      console.error(err);
    });
  }

  pageChange(page: number) {
    this.examFilter.value.Page = page;
    this.getListClass()
  }

  onJoinClass(meetingRoomId: number, link: string): void {
    let clickedRoom = this.clickedRooms.find(x => x.id == meetingRoomId);
    if (!clickedRoom) {
      this.studentLogsEndpoint.create(meetingRoomId).then(res => {
        this.clickedRooms.push({ id: meetingRoomId, lastClicked: new Date(res.lastJoinDate).getTime() });
        window.open(link, '_blank');
      }).catch((err) => {
        this.snackBar.openFromComponent(ErrorSnackBarComponent, {
          data: 'Điểm danh không thành công',
          duration: 3000,
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
      });
      this.isFirstLog = false;
    }
    else {
      if ((new Date().getTime() - clickedRoom.lastClicked) > SETTING_LOG) {
        this.studentLogsEndpoint.create(meetingRoomId).then(res => {
          this.startLog = new Date().getTime();
          clickedRoom.lastClicked = new Date().getTime();
          window.open(link, '_blank');
        }).catch((err) => {
          this.snackBar.openFromComponent(ErrorSnackBarComponent, {
            data: 'Điểm danh không thành công',
            duration: 3000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
          });
        });
      } else {
        window.open(link, '_blank');
      }
    }
  }
}

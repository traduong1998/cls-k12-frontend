import { NgModule } from '@angular/core';
import { InteractiveClassComponent } from './interactive-class.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards';
import {NgxPaginationModule} from 'ngx-pagination';

const StarterRoutes: Routes = [
  {
    path: '',
    component: InteractiveClassComponent,//admin
    canActivate: [AuthGuard],
    data: {
      title: 'Lớp học tương tác'
    }
  },
];

@NgModule({
  declarations: [
    InteractiveClassComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(StarterRoutes),
    NgxPaginationModule
  ]
})
export class InteractiveClassModule { }

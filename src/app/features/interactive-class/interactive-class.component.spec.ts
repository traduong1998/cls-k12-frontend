import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InteractiveClassComponent } from './interactive-class.component';

describe('InteractiveClassComponent', () => {
  let component: InteractiveClassComponent;
  let fixture: ComponentFixture<InteractiveClassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InteractiveClassComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InteractiveClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

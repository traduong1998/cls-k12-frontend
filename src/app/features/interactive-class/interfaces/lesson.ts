
  export interface Lesson {
    id?: number;
    name?: string;
    type?: number;
    avatarTeacher?: string;
    nameTeacher?: string;
    status?: string;
    startTime?: string;
    endTime?: string;
    linkClass?: string
  }
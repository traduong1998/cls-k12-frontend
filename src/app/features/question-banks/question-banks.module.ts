import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionBanksComponent } from './question-banks.component'
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { QuestionBanksRoutingModule } from './question-banks-routing.module';
import { CKEditorModule } from 'ckeditor4-angular';
import { FormsModule } from '@angular/forms';
//import angular material
import { MaterialModule } from '../../shared/material/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CreateQuestionComponent } from './views/create-question/create-question.view';
import { HeaderQuestionBankComponent } from './components/header-question-bank/header-create-question.component';
import { ContentComponent } from './components/content/content.component';
import { ContentListQuestionComponent } from './components/content-list-question/content-list-question.component'
import { TypeQuestionComponent } from './components/type-question/type-question.component';
import { CreateSingleChoiceQuestionComponent } from './components/create-single-choice-question/create-single-choice-question.component';
import { CreateMultipleChoiceQuestionComponent } from './components/create-multiple-choice-question/create-multiple-choice-question.component';
import { CreateTrueFalseQuestionComponent } from './components/create-true-false-question/create-true-false-question.component';
import { CreateTrueFalseClauseQuestionComponent } from './components/create-true-false-clause-question/create-true-false-clause-question.component';
import { CreateEssayQuestionComponent } from './components/create-essay-question/create-essay-question.component';
import { CreateUnderLineQuestionComponent } from './components/create-under-line-question/create-under-line-question.component';
import { CreateMissingWord1QuestionComponent } from './components/create-missing-word1-question/create-missing-word1-question.component';
import { CreateMissingWord2QuestionComponent } from './components/create-missing-word2-question/create-missing-word2-question.component';
import { CreateMatchingQuestionComponent } from './components/create-matching-question/create-matching-question.component';
import { GeneralQuestionComponent } from './components/general-question/general-question.component';
import { CreateGroupQuestionComponent } from './components/create-group-question/create-group-question.component';
import { FooterQuestionComponent } from './components/footer-question/footer-question.component';
import { ListQuestionComponent } from './views/list-question/list-question.component'
//view list
import { SingleChoiceComponent } from './components/single-choice/single-choice.component'
import { SafeHtmlPipe } from 'src/app/shared/pipes/SafeHtmlPipe.pipe';
import { MultiChoiceComponent } from './components/multi-choice/multi-choice.component';
import { TrueFalseComponent } from './components/true-false/true-false.component';
import { TrueFalseClauseComponent } from './components/true-false-clause/true-false-clause.component';
import { EssayComponent } from './components/essay/essay.component';
import { MissingWord1Component } from './components/missing-word1/missing-word1.component';
import { MissingWord2Component } from './components/missing-word2/missing-word2.component';
import { UnderLineComponent } from './components/under-line/under-line.component';
import { GroupQuestionComponent } from './components/group-question/group-question.component';
import { MatchingComponent } from './components/matching/matching.component';
import { EditQuestionComponent } from './components/edit-question/edit-question.component';
import { LoadingScreenComponent } from '../loading-screen/loading-screen.component';
import { MathModule } from 'src/app/shared/math/math.module';
import { ExportWordComponent } from './views/export-word/export-word.component';
import { ContentFileWordComponent } from './components/content-file-word/content-file-word.component';

@NgModule({
  declarations: [
    QuestionBanksComponent,
    CreateQuestionComponent,
    ListQuestionComponent,
    HeaderQuestionBankComponent,
    ContentComponent,
    ContentListQuestionComponent,
    TypeQuestionComponent,
    CreateSingleChoiceQuestionComponent,
    CreateMultipleChoiceQuestionComponent,
    CreateTrueFalseQuestionComponent,
    CreateTrueFalseClauseQuestionComponent,
    CreateEssayQuestionComponent,
    CreateUnderLineQuestionComponent,
    CreateMissingWord1QuestionComponent,
    CreateMissingWord2QuestionComponent,
    CreateMatchingQuestionComponent,
    GeneralQuestionComponent,
    CreateGroupQuestionComponent,
    FooterQuestionComponent,
    SingleChoiceComponent,
    MultiChoiceComponent,
    TrueFalseComponent,
    TrueFalseClauseComponent,
    EssayComponent,
    MissingWord1Component,
    MissingWord2Component,
    UnderLineComponent,
    GroupQuestionComponent,
    MatchingComponent,
    EditQuestionComponent,
    ExportWordComponent,
    ContentFileWordComponent,
    //LoadingScreenComponent
    //SafeHtmlPipe
  ],
  entryComponents: [EditQuestionComponent],
  imports: [
    SharedModule,
    QuestionBanksRoutingModule,
    CKEditorModule, FormsModule,
    MathModule
  ]
})
export class QuestionBanksModule { }

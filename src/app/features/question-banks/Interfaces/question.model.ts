export interface Question{
    id: string;
    Content: string;
    type:string;
    isAutoApproval?:boolean;
    level:string;
}
export interface QuestionGroup{
    id: string;
    globalContent:string;
    isShuffleChild:boolean;
    Questions: Question[] ;
}
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionBanksComponent } from './question-banks.component'
import { CreateQuestionComponent } from './views/create-question/create-question.view';
import { ListQuestionComponent } from './views/list-question/list-question.component'
import { ExportWordComponent } from './views/export-word/export-word.component';
import { AuthGuard } from 'src/app/core/guards';

const routes: Routes = [
  {
    path: '',
    component: QuestionBanksComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: ListQuestionComponent,
        data: {
          title: 'Danh sách câu hỏi',
          urls: [
            { title: 'Dashboard', url: '/dashboard/learn' },
            { title: 'Danh sách câu hỏi' }
          ]
        }
      },
      {
        path: 'create',
        component: CreateQuestionComponent,
        data: {
          title: 'Tạo câu hỏi',
          urls: [
            { title: 'Dashboard', url: '/dashboard' },
            { title: 'Danh sách câu hỏi', url: '/questionbanks' },
            { title: 'Tạo câu hỏi' }
          ]
        }
      },
      {
        path: 'export-word',
        component: ExportWordComponent
      },
    ],

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class QuestionBanksRoutingModule { }

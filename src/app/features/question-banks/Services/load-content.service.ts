import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadContentService {

  private _formatOfQuestion= new Subject<string>();
  type$ = this._formatOfQuestion.asObservable();

  private _typeOfQuestion= new Subject<string>();
  typeQuestion$ = this._typeOfQuestion.asObservable();

  private _levelfQuestion= new Subject<string>();
  levelfQuestion$ = this._levelfQuestion.asObservable();

  private _isAutoApproval = new Subject<boolean>();
  isAutoApproval$ = this._isAutoApproval.asObservable();

  constructor() { }

  getFormatOfQuestion(type:string){
    this._formatOfQuestion.next(type);
  }

  getTypeOfQuestion(type:string){
    this._typeOfQuestion.next(type);
  }

  getlevelOfQuestion(level:string){
    this._levelfQuestion.next(level);
  }

  getIsAutoApproval(isAutoApproval:boolean){
    this._isAutoApproval.next(isAutoApproval);
  }
}

import { Component, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { MatchingAnswer } from 'src/app/shared/modules/question/Answer';
import { Question } from 'src/app/shared/modules/question/Question';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { LoadContentService } from '../../Services/load-content.service';

@Component({
  selector: 'app-create-matching-question',
  templateUrl: './create-matching-question.component.html',
  styleUrls: ['./create-matching-question.component.scss']
})
export class CreateMatchingQuestionComponent implements OnInit {
  @Input() dataQuestion
  isDisable?:boolean;
  soDapAnDung=4;
  soDapAnNhieu=0;
  configQuestion={};
  question:Question;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  //get type,level
  type='matching';
  level='NB';
  isAutoApproval=false;
  constructor(private loadData:LoadContentService) {
    this.configQuestion = {
      contentsCss :  window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: true,
      allowedContent: true,
      extraPlugins:ckConfig.extraPlugins,
      toolbarGroups:ckConfig.toolbarGroups,
      removeButtons:ckConfig.removeButtons,
    };
   }
  ngOnInit(): void {
    this.loadData.levelfQuestion$.subscribe(level=>{
      if(level=='NB'){
        this.level='NB';
      }else if(level=='TH'){
        this.level='TH';
      }
      else if(level=='VD'){
        this.level='VD';
      }
      else if(level=='VDC'){
        this.level='VDC';
      }
    });

    this.loadData.isAutoApproval$.subscribe(isAuto=>{
      if(isAuto==true){
        this.isAutoApproval=true;
      }else{
        this.isAutoApproval=false;
      }
    })
    if(this.dataQuestion!=null){
      this.ckeditorContent = this.dataQuestion.content;
      this.answers = this.dataQuestion.listAnswer;
    }
  }

  getvalue(){
    var check:boolean;
    check=this.answers.some(x=>(isNullOrEmpty(x.NoiDung)));
    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
    } 
    else if(check==true){
      alert("Bạn chưa nhập nội dung câu trả lời");
    }
    else{
      this.question={id:1,content:this.ckeditorContent,type:this.type,level:this.level,isAutoApproval:this.isAutoApproval,listAnswer:this.answers};
      console.log(this.question);
      alert("lưu thành công!!!")
    }
  }
  ckeditorContent="";
 
  addAnswer(){
    if (this.groupAnswers().length<9) {
      this.answers.push(  
        { id: generateRandomUniqueInteger(1000,9999),NoiDung:"",viTri:true,dapAnDung:this.groupAnswers().length+1,xaoTronDapAn:true},
        { id: generateRandomUniqueInteger(1000,9999),NoiDung:"",viTri:false,dapAnDung:this.groupAnswers().length+1,xaoTronDapAn:true},);
        this.soDapAnDung++;
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }
  
  deleteAnswer(index) {
    if(this.groupAnswers().length>1 && (this.soDapAnDung>1 || this.answersOfGroup(index).length!=2)){
      if (this.answersOfGroup(index).length==2){
        this.answers=this.answers.filter(x=>x.dapAnDung!=index);
        this.soDapAnDung--;
      }
      else{
        if(this.isDisable==false){
          this.soDapAnNhieu--;
          this.answers=this.answers.filter(x=>x.dapAnDung!=index);
        }
        
        if(this.isDisable==true){
          this.soDapAnNhieu++;
          this.answers=this.answers.filter(x=>x.dapAnDung!=index);
        }
        if(this.soDapAnNhieu==0){
          this.isDisable=null;
        }
      }
      this.answers.map((ans, i) => {
        if(ans.dapAnDung > index){
          this.answers[i].dapAnDung--;
       }});
    }
  }
  addLeftInterference(){
    if (this.groupAnswers().length<9) {
      this.answers.push( { id: generateRandomUniqueInteger(1000,9999),NoiDung:"",viTri:true,dapAnDung:this.groupAnswers().length+1,xaoTronDapAn:true});
    }
    else {
      alert("Số phương án tối đa là 9");
    }
    this.soDapAnNhieu++;
    this.isDisable=false;
  }
  addRightInterference(){
    if (this.groupAnswers().length<9) {
      this.answers.push( { id: generateRandomUniqueInteger(1000,9999),NoiDung:"",viTri:false,dapAnDung:this.groupAnswers().length+1,xaoTronDapAn:true});
    }
    else {
      alert("Số phương án tối đa là 9");
    }
    this.soDapAnNhieu--;
    this.isDisable=true;
  }

  answers: MatchingAnswer[]=[
   { id: generateRandomUniqueInteger(1000,9999),NoiDung:"",viTri:true,dapAnDung:1,xaoTronDapAn:true},
   { id: generateRandomUniqueInteger(1000,9999),NoiDung:"",viTri:true,dapAnDung:2,xaoTronDapAn:true},
   { id: generateRandomUniqueInteger(1000,9999),NoiDung:"",viTri:true,dapAnDung:3,xaoTronDapAn:true},
   { id: generateRandomUniqueInteger(1000,9999),NoiDung:"",viTri:true,dapAnDung:4,xaoTronDapAn:true},
   { id: generateRandomUniqueInteger(1000,9999),NoiDung:"",viTri:false,dapAnDung:1,xaoTronDapAn:true},
   { id: generateRandomUniqueInteger(1000,9999),NoiDung:"",viTri:false,dapAnDung:2,xaoTronDapAn:true},
   { id: generateRandomUniqueInteger(1000,9999),NoiDung:"",viTri:false,dapAnDung:3,xaoTronDapAn:true},
   { id: generateRandomUniqueInteger(1000,9999),NoiDung:"",viTri:false,dapAnDung:4,xaoTronDapAn:true},
  ]
  configAnswer = {
    // startupFocus: true,
    height: 60,
    allowedContent: true,
    extraPlugins:ckConfig.extraPlugins,
      toolbarGroups:ckConfig.toolbarAnswer,
      removeButtons:ckConfig.removeButtonsAnswer,
  };
  @ViewChildren("cks") cks: QueryList<CKEditorComponent>;
  @ViewChild("ck") ck: CKEditorComponent
  countCk=8;
  ngAfterViewInit(): void {
    this.cks.changes.subscribe((x) => {
     
      let secondlast=x._results.length-2;
      this.initAnswer(x._results[secondlast]);
      this.initAnswer(x.last);
    });

    this.cks.forEach((x) => {
      this.initAnswer(x);
    });
     //reload edit
     this.ck.ready.subscribe(()=>{
      var head = this.ck.instance.document.getHead();
      var myscript = this.ck.instance.document.createElement( 'script', {
          attributes : {
          type : 'text/javascript',
          'src' : 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-MML-AM_CHTML'
        }
      });
      head.append( myscript );
    });
  }
  ChangeShuffler(id) {
    this.answers.map((ans, i) => {
       if(ans.id == id){
         this.answers[i].xaoTronDapAn= !this.answers[i].xaoTronDapAn;
      }
   });
 }
  private initAnswer(ck: CKEditorComponent) {
    ck.ready.subscribe((ev) => {
    document.getElementById(ck.instance.id + '_top').style.display = 'none';});
    ck.focus.subscribe((ev) => {
    document.getElementById(ck.instance.id + '_top').style.display = 'block';});
    ck.blur.subscribe((ev) => {
    document.getElementById(ck.instance.id + '_top').style.display = 'none';});
  }
  groupAnswers(){
    return [...new Set(this.answers.map(item => item.dapAnDung))]
  }
  answersOfGroup(index){
    return this.answers.filter(x=>x.dapAnDung==index);
  }
  SaveAndCreate(){
    window.location.reload();
    alert("Đã lưu")
  }
}

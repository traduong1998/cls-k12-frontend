import { AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, NgZone, OnInit, Output, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { QuestionType, QuestionTypeGroup } from '../../Interfaces/questionType.model';
import { LoadContentService } from '../../Services/load-content.service';

@Component({
  selector: 'app-type-question',
  templateUrl: './type-question.component.html',
  styleUrls: ['./type-question.component.scss']
})
export class TypeQuestionComponent implements OnInit {
  @Input() config: number;
  @Input() typeOfQuestion: string;
  @Input() levelOfQuestion: string;
  @Input() dataType;
  @Output() level: EventEmitter<any> = new EventEmitter<any>();
  check = 40;
  selectedKindOfQuestion = 'singlechoice';
  selectedLevel = 'NB';
  private readonly destroy$ = new Subject();
  constructor(private _typeOfQuestion: LoadContentService) {
  }

  ngOnInit(): void {
    if (this.config == 1) {
      this.check = 40;
    }
    else if (this.config == 2) {
      this.check = 100
    }
    if (this.dataType != null) {
      this.selectedKindOfQuestion = this.dataType;
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  selected(event) {
    this._typeOfQuestion.getTypeOfQuestion(event.value);
    this.selectedLevel = "NB";
  }
  changecauhoi() {
    this.selectedLevel = 'NB';
  }
  levelChange(event) {
    this._typeOfQuestion.getlevelOfQuestion(event.value);
  }

  questionGroups: QuestionTypeGroup[] = [
    {
      name: 'Câu hỏi trắc nghiệm',
      types: [
        { value: 'singlechoice', viewValue: 'Trắc nghiệm một lựa chọn' },
        { value: 'multichoice', viewValue: 'Trắc nghiệm nhiều lựa chọn' },
        { value: 'underline', viewValue: 'Gạch chân' },
        { value: 'truefalse', viewValue: 'Lựa chọn đúng sai' },
        { value: 'truefalseclause', viewValue: 'Mệnh đề đúng sai' },
        { value: 'fillblank', viewValue: 'Điền khuyết' },
        { value: 'fillblank2', viewValue: 'Điền khuyết loại 2' },
        { value: 'matching', viewValue: 'Ghép đôi' },
      ]
    },
    {
      name: 'Câu hỏi tự luận',
      types: [
        { value: 'essay', viewValue: 'Tự luận' },

      ]
    }
  ];
  levels: QuestionType[] = [
    { value: 'NB', viewValue: 'Nhận biết' },
    { value: 'TH', viewValue: 'Thông hiểu' },
    { value: 'VD', viewValue: 'Vận dụng' },
    { value: 'VDC', viewValue: 'Vận dụng cao' },
  ];
}

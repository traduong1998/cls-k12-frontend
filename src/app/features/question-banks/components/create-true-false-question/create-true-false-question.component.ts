import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { TrueFalseAnswer } from 'src/app/shared/modules/question/Answer';
import { Question } from 'src/app/shared/modules/question/Question';
import { ckConfig } from 'src/environments/ckeditorConfig';

import {LoadContentService } from '../../Services/load-content.service';

@Component({
  selector: 'app-create-true-false-question',
  templateUrl: './create-true-false-question.component.html',
  styleUrls: ['./create-true-false-question.component.scss']
})
export class CreateTrueFalseQuestionComponent implements OnInit {
  @Input() dataQuestion
  @Input() config:number;
  @Input() content:any;
  @Input() answers:any;
  flexLayout;
  ckeditorContent="";
  question:Question;
  ckeQuestionConfig = {};
  //get type,level
  type='truefalse';
  level='NB';
  isAutoApproval=false;

  @ViewChild("ck") ck : CKEditorComponent;

  constructor(private loadData:LoadContentService) {
   }
  ngOnInit(): void {
    if(this.config==1){
      this.flexLayout='row'
    }
    else if(this.config==2){
      this.flexLayout='column'
    }
    this.ckeQuestionConfig = {
      contentsCss :  window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: true,
      allowedContent: true,
    
      extraPlugins:ckConfig.extraPlugins,
      toolbarGroups:ckConfig.toolbarGroups,
      removeButtons:ckConfig.removeButtons,
    };

    this.loadData.levelfQuestion$.subscribe(level=>{
      if(level=='NB'){
        this.level='NB';
      }else if(level=='TH'){
        this.level='TH';
      }
      else if(level=='VD'){
        this.level='VD';
      }
      else if(level=='VDC'){
        this.level='VDC';
      }
    });

    this.loadData.isAutoApproval$.subscribe(isAuto=>{
      if(isAuto==true){
        this.isAutoApproval=true;
      }else{
        this.isAutoApproval=false;
      }
    })

    if(this.dataQuestion!=null){
      this.ckeditorContent = this.dataQuestion.content
    }
  }
  selected='true';
  answer:TrueFalseAnswer[] =[{NoiDung:'ans-true',dapAnDung:1}];
  ChangeAnswer(event){
    if(event.checked==true){
      this.answer=[{NoiDung:'ans-true',dapAnDung:1}]
    }else{
      this.answer=[{NoiDung:'ans-false',dapAnDung:0}]
    }
  }
  setAnswers(){
    return this.answer;
  }
  setQuestion(){
    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
    }
    else{
      let questionOfGroup={content:this.ckeditorContent,PhuongAn:this.answers};
      return questionOfGroup;
    }
  }
  
  getvalue(){

    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
    }
    // else if(this.answers.some(x=>x.dapAnDung==1)==false){
    //   alert("Bạn chưa chọn câu trả lời đúng");
    // }
    else{
      this.question ={id:1,content:this.ckeditorContent,type:this.type,level:this.level,listAnswer:this.answer,isAutoApproval:this.isAutoApproval}
      console.log(this.question);
      alert("lưu thành công!!!")
    }
  }
  SaveAndCreate(){
    window.location.reload();
    alert("Đã lưu")
  }
}

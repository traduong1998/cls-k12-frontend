import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissingWord1Component } from './missing-word1.component';

describe('MissingWord1Component', () => {
  let component: MissingWord1Component;
  let fixture: ComponentFixture<MissingWord1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MissingWord1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MissingWord1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

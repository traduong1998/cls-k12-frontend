import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-missing-word1',
  templateUrl: './missing-word1.component.html',
  styleUrls: ['./missing-word1.component.scss']
})
export class MissingWord1Component implements OnInit {
  @Input() dataQuestion;
  @Input() isExportWordFile
  defaultMath=`<math xmlns="http://www.w3.org/1998/Math/MathML"`;;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  constructor() { }

  ngOnInit(): void {
    console.log(this.dataQuestion.listAnswer)
  }

}

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-multi-choice',
  templateUrl: './multi-choice.component.html',
  styleUrls: ['./multi-choice.component.scss']
})
export class MultiChoiceComponent implements OnInit {
  @Input() dataQuestion;
  @Input() isExportWordFile;
  defaultMath=`<math xmlns="http://www.w3.org/1998/Math/MathML"`;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  constructor() { }

  ngOnInit(): void {
    console.log(this.dataQuestion);
  }

}

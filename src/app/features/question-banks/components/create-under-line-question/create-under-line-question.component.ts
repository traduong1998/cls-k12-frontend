import { Component, Input, NgZone, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Answer } from '../../Interfaces/answer.model';
import * as $ from 'jquery';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { getSelectionHTML } from 'src/environments/ckeditorFuntion';
import { UnderLineAnswer } from 'src/app/shared/modules/question/Answer';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { LoadContentService } from '../../Services/load-content.service';
import { Question } from 'src/app/shared/modules/question/Question';
import { CKEditorComponent } from 'ckeditor4-angular';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
declare var CKEDITOR: any;
@Component({
  selector: 'app-create-under-line-question',
  templateUrl: './create-under-line-question.component.html',
  styleUrls: ['./create-under-line-question.component.scss']
})
export class CreateUnderLineQuestionComponent implements OnInit {
  ckeQuestionConfig = {};
  @Input() dataQuestion;
  @Input() config: number;
  @Input() content: any;
  @Input() answer: any;
  flexLayout;
  question: Question;
  type = 'truefalse';
  level = 'NB';
  isAutoApproval = false;
  currentUnderlineID = 1;
  CauHoiID = "$CauHoiID$";
  configAnswer = {
    // startupFocus: true,
    contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
    height: 100,
    allowedContent: true,
    fullPage: true,
    extraPlugins: ckConfig.extraPlugins,
    toolbarGroups: ckConfig.toolbarAnswer,
    removeButtons: ckConfig.removeButtonsAnswer,
  };
  constructor(private loadData: LoadContentService, private zone: NgZone) {
    this.ckeQuestionConfig = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: true,
      allowedContent: true,
      extraPlugins: ['insertaudio,insertimage,insertvideo', 'createunderline', 'ckeditor_wiris'],
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
      removePlugins: 'Underline',
    };
  }

  ngOnInit(): void {
    if (this.config == 1) {
      this.flexLayout = 'row'
    }
    else if (this.config == 2) {
      this.flexLayout = 'column'
    }
    this.loadData.levelfQuestion$.subscribe(level => {
      if (level == 'NB') {
        this.level = 'NB';
      } else if (level == 'TH') {
        this.level = 'TH';
      }
      else if (level == 'VD') {
        this.level = 'VD';
      }
      else if (level == 'VDC') {
        this.level = 'VDC';
      }
    });

    this.loadData.isAutoApproval$.subscribe(isAuto => {
      if (isAuto == true) {
        this.isAutoApproval = true;
      } else {
        this.isAutoApproval = false;
      }
    })

    //khi chỉnh sửa câu hỏi
    if (this.dataQuestion != null) {
      this.ckeditorContent = this.dataQuestion.content
      this.answers = this.dataQuestion.listAnswer;
    }
  }
  ckeditorContent = "";
  setAnswers() {
    return this.answers;
  }
  setQuestion() {
    let err = 0;
    let editor = this.ck.instance;
    let questionContent = editor.document.getBody().getText();
    if (isNullOrWhiteSpace(this.ckeditorContent.toString().trim()) || isNullOrEmpty(this.ckeditorContent.toString().trim())) {
      err++
      alert("Bạn chưa nhập nội dung câu hỏi câu gạch chân");
      return { content: err.toString(), PhuongAn: [] };
    }
    else if (this.answers.length == 0 || this.answers == null) {
      err++;
      alert("Bạn chưa đưa ra phương án lựa chọn cho câu gạch chân");
      return { content: err.toString(), PhuongAn: [] };
    }
    else if (this.answers.some(x => x.dapAnDung == 1) == false) {
      err++;
      alert("Bạn chưa chọn câu trả lời đúng câu gạch chân");
      return { content: err.toString(), PhuongAn: [] };
    }
    else {
      let questionOfGroup = { content: this.ckeditorContent, PhuongAn: this.answers };
      return questionOfGroup;
    }
  }
  getvalue() {
    var ckeEditableArea = $(this.ck.instance.editable().$);
    if (isNullOrWhiteSpace(this.ckeditorContent.toString().trim()) || isNullOrEmpty(this.ckeditorContent.toString().trim())) {
      alert("Bạn chưa nhập nội dung câu hỏi");
    }
    else if (this.answers.length == 0 || this.answers == null) {
      alert("Bạn chưa đưa ra phương án lựa chọn");
    }
    else if (this.answers.length != ckeEditableArea.find('u').length) {
      alert("Số vị trí gạch chân phải bằng số lượng phương án đúng");
    }
    else if (this.answers.some(x => x.dapAnDung == 1) == false) {
      alert("Bạn chưa chọn câu trả lời đúng");
    }
    else {
      this.question = { id: 1, content: this.ckeditorContent, type: this.type, level: this.level, listAnswer: this.answers, isAutoApproval: this.isAutoApproval }
      console.log(this.question);
      alert("lưu thành công!!!")
    }
  }

  addAnswer(content, stt) {

    if (this.answers.length < 9) {
      this.zone.run(() => {
        this.answers.push({ id: generateRandomUniqueInteger(1000, 9999), NoiDung: content, dapAnDung: 0, stt: stt });

      });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  deleteAnswer(id) {
    let answer = this.answers.find(x => x.id == id);
    let index = this.answers.indexOf(answer, 0);
    this.answers.splice(index, 1);
    this.answers.map((ans, i) => {
      if (ans.stt > answer.stt) {
        this.answers[i].stt--;
      }
    });

    var ckeEditableArea = $(this.ck.instance.editable().$);
    // Bỏ gạch chân phương án đã chọn để xóa.
    var htmlData = ckeEditableArea.find('*[data-stt = ' + answer.stt + ']');
    htmlData.replaceWith($(htmlData).html());
    this.capNhatDanhSachPhuongAn();
  }



  answers: UnderLineAnswer[] = [];

  @ViewChild("ck") ck: CKEditorComponent;

  ngAfterViewInit(): void {
    this.ck.ready.subscribe((ev) => {
      this.ck.instance.on('beforeCommandExec', (event) => {
        return this.handleBeforeCommandExec(event, this.ck.instance);
      });
      this.ck.instance.on('afterCommandExec', (event) => {

        this.handleAfterCommandExec(event, this.ck.instance);
      });
    });
    //reload edit
    this.ck.ready.subscribe(() => {
      var head = this.ck.instance.document.getHead();
      var myscript = this.ck.instance.document.createElement('script', {
        attributes: {
          type: 'text/javascript',
          'src': 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-MML-AM_CHTML'
        }
      });
      head.append(myscript);
    });
  }

  changeSelectAnswer(i) {
    this.answers.map(ans => ans.dapAnDung = 0);
    this.answers[i].dapAnDung = 1;
  }
  changeSTT(index) {
    this.answers.map((ans, i) => {
      if (ans.stt >= index) {
        this.answers[i].stt += 1;
      }
    });
  }
  SortedAnswers() {
    if (this.answers.length > 0) {
      return this.answers.filter(x => x.stt != -1).sort((a, b) => a.stt > b.stt ? 1 : a.stt < b.stt ? -1 : 0);
    } else {
      return this.answers;
    }
  }

  // Kiểm tra sự kiện trước khi tạo đáp án
  handleBeforeCommandExec(event, editor) {
    var commandName = event.data.name;
    if (commandName == 'underline') {
      var mySelection = editor.getSelection();
      var selectedText = mySelection.getNative();
      var realSelectedText = getSelectionHTML(selectedText).toString();
      if (mySelection.getType() == CKEDITOR.SELECTION_TEXT) {
        //XLS
        if (CKEDITOR.env.ie) {
          mySelection.unlock(true);
          selectedText = mySelection.getNative();
        } else {
          selectedText = mySelection.getNative();
        }

        realSelectedText = selectedText.toString();


        if (realSelectedText.trim().length == 0) {
          return false;
        }
      }

      var currentTag;
      if (CKEDITOR.env.ie) {
        mySelection.unlock(true);
        selectedText = mySelection.getNative();
      } else {
        selectedText = mySelection.getNative();
      }
      if (realSelectedText.trim() == "") {
        return false;
      }
      if ((CKEDITOR.env.ie)) {
        currentTag = mySelection.getNative().anchorNode;
      } else {
        currentTag = mySelection.getNative().anchorNode.parentElement.localName;
      }
      // nếu người dùng spam để tạo tiếp phương án tại vị trí vừa mới tạo thì return false
      if ($(mySelection.getNative().focusNode).closest('u').length > 0) {
        return false;
      }
      // kiểm tra nếu phần gạch chân là video thì không cho gạch chân
      if ($(selectedText.baseNode) != undefined) {
        if ($(selectedText.baseNode).find('.cke_video').length > 0) {
          alert("Phương án chỉ bao gồm ký tự và số");
          return false;
        }

      }
      // kiểm tra nếu phần gạch chân là video thì không cho gạch chân
      if ($(selectedText.focusNode).find('.cke_video').length > 0) {
        alert("Phương án chỉ bao gồm ký tự và số");
        return false;
      }
      // Kiểm tra vùng đã tồn tại phương án
      if (/<u[^>]*>.+<\/u>/g.test(getSelectionHTML(selectedText)) || currentTag == 'u') {
        alert("Vùng chọn tồn tại phương án");
        return false;
      }
      // kiểm tra nếu phần gạch chân là ảnh
      if (/<span tabindex="-1"/g.test(getSelectionHTML(selectedText))) {
        alert("Phương án chỉ bao gồm ký tự và số");
        return false;
      }

    }

  }
  handleAfterCommandExec(event, editor): void {
    var commandName = event.data.name;
    // Kiểm tra sự kiện sau khi click tag U
    // For 'underline' commmand
    let id = generateRandomUniqueInteger(1000, 9999);
    if (commandName == "underline") {

      let mySelection = editor.getSelection();
      var selectedText = mySelection.getNative();
      var realSelectedText = getSelectionHTML(selectedText).toString();

      // Xác định vùng được gạch chân
      if (mySelection.getType() == 2) {

        if (CKEDITOR.env.ie) {
          mySelection.unlock(true);
          selectedText = mySelection.getNative();
        }
        else {
          selectedText = mySelection.getNative();


          realSelectedText = selectedText.toString();

          if (realSelectedText.trim() == "") {
            return;
          }
        }
      }
      //chuyển đổi nội dung người dùng soạn thảo trên CKEDITOR thành đối tương Jquery
      //  let ckeEditableArea = $(editor.editable().$);

      this.taoPhuongAn(realSelectedText);

    }


  }


  //Cập nhật lại danh sách các phương án (update mới)
  taoPhuongAn(realSelectedText) {
    var ckeEditableArea = $(this.ck.instance.editable().$);
    //remove all row
    $('.tbl-phuong-an-js > tbody').empty();
    let position = 0;
    ckeEditableArea.find('u').each(function (index) {
      //can thiệp vào thẻ u
      $(this).attr('contenteditable', "false");
      $(this).attr('data-stt', index);
      $(this).attr('class', 'ck-phuongan-underline');
      if ($(this).text() == realSelectedText) {
        position = index;
      }
    });
    if (this.answers.length >= 1) {
      this.changeSTT(position);
    }
    this.addAnswer(realSelectedText, position);
  }
  capNhatDanhSachPhuongAn() {
    var ckeEditableArea = $(this.ck.instance.editable().$);
    //remove all row
    ckeEditableArea.find('u').each(function (index) {
      //can thiệp vào thẻ u
      $(this).attr('data-stt', index);
    });
  }
  SaveAndCreate() {
    window.location.reload();
    alert("Đã lưu")
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnderLineComponent } from './under-line.component';

describe('UnderLineComponent', () => {
  let component: UnderLineComponent;
  let fixture: ComponentFixture<UnderLineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnderLineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnderLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

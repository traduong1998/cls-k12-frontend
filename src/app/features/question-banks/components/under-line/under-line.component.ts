import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-under-line',
  templateUrl: './under-line.component.html',
  styleUrls: ['./under-line.component.scss']
})
export class UnderLineComponent implements OnInit {
  @Input() dataQuestion;
  @Input() isExportWordFile;
  defaultMath=`<math xmlns="http://www.w3.org/1998/Math/MathML"`;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  constructor() { }

  ngOnInit(): void {
  }

}

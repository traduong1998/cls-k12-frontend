import { Component, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { FillBlank2Answer } from 'src/app/shared/modules/question/Answer';
import { Question } from 'src/app/shared/modules/question/Question';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { getSelectionHTML } from 'src/environments/ckeditorFuntion';
import { LoadContentService } from '../../Services/load-content.service';

@Component({
  selector: 'app-create-missing-word2-question',
  templateUrl: './create-missing-word2-question.component.html',
  styleUrls: ['./create-missing-word2-question.component.scss']
})
export class CreateMissingWord2QuestionComponent implements OnInit {
  @Input() dataQuestion;
  currentBlankID = 1;
  configQuestion = {};
  @Input() config:number;
  flexLayout;
  phuongAnGayNhieuID = 1;
  CauHoiID = "$CauHoiID$";
  answers: FillBlank2Answer[] = [];
  question:Question;
  isDelete=false;
  //get type,level
  type='fillblank2';
  level='NB';
  isAutoApproval=false;
  configAnswer = {
    // startupFocus: true,
    height: 60,
    allowedContent: true,
  
    extraPlugins:ckConfig.extraPlugins,
      toolbarGroups:ckConfig.toolbarAnswer,
      removeButtons:ckConfig.removeButtonsAnswer,
  };
  constructor(private loadData:LoadContentService) {
    this.configQuestion = {
      contentsCss :  window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: true,
      allowedContent: true,
    
      extraPlugins:['insertaudio,insertimage,insertvideo','fillblank2','ckeditor_wiris'],
      toolbarGroups:ckConfig.toolbarGroups,
      removeButtons:ckConfig.removeButtonsFillBlank,
    };
   }

  ngOnInit(): void {
    this.loadData.levelfQuestion$.subscribe(level=>{
      if(level=='NB'){
        this.level='NB';
      }else if(level=='TH'){
        this.level='TH';
      }
      else if(level=='VD'){
        this.level='VD';
      }
      else if(level=='VDC'){
        this.level='VDC';
      }
    });

    this.loadData.isAutoApproval$.subscribe(isAuto=>{
      if(isAuto==true){
        this.isAutoApproval=true;
      }else{
        this.isAutoApproval=false;
      }
    })
    if(this.dataQuestion!=null){
      this.ckeditorContent = this.dataQuestion.content;
      this.answers = this.dataQuestion.listAnswer;
    }
  }

  getvalue(){
    let ckeEditableArea = $(this.ck.instance.editable().$);
    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
    } 
    else if(this.answers.some(x=>(isNullOrEmpty(x.NoiDung)))) {
      alert("Bạn chưa nhập nội dung đáp án nhiễu");
    } 
    else if(this.groupAnswers().length!=ckeEditableArea.find('span[class^="Blanking2"]').length){
      alert("Số vị trí điền khuyết phải bằng số lượng phương án đúng");
    }
    else{
      this.question={id:1,content:this.ckeditorContent,type:this.type,level:this.level,isAutoApproval:this.isAutoApproval,listAnswer:this.answers};
      console.log(this.question);
      alert("lưu thành công!!!")
    }
  }
  ckeditorContent="";
  
  addAnswer(answer,index) {
    if (this.groupAnswers().length < 9) {
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999),viTri:true, NoiDung: answer, dapAnDung: index, xaoTronDapAn: true });
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999),viTri:false, NoiDung: "", dapAnDung: index, xaoTronDapAn: true });
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999),viTri:false, NoiDung: "", dapAnDung: index, xaoTronDapAn: true });
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999),viTri:false, NoiDung: "", dapAnDung: index, xaoTronDapAn: true });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }
  addFakeAnswer(index){
    if (this.answersOfGroup(index).length < 9) {
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999),viTri:false, NoiDung: "", dapAnDung: index, xaoTronDapAn: true });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }
  changeSelectAnswer(i) {
    this.answers.map(ans => ans.dapAnDung = 0);
    this.answers[i].dapAnDung = 1;
  }
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  @ViewChild("ck") ck: CKEditorComponent;
  
  @ViewChildren("cks") cks: QueryList<CKEditorComponent>;
  ngAfterViewInit(): void {
    // add command tạo phương án
    this.ck.ready.subscribe(()=>{
      this.ck.instance.on('afterCommandExec', (event)=> {
        this.handleAfterCommandExec(event,this.ck.instance)
      });  
      this.ck.instance.addCommand( 'taophuongan', {
        exec: function( editor ) {
        }
    } );
    });
      // ẩn toolbar ckedtior
  this.cks.changes.subscribe((x) => {
      for (let index = 0; index < x._results.length; index++) {
        this.initAnswer(x._results[index]);
      }
  });

  this.cks.forEach((x) => {
    this.initAnswer(x);
  });
   //reload edit
   this.ck.ready.subscribe(()=>{
    var head = this.ck.instance.document.getHead();
    var myscript = this.ck.instance.document.createElement( 'script', {
        attributes : {
        type : 'text/javascript',
        'src' : 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-MML-AM_CHTML'
      }
    });
    head.append( myscript );
  });
  }

  handleAfterCommandExec(event,editor) {
    var commandName = event.data.name;
    // Kiểm tra sự kiện sau khi click tag U
    // For 'underline' commmand
    if (commandName == "taophuongan") {
      var mySelection = this.ck.instance.getSelection();
      let selectedText = mySelection.getNative();
      let editor = this.ck.instance;
      let realSelectedText = getSelectionHTML(selectedText).toString();
      // if (mySelection.getType() == 2) {
  
      //   if (/<img[^>]*>/g.test(getSelectionHTML(selectedText)) || /<video[^>]*>/g.test(getSelectionHTML(selectedText))) {
      //     alert('Phương án chỉ chứa văn bản');
      //     return;
      //   }
  
      //   let realSelectedText = getSelectionHTML(selectedText).toString();
      //   if (isNullOrWhiteSpace(selectedText.toString().trim()) || isNullOrEmpty(selectedText.toString().trim())) {
      //     alert('Vui lòng chọn văn bản');
      //     return;
      //   }
        if (getSelectionHTML(selectedText).indexOf('class="Blanking2') != -1 ||
        ($(editor.getSelectedHtml().$).is('span') &&
            $(editor.getSelectedHtml().getHtml()).hasClass('Blanking2'))) {
        alert('Vùng chọn đã có phương án');
        return;
        }
        //insert blank, thay bằng index của phần điền khuyết vào editor
        let insertedBlank = editor.document.createElement('span');
        insertedBlank.setAttribute('data-current-answer', '');
        insertedBlank.setAttribute('class', 'Blanking2 Blanking2-' + this.CauHoiID); //CauHoiID được truyền vào từ View MissingWordAdd(Edit)
        insertedBlank.setAttribute('data-position', this.currentBlankID); //currentBlankID được truyền vào từ View MissingWordAdd(Edit)
        //insertedBlank.setAttribute('style', 'font-weight: bold; border-bottom: 1px solid #c0c0c0; min-width: 50px; display:inline-block; text-align: center;');
        //ngăn không cho người dùng chỉnh sửa phần điền khuyết
        insertedBlank.setAttribute('contenteditable', 'false');
        insertedBlank.appendText('(' + this.currentBlankID.toString() + ')');
        editor.insertElement(insertedBlank);

        this.reorderAnswerIndexes(this.currentBlankID, editor,realSelectedText);
    
        this.currentBlankID++;
  
      // } else {
      //   alert('vui lòng chỉ chọn văn bản');
      //   return;
      // }
    }
  }
  reorderAnswerIndexes(currentMaxID, editor,realSelectedText) {

    let ckeEditableArea = $(editor.editable().$);
    let blankIndexElements = ckeEditableArea.find('span[class^="Blanking2"]');
    let insertedPosition = 0;
    blankIndexElements.each(function (index, element) {
      if ($(element).attr('data-position') == currentMaxID) {
        insertedPosition = index + 1;
      }
    });
  
    $(ckeEditableArea.find('span[class^="Blanking2"]:gt(' + (insertedPosition - 1) + ')').get().reverse()).each(function (index, element) {
      let oldIndex = parseInt($(element).attr('data-position'));
      $(element).attr('data-position', oldIndex + 1);
      $(element).text('(' + (oldIndex + 1) + ')');
      // let dongPhuongAnDungHienTai = $('.phuong-an-dung > table > tbody > tr[data-position="' + oldIndex + '"]');
      // dongPhuongAnDungHienTai.attr('data-position', oldIndex + 1);
      // dongPhuongAnDungHienTai.find('*[blank-index]').text((oldIndex + 1));
    });
  
    ckeEditableArea.find('span[class^="Blanking2"]').each(function (index, element) {
      if ((index + 1) == insertedPosition) {
        $(element).attr('data-position', insertedPosition);
        $(element).text('(' + insertedPosition + ')');
      }
    });
    if(this.answers.length>=1){
      this.changeSTT(insertedPosition);
    }
    this.addAnswer(realSelectedText,insertedPosition);
  }
  
  ChangeShuffler(id) {
    this.answers.map((ans, i) => {
       if(ans.id == id){
         this.answers[i].xaoTronDapAn= !this.answers[i].xaoTronDapAn;
      }
   });
 }
 deleteAnswer(id) {
  let answer=this.answers.find(x=>x.id==id);

  if(answer.viTri==true){
    this.answers=this.answers.filter(x=>x.dapAnDung!=answer.dapAnDung);
    let ckeEditableArea = $(this.ck.instance.editable().$);

    let htmlData = ckeEditableArea.find('span[class^="Blanking2"]');
    htmlData.each(function (index, element) {
        let currentDataBlankIndex = parseInt($(element).attr('data-position'));
        if (currentDataBlankIndex > answer.dapAnDung) {
            $(element).html('(' + (currentDataBlankIndex - 1) + ')');
            //cập nhật lại index ở div phuong-an-dung
            let rowPhuongAnDung = $('.phuong-an-dung > table > tbody > tr[data-position="' + currentDataBlankIndex + '"]');
            $(rowPhuongAnDung).find('*[blank-index]').text(currentDataBlankIndex - 1);
            $(rowPhuongAnDung).attr('data-position', currentDataBlankIndex - 1);
            $(element).attr('data-position', currentDataBlankIndex - 1);
        } else if (currentDataBlankIndex == answer.dapAnDung) {
          $(element).replaceWith(answer.NoiDung);
        }
    });

    this.currentBlankID -= 1;
    this.answers.map((ans, i) => {
      if(ans.dapAnDung > answer.dapAnDung){
        this.answers[i].dapAnDung--; 
     }
  });
  }
  else{
    let index= this.answers.indexOf(answer,0);
  
    if(this.answersOfGroup(answer.dapAnDung).length<=2){
      alert("Phải có ít nhất 1 phương án nhiễu")
    }
    else{
      this.answers.splice(index, 1);
    }
  }
}
  changeSTT(index){
    this.answers.map((ans, i) => {
      if(ans.dapAnDung >= index){
        this.answers[i].dapAnDung+=1; 
     }
  });
  }
  SortedAnswers(){
    return this.answers.sort((a,b)=>a.dapAnDung>b.dapAnDung?1:a.dapAnDung<b.dapAnDung?-1:0);
  }
  groupAnswers(){
    return [...new Set(this.SortedAnswers().map(item => item.dapAnDung))]
  }
  answersOfGroup(index){
    return this.answers.filter(x=>x.dapAnDung==index);
  }
  SaveAndCreate(){
    window.location.reload();
    alert("Đã lưu")
  }
  private initAnswer(ck: CKEditorComponent) {
    if(ck){
    ck.focus.subscribe((ev) => {
    document.getElementById(ck.instance.id + '_top').style.display = 'block';});
    ck.blur.subscribe((ev) => {
    document.getElementById(ck.instance.id + '_top').style.display = 'none';});
  }
}
}

import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { EventEmitter } from 'events';
import { SaveDataService } from '../../Services/save-data.service';
import { CreateGroupQuestionComponent } from '../create-group-question/create-group-question.component';

@Component({
  selector: 'app-footer-question',
  templateUrl: './footer-question.component.html',
  styleUrls: ['./footer-question.component.scss']
})
export class FooterQuestionComponent implements OnInit {

  @ViewChild(CreateGroupQuestionComponent) groupQuestion: CreateGroupQuestionComponent

  constructor(private saveData :SaveDataService) { }

  ngOnInit(): void {
    this.saveData.question$.subscribe(data=>{
      console.log(data);
    });
  }
  save(){
    this.groupQuestion.show();
  }
}

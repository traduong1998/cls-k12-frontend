import { Component, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { Answer } from 'src/app/shared/modules/question/Answer';
import { Question } from 'src/app/shared/modules/question/Question';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { LoadContentService } from '../../Services/load-content.service';

@Component({
  selector: 'app-create-true-false-clause-question',
  templateUrl: './create-true-false-clause-question.component.html',
  styleUrls: ['./create-true-false-clause-question.component.scss']
})
export class CreateTrueFalseClauseQuestionComponent implements OnInit {
  @Input() dataQuestion;
  @Input() config: number;
  @Input() levelOfQuestion: string;

  @Input() content: any;
  @Input() answer: any;
  flexLayout;

  //get type,level
  type = 'truefalseclause';
  level = 'NB';
  isAutoApproval = false;
  question: Question;
  isDelete = false;
  configQuestion = {};
  @ViewChild("ck") ck: CKEditorComponent;

  constructor(private loadData: LoadContentService) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: true,
      allowedContent: true,

      extraPlugins: ckConfig.extraPlugins,
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
    };
  }

  ngOnInit(): void {
    if (this.config == 1) {
      this.flexLayout = 'row'
    }
    else if (this.config == 2) {
      this.flexLayout = 'column'
    }
    this.loadData.levelfQuestion$.subscribe(level => {
      if (level == 'NB') {
        this.level = 'NB';
      } else if (level == 'TH') {
        this.level = 'TH';
      }
      else if (level == 'VD') {
        this.level = 'VD';
      }
      else if (level == 'VDC') {
        this.level = 'VDC';
      }
    });

    this.loadData.isAutoApproval$.subscribe(isAuto => {
      if (isAuto == true) {
        this.isAutoApproval = true;
      } else {
        this.isAutoApproval = false;
      }
    });

    //khi chỉnh sửa câu hỏi
    if (this.dataQuestion != null) {
      this.ckeditorContent = this.dataQuestion.content
      this.answers = this.dataQuestion.listAnswer;
    }
  }
  changeSelectAnswer(i) {
    if (this.answers[i].dapAnDung == 0) {
      this.answers[i].dapAnDung = 1;
    }
    else {
      this.answers[i].dapAnDung = 0;
    }
  }
  setAnswers() {
    return this.answers;
  }
  setQuestion() {
    let err = 0;
    var check: boolean;
    check = this.answers.some(x => (isNullOrEmpty(x.NoiDung)));

    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      err++;
      alert("Bạn chưa nhập nội dung câu hỏi cho câu mệnh đề đúng sai");
      return { content: err.toString(), PhuongAn: this.answers };
    }
    else if (check == true) {
      err++;
      alert("Bạn chưa nhập nội dung câu trả lời cho câu mệnh đề đúng sai");
      return { content: err.toString(), PhuongAn: this.answers };
    }
    else {
      let questionOfGroup = { content: this.ckeditorContent, PhuongAn: this.answers };
      return questionOfGroup;
    }
    //return this.ckeditorContent;
  }

  getvalue() {
    let err = 0;
    var check: boolean;
    check = this.answers.some(x => (isNullOrEmpty(x.NoiDung)));
    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
    }
    else if (check == true) {
      alert("Bạn chưa nhập nội dung câu trả lời");
    }
    else {
      this.question = { id: 1, content: this.ckeditorContent, type: this.type, level: this.level, listAnswer: this.answers, isAutoApproval: this.isAutoApproval }
      console.log(this.question);
      alert("lưu thành công!!!")
    }
  }

  ckeditorContent = "";
  addAnswer() {
    if (this.answers.length < 9) {
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 1, viTri: null, xaoTronDapAn: true });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  deleteAnswer(i) {
    this.answers.splice(i, 1);
    if (this.answers.length == 1) {
      this.isDelete = true;
    }
  }
  ChangeShuffler(i) {
    this.answers[i].xaoTronDapAn = !this.answers[i].xaoTronDapAn;
  }

  answers: Answer[] = [
    { id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 1, viTri: null, xaoTronDapAn: true },
    { id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 1, viTri: null, xaoTronDapAn: true },
    { id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 1, viTri: null, xaoTronDapAn: true },
    { id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 1, viTri: null, xaoTronDapAn: true },
  ];
  editorData = `<p>This is a CKEditor 4 WYSIWYG editor instance created with Angular.</p>`;
  configAnswer = {
    // startupFocus: true,
    height: 60,
    allowedContent: true,

    extraPlugins: ckConfig.extraPlugins,
    toolbarGroups: ckConfig.toolbarAnswer,
    removeButtons: ckConfig.removeButtonsAnswer,
  };
  @ViewChildren("cks") cks: QueryList<CKEditorComponent>;

  ngAfterViewInit(): void {
    this.cks.changes.subscribe((x) => {
      this.initAnswer(x.last);
    });

    this.cks.forEach((x) => {
      this.initAnswer(x);
    });
    //reload edit
    this.ck.ready.subscribe(() => {
      var head = this.ck.instance.document.getHead();
      var myscript = this.ck.instance.document.createElement('script', {
        attributes: {
          type: 'text/javascript',
          'src': 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-MML-AM_CHTML'
        }
      });
      head.append(myscript);
    });
  }

  private initAnswer(ck: CKEditorComponent) {
    ck.ready.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'none';
    });
    ck.focus.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'block';
    });
    ck.blur.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'none';
    });
  }
  SaveAndCreate() {
    window.location.reload();
    alert("Đã lưu")
  }
}

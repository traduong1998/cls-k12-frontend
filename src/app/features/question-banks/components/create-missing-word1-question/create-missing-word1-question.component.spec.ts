import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMissingWord1QuestionComponent } from './create-missing-word1-question.component';

describe('CreateMissingWord1QuestionComponent', () => {
  let component: CreateMissingWord1QuestionComponent;
  let fixture: ComponentFixture<CreateMissingWord1QuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateMissingWord1QuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMissingWord1QuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

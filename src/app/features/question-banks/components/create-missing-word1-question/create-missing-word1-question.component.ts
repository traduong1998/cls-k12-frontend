import { Component, Input, OnChanges, OnInit, QueryList, SimpleChanges, ViewChild, ViewChildren } from '@angular/core';
import { Answer } from '../../Interfaces/answer.model';
import * as $ from 'jquery';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { getSelectionHTML } from 'src/environments/ckeditorFuntion';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { FillBlankAnswer } from 'src/app/shared/modules/question/Answer';
import { Question } from 'src/app/shared/modules/question/Question';
import { LoadContentService } from '../../Services/load-content.service';
import { CKEditorComponent } from 'ckeditor4-angular';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
declare var CKEDITOR: any;
@Component({
  selector: 'app-create-missing-word1-question',
  templateUrl: './create-missing-word1-question.component.html',
  styleUrls: ['./create-missing-word1-question.component.scss']
})
export class CreateMissingWord1QuestionComponent implements OnInit {
  configQuestion = {};
  @Input() dataQuestion
  @Input() config: number;
  @Input() content: any;
  @Input() answer: any;
  flexLayout;
  currentBlankID = 1;
  phuongAnGayNhieuID = 1;
  CauHoiID = "$CauHoiID$";
  answers: FillBlankAnswer[] = [];
  question: Question;
  //get type,level
  type = 'fillblank';
  level = 'NB';
  isAutoApproval = false;
  @ViewChild("ck") ck: CKEditorComponent;
  @ViewChildren("cks") cks: QueryList<CKEditorComponent>;
  constructor(private loadData: LoadContentService) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: true,
      allowedContent: true,

      extraPlugins: ['insertaudio,insertimage,insertvideo', 'fillblank1', 'ckeditor_wiris'],
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtonsFillBlank,
    };
  }
  ngOnInit(): void {
    if (this.config == 1) {
      this.flexLayout = 'row'
    }
    else if (this.config == 2) {
      this.flexLayout = 'column'
    }
    this.loadData.levelfQuestion$.subscribe(level => {
      if (level == 'NB') {
        this.level = 'NB';
      } else if (level == 'TH') {
        this.level = 'TH';
      }
      else if (level == 'VD') {
        this.level = 'VD';
      }
      else if (level == 'VDC') {
        this.level = 'VDC';
      }
    });
    this.loadData.isAutoApproval$.subscribe(isAuto => {
      if (isAuto == true) {
        this.isAutoApproval = true;
      } else {
        this.isAutoApproval = false;
      }
    })

    //khi chỉnh sửa câu hỏi
    if (this.dataQuestion != null) {
      this.ckeditorContent = this.dataQuestion.content
      this.answers = this.dataQuestion.listAnswer;
    }
  }
  setAnswers() {
    return this.answers;
  }
  setQuestion() {
    let err = 0;
    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      err++;
      alert("Bạn chưa nhập nội dung câu hỏi của câu điền khuyết");
      return { content: err.toString(), PhuongAn: [] };
    }
    else if (this.answers.some(x => x.dapAnDung == 1) == false) {
      err++;
      alert("Bạn chưa đưa ra đáp án đúng");
      return { content: err.toString(), PhuongAn: [] };
    } else if (this.answers.some(x => (isNullOrEmpty(x.NoiDung)))) {
      err++;
      alert("Bạn chưa nhập nội dung phương án nhiễu câu điền khuyết");
      return { content: err.toString(), PhuongAn: [] };
    }
    else {
      let questionOfGroup = { content: this.ckeditorContent, PhuongAn: this.answers };
      return questionOfGroup;
    }
  }

  getvalue() {
    let editor = this.ck.instance;
    let questionContent = editor.document.getBody().getText();
    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
    }
    else if (this.answers.some(x => x.dapAnDung == 1) == false) {
      alert("Bạn chưa đưa ra đáp án đúng");
    }
    else if (this.answers.some(x => (isNullOrEmpty(x.NoiDung)))) {
      alert("Bạn chưa nhập nội dung đáp án nhiễu");
    }
    else {
      this.question = { id: 1, content: this.ckeditorContent, type: this.type, level: this.level, isAutoApproval: this.isAutoApproval, listAnswer: this.answers };
      console.log(this.question);
      alert("lưu thành công!!!")
    }
  }
  ckeditorContent = "";

  addAnswer(answer, index) {
    if (this.answers.length < 9) {
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999), NoiDung: answer, dapAnDung: index, xaoTronDapAn: true });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }
  addFakeAnswer() {
    if (this.answers.length < 9) {
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: -1, xaoTronDapAn: true });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  deleteAnswer(id) {
    let answer = this.answers.find(x => x.id == id);
    let index = this.answers.indexOf(answer, 0);
    if (answer.dapAnDung > -1) {

      let ckeEditableArea = $(this.ck.instance.editable().$);

      let htmlData = ckeEditableArea.find('span[class^="Blanking"]');
      htmlData.each(function (index, element) {
        let currentDataBlankIndex = parseInt($(element).attr('data-position'));
        if (currentDataBlankIndex > answer.dapAnDung) {
          $(element).html('(' + (currentDataBlankIndex - 1) + ')');
          //cập nhật lại index ở div phuong-an-dung
          let rowPhuongAnDung = $('.phuong-an-dung > table > tbody > tr[data-position="' + currentDataBlankIndex + '"]');
          $(rowPhuongAnDung).find('*[blank-index]').text(currentDataBlankIndex - 1);
          $(rowPhuongAnDung).attr('data-position', currentDataBlankIndex - 1);
          $(element).attr('data-position', currentDataBlankIndex - 1);
        } else if (currentDataBlankIndex == answer.dapAnDung) {
          $(element).replaceWith(answer.NoiDung);
          console.log('xóa', answer.NoiDung)
          // $(element).replaceWith(answer.NoiDung.substring(3,answer.NoiDung.length-5));
        }
      });

      this.currentBlankID -= 1;
      this.answers.map((ans, i) => {
        if (ans.dapAnDung > answer.dapAnDung) {
          this.answers[i].dapAnDung--;
        }
      });
    }
    this.answers.splice(index, 1);

  }
  changeSTT(index) {
    this.answers.map((ans, i) => {
      if (ans.dapAnDung >= index) {
        this.answers[i].dapAnDung += 1;
      }
    });
  }
  ChangeShuffler(id) {
    this.answers.map((ans, i) => {
      if (ans.id == id) {
        this.answers[i].xaoTronDapAn = !this.answers[i].xaoTronDapAn;
      }
    });
  }


  configAnswer = {
    // startupFocus: true,
    height: 60,
    allowedContent: true,

    extraPlugins: ckConfig.extraPlugins,
    toolbarGroups: ckConfig.toolbarAnswer,
    removeButtons: ckConfig.removeButtonsAnswer,
  };
  ngAfterViewInit(): void {
    this.ck.ready.subscribe(() => {
      this.ck.instance.on('afterCommandExec', (event) => {
        this.handleAfterCommandExec(event, this.ck.instance)
      });
      this.ck.instance.addCommand('taophuongan', {
        exec: function (editor) {
        }
      });
    });
    // ẩn toolbar ckedtior
    this.cks.changes.subscribe((x) => {
      this.initAnswer(x.last);
    });

    this.cks.forEach((x) => {
      this.initAnswer(x);
    });
    //reload edit
    this.ck.ready.subscribe(() => {
      var head = this.ck.instance.document.getHead();
      var myscript = this.ck.instance.document.createElement('script', {
        attributes: {
          type: 'text/javascript',
          'src': 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-MML-AM_CHTML'
        }
      });
      head.append(myscript);
    });
  }
  handleAfterCommandExec(event, editor) {
    var commandName = event.data.name;
    // Kiểm tra sự kiện sau khi click tag U
    // For 'underline' commmand
    if (commandName == "taophuongan") {
      var mySelection = this.ck.instance.getSelection();

      let selectedText = mySelection.getNative();
      let editor = this.ck.instance;
      let realSelectedText = getSelectionHTML(selectedText).toString();
      // if (mySelection.getType() == 2) {

      if (mySelection.getType() == CKEDITOR.SELECTION_TEXT) {
        //XLS
        if (CKEDITOR.env.ie) {
          mySelection.unlock(true);
          selectedText = mySelection.getNative();
        } else {
          selectedText = mySelection.getNative();
        }

        realSelectedText = selectedText.toString();


        if (realSelectedText.trim().length == 0) {
          return false;
        }
      }

      var currentTag;
      if (CKEDITOR.env.ie) {
        mySelection.unlock(true);
        selectedText = mySelection.getNative();
      } else {
        selectedText = mySelection.getNative();
      }
      if (realSelectedText.trim() == "") {
        return false;
      }


      // if (isNullOrWhiteSpace(selectedText.toString().trim()) || isNullOrEmpty(selectedText.toString().trim())) {
      //   alert('Vui lòng chọn văn bản');
      //   return;
      // }
      if (getSelectionHTML(selectedText).indexOf('class="Blanking') != -1 ||
        ($(editor.getSelectedHtml().$).is('span') &&
          $(editor.getSelectedHtml().getHtml()).hasClass('Blanking'))) {
        alert('Vùng chọn đã có phương án');
        return;
      }

      //insert blank, thay bằng index của phần điền khuyết vào editor
      let insertedBlank = editor.document.createElement('span');
      insertedBlank.setAttribute('data-current-answer', '');
      insertedBlank.setAttribute('class', 'Blanking Blanking-' + this.CauHoiID); //CauHoiID được truyền vào từ View MissingWordAdd(Edit)
      insertedBlank.setAttribute('data-position', this.currentBlankID); //currentBlankID được truyền vào từ View MissingWordAdd(Edit)
      //insertedBlank.setAttribute('style', 'font-weight: bold; border-bottom: 1px solid #c0c0c0; min-width: 50px; display:inline-block; text-align: center;');
      //ngăn không cho người dùng chỉnh sửa phần điền khuyết
      insertedBlank.setAttribute('contenteditable', 'false');
      insertedBlank.appendText('(' + this.currentBlankID.toString() + ')');
      editor.insertElement(insertedBlank);

      this.reorderAnswerIndexes(this.currentBlankID, editor, realSelectedText);

      this.currentBlankID++;

      // } else {
      //   alert('vui lòng chỉ chọn văn bản');
      //   return;
      // }
    }
  }
  SortedTrueAnswers() {
    return this.answers.filter(x => x.dapAnDung != -1).sort((a, b) => a.dapAnDung > b.dapAnDung ? 1 : a.dapAnDung < b.dapAnDung ? -1 : 0);
  }
  FakeAnswers() {
    return this.answers.filter(x => x.dapAnDung == -1);
  }
  reorderAnswerIndexes(currentMaxID, editor, realSelectedText) {

    let ckeEditableArea = $(editor.editable().$);
    let blankIndexElements = ckeEditableArea.find('span[class^="Blanking"]');
    let insertedPosition = 0;
    blankIndexElements.each(function (index, element) {
      if ($(element).attr('data-position') == currentMaxID) {
        insertedPosition = index + 1;
      }
    });

    $(ckeEditableArea.find('span[class^="Blanking"]:gt(' + (insertedPosition - 1) + ')').get().reverse()).each(function (index, element) {
      let oldIndex = parseInt($(element).attr('data-position'));
      $(element).attr('data-position', oldIndex + 1);
      $(element).text('(' + (oldIndex + 1) + ')');
      // let dongPhuongAnDungHienTai = $('.phuong-an-dung > table > tbody > tr[data-position="' + oldIndex + '"]');
      // dongPhuongAnDungHienTai.attr('data-position', oldIndex + 1);
      // dongPhuongAnDungHienTai.find('*[blank-index]').text((oldIndex + 1));
    });

    ckeEditableArea.find('span[class^="Blanking"]').each(function (index, element) {
      if ((index + 1) == insertedPosition) {
        $(element).attr('data-position', insertedPosition);
        $(element).text('(' + insertedPosition + ')');
      }
    });
    if (this.answers.length >= 1) {
      this.changeSTT(insertedPosition);
    }
    this.addAnswer(realSelectedText, insertedPosition);
    console.log("tạo", realSelectedText);
  }
  SaveAndCreate() {
    window.location.reload();
    alert("Đã lưu")
  }
  private initAnswer(ck: CKEditorComponent) {
    if (ck) {
      ck.focus.subscribe((ev) => {
        document.getElementById(ck.instance.id + '_top').style.display = 'block';
      });
      ck.blur.subscribe((ev) => {
        document.getElementById(ck.instance.id + '_top').style.display = 'none';
      });
    }
  }
}

import { Component, NgZone, OnInit } from '@angular/core';
import { QuestionBankService } from 'src/app/core/services/question-bank/question-bank.service';

@Component({
  selector: 'app-content-file-word',
  templateUrl: './content-file-word.component.html',
  styleUrls: ['./content-file-word.component.scss']
})
export class ContentFileWordComponent implements OnInit {

  questions = [];
  isExportWordFile = true;
  constructor(private questionService: QuestionBankService, private _zone: NgZone) { }
  ngOnInit(): void {
    this.getQuestionData();
  }
  getQuestionData() {
    this.questionService.getQuestionData().subscribe(res => {
      this._zone.run(() => {
        res.forEach((r) => {
          setTimeout(() => {
            this.questions.push(r);
          }, 100);
        })
      });
    });
  }

  public download(): void {

    var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' " +
      "xmlns:w='urn:schemas-microsoft-com:office:word' " +
      "xmlns='http://www.w3.org/1998/Math/MathML' " +
      "<?xml version='1.0'?>" +
      "xmlns='http://www.w3.org/TR/REC-html40'>" +
      "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
    var footer = "</body></html>";
    debugger
    var test  = document.getElementById("source-html").innerHTML;
    console.log(test)

    var sourceHTML = header + document.getElementById("source-html").innerHTML + footer;

    var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
    var fileDownload = document.createElement("a");
    document.body.appendChild(fileDownload);
    fileDownload.href = source;
    fileDownload.download = 'document.doc';
    fileDownload.click();
    document.body.removeChild(fileDownload);
  }
}

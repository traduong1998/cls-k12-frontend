import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentFileWordComponent } from './content-file-word.component';

describe('ContentFileWordComponent', () => {
  let component: ContentFileWordComponent;
  let fixture: ComponentFixture<ContentFileWordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentFileWordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentFileWordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSingleChoiceQuestionComponent } from './create-single-choice-question.component';

describe('CreateSingleChoiceQuestionComponent', () => {
  let component: CreateSingleChoiceQuestionComponent;
  let fixture: ComponentFixture<CreateSingleChoiceQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateSingleChoiceQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSingleChoiceQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnChanges, OnDestroy, OnInit, Output, QueryList, SimpleChange, SimpleChanges, ViewChild, ViewChildren } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { delay } from 'rxjs/operators';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { SingleChoiceAnswer } from 'src/app/shared/modules/question/Answer';
import { Question } from 'src/app/shared/modules/question/Question';
import { ckConfig } from 'src/environments/ckeditorConfig';

import { SaveDataService } from '../../Services/save-data.service';


@Component({
  selector: 'app-create-single-choice-question',
  templateUrl: './create-single-choice-question.component.html',
  styleUrls: ['./create-single-choice-question.component.scss']
})
export class CreateSingleChoiceQuestionComponent implements OnInit {
  @Input() config: number;
  @Input() type: string;
  @Input() levelOfQuestion: string;
  @Input() isAutoApproval: boolean;
  @Input() content: any;
  @Input() answer: any;
  @Input() dataQuestion;
  selectedAnswer = "";
  //@ViewChild("cktest") cktest : CKEditorComponent;
  ckeditorContentTest: string;

  isDelele = false;
  flexLayout;
  ckeditorContent = "";
  typeOfQuestion;
  level;
  isAuto;
  configQuestion = {};
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  question: Question;

  constructor(private saveData: SaveDataService) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: true,
      allowedContent: true,

      extraPlugins: ckConfig.extraPlugins,
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
    };
  }


  destroyCk() {
    console.log(`destroyCk`);
    this.ck.instance.destroy();
    this.ck = null;
  }

  ngOnInit(): void {
    if ((this.dataQuestion != null || this.dataQuestion != undefined)) {
      if (this.dataQuestion.listAnswer.length != 0) {
        this.ckeditorContent = this.dataQuestion.content
        this.answers = this.dataQuestion.listAnswer;
        this.answers.map((ans, i) => {
          if (ans.dapAnDung >= 1) {
            this.selectedAnswer = "checkboxG_" + i;
          }
        });
      }
    }
    if (this.config == 1) {
      this.flexLayout = 'row'
    }
    else if (this.config == 2) {
      this.flexLayout = 'column'
    }
    this.typeOfQuestion = this.type;
    this.level = this.levelOfQuestion;
    this.isAuto = this.isAutoApproval;
  }
  answers: SingleChoiceAnswer[] = [
    { id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 0, xaoTronDapAn: true },
    { id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 0, xaoTronDapAn: true },
    { id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 0, xaoTronDapAn: true },
    { id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 0, xaoTronDapAn: true },
  ];

  setQuestion() {
    let err = 0;
    delay(1000)

    var check: boolean;
    check = this.answers.some(x => (isNullOrEmpty(x.NoiDung)));
    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      err++;
      alert("Bạn chưa nhập nội dung câu hỏi  của câu một lựa chọn");
      return { content: err.toString(), listAnswer: [] };
    }
    else if (check == true) {
      err++;
      alert("Bạn chưa nhập nội dung câu trả lời  của câu một lựa chọn");
      return { content: err.toString(), listAnswer: [] };
    }
    else if (this.answers.some(x => x.dapAnDung == 1) == false) {
      err++;
      alert("Bạn chưa chọn câu trả lời đúng");
      return { content: err.toString(), listAnswer: [] };
    }
    else {
      let questionOfGroup = { content: this.ckeditorContent, listAnswer: this.answers };
      return questionOfGroup;
    }
  }

  getvalue() {
    var check: boolean;
    check = this.answers.some(x => (isNullOrEmpty(x.NoiDung)));
    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
    }
    else if (check === true) {
      alert("Bạn chưa nhập nội dung câu trả lời");
    }
    else if (this.answers.some(x => x.dapAnDung == 1) == false) {
      alert("Bạn chưa chọn câu trả lời đúng");
    }
    else {
      //save question
      this.question = { id: 1, content: this.ckeditorContent, type: 'singlechoice', level: this.levelOfQuestion, isAutoApproval: this.isAutoApproval, listAnswer: this.answers };
      this.saveData.getQuestion(this.question);
      //save ansswer
      this.saveData.getAnswer(this.answers);
      alert("lưu thành công!!!")
      console.log(this.question);
    }
  }
  addAnswer() {
    if (this.answers.length < 9) {
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 0, xaoTronDapAn: true });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }
  setValueAnswer(i, event) {
    //console.log("chnage",event);
    //if(typeof this.answers != 'undefined'){}
    this.answers[i].NoiDung = event.value;
  }
  changeSelectAnswer(i) {
    this.answers.map(ans => ans.dapAnDung = 0);
    this.answers[i].dapAnDung = 1;
  }

  deleteAnswer(i) {
    this.answers.splice(i, 1);
    if (this.answers.length == 1) {
      this.isDelele = true;
    }
  }
  ChangeShuffler(i) {
    this.answers[i].xaoTronDapAn = !this.answers[i].xaoTronDapAn;
  }
  configAnswer = {
    // startupFocus: true,
    height: 60,
    allowedContent: true,
    fullPage: true,
    extraPlugins: ckConfig.extraPlugins,
    toolbarGroups: ckConfig.toolbarAnswer,
    removeButtons: ckConfig.removeButtonsAnswer,
  };
  @ViewChild("ck") ck: CKEditorComponent;
  @ViewChildren("cks") cks: QueryList<CKEditorComponent>;

  ngAfterViewInit(): void {
    this.cks.changes.subscribe((x) => {
      this.initAnswer(x.last);
    });

    this.cks.forEach((x) => {
      this.initAnswer(x);
    });
    //reload edit
    this.ck.ready.subscribe(() => {
      var head = this.ck.instance.document.getHead();
      var myscript = this.ck.instance.document.createElement('script', {
        attributes: {
          type: 'text/javascript',
          'src': 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-MML-AM_CHTML'
        }
      });
      head.append(myscript);
    });
  }


  private initAnswer(ck: CKEditorComponent) {
    ck.ready.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'none';
    });
    ck.focus.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'block';
    });
    ck.blur.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'none';
    });
  }
  SaveAndCreate() {
    window.location.reload();
    alert("Đã lưu")
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEssayQuestionComponent } from './create-essay-question.component';

describe('CreateEssayQuestionComponent', () => {
  let component: CreateEssayQuestionComponent;
  let fixture: ComponentFixture<CreateEssayQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateEssayQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEssayQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { Question } from 'src/app/shared/modules/question/Question';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { LoadContentService } from '../../Services/load-content.service';

@Component({
  selector: 'app-create-essay-question',
  templateUrl: './create-essay-question.component.html',
  styleUrls: ['./create-essay-question.component.scss']
})
export class CreateEssayQuestionComponent implements OnInit {
  configQuestion={};
  @Input() dataQuestion;
  ckeditorContent="";
  //get type,level
  type='essay';
  level='NB';
  isAutoApproval=false;
  question:Question;
  @ViewChild("ck") ck: CKEditorComponent
  constructor(private loadData:LoadContentService) {
    this.configQuestion = {
      contentsCss :  window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: true,
      allowedContent: true,
      extraPlugins:ckConfig.extraPlugins,
      toolbarGroups:ckConfig.toolbarGroups,
      removeButtons:ckConfig.removeButtons,
    };
   }

  ngOnInit(): void {
    this.loadData.levelfQuestion$.subscribe(level=>{
      if(level=='NB'){
        this.level='NB';
      }else if(level=='TH'){
        this.level='TH';
      }
      else if(level=='VD'){
        this.level='VD';
      }
      else if(level=='VDC'){
        this.level='VDC';
      }
    });

    this.loadData.isAutoApproval$.subscribe(isAuto=>{
      if(isAuto==true){
        this.isAutoApproval=true;
      }else{
        this.isAutoApproval=false;
      }
    });

    if(this.dataQuestion!=null){
      this.ckeditorContent = this.dataQuestion.content;
    }

  }
  ngAfterViewInit(): void {
     this.ck.ready.subscribe(()=>{
      var head = this.ck.instance.document.getHead();
      var myscript = this.ck.instance.document.createElement( 'script', {
          attributes : {
          type : 'text/javascript',
          'src' : 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-MML-AM_CHTML'
        }
      });
      head.append( myscript );
    });
  }
  getvalue(){
    let editor = this.ck.instance;
    let questionContent = editor.document.getBody().getText();
    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
    }
    else{
      this.question={id:1,content:this.ckeditorContent,type:this.type,level:this.level,isAutoApproval:this.isAutoApproval,listAnswer:null};
      console.log(this.question);
      alert("Lưu thành công!!!")
    }
  }
}

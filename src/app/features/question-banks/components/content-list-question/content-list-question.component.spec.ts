import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentListQuestionComponent } from './content-list-question.component';

describe('ContentListQuestionComponent', () => {
  let component: ContentListQuestionComponent;
  let fixture: ComponentFixture<ContentListQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentListQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentListQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { AfterViewInit, Component, NgZone, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {EditQuestionComponent} from '../edit-question/edit-question.component'
import { QuestionBankService } from 'src/app/core/services/question-bank/question-bank.service';

@Component({
  selector: 'app-content-list-question',
  templateUrl: './content-list-question.component.html',
  styleUrls: ['./content-list-question.component.scss']
})
export class ContentListQuestionComponent implements OnInit {

  constructor(private questionService: QuestionBankService,private dialog: MatDialog,private _zone:NgZone) { }
  questions=[];
  CheckedIds=[];
  checkAll=false;
  isCheckAll=false;
  ngOnInit(): void {
    this.getQuestionData();

  }
  checkChild(){
    this.isCheckAll=!this.checkAll;
    if(this.isCheckAll==true){
      this.CheckedIds=[];
      this.questions.map((q)=>{
        this.CheckedIds.push(q.questionId);
      });
    }
  }
  getQuestionData() {
    this.questionService.getQuestionData().subscribe(res => { 
      this._zone.run(()=>{
        res.forEach((r)=>{
          setTimeout(()=>{
            this.questions.push(r);
          },100);
          this.hideloader();
        })
      });
      console.log(`getQuestionData -> add question`,this.questions);
     });
  }
  checkedOne(event,questionId){
    if(event.checked==true){
      this.CheckedIds.push(questionId);
    }else{
     let index=this.CheckedIds.indexOf(questionId);
     this.CheckedIds.splice(index,1);
    }
    if(this.CheckedIds.length==this.questions.length){
      this.checkAll=true;
    }else{
      this.checkAll=false;
    }
  }
  EditQuestion(data){
    //show dialog
    this.dialog.open(EditQuestionComponent,{panelClass: 'my-full-screen-dialog',data});
  }
  deleteQuestion(question){
    if(confirm("Bạn có chắc muốn xóa câu hỏi này? ")) {
     let index= this.questions.indexOf(question);
     this.questions.splice(index,1);
    }
  }
  deleteSelectedQuestion(){
   this.CheckedIds.map((id)=>{
     let question= this.questions.find(x=>x.questionId==id);
     this.questions.splice(this.questions.indexOf(question),1);
   })
   this.CheckedIds=[];
   this.checkAll=false;
  }
  hideloader() {
    document.getElementById('loading')
      .style.display = 'none';
  }
}

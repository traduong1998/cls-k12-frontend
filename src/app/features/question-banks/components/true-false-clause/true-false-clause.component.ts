import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-true-false-clause',
  templateUrl: './true-false-clause.component.html',
  styleUrls: ['./true-false-clause.component.scss']
})
export class TrueFalseClauseComponent implements OnInit {
  @Input() dataQuestion;
  @Input() isExportWordFile
  defaultMath = `<math xmlns="http://www.w3.org/1998/Math/MathML"`;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  constructor() { }

  ngOnInit(): void {
  }

}

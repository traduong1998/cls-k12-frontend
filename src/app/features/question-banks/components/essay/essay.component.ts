import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-essay',
  templateUrl: './essay.component.html',
  styleUrls: ['./essay.component.scss']
})
export class EssayComponent implements OnInit {
  @Input() dataQuestion
  @Input() isExportWordFile
  constructor() { }

  ngOnInit(): void {
  }

}

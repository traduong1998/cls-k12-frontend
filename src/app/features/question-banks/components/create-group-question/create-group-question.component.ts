import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CKEditorComponent, CKEditorModule  } from 'ckeditor4-angular';
import { delay } from 'rxjs/operators';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { ckConfig } from 'src/environments/ckeditorConfig';
import {QuestionGroup,Question} from '../../../../shared/modules/question/Question'
import { Level, QuestionTypeGroup } from '../../Interfaces/questionType.model';
import { LoadContentService } from '../../Services/load-content.service';
import { CreateMissingWord1QuestionComponent } from '../create-missing-word1-question/create-missing-word1-question.component';
import { CreateMultipleChoiceQuestionComponent } from '../create-multiple-choice-question/create-multiple-choice-question.component';
import { CreateSingleChoiceQuestionComponent } from '../create-single-choice-question/create-single-choice-question.component';
import { CreateTrueFalseClauseQuestionComponent } from '../create-true-false-clause-question/create-true-false-clause-question.component';
import { CreateTrueFalseQuestionComponent } from '../create-true-false-question/create-true-false-question.component';
import { GeneralQuestionComponent } from '../general-question/general-question.component';
@Component({
  selector: 'app-create-group-question',
  templateUrl: './create-group-question.component.html',
  styleUrls: ['./create-group-question.component.scss']
})
export class CreateGroupQuestionComponent implements OnInit {
  @Input() dataQuestions;
  ckeditorContent="";
  ckeQuestionConfig={};
  isAutoApproval;
  isShuffleChild=true;
  id:number;
  name:string;
  questionGroup: QuestionGroup = {
    id:1,globalContent:'',isShuffleChild:true,
    Questions:[   
      {id:generateRandomUniqueInteger(1000,9999),content:'',type:'singlechoice',isAutoApproval:false,level:'NB',listAnswer:[]},
    ]
  };
  @ViewChildren('singleChoice') singleChoices: QueryList<CreateSingleChoiceQuestionComponent>
  @ViewChildren('multichoice') multiChoice: QueryList<CreateMultipleChoiceQuestionComponent>
  @ViewChildren('underline') underline: QueryList<CreateTrueFalseQuestionComponent>
  @ViewChildren('truefalse') truefalse: QueryList<CreateTrueFalseClauseQuestionComponent>
  @ViewChildren('truefalseclause') truefalseclause: QueryList<CreateTrueFalseQuestionComponent>
  @ViewChildren('missingword') missingword: QueryList<CreateMissingWord1QuestionComponent>

  @ViewChild("ck") ck : CKEditorComponent;
  selectedQuestion='1';
  constructor(private loadData:LoadContentService) { 
    this.ckeQuestionConfig = {
      contentsCss :  window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: true,
      allowedContent: true,
      extraPlugins:ckConfig.extraPlugins,
      toolbarGroups:ckConfig.toolbarGroups,
      removeButtons:ckConfig.removeButtonsFillBlank,
    };
  }
  questionTypeGroups: QuestionTypeGroup[] = [
    {
      name: 'Câu hỏi trắc nghiệm',
      types: [
        {value: 'singlechoice', viewValue: 'Trắc nghiệm một lựa chọn'},
        {value: 'multichoice', viewValue: 'Trắc nghiệm nhiều lựa chọn'},
        {value: 'underline', viewValue: 'Gạch chân'},
        {value: 'truefalse', viewValue: 'Lựa chọn đúng sai'},
        {value: 'truefalseclause', viewValue: 'Mệnh đề đúng sai'},
        {value: 'fillblank', viewValue: 'Điền khuyết'},
        {value: 'fillblank2', viewValue: 'Điền khuyết loại 2'},
        {value: 'matching', viewValue: 'Ghép đôi'},
      ]
    },
    {
      name: 'Câu hỏi tự luận',
      types: [
        {value: 'essay', viewValue: 'Tự luận'},

      ]
    }
  ];
  levels: Level[]=[
    {value: 'NB', viewValue: 'Nhận biết'},
    {value: 'TH', viewValue: 'Thông hiểu'},
    {value: 'VD', viewValue: 'Vận dụng'},
    {value: 'VDC', viewValue: 'Vận dụng cao'},
   ];
  ngOnInit(): void {
    //debugger
    if(this.dataQuestions!=null){
      this.ckeditorContent = this.dataQuestions.generalRequirements;
      // console.log("test 1",this.dataQuestions.subQuestion);
      // console.log("tets 2",this.dataQuestions)
      this.questionGroup.Questions = this.dataQuestions.subQuestion;
    
    }

    this.loadData.isAutoApproval$.subscribe(isAuto=>{
      if(isAuto==true){
        this.isAutoApproval=true;
      }else{
        this.isAutoApproval=false;
      }
    });
  }
  ngAfterViewInit(): void {
     //reload edit
     this.ck.ready.subscribe(()=>{
      var head = this.ck.instance.document.getHead();
      var myscript = this.ck.instance.document.createElement( 'script', {
          attributes : {
          type : 'text/javascript',
          'src' : 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-MML-AM_CHTML'
        }
      });
      head.append( myscript );
    });
  }
 
  AddQuestion(){
    this.questionGroup.Questions.push( {id:generateRandomUniqueInteger(1000,9999),content:"",type:'singlechoice',isAutoApproval:false,level:'NB',listAnswer:[]})
    
  }
  SubQuestion(index){
    if(parseInt(index)<=1){
      alert("Bạn phải có tối thiếu 1 câu hỏi");
    }
    else{

      if(index==this.questionGroup.Questions.length){
        this.selectedQuestion=(index-1).toString();
      }
      this.questionGroup.Questions.splice(parseInt(index)-1, 1);
    }
  }
  changeShuffleChild(event){
    this.isShuffleChild =event.checked;
  }
  changeTypeQuestion(i){
    this.questionGroup.Questions[i].level='NB';
  }

  show(){
    let editor = this.ck.instance;
    let questionContent = editor.document.getBody().getText();

    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      alert("Bạn chưa nhập nội dung câu hỏi chung");
    }
    else{
      let err=0;
      let countSingle =0;
      let countMulti =0;
      let countUnderline=0;
      let countTruefalse=0;
      let countTrueFalseClause=0;
      let countMissingWord=0
      this.questionGroup.globalContent = this.ckeditorContent;
      this.questionGroup.isShuffleChild = this.isShuffleChild;
      for(let i in this.questionGroup.Questions){
        delay(1000);
        if(this.singleChoices.toArray().length > 0 && this.questionGroup.Questions[i].type=='singlechoice'){
          let questionOfGroup = this.singleChoices.toArray()[countSingle].setQuestion();
          if(Number(questionOfGroup.content)==1){
              err++;
              alert("Bạn nhập thiếu thông tin ở câu "+ (parseInt(i)+1));
              break;
          } 
          else{
            this.questionGroup.Questions[i].content= questionOfGroup.content;
            this.questionGroup.Questions[i].listAnswer = questionOfGroup.listAnswer;
          }
          countSingle++;
        }
        if(this.multiChoice.toArray().length>0 && this.questionGroup.Questions[i].type=='multichoice') {
          let questionOfGroup = this.multiChoice.toArray()[countMulti].setQuestion();
          if(Number(questionOfGroup.content)==1){
            err++;
            alert("Bạn nhập thiếu thông tin ở câu "+ (parseInt(i)+1));
            break;
          }
          else{
            this.questionGroup.Questions[i].content= questionOfGroup.content;
            this.questionGroup.Questions[i].listAnswer = questionOfGroup.PhuongAn;
          }
          countMulti++;
        }
        if(this.underline.toArray().length>0 && this.questionGroup.Questions[i].type=='underline'){
          let questionOfGroup = this.underline.toArray()[countUnderline].setQuestion();       
          if(Number(questionOfGroup.content)==1){
            err++;
            alert("Bạn nhập thiếu thông tin ở câu "+ (parseInt(i)+1));
            break;
          }
          else{
            this.questionGroup.Questions[i].content= questionOfGroup.content;
            this.questionGroup.Questions[i].listAnswer = questionOfGroup.PhuongAn;
          }
          countUnderline++;
        }
        if(this.truefalse.toArray().length>0 && this.questionGroup.Questions[i].type=='truefalse'){
          let questionOfGroup = this.truefalse.toArray()[countTruefalse].setQuestion();
          if(Number(questionOfGroup.content)==1){
            err++;
            alert("Bạn nhập thiếu thông tin ở câu "+ (parseInt(i)+1));
            break;
          }
          else{
            this.questionGroup.Questions[i].content= questionOfGroup.content;
            this.questionGroup.Questions[i].listAnswer = questionOfGroup.PhuongAn;
          }
          countTruefalse++;
        }
        if(this.truefalseclause.toArray().length>0 && this.questionGroup.Questions[i].type=='truefalseclause'){
          let questionOfGroup = this.truefalseclause.toArray()[countTrueFalseClause].setQuestion();
          if(Number(questionOfGroup.content)==1){
            err++;
            alert("Bạn nhập thiếu thông tin ở câu "+ (parseInt(i)+1));
            break;
          }
          else{
            this.questionGroup.Questions[i].content= questionOfGroup.content;
            this.questionGroup.Questions[i].listAnswer = questionOfGroup.PhuongAn;
          }
          countTrueFalseClause++;
        }
        if(this.missingword.toArray().length>0 && this.questionGroup.Questions[i].type=='fillblank'){
          let questionOfGroup = this.missingword.toArray()[countMissingWord].setQuestion();
          if(Number(questionOfGroup.content)==1){
            err++;
            alert("Bạn nhập thiếu thông tin ở câu "+ (parseInt(i)+1));
            break;
          }
          else{
            this.questionGroup.Questions[i].content= questionOfGroup.content;
            this.questionGroup.Questions[i].listAnswer = questionOfGroup.PhuongAn;
          }
          countMissingWord++;
        }
      }
      if(err==0){
        console.log(this.questionGroup);
        alert("lưu thành công!!!")
      }
      else{
        alert("Lưu thất bại")
      }
    }
  }
  SaveAndCreate(){
    window.location.reload();
    alert("Đã lưu")
  }
}

import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { LoadContentService } from '../../Services/load-content.service';
import { CreateMultipleChoiceQuestionComponent } from '../create-multiple-choice-question/create-multiple-choice-question.component';
import { CreateSingleChoiceQuestionComponent } from '../create-single-choice-question/create-single-choice-question.component';
declare var CKEDITOR: any;

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit,OnDestroy,AfterViewInit{
  formatOfQuestion = 1;
  typeOfQuestion = 'singlechoice';
  levelOfQuestion = 'NB';
  isAutoApproval = false;
  @ViewChild("ck") ck: CKEditorComponent;

  //@ViewChild("cktest") cktest : CKEditorComponent;
  @ViewChild("questionComponentTest") questionComponentTest : CreateSingleChoiceQuestionComponent;
  @ViewChild("muilti") muilti : CreateMultipleChoiceQuestionComponent;
  //hideCKtest = true;

  constructor(private _formatOfQuestion: LoadContentService) { }
  
  ngAfterViewInit(): void {
    // this.cktest.instance.on('instanceReady', function () {
    //   console.log(`instanceReady`);
    // });
    // this.cktest.instance.on('focus', () => {
    //   console.log(`focus`);
    // });
    // this.cktest.instance.on('blur', () => {
      
    // });
  }

  // hideCK(){
  //   console.log(`hideCK CKEDITOR.instances`,CKEDITOR.instances);

  //   if(this.hideCKtest){
  //     if(this.cktest){
  //       console.log(`hideCK`,this.cktest.instance);
  //       this.cktest.instance.destroy();
  //       this.cktest.instance = null;
  //     }
  
  //     console.warn(`questionComponentTest`,this.questionComponentTest);
  //     console.warn(`this.questionComponentTest.cktest.instance`,this.questionComponentTest.cktest.instance);
  //     this.questionComponentTest.destroyCk();
  //   }

  //   this.hideCKtest = !this.hideCKtest;
  //   console.log(`hideCKtest`,this.hideCKtest);
  // }

  ngOnDestroy(): void {
    // console.log(`ngOnDestroy cktest`,this.cktest);
    // this.cktest.ngOnDestroy();
    // this.cktest = null;
    // console.log(`ngOnDestroy cktest`,this.cktest);
  }


  ngOnInit(): void {
    this._formatOfQuestion.levelfQuestion$.subscribe(level => {
      if (level == 'NB') {
        this.levelOfQuestion = 'NB';
      } else if (level == 'TH') {
        this.levelOfQuestion = 'TH';
      }
      else if (level == 'VD') {
        this.levelOfQuestion = 'VD';
      }
      else if (level == 'VDC') {
        this.levelOfQuestion = 'VDC';
      }
    });

    this._formatOfQuestion.isAutoApproval$.subscribe(isAuto => {
      if (isAuto == true) {
        this.isAutoApproval = true;
      } else {
        this.isAutoApproval = false;
      }
    })

    this._formatOfQuestion.type$.subscribe(type => {
      if (type == 'single') {
        this.formatOfQuestion = 1;
      } else if (type == 'group') {
        this.formatOfQuestion = 2;
      }
    });
   
    this._formatOfQuestion.typeQuestion$.subscribe(type => {
      //debugger
      //console.warn(`typeQuestion$ subscribe`);
     
      //this.muilti.destroyCk();
      // this.cktest.instance.
      //this.ck.instance.();
      //this.typeOfQuestion =type;
      // this.questionComponentTest.destroyCk();
      // this.muilti.destroyCk();
      if (type == 'singlechoice') {
        this.typeOfQuestion = 'singlechoice';
      } else if (type == 'multichoice') {
        this.typeOfQuestion = 'multichoice';
      }
      else if (type == 'underline') {
        this.typeOfQuestion = 'underline';
      }
      else if (type == 'truefalse') {
        this.typeOfQuestion = 'truefalse';
      }
      else if (type == 'truefalseclause') {
        this.typeOfQuestion = 'truefalseclause';
      }
      else if (type == 'fillblank') {
        this.typeOfQuestion = 'fillblank';
      }
      else if (type == 'fillblank2') {
        this.typeOfQuestion = 'fillblank2';
      }
      else if (type == 'matching') {
        this.typeOfQuestion = 'matching';
      }
      else if (type == 'essay') {
        this.typeOfQuestion = 'essay';
      }
    });
  }

}

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-matching',
  templateUrl: './matching.component.html',
  styleUrls: ['./matching.component.scss']
})
export class MatchingComponent implements OnInit {
  @Input() dataQuestion;
  @Input() isExportWordFile;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  answers;
  defaultMath=`<math xmlns="http://www.w3.org/1998/Math/MathML"`;
  constructor() { }

  ngOnInit(): void {
    this.answers = this.dataQuestion.listAnswer.reduce((r, a) => {
      r[a.viTri] = [...r[a.viTri] || [], a];
      return r;
     }, {});

     console.log(this.answers)
  }
  groupAnswers(){
    return [...new Set(this.dataQuestion.listAnswer.map(item => item.dapAnDung))]
  }
  answersOfGroup(index){
    return this.dataQuestion.listAnswer.filter(x=>x.dapAnDung==index);
  }
}

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-group-question',
  templateUrl: './group-question.component.html',
  styleUrls: ['./group-question.component.scss']
})
export class GroupQuestionComponent implements OnInit {
  @Input() dataQuestion
  @Input() isExportWordFile;
  constructor() { }

  ngOnInit(): void {
  }

}

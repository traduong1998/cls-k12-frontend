import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MissingWord2Component } from './missing-word2.component';

describe('MissingWord2Component', () => {
  let component: MissingWord2Component;
  let fixture: ComponentFixture<MissingWord2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MissingWord2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MissingWord2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-missing-word2',
  templateUrl: './missing-word2.component.html',
  styleUrls: ['./missing-word2.component.scss']
})
export class MissingWord2Component implements OnInit {
  @Input() dataQuestion;
  @Input() isExportWordFile;
  dataAnswers;
  defaultMath=`<math xmlns="http://www.w3.org/1998/Math/MathML"`;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  constructor() { }

  ngOnInit(): void {
    console.log(this.dataQuestion.listAnswer)
  }
  SortedAnswers(){
    return this.dataQuestion.listAnswer.sort((a,b)=>a.dapAnDung>b.dapAnDung?1:a.dapAnDung<b.dapAnDung?-1:0);
  }
  groupAnswers(){
    return [...new Set(this.SortedAnswers().map(item => item.dapAnDung))]
  }
  answersOfGroup(index){
    return this.dataQuestion.listAnswer.filter(x=>x.dapAnDung==index);
  }
}

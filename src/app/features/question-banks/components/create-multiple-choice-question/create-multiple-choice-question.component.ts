import { Component, Input, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { MultiChoiceAnswer } from 'src/app/shared/modules/question/Answer';
import { Question } from 'src/app/shared/modules/question/Question';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { Answer } from '../../Interfaces/answer.model';
import { SaveDataService } from '../../Services/save-data.service';

@Component({
  selector: 'app-create-multiple-choice-question',
  templateUrl: './create-multiple-choice-question.component.html',
  styleUrls: ['./create-multiple-choice-question.component.scss']
})
export class CreateMultipleChoiceQuestionComponent implements OnInit {
  @Input() dataQuestion;
  @Input() config: number;
  flexLayout;
  @Input() type: string;
  @Input() levelOfQuestion: string;
  @Input() isAutoApproval: boolean;

  @Input() content: any;
  @Input() answer: any;

  isDelele = false;
  ckeditorContent = "";
  typeOfQuestion;
  level;
  isAuto;
  question: Question;
  configQuestion = {};

  constructor(private saveData: SaveDataService) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: true,
      allowedContent: true,

      extraPlugins: ckConfig.extraPlugins,
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
    };
  }
  destroyCk() {
    console.log(`destroyCk`);
    this.ck.instance.destroyed;
    //this.ck.instance = null;
  }

  ngOnInit(): void {

    if (this.config == 1) {
      this.flexLayout = 'row'
    }
    else if (this.config == 2) {
      this.flexLayout = 'column'
    }
    this.typeOfQuestion = this.type;
    this.level = this.levelOfQuestion;
    this.isAuto = this.isAutoApproval;

    if (this.dataQuestion != null) {
      this.ckeditorContent = this.dataQuestion.content;
      this.answers = this.dataQuestion.listAnswer;
    }
  }

  setAnswers() {
    return this.answers;
  }

  setQuestion() {
    let err = 0;

    var check: boolean;
    check = this.answers.some(x => (isNullOrEmpty(x.NoiDung)));
    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      err++;
      alert("Bạn chưa nhập nội dung câu hỏi của câu nhiều lựa chọn");
      return { content: err.toString(), PhuongAn: [] };
    }
    else if (check == true) {
      err++;
      alert("Bạn chưa nhập nội dung câu trả lời của câu nhiều lựa chọn");
      return { content: err.toString(), PhuongAn: [] };
    }
    else if (this.answers.some(x => x.dapAnDung == 1) == false) {
      err++;
      alert("Bạn chọn ít nhất 1 câu trả lời đúng của câu nhiều lựa chọn");
      return { content: err.toString(), PhuongAn: [] };
    }
    else {
      let questionOfGroup = { content: this.ckeditorContent, PhuongAn: this.answers };
      return questionOfGroup;
    }
    //return this.ckeditorContent;
  }

  getvalue() {
    var check: boolean;
    check = this.answers.some(x => (isNullOrEmpty(x.NoiDung)));
    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
    }
    else if (check == true) {
      alert("Bạn chưa nhập nội dung câu trả lời");
    }

    else if (this.answers.some(x => x.dapAnDung == 1) == false) {
      alert("Bạn chọn ít nhất 1 câu trả lời đúng");
    }
    else {
      //save question
      this.question = { id: 1, content: this.ckeditorContent, type: 'multichoice', level: this.levelOfQuestion, isAutoApproval: this.isAutoApproval, listAnswer: this.answers };
      this.saveData.getQuestion(this.question);
      //save ansswer
      this.saveData.getAnswerMuti(this.answers);
      console.log(this.question);
      alert("lưu thành công!!!")
    }
  }

  changeSelectAnswer(i, event) {

    if (event.checked == true) {
      this.answers[i].dapAnDung = 1;
    }
    else {
      this.answers[i].dapAnDung = 0;
    }
  }

  addAnswer() {
    if (this.answers.length < 9) {
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 0, xaoTronDapAn: true });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  deleteAnswer(i) {
    this.answers.splice(i, 1);
    if (this.answers.length == 1) {
      this.isDelele = true;
    }
  }
  ChangeShuffler(i) {
    this.answers[i].xaoTronDapAn = !this.answers[i].xaoTronDapAn;
  }

  answers: MultiChoiceAnswer[] = [
    { id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 0, xaoTronDapAn: true },
    { id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 0, xaoTronDapAn: true },
    { id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 0, xaoTronDapAn: true },
    { id: generateRandomUniqueInteger(1000, 9999), NoiDung: '', dapAnDung: 0, xaoTronDapAn: true },
  ];
  configAnswer = {
    // startupFocus: true,
    height: 60,
    allowedContent: true,

    extraPlugins: ckConfig.extraPlugins,
    toolbarGroups: ckConfig.toolbarAnswer,
    removeButtons: ckConfig.removeButtonsAnswer,
  };
  @ViewChild("ck") ck: CKEditorComponent;
  @ViewChildren("cks") cks: QueryList<CKEditorComponent>;

  ngAfterViewInit(): void {
    this.cks.changes.subscribe((x) => {
      this.initAnswer(x.last);
    });

    this.cks.forEach((x) => {
      this.initAnswer(x);
    });
    //reload edit
    this.ck.ready.subscribe(() => {
      var head = this.ck.instance.document.getHead();
      var myscript = this.ck.instance.document.createElement('script', {
        attributes: {
          type: 'text/javascript',
          'src': 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-MML-AM_CHTML'
        }
      });
      head.append(myscript);
    });
  }


  private initAnswer(ck: CKEditorComponent) {
    ck.ready.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'none';
    });
    ck.focus.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'block';
    });
    ck.blur.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'none';
    });
  }
  SaveAndCreate() {
    window.location.reload();
    alert("Đã lưu")
  }
}

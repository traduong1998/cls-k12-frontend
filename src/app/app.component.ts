import { isPlatformBrowser } from '@angular/common';
import { Component, Inject, PLATFORM_ID } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NgxPermissionsService } from 'ngx-permissions';
import { AppConfig, UnitConfig } from './core/interfaces/app-config';
import { AppConfigService } from './core/services';
import { CLS } from 'cls-k12-sdk-js/src';
import { Title } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  config: AppConfig;
  title: string;
  unitConfig: UnitConfig;

  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    private configService: AppConfigService,
    translate: TranslateService,
    private permissionsService: NgxPermissionsService,
    private titleService: Title) {
    console.log(`AppComponent constructor`);
    if (environment.gaTrackingId) {
      // register google tag manager
      const gTagManagerScript = document.createElement('script');
      gTagManagerScript.async = true;
      gTagManagerScript.src = `https://www.googletagmanager.com/gtag/js?id=${environment.gaTrackingId}`;
      document.head.appendChild(gTagManagerScript);
  
      // register google analytics
      const gaScript = document.createElement('script');
      gaScript.innerHTML = `
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());
        gtag('config', '${environment.gaTrackingId}');
      `;
      document.head.appendChild(gaScript);
    }

    this.unitConfig = this.configService.getUnitConfig();

    if (isPlatformBrowser(this.platformId)) {
      //Ripple is an optional animation for the supported components such as buttons. It is disabled by default and needs to be enabled globally at your main component e.g. app.component.ts by injecting PrimeNGConfig.
      // this.primengConfig.ripple = true;
    }
    translate.setDefaultLang('vi');

    const perm = ["questionbank_view", "questionbank_create"];

    this.permissionsService.loadPermissions(perm);


    if (this.unitConfig) {

      this.titleService.setTitle(this.unitConfig.unitName);

      var favIcon: HTMLLinkElement = document.querySelector('#favIcon');
      if (favIcon) favIcon.href = this.unitConfig.favicon;
    }
  }
}

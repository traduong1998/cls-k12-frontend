import { AbstractControl, ValidatorFn } from "@angular/forms";


export function InvalidDateTimeValidator(dateTwo: Date): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        debugger;
        const forbidden = new Date(control.value).getTime() >= dateTwo.getTime();
        return forbidden ? { 'invalidDateFromTo': { value: control.value } } : null;
    }
}

export function InValidDateLearningPahtValidator(dateOne: Date, dateTwo: Date): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        let controlDate = control.value ? new Date(control.value) : null
        if (null) {
            return null;
        } else {
            const forbidden = (controlDate.getTime() < dateOne.getTime()) || (controlDate.getTime() > dateTwo.getTime());
            return forbidden ? { 'invalidDateLearningPath': { value: control.value } } : null;
        }

    }
}
import { TranslateModule } from '@ngx-translate/core';
import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import {
  AccordionAnchorDirective,
  AccordionLinkDirective,
  AccordionDirective
} from './accordion';
import { LoadingScreenComponent } from '../features/loading-screen/loading-screen.component';
import { SafeHtmlPipe } from './pipes/SafeHtmlPipe.pipe';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DemoMaterialModule } from '../demo-material-module';
import { MaterialModule } from './material/material.module';
import { SpinnerModule } from '../core/modules/spinner/spinner.module';
import { NgxPermissionsModule } from 'ngx-permissions';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgxMatDateFormats, NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule, NGX_MAT_DATE_FORMATS } from '@angular-material-components/datetime-picker';
import { NgxMatMomentModule } from '@angular-material-components/moment-adapter'
import { ReactiveFormsModule } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormsModule, NgControl } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { DeleteDiaLogComponent } from './dialog/delete-dia-log/delete-dia-log.component';
import { SafeUrlPipe } from 'src/app/shared/pipes/SafeUrlPipe.pipe';
import { ProgressDialogComponent } from './progress-dialog/progress-dialog.component';
import { StylePaginatorDirective } from './directives/style-paginator.directive';
import { SuccessSnackBarComponent } from './snack-bar/success-snack-bar/success-snack-bar.component';
import { ErrorSnackBarComponent } from './snack-bar/error-snack-bar/error-snack-bar.component';
import { WarningSnackBarComponent } from './snack-bar/warning-snack-bar/warning-snack-bar.component';
import { Spinner2Module } from './modules/spinner/spinner2.module';
import { ProgressPlayerComponent } from './progress-player/progress-player.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { YoutubePlayerComponent } from './youtube-player/youtube-player.component';
import { VideojsPlayerComponent } from './videojs-player/videojs-player.component';
import { ImageDialogComponent } from './dialog/image-dialog/image-dialog.component';
import { MathJaxDirective } from '../features/manage-question-bank/services/mathjax.directive';
import { RulesConfirmDialogComponent } from './dialog/rules-confirm-dialog/rules-confirm-dialog.component';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';

export const MOMENT_DATETIME_WITH_SECONDS_FORMAT = 'HH:mm:ss, DD/MM/yyyy ';

// If using Moment
const CUSTOM_MOMENT_FORMATS: NgxMatDateFormats = {
  parse: {
    dateInput: MOMENT_DATETIME_WITH_SECONDS_FORMAT,
  },
  display: {
    dateInput: MOMENT_DATETIME_WITH_SECONDS_FORMAT,
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MM YYYY',
  },
};

@NgModule({
  declarations: [
    LoadingScreenComponent,
    SafeHtmlPipe,
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    DeleteDiaLogComponent,
    SafeUrlPipe,
    ProgressDialogComponent,
    StylePaginatorDirective,
    SuccessSnackBarComponent,
    ErrorSnackBarComponent,
    WarningSnackBarComponent,
    ProgressPlayerComponent,
    YoutubePlayerComponent,
    VideojsPlayerComponent,
    ImageDialogComponent,
    MathJaxDirective,
    RulesConfirmDialogComponent
  ],
  imports: [
    //vendor
    CommonModule,
    RouterModule,
    TranslateModule,
    FlexLayoutModule,
    DemoMaterialModule,
    MaterialModule,
    SpinnerModule,
    MatSelectModule,
    MatFormFieldModule,
    NgxMatSelectSearchModule,
    NgxMatNativeDateModule,
    FormsModule,
    Spinner2Module,
    PdfViewerModule,
    PdfJsViewerModule
  ],
  exports: [
    //vendor
    CommonModule,
    RouterModule,
    TranslateModule,
    NgxPermissionsModule,
    FlexLayoutModule,
    DemoMaterialModule,
    MaterialModule,
    SpinnerModule,
    SafeHtmlPipe,
    LoadingScreenComponent,
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    NgxDropzoneModule,
    MatDatepickerModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatMomentModule,
    ReactiveFormsModule,
    FormsModule,
    SafeUrlPipe,
    StylePaginatorDirective,
    Spinner2Module,
    ProgressPlayerComponent,
    PdfViewerModule,
    YoutubePlayerComponent,
    VideojsPlayerComponent,
    MathJaxDirective,
    PdfJsViewerModule
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi-vn' },
    { provide: NGX_MAT_DATE_FORMATS, useValue: CUSTOM_MOMENT_FORMATS }
  ]
})

/**
 * Module dùng cho các feature module
 * Khai báo (components, directives, pipes,...) mà chúng ta muốn cho các module features;
 * Warning: Shared module will be imported by many lazy loaded features and because of that it should NEVER implement any services
 */
export class SharedModule { }

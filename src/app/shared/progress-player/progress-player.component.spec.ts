import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressPlayerComponent } from './progress-player.component';

describe('ProgressPlayerComponent', () => {
  let component: ProgressPlayerComponent;
  let fixture: ComponentFixture<ProgressPlayerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgressPlayerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ChangeDetectorRef, Component, Input, OnInit, Output, EventEmitter, OnDestroy, SimpleChanges } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';
import { takeUntil } from 'rxjs/operators';
import { ProgressPlayerService } from 'src/app/features/learner-lesson/Services/progress-player.service';
// import { BreakPoint } from './interface/break-point';
import { ImageDialogComponent } from '../dialog/image-dialog/image-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ReplaySubject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-progress-player',
  templateUrl: './progress-player.component.html',
  styleUrls: ['./progress-player.component.scss']
})
export class ProgressPlayerComponent implements OnInit, OnDestroy {
  progress = {
    start: null,
    value: null,
    end: null,
    min: 0,
    max: 100,
    hidebookmark: true,
    valueHoverSlider: null
  };

  //tag => audio or doc
  destroy: ReplaySubject<any> = new ReplaySubject<any>(1);
  @Input() tag;
  @Input() isHide;
  // Trạng thái complete nội dung thì chưa load content
  @Input() isCompleteContent = false;
  // Trạng thái đang chờ nội dung thì hiển thị banner
  @Input() isWaitingContent = false;
  // Ẩn các chức năng, chỉ có button qua bài và trở lại
  @Input() hideAction = false;
  // Ẩn và disabled các chức năng khi ở chế độ darkmode
  @Input() hideSetting = false;
  // Link audio
  @Input() linkAudio = '';
  // Link document
  @Input() linkDocument = '';
  // Id pure
  @Input() idPureContent = '';

  // Output
  // Lưu lại phần trăm bài học
  @Output() emitDuration = new EventEmitter();

  // Nếu tua video thì không lưu lại duration
  isTrackingAudio = false;

  // Nếu tua doc thì không lưu lại duration
  isTrackingDocument = false;

  // Nếu tua pure thì không lưu lại duration
  isTrackingPure = false;

  // hiển thị nút volume khi rê vào
  isShowVolume = false;
  //tag = 'doc';
  adjust = {
    speed: [
      { name: 'Chậm', ratio: 2, active: false },
      { name: 'Bình thường', ratio: 1, active: true },
      { name: 'Nhanh', ratio: 0.5, active: false },
    ],
    playback: [
      { ratio: 3, active: false },
      { ratio: 5, active: true },
      { ratio: 10, active: false }
    ]
  }
  currentTime: number;
  currentTimeDisplay: string = '0:00';
  //==> doc
  valueSlider = 0;
  sjValue;
  percent = 0;

  durationDoc = 0;

  progressValue = 0;

  isPlay = false;

  isMuted = false;

  volume = 100;

  isLightMode = false;

  isShowReplay = false;

  //=> media
  time: string;
  maxTime: number = 0;

  playbackRate: number = 5;


  isMute = false;
  // disable nút qua bài và trở lại
  disableNext = false;
  disableBack = false;
  sjDisableButtonNext;
  sjDisableButtonBack;
  objCloneDurationLocal = {
    duration: 0,
    percent: 0
  };

  objCloneDurationDocumentLocal = {
    duration: 0,
    percent: 0
  };
  idContentCurrent = 0;

  percentBeforeDoc = 0;
  percentBeforePure = 0;
  isClickForwardDoc = false;
  isClickForwardPure = false;
  durationSeek = {
    isSeek: false,
    time: null,
    timeSeekFirst: null
  };
  pageNow = 1;
  pageInput = 1;
  totalPage: number;

  pageControl = new FormControl('', [
    Validators.required,
    Validators.min(1),
  ]);

  choosePercentService;
  widthScreen = window.innerWidth;
  toogleMobile = window.innerWidth > 525 ? true : false;

  constructor(
    private _progressPlayerService: ProgressPlayerService,
    public router: Router,
    private cdr: ChangeDetectorRef,
    public dialog: MatDialog,
    private route: ActivatedRoute
  ) {
    this.idContentCurrent = this.route.snapshot.params.id;
    //disable back
    this._progressPlayerService.disableButtonBack$.subscribe((v) => {
      this.disableBack = v;
    })

    //disable next
    this._progressPlayerService.disableButtonNext$.subscribe((v) => {
      this.disableNext = v;
    })
    window.addEventListener('resize', (event) => this.handleResize(event));
  }

  ngOnInit(): void {
    if (localStorage.getItem('light_mode') === 'true') {
      this.isLightMode = true;
      setTimeout(() => { this._progressPlayerService.emitLightModeEvent(true) }, 0);
    }
    if (this.tag == 'media') {
      this.getPercentCompleteLocal();
      this.getTime();
      this.getCurrentTime();
    }
    if (this.tag == 'doc') {
      this.getPercent();
      this.getPercentCompleteLocalDocument();
    }
    if (this.tag == 'pure') {
      this.getPercent();
      this.getPercentCompleteLocalPure();
    }
    this.getActionPlay();

    this.isDisableProgressbar();
    this.getEventFullscreen();
    if (localStorage.getItem('light_mode') === 'true') {
      this.isLightMode = true;
      setTimeout(() => { this._progressPlayerService.emitLightModeEvent(true); }, 0);
    }

    // Lắng nghe khi ending audio;
    this._progressPlayerService.endingAudio$.subscribe(() => {
      this.isShowReplay = true;
      this.currentTime = this.maxTime;
      this.onSetLocalDuration(true);
      if (!this.isTrackingAudio) {
        setTimeout(() => { this._progressPlayerService.percentCompleteSound.next(this.linkAudio); }, 0);
      }
    });
    // this._progressPlayerService.returnDurationVideo$.pipe(takeUntil(this.destroy)).subscribe(() => this.currentTime = 0);

    // Lắng nghe khi thay đổi link audio;
    this._progressPlayerService.changeLinkAudio$.pipe(takeUntil(this.destroy)).subscribe(res => {
      if (res === 'audio') {
        this.onSetLocalDuration();
      }
    });

    // Lắng nghe khi thay đổi link document;
    this._progressPlayerService.changeLinkDocument$.pipe(takeUntil(this.destroy)).subscribe(res => {

      if (res.type === 'doc') {
        this.onSetLocalDurationDocument(res.page);
      }
    });

    // Lắng nghe khi thay đổi link pure;
    this._progressPlayerService.changeLinkPure$.pipe(takeUntil(this.destroy)).subscribe(res => {

      if (res === 'pure') {
        this.onSetLocalDurationPure();
      }
    });
  }

  getPercentCompleteLocal(): void {
    if (localStorage.getItem('duration_audio')) {
      const value = JSON.parse(localStorage.getItem('duration_audio'));
      const filterArr = value.find(item => item.id === this.linkAudio);
      if (filterArr) {
        this.currentTime = filterArr.duration;
        this.currentTimeDisplay = this.formatTime(filterArr.duration);
        this.emitDuration.emit(filterArr);
        this._progressPlayerService.emitCurrentTime.next(this.currentTime);
        if (this.linkAudio) {
          this._progressPlayerService.percentCompleteSound.next(this.linkAudio);
        }
      }
    }
  }

  getPercentCompleteLocalDocument(): void {

    if (localStorage.getItem('duration_document')) {
      const value = JSON.parse(localStorage.getItem('duration_document'));
      const filterArr = value.find(item => item.id === this.linkDocument);
      if (filterArr) {

        this.percent = filterArr.duration;
        this.percentBeforeDoc = filterArr.percent;
        this.isTrackingDocument = filterArr.isTracking;
        this.isClickForwardDoc = filterArr.isClickForward;
        // if (this.percent <= filterArr.percent) {
        //   this.isTrackingDocument = false;
        //   // filterArr.percent_main = filterArr.percent_temp;
        // }
        this.percentDisplay = Math.round(+this.percent);
        // this._progressPlayerService.emitPercentClickEvent(this.percent);
        this._progressPlayerService.emitPercent.next({ percent: this.percent, page: this.pageNow, totalPage: this.totalPage });
        this.emitDuration.emit(filterArr);
        if (this.linkDocument) {
          this._progressPlayerService.linkDocumentLocalStorage.next(this.linkDocument);
        }
      }
    }
  }

  getPercentCompleteLocalPure(): void {
    if (localStorage.getItem('duration_pure')) {
      const value = JSON.parse(localStorage.getItem('duration_pure'));
      const filterArr = value.find(item => item.id === this.idPureContent);
      if (filterArr) {
        this.percent = filterArr.duration;
        this.isTrackingPure = filterArr.isTracking;
        this.isClickForwardPure = filterArr.isClickForward;
        // if (this.percent <= filterArr.percent) {
        //   this.isTrackingDocument = false;
        //   // filterArr.percent_main = filterArr.percent_temp;
        // }
        this.percentDisplay = Math.round(+this.percent);
        // this._progressPlayerService.emitPercentClickEvent(this.percent);
        this._progressPlayerService.emitPercent.next({ percent: this.percent, page: this.pageNow, totalPage: this.totalPage });
        this.emitDuration.emit(filterArr);
        if (this.idPureContent) {
          this._progressPlayerService.idPureLocalStorage.next(this.idPureContent);
        }
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {

    this.isShowReplay = false;
    if (changes?.linkAudio?.currentValue) {
      // this.isTrackingAudio = false;
      this.getPercentCompleteLocal();
      this.idContentCurrent = this.route.snapshot.params.id;
      // Nếu số giây hiện tại mà nhỏ hơn số giây tổng thì ẩn nút replay;
      if (this.progress.value < this.progress.max) {
        this.isShowReplay = false;
      }
      this.durationSeek.isSeek = false;
      // Khi thay đổi link thì dừng video
      this._progressPlayerService.emitPlayEvent.next(false);
      this._progressPlayerService.percentCompleteSound.next(changes?.linkAudio?.currentValue);
      // this.setDurationChange();
    }
    else if (changes?.linkDocument && changes?.linkDocument?.currentValue) {
      if (this.choosePercentService) {
        this.choosePercentService.unsubscribe();
      }
      this.pageNow = 1;
      this.pageInput = 1;
      this.percent = 0;
      this.isTrackingDocument = false;
      this.isClickForwardDoc = false;
      this.getPercentCompleteLocalDocument();
      this.idContentCurrent = this.route.snapshot.params.id;
      // Khi thay đổi link thì dừng phat
      this._progressPlayerService.emitPlayEvent.next(false);
      this._progressPlayerService.linkDocumentLocalStorage.next(changes?.linkDocument?.currentValue);
      // this.setDurationChangeDocument();

      // if (localStorage.getItem('duration_document')) {
      //   const value = JSON.parse(localStorage.getItem('duration_document'));
      //   const filterArr = value.find(item => item.id === this.linkDocument);
      //   if (filterArr) {
      //     this.percent = filterArr.percent_temp;
      //     this.percentDisplay = Math.round(+this.percent);
      //     this._progressPlayerService.emitPercentClickEvent(this.percent);
      //     this._progressPlayerService.emitPercentEvent(this.percent);
      //     this.emitDuration.emit(filterArr);
      //     if (this.linkDocument) {
      //       this._progressPlayerService.linkDocumentLocalStorage.next(this.linkDocument);
      //     }
      //   }
      // }
    }
    else if (changes?.idPureContent && changes?.idPureContent?.currentValue) {
      this.isTrackingPure = false;
      this.isClickForwardPure = false;
      this.getPercentCompleteLocalPure();
      this.idContentCurrent = this.route.snapshot.params.id;
      // Khi thay đổi link thì dừng phat
      this._progressPlayerService.emitPlayEvent.next(false);
      this._progressPlayerService.idPureLocalStorage.next(changes?.linkDocument?.currentValue);
      // this._progressPlayerService.emitPercent.next(changes?.idPureContent?.currentValue);
      // this.setDurationChangeDocument();
    }
  }

  // setDurationChangeDocument() {
  //   if (localStorage.getItem('duration_document')) {

  //     const value = JSON.parse(localStorage.getItem('duration_document'));
  //     const filterArr = value.find(item => item.id === this.linkDocument);
  //     if (filterArr) {
  //       //filterArr.duration = this.currentTime;
  //       // filterArr.percent = this.percent;
  //       filterArr.dateTimeUpdate = + new Date();
  //     }
  //     console.log("setDurationChangeDocument", filterArr);
  //   }
  // }

  //hover
  valueHover;
  mouseover(e) {
    if (this.calcSliderPos(e).toFixed(2) != 'NaN') {
      this.valueHover = Math.round(parseInt(this.calcSliderPos(e).toFixed(2)));
    }
    this.formatLabel(this.valueHover)
  }

  calcSliderPos(e) {
    return (e.offsetX / e.target.clientWidth) * this.progress.max;
  }

  formatLabel(value) {
    return value;
  }
  ///wheel
  // mousewheel(e) {
  //   if (!this.isDisable) {
  //     if (e.deltaY < 0 && this.percent < 100) {
  //       this.percent += 1;
  //     } else {
  //       if (this.percent > 0) {
  //         this.percent -= 1;
  //       }
  //     }
  //     this._progressPlayerService.emitPercentClickEvent(this.percent);
  //   }
  // }

  onChangePage(pageChange: number) {
    pageChange = +pageChange;
    console.log("onChangePage", pageChange);
    this.isTrackingDocument = true;

    if (!this.isDisable) {
      this.getPercentService.unsubscribe();
      if (!this.isClickForwardDoc) {
        this.percentBeforeDoc = this.percent;
        this.isClickForwardDoc = true;
      }

      if (this.tag == 'doc') {
        if (pageChange <= 1) {
          this.isTrackingDocument = false;
        }
        pageChange = pageChange <= 0 || pageChange == null ? 1 : pageChange;
        pageChange = pageChange > this.totalPage ? this.totalPage : pageChange;
        this.pageNow = pageChange;
        this.pageInput = this.pageNow;
        console.log(" this.pageNow", this.pageNow);
        this.percent = this.pageNow / this.totalPage * 100;
      }
      else {
        this.percent = pageChange;
      }
      setTimeout(() => {
        this.choosePercentService = this._progressPlayerService.emitPercentClickEvent(pageChange);

      }, 0);
      this.percentDisplay = Math.round(this.percent);
      this.getPercent();

      if (localStorage.getItem('duration_document')) {
        const value = JSON.parse(localStorage.getItem('duration_document'));
        const filterArr = value.find(item => item.id === this.linkDocument);

        if (filterArr) {
          // this.percent = filterArr.percent_temp;
          // this.isTrackingDocument = filterArr.isTracking;
          if (this.percent <= filterArr.percent) {
            this.isTrackingDocument = false;
            this.isClickForwardDoc = false;
            // filterArr.percent_main = filterArr.percent_temp;
          }
        }
      }
    }
    console.log("onChangePage", this.isTrackingDocument);

  }
  onSlide(event: MatSliderChange) {
    if (this.tag == 'doc') {
      this.isTrackingDocument = true;
    }
    else if (this.tag == 'pure') {
      this.isTrackingPure = true;
    }

    if (!this.isDisable) {
      this.getPercentService.unsubscribe();
      if (this.tag == 'doc') {
        if (!this.isClickForwardDoc) {
          this.percentBeforeDoc = this.percent;
          this.isClickForwardDoc = true;
        }
      }
      else if (this.tag == 'pure') {
        if (!this.isClickForwardPure) {
          this.percentBeforePure = this.percent;
          this.isClickForwardPure = true;
        }
      }


      if (this.tag == 'doc') {
        if (event.value < 1) {
          this.isTrackingDocument = false;
        }
        event.value = event.value == 0 ? 1 : event.value;
        this.pageNow = event.value;
        this.pageInput = this.pageNow;
        this.percent = this.pageNow / this.totalPage * 100;
        console.log(`onslide`, this.percent)
      }
      else if (this.tag == 'pure') {
        this.percent = event.value;
      }
      setTimeout(() => {
        this.choosePercentService = this._progressPlayerService.emitPercentClickEvent(event.value);

      }, 0);
      this.percentDisplay = Math.round(this.percent);
      this.getPercent();

      if (localStorage.getItem('duration_document')) {
        const value = JSON.parse(localStorage.getItem('duration_document'));
        const filterArr = value.find(item => item.id === this.linkDocument);

        if (filterArr) {
          // this.percent = filterArr.percent_temp;
          // this.isTrackingDocument = filterArr.isTracking;
          if (this.percent <= filterArr.percent) {
            this.isTrackingDocument = false;
            this.isClickForwardDoc = false;
            // filterArr.percent_main = filterArr.percent_temp;
          }
        }
      }
      if (localStorage.getItem('duration_pure')) {
        const value = JSON.parse(localStorage.getItem('duration_pure'));
        const filterArr = value.find(item => item.id === this.idPureContent);

        if (filterArr) {
          // this.percent = filterArr.percent_temp;
          // this.isTrackingDocument = filterArr.isTracking;
          if (this.percent <= filterArr.percent) {
            this.isTrackingPure = false;
            this.isClickForwardPure = false;
            // filterArr.percent_main = filterArr.percent_temp;
          }
        }
      }
    }
  }

  percentDisplay = 0;
  getPercentService;
  getPercent() {
    this.getPercentService = this._progressPlayerService.percent$.pipe(takeUntil(this.destroy)).subscribe(
      (p) => {
        console.log(`per-progress`, p);
        debugger
        this.percent = this.percent > +p.percent ? this.percent : +p.percent;
        this.durationDoc=p.percent;
        this.pageNow = p.page;
        this.pageInput = p.page;
        this.totalPage = p.totalPage;
        this.percentDisplay = Math.round(this.percent);
        if (this.tag == 'doc') {
          this.progress.max = this.totalPage;
        }
        if (this.percentDisplay >= 100) {
          this.isPlay = false;
          this.percentDisplay = 100;
          if (this.tag == 'pure') {
            this.percent = 100;
            this.isShowReplay = true;
          }
          this._progressPlayerService.emitPlayEvent.next(this.isPlay);

          //doc complete
          if (this.linkDocument != "") {
            this.onSetLocalDurationDocument(this.pageNow);
            if (!this.isTrackingDocument) {
              setTimeout(() => { this._progressPlayerService.linkDocumentLocalStorage.next(this.linkDocument); }, 0);
            }
          }


          //pure complete
          if (this.idPureContent != "") {
            this.onSetLocalDurationPure();
            if (!this.isTrackingPure) {
              setTimeout(() => { this._progressPlayerService.idPureLocalStorage.next(this.idPureContent); }, 0);
            }
          }
        }
        // else if (this.percentDisplay > 100) {
        //   this.percentDisplay = 100;
        // }
        else {
          this.isShowReplay = false;
        }
      }
    );
    this.cdr.detectChanges();
  }

  play() {
    this.isPlay = !this.isPlay;
    this._progressPlayerService.emitPlayEvent.next(this.isPlay);
  }

  getActionPlay() {
    this._progressPlayerService.emitPlayEvent.pipe(takeUntil(this.destroy)).subscribe((v) => {
      this.isPlay = v;
    })
  }

  onClickLightMode() {
    this.isLightMode = !this.isLightMode;
    localStorage.setItem('light_mode', `${this.isLightMode}`);
    this._progressPlayerService.emitLightModeEvent(this.isLightMode);
  }
  isDisable;
  isDisableProgressbar() {
    this._progressPlayerService.isDisable$.pipe(takeUntil(this.destroy)).subscribe((v) => {
      this.isDisable = v;
    })
  }

  next() {
    // Nếu tua tới [s] thì set để xử lý lưu local %
    console.log('nextProgress')

    this.isTrackingAudio = true;
    // this.isTrackingDocument = true;
    this.isTrackingPure = true;
    if (this.percent < 100) {

      if (!this.isClickForwardDoc) {
        // this.percentBeforeDoc = this.percent;
        // this.isClickForward = true;
      }
      if (!this.isClickForwardPure) {
        this.percentBeforePure = this.percent;
        this.isClickForwardPure = true;
      }

      if (localStorage.getItem('duration_document')) {
        const value = JSON.parse(localStorage.getItem('duration_document'));
        const filterArr = value.find(item => item.id === this.linkDocument);
        if (!this.isClickForwardDoc) {
          // this.percentBeforeDoc = this.percent;
          // this.isClickForward = true;
        }
        if (filterArr) {
          // this.percent = filterArr.percent_temp;
          // this.isTrackingDocument = filterArr.isTracking;
          if ((this.percent + this.playbackRate) <= filterArr.percent) {
            this.isTrackingDocument = false;
            this.isClickForwardDoc = false;

            // filterArr.percent_main = filterArr.percent_temp;
          }
        }
      }
      else if (localStorage.getItem('duration_pure')) {
        const value = JSON.parse(localStorage.getItem('duration_pure'));
        const filterArr = value.find(item => item.id === this.idPureContent);
        if (!this.isClickForwardPure) {
          this.percentBeforePure = this.percent;
          this.isClickForwardPure = true;
        }
        if (filterArr) {
          // this.percent = filterArr.percent_temp;
          // this.isTrackingDocument = filterArr.isTracking;
          if ((this.percent + this.playbackRate) <= filterArr.percent) {
            this.isTrackingPure = false;
            this.isClickForwardPure = false;
            // filterArr.percent_main = filterArr.percent_temp;
          }
        }
      }
      setTimeout(() => {
        this._progressPlayerService.emitNextEvent(this.playbackRate);
      }, 333);
    }
    if (this.tag === 'media') {
      const time = { value: this.currentTime };
      this.onCheckSeekVideo(time, true);
    }
  }

  previous() {
    // Nếu lùi [s] thì set để xử lý lưu local %
    this.isTrackingAudio = true;
    // this.isTrackingDocument = true;
    this.isTrackingPure = true;
    setTimeout(() => {
      this._progressPlayerService.emitPreviousEvent(this.playbackRate);
    }, 333);
    if (localStorage.getItem('duration_document')) {
      const value = JSON.parse(localStorage.getItem('duration_document'));
      const filterArr = value.find(item => item.id === this.linkDocument);

      if (filterArr) {
        // this.percent = filterArr.percent_temp;
        // this.isTrackingDocument = filterArr.isTracking;
        if (this.percent <= filterArr.percent) {
          this.isTrackingDocument = false;
          this.isClickForwardDoc = false;
          // filterArr.percent_main = filterArr.percent_temp;
        }
      }
    }
    if (localStorage.getItem('duration_pure')) {
      const value = JSON.parse(localStorage.getItem('duration_pure'));
      const filterArr = value.find(item => item.id === this.idPureContent);

      if (filterArr) {
        // this.percent = filterArr.percent_temp;
        // this.isTrackingDocument = filterArr.isTracking;
        if (this.percent <= filterArr.percent) {
          this.isTrackingPure = false;
          this.isClickForwardPure = false;
          // filterArr.percent_main = filterArr.percent_temp;
        }
      }
    }

    if (this.tag === 'media') {
      const time = { value: this.currentTime };
      this.onCheckSeekVideo(time, false);
    }
  }

  onCheckSeekVideo(event, check?: boolean): void {
    if (check) {
      if (this.durationSeek.isSeek === false) {
        localStorage.setItem('fake_duration_audio', this.durationSeek.timeSeekFirst);
      }
      this.durationSeek.isSeek = true;
    }
    if (localStorage.getItem('duration_audio')) {
      const value = JSON.parse(localStorage.getItem('duration_audio'));
      const filterArr = value.find(item => item.id === this.linkAudio);
      if (filterArr) {
        if (event.value <= filterArr.fake) {
          filterArr.isSeekTo = false;
        } else {
          filterArr.isSeekTo = true;
        }
      }
      localStorage.setItem('duration_audio', JSON.stringify(value));
    }
  }



  onSlideVolume(event: MatSliderChange) {
    this.volume = event.value;
    if (this.volume == 0) {
      this.isMuted = true;
    }
    else {
      this.isMuted = false;
    }
    this._progressPlayerService.emitVolumeEvent(event.value);
  }

  muted() {
    this.isMuted = !this.isMuted;
    this._progressPlayerService.emitMutedEvent(this.isMuted);
  }

  //media
  getTime() {
    this._progressPlayerService.time$.pipe(takeUntil(this.destroy)).subscribe((t) => {
      this.time = this.formatTime(t);
      this.maxTime = t;
      this.progress.max = t;
    })
  }

  onSlideMedia(event: MatSliderChange): void {
    this.isTrackingAudio = true;
    // cập nhật current time hiện tại
    this.currentTime = event.value;
    this.currentTimeDisplay = this.formatTime(event.value);
    // bắn mới cái currentime qua chỗ
    this._progressPlayerService.emitCurrentTimeSendingEvent(event.value);
    this._progressPlayerService.emitPlayEvent.next(false);
    this.onCheckSeekVideo(event, true);
  }

  onChangeSlideMedia(event) {
    if (event.value) {
      this._progressPlayerService.stopAndPlay.next(true);
      this.isPlay = false;
      this._progressPlayerService.emitPlayEvent.next(true);
    }
  }

  sub;
  getCurrentTime() {
    this.sub = this._progressPlayerService.currentTime$.pipe(takeUntil(this.destroy)).subscribe((currentTime) => {
      this.durationSeek.timeSeekFirst = currentTime;
      this.currentTime = Math.round(currentTime);
      this.currentTimeDisplay = this.formatTime(Math.round(currentTime));
      if (this.maxTime == 0) {
        this.isShowReplay = false;
      }
      else {
        if (this.currentTime === Math.round(this.maxTime)) {
          this.isShowReplay = true;
        } else {
          this.isShowReplay = false;
        }
      }

    })
    this.cdr.detectChanges();
  }
  formatTime(seconds) {
    const h = Math.floor(seconds / 3600)
    const m = Math.floor((seconds % 3600) / 60)
    const s = Math.round(seconds % 60)
    return [h, m > 9 ? m : h ? '0' + m : m || '0', s > 9 ? s : '0' + s]
      .filter(a => a)
      .join(':')
  }

  //tăng giảm text
  onClickZoomIn() {
    this._progressPlayerService.emitZoomTextEvent(true);
  }

  onClickZoomOut() {
    this._progressPlayerService.emitZoomTextEvent(false);
  }

  //qua bài
  onClickBackContent() {
    this._progressPlayerService.emitBackContentEvent(true);
  }

  onClickNextContent() {
    this._progressPlayerService.emitNextContentEvent(true);
  }

  ngAfterViewInit(): void {
    if (!this.isCompleteContent) {
      setTimeout(() => {
        let oldPlay: boolean;
        const elementId = document.getElementById('seekslider');
        if (elementId) {
          elementId.addEventListener('mousemove', (e) => {
            let value = + this.calcSliderPos(e).toFixed(2);
            // console.log(`valueHover`, value)
            let element = document.getElementById('hover_span');
            element.style.display = 'block';
            let slider = document.getElementById('seekslider');
            // Vị trí hiển thị trên trình phát
            element.style.left = `${(Math.round(value) / this.progress.max) * 100}%`;
            // Giá trị
            if (this.tag == 'media') {
              this.progress.valueHoverSlider = this.formatTime(value);
            }
            if (this.tag == 'doc') {
              this.progress.valueHoverSlider = Math.round(value);
            }
            if (this.tag == 'pure') {
              this.progress.valueHoverSlider = Math.round(value);
            }

            slider.classList.add('hideChild')
          });

          elementId.addEventListener('mouseleave', () => {
            document.getElementById('hover_span').style.display = 'none';
          });

          elementId.addEventListener('mouseenter', () => {
            if (this.isPlay == true) {
              oldPlay = true;
            }
            // else {
            //   oldPlay = false;
            // }
          });

          if (this.tag === 'doc') {
            var isHolding = false;
            elementId.addEventListener('mousedown', () => {
              if (this.isPlay == true) {
                isHolding = true;
                this._progressPlayerService.emitPlayEvent.next(false);
                // oldPlay = true;
              }
            });
          }

          if (this.tag === 'pure') {
            var isHolding = false;
            elementId.addEventListener('mousedown', () => {
              if (this.isPlay == true) {
                isHolding = true;
                this._progressPlayerService.emitPlayEvent.next(false);
                // oldPlay = true;
              }
            });
          }
        }
        if (this.tag == 'doc') {
          document.addEventListener('mouseup', () => {
            if (oldPlay == true && this.isPlay == false && isHolding) {
              this._progressPlayerService.emitPlayEvent.next(true);
              isHolding = false;
            }
          });

          document.getElementById('forward-previous').addEventListener('mouseenter', () => {
            if (this.isPlay == true) {
              oldPlay = true;
            }
            // else {
            //   oldPlay = false;
            // }
          });

          var isHolding = false;
          document.getElementById('forward-previous').addEventListener('mousedown', () => {
            if (this.isPlay == true) {
              isHolding = true;
              this._progressPlayerService.emitPlayEvent.next(false);
              // oldPlay = true;
            }
          });
          document.getElementById('forward-previous').addEventListener('mouseup', () => {
            if (oldPlay == true && this.isPlay == false && isHolding) {
              this._progressPlayerService.emitPlayEvent.next(true);
              isHolding = false;
            }
          });

          document.getElementById('forward-next').addEventListener('mouseenter', () => {
            if (this.isPlay == true) {
              oldPlay = true;
            }
            // else {
            //   oldPlay = false;
            // }
          });

          var isHolding = false;
          document.getElementById('forward-next').addEventListener('mousedown', () => {
            if (this.isPlay == true) {
              isHolding = true;
              this._progressPlayerService.emitPlayEvent.next(false);
              // oldPlay = true;
            }
          });
          document.getElementById('forward-next').addEventListener('mouseup', () => {
            if (oldPlay == true && this.isPlay == false && isHolding) {
              this._progressPlayerService.emitPlayEvent.next(true);
              isHolding = false;
            }
          });
          this.cdr.detectChanges();
        }

        if (this.tag == 'pure') {
          document.addEventListener('mouseup', () => {
            if (oldPlay == true && this.isPlay == false && isHolding) {
              this._progressPlayerService.emitPlayEvent.next(true);
              isHolding = false;
            }
          });

          document.getElementById('forward-previous').addEventListener('mouseenter', () => {
            if (this.isPlay == true) {
              oldPlay = true;
            }
            // else {
            //   oldPlay = false;
            // }
          });

          var isHolding = false;
          document.getElementById('forward-previous').addEventListener('mousedown', () => {
            if (this.isPlay == true) {
              isHolding = true;
              this._progressPlayerService.emitPlayEvent.next(false);
              // oldPlay = true;
            }
          });
          document.getElementById('forward-previous').addEventListener('mouseup', () => {
            if (oldPlay == true && this.isPlay == false && isHolding) {
              this._progressPlayerService.emitPlayEvent.next(true);
              isHolding = false;
            }
          });

          document.getElementById('forward-next').addEventListener('mouseenter', () => {
            if (this.isPlay == true) {
              oldPlay = true;
            }
            // else {
            //   oldPlay = false;
            // }
          });

          var isHolding = false;
          document.getElementById('forward-next').addEventListener('mousedown', () => {
            if (this.isPlay == true) {
              isHolding = true;
              this._progressPlayerService.emitPlayEvent.next(false);
              // oldPlay = true;
            }
          });
          document.getElementById('forward-next').addEventListener('mouseup', () => {
            if (oldPlay == true && this.isPlay == false && isHolding) {
              this._progressPlayerService.emitPlayEvent.next(true);
              isHolding = false;
            }
          });
          this.cdr.detectChanges();
        }
      }, 0);
    }
  }


  // Xem ở chế độ toàn màn hình
  isFull = false;
  onFullscreen() {
    this.isFull = !this.isFull;
    this._progressPlayerService.emitFullScreenEvent(this.isFull);
  }

  getEventFullscreen(): void {
    this._progressPlayerService.fullScreen$.pipe(takeUntil(this.destroy)).subscribe((value) => {
      this.isFull = value;
    });
  }

  onChooseBackground(): void {
    this.dialog.open(ImageDialogComponent, {
      width: '550px',
      height: 'auto',
      data: {}
    });
  }

  // Tốc độ phát
  onSpeed(ratio: number, speed): void {
    if (ratio) {
      this.adjust.speed.forEach(value => value.active = false);
      speed.active = true;
      this._progressPlayerService.emitSpeedDocEvent(ratio);
      // this.player.setPlaybackRate(ratio);
    }
  }

  // Tốc độ tua
  onPlaybackRate(ratio: number, rate): void {
    // this.timeNextAndPrevious = ratio;
    this.playbackRate = ratio;
    this.adjust.playback.forEach(value => value.active = false);
    rate.active = true;
  }

  // replay
  replayVideo(): void {
    this._progressPlayerService.emitPercentClickEvent(0);
    this._progressPlayerService.emitPlayEvent.next(false);
    this._progressPlayerService.replaySound.next(true);
    this.isShowReplay = false;
  }

  ngOnDestroy(): void {
    this.destroy.next(null);
    if (this.choosePercentService) {
      this.choosePercentService.unsubscribe();
    }
    if (this.tag === 'media') {
      this.onSetLocalDuration();
    }
    else if (this.tag === 'doc' && this.percent) {
      this.onSetLocalDurationDocument(this.pageNow);
    }
    else if (this.tag === 'pure' && this.percent) {
      this.onSetLocalDurationPure();
    }
    localStorage.removeItem('fake_duration_audio');
  }

  checkDurationBefore(filter, clone): number {
    if (!filter.isSeekTo) {
      return filter.duration;
    } else {
      return clone.duration;
    }
  }

  // Tính toán percent để lưu
  checkDurationPercent(clone, filter): number {
    if (this.handleTrackingPercentComplete(clone, filter) > 100) {
      return 100;
    } else {
      return this.handleTrackingPercentComplete(clone, filter);
    }
  }

  // Tính toán percent để lưu
  checkDurationPercentNew(durationLocal): number {
    if (this.isTrackingAudio) {
      if (durationLocal) {
        return (+durationLocal / this.maxTime) * 100;
      } else {
        return 0;
      }
    } else {
      return (this.currentTime / this.maxTime) * 100;
    }
  }

  // Lưu local % tiến trình hoàn thành bài giảng
  onSetLocalDuration(check?: boolean): void {
    if (this.currentTime) {
      if (localStorage.getItem('duration_audio')) {
        const value = JSON.parse(localStorage.getItem('duration_audio'));
        const filterArr = value.find(item => item.id === this.linkAudio);
        const durationLocal = localStorage.getItem('fake_duration_audio');
        if (filterArr) {
          const cloneFilter = JSON.parse(JSON.stringify(filterArr));
          filterArr.duration = + this.currentTime;
          // Nếu có tua và % hiện tại < % đã lưu local ở trước thì lấy ở local;
          filterArr.isSeekTo = (cloneFilter.isSeekTo !== undefined || cloneFilter.isSeekTo !== null)
            ? cloneFilter.isSeekTo : this.isTrackingAudio;
          filterArr.percent = this.handleTrackingPercentComplete(cloneFilter, filterArr) > 100 ? 100 :
            this.handleTrackingPercentComplete(cloneFilter, filterArr);
          filterArr.fake = cloneFilter.isSeekTo ? filterArr.fake : filterArr.duration;
          this.checkCompleteContent(filterArr, check);
          this.savePercentLocal(filterArr.id, filterArr.percent, filterArr.contentID);
        } else {
          value.push({
            id: this.linkAudio,
            duration: this.currentTime,
            isSeekTo: this.isTrackingAudio,
            percent: this.checkDurationPercentNew(durationLocal),
            contentID: + this.idContentCurrent,
            fake: + durationLocal,
          });
          this.checkCompleteContent(value[value.length - 1], check);
          this.savePercentLocal(value[value.length - 1].id, value[value.length - 1].percent, value[value.length - 1].contentID);
        }
        localStorage.setItem('duration_audio', JSON.stringify(value));
      } else {
        const durationLocal = localStorage.getItem('fake_duration_audio');
        const objVideo = [
          {
            id: this.linkAudio,
            duration: this.currentTime,
            isSeekTo: this.isTrackingAudio,
            percent: this.checkDurationPercentNew(durationLocal),
            contentID: + this.idContentCurrent,
            fake: + durationLocal,
          }
        ];
        this.checkCompleteContent(objVideo[0], check);
        this.savePercentLocal(objVideo[0].id, objVideo[0].percent, objVideo[0].contentID);
        localStorage.setItem('duration_audio', JSON.stringify(objVideo));
      }
    }
  }

  // Nếu thỏa mãn điều kiện thì lưu thành 100%
  checkCompleteContent(obj, check: boolean): void {
    if (obj.percent && (obj.duration === obj.fake || obj.fake === 0) && !obj.isSeekTo && check) {
      obj.percent = 100;
    }
  }

  savePercentLocal(id: string, percent: number, idContent: number): void {
    this._progressPlayerService.percentCompleteVideoOut.next(
      { id, type: 'audio', percent, contentId: idContent }
    );
  }

  handleTrackingPercentComplete(obj, root): number {
    if (obj.isSeekTo) {
      // Nếu tua thì lấy ở lần trước;
      // Có 2 trường hợp là tua lớn hơn và tua nhỏ hơn
      return obj.percent;
    } else {
      const current = (this.currentTime / this.maxTime) * 100;
      // root.durationBefore = root.duration;
      if (current <= obj.percent) {
        return obj.percent;
      } else {
        return current;
      }
    }
  }

  checkPercent(cloneFilter: any): number {
    debugger
    if (cloneFilter.percent >= 100) {
      cloneFilter.percent = 100;
    }
    if (this.isTrackingDocument == true) {
      if (cloneFilter.percent > this.percentBeforeDoc) {
        return cloneFilter.percent;
      }
      else {
        return this.percentBeforeDoc;
      }
    }
    else {
      if (this.percent > cloneFilter.percent) {
        return this.percent;
      }
      else {
        return cloneFilter.percent;
      }
    }
  }
  onSetLocalDurationDocument(page?: number): void {


    if (this.percent >= 0) {
      if (this.percent >= 100) {
        this.percent = 100;
      }
      if (this.percentBeforeDoc >= 100) {
        this.percentBeforeDoc = 100;
      }
      if (localStorage.getItem('duration_document')) {
        const value = JSON.parse(localStorage.getItem('duration_document'));
        const filterArr = value.find(item => item.id === this.linkDocument);
        if (filterArr) {
          //filterArr.duration = this.currentTime;
          // Nếu có tua và % hiện tại < % đã lưu local ở trước thì lấy ở local;
          const cloneFilter = JSON.parse(JSON.stringify(filterArr));

          filterArr.duration = this.durationDoc;
          // filterArr.duration = 0;


          // filterArr.percent = this.isTrackingDocument ? (cloneFilter.percent > this.percentBefore ? cloneFilter.percent : this.percentBefore) : ((this.percent > cloneFilter.percent ? this.percent : cloneFilter.percent) > this.percentBefore ? (this.percent > cloneFilter.percent ? this.percent : cloneFilter.percent) : this.percentBefore);

          filterArr.percent = this.checkPercent(cloneFilter);
          // filterArr.percent = 0;

          filterArr.page = Math.round((this.checkPercent(cloneFilter)) / 100 * this.totalPage);
          // filterArr.page = 1;

          filterArr.totalPage = this.totalPage;
          filterArr.dateTimeUpdate = + new Date();
          // if (!this.isTrackingDocument) {
          //   filterArr.percent_main = this.percent;
          // }
          // filterArr.percent_temp = this.percent;
          filterArr.dateTimeUpdate = + new Date();
          filterArr.type = 'doc';
          filterArr.isTracking = this.isTrackingDocument;
          filterArr.isClickForward = this.isClickForwardDoc;
          this.savePercentLocal(filterArr.id, filterArr.percent, filterArr.contentID);

        } else {
          value.push({
            id: this.linkDocument,


            duration: this.durationDoc,
            // duration: 0,


            percent: this.isTrackingDocument ? (this.percentBeforeDoc > 0 ? this.percentBeforeDoc : 0) : this.percent,
            // percent: 0,

            page: Math.round((this.isTrackingDocument ? (this.percentBeforeDoc > 0 ? this.percentBeforeDoc : 0) : this.percent) / 100 * this.totalPage),

            // page: 1,

            totalPage: this.totalPage,
            dateTimeUpdate: + new Date(),
            type: 'doc',
            isTracking: this.isTrackingDocument,
            isClickForward: this.isClickForwardDoc,
            contentID: this.idContentCurrent
          });
          this.savePercentLocal(value[value.length - 1].id, value[value.length - 1].percent, value[value.length - 1].contentID);
        }
        localStorage.setItem('duration_document', JSON.stringify(value));
      } else {
        const objDocument = [
          {
            id: this.linkDocument,


            duration: this.durationDoc,
            // duration: 0,


            percent: this.isTrackingDocument ? (this.percentBeforeDoc > 0 ? this.percentBeforeDoc : 0) : this.percent,
            // percent: 0,

            page: Math.round((this.isTrackingDocument ? (this.percentBeforeDoc > 0 ? this.percentBeforeDoc : 0) : this.percent) / 100 * this.totalPage),
            // page: 1,

            totalPage: this.totalPage,
            dateTimeUpdate: + new Date(),
            type: 'doc',
            isTracking: this.isTrackingDocument,
            isClickForward: this.isClickForwardDoc,
            contentID: this.idContentCurrent
          }
        ];
        this.savePercentLocal(objDocument[0].id, objDocument[0].percent, objDocument[0].contentID);
        localStorage.setItem('duration_document', JSON.stringify(objDocument));
      }
    }
  }


  onSetLocalDurationPure(): void {
    if (this.percent >= 0) {
      if (localStorage.getItem('duration_pure')) {
        const value = JSON.parse(localStorage.getItem('duration_pure'));
        const filterArr = value.find(item => item.id === this.idPureContent);
        if (filterArr) {
          //filterArr.duration = this.currentTime;
          // Nếu có tua và % hiện tại < % đã lưu local ở trước thì lấy ở local;

          const cloneFilter = JSON.parse(JSON.stringify(filterArr));
          filterArr.duration = this.percent;
          filterArr.percent = this.isTrackingPure ? (cloneFilter.percent > this.percentBeforePure ? cloneFilter.percent : this.percentBeforePure) : ((this.percent > cloneFilter.percent ? this.percent : cloneFilter.percent) > this.percentBeforePure ? (this.percent > cloneFilter.percent ? this.percent : cloneFilter.percent) : this.percentBeforePure);
          filterArr.dateTimeUpdate = + new Date();
          // if (!this.isTrackingDocument) {
          //   filterArr.percent_main = this.percent;
          // }
          // filterArr.percent_temp = this.percent;
          filterArr.dateTimeUpdate = + new Date();
          filterArr.type = 'pure';
          filterArr.isTracking = this.isTrackingPure;
          filterArr.isClickForward = this.isClickForwardPure;
          this.savePercentLocal(filterArr.id, filterArr.percent, filterArr.contentID);
        } else {

          value.push({
            id: this.idPureContent,
            duration: this.percent,
            percent: this.isTrackingPure ? (this.percentBeforePure > 0 ? this.percentBeforePure : 0) : this.percent,
            dateTimeUpdate: + new Date(),
            type: 'pure',
            isTracking: this.isTrackingPure,
            isClickForward: this.isClickForwardPure,
            contentID: this.idContentCurrent
          });
          this.savePercentLocal(value[value.length - 1].id, value[value.length - 1].percent, value[value.length - 1].contentID);
        }
        localStorage.setItem('duration_pure', JSON.stringify(value));
      } else {
        const objDocument = [
          {
            id: this.idPureContent,
            duration: this.percent,
            percent: this.isTrackingPure ? (this.percentBeforePure > 0 ? this.percentBeforePure : 0) : this.percent,
            dateTimeUpdate: + new Date(),
            type: 'pure',
            isTracking: this.isTrackingPure,
            isClickForward: this.isClickForwardPure,
            contentID: this.idContentCurrent
          }
        ];
        this.savePercentLocal(objDocument[0].id, objDocument[0].percent, objDocument[0].contentID);
        localStorage.setItem('duration_pure', JSON.stringify(objDocument));
      }
    }
  }


  // Hover vào ẩn hiện nút volume
  onHoverVolume(isHover: boolean): void {
    this.isShowVolume = isHover ? true : false;
  }

  // Lấy toàn bộ thông số của player
  public getAllProperties(): void {
    this._progressPlayerService.allPropertiesPlayer$.pipe(takeUntil(this.destroy)).subscribe((value: any) => {
      if (value) {
        return {
          configPlayer: {
            type: value.type,
            play: this.isPlay,
            adjustVideo: this.adjust,
            volume: this.volume,
            // breakpoint: this.breakPoint
          },
          sound: {
            link: value.link,
            value: this.progress,
          },
        };
      }
    });
  }

  // Lấy toàn bộ thông số của document
  public getAllPropertiesDocument(): void {
    this._progressPlayerService.allPropertiesDocument$.pipe(takeUntil(this.destroy)).subscribe((value: any) => {
      if (value) {
        return {
          configDocument: {
            type: value.type,
            play: this.isPlay,
            adjust: this.adjust,
            // volume: this.volume,
            // breakpoint: this.breakPoint
          },
          document: {
            link: value.link,
            value: this.progress,
          },
        };
      }
    });
  }

  handleResize({ target }): void {
    this.widthScreen = target.innerWidth;
    if (this.widthScreen >= 525) {
      this.toogleMobile = true;
    } else {
      this.toogleMobile = false;
    }
  }

  toogleMenuMobile(): void {
    this.toogleMobile = !this.toogleMobile;
  }

  // Lấy toàn bộ thông số của document
  public getAllPropertiesPuret(): void {
    this._progressPlayerService.allPropertiesPure$.pipe(takeUntil(this.destroy)).subscribe((value: any) => {
      if (value) {
        return {
          configDocument: {
            type: value.type,
            play: this.isPlay,
            adjust: this.adjust,
            // volume: this.volume,
            // breakpoint: this.breakPoint
          },
          pure: {
            id: value.idPureContent,
            value: this.progress,
          },
        };
      }
    });
  }
}

export interface BreakPoint {
    position : number;
    icon: string,
    todo: string,
    isDisable: boolean;
}
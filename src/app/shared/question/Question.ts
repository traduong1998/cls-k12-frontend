import { Answer } from "./Answer";

export interface Question{
    id:number;
    content: string;
    type:string;
    isAutoApproval?:boolean;
    level:string;
    listAnswer:Answer[];
}
export interface QuestionGroup{
    id: number;
    globalContent:string;
    isShuffleChild:boolean;
    Questions: Question[] ;
}
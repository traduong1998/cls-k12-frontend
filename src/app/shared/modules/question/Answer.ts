export interface Answer {
    id?: number;
    NoiDung: string;
    dapAnDung?:number;
    viTri?:boolean;
    xaoTronDapAn?:boolean;
}
export class SingleChoiceAnswer implements Answer{
    id: number;
    NoiDung: string;
    dapAnDung?: number;
    xaoTronDapAn: boolean;
}
export class MultiChoiceAnswer implements Answer{
    id: number;
    NoiDung: string;
    dapAnDung?: number;
    xaoTronDapAn: boolean;
}
export class TrueFalseClaseAnswer implements Answer{
    id: number;
    NoiDung: string;
    dapAnDung?: number;
    xaoTronDapAn: boolean;
}
export class TrueFalseAnswer implements Answer{
    dapAnDung?: number;
    NoiDung: string;
}

export class FillBlankAnswer implements Answer{
    id: number;
    NoiDung: string;
    dapAnDung?: number;
    xaoTronDapAn?: boolean=false;
}
export class FillBlank2Answer implements Answer{
    id: number;
    NoiDung: string;
    viTri:boolean;
    dapAnDung?: number;
    xaoTronDapAn?: boolean;
}
export class UnderLineAnswer implements Answer{
    id?: number;
    NoiDung: string;
    dapAnDung?: number;
    stt:number;
}
export class MatchingAnswer implements Answer{
    id?: number;
    NoiDung: string;
    dapAnDung?: number;
    viTri?: boolean;
    xaoTronDapAn?: boolean;

}
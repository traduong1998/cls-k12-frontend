export interface SupportMessage{
    supportMessageId:number;
    supportType:string;
    userId:number;
    examId:number;
    supervisorId?:number;
    userDescription?:string;
    supervisorDescription?:string;
    timeRequest:Date;
    timeResponse?:Date;
    status?:string;
}

export interface UserExercise {
    questionId: number;
    userAnswers: AnswerOfUser[];
    /**Đánh dấu */
    isMark: boolean;
    /**Đã trả lời */
    isAnswered: boolean;
    isEssayQuestion?: boolean;
    /**link file câu tự luận */
    fileUrl?: string;
    /**Nội dung trả lời câu tự luận */
    content?: string;
}
export interface AnswerOfUser {
    answerId: number;
    answer: number;
    fillInPosition?: number;
}

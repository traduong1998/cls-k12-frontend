export interface ConfigUploadCkeditor {
    portalId: number;
    uploadUrl: string;
    token: string;
}
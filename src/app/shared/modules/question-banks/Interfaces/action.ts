export enum Action {
    Delete = 'DEL',
    Add = 'ADD',
    EditScore = 'SCO',
    EditAll = 'EDI',
    EditContent = 'CON',
    FlagRemove = 'FLR',
}
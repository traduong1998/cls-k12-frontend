import { NgModule } from '@angular/core';
import { QuestionBanksComponent } from './question-banks.component'
import { SharedModule } from 'src/app/shared/shared.module';
import { CKEditorModule } from 'ckeditor4-angular';
import { FormsModule } from '@angular/forms';
//import angular material
import { CreateQuestionComponent } from './views/create-question/create-question.view';
import { ContentComponent } from './components/content/content.component';
import { CreateSingleChoiceQuestionComponent } from './components/create-single-choice-question/create-single-choice-question.component';
import { CreateMultipleChoiceQuestionComponent } from './components/create-multiple-choice-question/create-multiple-choice-question.component';
import { CreateTrueFalseQuestionComponent } from './components/create-true-false-question/create-true-false-question.component';
import { CreateTrueFalseClauseQuestionComponent } from './components/create-true-false-clause-question/create-true-false-clause-question.component';

import { CreateUnderLineQuestionComponent } from './components/create-under-line-question/create-under-line-question.component';
import { CreateMissingWord1QuestionComponent } from './components/create-missing-word1-question/create-missing-word1-question.component';
import { CreateMissingWord2QuestionComponent } from './components/create-missing-word2-question/create-missing-word2-question.component';
import { CreateMatchingQuestionComponent } from './components/create-matching-question/create-matching-question.component';
import { CreateGroupQuestionComponent } from './components/create-group-question/create-group-question.component';
import { MathModule } from 'src/app/shared/math/math.module';
import { CreateEasayQuestionComponent } from './components/create-easay-question/create-easay-question.component';
import { EditQuestionComponent } from './components/edit-question/edit-question.component';
import { EditEssayQuestionComponent } from './components/edit-essay-question/edit-essay-question.component';
import { EditGroupQuestionComponent } from './components/edit-group-question/edit-group-question.component';
import { EditMatchingQuestionComponent } from './components/edit-matching-question/edit-matching-question.component';
import { EditMissingWord1QuestionComponent } from './components/edit-missing-word1-question/edit-missing-word1-question.component';
import { EditMissingWord2QuestionComponent } from './components/edit-missing-word2-question/edit-missing-word2-question.component';
import { EditMultipleChoiceQuestionComponent } from './components/edit-multiple-choice-question/edit-multiple-choice-question.component';
import { EditSingleChoiceQuestionComponent } from './components/edit-single-choice-question/edit-single-choice-question.component';
import { EditTrueFalseClauseQuestionComponent } from './components/edit-true-false-clause-question/edit-true-false-clause-question.component';
import { EditTrueFalseQuestionComponent } from './components/edit-true-false-question/edit-true-false-question.component';
import { EditUnderLineQuestionComponent } from './components/edit-under-line-question/edit-under-line-question.component';

@NgModule({
  declarations: [
    QuestionBanksComponent,
    CreateQuestionComponent,
    ContentComponent,
    CreateSingleChoiceQuestionComponent,
    CreateMultipleChoiceQuestionComponent,
    CreateTrueFalseQuestionComponent,
    CreateTrueFalseClauseQuestionComponent,
    CreateUnderLineQuestionComponent,
    CreateMissingWord1QuestionComponent,
    CreateMissingWord2QuestionComponent,
    CreateMatchingQuestionComponent,
    CreateGroupQuestionComponent,
    CreateEasayQuestionComponent,
    EditQuestionComponent,
    EditEssayQuestionComponent,
    EditGroupQuestionComponent,
    EditMatchingQuestionComponent,
    EditMissingWord1QuestionComponent,
    EditMissingWord2QuestionComponent,
    EditMultipleChoiceQuestionComponent,
    EditSingleChoiceQuestionComponent,
    EditTrueFalseClauseQuestionComponent,
    EditTrueFalseQuestionComponent,
    EditUnderLineQuestionComponent
  ],
  imports: [
    SharedModule,
    CKEditorModule,
    FormsModule,
    MathModule
  ],
  exports: [
    QuestionBanksComponent,
    CreateQuestionComponent,
    ContentComponent,
    CreateSingleChoiceQuestionComponent,
    CreateMultipleChoiceQuestionComponent,
    CreateTrueFalseQuestionComponent,
    CreateTrueFalseClauseQuestionComponent,
    CreateUnderLineQuestionComponent,
    CreateMissingWord1QuestionComponent,
    CreateMissingWord2QuestionComponent,
    CreateMatchingQuestionComponent,
    CreateGroupQuestionComponent,
    CreateEasayQuestionComponent,
    EditQuestionComponent,
    EditEssayQuestionComponent,
    EditGroupQuestionComponent,
    EditMatchingQuestionComponent,
    EditMissingWord1QuestionComponent,
    EditMissingWord2QuestionComponent,
    EditMultipleChoiceQuestionComponent,
    EditSingleChoiceQuestionComponent,
    EditTrueFalseClauseQuestionComponent,
    EditTrueFalseQuestionComponent,
    EditUnderLineQuestionComponent
  ]
})
export class QuestionBanksModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTrueFalseClauseQuestionComponent } from './edit-true-false-clause-question.component';

describe('EditTrueFalseClauseQuestionComponent', () => {
  let component: EditTrueFalseClauseQuestionComponent;
  let fixture: ComponentFixture<EditTrueFalseClauseQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditTrueFalseClauseQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTrueFalseClauseQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

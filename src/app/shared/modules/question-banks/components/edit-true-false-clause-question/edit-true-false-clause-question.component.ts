import { AnswerViewData } from './../../Interfaces/question-data';
import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';;
import { ckConfig } from 'src/environments/ckeditorConfig';
import { QuestionViewData } from '../../Interfaces/question-data';
import { Action } from '../../Interfaces/action';
import { FormControl } from '@angular/forms';
import { QuestionTypeOption } from '../../Interfaces/question-type-option';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { QuestionLevelOption } from '../../Interfaces/question-level-option';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { ConfigUploadCkeditor } from '../../Interfaces/config-upload-ckeditor';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-edit-true-false-clause-question',
  templateUrl: './edit-true-false-clause-question.component.html',
  styleUrls: ['./edit-true-false-clause-question.component.scss']
})
export class EditTrueFalseClauseQuestionComponent implements OnInit {
  @Input() dataQuestion: QuestionViewData;
  @Input() isGroup = false;
  @Input() hideAction = false;
  @Input() configUpload: ConfigUploadCkeditor;
  @Output() questionOutput = new EventEmitter();
  //get type,level
  //question: QuestionViewData;
  isDelete = false;
  configQuestion = {};
  answers: AnswerViewData[] = [];
  editorData = `<p>This is a CKEditor 4 WYSIWYG editor instance created with Angular.</p>`;
  configAnswer = {
    // startupFocus: true,
    height: 60,
    allowedContent: true,

    extraPlugins: ckConfig.extraPlugins,
    toolbarGroups: ckConfig.toolbarAnswer,
    removeButtons: ckConfig.removeButtonsAnswer,
    removePlugins: 'elementspath,magicline'
  };
  disabledUpdate: boolean = false;
  // danh sách waiting timeout object của answer
  // dùng để delay cập nhật phương án khi người dùng gõ thay đổi phương án
  answerWTOs = [];
  @ViewChildren("cks") cks: QueryList<CKEditorComponent>;
  @ViewChild("ck") ck: CKEditorComponent;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  ckeditorContent = "";
  questionTypeCtrl = new FormControl();
  questionLevelCtrl = new FormControl();

  levelQuestionEnum: QuestionTypeOption[] = [
    { id: Level.NB, name: "Nhận biết" },
    { id: Level.TH, name: "Thông hiểu" },
    { id: Level.VD, name: "Vận dụng" },
    { id: Level.VDC, name: "Vận dụng cao" }];

  typeQuestionEnum: QuestionLevelOption[] = [
    { id: Type.singlechoice, name: "Một lựa chọn" },
    { id: Type.multichoice, name: "Nhiều lựa chọn" },
    { id: Type.underline, name: "Gạch chân" },
    { id: Type.fillblank, name: "Điền khuyết" },
    { id: Type.fillblank2, name: "Điền khuyết loại 2" },
    { id: Type.matching, name: "Ghép đôi" },
    { id: Type.truefasle, name: "Đúng sai" },
    { id: Type.truefaslechause, name: "Mệnh đề đúng sai" },
    { id: Type.essay, name: "Tự luận" }];
  constructor(private _messageService:MessageService) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: true,
      allowedContent: true,
      extraPlugins: ckConfig.extraPlugins,
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
      removePlugins: 'elementspath,magicline'
    };
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    clearTimeout(this.wto);
    this.answerWTOs.map(x => {
      clearTimeout(x.wto);
    })
    this.disabledUpdate = false;
    if (this.dataQuestion != null) {
      this.ckeditorContent = this.dataQuestion.content;
      this.answers = this.dataQuestion.answers;

      // Khởi tạo thứ tự phương án trả lời
      this.answers.map((ans, i) => {
        ans.order = i;
      });
      this.questionTypeCtrl.setValue(this.dataQuestion.type);
      this.questionLevelCtrl.setValue(this.dataQuestion.level);
    }
  }

  ngAfterViewInit(): void {
    this.cks.changes.subscribe((x) => {
      this.initAnswer(x.last);
    });

    this.cks.forEach((x) => {
      this.initAnswer(x);
    });

    this.initQuestionContent(this.ck);
  }

  ngOnDestroy(): void {
    clearTimeout(this.wto);
  }

  updateQuestion(notifi?: boolean): void {
    if (this.getvalue() != null) {
      this.questionOutput.emit(this.dataQuestion);
      if (notifi) {
        this._messageService.susccessCallback("Đã áp dụng");
      }
    }
  }

  getActiveAnswer() {
    return this.answers.filter(x => x.action != Action.Delete);
  }

  changeSelectAnswer(i) {
    let answer = this.answers[i];
    if (answer.trueAnswer == 0) {
      answer.trueAnswer = 1;
    }
    else {
      answer.trueAnswer = 0;
    }
    if (answer.action != Action.Add) {
      answer.action = Action.EditContent;
    }
  }

  setAnswers() {
    return this.answers;
  }

  setQuestion() {
    let err = 0;
    var check: boolean;
    check = this.getActiveAnswer().some(x => (isNullOrEmpty(x.content)));

    if (isNullOrWhiteSpace(this.dataQuestion.content) || isNullOrEmpty(this.dataQuestion.content)) {
      err++;
      alert("Bạn chưa nhập nội dung câu hỏi cho câu mệnh đề đúng sai");
      return { content: err.toString(), answers: this.answers };
    }
    else if (check == true) {
      err++;
      alert("Bạn chưa nhập nội dung câu trả lời cho câu mệnh đề đúng sai");
      return { content: err.toString(), answers: this.answers };
    }
    else {
      this.dataQuestion.answers = this.answers;
      return this.dataQuestion;
    }
  }

  getvalue() {
    var check: boolean;
    check = this.getActiveAnswer().some(x => (isNullOrEmpty(x.content)));
    if (isNullOrWhiteSpace(this.dataQuestion.content) || isNullOrEmpty(this.dataQuestion.content)) {
      alert("Bạn chưa nhập nội dung câu hỏi");

      //this.question = { id: 5, content: null, type:Type.truefaslechause, level: Level.NB, answers: this.answers,formatQuestion:FormatQuestion.single,questionGroupId:null, creatorId:null}
      return null;
    } else if(this.answers.length === 0){
      alert("Bạn chưa thêm đáp án nào");
      return null;
    }
    else if (check == true) {
      alert("Bạn chưa nhập nội dung câu trả lời");
      //this.question = { id: 5, content: null, type:Type.truefaslechause, level: Level.NB, answers: this.answers,formatQuestion:FormatQuestion.single,questionGroupId:null, creatorId:null}
      return null;
    }
    else {
      //this.question = { id: 5, content: this.ckeditorContent, type:Type.truefaslechause, level: Level.NB, answers: this.answers,formatQuestion:FormatQuestion.single,questionGroupId:null, creatorId:null}
      this.dataQuestion.level = this.questionLevelCtrl.value;
      this.dataQuestion.answers = this.answers;
      return this.dataQuestion;
    }
  }

  addAnswer() {
    if (this.getActiveAnswer().length < 9) {
      this.answers.push({
        id: generateRandomUniqueInteger(1000, 9999),
        content: '', questionId: this.dataQuestion.id, trueAnswer: 1, isShuffler: false, action: Action.Add, order: this.getActiveAnswer().length
      });
      if (this.getActiveAnswer().length > 1) {
        this.isDelete = false;
      }
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }

  deleteAnswer(index) {
    let answer = this.answers[index];

    if (answer.action == Action.Add) {
      this.answers.splice(index, 1);
    }
    else {
      answer.action = Action.Delete;
    }

    if (this.getActiveAnswer().length == 1) {
      this.isDelete = true;
    }

    this.answers.map((ans, i) => {
      if (ans.order > answer.order) {
        ans.order--;
      }
    });

    answer.order = 0;
  }

  private initAnswer(ck: CKEditorComponent) {

    ck.ready.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'none';
      this.answerWTOs.push({ id: ck.instance.id, wto: null });
    });
    ck.focus.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'block';
    });
    ck.blur.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'none';
    });
  }

  private initQuestionContent(ck: CKEditorComponent) {

    // ck.ready.subscribe((ev) => {
    //   document.getElementById(ck.instance.id + '_top').style.display = 'none';
    // });
    // ck.focus.subscribe((ev) => {
    //   document.getElementById(ck.instance.id + '_top').style.display = 'block';
    // });
    // ck.blur.subscribe((ev) => {
    //   document.getElementById(ck.instance.id + '_top').style.display = 'none';
    // });
  }

  wto;
  onQuestionContentChange(e) {
    let content = e.editor.getData();
    clearTimeout(this.wto);
    this.disabledUpdate = true;
    this.wto = setTimeout(() => {
      // do stuff when user has been idle for 1 second
      if (this.dataQuestion.content != content.trim()) {
        if (this.dataQuestion.action == Action.EditScore) {
          this.dataQuestion.action = Action.EditAll;
        } if (this.dataQuestion.action != Action.EditAll) {
          this.dataQuestion.action = Action.EditContent
        }
        this.dataQuestion.content = content.trim();
      }
      this.disabledUpdate = false;
    }, 300);
  }

  onAnswerContentChange(e, answer) {
    //two way binding content, delay 1s rồi mới cập nhật action
    let wtoIndex = this.answerWTOs.findIndex(x => x.id == e.editor.id);
    let content = e.editor.getData();
    clearTimeout(this.answerWTOs[wtoIndex].wto);
    this.answerWTOs[wtoIndex].wto = null;
    this.disabledUpdate = true;
    this.answerWTOs[wtoIndex].wto = setTimeout(() => {
      let i = this.answers.indexOf(answer);
      if (this.answers[i].content != content) {
        if (this.answers[i].action != Action.Add) {
          this.answers[i].action = Action.EditContent;
        }

        this.answers[i].content = content;
      }
      this.disabledUpdate = false;
    }, 300);
  }
}

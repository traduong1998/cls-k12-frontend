import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { CKEditorComponent } from 'ckeditor4-angular';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { TrueFalseAnswer } from 'src/app/shared/modules/question/Answer';
import { MessageService } from 'src/app/shared/services/message.service';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { Action } from '../../Interfaces/action';
import { ConfigUploadCkeditor } from '../../Interfaces/config-upload-ckeditor';
import { AnswerViewData, QuestionViewData } from '../../Interfaces/question-data';
import { QuestionLevelOption } from '../../Interfaces/question-level-option';
import { QuestionTypeOption } from '../../Interfaces/question-type-option';

@Component({
  selector: 'app-edit-true-false-question',
  templateUrl: './edit-true-false-question.component.html',
  styleUrls: ['./edit-true-false-question.component.scss']
})
export class EditTrueFalseQuestionComponent implements OnInit {
  @Input() dataQuestion: QuestionViewData;
  @Input() isGroup = false;
  @Input() hideAction = false;
  @Input() configUpload: ConfigUploadCkeditor;
  @Output() questionOutput = new EventEmitter();

  answers: AnswerViewData[];
  ckeditorContent = "";
  //question: QuestionViewData

  configQuestion;
  disabledUpdate: boolean = false;
  //get type,level
  @ViewChild("ck") ck: CKEditorComponent;
  selected = 'true';
  answer: AnswerViewData[] = [{ content: 'ans-true', trueAnswer: 1, isShuffler: false, questionId: null }];
  questionTypeCtrl = new FormControl();
  questionLevelCtrl = new FormControl();

  levelQuestionEnum: QuestionTypeOption[] = [
    { id: Level.NB, name: "Nhận biết" },
    { id: Level.TH, name: "Thông hiểu" },
    { id: Level.VD, name: "Vận dụng" },
    { id: Level.VDC, name: "Vận dụng cao" }];

  typeQuestionEnum: QuestionLevelOption[] = [
    { id: Type.singlechoice, name: "Một lựa chọn" },
    { id: Type.multichoice, name: "Nhiều lựa chọn" },
    { id: Type.underline, name: "Gạch chân" },
    { id: Type.fillblank, name: "Điền khuyết" },
    { id: Type.fillblank2, name: "Điền khuyết loại 2" },
    { id: Type.matching, name: "Ghép đôi" },
    { id: Type.truefasle, name: "Đúng sai" },
    { id: Type.truefaslechause, name: "Mệnh đề đúng sai" },
    { id: Type.essay, name: "Tự luận" }];
  constructor(private _messageService:MessageService) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: false,
      allowedContent: true,
      extraPlugins: ckConfig.extraPlugins,
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
      removePlugins: 'elementspath,magicline'
    };
  }

  ngOnChanges(): void {
    clearTimeout(this.wto);
    this.disabledUpdate = false;
    if (this.dataQuestion != null) {
      this.ckeditorContent = this.dataQuestion.content;
      this.answers = this.dataQuestion.answers;
      if(this.answers.length > 0){
        if (this.answers[0]?.content == 'ans-true') {
          // this.answers = [{ content: 'ans-true', trueAnswer: 1, isShuffler: false, questionId: null  }];
          this.selected = 'true';
        } else {
          // this.answers = [{ content: 'ans-false', trueAnswer: 0, isShuffler: false, questionId: null  }];
          this.selected = 'false';
        }
      }else{
        this.answers = [{ content: 'ans-true', trueAnswer: 1, isShuffler: false, questionId: null  }];
      }
      this.questionTypeCtrl.setValue(this.dataQuestion.type);
      this.questionLevelCtrl.setValue(this.dataQuestion.level);
    }
  }
  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.initAnswer(this.ck);
  }

  ngOnDestroy(): void {
    clearTimeout(this.wto);
  }

  updateQuestion(notifi?: boolean): void {
    if (this.getvalue() != null) {
      this.questionOutput.emit(this.dataQuestion);
      if (notifi) {
        this._messageService.susccessCallback("Đã áp dụng");
      }
    }
  }

  ChangeAnswer(event) {
    if (event.value == 'true') {
      // this.answers = [{ content: 'ans-true', trueAnswer: 1, isShuffler: false, questionId: null, action: Action.EditContent  }]
      this.answers[0].content = 'ans-true';
      this.answers[0].trueAnswer = 1;
      this.answers[0].action = Action.EditContent;
      this.selected = 'true';
    } else {
      // this.answers = [{ content: 'ans-false', trueAnswer: 0, isShuffler: false, questionId: null, action: Action.EditContent  }]
      this.answers[0].content = 'ans-false';
      this.answers[0].trueAnswer = 0;
      this.answers[0].action = Action.EditContent;
      this.selected = 'false';
    }
  }

  setAnswers() {
    return this.answer;
  }

  setQuestion() {
    let err;
    if (isNullOrWhiteSpace(this.dataQuestion.content) || isNullOrEmpty(this.dataQuestion.content)) {
      err++;
      alert("Bạn chưa nhập nội dung câu hỏi");
      return { content: err.toString(), answers: [] };
    }
    else {
      this.dataQuestion.answers = this.answers;
      return this.dataQuestion;
    }
  }

  getvalue(): QuestionViewData {
    if (isNullOrWhiteSpace(this.dataQuestion.content) || isNullOrEmpty(this.dataQuestion.content)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
    }
    else {
      //CHECK: type -> typeOfQuestion, creatorId thieu
      // this.question = { id: 4, content: this.ckeditorContent, type: Type.truefasle, level: Level.NB, answers: this.answers, formatQuestion: FormatQuestion.single, questionGroupId: null, creatorId: null }
      // console.log(this.question);
      this.dataQuestion.answers = this.answers;
      this.dataQuestion.level = this.questionLevelCtrl.value;
      return this.dataQuestion;
    }
  }

  private initAnswer(ck: CKEditorComponent) {
    // ck.ready.subscribe((ev) => {
    //   document.getElementById(ck.instance.id + '_top').style.display = 'none';
    // });
    // ck.focus.subscribe((ev) => {
    //   document.getElementById(ck.instance.id + '_top').style.display = 'block';
    // });
    // ck.blur.subscribe((ev) => {
    //   document.getElementById(ck.instance.id + '_top').style.display = 'none';
    // });
  }

  wto;
  onQuestionContentChange(e) {
    let content = e.editor.getData();
    clearTimeout(this.wto);
    this.disabledUpdate = true;
    this.wto = setTimeout(() => {
      // do stuff when user has been idle for 1 second
      if (this.dataQuestion.content != content.trim()) {
        if (this.dataQuestion.action == Action.EditScore) {
          this.dataQuestion.action = Action.EditAll;
        } if (this.dataQuestion.action != Action.EditAll) {
          this.dataQuestion.action = Action.EditContent
        }
        this.dataQuestion.content = content.trim();
      }
      this.disabledUpdate = false;
    }, 300);
  }
}

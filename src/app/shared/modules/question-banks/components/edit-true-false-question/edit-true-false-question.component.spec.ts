import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTrueFalseQuestionComponent } from './edit-true-false-question.component';

describe('EditTrueFalseQuestionComponent', () => {
  let component: EditTrueFalseQuestionComponent;
  let fixture: ComponentFixture<EditTrueFalseQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditTrueFalseQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTrueFalseQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTrueFalseClauseQuestionComponent } from './create-true-false-clause-question.component';

describe('CreateTrueFalseClauseQuestionComponent', () => {
  let component: CreateTrueFalseClauseQuestionComponent;
  let fixture: ComponentFixture<CreateTrueFalseClauseQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateTrueFalseClauseQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTrueFalseClauseQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

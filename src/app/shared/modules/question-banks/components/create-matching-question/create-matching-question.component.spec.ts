import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMatchingQuestionComponent } from './create-matching-question.component';

describe('CreateMatchingQuestionComponent', () => {
  let component: CreateMatchingQuestionComponent;
  let fixture: ComponentFixture<CreateMatchingQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateMatchingQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMatchingQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

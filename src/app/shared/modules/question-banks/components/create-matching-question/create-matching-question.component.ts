import { AnswerViewData } from './../../Interfaces/question-data';
import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { MatchingAnswer } from 'sdk/cls-k12-sdk-js/src/services/question/models/Answer';
import { Question } from 'sdk/cls-k12-sdk-js/src/services/question/models/Question';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { CreateQuestionEvent } from '../../Interfaces/create-question-event';
import { QuestionViewData } from '../../Interfaces/question-data';
import { LoadContentService } from '../../Services/load-content.service';
import { Action } from '../../Interfaces/action';
import { FormControl } from '@angular/forms';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { QuestionLevelOption } from '../../Interfaces/question-level-option';
import { QuestionTypeOption } from '../../Interfaces/question-type-option';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { ConfigUploadCkeditor } from '../../Interfaces/config-upload-ckeditor';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-create-matching-question',
  templateUrl: './create-matching-question.component.html',
  styleUrls: ['./create-matching-question.component.scss']
})
export class CreateMatchingQuestionComponent implements OnInit, CreateQuestionEvent {
  @Input() dataQuestion: QuestionViewData;
  @Output() questionOutput = new EventEmitter();
  @Input() hideAction = false;
  @Input() configUpload: ConfigUploadCkeditor;
  hasLeftFakeAnswer?: boolean;
  soDapAnDung = 0;
  // Nếu là nhiễu trái thì sẽ ++ => là số dương
  // Nếu là nhiễu phải thì sẽ -- => là số âm
  soDapAnNhieu = 0;
  configQuestion = {};

  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  answers: AnswerViewData[] = []
  countCk = 8;
  isReady: boolean;
  disabledUpdate: boolean = false;
  configAnswer = {
    height: 60,
    allowedContent: true,
    extraPlugins: ckConfig.extraPlugins,
    toolbarGroups: ckConfig.toolbarAnswer,
    removeButtons: ckConfig.removeButtonsAnswer,
    removePlugins: 'elementspath,magicline'
  };
  ckeditorContent = "";
  // danh sách waiting timeout object của answer
  // dùng để delay cập nhật phương án khi người dùng gõ thay đổi phương án
  answerWTOs = [];

  questionTypeCtrl = new FormControl();
  questionLevelCtrl = new FormControl();

  levelQuestionEnum: QuestionTypeOption[] = [
    { id: Level.NB, name: "Nhận biết" },
    { id: Level.TH, name: "Thông hiểu" },
    { id: Level.VD, name: "Vận dụng" },
    { id: Level.VDC, name: "Vận dụng cao" }];

  typeQuestionEnum: QuestionLevelOption[] = [
    { id: Type.singlechoice, name: "Một lựa chọn" },
    { id: Type.multichoice, name: "Nhiều lựa chọn" },
    { id: Type.underline, name: "Gạch chân" },
    { id: Type.fillblank, name: "Điền khuyết" },
    { id: Type.fillblank2, name: "Điền khuyết loại 2" },
    { id: Type.matching, name: "Ghép đôi" },
    { id: Type.truefasle, name: "Đúng sai" },
    { id: Type.truefaslechause, name: "Mệnh đề đúng sai" },
    { id: Type.essay, name: "Tự luận" }];

  @ViewChildren("cks") cks: QueryList<CKEditorComponent>;
  @ViewChild("ck") ck: CKEditorComponent;
  constructor(private loadData: LoadContentService,private _messageService:MessageService) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      allowedContent: true,
      extraPlugins: ckConfig.extraPlugins,
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
      removePlugins: 'elementspath,magicline'
    };
  }

  ngOnChanges() {
    this.isReady = false;
    clearTimeout(this.wto);
    this.answerWTOs.map(x => {
      clearTimeout(x.wto);
    })
    this.disabledUpdate = false;

    if (this.dataQuestion != null) {
      this.ckeditorContent = this.dataQuestion.content;
      this.answers = this.dataQuestion.answers;
      this.initDataQuestion();
      this.questionTypeCtrl.setValue(this.dataQuestion.type);
      this.questionLevelCtrl.setValue(this.dataQuestion.level);
    }

    setTimeout(() => {
      this.isReady = true;
    }, 500);

  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.cks.changes.subscribe((x) => {
      let secondlast = x._results.length - 2;
      if (secondlast >= 0) {
        this.initAnswer(x._results[secondlast]);
      }
      this.initAnswer(x.last);
    });

    this.cks.forEach((x) => {
      this.initAnswer(x);
    });
  }

  updateQuestion(notifi?: boolean): void {
    // console.log(this.dataQuestion);
    if (this.getvalue() != null) {
      this.questionOutput.emit(this.dataQuestion);
      if (notifi) {
        this._messageService.susccessCallback("Đã áp dụng");
      }
    }
  }

  getvalue() {
    var check: boolean;
    check = this.answers.some(x => (isNullOrEmpty(x.content)));
    if (isNullOrWhiteSpace(this.dataQuestion.content) || isNullOrEmpty(this.dataQuestion.content)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
      return null;
    }
    else if (check == true) {
      alert("Bạn chưa nhập nội dung câu trả lời");
      return null;
    }
    else if (this.answers.length === 0){
      alert("Bạn chưa thêm phương án nào");
      return null;
    }
    else {
      this.dataQuestion.level = this.questionLevelCtrl.value;
      this.dataQuestion.answers = this.answers;
      return this.dataQuestion;
    }
  }

  addAnswer() {
    if (this.groupAnswers().length < 9) {
      // trueAnswer: ở đây là groupAnswerId của câu hỏi ghép đôi
      // nếu là cặp mệnh đề thì mỗi group có 2 answer
      // nếu là mệnh đề nhiễu thì chỉ có 1 answer
      //--answer nhiều là về trái nếu position = false
      //--answer nhiều là về phải nếu position = true

      this.answers.push(
        {
          id: generateRandomUniqueInteger(1000, 9999), content: "", position: false, questionId: this.dataQuestion.id,
          trueAnswer: this.groupAnswers().length + 1, isShuffler: false, action: Action.Add
        },
        {
          id: generateRandomUniqueInteger(1000, 9999), content: "", position: true, questionId: this.dataQuestion.id,
          trueAnswer: this.groupAnswers().length + 1, isShuffler: false, action: Action.Add
        });
      this.soDapAnDung++;

      console.log(`this.groupAnswers()`, this.groupAnswers());
      console.log(`this.answers()`, this.answers);
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }

  deleteAnswer(groupId) {

    // Số cặp mệnh đề đang >= 1, =1 thì có thể xóa đc phương án nhiễu
    if (this.soDapAnDung >= 1) {

      // Nếu là Xóa cặp mệnh đề
      if (this.answersOfGroup(groupId).length == 2) {
        this.soDapAnDung--;
      }
      else {

        if (this.hasLeftFakeAnswer == true) {
          // Nếu là Xóa phương án nhiễu trái
          this.soDapAnNhieu--;
        }
        else if (this.hasLeftFakeAnswer == false) {
          // Nếu là Xóa phương án nhiễu phải
          this.soDapAnNhieu++;
        }

        if (this.soDapAnNhieu == 0) {
          this.hasLeftFakeAnswer = null;
        }
      }


      this.answers = this.answers.filter(x => x.trueAnswer != groupId);

      this.answers.map((ans, i) => {
        if (ans.trueAnswer > groupId) {
          ans.trueAnswer = ans.trueAnswer - 1;
        }
      });
    }
  }

  addLeftInterference() {
    if (this.groupAnswers().length < 9) {
      this.answers.push({
        id: generateRandomUniqueInteger(1000, 9999), content: "", position: false, questionId: this.dataQuestion.id,
        trueAnswer: this.groupAnswers().length + 1, isShuffler: false, action: Action.Add
      });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
    this.soDapAnNhieu++;
    this.hasLeftFakeAnswer = true;
  }

  addRightInterference() {
    if (this.groupAnswers().length < 9) {
      this.answers.push({
        id: generateRandomUniqueInteger(1000, 9999), content: "", position: true, questionId: this.dataQuestion.id,
        trueAnswer: this.groupAnswers().length + 1, isShuffler: false, action: Action.Add
      });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
    this.soDapAnNhieu--;
    this.hasLeftFakeAnswer = false;
  }

  initDataQuestion() {
    this.soDapAnNhieu = 0;
    this.hasLeftFakeAnswer = null;
    // Check lần đầu tiên
    this.answers.filter((x, index) => {
      // Mệnh đề nhiễu
      if (this.answersOfGroup(x.trueAnswer).length < 2) {
        if (x.position == false) {
          this.hasLeftFakeAnswer = true;
          this.soDapAnNhieu++;
        } else {
          this.hasLeftFakeAnswer = false;
          this.soDapAnNhieu--;
        }
      } else {
        // 1 cặp mệnh đề thì có 2 answer cùng trueAnswer (groupID), chỉ lấy answer đầu tiên
        if (index % 2 == 0) {
          this.soDapAnDung++;
        }
      }
    })
  }

  private initAnswer(ck: CKEditorComponent) {
    ck.ready.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'none';
      this.answerWTOs.push({ id: ck.instance.id, wto: null });
    });
    ck.focus.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'block';
    });
    ck.blur.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'none';
    });
  }

  groupAnswers() {
    return [...new Set(this.answers.map(item => item.trueAnswer))]
  }

  answersOfGroup(groupId) {
    return this.answers.filter(x => x.trueAnswer == groupId);
  }

  answersOfGroupLeft(groupId) {
    return this.answers.find(x => x.trueAnswer == groupId && x.position == false);
  }
  answersOfGroupRight(groupId) {
    return this.answers.find(x => x.trueAnswer == groupId && x.position == true);
  }
  wto;
  onQuestionContentChange(e) {
    let content = e.editor.getData();
    clearTimeout(this.wto);
    this.disabledUpdate = true;
    this.wto = setTimeout(() => {
      // do stuff when user has been idle for 1 second
      if (this.dataQuestion.content != content.trim()) {
        this.dataQuestion.content = content.trim();
      }
      this.disabledUpdate = false;
    }, 300);
  }

  onAnswerContentChange(e, answer) {
    let content = e.editor.getData();
    //two way binding content, delay 1s rồi mới cập nhật action
    let wtoIndex = this.answerWTOs.findIndex(x => x.id == e.editor.id);
    clearTimeout(this.answerWTOs[wtoIndex].wto);
    this.answerWTOs[wtoIndex].wto = null;
    this.disabledUpdate = true;
    this.answerWTOs[wtoIndex].wto = setTimeout(() => {
      let i = this.answers.indexOf(answer);
      if (this.answers[i].content != content) {
        this.answers[i].content = content;
      }

      this.disabledUpdate = false;
    }, 300);
  }
}

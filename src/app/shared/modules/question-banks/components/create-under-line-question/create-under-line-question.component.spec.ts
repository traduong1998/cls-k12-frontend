import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUnderLineQuestionComponent } from './create-under-line-question.component';

describe('CreateUnderLineQuestionComponent', () => {
  let component: CreateUnderLineQuestionComponent;
  let fixture: ComponentFixture<CreateUnderLineQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateUnderLineQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUnderLineQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { flatMap } from 'lodash/fp';
import { Component, EventEmitter, Input, NgZone, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import * as $ from 'jquery';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { getSelectionHTML } from 'src/environments/ckeditorFuntion';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { CKEditorComponent } from 'ckeditor4-angular';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { CreateQuestionEvent } from '../../Interfaces/create-question-event';
import { Question } from 'sdk/cls-k12-sdk-js/src/services/question/models/Question';
import { AnswerViewData, QuestionViewData } from '../../Interfaces/question-data';
import { Action } from '../../Interfaces/action';
import { FormControl } from '@angular/forms';
import { QuestionTypeOption } from '../../Interfaces/question-type-option';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { QuestionLevelOption } from '../../Interfaces/question-level-option';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { ConfigUploadCkeditor } from '../../Interfaces/config-upload-ckeditor';
import { MessageService } from 'src/app/shared/services/message.service';

declare var CKEDITOR: any;
@Component({
  selector: 'app-create-under-line-question',
  templateUrl: './create-under-line-question.component.html',
  styleUrls: ['./create-under-line-question.component.scss']
})

export class CreateUnderLineQuestionComponent implements OnInit, CreateQuestionEvent {
  @Input() isGroup: boolean;
  @Input() configUpload: ConfigUploadCkeditor;
  @Output() questionOutput = new EventEmitter();
  answers: AnswerViewData[] = [];
  @Input() hideAction = false;
  @ViewChildren("cks") cks: QueryList<CKEditorComponent>;
  @Input() dataQuestion: QuestionViewData;
  flexLayout;
  selectedAnswer;
  configQuestion;
  configAnswer = {
    // startupFocus: true,
    height: 60,
    allowedContent: true,
    fullPage: true,
    extraPlugins: ckConfig.extraPlugins,
    toolbarGroups: ckConfig.toolbarAnswer,
    removeButtons: ckConfig.removeButtonsAnswer,
    removePlugins: 'elementspath,magicline'
  };
  disabledUpdate: boolean = false;
  ckeditorContent = "";
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  questionTypeCtrl = new FormControl();
  questionLevelCtrl = new FormControl();

  levelQuestionEnum: QuestionTypeOption[] = [
    { id: Level.NB, name: "Nhận biết" },
    { id: Level.TH, name: "Thông hiểu" },
    { id: Level.VD, name: "Vận dụng" },
    { id: Level.VDC, name: "Vận dụng cao" }];

  typeQuestionEnum: QuestionLevelOption[] = [
    { id: Type.singlechoice, name: "Một lựa chọn" },
    { id: Type.multichoice, name: "Nhiều lựa chọn" },
    { id: Type.underline, name: "Gạch chân" },
    { id: Type.fillblank, name: "Điền khuyết" },
    { id: Type.fillblank2, name: "Điền khuyết loại 2" },
    { id: Type.matching, name: "Ghép đôi" },
    { id: Type.truefasle, name: "Đúng sai" },
    { id: Type.truefaslechause, name: "Mệnh đề đúng sai" },
    { id: Type.essay, name: "Tự luận" }];
  @ViewChild("ck") ck: CKEditorComponent;

  constructor(private zone: NgZone,private _messageService:MessageService) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: false,
      allowedContent: true,
      extraPlugins: ckConfig.extraPlugins,
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
      removePlugins: 'elementspath,magicline'
    };
  }

  ngOnChanges(): void {
    clearTimeout(this.wto);
    this.disabledUpdate = false;
    if (this.dataQuestion != null) {
      this.ckeditorContent = this.dataQuestion.content;
      this.answers = this.dataQuestion.answers;
      this.questionTypeCtrl.setValue(this.dataQuestion.type);
      this.questionLevelCtrl.setValue(this.dataQuestion.level);
    }
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.initQuestionContent(this.ck);
  }

  ngOnDestroy(): void {
    clearTimeout(this.wto);
    this.disabledUpdate = false;
  }

  updateQuestion(notifi?: boolean): void {
    if (this.getvalue() != null) {
      this.questionOutput.emit(this.dataQuestion);
      if (notifi) {
        this._messageService.susccessCallback("Đã áp dụng");
      }
    }
  }

  setAnswers() {
    return this.answers;
  }

  getActiveAnswer() {
    return this.answers;
  }

  setQuestion() {
    let err = 0;
    if (isNullOrWhiteSpace(this.dataQuestion.content) || isNullOrEmpty(this.dataQuestion.content)) {
      err++
      alert("Bạn chưa nhập nội dung câu hỏi câu gạch chân");
      return { content: err.toString(), answers: [] };
    }
    else if (this.getActiveAnswer().length == 0 || this.answers == null) {
      err++;
      alert("Bạn chưa đưa ra phương án lựa chọn cho câu gạch chân");
      return { content: err.toString(), answers: [] };
    }
    else if (this.getActiveAnswer().some(x => x.trueAnswer == 1) == false) {
      err++;
      alert("Bạn chưa chọn câu trả lời đúng câu gạch chân");
      return { content: err.toString(), answers: [] };
    }
    else {
      this.dataQuestion.answers = this.answers;
      return this.dataQuestion;
    }
  }

  getvalue() {
    var ckeEditableArea = $(this.ck.instance.editable().$);
    if (isNullOrWhiteSpace(this.dataQuestion.content) || isNullOrEmpty(this.dataQuestion.content)) {
      alert("Bạn chưa nhập nội dung câu hỏi");

      return null;
    }
    else if (this.getActiveAnswer().length == 0 || this.answers == null) {
      alert("Bạn chưa đưa ra phương án lựa chọn");

      return null;
    }
    else if (this.getActiveAnswer().length != ckeEditableArea.find('u').length) {
      alert("Số vị trí gạch chân phải bằng số lượng phương án đúng");

      return null;
    }
    else if (this.getActiveAnswer().some(x => x.trueAnswer == 1) == false) {
      alert("Bạn chưa chọn câu trả lời đúng");

      return null;
    }
    else {
      this.dataQuestion.level = this.questionLevelCtrl.value;
      this.dataQuestion.answers = this.answers;
      return this.dataQuestion;
    }
  }

  addAnswer(content, order) {
    if (this.getActiveAnswer().length < 9) {
      this.zone.run(() => {
        this.answers.push({
          content: content, questionId: this.dataQuestion.id,
          trueAnswer: 0, order: order, isShuffler: false, action: Action.Add
        });
        // Cập nhật lại vị trí selected.
        // Vị trí order của answer sau vị trí gạch chân mới sẽ bị thay đổi=> answer selected cần được update lại
        this.updateCurrentSelectedAnswer();
      });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }

  deleteAnswer(answer) {
    if (this.getActiveAnswer().length == 1) {
      return;
    }

    let index = this.answers.indexOf(answer);

    // Nếu phương án bị xóa là phương án được chọn, thì cập nhật lại
    if (answer.trueAnswer == 1) {
      this.selectedAnswer = undefined;
    }

    this.answers.splice(index, 1);

    this.answers.map((ans, i) => {
      if (ans.order > answer.order) {
        ans.order--;
        if (ans.trueAnswer == 1) {
          this.selectedAnswer = "checkboxG_" + ans.order;
        }
      }
    });

    var ckeEditableArea = $(this.ck.instance.editable().$);
    // Bỏ gạch chân phương án đã chọn để xóa.
    var htmlData = ckeEditableArea.find('*[data-order = ' + answer.order + ']');

    answer.order = 0;
    htmlData.replaceWith($(htmlData).html());
    this.dataQuestion.content = this.ck.instance.getData();

    this.capNhatDanhSachPhuongAn();
  }


  private initQuestionContent(ck: CKEditorComponent) {
    this.ck.ready.subscribe((ev) => {
      // sau khi init question ready thì duyệt qua các thẻ gạch chân, để gán order cho các phương án
      this.initFormatAnswerUnderline();
      this.ck.instance.on('beforeCommandExec', (event) => {
        return this.handleBeforeCommandExec(event, this.ck.instance);
      });
      this.ck.instance.on('afterCommandExec', (event) => {
        this.handleAfterCommandExec(event, this.ck.instance);
      });
    });
  }

  //Cập nhật lại danh sách các phương án, bổ sung vào thuộc tính order (update mới)
  initFormatAnswerUnderline() {
    this.answers.forEach(answer => {
      var ckeEditableArea = $(this.ck.instance.editable().$);
      let position = 0;
      let answerPlainText = this.htmlToText(answer.content);

      ckeEditableArea.find('u').each(function (index) {
        //can thiệp vào thẻ u
        $(this).attr('contenteditable', "false");
        $(this).attr('data-order', index);
        $(this).attr('class', 'ck-phuongan-underline');

        if ($(this).text() == answerPlainText) {
          position = index;
        }
      });

      answer.order = position;
      // Sau khi có order thì có thể cập nhật lại selectedAnswer theo order của phương án
      if (answer.trueAnswer == 1) {
        this.selectedAnswer = "checkboxG_" + answer.order;
      }
      return answer;
    });
  }

  updateAnswerByTextContent() {
    this.initFormatAnswerUnderline();
  }

  htmlToText(html) {
    var tmp = document.createElement('DIV'); // TODO: Check if this the way to go with Angular
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || '';
  }

  changeSelectAnswer(answer) {
    let index = this.answers.indexOf(answer);

    this.answers.map((ans, i) => {
      if (index == i) {
        ans.trueAnswer = 1;
      } else {
        ans.trueAnswer = 0;
      }
    });
  }

  changeOrder(index) {
    this.answers.map((ans, i) => {
      if (ans.order >= index) {
        this.answers[i].order += 1;
      }
    });
  }

  sortedAnswers() {
    if (this.answers.length > 0) {
      return this.answers.filter(x => x.order != undefined).sort((a, b) => a.order - b.order);
    }
  }
  // Kiểm tra sự kiện trước khi tạo đáp án
  handleBeforeCommandExec(event, editor) {
    var commandName = event.data.name;
    if (commandName == 'underline') {
      var mySelection = editor.getSelection();
      var selectedText = mySelection.getNative();
      var realSelectedText = getSelectionHTML(selectedText).toString();
      if (mySelection.getType() == CKEDITOR.SELECTION_TEXT) {
        //XLS
        if (CKEDITOR.env.ie) {
          mySelection.unlock(true);
          selectedText = mySelection.getNative();
        } else {
          selectedText = mySelection.getNative();
        }

        realSelectedText = selectedText.toString();


        if (realSelectedText.trim().length == 0) {
          return false;
        }
      }

      var currentTag;
      if (CKEDITOR.env.ie) {
        mySelection.unlock(true);
        selectedText = mySelection.getNative();
      } else {
        selectedText = mySelection.getNative();
      }
      if (realSelectedText.trim() == "") {
        return false;
      }
      if ((CKEDITOR.env.ie)) {
        currentTag = mySelection.getNative().anchorNode;
      } else {
        currentTag = mySelection.getNative().anchorNode.parentElement.localName;
      }
      // nếu người dùng spam để tạo tiếp phương án tại vị trí vừa mới tạo thì return false
      if ($(mySelection.getNative().focusNode).closest('u').length > 0) {
        return false;
      }
      // kiểm tra nếu phần gạch chân là video thì không cho gạch chân
      if ($(selectedText.baseNode) != undefined) {
        if ($(selectedText.baseNode).find('.cke_video').length > 0) {
          alert("Phương án chỉ bao gồm ký tự và số");
          return false;
        }

      }
      // kiểm tra nếu phần gạch chân là video thì không cho gạch chân
      if ($(selectedText.focusNode).find('.cke_video').length > 0) {
        alert("Phương án chỉ bao gồm ký tự và số");
        return false;
      }
      // Kiểm tra vùng đã tồn tại phương án
      if (/<u[^>]*>.+<\/u>/g.test(getSelectionHTML(selectedText)) || currentTag == 'u') {
        alert("Vùng chọn tồn tại phương án");
        return false;
      }
      // kiểm tra nếu phần gạch chân là ảnh
      if (/<span tabindex="-1"/g.test(getSelectionHTML(selectedText))) {
        alert("Phương án chỉ bao gồm ký tự và số");
        return false;
      }

      if (this.getActiveAnswer().length >= 9) {
        alert("Số phương án tối đa là 9");
        return false;
      }
    }

  }

  handleAfterCommandExec(event, editor): void {
    var commandName = event.data.name;
    // Kiểm tra sự kiện sau khi click tag U
    // For 'underline' commmand
    let id = generateRandomUniqueInteger(1000, 9999);

    if (commandName == "underline") {
      if (this.getActiveAnswer().length < 9) {
        let mySelection = editor.getSelection();
        var selectedText = mySelection.getNative();
        var realSelectedText = getSelectionHTML(selectedText).toString();

        // Xác định vùng được gạch chân
        if (mySelection.getType() == 2) {

          if (CKEDITOR.env.ie) {
            mySelection.unlock(true);
            selectedText = mySelection.getNative();
          }
          else {
            selectedText = mySelection.getNative();

            realSelectedText = selectedText.toString();

            if (realSelectedText.trim() == "") {
              return;
            }
          }
        }
        if (isNullOrEmpty(realSelectedText) || isNullOrWhiteSpace(realSelectedText)) {
          alert('Vùng chọn phải có ký tự, không được hoàn toàn là khoảng trắng.');
        }
        else {
          this.taoPhuongAn(realSelectedText);
        }
      }
      else {
        alert("Số phương án tối đa là 9");
      }
    }
  }


  //Cập nhật lại danh sách các phương án (update mới)
  taoPhuongAn(realSelectedText) {
    var ckeEditableArea = $(this.ck.instance.editable().$);
    ckeEditableArea.trigger('focusout');
    //remove all row
    $('.tbl-phuong-an-js > tbody').empty();
    let position = 0;
    ckeEditableArea.find('u').each(function (index) {
      //can thiệp vào thẻ u
      $(this).attr('contenteditable', "false");
      $(this).attr('data-order', index);
      $(this).attr('class', 'ck-phuongan-underline');
      if ($(this).text() == realSelectedText) {
        position = index;
      }
    });
    if (this.getActiveAnswer().length >= 1) {
      this.changeOrder(position);
    }

    this.addAnswer(realSelectedText, position);
  }

  capNhatDanhSachPhuongAn() {
    var ckeEditableArea = $(this.ck.instance.editable().$);
    //remove all row
    ckeEditableArea.find('u').each(function (index) {
      //can thiệp vào thẻ u
      $(this).attr('data-order', index);
    });
  }
  SaveAndCreate() {
    window.location.reload();
    alert("Đã lưu")
  }

  updateCurrentSelectedAnswer() {
    this.answers.map((ans, i) => {
      if (ans.trueAnswer == 1) {
        this.selectedAnswer = "checkboxG_" + ans.order;
      }
    });
  }

  wto;
  onQuestionContentChange(e) {
    let content = e.editor.getData();
    clearTimeout(this.wto);
    this.disabledUpdate = true;
    this.wto = setTimeout(() => {
      // do stuff when user has been idle for 1 second
      if (this.dataQuestion.content != content.trim()) {
        this.dataQuestion.content = content.trim();
      }
      this.disabledUpdate = false;
    }, 300);
  }
}

import { QuestionViewData } from './../../Interfaces/question-data';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { Question } from 'sdk/cls-k12-sdk-js/src/services/question/models/Question';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { CreateQuestionEvent } from '../../Interfaces/create-question-event';
import { FormControl } from '@angular/forms';
import { QuestionTypeOption } from '../../Interfaces/question-type-option';
import { QuestionLevelOption } from '../../Interfaces/question-level-option';
import { ConfigUploadCkeditor } from '../../Interfaces/config-upload-ckeditor';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-create-easay-question',
  templateUrl: './create-easay-question.component.html',
  styleUrls: ['./create-easay-question.component.scss']
})
export class CreateEasayQuestionComponent implements OnInit, CreateQuestionEvent {
  configQuestion = {};
  @Output() questionOutput = new EventEmitter();
  @Input() dataQuestion: QuestionViewData;
  @Input() configUpload: ConfigUploadCkeditor;
  @Input() hideAction = false;
  ckeditorContent = "";
  //get type,level
  isAutoApproval = false;
  //question: QuestionViewData;
  disabledUpdate: boolean = false;

  questionTypeCtrl = new FormControl();
  questionLevelCtrl = new FormControl();

  levelQuestionEnum: QuestionTypeOption[] = [
    { id: Level.NB, name: "Nhận biết" },
    { id: Level.TH, name: "Thông hiểu" },
    { id: Level.VD, name: "Vận dụng" },
    { id: Level.VDC, name: "Vận dụng cao" }];

  typeQuestionEnum: QuestionLevelOption[] = [
    { id: Type.singlechoice, name: "Một lựa chọn" },
    { id: Type.multichoice, name: "Nhiều lựa chọn" },
    { id: Type.underline, name: "Gạch chân" },
    { id: Type.fillblank, name: "Điền khuyết" },
    { id: Type.fillblank2, name: "Điền khuyết loại 2" },
    { id: Type.matching, name: "Ghép đôi" },
    { id: Type.truefasle, name: "Đúng sai" },
    { id: Type.truefaslechause, name: "Mệnh đề đúng sai" },
    { id: Type.essay, name: "Tự luận" }];
  @ViewChild("ck") ck: CKEditorComponent;
  constructor(private _messageService: MessageService) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      allowedContent: true,
      extraPlugins: ckConfig.extraPlugins,
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
      removePlugins: 'elementspath,magicline'
    };
  }
  updateQuestion(notifi?: boolean): void {
    if (this.getvalue() != null) {
      this.questionOutput.emit(this.dataQuestion);
      if (notifi) {
        this._messageService.susccessCallback("Đã áp dụng");
      }
    }
  }

  ngOnChanges(): void {
    clearTimeout(this.wto);
    this.disabledUpdate = false;
    if (this.dataQuestion != null) {
      this.ckeditorContent = this.dataQuestion.content;
      this.questionTypeCtrl.setValue(this.dataQuestion.type);
      this.questionLevelCtrl.setValue(this.dataQuestion.level);
    }
  }

  ngOnInit(): void {
    console.log(this.dataQuestion)
  }
  ngAfterViewInit(): void {

    this.initAnswer(this.ck)

    this.ck.ready.subscribe(() => {
      var head = this.ck.instance.document.getHead();
      var myscript = this.ck.instance.document.createElement('script', {
        attributes: {
          type: 'text/javascript',
          'src': 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-MML-AM_CHTML'
        }
      });
      head.append(myscript);
    });
  }
  ngOnDestroy(): void {
    clearTimeout(this.wto);
    this.disabledUpdate = false;
  }

  getvalue() {
    debugger;
    let editor = this.ck.instance;
    let questionContent = editor.document.getBody().getText();
    if (isNullOrWhiteSpace(questionContent) || isNullOrEmpty(questionContent)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
      return null;
    }
    else {
      //this.question = { id: 6, content: this.ckeditorContent, type: Type.essay, level: Level.NB, answers: null, formatQuestion: FormatQuestion.single, questionGroupId: null, creatorId: null }
      //console.log(this.question);
      //alert("Lưu thành công!!!")
      this.dataQuestion.level = this.questionLevelCtrl.value;
      return this.dataQuestion;
    }
  }

  private initAnswer(ck: CKEditorComponent) {

    // ck.ready.subscribe((ev) => {
    //   document.getElementById(ck.instance.id + '_top').style.display = 'none';
    // });
    // ck.focus.subscribe((ev) => {
    //   document.getElementById(ck.instance.id + '_top').style.display = 'block';
    // });
    // ck.blur.subscribe((ev) => {
    //   document.getElementById(ck.instance.id + '_top').style.display = 'none';
    // });
  }
  wto;
  onQuestionContentChange(e) {
    let content = e.editor.getData();
    clearTimeout(this.wto);
    this.disabledUpdate = true;
    this.wto = setTimeout(() => {
      // do stuff when user has been idle for 1 second
      if (this.dataQuestion.content != content.trim()) {

        this.dataQuestion.content = content.trim();
      }
      this.disabledUpdate = false;
    }, 300);
  }
}

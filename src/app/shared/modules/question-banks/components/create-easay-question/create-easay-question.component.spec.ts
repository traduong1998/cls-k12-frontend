import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEasayQuestionComponent } from './create-easay-question.component';

describe('CreateEasayQuestionComponent', () => {
  let component: CreateEasayQuestionComponent;
  let fixture: ComponentFixture<CreateEasayQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateEasayQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEasayQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMissingWord2QuestionComponent } from './edit-missing-word2-question.component';

describe('EditMissingWord2QuestionComponent', () => {
  let component: EditMissingWord2QuestionComponent;
  let fixture: ComponentFixture<EditMissingWord2QuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditMissingWord2QuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMissingWord2QuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

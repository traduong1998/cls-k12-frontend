import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { AnswerRequestOrResponse, QuestionViewData } from './../../Interfaces/question-data';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { QuestionRequestOrResponse } from '../../Interfaces/question-data';
import { ConfigUploadCkeditor } from '../../Interfaces/config-upload-ckeditor';

@Component({
  selector: 'app-edit-question',
  templateUrl: './edit-question.component.html',
  styleUrls: ['./edit-question.component.scss']
})
export class EditQuestionComponent implements OnInit {

  @Input() data: QuestionRequestOrResponse;
  @Input() configUpload: ConfigUploadCkeditor;
  
  @Input() childIndex: number;
  @Output() outputData = new EventEmitter();

  dataQuestionView: QuestionViewData;
  childIndexView: number;
  isReady: boolean;

  @ViewChild("ck") ck: CKEditorComponent;

  constructor() {

  }

  ngAfterViewInit(): void {
    this.isReady = true;
  }

  updateQuestion(response: QuestionViewData): void {
    console.log(`response: `, response);
    console.log(`this.data : `, this.data);
    let questionReturn: QuestionRequestOrResponse = { ...this.data };
    questionReturn.id = this.data.id;
    questionReturn.content = response.content;
    questionReturn.type = response.type;
    questionReturn.questionGroupId = response.questionGroupId;
    questionReturn.level = response.level;
    questionReturn.format = response.format;
    questionReturn.action = response.action;
    questionReturn.answers = [];
    questionReturn.questions = [];
    if (this.data.format == FormatQuestion.single) {
      response.answers.forEach((answer) => {
        questionReturn.answers.push({
          id: answer.id,
          questionId: answer.questionId,
          content: answer.content,
          trueAnswer: answer.trueAnswer,
          position: answer.position,
          order: answer.order,
          groupOfFillBlank2: answer.groupOfFillBlank2,
          isShuffler: false,
          action: answer.action,
        });
      });
    } else if (this.data.format == FormatQuestion.group) {
      response.questions.forEach((qs) => {
        let answers: AnswerRequestOrResponse[] = [];
        let question: QuestionRequestOrResponse;
        let oldQuestion = this.data.questions.find(x => x.id == qs.id);
        if (oldQuestion) {
          question = { ...oldQuestion };
        }
        // build answers of child question
        qs.answers.forEach((answer) => {
          answers.push({
            id: answer.id,
            questionId: answer.questionId,
            content: answer.content,
            trueAnswer: answer.trueAnswer,
            position: answer.position,
            order: answer.order,
            groupOfFillBlank2: answer.groupOfFillBlank2,
            isShuffler: false,
            action: answer.action,
          });
        });

        question.content = qs.content;
        question.questionGroupId = qs.questionGroupId;
        question.level = qs.level;
        question.action = qs.action;
        question.answers = answers;
        questionReturn.questions.push(question);

        // // push child question
        // questionReturn.questions.push({
        //   id: qs.id,
        //   content: qs.content,
        //   type: qs.type,
        //   questionGroupId: qs.questionGroupId,
        //   level: qs.level,
        //   format: qs.format,
        //   action: qs.action,
        //   answers: answers,
        //   questions: []
        // });
      });
    }

    this.outputData.emit(questionReturn);
  }

  ngOnChanges(): void {
    if (this.data != null) {
      console.log(`this.data a9mmmmmm`, this.data);
      this.dataQuestionView = QuestionViewData.FromQuestionResponse(this.data);
      this.childIndexView = this.childIndex;
      console.log('this.childIndex:  ',this.childIndex)
    }
    console.log("Input question: ----->", this.data);
  }

  ngOnInit(): void {
  }

  onLoadedQuestion(event) {
    console.log(`onLoadedQuestion`, event);
  }
}

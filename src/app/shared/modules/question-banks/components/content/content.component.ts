import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { AnswerRequestOrResponse, QuestionViewData } from './../../Interfaces/question-data';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { EditQuestionEvent } from '../../Interfaces/create-question-event';
import { QuestionRequestOrResponse } from '../../Interfaces/question-data';
import { ConfigUploadCkeditor } from '../../Interfaces/config-upload-ckeditor';
// import { LoadContentService } from '../../Services/load-content.service';
// declare var CKEDITOR: any;

@Component({
  selector: 'all-questions',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit, EditQuestionEvent {

  @Input() data: QuestionRequestOrResponse;
  @Input() childIndex: number;
  @Input() configUpload: ConfigUploadCkeditor;
  @Output() outputData = new EventEmitter();

  dataQuestionView: QuestionViewData;
  childIndexView: number;
  isReady: boolean;

  @ViewChild("ck") ck: CKEditorComponent;

  constructor() {

  }

  ngAfterViewInit(): void {
    this.isReady = true;
  }

  updateQuestion(response: QuestionViewData): void {
    debugger
    let questionReturn: QuestionRequestOrResponse = { ...this.data };
    questionReturn.id = this.data.id;
    questionReturn.content = response.content;
    questionReturn.type = response.type;
    questionReturn.questionGroupId = response.questionGroupId;
    questionReturn.level = response.level;
    questionReturn.format = response.format;
    questionReturn.answers = [];
    questionReturn.questions = [];
    if (this.data.format == FormatQuestion.single) {
      response.answers.forEach((answer) => {
        questionReturn.answers.push({
          id: answer.id,
          questionId: answer.questionId,
          content: answer.content,
          trueAnswer: answer.trueAnswer,
          order: answer.order,
          position: answer.position,
          groupOfFillBlank2: answer.groupOfFillBlank2,
          isShuffler: false
        });
      });
    }
    else if (this.data.format == FormatQuestion.group) {
      response.questions.forEach((qs) => {
        let answers: AnswerRequestOrResponse[] = [];

        // build answers of child question
        qs.answers.forEach((answer) => {
          answers.push({
            id: answer.id,
            questionId: answer.questionId,
            content: answer.content,
            trueAnswer: answer.trueAnswer,
            position: answer.position,
            order: answer.order,
            groupOfFillBlank2: answer.groupOfFillBlank2,
            isShuffler: false,
            action: answer.action,
          });
        });

        // // push child question
        questionReturn.questions.push({
          id: qs.id,
          content: qs.content,
          type: qs.type,
          questionGroupId: qs.questionGroupId,
          level: qs.level,
          format: qs.format,
          action: qs.action,
          answers: answers,
          questions: []
        });
      });
    }
    this.outputData.emit(questionReturn);
  }

  ngOnChanges(): void {
    if (this.data != null) {
      console.log(`this.data a9mmmmmm`, this.data);
      this.dataQuestionView = QuestionViewData.FromQuestionResponse(this.data);
      this.childIndexView = this.childIndex;
    }
  }

  ngOnInit(): void {
  }

  onLoadedQuestion(event) {
    console.log(`onLoadedQuestion`, event);
  }
}

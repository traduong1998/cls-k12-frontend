import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTrueFalseQuestionComponent } from './create-true-false-question.component';

describe('CreateTrueFalseQuestionComponent', () => {
  let component: CreateTrueFalseQuestionComponent;
  let fixture: ComponentFixture<CreateTrueFalseQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateTrueFalseQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTrueFalseQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

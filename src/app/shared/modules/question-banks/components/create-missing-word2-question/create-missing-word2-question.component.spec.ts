import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMissingWord2QuestionComponent } from './create-missing-word2-question.component';

describe('CreateMissingWord2QuestionComponent', () => {
  let component: CreateMissingWord2QuestionComponent;
  let fixture: ComponentFixture<CreateMissingWord2QuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateMissingWord2QuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMissingWord2QuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

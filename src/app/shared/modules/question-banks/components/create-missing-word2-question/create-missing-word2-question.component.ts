import { AnswerViewData } from './../../Interfaces/question-data';
import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { FillBlank2Answer } from 'sdk/cls-k12-sdk-js/src/services/question/models/Answer';
import { Question } from 'sdk/cls-k12-sdk-js/src/services/question/models/Question';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { getSelectionHTML } from 'src/environments/ckeditorFuntion';
import { CreateQuestionEvent } from '../../Interfaces/create-question-event';
import { QuestionViewData } from '../../Interfaces/question-data';
import { LoadContentService } from '../../Services/load-content.service';
import { FormControl } from '@angular/forms';
import { QuestionTypeOption } from '../../Interfaces/question-type-option';
import { QuestionLevelOption } from '../../Interfaces/question-level-option';
import { ConfigUploadCkeditor } from '../../Interfaces/config-upload-ckeditor';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-create-missing-word2-question',
  templateUrl: './create-missing-word2-question.component.html',
  styleUrls: ['./create-missing-word2-question.component.scss']
})
export class CreateMissingWord2QuestionComponent implements OnInit, CreateQuestionEvent {
  @Input() dataQuestion: QuestionViewData;
  @Input() configUpload: ConfigUploadCkeditor;
  @ViewChild("ck") ck: CKEditorComponent;
  @Output() questionOutput = new EventEmitter();
  @ViewChildren("cks") cks: QueryList<CKEditorComponent>;
  currentBlankID = 1;
  @Input() hideAction = false;
  configQuestion = {};
  @Input() config: number;
  flexLayout;
  phuongAnGayNhieuID = 1;
  CauHoiID = "$CauHoiID$";
  answers: AnswerViewData[] = [];
  isDelete = false;
  //get type,level
  type = 'fillblank2';
  level = 'NB';
  isAutoApproval = false;
  disabledUpdate: boolean = false;
  // danh sách waiting timeout object của answer
  // dùng để delay cập nhật phương án khi người dùng gõ thay đổi phương án
  answerWTOs = [];
  configAnswer = {
    // startupFocus: true,
    height: 60,
    allowedContent: true,

    extraPlugins: ckConfig.extraPlugins,
    toolbarGroups: ckConfig.toolbarAnswer,
    removeButtons: ckConfig.removeButtonsAnswer,
  };
  ckeditorContent = "";
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  questionTypeCtrl = new FormControl();
  questionLevelCtrl = new FormControl();

  levelQuestionEnum: QuestionTypeOption[] = [
    { id: Level.NB, name: "Nhận biết" },
    { id: Level.TH, name: "Thông hiểu" },
    { id: Level.VD, name: "Vận dụng" },
    { id: Level.VDC, name: "Vận dụng cao" }];

  typeQuestionEnum: QuestionLevelOption[] = [
    { id: Type.singlechoice, name: "Một lựa chọn" },
    { id: Type.multichoice, name: "Nhiều lựa chọn" },
    { id: Type.underline, name: "Gạch chân" },
    { id: Type.fillblank, name: "Điền khuyết" },
    { id: Type.fillblank2, name: "Điền khuyết loại 2" },
    { id: Type.matching, name: "Ghép đôi" },
    { id: Type.truefasle, name: "Đúng sai" },
    { id: Type.truefaslechause, name: "Mệnh đề đúng sai" },
    { id: Type.essay, name: "Tự luận" }];
  constructor(private loadData: LoadContentService,private _messageService:MessageService) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      allowedContent: true,

      extraPlugins: ['fillblank2', 'ckeditor_wiris'],
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtonsFillBlank,
    };

    // 
    let a = this.configUpload;
  }

  ngOnInit(): void {

  }

  ngOnChanges(): void {
    clearTimeout(this.wto);
    this.answerWTOs.map(x => {
      clearTimeout(x.wto);
    })
    this.disabledUpdate = false;
    if (this.dataQuestion != null) {
      this.ckeditorContent = this.dataQuestion.content;
      this.answers = this.dataQuestion.answers;
      this.questionTypeCtrl.setValue(this.dataQuestion.type);
      this.questionLevelCtrl.setValue(this.dataQuestion.level);
    }
  }

  ngAfterViewInit(): void {
    this.initQuestionContent(this.ck);

    // ẩn toolbar ckedtior
    this.cks.changes.subscribe((x) => {
      for (let index = 0; index < x._results.length; index++) {
        this.initAnswer(x._results[index]);
      }
    });

    this.cks.forEach((x) => {
      this.initAnswer(x);
    });
  }

  updateQuestion(notifi?: boolean): void {
    if (this.getvalue() != null) {
      this.questionOutput.emit(this.dataQuestion);
      if (notifi) {
        this._messageService.susccessCallback("Đã áp dụng");
      }
    }
  }

  getvalue() {
    let ckeEditableArea = $(this.ck.instance.editable().$);
    if (isNullOrWhiteSpace(this.dataQuestion.content) || isNullOrEmpty(this.dataQuestion.content)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
    }
    else if (this.groupAnswers().length == 0) {
      alert("Bạn cần tạo ít nhật 1 phương án trả lời");
      return null;
    }
    else if (this.answers.some(x => (isNullOrEmpty(x.content)))) {
      alert("Bạn chưa nhập nội dung đáp án nhiễu");
    }
    else if (this.groupAnswers().length != ckeEditableArea.find('span[class^="Blanking2"]').length) {
      alert("Số vị trí điền khuyết phải bằng số lượng phương án đúng");
    }
    else {
      //this.question = { id: 3, content: this.ckeditorContent, type:Type.fillblank2, level: Level.NB, answers: this.answers,formatQuestion:FormatQuestion.single,questionGroupId:null, creatorId:null}
      //console.log(this.dataQuestion);
      this.dataQuestion.level = this.questionLevelCtrl.value;
      this.dataQuestion.answers = this.answers;
      return this.dataQuestion;
    }
  }

  addAnswer(answer, index) {
    if (this.groupAnswers().length < 9) {
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999), trueAnswer: 1, content: answer, questionId: this.dataQuestion.id, groupOfFillBlank2: index, isShuffler: false });
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999), trueAnswer: 0, content: "", questionId: this.dataQuestion.id, groupOfFillBlank2: index, isShuffler: false });
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999), trueAnswer: 0, content: "", questionId: this.dataQuestion.id, groupOfFillBlank2: index, isShuffler: false });
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999), trueAnswer: 0, content: "", questionId: this.dataQuestion.id, groupOfFillBlank2: index, isShuffler: false });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }

  addFakeAnswer(index) {
    if (this.answersOfGroup(index).length < 9) {
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999), trueAnswer: 0, content: "", questionId: this.dataQuestion.id, groupOfFillBlank2: index, isShuffler: false });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }

  changeSelectAnswer(i) {
    this.answers.map(ans => ans.groupOfFillBlank2 = 0);
    this.answers[i].groupOfFillBlank2 = 1;
  }

  handleAfterCommandExec(event, editor) {
    var commandName = event.data.name;
    // Kiểm tra sự kiện sau khi click tag U
    // For 'underline' commmand
    if (commandName == "taophuongan") {
      debugger;
      var mySelection = this.ck.instance.getSelection();
      let selectedText = mySelection.getNative();
      let editor = this.ck.instance;
      let realSelectedText = getSelectionHTML(selectedText).toString();
      // if (mySelection.getType() == 2) {

      //   if (/<img[^>]*>/g.test(getSelectionHTML(selectedText)) || /<video[^>]*>/g.test(getSelectionHTML(selectedText))) {
      //     alert('Phương án chỉ chứa văn bản');
      //     return;
      //   }

      //   let realSelectedText = getSelectionHTML(selectedText).toString();
      //   if (isNullOrWhiteSpace(selectedText.toString().trim()) || isNullOrEmpty(selectedText.toString().trim())) {
      //     alert('Vui lòng chọn văn bản');
      //     return;
      //   }
      if (getSelectionHTML(selectedText).indexOf('class="Blanking2') != -1 ||
        ($(editor.getSelectedHtml().$).is('span') &&
          $(editor.getSelectedHtml().getHtml()).hasClass('Blanking2'))) {
        alert('Vùng chọn đã có phương án');
        return;
      }
      // Nếu khoảng trắng thì không cho tạo
      else if (isNullOrEmpty(realSelectedText) || isNullOrWhiteSpace(realSelectedText)) {
        alert('Vùng chọn phải có ký tự, không được hoàn toàn là khoảng trắng.');
        return;
      }
      //insert blank, thay bằng index của phần điền khuyết vào editor
      let insertedBlank = editor.document.createElement('span');
      insertedBlank.setAttribute('data-current-answer', '');
      insertedBlank.setAttribute('class', 'Blanking2 Blanking2-' + this.CauHoiID); //CauHoiID được truyền vào từ View MissingWordAdd(Edit)
      insertedBlank.setAttribute('data-position', this.currentBlankID); //currentBlankID được truyền vào từ View MissingWordAdd(Edit)
      //insertedBlank.setAttribute('style', 'font-weight: bold; border-bottom: 1px solid #c0c0c0; min-width: 50px; display:inline-block; text-align: center;');
      //ngăn không cho người dùng chỉnh sửa phần điền khuyết
      insertedBlank.setAttribute('contenteditable', 'false');
      insertedBlank.appendText('(' + this.currentBlankID.toString() + ')');
      editor.insertElement(insertedBlank);

      this.reorderAnswerIndexes(this.currentBlankID, editor, realSelectedText);

      this.currentBlankID++;

      // } else {
      //   alert('vui lòng chỉ chọn văn bản');
      //   return;
      // }
    }
  }

  reorderAnswerIndexes(currentMaxID, editor, realSelectedText) {

    let ckeEditableArea = $(editor.editable().$);
    let blankIndexElements = ckeEditableArea.find('span[class^="Blanking2"]');
    let insertedPosition = 0;
    blankIndexElements.each(function (index, element) {
      if ($(element).attr('data-position') == currentMaxID) {
        insertedPosition = index + 1;
      }
    });

    $(ckeEditableArea.find('span[class^="Blanking2"]:gt(' + (insertedPosition - 1) + ')').get().reverse()).each(function (index, element) {
      let oldIndex = parseInt($(element).attr('data-position'));
      $(element).attr('data-position', oldIndex + 1);
      $(element).text('(' + (oldIndex + 1) + ')');
      // let dongPhuongAnDungHienTai = $('.phuong-an-dung > table > tbody > tr[data-position="' + oldIndex + '"]');
      // dongPhuongAnDungHienTai.attr('data-position', oldIndex + 1);
      // dongPhuongAnDungHienTai.find('*[blank-index]').text((oldIndex + 1));
    });

    ckeEditableArea.find('span[class^="Blanking2"]').each(function (index, element) {
      if ((index + 1) == insertedPosition) {
        $(element).attr('data-position', insertedPosition);
        $(element).text('(' + insertedPosition + ')');
      }
    });
    if (this.answers.length >= 1) {
      this.changeSTT(insertedPosition);
    }
    this.addAnswer(realSelectedText, insertedPosition);
  }

  deleteAnswer(answer) {
    // 
    //let answer = this.answers.find(x => x.id == id);
    if (answer.trueAnswer == 1) {
      // Nếu xóa phương án đúng, thì xóa luôn cả nhóm phương án nhiễu của phương án đó
      this.answers = this.answers.filter(x => x.groupOfFillBlank2 != answer.groupOfFillBlank2);

      let ckeEditableArea = $(this.ck.instance.editable().$);

      let htmlData = ckeEditableArea.find('span[class^="Blanking2"]');
      htmlData.each(function (index, element) {
        let currentDataBlankIndex = parseInt($(element).attr('data-position'));
        if (currentDataBlankIndex > answer.groupOfFillBlank2) {
          $(element).html('(' + (currentDataBlankIndex - 1) + ')');
          //cập nhật lại index ở div phuong-an-dung
          let rowPhuongAnDung = $('.phuong-an-dung > table > tbody > tr[data-position="' + currentDataBlankIndex + '"]');
          $(rowPhuongAnDung).find('*[blank-index]').text(currentDataBlankIndex - 1);
          $(rowPhuongAnDung).attr('data-position', currentDataBlankIndex - 1);
          $(element).attr('data-position', currentDataBlankIndex - 1);
        } else if (currentDataBlankIndex == answer.groupOfFillBlank2) {
          $(element).replaceWith(answer.content);
        }
      });

      this.dataQuestion.content = this.ck.instance.getData();

      this.currentBlankID -= 1;
      this.answers.map((ans, i) => {
        if (ans.groupOfFillBlank2 > answer.groupOfFillBlank2) {
          ans.groupOfFillBlank2--;
        }
      });
    }
    else {
      let index = this.answers.indexOf(answer, 0);

      if (this.answersOfGroup(answer.groupOfFillBlank2).length <= 2) {
        alert("Phải có ít nhất 1 phương án nhiễu")
      }
      else {
        this.answers.splice(index, 1);
      }
    }
  }

  changeSTT(index) {
    this.answers.map((ans, i) => {
      if (ans.groupOfFillBlank2 >= index) {
        this.answers[i].groupOfFillBlank2 += 1;
      }
    });
  }

  sortedAnswers() {
    return this.answers.sort((a, b) => a.groupOfFillBlank2 > b.groupOfFillBlank2 ? 1 : a.groupOfFillBlank2 < b.groupOfFillBlank2 ? -1 : 0);
  }

  groupAnswers() {
    return [...new Set(this.sortedAnswers().map(item => item.groupOfFillBlank2))]
  }

  answersOfGroup(index) {
    return this.answers.filter(x => x.groupOfFillBlank2 == index);
  }

  private initQuestionContent(ck: CKEditorComponent) {
    // add command tạo phương án
    ck.ready.subscribe(() => {
      ck.instance.on('afterCommandExec', (event) => {
        this.handleAfterCommandExec(event, ck.instance)
      });
      ck.instance.addCommand('taophuongan', {
        exec: function (editor) {
        }
      });
    });
  }

  private initAnswer(ck: CKEditorComponent) {
    if (ck) {
      ck.ready.subscribe((ev) => {
        document.getElementById(ck.instance.id + '_top').style.display = 'none';
        this.answerWTOs.push({ id: ck.instance.id, wto: null });
      });
      ck.focus.subscribe((ev) => {
        document.getElementById(ck.instance.id + '_top').style.display = 'block';
      });
      ck.blur.subscribe((ev) => {
        document.getElementById(ck.instance.id + '_top').style.display = 'none';
      });
    }
  }

  wto;
  onQuestionContentChange(e) {
    let content = e.editor.getData();
    clearTimeout(this.wto);
    this.disabledUpdate = true;
    this.wto = setTimeout(() => {
      // do stuff when user has been idle for 1 second
      if (this.dataQuestion.content != content.trim()) {
        this.dataQuestion.content = content.trim();
      }
      this.disabledUpdate = false;
    }, 300);
  }

  onAnswerContentChange(e, answer) {
    // 
    //two way binding content, delay 1s rồi mới cập nhật action
    let wtoIndex = this.answerWTOs.findIndex(x => x.id == e.editor.id);
    let content = e.editor.getData();
    clearTimeout(this.answerWTOs[wtoIndex].wto);
    this.answerWTOs[wtoIndex].wto = null;
    this.disabledUpdate = true;
    this.answerWTOs[wtoIndex].wto = setTimeout(() => {
      let i = this.answers.indexOf(answer);
      if (this.answers[i].content != content) {
        this.answers[i].content = content;
      }
      this.disabledUpdate = false;

      console.log(`this.dataQuestion.999`, this.dataQuestion);
      console.log(`this.answers.999`, this.answers);
    }, 300);
  }
}

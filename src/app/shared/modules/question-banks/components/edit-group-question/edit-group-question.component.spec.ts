import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGroupQuestionComponent } from './edit-group-question.component';

describe('EditGroupQuestionComponent', () => {
  let component: EditGroupQuestionComponent;
  let fixture: ComponentFixture<EditGroupQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditGroupQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGroupQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

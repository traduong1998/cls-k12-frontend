import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { delay } from 'rxjs/operators';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { QuestionViewData } from '../../Interfaces/question-data';
import { CreateMissingWord1QuestionComponent } from '../create-missing-word1-question/create-missing-word1-question.component';
import { CreateMultipleChoiceQuestionComponent } from '../create-multiple-choice-question/create-multiple-choice-question.component';
import { CreateSingleChoiceQuestionComponent } from '../create-single-choice-question/create-single-choice-question.component';
import { CreateTrueFalseClauseQuestionComponent } from '../create-true-false-clause-question/create-true-false-clause-question.component';
import { CreateTrueFalseQuestionComponent } from '../create-true-false-question/create-true-false-question.component';
import { CreateUnderLineQuestionComponent } from '../create-under-line-question/create-under-line-question.component';
import { Action } from '../../Interfaces/action';
import { FormControl } from '@angular/forms';
import { QuestionTypeOption } from '../../Interfaces/question-type-option';
import { QuestionLevelOption } from '../../Interfaces/question-level-option';
import { ConfigUploadCkeditor } from '../../Interfaces/config-upload-ckeditor';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-edit-group-question',
  templateUrl: './edit-group-question.component.html',
  styleUrls: ['./edit-group-question.component.scss']
})
export class EditGroupQuestionComponent implements OnInit {
  @Input() dataQuestion: QuestionViewData;
  @Input() childIndex: number;
  @Input() configUpload: ConfigUploadCkeditor;
  @Output() questionOutput = new EventEmitter();
  subQuestion: QuestionViewData[];
  @Input() hideAction = false;
  ckeditorContent = "";
  configQuestion;
  isAutoApproval;
  isShuffleChild = true;
  disabledUpdate: boolean = false;
  id: number;
  name: string;
  questionGroup: QuestionViewData;
  @ViewChildren('singleChoice') singleChoices: QueryList<CreateSingleChoiceQuestionComponent>
  @ViewChildren('multichoice') multiChoice: QueryList<CreateMultipleChoiceQuestionComponent>
  @ViewChildren('underline') underline: QueryList<CreateUnderLineQuestionComponent>
  @ViewChildren('truefalse') truefalse: QueryList<CreateTrueFalseQuestionComponent>
  @ViewChildren('truefalseclause') truefalseclause: QueryList<CreateTrueFalseClauseQuestionComponent>
  @ViewChildren('missingword') missingword: QueryList<CreateMissingWord1QuestionComponent>

  @ViewChild("ck") ck: CKEditorComponent;
  selectedQuestion = '1';
  questionChildTypeCtrl = new FormControl();
  questionChildLevelCtrl = new FormControl();

  levelQuestionEnum: QuestionTypeOption[] = [
    { id: Level.NB, name: "Nhận biết" },
    { id: Level.TH, name: "Thông hiểu" },
    { id: Level.VD, name: "Vận dụng" },
    { id: Level.VDC, name: "Vận dụng cao" }];

  typeQuestionEnum: QuestionLevelOption[] = [
    { id: Type.singlechoice, name: "Trắc nghiệm một lựa chọn" },
    { id: Type.multichoice, name: "Trắc nghiệm nhiều lựa chọn" },
    { id: Type.underline, name: "Gạch chân" },
    { id: Type.fillblank, name: "Điền khuyết" },
    // { id: Type.fillblank2, name: "Điền khuyết loại 2" },
    // { id: Type.matching, name: "Ghép đôi" },
    { id: Type.truefasle, name: "Lựa chọn đúng sai" },
    { id: Type.truefaslechause, name: "Mệnh đề đúng sai" },
    // { id: Type.essay, name: "Tự luận" }
  ];
  constructor(private _messageService: MessageService) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      allowedContent: true,
      extraPlugins: ckConfig.extraPlugins,
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
      removePlugins: 'elementspath,magicline'
    };
  }
  updateQuestion(notifi?: boolean): void {
    // console.log(`this.dataQuestion999: `, this.dataQuestion);
    if (this.show() != null) {
      this.questionOutput.emit(this.dataQuestion);
      if (notifi) {
        this._messageService.susccessCallback("Đã áp dụng");
      }
    }
  }
  ngOnInit(): void {
  }

  ngOnChanges(): void {

    if (this.dataQuestion != null) {
      this.ckeditorContent = this.dataQuestion.content;
      this.subQuestion = this.dataQuestion.questions;

      // console.log(`subQuestion`, this.dataQuestion);
      if (this.childIndex) {
        this.selectedQuestion = this.childIndex + '';
      }
      let subSelectedIndex = parseInt(this.selectedQuestion);
      this.questionChildTypeCtrl.setValue(this.subQuestion[subSelectedIndex - 1].type);
      this.questionChildLevelCtrl.setValue(this.subQuestion[subSelectedIndex - 1].level);
    }
  }

  ngAfterViewInit(): void {
    this.initAnswer(this.ck);
  }

  addSubQuestion() {
    this.subQuestion.push({ content: "", type: Type.singlechoice, format: FormatQuestion.single, level: Level.NB, answers: [] })
  }

  removeSubQuestion(index) {

    if (parseInt(index) <= 1) {
      alert("Bạn phải có tối thiếu 1 câu hỏi");
    }
    else {
      if (index == this.subQuestion.length) {
        this.selectedQuestion = (index - 1).toString();
      }
      this.subQuestion.splice(parseInt(index) - 1, 1);
    }
  }

  changeShuffleChild(event) {
    this.isShuffleChild = event.checked;
  }

  onQuestionLevelSelectChange(value, index) {
    this.subQuestion.map((qs, i) => {
      if (i == index) {
        qs.level = value;
      }
      return qs;
    });
  }

  show() {
    if (isNullOrWhiteSpace(this.ckeditorContent) || isNullOrEmpty(this.ckeditorContent)) {
      alert("Bạn chưa nhập nội dung câu hỏi chung");
    }
    else {
      let err = 0;
      let countSingle = 0;
      let countMulti = 0;
      let countUnderline = 0;
      let countTruefalse = 0;
      let countTrueFalseClause = 0;
      let countMissingWord = 0
      for (let i in this.subQuestion) {
        delay(1000);
        if (this.singleChoices.toArray().length > 0 && this.subQuestion[i].type == Type.singlechoice) {
          let questionOfGroup = this.singleChoices.toArray()[countSingle].setQuestion();
          if (Number(questionOfGroup.content) == 1) {
            err++;
            if (!this.hideAction) {
              alert("Bạn nhập thiếu thông tin ở câu " + (parseInt(i) + 1));
            }
            return null;
          }
          else {
            this.subQuestion[i].content = questionOfGroup.content;
            this.subQuestion[i].answers = questionOfGroup.answers;
          }
          countSingle++;
        }
        if (this.multiChoice.toArray().length > 0 && this.subQuestion[i].type == Type.multichoice) {
          let questionOfGroup = this.multiChoice.toArray()[countMulti].setQuestion();
          if (Number(questionOfGroup.content) == 1) {
            err++;
            if (!this.hideAction) {
              alert("Bạn nhập thiếu thông tin ở câu " + (parseInt(i) + 1));
            }
            return null;
          }
          else {
            this.subQuestion[i].content = questionOfGroup.content;
            this.subQuestion[i].answers = questionOfGroup.answers;
          }
          countMulti++;
        }
        if (this.underline.toArray().length > 0 && this.subQuestion[i].type == Type.underline) {
          let questionOfGroup = this.underline.toArray()[countUnderline].setQuestion();
          if (Number(questionOfGroup.content) == 1) {
            err++;
            if (!this.hideAction) {
              alert("Bạn nhập thiếu thông tin ở câu " + (parseInt(i) + 1));
            }
            return null;
          }
          if (questionOfGroup.answers.length <= 0 && this.hideAction) {
            return null;
          }
          else {
            this.subQuestion[i].content = questionOfGroup.content;
            this.subQuestion[i].answers = questionOfGroup.answers;
          }
          countUnderline++;
        }
        if (this.truefalse.toArray().length > 0 && this.subQuestion[i].type == Type.truefasle) {
          let questionOfGroup = this.truefalse.toArray()[countTruefalse].setQuestion();
          if (Number(questionOfGroup.content) == 1) {
            err++;
            if (!this.hideAction) {
              alert("Bạn nhập thiếu thông tin ở câu " + (parseInt(i) + 1));
            }
            return null;
          }
          else {
            this.subQuestion[i].content = questionOfGroup.content;
            this.subQuestion[i].answers = questionOfGroup.answers;
          }
          countTruefalse++;
        }
        if (this.truefalseclause.toArray().length > 0 && this.subQuestion[i].type == Type.truefaslechause) {
          let questionOfGroup = this.truefalseclause.toArray()[countTrueFalseClause].setQuestion();
          if (Number(questionOfGroup.content) == 1) {
            err++;
            if (!this.hideAction) {
              alert("Bạn nhập thiếu thông tin ở câu " + (parseInt(i) + 1));
            }
            return null;
          }
          else {
            this.subQuestion[i].content = questionOfGroup.content;
            this.subQuestion[i].answers = questionOfGroup.answers;
          }
          countTrueFalseClause++;
        }
        if (this.missingword.toArray().length > 0 && this.subQuestion[i].type == Type.fillblank) {
          let questionOfGroup = this.missingword.toArray()[countMissingWord].setQuestion();
          if (Number(questionOfGroup) == 1) {
            err++;
            if (!this.hideAction) {
              alert("Bạn nhập thiếu thông tin ở câu " + (parseInt(i) + 1));
            }
            return null;
          }
          if (questionOfGroup.answers.length <= 0 && this.hideAction) {
            return null;
          }
          else {
            this.subQuestion[i].content = questionOfGroup.content;
            this.subQuestion[i].answers = questionOfGroup.answers;
          }
          countMissingWord++;
        }
      }
      if (err == 0) {
        let subSelectedIndex = parseInt(this.selectedQuestion);
        this.subQuestion.map((sq, i) => {
          if ((i + 1) == subSelectedIndex) {
            sq.level = this.questionChildLevelCtrl.value;
          }

          return sq;
        });
        this.dataQuestion.questions = this.subQuestion;
        return this.dataQuestion;
      }
      else {
        alert("Lưu thất bại")
      }
    }
  }

  getvalue() {

    return this.dataQuestion;
  }

  onChangeChildQuestion(value) {
    let subSelectedIndex = parseInt(this.selectedQuestion);
    this.questionChildTypeCtrl.setValue(this.subQuestion[subSelectedIndex - 1].type);
    this.questionChildLevelCtrl.setValue(this.subQuestion[subSelectedIndex - 1].level);
  }

  private initAnswer(ck: CKEditorComponent) {
    // if (ck) {
    //   ck.focus.subscribe((ev) => {
    //     document.getElementById(ck.instance.id + '_top').style.display = 'block';
    //   });
    //   ck.blur.subscribe((ev) => {
    //     document.getElementById(ck.instance.id + '_top').style.display = 'none';
    //   });
    // }
  }

  onchangeLevel(value: string): void {
    const subLevelSelected = parseInt(this.selectedQuestion);
    this.subQuestion[subLevelSelected - 1].level = value;
    this.subQuestion[subLevelSelected - 1].action = 'EDI'
  }

  wto;
  onQuestionContentChange(e) {
    let content = e.editor.getData();
    clearTimeout(this.wto);
    this.disabledUpdate = true;
    this.wto = setTimeout(() => {
      // do stuff when user has been idle for 1 second
      if (this.dataQuestion.content != content.trim()) {
        if (this.dataQuestion.action == Action.EditScore) {
          this.dataQuestion.action = Action.EditAll;
        } if (this.dataQuestion.action != Action.EditAll) {
          this.dataQuestion.action = Action.EditContent
        }
        this.dataQuestion.content = content.trim();
      }
      this.disabledUpdate = false;
    }, 300);
  }

}

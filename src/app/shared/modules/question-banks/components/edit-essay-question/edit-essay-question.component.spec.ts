import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEssayQuestionComponent } from './edit-essay-question.component';

describe('EditEssayQuestionComponent', () => {
  let component: EditEssayQuestionComponent;
  let fixture: ComponentFixture<EditEssayQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditEssayQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEssayQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMatchingQuestionComponent } from './edit-matching-question.component';

describe('EditMatchingQuestionComponent', () => {
  let component: EditMatchingQuestionComponent;
  let fixture: ComponentFixture<EditMatchingQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditMatchingQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMatchingQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

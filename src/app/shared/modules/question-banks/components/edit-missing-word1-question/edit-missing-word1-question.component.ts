import { AnswerViewData } from './../../Interfaces/question-data';
import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import * as $ from 'jquery';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { getSelectionHTML } from 'src/environments/ckeditorFuntion';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { CKEditorComponent } from 'ckeditor4-angular';
import { generateRandomUniqueInteger } from 'src/app/shared/helpers/cls.helper';
import { CreateQuestionEvent } from '../../Interfaces/create-question-event';
import { QuestionViewData } from '../../Interfaces/question-data';
import { Action } from '../../Interfaces/action';
import { QuestionTypeOption } from '../../Interfaces/question-type-option';
import { FormControl } from '@angular/forms';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { QuestionLevelOption } from '../../Interfaces/question-level-option';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { ConfigUploadCkeditor } from '../../Interfaces/config-upload-ckeditor';
import { MessageService } from 'src/app/shared/services/message.service';
declare var CKEDITOR: any;

@Component({
  selector: 'app-edit-missing-word1-question',
  templateUrl: './edit-missing-word1-question.component.html',
  styleUrls: ['./edit-missing-word1-question.component.scss']
})
export class EditMissingWord1QuestionComponent implements OnInit {
  configQuestion = {};
  @Input() isGroup: boolean;
  @Output() questionOutput = new EventEmitter();
  @Input() dataQuestion: QuestionViewData;
  @Input() configUpload: ConfigUploadCkeditor;
  @Input() hideAction = false;
  currentBlankID = 1;
  phuongAnGayNhieuID = 1;
  CauHoiID = "$CauHoiID$";
  answers: AnswerViewData[] = [];
  disabledUpdate: boolean = false;
  @ViewChild("ck") ck: CKEditorComponent;
  @ViewChildren("cks") cks: QueryList<CKEditorComponent>;
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  ckeditorContent = "";
  // danh sách waiting timeout object của answer
  // dùng để delay cập nhật phương án khi người dùng gõ thay đổi phương án
  answerWTOs = [];
  configAnswer = {
    // startupFocus: true,
    height: 60,
    allowedContent: true,

    extraPlugins: ckConfig.extraPlugins,
    toolbarGroups: ckConfig.toolbarAnswer,
    removeButtons: ckConfig.removeButtonsAnswer,
    removePlugins: 'elementspath,magicline'
  };
  questionTypeCtrl = new FormControl();
  questionLevelCtrl = new FormControl();

  levelQuestionEnum: QuestionTypeOption[] = [
    { id: Level.NB, name: "Nhận biết" },
    { id: Level.TH, name: "Thông hiểu" },
    { id: Level.VD, name: "Vận dụng" },
    { id: Level.VDC, name: "Vận dụng cao" }];

  typeQuestionEnum: QuestionLevelOption[] = [
    { id: Type.singlechoice, name: "Một lựa chọn" },
    { id: Type.multichoice, name: "Nhiều lựa chọn" },
    { id: Type.underline, name: "Gạch chân" },
    { id: Type.fillblank, name: "Điền khuyết" },
    { id: Type.fillblank2, name: "Điền khuyết loại 2" },
    { id: Type.matching, name: "Ghép đôi" },
    { id: Type.truefasle, name: "Đúng sai" },
    { id: Type.truefaslechause, name: "Mệnh đề đúng sai" },
    { id: Type.essay, name: "Tự luận" }];
  constructor(private _messageService:MessageService) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      allowedContent: true,
      extraPlugins: ['fillblank1', 'ckeditor_wiris'],
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtonsFillBlank,
      removePlugins: 'elementspath,magicline'
    };
  }

  ngOnChanges(): void {
    clearTimeout(this.wto);
    this.answerWTOs.map(x => {
      clearTimeout(x.wto);
    })
    this.disabledUpdate = false;
    // console.log(`this.dataQuestion9999:`, this.dataQuestion);
    if (this.dataQuestion != null) {
      this.ckeditorContent = this.dataQuestion.content;
      this.answers = this.dataQuestion.answers;
      this.questionTypeCtrl.setValue(this.dataQuestion.type);
      this.questionLevelCtrl.setValue(this.dataQuestion.level);
    }
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {

    this.initQuestionContent(this.ck);
    this.dataQuestion.answers = [];
    this.ck.ready.subscribe(() => {
      this.ck.instance.on('afterCommandExec', (event) => {
        this.handleAfterCommandExec(event, this.ck.instance)
      });
      this.ck.instance.addCommand('taophuongan', {
        exec: function (editor) {
        }
      });
    });

    // ẩn toolbar ckedtior
    this.cks.changes.subscribe((x) => {
      this.initAnswer(x.last);
    });

    this.cks.forEach((x) => {
      this.initAnswer(x);
    });
  }

  updateQuestion(notifi?: boolean): void {
    console.log(this.updateQuestion);
    if (this.getvalue() != null) {
      this.questionOutput.emit(this.dataQuestion);
      if (notifi) {
        this._messageService.susccessCallback("Đã áp dụng");
      }
    }
  }

  setAnswers() {
    return this.answers;
  }

  setQuestion() {
    let err = 0;
    if (isNullOrWhiteSpace(this.dataQuestion.content) || isNullOrEmpty(this.dataQuestion.content)) {
      err++;
      alert("Bạn chưa nhập nội dung câu hỏi của câu điền khuyết");
      return { content: err.toString(), answers: [] };
    }
    else if (this.answers.filter(x => x.action != Action.Delete).some(x => x.trueAnswer == 1) == false) {
      err++;
      alert("Bạn chưa đưa ra đáp án đúng");
      return { content: err.toString(), answers: [] };
    } else if (this.answers.filter(x => x.action != Action.Delete).some(x => (isNullOrEmpty(x.content)))) {
      err++;
      alert("Bạn chưa nhập nội dung phương án nhiễu câu điền khuyết");
      return { content: err.toString(), answers: [] };
    }
    else {
      this.dataQuestion.answers = this.answers;
      return this.dataQuestion;
    }
  }

  getvalue() {
    console.log('logggggg', this.dataQuestion);
    if (isNullOrWhiteSpace(this.dataQuestion.content) || isNullOrEmpty(this.dataQuestion.content)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
      return null;
    }
    else if (this.answers.filter(x => x.action != Action.Delete).some(x => x.trueAnswer == 1) == false) {
      alert("Bạn chưa đưa ra đáp án đúng");
      return null;
    }
    else if (this.answers.filter(x => x.action != Action.Delete).some(x => (isNullOrEmpty(x.content)))) {
      alert("Bạn chưa nhập nội dung đáp án nhiễu");
      return null;
    }
    else {
      //this.question = { id: 9, content: this.ckeditorContent, type: Type.fillblank, level: Level.NB, answers: this.answers, formatQuestion: FormatQuestion.single, questionGroupId: null, creatorId: null }
      this.dataQuestion.level = this.questionLevelCtrl.value;
      this.dataQuestion.answers = this.answers;
      console.log(this.answers);
      return this.dataQuestion;
    }
  }

  addAnswer(answer, index) {
    if (this.answers.filter(x => x.action != Action.Delete).length < 9) {
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999), content: answer, questionId: this.dataQuestion.id, trueAnswer: index, isShuffler: false, action: Action.Add });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }

  addFakeAnswer() {
    if (this.answers.filter(x => x.action != Action.Delete).length < 9) {
      this.answers.push({ id: generateRandomUniqueInteger(1000, 9999), content: '', questionId: this.dataQuestion.id, trueAnswer: 0, isShuffler: false, action: Action.Add });
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }

  deleteAnswer(id) {
    let answer = this.answers.find(x => x.id == id);
    let index = this.answers.indexOf(answer, 0);

    if (answer.trueAnswer > 0) {

      let ckeEditableArea = $(this.ck.instance.editable().$);

      let htmlData = ckeEditableArea.find('span[class^="Blanking"]');
      htmlData.each(function (index, element) {
        let currentDataBlankIndex = parseInt($(element).attr('data-position'));
        if (currentDataBlankIndex > answer.trueAnswer) {
          $(element).html('(' + (currentDataBlankIndex - 1) + ')');
          //cập nhật lại index ở div phuong-an-dung
          let rowPhuongAnDung = $('.phuong-an-dung > table > tbody > tr[data-position="' + currentDataBlankIndex + '"]');
          $(rowPhuongAnDung).find('*[blank-index]').text(currentDataBlankIndex - 1);
          $(rowPhuongAnDung).attr('data-position', currentDataBlankIndex - 1);
          $(element).attr('data-position', currentDataBlankIndex - 1);
        } else if (currentDataBlankIndex == answer.trueAnswer) {
          $(element).replaceWith(answer.content);
          console.log('xóa', answer.content)
          // $(element).replaceWith(answer.content.substring(3,answer.content.length-5));
        }
      });

      if (this.dataQuestion.action == Action.EditScore) {
        this.dataQuestion.action = Action.EditAll;
      } if (this.dataQuestion.action != Action.EditAll) {
        this.dataQuestion.action = Action.EditContent
      }

      this.dataQuestion.content = this.ck.instance.getData();

      this.currentBlankID -= 1;
      this.answers.map((ans, i) => {
        if (ans.trueAnswer > answer.trueAnswer) {
          this.answers[i].trueAnswer--;
        }
      });
    }

    if (answer.action == Action.Add) {
      this.answers.splice(index, 1);
    } else {
      answer.action = Action.Delete;
    }
  }

  changeSTT(index) {
    this.answers.map((ans, i) => {
      if (ans.trueAnswer >= index) {
        this.answers[i].trueAnswer += 1;
      }
    });
  }

  handleAfterCommandExec(event, editor) {
    if (this.answers.filter(x => x.action != Action.Delete).length < 9) {
      var commandName = event.data.name;
      // Kiểm tra sự kiện sau khi click tag U
      // For 'underline' commmand
      if (commandName == "taophuongan") {
        var mySelection = this.ck.instance.getSelection();

        let selectedText = mySelection.getNative();
        let editor = this.ck.instance;
        let realSelectedText = getSelectionHTML(selectedText).toString();
        // if (mySelection.getType() == 2) {

        if (mySelection.getType() == CKEDITOR.SELECTION_TEXT) {
          //XLS
          if (CKEDITOR.env.ie) {
            mySelection.unlock(true);
            selectedText = mySelection.getNative();
          } else {
            selectedText = mySelection.getNative();
          }

          realSelectedText = selectedText.toString();


          if (realSelectedText.trim().length == 0) {
            return false;
          }
        }

        var currentTag;
        if (CKEDITOR.env.ie) {
          mySelection.unlock(true);
          selectedText = mySelection.getNative();
        } else {
          selectedText = mySelection.getNative();
        }
        if (realSelectedText.trim() == "") {
          return false;
        }


        // if (isNullOrWhiteSpace(selectedText.toString().trim()) || isNullOrEmpty(selectedText.toString().trim())) {
        //   alert('Vui lòng chọn văn bản');
        //   return;
        // }
        if (getSelectionHTML(selectedText).indexOf('class="Blanking') != -1 ||
          ($(editor.getSelectedHtml().$).is('span') &&
            $(editor.getSelectedHtml().getHtml()).hasClass('Blanking'))) {
          alert('Vùng chọn đã có phương án');
          return;
        }

        //insert blank, thay bằng index của phần điền khuyết vào editor
        let insertedBlank = editor.document.createElement('span');
        insertedBlank.setAttribute('data-current-answer', '');
        insertedBlank.setAttribute('class', 'Blanking Blanking-' + this.CauHoiID); //CauHoiID được truyền vào từ View MissingWordAdd(Edit)
        insertedBlank.setAttribute('data-position', this.currentBlankID); //currentBlankID được truyền vào từ View MissingWordAdd(Edit)
        //insertedBlank.setAttribute('style', 'font-weight: bold; border-bottom: 1px solid #c0c0c0; min-width: 50px; display:inline-block; text-align: center;');
        //ngăn không cho người dùng chỉnh sửa phần điền khuyết
        insertedBlank.setAttribute('contenteditable', 'false');
        insertedBlank.appendText('(' + this.currentBlankID.toString() + ')');
        editor.insertElement(insertedBlank);

        this.reorderAnswerIndexes(this.currentBlankID, editor, realSelectedText);

        this.currentBlankID++;

        // } else {
        //   alert('vui lòng chỉ chọn văn bản');
        //   return;
        // }
      }
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }

  sortedTrueAnswers() {
    return this.answers.filter(x => x.trueAnswer != 0 && x.action != Action.Delete).sort((a, b) => a.trueAnswer > b.trueAnswer ? 1 : a.trueAnswer < b.trueAnswer ? -1 : 0);
  }

  fakeAnswers() {
    return this.answers.filter(x => x.trueAnswer == 0 && x.action != Action.Delete);
  }

  reorderAnswerIndexes(currentMaxID, editor, realSelectedText) {
    let ckeEditableArea = $(editor.editable().$);
    let blankIndexElements = ckeEditableArea.find('span[class^="Blanking"]');
    let insertedPosition = 0;
    blankIndexElements.each(function (index, element) {
      if ($(element).attr('data-position') == currentMaxID) {
        insertedPosition = index + 1;
      }
    });

    $(ckeEditableArea.find('span[class^="Blanking"]:gt(' + (insertedPosition - 1) + ')').get().reverse()).each(function (index, element) {
      let oldIndex = parseInt($(element).attr('data-position'));
      $(element).attr('data-position', oldIndex + 1);
      $(element).text('(' + (oldIndex + 1) + ')');
    });

    ckeEditableArea.find('span[class^="Blanking"]').each(function (index, element) {
      if ((index + 1) == insertedPosition) {
        $(element).attr('data-position', insertedPosition);
        $(element).text('(' + insertedPosition + ')');
      }
    });
    if (this.answers.length >= 1) {
      this.changeSTT(insertedPosition);
    }
    this.addAnswer(realSelectedText, insertedPosition);
    console.log("tạo", realSelectedText);
  }

  private initQuestionContent(ck: CKEditorComponent) {
    if (ck) {
      ck.focus.subscribe((ev) => {
        //document.getElementById(ck.instance.id + '_top').style.display = 'block';
      });
      ck.blur.subscribe((ev) => {
        //document.getElementById(ck.instance.id + '_top').style.display = 'none';
      });
    }
  }

  private initAnswer(ck: CKEditorComponent) {
    if (ck) {
      ck.ready.subscribe(() => {
        this.answerWTOs.push({ id: ck.instance.id, wto: null });
      });
      ck.focus.subscribe((ev) => {
        document.getElementById(ck.instance.id + '_top').style.display = 'block';
      });
      ck.blur.subscribe((ev) => {
        document.getElementById(ck.instance.id + '_top').style.display = 'none';
      });
    }
  }

  wto;
  onQuestionContentChange(e) {
    let content = e.editor.getData();
    clearTimeout(this.wto);
    this.disabledUpdate = true;
    this.wto = setTimeout(() => {
      // do stuff when user has been idle for 1 second
      // không cần trim luôn, vì get data ckeditor ra dạng html,
      // so plaintext thì cũng không bao quát các trường hợp định dạng
      if (this.dataQuestion.content != content) {
        if (this.dataQuestion.action == Action.EditScore) {
          this.dataQuestion.action = Action.EditAll;
        } if (this.dataQuestion.action != Action.EditAll) {
          this.dataQuestion.action = Action.EditContent
        }
        this.dataQuestion.content = content;
      }

      this.disabledUpdate = false;
    }, 300);
  }

  onAnswerContentChange(e, answer) {
    //two way binding content, delay 1s rồi mới cập nhật action
    let wtoIndex = this.answerWTOs.findIndex(x => x.id == e.editor.id);
    let content = e.editor.getData();
    clearTimeout(this.answerWTOs[wtoIndex].wto);
    this.answerWTOs[wtoIndex].wto = null;
    this.disabledUpdate = true;
    this.answerWTOs[wtoIndex].wto = setTimeout(() => {
      let i = this.answers.indexOf(answer);
      if (this.answers[i].content != content) {
        if (this.answers[i].action != Action.Add) {
          this.answers[i].action = Action.EditContent;
        }

        this.answers[i].content = content;
      }
      this.disabledUpdate = false;
    }, 300);
  }
}

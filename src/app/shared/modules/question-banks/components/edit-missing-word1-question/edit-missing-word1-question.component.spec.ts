import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMissingWord1QuestionComponent } from './edit-missing-word1-question.component';

describe('EditMissingWord1QuestionComponent', () => {
  let component: EditMissingWord1QuestionComponent;
  let fixture: ComponentFixture<EditMissingWord1QuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditMissingWord1QuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMissingWord1QuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

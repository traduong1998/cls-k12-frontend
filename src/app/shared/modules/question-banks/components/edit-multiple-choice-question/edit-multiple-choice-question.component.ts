import { AnswerViewData } from './../../Interfaces/question-data';
import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CKEditorComponent } from 'ckeditor4-angular';
import { FormatQuestion } from 'sdk/cls-k12-sdk-js/src/services/question/enums/FormatQuestion';
import { Level } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Level';
import { Type } from 'sdk/cls-k12-sdk-js/src/services/question/enums/Type';
import { isNullOrEmpty, isNullOrWhiteSpace } from 'src/app/shared/helpers/validation.helper';
import { ckConfig } from 'src/environments/ckeditorConfig';
import { QuestionViewData } from '../../Interfaces/question-data';
import { SaveDataService } from '../../Services/save-data.service';
import { Action } from '../../Interfaces/action';
import { QuestionLevelOption } from '../../Interfaces/question-level-option';
import { QuestionTypeOption } from '../../Interfaces/question-type-option';
import { FormControl } from '@angular/forms';
import { ConfigUploadCkeditor } from '../../Interfaces/config-upload-ckeditor';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-edit-multiple-choice-question',
  templateUrl: './edit-multiple-choice-question.component.html',
  styleUrls: ['./edit-multiple-choice-question.component.scss']
})
export class EditMultipleChoiceQuestionComponent implements OnInit {
  @Input() dataQuestion: QuestionViewData;
  @Input() isGroup: boolean;
  @Input() hideAction = false;
  @Output() questionOutput = new EventEmitter();
  @Output() onReady = new EventEmitter<boolean>();
  @Input() configUpload: ConfigUploadCkeditor;

  isReady: boolean;
  isDelele = false;
  ckeditorContent = "";
  configQuestion = {};
  totalAnswer: number = 0;
  disabledUpdate: boolean = false;
  // danh sách waiting timeout object của answer
  // dùng để delay cập nhật phương án khi người dùng gõ thay đổi phương án
  answerWTOs = [];
  alphabet: string[] = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "V", "X", "Y", "Z"];
  answers: AnswerViewData[] = [];

  configAnswer = {
    // startupFocus: true,
    height: 60,
    allowedContent: true,

    extraPlugins: ckConfig.extraPlugins,
    toolbarGroups: ckConfig.toolbarAnswer,
    removeButtons: ckConfig.removeButtonsAnswer,
    removePlugins: 'elementspath,magicline'
  };
  questionTypeCtrl = new FormControl();
  questionLevelCtrl = new FormControl();

  levelQuestionEnum: QuestionTypeOption[] = [
    { id: Level.NB, name: "Nhận biết" },
    { id: Level.TH, name: "Thông hiểu" },
    { id: Level.VD, name: "Vận dụng" },
    { id: Level.VDC, name: "Vận dụng cao" }];

  typeQuestionEnum: QuestionLevelOption[] = [
    { id: Type.singlechoice, name: "Một lựa chọn" },
    { id: Type.multichoice, name: "Nhiều lựa chọn" },
    { id: Type.underline, name: "Gạch chân" },
    { id: Type.fillblank, name: "Điền khuyết" },
    { id: Type.fillblank2, name: "Điền khuyết loại 2" },
    { id: Type.matching, name: "Ghép đôi" },
    { id: Type.truefasle, name: "Đúng sai" },
    { id: Type.truefaslechause, name: "Mệnh đề đúng sai" },
    { id: Type.essay, name: "Tự luận" }];
  @ViewChild("ck") ck: CKEditorComponent;
  @ViewChildren("cks") cks: QueryList<CKEditorComponent>;

  constructor(private saveData: SaveDataService,private _messageService:MessageService) {
    this.configQuestion = {
      contentsCss: window.location.origin + '/assets/ckeditor/contents.css',
      startupFocus: true,
      allowedContent: true,
      extraPlugins: ckConfig.extraPlugins,
      toolbarGroups: ckConfig.toolbarGroups,
      removeButtons: ckConfig.removeButtons,
      removePlugins: 'elementspath,magicline'
    };
  }

  ngOnChanges(): void {
    this.isReady = false;
    this.answerWTOs.map(x => {
      clearTimeout(x.wto);
    })
    clearTimeout(this.wto);
    this.disabledUpdate = false;

    if (this.dataQuestion != null) {
      this.ckeditorContent = this.dataQuestion.content;
      this.answers = this.dataQuestion.answers;

      // Khởi tạo thứ tự phương án trả lời
      this.answers.map((ans, i) => {
        ans.order = i;
      });

      this.totalAnswer = this.answers.filter(x => x.action != Action.Delete).length;
      setTimeout(() => {
        this.isReady = true;
      }, 500);
      this.questionTypeCtrl.setValue(this.dataQuestion.type);
      this.questionLevelCtrl.setValue(this.dataQuestion.level);
    }
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.cks.changes.subscribe((x) => {
      this.initAnswer(x.last);
    });

    this.cks.forEach((x) => {
      this.initAnswer(x);
    });

    this.initQuestionContent(this.ck);
  }

  updateQuestion(notifi?: boolean): void {
    if (this.getvalue() != null) {
      this.questionOutput.emit(this.dataQuestion);
      if(notifi){
        this._messageService.susccessCallback("Đã áp dụng");
      }
    }
  }

  getActiveAnswer() {
    return this.answers.filter(x => x.action != Action.Delete);
  }

  setAnswers() {
    return this.answers;
  }

  setQuestion() {
    let err = 0;

    var check: boolean;
    check = this.answers.filter(x => x.action != Action.Delete).some(x => (isNullOrEmpty(x.content)));
    if (isNullOrWhiteSpace(this.dataQuestion.content) || isNullOrEmpty(this.dataQuestion.content)) {
      err++;
      alert("Bạn chưa nhập nội dung câu hỏi của câu nhiều lựa chọn");
      return { content: err.toString(), answers: [] };
    }
    else if (check == true) {
      err++;
      alert("Bạn chưa nhập nội dung câu trả lời của câu nhiều lựa chọn");
      return { content: err.toString(), answers: [] };
    }
    else if (this.answers.filter(x => x.action != Action.Delete).some(x => x.trueAnswer == 1) == false) {
      err++;
      alert("Bạn chọn ít nhất 1 câu trả lời đúng của câu nhiều lựa chọn");
      return { content: err.toString(), answers: [] };
    }
    else {
      this.dataQuestion.answers = this.answers;
      return this.dataQuestion;
    }
    //return this.ckeditorContent;
  }

  getvalue() {
    var check: boolean;
    if (this.answers.filter(x => x.action != Action.Delete).length == 0) {
      alert("Bạn cần tạo ít nhất 1 phương án trả lời cho câu hỏi");
      return null;
    }
    check = this.answers.filter(x => x.action != Action.Delete).some(x => (isNullOrEmpty(x.content)));
    if (isNullOrWhiteSpace(this.dataQuestion.content) || isNullOrEmpty(this.dataQuestion.content)) {
      alert("Bạn chưa nhập nội dung câu hỏi");
      return null;
    }
    else if (check == true) {
      alert("Bạn chưa nhập nội dung câu trả lời");
      return null;
    }

    else if (this.answers.filter(x => x.action != Action.Delete).some(x => x.trueAnswer == 1) == false) {
      alert("Bạn chọn ít nhất 1 câu trả lời đúng");
    }
    else {
      this.dataQuestion.level = this.questionLevelCtrl.value;
      this.dataQuestion.answers = this.answers;
      return this.dataQuestion;
    }
  }

  changeSelectAnswer(i, event) {

    if (event.checked == true) {
      this.answers[i].trueAnswer = 1;
    }
    else {
      this.answers[i].trueAnswer = 0;
    }

    if (this.answers[i].action != Action.Add) {
      this.answers[i].action = Action.EditContent;
    }
  }

  addAnswer() {
    if (this.getActiveAnswer().length < 9) {
      this.answers.push({
        content: '', questionId: this.dataQuestion.id,
        trueAnswer: 0, isShuffler: false, action: Action.Add, order: this.getActiveAnswer().length
      });
      this.totalAnswer = this.getActiveAnswer().length;
    }
    else {
      alert("Số phương án tối đa là 9");
    }
  }

  deleteAnswer(i) {
    if (this.totalAnswer == 1)
      return;
    let answer = this.answers[i];
    //this.answers.splice(i, 1);
    // Nếu là phương án mới đc thêm vào thì xóa hẵn luôn
    // Nếu là phương án được lưu DB rồi thì cập nhật action để xóa trên backend;
    if (this.answers[i].action == Action.Add) {
      this.answers.splice(i, 1);
    } else {
      this.answers.map((ans, index) => {
        if (index == i) {
          ans.action = Action.Delete;
          ans.trueAnswer = 0;
        }
        return ans;
      });
    }
    this.answers.map((ans, i) => {
      if (ans.order > answer.order) {
        ans.order--;
      }
    });

    answer.order = 0;

    this.totalAnswer = this.getActiveAnswer().length;
  }

  private initQuestionContent(ck: CKEditorComponent) {
    ck.ready.subscribe((ev) => {
      //document.getElementById(ck.instance.id + '_top').style.display = 'none';
    });
    ck.focus.subscribe((ev) => {
      //document.getElementById(ck.instance.id + '_top').style.display = 'block';
    });
    ck.blur.subscribe((ev) => {
      //document.getElementById(ck.instance.id + '_top').style.display = 'none';
    });
  }

  private initAnswer(ck: CKEditorComponent) {
    ck.ready.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'none';
      this.answerWTOs.push({ id: ck.instance.id, wto: null });
    });
    ck.focus.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'block';
    });
    ck.blur.subscribe((ev) => {
      document.getElementById(ck.instance.id + '_top').style.display = 'none';
    });
  }

  wto;
  onQuestionContentChange(e) {
    let content = e.editor.getData();
    clearTimeout(this.wto);
    this.disabledUpdate = true;
    this.wto = setTimeout(() => {
      // do stuff when user has been idle for 1 second
      if (this.dataQuestion.content != content.trim()) {
        if (this.dataQuestion.action == Action.EditScore) {
          this.dataQuestion.action = Action.EditAll;
        } if (this.dataQuestion.action != Action.EditAll) {
          this.dataQuestion.action = Action.EditContent
        }
        this.dataQuestion.content = content.trim();
      }
      this.disabledUpdate = false;
    }, 300);
  }

  onAnswerContentChange(e, answer) {
    //two way binding content, delay 1s rồi mới cập nhật action
    let wtoIndex = this.answerWTOs.findIndex(x => x.id == e.editor.id);
    let content = e.editor.getData();
    clearTimeout(this.answerWTOs[wtoIndex].wto);
    this.answerWTOs[wtoIndex].wto = null;
    this.disabledUpdate = true;
    this.answerWTOs[wtoIndex].wto = setTimeout(() => {
      let i = this.answers.indexOf(answer);
      if (this.answers[i].content != content) {
        if (this.answers[i].action != Action.Add) {
          this.answers[i].action = Action.EditContent;
        }

        this.answers[i].content = content;
      }
      this.disabledUpdate = false;
    }, 300);
  }
}

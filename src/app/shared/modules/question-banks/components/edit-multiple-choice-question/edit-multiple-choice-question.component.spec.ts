import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMultipleChoiceQuestionComponent } from './edit-multiple-choice-question.component';

describe('EditMultipleChoiceQuestionComponent', () => {
  let component: EditMultipleChoiceQuestionComponent;
  let fixture: ComponentFixture<EditMultipleChoiceQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditMultipleChoiceQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMultipleChoiceQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

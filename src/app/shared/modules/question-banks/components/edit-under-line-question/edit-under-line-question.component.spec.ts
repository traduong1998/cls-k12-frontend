import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUnderLineQuestionComponent } from './edit-under-line-question.component';

describe('EditUnderLineQuestionComponent', () => {
  let component: EditUnderLineQuestionComponent;
  let fixture: ComponentFixture<EditUnderLineQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditUnderLineQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUnderLineQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

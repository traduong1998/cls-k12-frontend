import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSingleChoiceQuestionComponent } from './edit-single-choice-question.component';

describe('EditSingleChoiceQuestionComponent', () => {
  let component: EditSingleChoiceQuestionComponent;
  let fixture: ComponentFixture<EditSingleChoiceQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditSingleChoiceQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSingleChoiceQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

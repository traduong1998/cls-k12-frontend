import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { Answer, MultiChoiceAnswer, SingleChoiceAnswer } from 'src/app/shared/modules/question/Answer';
import { Question } from 'src/app/shared/modules/question/Question';

@Injectable({
  providedIn: 'root'
})
export class SaveDataService {

  //question
  private _question = new Subject<Question>();
  question$ = this._question.asObservable();
  //answer
  private _answer = new Subject<SingleChoiceAnswer[]>();
  answer$ = this._answer.asObservable();
  constructor() { }

  getQuestion(question:Question){
    this._question.next(question);
  }

  getAnswer(answer:SingleChoiceAnswer[]){
    this._answer.next(answer);
  }
  getAnswerMuti(answer:MultiChoiceAnswer[]){
    this._answer.next(answer);
  }
}

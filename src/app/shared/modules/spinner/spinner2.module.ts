import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NgxSpinnerModule } from "ngx-spinner";
import { Spinner2Component } from "./spinner2.component";

@NgModule({
    declarations: [Spinner2Component],
    imports: [
        CommonModule,
        NgxSpinnerModule
    ],
    exports: [
        Spinner2Component
    ]
})
export class Spinner2Module { }
import { Component, Inject, Input, OnInit } from "@angular/core";
import { PRIMARY_SPINNER, Size, Spinner } from "ngx-spinner";
import { DEFAULT_NGXSPINNER } from "./config";

@Component({
    selector: 'app-spinner2',
    templateUrl: './spinner2.component.html'
})

export class Spinner2Component implements OnInit {
    /**
   * To set backdrop color
   * Only supports RGBA color format
   * @memberof NgxSpinnerComponent
   */
    @Input() bdColor: string;
    /**
     * To set spinner size
     *
     * @memberof NgxSpinnerComponent
     */
    @Input() size: Size;
    /**
     * To set spinner color(DEFAULTS.SPINNER_COLOR)
     *
     * @memberof NgxSpinnerComponent
     */
    @Input() color: string;
    /**
     * To set type of spinner
     *
     * @memberof NgxSpinnerComponent
     */
    @Input() type: string;
    /**
     * To toggle fullscreen mode
     *
     * @memberof NgxSpinnerComponent
     */
    @Input() fullScreen: boolean;
    /**
     * Spinner name
     *
     * @memberof NgxSpinnerComponent
     */
    @Input() name: string;
    /**
     * z-index value
     *
     * @memberof NgxSpinnerComponent
     */
    @Input() zIndex: number;
    /**
     * Custom template for spinner/loader
     *
     * @memberof NgxSpinnerComponent
     */
    @Input() template: string;
    /**
     * Show/Hide the spinner
     *
     * @type {boolean}
     * @memberof NgxSpinnerComponent
     */
    @Input() showSpinner: boolean;

    constructor(@Inject(DEFAULT_NGXSPINNER) public spinnerConfig: Spinner) {
        this.bdColor = spinnerConfig.bdColor;
        this.zIndex = spinnerConfig.zIndex;
        this.color = spinnerConfig.color;
        this.type = spinnerConfig.type;
        this.size = spinnerConfig.size;
        this.fullScreen = spinnerConfig.fullScreen;
        this.name = PRIMARY_SPINNER;
        this.template = spinnerConfig.template;
        this.showSpinner = false;
    }
    ngOnInit(): void {
        // console.log(`spinnerConfig`, this.spinnerConfig);
    }

}
import { InjectionToken } from "@angular/core";

export const DEFAULT_NGXSPINNER = new InjectionToken<string>('DEFAULT_NGXSPINNER');

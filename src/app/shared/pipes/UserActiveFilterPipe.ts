import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'userActiveFilter',
    pure: false
})
export class UserActiveFilterPipe implements PipeTransform {
    transform(items: any[], filter: any): any {
        return items.filter(item => {
            return item.isActive == true;
        });
    }
}
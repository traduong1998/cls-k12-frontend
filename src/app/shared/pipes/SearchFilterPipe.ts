import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'searchFilterPipe'
})
export class SearchFilterPipe implements PipeTransform {

  transform(arrays: any[], input: string, searchInProperties: string[], order: "ASC" | "DESC", sortFunction?): any {
    if (!order) {
      order = 'ASC';
    }
    const hasSearchWithProperties = searchInProperties && searchInProperties.length > 0;

    if (input) {
      input = this.chuyenSangKhongDau(input.toLowerCase());
      arrays = arrays.filter(x => {
        var ok = true;
        if (hasSearchWithProperties) {
          searchInProperties.forEach(p => {
            ok = this.chuyenSangKhongDau(x[p]).indexOf(input) !== -1;
          });
        }
        else {
          ok = this.chuyenSangKhongDau(x).indexOf(input) !== -1;
        }
        return ok;
      });
    }

    if (sortFunction) {
      arrays = sortFunction(arrays);
    }

    if (order === "DESC") {
      arrays.reverse();
    }
    return arrays;
  }

  private chuyenSangKhongDau(alias) {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
    str = str.replace(/ + /g, " ");
    str = str.trim();
    return str;
  }
}

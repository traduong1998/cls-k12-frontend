import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'orderBy',
    pure: false
})
export class OrderByPipe implements PipeTransform {
    transform(items: any[], config: string[], orderBy: "ASC" | "DESC" = "ASC"): any {
        var key = config[0];
        if (items) {
            items.sort(function (a, b) {
                if(orderBy == "DESC"){
                    return (a[key] < b[key] ? 1 : -1);
                }
                return (a[key] > b[key] ? 1 : -1);
            });
        }
        return items;
    }
}
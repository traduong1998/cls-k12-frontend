import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'sortParticipantFilterPipe'
})
export class SortParticipantFilterPipe implements PipeTransform {

  transform(arrays: any[]): any {
    var tienIndex = -1;
    var luiIndex = 1;
    var guiNguyenIndex = 0;
    arrays.sort((a, b) => {
      if (a.isOwner) {
        return tienIndex;
      }
      else if (b.isOwner) {
        return luiIndex;
      }
      else if (a.isActive) {
        if (b.isRaiseHand) {
          return luiIndex;
        }
        else if (b.isActive) {
          return guiNguyenIndex;
        }
        else {
          return tienIndex;
        }
      }
      else if (a.isRaiseHand) {
        if (b.isRaiseHand) {
          return guiNguyenIndex;
        }
        else {
          return tienIndex;
        }
      }
      else if (!a.isActive && (b.isRaiseHand || b.isActive)) {
        return luiIndex;
      }
      else if (!a.isRaiseHand && (b.isRaiseHand || b.isActive)) {
        return luiIndex;
      }
      else
        return guiNguyenIndex;
    });
    arrays.reverse();
    console.log(`pipe`);;
    return arrays;
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RulesConfirmDialogComponent } from './rules-confirm-dialog.component';

describe('RulesConfirmDialogComponent', () => {
  let component: RulesConfirmDialogComponent;
  let fixture: ComponentFixture<RulesConfirmDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RulesConfirmDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RulesConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

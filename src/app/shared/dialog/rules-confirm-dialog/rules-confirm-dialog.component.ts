import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { DeleteDialog } from '../../interface/backend/DeleteDiaglog';
import { ErrorSnackBarComponent } from '../../snack-bar/error-snack-bar/error-snack-bar.component';

@Component({
  selector: 'app-rules-confirm-dialog',
  templateUrl: './rules-confirm-dialog.component.html',
  styleUrls: ['./rules-confirm-dialog.component.scss']
})
export class RulesConfirmDialogComponent implements OnInit {

  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  txtXoa = '';
  inputData;
  checked = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DeleteDialog, private mdDialogRef: MatDialogRef<RulesConfirmDialogComponent>, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.inputData = this.data;
  }

  submit() {
    this.mdDialogRef.close("Save");
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { DeleteDialog } from '../../interface/backend/DeleteDiaglog';
import { ErrorSnackBarComponent } from '../../snack-bar/error-snack-bar/error-snack-bar.component';

@Component({
  selector: 'app-delete-dia-log',
  templateUrl: './delete-dia-log.component.html',
  styleUrls: ['./delete-dia-log.component.scss']
})
export class DeleteDiaLogComponent implements OnInit {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  txtXoa = '';
  inputData;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DeleteDialog, private mdDialogRef: MatDialogRef<DeleteDiaLogComponent>,private _snackBar: MatSnackBar) {
  }
  ngOnInit(): void {
    this.inputData = this.data;
  }

  submit() {
    if (this.txtXoa != "DONGY"){
      this._snackBar.openFromComponent(ErrorSnackBarComponent, {
        data: 'Bạn nhập chưa đúng, vui lòng nhập lại',
        duration: 3000,
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
      });
    }else{
      this.mdDialogRef.close(this.txtXoa);
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteDiaLogComponent } from './delete-dia-log.component';

describe('DeleteDiaLogComponent', () => {
  let component: DeleteDiaLogComponent;
  let fixture: ComponentFixture<DeleteDiaLogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteDiaLogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDiaLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

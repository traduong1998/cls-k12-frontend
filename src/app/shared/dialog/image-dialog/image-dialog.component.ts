import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ProgressPlayerService } from '../../../features/learner-lesson/Services/progress-player.service';

@Component({
  selector: 'app-image-dialog',
  templateUrl: './image-dialog.component.html',
  styleUrls: ['./image-dialog.component.scss']
})
export class ImageDialogComponent implements OnInit {
  listImage = [
    // {id: 1, thumbnail: '/assets/images/dialog/1.png', url: '/assets/images/dialog/bg1.jpg'},
    { id: 2, thumbnail: '/assets/images/dialog/banner-1.jpg', url: '/assets/images/dialog/banner-1-active.jpg' },
    { id: 3, thumbnail: '/assets/images/dialog/banner-2.jpg', url: '/assets/images/dialog/banner-2-active.jpg' },
    { id: 4, thumbnail: '/assets/images/dialog/banner-3.jpg', url: '/assets/images/dialog/banner-3-active.jpg' },
    { id: 5, thumbnail: '/assets/images/dialog/banner-4.jpg', url: '/assets/images/dialog/banner-4-active.jpg' },
    { id: 6, thumbnail: '/assets/images/dialog/banner-5.jpg', url: '/assets/images/dialog/banner-5-active.jpg' },
    { id: 7, thumbnail: '/assets/images/dialog/banner-6.jpg', url: '/assets/images/dialog/banner-6-active.jpg' },
    { id: 8, thumbnail: '/assets/images/dialog/banner-7.jpg', url: '/assets/images/dialog/banner-7-active.jpg' },
    { id: 9, thumbnail: '/assets/images/dialog/banner-8.jpg', url: '/assets/images/dialog/banner-8-active.jpg' },
    { id: 10, thumbnail: '/assets/images/dialog/banner-9.jpg', url: '/assets/images/dialog/banner-9-active.jpg' },
    { id: 11, thumbnail: '/assets/images/dialog/banner-10.png', url: '/assets/images/dialog/banner-10-active.jpg' },
    { id: 12, thumbnail: '/assets/images/dialog/banner-11.jpg', url: '/assets/images/dialog/banner-11-active.jpg' }
  ];
  isImageSelected = '';
  
  constructor(
    public dialogRef: MatDialogRef<ImageDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private progressPlayerService: ProgressPlayerService
  ) { }

  ngOnInit(): void { }

  closeModal(): void {
    this.dialogRef.close();
  }

  onSelectImage(image): void {
    if (!image) {
      // localStorage.removeItem('bg_learn');
      this.isImageSelected = '';
    } else {
      this.isImageSelected = image.url;
    }
    // Nếu là chế độ tối thì không được đổi background
    if (localStorage.getItem('light_mode') !== 'true') {
      this.progressPlayerService.backgroundEmit.next(this.isImageSelected);
    }
  }

  // Khi bấm thay đổi mới lưu vào localStorage
  confirm(): void {
    localStorage.setItem('bg_learn', this.isImageSelected);
    this.dialogRef.close();
  }

}

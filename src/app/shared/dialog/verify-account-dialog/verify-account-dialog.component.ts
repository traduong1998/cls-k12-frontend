import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
export interface VerifyAccountType {
  title: string,
  detail: string,
  buttonText: string
}
@Component({
  selector: 'app-verify-account-dialog',
  templateUrl: './verify-account-dialog.component.html',
  styleUrls: ['./verify-account-dialog.component.scss']
})
export class VerifyAccountDialogComponent implements OnInit {
  title: string = 'Tài khoản Zoom Chưa được cài đặt hoa';
  detail: string = `Vui lòng vào phần <span class="text-primary-color">Cài đặt</span> để tạo liên kết Zoom`;
  buttonText: string = 'Đi tới cài đặt';
  constructor(@Inject(MAT_DIALOG_DATA) public data: VerifyAccountType, public dialogRef: MatDialogRef<VerifyAccountDialogComponent>) {
  }

  ngOnInit(): void {
    if (this.data) {
      this.title = this.data.title;
      this.detail = this.data.detail;
      this.buttonText = this.data.buttonText
    }
  }
  onGotoSettingClickHandler() {
    this.dialogRef.close(true);
  }
}

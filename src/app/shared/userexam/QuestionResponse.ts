
export interface QuestionResponse {
    STT: number;
    mark: number;
    questionId: number;
    type: string;
    content: string;
    groupId?: number;
    level: string;
    generalRequirements?: string;
    questionFormat: string;
    isShuffle: boolean;
    subQuestion?: QuestionResponse[];
    listAnswer: Answer[];
    numberOfFillblank?: number;
}
export interface Answer {
    id?: number;
    NoiDung: string;
    dapAnDung?: number;
    xaoTronDapAn?: boolean;

}

export interface UserExercise  {
   questionId:number;
   userAnswer:AnswerOfUser[];
   isTicked:boolean;
   isAnswered:boolean;
}
export interface AnswerOfUser{
    answerId:number;
    answer:number;
    fill_inPosition?:number;
}

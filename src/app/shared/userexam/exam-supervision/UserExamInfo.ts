export interface UserExamInfo{
    userExamInfoId:number;
    userId:number;
    username:string;
    fullName:string;
    examId:number;
    thoiGianVaoPhongThi:Date;
    thoiGianNhanDe:Date;
    thoiGianBatDauLam:Date;
    infringes:Infringe[];
    userStatuses:UserStatus[];
    timeFinishExam:Date;
}
export interface Infringe{
    infringeId:number;
    userExamInfoId:number;
    supervisorId?:number;
    infringeName:string;
    hinhThucXuLy:string;
    diemBiTru:number;
    infringeTime:Date;
    
}
export interface UserStatus{
    userStatusId:number;
    userStatusName:string;
    timeXayRa:Date;
}

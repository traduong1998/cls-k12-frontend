import { Component, OnInit, Input, ChangeDetectorRef, SimpleChanges, OnChanges, AfterViewInit, OnDestroy } from '@angular/core';
import videojs from 'video.js';
import { MatSliderChange } from '@angular/material/slider';
import { MimeTypes } from '../enums/mime-type';
import { AuthService } from '../../core/services/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { ImageDialogComponent } from '../dialog/image-dialog/image-dialog.component';
import { ProgressPlayerService } from '../../features/learner-lesson/Services/progress-player.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FileServerEndpoint } from 'sdk/cls-k12-sdk-js/src/services/file-server/endpoints/file-server-endpoint';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-videojs-player',
  templateUrl: './videojs-player.component.html',
  styleUrls: ['./videojs-player.component.scss']
})
export class VideojsPlayerComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  @Input() linkVideo = '';
  // Thời gian tiến tới và lùi video (s)
  @Input() timeNextAndPrevious = 0;
  fileServer: FileServerEndpoint;
  isPlay = true;
  adjustVideo = {
    speed: [
      { name: 'Chậm', ratio: 0.5, active: false },
      { name: 'Bình thường', ratio: 1, active: true },
      { name: 'Nhanh', ratio: 2, active: false },
      { name: 'Tuỳ chỉnh', ratio: null, active: false },
    ],
    playback: [
      { ratio: 3, active: false },
      { ratio: 5, active: true },
      { ratio: 10, active: false }
    ],
    playbackRate: [
      { name: '1080p', ratio: 'hd1080', active: false },
      { name: '720p', ratio: 'hd720', active: false },
      { name: '480p', ratio: 'large', active: false },
      { name: '360p', ratio: 'medium', active: false },
      { name: '240p', ratio: 'small', active: false },
      { name: 'Tự động', ratio: '', active: true },
    ]
  };
  volume = {
    value: null,
    isMute: true,
    show: false
  };
  playSpeed = {
    speed: [0.25, 0.5, 1, 1.5, 2],
    speedValue: 1
  };
  progress = {
    start: null,
    value: null,
    end: null,
    min: 0,
    max: null,
    valueHoverSlider: null
  };
  breakPoint = [{ point: null, disable: false }, { point: null, disable: true }];
  limitRange = {
    isLimit: false,
    timeLimit: null
  };
  player;
  // Load xong video mới enable trình phát
  isLoadVideo = true;
  // Chế độ sáng / tối
  mode = {
    light: false,
    show: true
  };
  // disable nút qua bài và trở lại
  disableNext = false;
  disableBack = false;
  sjDisableButtonNext;
  sjDisableButtonBack;
  isShowReplay = false;
  sjReturn;
  // Nếu tua video thì không lưu lại duration
  isTrackingVideo = false;
  isFullScreen;
  isOnBottom = false;
  idContentCurrent = 0;
  durationSeek = {
    isSeek: false,
    time: null,
    timeSeekFirst: null
  };
  widthScreen = window.innerWidth;
  toogleMobile = window.innerWidth > 525 ? true : false;
  videoMimeTypeSupports = [MimeTypes.m3u8, MimeTypes.mp4];
  currentMimeType = 0;
  isIOS = false;

  constructor(
    private authService: AuthService,
    private cdr: ChangeDetectorRef,
    public dialog: MatDialog,
    private progressPlayerService: ProgressPlayerService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute
  ) {
    this.escEvent();
    this.fileServer = new FileServerEndpoint();
    this.idContentCurrent = this.route.snapshot.params.id;
    window.addEventListener('resize', (event) => this.handleResize(event));
  }

  ngOnChanges(changes: SimpleChanges): void {
    // Khi link video thay đổi thì gán lại và init lại videojs iframe
    if (changes.linkVideo.currentValue) {
      this.linkVideo = changes.linkVideo.currentValue;
      // Init lại trình phát và set localstorage
      if (this.player) {
        this.onSetLocalDuration(changes.linkVideo.previousValue);
        this.idContentCurrent = this.route.snapshot.params.id;
        this.isLoadVideo = true;
        this.init(true);
        this.isShowReplay = false;
        this.durationSeek.isSeek = false;
      }
    }
  }

  ngOnInit(): void {
    this.spinner.show();
    this.init();
    // Check chế độ darkmode localStorage
    if (localStorage.getItem('light_mode') === 'true') {
      this.mode.light = true;
      setTimeout(() => { this.progressPlayerService.emitLightModeEvent(true); }, 0);
    }
    // Lắng nghe click content để ẩn disabled button qua bài và trở lại
    this.sjDisableButtonNext = this.progressPlayerService.disableButtonNext$.subscribe((res) => this.disableNext = res);
    this.sjDisableButtonBack = this.progressPlayerService.disableButtonBack$.subscribe((res) => this.disableBack = res);
    // Lắng nghe content là complete từ learner, true thì phát lại video từ đầu, không thì get duration từ localstorage
    this.sjReturn = this.progressPlayerService.returnDurationVideo$.subscribe(() => {
      this.progress.value = 0;
      this.player.currentTime(0);
      this.progress.start = 0;
    });
  }

  onLinkVideoIOS(): Promise<string> {
    const linkConvert = this.checkLinkUrl();
    return new Promise(resolve => {
      this.fileServer.getLinkIOSLinks(linkConvert).then((res: any) => {
        resolve(res.publicLink);
      });
    })
  }

  init = async (checkLocal?: boolean)=> {
    this.spinner.show();
    if (this.checkIOSDevice() === 'iOS') {
      this.isIOS = true;
      this.linkVideo = await this.onLinkVideoIOS();
    }
    this.player = videojs('player', {
      autoplay: false, controls: false, errorDisplay: false
    }, () => {
      this.player.src({ src: this.linkVideo, type: MimeTypes.m3u8 });
      if(this.isIOS){
        this.player.play();
      }
    });
    // Loading video
    this.player.on('loadedmetadata', () => this.onReadyVideo(checkLocal));
    // Video đã load xong
    this.player.on('loadeddata', () => this.isLoadDoneVideo());
    // Kiểm tra link lỗi
    this.player.on('error', () => {
      if(!this.isIOS){
        this.checkStatusVideo();
      }
      if (this.player.error().code === 4) {
        this.currentMimeType++;
        if (this.currentMimeType > this.videoMimeTypeSupports.length) {
          // alert("Không hỗ trợ video này");
          return false;
        }
        this.player.src({ src: this.linkVideo, type: this.videoMimeTypeSupports[this.currentMimeType] });
      }
    });
    // Nếu video kết thúc thì hiện nút replay
    this.player.on('ended', () => {
      // document.getElementById("poster").setAttribute('style', 'display:flex !important');
      this.progress.start = this.progress.end;
      this.progress.value = this.progress.max;
      this.player.posterImage.show();
      this.player.bigPlayButton.show();
      this.player.pause();
      this.isPlay = true;
      this.isShowReplay = true;
      // Nếu xem từ đầu đến cuối thì set % hoàn thành là 100%;
      this.onSetLocalDuration(this.linkVideo, true);
      if (!this.isTrackingVideo) {
        this.progressPlayerService.percentCompleteVideo.next({ link: this.linkVideo, type: 'videojs' });
      }
    });
    // Nếu video đang chạy thì ẩn nút replay
    this.player.on('playing', () => {
      this.isPlay = false;
      this.isShowReplay = false;
    });
    // emit thời gian video
    this.player.on('timeupdate', () => {
      this.durationSeek.timeSeekFirst = this.player.currentTime();
      this.progress.value = this.player.currentTime();
      this.cdr.detectChanges();
      const value = Math.floor(this.player.currentTime());
      this.progress.start = this.formatTime(value);
      // Nếu chạy đến điểm limit video thì stop
      if (this.limitRange.timeLimit && Math.floor(this.progress.value) === this.limitRange.timeLimit) {
        this.player.pause();
      }
      // Nếu gặp breakpoint thì dừng lại để ...
      const arrTime = this.breakPoint.map(x => x.point);
      if (arrTime.includes(Math.floor(this.progress.value))) {
        this.player.pause();
      }
    });
  }

  isLoadDoneVideo(): void {
    this.isLoadVideo = false;
    this.spinner.hide();
  }

  onReadyVideo(checkLocal?: boolean): void {
    this.isLoadVideo = true;
    const value = Math.floor(this.player.duration());
    this.progress.max = value;
    this.progress.end = this.formatTime(value);
    this.volume.value = this.player.volume() * 100;
    // Kiểm tra xem người dùng đã xem video chưa, nếu có thì get lại thời gian của video từ localstorage
    if (localStorage.getItem('duration_videojs')) {
      const valueFilter = JSON.parse(localStorage.getItem('duration_videojs'));
      const filterArr = valueFilter.find(item => item.id === this.linkVideo);
      if (filterArr) {
        this.player.currentTime(+ filterArr.duration, true);
        this.progress.value = + filterArr.duration;
        if (!filterArr.isSeekTo) {
          filterArr.fake = filterArr.duration;
        }
        // Nếu thời gian hiện tại = thời gian của video thì hiển thị nút xem lại
        if (Math.round(this.progress.value) === Math.round(this.progress.max)) {
          this.isShowReplay = true;
        }
        localStorage.setItem('duration_videojs', JSON.stringify(valueFilter));
      } else {
        this.player.currentTime(0);
        this.progress.value = 0;
        this.progress.start = 0;
      }
      this.player.pause();
    }
    if (checkLocal) {
      this.isPlay = true;
      this.player.pause();
    }
  }

  checkLinkUrl():string{
    let link = '';
    if (this.linkVideo.includes('loadVideo?folderVideo=')) {
      link = this.linkVideo.split('loadVideo?folderVideo=')[0];
    } else {
      link = (this.linkVideo.split('file/').pop()).split('/preview')[0];
    }
    return link;
  }

  checkStatusVideo(): void {
    if (this.linkVideo) {
      const link = this.checkLinkUrl();
      if (link) {
        this.fileServer.getStatusFile(link).then(res => {
          if (res) {
            this.progressPlayerService.statusContent.next(res);
            this.spinner.hide();
          }
        });
      }
    }
  }

  checkIOSDevice(): string {
    var userAgent = navigator.userAgent || navigator.vendor || window['opera'];
    if (/iPad|iPhone|iPod/.test(userAgent) && !window['MSStream']) {
      return "iOS";
    }
  }

  ngAfterViewInit(): void {
    // Gửi quyền xem video
    videojs.Hls.xhr.beforeRequest = options => {
      options.headers = options.headers || {};
      const token = `Bearer ${this.authService.getToken()}`;
      options.headers['Authorization'] = token;
    };
    // Hiện thời gian trên trình phát
    document.getElementById('seeksliderjs').addEventListener('mousemove', (e) => {
      const value = + this.calcSliderPos(e).toFixed(2);
      const element = document.getElementById('hover_spanjs');
      element.style.display = 'block';
      const slider = document.getElementById('seeksliderjs');
      // Vị trí hiển thị trên trình phát
      element.style.left = `${(Math.round(value) / this.progress.max) * 100}%`;
      this.progress.valueHoverSlider = this.formatTime(+Math.round(value));
      slider.classList.add('hideChild');
    });
    // Ẩn thời gian trên trình phát
    document.getElementById('seeksliderjs').addEventListener('mouseleave', () => {
      document.getElementById('hover_spanjs').style.display = 'none';
    });
    this.cdr.detectChanges();
  }

  // Chạy / dừng video
  onPlayVideo(status): void {
    // if (this.progress.value < this.limitRange.timeLimit) {
    this.isPlay = !this.isPlay;
    if (status) {
      this.player.play();
    } else {
      this.player.pause();
    }
    // }
  }

  // replay video
  replayVideo(): void {
    this.isPlay = false;
    this.player.currentTime(0.0);
    this.player.play();
    this.isShowReplay = false;
    this.isTrackingVideo = true;
  }

  // Kéo tới/lùi video
  onSlide(event: MatSliderChange): void {
    this.progress.start = this.formatTime(event.value);
    document.getElementById('hover_spanjs').style.display = 'none';
    this.player.pause();
    this.progress.value = event.value;
    this.player.currentTime(this.progress.value);
    this.cdr.detectChanges();
    this.isShowReplay = false;
    this.isTrackingVideo = true;
    this.isPlay = true;
    this.onCheckSeekVideo(event, true);
  }

  onCheckSeekVideo(event, check?: boolean): void {
    if (check) {
      if (this.durationSeek.isSeek === false) {
        localStorage.setItem('fake_duration_vjs', this.durationSeek.timeSeekFirst);
      }
      this.durationSeek.isSeek = true;
    }
    if (localStorage.getItem('duration_videojs')) {
      const value = JSON.parse(localStorage.getItem('duration_videojs'));
      const filterArr = value.find(item => item.id === this.linkVideo);
      if (filterArr) {
        if (event.value <= filterArr.fake) {
          filterArr.isSeekTo = false;
        } else {
          filterArr.isSeekTo = true;
        }
      }
      localStorage.setItem('duration_videojs', JSON.stringify(value));
    }
  }

  onChangeSlidePlayer(event): void {
    if (event.value) {
      this.player.play();
    }
  }

  // Bật/tắt tiếng video
  onMuteVideo(isMute): void {
    if (this.volume.value !== 0) {
      this.volume.isMute = !this.volume.isMute;
      if (isMute) {
        this.player.muted(true);
      } else {
        this.player.muted(false);
      }
    }
  }

  // Tăng giảm âm lượng video
  onSlideVolume(event: MatSliderChange): void {
    this.volume.value = event.value;
    this.player.volume((event.value / 100));
    this.volume.isMute = event.value === 0 ? false : true;
  }

  // Tốc độ phát video
  onPlaybackSpeed(speed: number): void {
    this.player.playbackRate(speed);
    this.playSpeed.speedValue = speed;
  }

  // Tiến tới [seconds] giây
  onForwardVideo(): void {
    this.isTrackingVideo = true;
    const currentTime = this.player.currentTime();
    const time = { value: currentTime + this.timeNextAndPrevious };
    this.player.currentTime(time.value);
    this.isShowReplay = false;
    this.onCheckSeekVideo(time, true);
  }

  // Lùi [seconds] giây
  onRewindVideo(): void {
    const currentTime = this.player.currentTime();
    const time = { value: currentTime - this.timeNextAndPrevious };
    this.player.currentTime(time.value);
    this.isShowReplay = false;
    this.onCheckSeekVideo(time, false);
  }

  // Định dạng thời gian video
  formatTime(seconds: number): string {
    const h = Math.floor(seconds / 3600);
    const m = Math.floor((seconds % 3600) / 60);
    const s = Math.round(seconds % 60);
    return [h, m > 9 ? m : h ? '0' + m : m || '0', s > 9 ? s : '0' + s]
      .filter(a => a)
      .join(':');
  }

  // Chế độ toàn màn hình với từng trình duyệt
  launchIntoFullscreen(element): void {
    this.isFullScreen = true;
    if (element.requestFullscreen) {
      element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
      element.mozRequestFullScreen();
    } else if (element.webkitRequestFullscreen) {
      element.webkitRequestFullscreen();
    } else if (element.msRequestFullscreen) {
      element.msRequestFullscreen();
    }
  }

  // Xem ở chế độ toàn màn hình
  onFullscreen(): void {
    const element = document.getElementById('video-content');
    if (element) {
      if (!this.isFullScreen) {
        this.launchIntoFullscreen(element);
        this.player.play();
      } else {
        if (document.exitFullscreen) {
          document.exitFullscreen();
        }
      }
    }
  }

  // Chế độ sáng / sáng
  onSwitchMode(): void {
    this.mode.light = !this.mode.light;
    localStorage.setItem('light_mode', `${this.mode.light}`);
    this.progressPlayerService.emitLightModeEvent(this.mode.light);
  }

  // Vị trí hiện tại khi hover chuột vào của video
  calcSliderPos(e): number {
    return (e.offsetX / e.target.clientWidth) * this.progress.max;
  }

  onChooseBackground(): void {
    this.dialog.open(ImageDialogComponent, {
      width: '550px',
      height: 'auto',
      data: {}
    });
  }

  // Chất lượng video
  onQuanlity(quality: string, item): void {
    this.adjustVideo.playbackRate.forEach(value => value.active = false);
    item.active = true;
    // this.player.setPlaybackQuality(quality);
  }

  // Tốc độ phát video
  onSpeedVideo(ratio: number, speed): void {
    if (ratio) {
      this.adjustVideo.speed.forEach(value => value.active = false);
      speed.active = true;
      this.player.playbackRate(ratio);
    }
  }

  // Chế độ tuỳ chỉnh tốc độ phát
  onSlideSpeedVideo(event: MatSliderChange): void {
    this.adjustVideo.speed.forEach(value => value.active = false);
    if (event.value <= 12.5) {
      this.player.playbackRate(0.25);
    } else if (event.value > 12.5 && event.value <= 25) {
      this.player.playbackRate(0.5);
    } else if (event.value > 25 && event.value <= 50) {
      this.player.playbackRate(1);
    } else if (event.value > 50 && event.value <= 75) {
      this.player.playbackRate(1.5);
    } else {
      this.player.playbackRate(2);
    }
  }

  // Tốc độ tua video
  onPlaybackRate(ratio: number, rate): void {
    this.timeNextAndPrevious = ratio;
    this.adjustVideo.playback.forEach(value => value.active = false);
    rate.active = true;
  }

  // Trở lại bài trước
  onBackContent(): void {
    this.progressPlayerService.emitBackContentEvent(true);
  }

  // Qua bài
  onNextContent(): void {
    this.progressPlayerService.emitNextContentEvent(true);
  }

  // Hover vào ẩn hiện nút volume
  onHoverVolume(isHover: boolean): void {
    this.volume.show = isHover ? true : false;
  }

  // Tính toán percent để lưu
  checkDurationPercent(clone, filter): number {
    if (this.handleTrackingPercentComplete(clone, filter) > 100) {
      return 100;
    } else {
      return this.handleTrackingPercentComplete(clone, filter);
    }
  }

  // Tính toán percent để lưu
  checkDurationPercentNew(durationLocal): number {
    if (this.isTrackingVideo) {
      if (durationLocal) {
        return (+durationLocal / this.progress.max) * 100;
      } else {
        return 0;
      }
    } else {
      return (this.progress.value / this.progress.max) * 100;
    }
  }

  // Lưu lại thời gian và thông tin của video de
  onSetLocalDuration(link: string, check?: boolean): void {
    if (this.progress.value) {
      if (localStorage.getItem('duration_videojs')) {
        const value = JSON.parse(localStorage.getItem('duration_videojs'));
        const filterArr = value.find(item => item.id === link);
        const durationLocal = localStorage.getItem('fake_duration_vjs');
        if (filterArr) {
          const cloneFilter = JSON.parse(JSON.stringify(filterArr));
          filterArr.duration = this.progress.value;
          // Nếu có tua và % hiện tại < % đã lưu local ở trước thì lấy ở local;
          filterArr.isSeekTo = (cloneFilter.isSeekTo !== undefined || cloneFilter.isSeekTo !== null)
            ? cloneFilter.isSeekTo : this.isTrackingVideo;
          filterArr.fake = cloneFilter.isSeekTo ? filterArr.fake : filterArr.duration;
          filterArr.percent = this.checkDurationPercent(cloneFilter, filterArr);
          this.checkCompleteContent(filterArr, check);
          this.savePercentLocal(filterArr.id, filterArr.percent, filterArr.contentID);
        } else {
          value.push({
            id: link,
            duration: this.progress.value,
            isSeekTo: this.isTrackingVideo,
            percent: this.checkDurationPercentNew(durationLocal),
            contentID: this.idContentCurrent,
            fake: + durationLocal
          });
          this.checkCompleteContent(value[value.length - 1], check);
          this.savePercentLocal(value[value.length - 1].id, value[value.length - 1].percent, value[value.length - 1].contentID);
        }
        localStorage.setItem('duration_videojs', JSON.stringify(value));
      } else {
        const durationLocal = localStorage.getItem('fake_duration_vjs');
        const objVideo = [{
          id: link,
          duration: this.progress.value,
          isSeekTo: this.isTrackingVideo,
          percent: this.checkDurationPercentNew(durationLocal),
          contentID: + this.idContentCurrent,
          fake: + durationLocal
        }];
        this.checkCompleteContent(objVideo[0], check);
        this.savePercentLocal(objVideo[0].id, objVideo[0].percent, objVideo[0].contentID);
        localStorage.setItem('duration_videojs', JSON.stringify(objVideo));
      }
    }
  }

  // Nếu thỏa mãn điều kiện thì lưu thành 100%
  checkCompleteContent(obj, check: boolean): void {
    if (obj.percent && (obj.duration === obj.fake || obj.fake === 0) && !obj.isSeekTo && check) {
      obj.percent = 100;
    }
  }

  savePercentLocal(id: string, percent: number, idContent: number): void {
    this.progressPlayerService.percentCompleteVideoOut.next(
      { id, type: 'videojs', percent, contentId: idContent }
    );
  }

  handleTrackingPercentComplete(obj, root): number {
    if (obj.isSeekTo) {
      // Nếu tua thì lấy ở lần trước;
      // Có 2 trường hợp là tua lớn hơn và tua nhỏ hơn
      return obj.percent;
    } else {
      const current = (this.progress.value / this.progress.max) * 100;
      // root.durationBefore = root.duration;
      if (current <= obj.percent) {
        return obj.percent;
      } else {
        return current;
      }
    }
  }

  ngOnDestroy(): void {
    this.onSetLocalDuration(this.linkVideo);
    if (this.player) {
      this.player.dispose();
    }
    if (this.sjDisableButtonNext) {
      this.sjDisableButtonNext.unsubscribe();
    }
    if (this.sjDisableButtonBack) {
      this.sjDisableButtonBack.unsubscribe();
    }
    if (this.sjReturn) {
      this.sjReturn.unsubscribe();
    }
    localStorage.removeItem('fake_duration_vjs');
  }

  escEvent(): void {
    document.addEventListener('fullscreenchange', (event) => {
      if (document.fullscreenElement) {
      } else {
        this.isFullScreen = false;
      }
    });
  }

  mousemove(e: any): void {
    if (this.isOnBottom === false && this.isFullScreen === true) {
      this.isOnBottom = true;
      setTimeout(() => {
        if (this.isFullScreen === true) {
          this.isOnBottom = false;
        }
      }, 3000);
    }
  }

  mousemoveSlider(e): void {
    this.isOnBottom = true;
  }

  mouseout(e): void {
    this.isOnBottom = false;
  }

  handleResize({ target }): void {
    this.widthScreen = target.innerWidth;
    if (this.widthScreen >= 525) {
      this.toogleMobile = true;
    } else {
      this.toogleMobile = false;
    }
  }

  toogleMenuMobile(): void {
    this.toogleMobile = !this.toogleMobile;
  }

  // Lấy toàn bộ thông số của player
  public getAllProperties(): {} {
    return {
      configPlayer: {
        type: 'videojs-player',
        play: this.isPlay,
        adjustVideo: this.adjustVideo,
        volume: this.volume,
        breakpoint: this.breakPoint,
      },
      video: {
        link: this.linkVideo,
        value: this.progress,
        status: this.isLoadVideo,
      },
      iframeYoutube: this.player
    };
  }

}

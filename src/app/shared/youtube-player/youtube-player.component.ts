import { Component, Input, OnInit, ChangeDetectorRef, SimpleChanges, OnChanges, AfterViewInit, OnDestroy } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';
import { MatDialog } from '@angular/material/dialog';
import { ImageDialogComponent } from '../dialog/image-dialog/image-dialog.component';
import { ProgressPlayerService } from '../../features/learner-lesson/Services/progress-player.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Content } from 'sdk/cls-k12-sdk-js/src/services/learner-lesson/models/content';
import { ReplaySubject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-youtube-player',
  templateUrl: './youtube-player.component.html',
  styleUrls: ['./youtube-player.component.scss']
})

export class YoutubePlayerComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  // Thời gian tiến tới và lùi video (s)
  @Input() timeNextAndPrevious = 0;
  @Input() linkVideo = '';
  @Input() content: Content;
  destroy$: ReplaySubject<any> = new ReplaySubject<any>(1);
  public YT: any;
  public player: any;
  // Trạng thái nút play
  isPlay = true;
  // Thiết lập video
  adjustVideo = {
    speed: [
      { name: 'Chậm', ratio: 0.5, active: false },
      { name: 'Bình thường', ratio: 1, active: true },
      { name: 'Nhanh', ratio: 2, active: false },
      { name: 'Tuỳ chỉnh', ratio: null, active: false },
    ],
    playback: [
      { ratio: 3, active: false },
      { ratio: 5, active: true },
      { ratio: 10, active: false }
    ],
    playbackRate: [
      { name: '1080p', ratio: 'hd1080', active: false },
      { name: '720p', ratio: 'hd720', active: false },
      { name: '480p', ratio: 'large', active: false },
      { name: '360p', ratio: 'medium', active: false },
      { name: '240p', ratio: 'small', active: false },
      { name: 'Tự động', ratio: '', active: true },
    ]
  };
  // Âm lượng
  volume = {
    value: null,
    isMute: true,
    show: false
  };
  // Thanh slider
  progress = {
    start: null,
    value: null,
    end: null,
    min: 0,
    max: null,
    valueHoverSlider: null,
  };
  // Điểm dừng video [chưa dùng]
  breakPoint = [{ point: null, disable: false }, { point: null, disable: true }];
  limitRange = {
    isLimit: false,
    timeLimit: null
  };
  // Load xong video mới enable trình phát
  isLoadVideo = true;
  // Chế độ sáng / tối
  mode = {
    light: false,
    show: true
  };
  // disable nút qua bài và trở lại
  disableNext = false;
  disableBack = false;
  // Hiện khi ending video
  isShowReplay = false;
  // Nếu tua video thì không lưu lại duration
  isTrackingVideo = false;
  isFullScreen;
  isOnBottom = false;
  idContentCurrent = 0;
  durationSeek = {
    isSeek: false,
    time: null,
    timeSeekFirst: null
  };
  widthScreen = window.innerWidth;
  toogleMobile = window.innerWidth > 525 ? true : false;

  constructor(
    private cdr: ChangeDetectorRef,
    public dialog: MatDialog,
    private progressPlayerService: ProgressPlayerService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute
  ) {
    this.escEvent();
    this.idContentCurrent = this.route.snapshot.params.id;
    window.addEventListener('resize', (event) => this.handleResize(event));
  }

  ngOnInit(): void {
    this.spinner.show();
    this.init();
    // Check chế độ darkmode localStorage
    if (localStorage.getItem('light_mode') === 'true') {
      this.mode.light = true;
      setTimeout(() => { this.progressPlayerService.emitLightModeEvent(true); }, 0);
    }
    // Lắng nghe click content để ẩn disabled button qua bài và trở lại
    this.progressPlayerService.disableButtonNext$.pipe(takeUntil(this.destroy$)).subscribe(res => this.disableNext = res);
    this.progressPlayerService.disableButtonBack$.pipe(takeUntil(this.destroy$)).subscribe(res => this.disableBack = res);
    // Lắng nghe content là complete từ learner, true thì phát lại video từ đầu, không thì get duration từ localstorage
    this.progressPlayerService.returnDurationVideo$.pipe(takeUntil(this.destroy$)).subscribe(res => {
      if (this.player) {
        this.progress.value = 0;
        this.progress.start = 0;
        this.player.seekTo(0);
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    // Khi link video thay đổi thì gán lại và init lại youtube iframe
    if (changes.linkVideo.currentValue) {
      this.linkVideo = changes.linkVideo.currentValue;
      if (changes.linkVideo.currentValue !== changes.linkVideo.previousValue) {
        if (this.player) {
          setTimeout(() => { this.idContentCurrent = this.route.snapshot.params.id; }, 0);
          this.init();
          this.onSetLocalDuration(changes.linkVideo.previousValue);
          this.isLoadVideo = true;
          this.isShowReplay = false;
          this.isTrackingVideo = false;
          this.durationSeek.isSeek = false;
        }
      }
    }
  }

  /* 2. Initialize method for YT IFrame API */
  init(): void {
    this.spinner.show();
    // Nếu khung video đã tồn tại thì huỷ vào init mới
    if (window['YT']) {
      window['YT'] = null;
      window['onYouTubeIframeAPIReady'] = null;
      if (this.player) {
        // this.player.stopVideo();
        this.player.destroy();
      }
      this.init();
      this.isPlay = true;
      this.isShowReplay = false;
    }
    const tag = document.createElement('script');
    tag.src = 'https://www.youtube.com/iframe_api';
    const firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    /* 3. startVideo() will create an <iframe> (and YouTube player) after the API code downloads. */
    window['onYouTubeIframeAPIReady'] = () => this.startVideo();
  }

  // Khởi tạo trình phát video
  startVideo(): void {
    this.player = new window['YT'].Player('player-youtube', {
      videoId: this.linkVideo,
      playerVars: {
        autoplay: 0,
        modestbranding: 1,
        controls: 0,
        disablekb: 1,
        rel: 0,
        showinfo: 0,
        fs: 0,
        playsinline: 1,
        loop: 0,
        origin: window.location.host,
        host: `${window.location.protocol}//www.youtube.com`
      },
      events: {
        'onStateChange': this.onPlayerStateChange.bind(this),
        'onReady': this.onPlayerReady.bind(this),
      }
    });
    this.onTriggerVideo();
    this.cdr.detectChanges();
  }

  // Tracking video khi đang chạy
  onTriggerVideo(): void {
    this.cdr.detectChanges();
    const iframeWindow = this.player.getIframe().contentWindow;
    if (iframeWindow) {
      const lastTimeUpdate = 0;
      window.addEventListener('message', (event) => {
        if (event.source === iframeWindow) {
          const data = JSON.parse(event.data);
          if (
            data.event === 'infoDelivery' &&
            data.info &&
            data.info.currentTime
          ) {
            // emit thời gian video
            const time = Math.floor(data.info.currentTime);
            const self = this;
            self.durationSeek.timeSeekFirst = data.info.currentTime;
            self.progress.value = data.info.currentTime;
            // lastTimeUpdate = time;
            self.progress.start = self.formatTime(time);
            this.volume.value = this.player.getVolume();
            // Nếu chạy đến điểm limit video thì stop
            if (this.limitRange.timeLimit && self.progress.value === this.limitRange.timeLimit) {
              self.player.pauseVideo();
            }
            // Nếu gặp breakpoint thì dừng lại để ...
            const arrTime = self.breakPoint.map(x => x.point);
            if (arrTime.includes(self.progress.value)) {
              self.player.pauseVideo();
            }
            this.cdr.detectChanges();
          }
        }
        // Update dom
      });
    }
  }

  // Được gọi khi trình video đã sẵn sàng
  onPlayerReady(event): void {
    if (this.player.getPlayerState() === -1 || this.player.getPlayerState() === 5) {
      document.getElementById('poster').setAttribute('style', 'display:flex !important');
    }
    this.progress.max = event.target.getDuration();
    this.progress.end = this.formatTime(event.target.getDuration());
    this.volume.value = this.player.getVolume();
    // Kiểm tra xem người dùng đã xem video chưa, nếu có thì get lại thời gian của video từ localstorage
    if (localStorage.getItem('duration_videoyt')) {
      const value = JSON.parse(localStorage.getItem('duration_videoyt'));
      const filterArr = value.find(item => item.id === this.linkVideo);
      if (filterArr) {
        this.player.seekTo(+ filterArr.duration);
        this.progress.value = + filterArr.duration;
        if (!filterArr.isSeekTo) {
          filterArr.fake = filterArr.duration;
        }
        // Nếu thời gian hiện tại = thời gian của video thì hiển thị nút xem lại
        if (Math.round(this.progress.value) === Math.round(this.progress.max)) {
          this.isShowReplay = true;
        }
        localStorage.setItem('duration_videoyt', JSON.stringify(value));
      } else {
        this.player.seekTo(0);
        this.progress.value = 0;
        this.progress.start = 0;
      }
      this.player.pauseVideo();
    } else {
      if (this.player) {
        this.player.seekTo(0);
        this.player.pauseVideo();
        this.progress.value = 0;
        this.progress.start = 0;
      }
    }
    this.isLoadVideo = false;
    // document.getElementById('player-youtube').style.opacity = '1';
    this.spinner.hide();
  }

  // Chạy / dừng video
  onPlayVideo(status): void {
    // if (this.progress.value < this.limitRange.timeLimit) {
    if (this.player) {
      this.isPlay = !this.isPlay;
      if (status) {
        this.player.playVideo();
      } else {
        this.player.pauseVideo();
      }
    }
  }

  // replay video
  replayVideo(): void {
    this.isPlay = false;
    this.player.seekTo(0);
    this.player.playVideo();
    this.isShowReplay = false;
    this.isTrackingVideo = true;
  }

  // Kéo tới/lùi video
  onSlidePlayer(event: MatSliderChange): void {
    this.progress.start = this.formatTime(event.value);
    this.player.pauseVideo();
    this.isPlay = true;
    this.progress.value = event.value;
    this.player.seekTo(this.progress.value);
    this.cdr.detectChanges();
    this.isShowReplay = false;
    this.isTrackingVideo = true;
    // Khi có tua video thì lấy lại duration và link;
    this.onCheckSeekVideo(event, true);
  }

  onCheckSeekVideo(event, check?: boolean): void {
    if (check) {
      if (this.durationSeek.isSeek === false) {
        localStorage.setItem('fake_duration', this.durationSeek.timeSeekFirst);
      }
      this.durationSeek.isSeek = true;
    }
    if (localStorage.getItem('duration_videoyt')) {
      const value = JSON.parse(localStorage.getItem('duration_videoyt'));
      const filterArr = value.find(item => item.id === this.linkVideo);
      if (filterArr) {
        if (event.value <= filterArr.fake) {
          filterArr.isSeekTo = false;
        } else {
          filterArr.isSeekTo = true;
        }
      }
      localStorage.setItem('duration_videoyt', JSON.stringify(value));
    }
  }

  onChangeSlidePlayer(event): void {
    if (event.value) {
      this.player.playVideo();
    }
  }

  // Bật/tắt tiếng video
  onMuteVideo(isMute): void {
    if (this.volume.value !== 0) {
      this.volume.isMute = !this.volume.isMute;
      if (isMute) {
        // this.volume.value = 0;
        this.player.mute();
      } else {
        // this.volume.value = this.player.getVolume();
        this.player.unMute();
      }
    }
  }

  // Tăng giảm âm lượng video
  onSlideVolume(event: MatSliderChange): void {
    this.volume.value = event.value;
    this.player.setVolume(event.value);
    this.volume.isMute = event.value === 0 ? false : true;
  }

  // Tiến tới [seconds] giây
  onForwardVideo(): void {
    this.isTrackingVideo = true;
    const currentTime = this.player.getCurrentTime();
    const time = { value: currentTime + this.timeNextAndPrevious };
    this.player.seekTo(time.value);
    this.isShowReplay = false;
    this.onCheckSeekVideo(time, true);
  }

  // Lùi [seconds] giây
  onRewindVideo(): void {
    const currentTime = this.player.getCurrentTime();
    const time = { value: currentTime - this.timeNextAndPrevious };
    this.player.seekTo(time.value);
    this.isShowReplay = false;
    this.onCheckSeekVideo(time, false);
  }

  // Định dạng thời gian video
  formatTime(seconds: number): string {
    const h = Math.floor(seconds / 3600);
    const m = Math.floor((seconds % 3600) / 60);
    const s = seconds % 60;
    return [h, m > 9 ? m : h ? '0' + m : m || '0', s > 9 ? s : '0' + s]
      .filter(a => a)
      .join(':');
  }

  // Trạng thái của video {PAUSE, PLAYING, ENDED}
  onPlayerStateChange(event): void {
    switch (event.data) {
      case window['YT'].PlayerState.PLAYING:
        document.getElementById('poster').style.cssText = 'opacity: 0;display:flex !important';
        this.isPlay = false;
        this.isShowReplay = false;
        break;
      case window['YT'].PlayerState.PAUSED:
        this.isPlay = true;
        break;
      case window['YT'].PlayerState.ENDED:
        this.progress.start = this.progress.end;
        this.progress.value = this.progress.max;
        this.cdr.detectChanges();
        this.player.pauseVideo();
        this.isPlay = true;
        this.isShowReplay = true;
        document.getElementById('poster').style.cssText = 'opacity: 1;display:flex !important';
        this.onSetLocalDuration(this.linkVideo, true);
        if (!this.isTrackingVideo) {
          this.progressPlayerService.percentCompleteVideo.next({ link: this.linkVideo, type: 'youtube' });
        }
        break;
    }
    this.cdr.detectChanges();
  }

  // Sự kiện hover vào slider và hiển thị thời gian
  ngAfterViewInit(): void {
    document.getElementById('seekslider-yt').addEventListener('mousemove', (e) => {
      const value = + this.calcSliderPos(e).toFixed(2);
      const element = document.getElementById('hover_span_yt');
      element.style.display = 'block';
      const slider = document.getElementById('seekslider-yt');
      // Vị trí hiển thị trên trình phát
      element.style.left = `${(Math.round(value) / this.progress.max) * 100}%`;
      // Giá trị
      this.progress.valueHoverSlider = this.formatTime(+Math.round(value));
      slider.classList.add('hideChild');
    });
    document.getElementById('seekslider-yt').addEventListener('mouseleave', () => {
      document.getElementById('hover_span_yt').style.display = 'none';
    });
    this.cdr.detectChanges();
  }

  // Chế độ toàn màn hình với từng trình duyệt
  launchIntoFullscreen(element): void {
    this.isFullScreen = true;
    document.getElementById('poster').style.cssText = 'opacity: 0;display:flex !important';
    if (element.requestFullscreen) {
      element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
      element.mozRequestFullScreen();
    } else if (element.webkitRequestFullscreen) {
      element.webkitRequestFullscreen();
    } else if (element.msRequestFullscreen) {
      element.msRequestFullscreen();
    }
  }

  // Xem ở chế độ toàn màn hình
  onFullscreen(): void {
    const element = document.getElementById('content-video');
    if (element) {
      if (!this.isFullScreen) {
        this.launchIntoFullscreen(element);
        this.player.playVideo();
      } else {
        if (document.exitFullscreen) {
          document.exitFullscreen();
        }
      }
    }
  }

  // Chế độ sáng / sáng
  onSwitchMode(): void {
    this.mode.light = !this.mode.light;
    localStorage.setItem('light_mode', `${this.mode.light}`);
    this.progressPlayerService.emitLightModeEvent(this.mode.light);
  }

  // Vị trí hiện tại khi hover chuột vào của video
  calcSliderPos(e): number {
    return (e.offsetX / e.target.clientWidth) * this.progress.max;
  }

  // Chọn background
  onChooseBackground(): void {
    this.dialog.open(ImageDialogComponent, {
      width: '550px',
      height: 'auto',
      data: {}
    });
  }

  // Chất lượng video
  onQuanlity(quality: string, item): void {
    this.adjustVideo.playbackRate.forEach(value => value.active = false);
    item.active = true;
    this.player.setPlaybackQuality(quality);
  }

  // Tốc độ phát video
  onSpeedVideo(ratio: number, speed): void {
    if (ratio) {
      this.adjustVideo.speed.forEach(value => value.active = false);
      speed.active = true;
      this.player.setPlaybackRate(ratio);
    }
  }

  // Chế độ tuỳ chỉnh tốc độ phát
  onSlideSpeedVideo(event: MatSliderChange): void {
    this.adjustVideo.speed.forEach(value => value.active = false);
    if (event.value <= 12.5) {
      this.player.setPlaybackRate(0.25);
    } else if (event.value > 12.5 && event.value <= 25) {
      this.player.setPlaybackRate(0.5);
    } else if (event.value > 25 && event.value <= 50) {
      this.player.setPlaybackRate(1);
    } else if (event.value > 50 && event.value <= 75) {
      this.player.setPlaybackRate(1.5);
    } else {
      this.player.setPlaybackRate(2);
    }
  }

  // Tốc độ tua video
  onPlaybackRate(ratio: number, rate): void {
    this.timeNextAndPrevious = ratio;
    this.adjustVideo.playback.forEach(value => value.active = false);
    rate.active = true;
  }

  // Qua bài
  onBackContent(): void {
    if (!this.disableBack) {
      this.progressPlayerService.emitBackContentEvent(true);
    }
  }

  // Trở lại bài trước
  onNextContent(): void {
    if (!this.disableNext) {
      this.progressPlayerService.emitNextContentEvent(true);
    }
  }

  // Hover vào ẩn hiện nút volume
  onHoverVolume(isHover: boolean): void {
    this.volume.show = isHover ? true : false;
  }

  // Tính toán percent để lưu
  checkDurationPercent(clone, filter): number {
    if (this.handleTrackingPercentComplete(clone, filter) > 100) {
      return 100;
    } else {
      return this.handleTrackingPercentComplete(clone, filter);
    }
  }

  // Tính toán percent để lưu
  checkDurationPercentNew(durationLocal): number {
    if (this.isTrackingVideo) {
      if (durationLocal) {
        return (+durationLocal / this.progress.max) * 100;
      } else {
        return 0;
      }
    } else {
      return (this.progress.value / this.progress.max) * 100;
    }
  }

  // Lưu lại thời gian và thông tin của video
  onSetLocalDuration(link: string, check?: boolean): void {
    if (this.progress.value) {
      if (localStorage.getItem('duration_videoyt')) {
        const value = JSON.parse(localStorage.getItem('duration_videoyt'));
        const filterArr = value.find(item => item.id === link);
        const durationLocal = + localStorage.getItem('fake_duration');
        if (filterArr) {
          const cloneFilter = JSON.parse(JSON.stringify(filterArr));
          filterArr.duration = this.progress.value;
          // Nếu có tua và % hiện tại < % đã lưu local ở trước thì lấy ở local;
          filterArr.isSeekTo = (cloneFilter.isSeekTo !== undefined || cloneFilter.isSeekTo !== null)
            ? cloneFilter.isSeekTo : this.isTrackingVideo;
          filterArr.fake = cloneFilter.isSeekTo ? filterArr.fake : filterArr.duration;
          filterArr.percent = this.checkDurationPercent(cloneFilter, filterArr);
          this.checkCompleteContent(filterArr, check);
          this.savePercentLocal(filterArr.id, filterArr.percent, filterArr.contentID);
        } else {
          value.push({
            id: link,
            duration: this.progress.value,
            isSeekTo: this.isTrackingVideo,
            percent: this.checkDurationPercentNew(durationLocal),
            contentID: this.idContentCurrent,
            fake: + durationLocal
          });
          this.checkCompleteContent(value[value.length - 1], check);
          this.savePercentLocal(value[value.length - 1].id, value[value.length - 1].percent, value[value.length - 1].contentID);
        }
        localStorage.setItem('duration_videoyt', JSON.stringify(value));
        // localStorage.removeItem('fake_duration');
      } else {
        const durationLocal = localStorage.getItem('fake_duration');
        const objVideo = [
          {
            id: link,
            duration: this.progress.value,
            isSeekTo: this.isTrackingVideo,
            percent: this.checkDurationPercentNew(durationLocal),
            contentID: + this.idContentCurrent,
            fake: + durationLocal
          }
        ];
        this.checkCompleteContent(objVideo[0], check);
        this.savePercentLocal(objVideo[0].id, objVideo[0].percent, objVideo[0].contentID);
        localStorage.setItem('duration_videoyt', JSON.stringify(objVideo));
      }
    }
  }

  // Nếu thỏa mãn điều kiện thì lưu thành 100%
  checkCompleteContent(obj, ending: boolean): void {
    if (obj.percent && (obj.duration === obj.fake || obj.fake === 0) && !obj.isSeekTo && ending) {
      obj.percent = 100;
    }
  }

  savePercentLocal(ids: string, percent: number, idContent: number): void {
    this.progressPlayerService.percentCompleteVideoOut.next(
      { id: ids, type: 'youtube', percent, contentId: idContent }
    );
  }

  handleTrackingPercentComplete(obj, root): number {
    if (obj.isSeekTo) {
      // Nếu tua thì lấy ở lần trước;
      // Có 2 trường hợp là tua lớn hơn và tua nhỏ hơn
      return obj.percent;
    } else {
      const current = (this.progress.value / this.progress.max) * 100;
      if (current <= obj.percent) {
        return obj.percent;
      } else {
        return current;
      }
    }
  }

  escEvent(): void {
    document.addEventListener('fullscreenchange', (event) => {
      if (!document.fullscreenElement) {
        this.isOnBottom = false;
        this.isFullScreen = false;
        document.getElementById('poster').style.cssText = 'opacity: 0;display:flex !important';
      }
    });
  }

  mousemove(e: any): void {
    if (this.isOnBottom === false && this.isFullScreen === true) {
      this.isOnBottom = true;
      setTimeout(() => {
        if (this.isFullScreen === true) {
          this.isOnBottom = false;
        }
      }, 3000);
    }
  }

  mouseout(e): void {
    this.isOnBottom = false;
  }

  // Lấy toàn bộ thông số của player
  public getAllProperties(): {} {
    return {
      configPlayer: {
        type: 'youtube-player',
        play: this.isPlay,
        adjustVideo: this.adjustVideo,
        volume: this.volume,
        breakpoint: this.breakPoint
      },
      video: {
        link: this.linkVideo,
        value: this.progress,
        status: this.isLoadVideo,
      },
      iframeYoutube: this.player
    };
  }

  handleResize({ target }): void {
    this.widthScreen = target.innerWidth;
    if (this.widthScreen >= 525) {
      this.toogleMobile = true;
    }else{
      this.toogleMobile = false;
    }
  }

  toogleMenuMobile(): void {
    this.toogleMobile = !this.toogleMobile;
  }

  ngOnDestroy(): void {
    this.onSetLocalDuration(this.linkVideo);
    this.destroy$.complete();
    window['onYouTubeIframeAPIReady'] = null;
    if (this.player) {
      this.player.destroy();
    }
    localStorage.removeItem('fake_duration');
  }

}

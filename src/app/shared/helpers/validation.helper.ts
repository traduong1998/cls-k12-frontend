//-------------------------------------------------
//		Tri Viet Validation based on JavaScript language
//		Created by Nguyen Duc Thinh from Tri Viet International
//		thinhndse@gmail.com - www.vnsun.us
//		
// 
//
//		v1.5 - Nov. 18, 2015
//		
//-------------------------------------------------



export function isNullOrEmpty(str) {
    return (str == null || str.length === 0);
}

export function isNullOrWhiteSpace(str : string) {
    return (str == null || str.trim().length === 0);
}

export function isHTML(str) {
    var htmlEl = new RegExp(/<[a-zA-Z!/]+/g);
    return htmlEl.test(str);
}

export function isDecimal(num) {
    var decimalRgx = /^\d+$/i;
    return decimalRgx.test(num.trim());
}

export function isFloat(num) {
    num = num.replace(',', '.')
    var decimalRgx = /^\d+(|\.\d+)$/i;
    return decimalRgx.test(num.trim());
}

export function isUserName(name) {
    var usrRgx = /^([\w-]+(?:\.[\w-]+)*)$/i;
    return usrRgx.test(name.trim());
}

export function isLettersAndNumbers(str)
{
    var letterNumberRgx = /^[a-zA-Z0-9]+$/i;
    return letterNumberRgx.test(str.trim());
}

export function isFullName(name) {

}

export function isPhoneNum(phone) {
    var phoneRgx = /^[0-9]*$/i;
    return phoneRgx.test(phone.trim());
}

export function isPasswordMatched(password, confirmPassword) {
    return password === confirmPassword;
}

export function isEmail(email) {
    var emailRgx = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return emailRgx.test(email.trim());
}

export function isWebsiteDomain(domain) {
    var domainRgx = /^((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return domainRgx.test(domain.trim());
}

export function isDate(date) {
    var dateRgxDot = /(\d{2}|\d)(\.)(\d{2}|\d)(\.)(\d{4})$/i;
    var dateRgxCross = /(\d{2}|\d)(\/)(\d{2}|\d)(\/)(\d{4})$/i;
    var dateRgxLine = /(\d{2}|\d)(\-)(\d{2}|\d)(\-)(\d{4})$/i;
    date = date.trim();
    if (dateRgxDot.test(date) || dateRgxCross.test(date) || dateRgxLine.test(date)) {
        date = date.replace(/\//g, '-').replace(/\./g, '-');
        var day = parseInt(date.substr(0, date.indexOf('-')));
        var month = parseInt(date.substr(date.indexOf('-') + 1, date.lastIndexOf('-') - date.indexOf('-') - 1));
        var year = parseInt(date.substr(6));
        if ((year % 4 == 0 && month == 2 && day <= 29 && day >= 1) ||
			((month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) && day >= 1 && day <= 31) ||
			((month == 4 || month == 6 || month == 9 || month == 11) && day >= 1 && day <= 30) ||
			(month == 2 && day >= 1 && day <= 28)) {
            return true;
        }
    }
    return false;
}

export function isDecimalInput(element, event) {
    if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) {
        return true;
    }
    if (event.charCode < 48 || event.charCode > 57) {
        event.preventDefault();
    }
}

import { mergeMap, map, debounceTime, switchMap } from 'rxjs/operators';
import { combineLatest, forkJoin, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class WindowScrollService {
  scrollY = new BehaviorSubject(0);
  scrollY$ = this.scrollY.asObservable();

  maxScrollY = new BehaviorSubject(0);
  maxScrollY$ = this.maxScrollY.asObservable();

  maxHeight = new BehaviorSubject(0);
  maxHeight$ = this.maxHeight.asObservable();
  static forRoot: any;
  constructor() {
  }

  updateScrollY(scrollY: number, maxScrollY: number, maxHeight: number): void {
    this.scrollY.next(scrollY);
    this.maxScrollY.next(maxScrollY);
    this.maxHeight.next(maxHeight);
  }

  checkIsBottom(): Observable<boolean> {
    return combineLatest([
      this.scrollY$,
      this.maxScrollY$,
      this.maxHeight$
    ]).pipe(
      debounceTime(700),
      map(([scrollY, maxScrollY, maxHeight]) => {
        if (((scrollY - 40) + maxScrollY) === maxHeight) {
          return true;
        }
        else {
          return false;
        }
      })
    )
  }
}

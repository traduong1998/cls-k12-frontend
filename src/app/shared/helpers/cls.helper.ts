import { number } from "ngx-custom-validators/src/app/number/validator";
import { BehaviorSubject, Observable } from "rxjs";

export function hideloader() {

  // Setting display of spinner 
  // element to none 
  document.getElementById('loading')
    .style.display = 'none';
}
export function generateRandomUniqueInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}
export function countdown(timeleft: number, callback, element, destroy$?: Observable<boolean>) {
  var x = setInterval(function () {
    // Time calculations for days, hours, minutes and seconds
    var hours = Math.floor(timeleft / (1000 * 60 * 60)).toLocaleString('en-US', {
      minimumIntegerDigits: 2,
      useGrouping: false
    });
    let minutes = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60)).toLocaleString('en-US', {
      minimumIntegerDigits: 2,
      useGrouping: false
    });
    let seconds = Math.floor((timeleft % (1000 * 60)) / 1000).toLocaleString('en-US', {
      minimumIntegerDigits: 2,
      useGrouping: false
    });

    // Output the result in an element with id="demo"
    document.getElementById(element).innerHTML = `<p>` + hours + ": "
      + minutes + ": " + seconds + `</p>`;

    // If the count down is over, write some text 
    if (timeleft <= 0) {
      clearInterval(x);
      document.getElementById(element).innerHTML = `<p>00: 00: 00 </p>`;
      callback();
    }
    timeleft -= 1000;
  }, 1000);
  if (destroy$) {
    destroy$.subscribe((isDestroy) => {
      if (isDestroy) {
        clearInterval(x);
      }
    });
  }
}

export function countdownRealTime(timeleft: number, callback, element, destroy$?: Observable<boolean>) {
  var mark = new Date(Math.floor(new Date().getTime()/1000)*1000 + timeleft); // date in this format will go here to create date object at frontend
  var timeToShow;
  
  function timer(timeLeft) {
    timeToShow = mark.getTime() - Math.floor(new Date().getTime()/1000)*1000;
    if(timeToShow<0){
      clearInterval(x);
      callback();
      return;
    }
    // Time calculations for days, hours, minutes and seconds
    var hours = Math.floor(timeToShow / (1000 * 60 * 60)).toLocaleString('en-US', {
      minimumIntegerDigits: 2,
      useGrouping: false
    });
    let minutes = Math.floor((timeToShow % (1000 * 60 * 60)) / (1000 * 60)).toLocaleString('en-US', {
      minimumIntegerDigits: 2,
      useGrouping: false
    });
    let seconds = Math.floor((timeToShow % (1000 * 60)) / 1000).toLocaleString('en-US', {
      minimumIntegerDigits: 2,
      useGrouping: false
    });
  
    // Output the result in an element with id="demo"
    document.getElementById(element).innerHTML = `<p>` + hours + ": " + minutes + ": " + seconds + `</p>`;
  }
  
  var x = setInterval(timer, 1000);
  
  if (destroy$) {
    destroy$.subscribe((isDestroy) => {
      if (isDestroy) {
        clearInterval(x);
      }
    });
  }
}

export function compareTwoObject(objectOne: any, objectTwo: any) {
  return JSON.stringify(objectOne) == JSON.stringify(objectTwo);
}

/** Compare two dateTime
 * return true if date one > date true
 */
export function compareTwoDate(dateOne: Date, dateTwo: Date) {
  return dateOne.getTime() > dateTwo.getTime()
}

export function isValidLengthString(value: string, number: number) {
  if (value.length > number)
    return false;
  else return true;
}

export function isValidUserName(value: string) {
  debugger;
  let result = /^[a-zA-Z0-9_.-]+$/.exec(value);
  const valid = !!result;
  return valid;
}

class Factory {
    create<T>(type: (new () => T)): T {
        return new type();
    }
}
export class MappingValueHelper {

    static CopyListFromTo<TResponse>(target): TResponse[] {
        let result = [];
        target.map((val, index) => {
            let sch = {};
            Object.keys(val).forEach(key => {
                sch[key] = val[key];
            });

            result.push(sch)

        })

        return result;
    }
}

import { YoutubeItem, IFileItem } from "./IFileItem";
import { EFileType } from "../enum/EFileType";
import { EFeature } from "wcf-client-js/lib/Enum/EFeature";
import { IPlaylistData } from "./backend/IPlaylistData";
import { ESettingDefault } from "../enum/ESettingDefault";

/**
 * For file in playlist
 */
export interface IPlaylistItem {
    /**
     * it is provide from server, unique for file
     */
    id: string;
    name: string;
    openWith: EFeature;
    /**
     * poster of file
     */
    poster: string;
    /**
     * position in list
     */
    itemIndex: number;
    /**
     * url of file
     */
    fileUrl: string;
    fileType: EFileType;
}

/**
 * class handle playlist item
 */
export class PlaylistItem implements IPlaylistItem {
    fileType: EFileType;
    id: string;
    openWith: EFeature;
    itemIndex: number;
    name: string;
    private _poster: string;
    public set poster(value) {
        this._poster = value;
    }
    public get poster(): string {
        if (this._poster) {
            return this._poster;
        }
        else {
            if (this.fileType == EFileType.video) {
                return ESettingDefault.PosterVideo;
            }
            else if (this.fileType == EFileType.audio) {
                return ESettingDefault.PosterAudio;
            }
            else if (this.fileType == EFileType.document) {
                return ESettingDefault.PosterDocument;
            }
            else if (this.fileType == EFileType.image) {
                return ESettingDefault.PosterImage;
            }
            else {
                return '';
            }
        }
    }
    statusMsg: string;
    fileUrl: string;

    constructor() {

    }

    static FromData(data: IPlaylistData) {
        var file = new PlaylistItem();
        file.id = data.id;
        file.name = data.name;
        file.poster = data.poster;
        file.itemIndex = data.itemIndex;
        file.openWith = data.openWith;
        file.fileUrl = data.fileUrl;
        file.fileType = data.fileType;
        return file;
    }

    static FromYoutubeItem(obj: YoutubeItem) {
        var item = new YoutubePlaylistItem();
        item.fileUrl = obj.videoId;
        item.name = obj.name;
        item.poster = obj.poster;
        item.openWith = obj.openWith;
        item.fileType = obj.fileType;
        console.log(`FromYoutubeItem return `, item);
        return item;
    }
    static FromFileItem(obj: IFileItem, openWithDefault: EFeature) {
        var item = new FilePlaylistItem();
        item.id = obj.id;
        item.fileUrl = obj.fileUrl;
        item.name = obj.name;
        item.poster = obj.poster;
        item.openWith = openWithDefault;
        item.fileType = obj.fileType;
        console.log(`FromFileItem return `, item);
        return item;
    }

    static FromVideoFileItem(obj: IFileItem, openWithDefault: EFeature) {
        var item = new FilePlaylistItem();
        item.id = obj.id;
        item.fileUrl = obj.fileUrl;
        item.name = obj.name;
        item.poster = obj.poster;
        item.openWith = openWithDefault;
        item.fileType = obj.fileType;
        console.log(`FromFileItem return `, item);
        return item;
    }
}

export class YoutubePlaylistItem extends PlaylistItem {

}

export class FilePlaylistItem extends PlaylistItem {

}

import { EFileType } from "../enum/EFileType";
import { EFeature } from "wcf-client-js/lib/Enum/EFeature";
import { IManageFileData } from "./backend/IManageFileData";

/**
 * Type to open file 
 */
// export enum EPlayListType {
//     youtube = 'yt',
//     presentation = 'pst',
//     quiz = 'qiz'
// }



/**
 * For file in manage file
 */
export interface IFileItem {
    id: string;
    name: string;
    readonly poster: string;
    /**
     * status type
     */
    status: 'none' | 'uploading' | 'converting' | 'uploadFailed' | 'insertDBFailed';

    /**
     * option playlist type: 'document' | 'video' | 'image' | 'audio'
     */
    fileType: EFileType;
    /**
     * link gốc
     */
    fileUrl: string;
    /**
     * link đã convert
     */
    fileUrlConverted: string;

    /**
     * chỉ thị đã upload được bao nhiêu phần trăm rồi
     */
    percentUploaded: number;
}

/**
 * use for file in manage file
 */
export class FileItem implements IFileItem {
    // status: "none" | "uploading" | "converting" | "uploadFailed";
    id: string;
    name: string;
    poster: string;
    posterId: number;
    fileSize: number;
    dateCreated: Date;
    statusMsg: string;
    // fileUrl: string;
    openWith: EFeature;
    private _status;
    private _fileUrl;
    // private _fileType;
    percentUploaded: number;
    fileUrlConverted: string;
    constructor() {
        this._status = 'none';
    }
    // public set fileType(fileType) {
    //     this._fileType = fileType;
    // }
    public get fileType(): EFileType {
        if (!this._fileUrl) {
            return EFileType.none;
        }

        var parts = this._fileUrl.split('.');
        const ext = parts[parts.length - 1].toLowerCase();

        if (['doc', 'docx', 'ppt', 'pptx', 'xls', 'xlsx', 'pdf'].find(x => x == ext)) {
            return EFileType.document;
        }
        else if (['png', 'jpg', 'jpeg', 'gif'].find(x => x == ext)) {
            return EFileType.image;
        }
        else if (['mp4', 'mkv', 'webm', 'm3u8'].find(x => x == ext)) {
            return EFileType.video;
        }
        else if (['mp3'].find(x => x == ext)) {
            return EFileType.audio;
        }
        else {
            return EFileType.none;
        }
        // return this._fileType;
    };

    public set fileUrl(fileUrl) {
        this._fileUrl = fileUrl;
    }
    public get fileUrl(): string {
        return this._fileUrl;
    }
    public get status(): "none" | "uploading" | "converting" | "uploadFailed" | "insertDBFailed" {
        return this._status;
    }

    public set status(status) {
        this._status = status;
    }

    public get isError() {
        return ['uploadFailed', 'insertDBFailed'].indexOf(this._status) != -1;
    }

    static From(data: IManageFileData) {
        var file = new FileItem();
        file.id = data.id;
        file.name = data.name;
        file.dateCreated = data.dateCreated;
        file.poster = data.poster;
        file.fileUrl = data.fileUrl;
        file.fileUrlConverted = data.fileUrlConverted;
        return file;
    }
}

/**
 * for result from Youtube
 */
export class YoutubeItem {
    index: number;
    poster: string;
    id: string;
    name: string;
    videoId: string;
    statusMsg: string;
    openWith: EFeature = EFeature.youtube;
    fileType: EFileType = EFileType.youtube;
    constructor() {

    }
}

/**
 *
 */
// export class PresentationPlaylist {
//     index: number;
//     dateCreated: Date;
//     dateModified: Date;
//     fileSize: number;
//     fileType: "document" | "video" | "image" | "audio";
//     poster: string;
//     id: string;
//     name: string;
//     status: 'none' | 'uploading' | 'converting' | 'uploadFailed';
//     fileUrl: string;
//     size: number;
//     statusMsg: string;
//     constructor() {
//     }
// }

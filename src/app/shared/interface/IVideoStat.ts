export class IVideoStat {
    id: string;
    connection?: 'Good' | 'Normal' | 'Bad' | 'Lost' | 'None' = 'None';
    bytesSend?: number;
    bytesReceive?: number;
    bytesSent?: number;
    bytesReceived?: number;
    packetsLost: number;
    packetsSent?: number;
    packetsReceived?: number;
    // Framerate: number;
    // VideoWidth: number;
    // VideoHeight: number;
}

export class VideoStream {
    id: string;//publisher id | subscriber id


}
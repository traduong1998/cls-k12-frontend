export interface IPaginationData<T> {
    pageLists: T[],
    totalRecord: number
}

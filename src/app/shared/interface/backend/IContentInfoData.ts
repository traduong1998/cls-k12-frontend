/**
 * manage file data from backend
 */
export interface IContentInfoData {
    id;
    name;
}
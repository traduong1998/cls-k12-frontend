import { ERoomState } from "../../enum/ERoomState";

/**
 * room data from backend
 */
export interface IRoomData {
    roomId: string;
    roomName: string;
    roomAddress: string;
    state: ERoomState;
    startTime: string;
    endCanTime: string;//thoi gian du kien ket thuc
    unitId: string;
    ownerId: string;
    ownerName: string;
    dateCreated: string;
    estimatedEndTime;

    /**
     * Session từ cloud
     */
    sessionId: string;
}
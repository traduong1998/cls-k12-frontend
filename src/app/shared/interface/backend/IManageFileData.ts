/**
 * manage file data from backend
 */
export interface IManageFileData {
    id;
    name;
    dateCreated;
    poster;
    fileUrl;
    fileUrlConverted;
    fileType;
    courseId;
    courseName;
}
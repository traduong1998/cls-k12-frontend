export interface IUserData {
    userId;
    userName;
    avatar;
    email;
    gender;
    phone;
    fullName;
}

export interface IGenerateTokenResponse {
    accessToken: string;
    userInfo: IUserData;
}
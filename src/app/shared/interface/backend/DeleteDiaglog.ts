export interface DeleteDialog{
    message: string;
    constraint:string;
    name:string;
}
/**
 * manage file data from backend
 */
export interface ICourseInfoData {
    id;
    name;
}
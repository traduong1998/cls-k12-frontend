export interface IChatMessageData {
    maChat?: string;
    userId: number;
    userName: string;
    msg: string;
    pathFile: string;
    type?: string;
    maTepTin?: number;
    avatar?: string;
}
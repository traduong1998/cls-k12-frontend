/**
 * room data from backend
 */
export interface IPlaylistData {
    id;
    name;
    dateCreated;
    poster;
    fileUrl;
    fileType;
    itemIndex;
    openWith;
}
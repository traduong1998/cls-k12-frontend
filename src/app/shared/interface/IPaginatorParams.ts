export interface IPaginatorParams {
    pageNumber: number;
    pageSize: number;
    keyWord: string;
}
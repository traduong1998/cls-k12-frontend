export interface IDeviceSelected {
    micDeviceSelected: MediaDeviceInfo,
    webcamDeviceSelected: MediaDeviceInfo
}
export interface IVideoResolution {
    HD: {
        height: 720
    },
    Mini: {
        width: 198,
        height: 108
    }
}
export class VideoResolution {
    _quality: 'HD' | 'Mini';
    public set quality(value: 'HD' | 'Mini') {
        if (value == 'HD') {
            this._height = 720;
        }
        else if (value == 'Mini') {
            this._height = 108;
        }
    }

    private _width?: number;
    private _height?: number;
    constructor(quality: 'HD' | 'Mini') {
        this._quality = quality;
    }
}
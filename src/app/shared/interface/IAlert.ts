export interface IAlert {
    type: 'success' | 'error' | 'warning';
    summary: string;
    detail?: string;
}
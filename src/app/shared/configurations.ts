import { Component } from '@angular/core';
/** Giới hạn file  */
export const LIMIT_FILE_VIDEO: number = 500;
export const LIMIT_FILE_AUDIO: number = 100;
export const LIMIT_FILE_IMAGE: number = 50;
export const LIMIT_FILE_DOCUMENT: number = 100;
export const LIMIT_FILE_SCORM: number = 300;
export const MESSAGE_LIMIT_FILE_VIDEO = `Tệp vượt quá ${LIMIT_FILE_VIDEO}MB. Xin thử lại `;
export const MESSAGE_LIMIT_FILE_AUDIO = `Tệp vượt quá ${LIMIT_FILE_AUDIO}MB. Xin thử lại `;
export const MESSAGE_LIMIT_FILE_IMAGE = `Tệp vượt quá ${LIMIT_FILE_IMAGE}MB. Xin thử lại `;
export const MESSAGE_LIMIT_FILE_DOCUMENT = `Tệp vượt quá ${LIMIT_FILE_DOCUMENT}MB. Xin thử lại `;
export const MESSAGE_LIMIT_FILE_SCORM = `Tệp vượt quá ${LIMIT_FILE_SCORM}MB. Xin thử lại `;
export const AUDIO: string[] = ['mp3', 'aac', 'ogg', 'wav', 'wave', 'wav', 'wma', 'ra', 'aif', 'aiff', 'flac', 'm4a', 'ape'];
export const VIDEO: string[] = ['mp4', 'webm', 'ogg', 'ogv', 'avi', 'mpeg', 'mpg', 'mov', 'wmv', '3gp', 'flv', 'rmvb', 'm4v', 'mkv'];
export const DOCUMENT: string[] = ["ppt", "pptx", "doc", "docx", "xls", "xlsx", "pdf"];
export const SCORM: string[] = ["zip"];



type Constructor<T = {}> = new (...args: any[]) => T;

export function Deprecated(oldSelector: string) { // This is a decorator factory
    return <T extends Constructor>(Base: T) => {
        return class Deprecated extends Base {
            selectors = [];
            constructor(...args: any[]) {
                super(...args);
                const selector = new Component((Deprecated as any).__annotations__[0]).selector;
                this.selectors = selector.split(', ');
                this.selectors = this.selectors.filter(selector => selector !== oldSelector);
                console.warn('The selector "' + oldSelector + '" is going to be deprecated. Please use one of these selectors [ ' + this.selectors.toString() + ' ]');
            }
        };
    };
}

export const TIMEOUT_SCROLL = 300;

export const SETTING_LOG = 1000 * 60;
/** Cấu hình chung */
export class Configuration {
    baseUrl = '';

    /** Lấy message limit file */
    public static getLimitFileMessage(file: File): ConfigurationResult {
        var mimeType = file.type;
        debugger;
        if (mimeType.match(/image\/*/)) {
            return this.checkLimitFile(file.size, LIMIT_FILE_IMAGE, MESSAGE_LIMIT_FILE_IMAGE)
        }

        if (mimeType.match(/audio\/*/)) {
            return this.checkLimitFile(file.size, LIMIT_FILE_AUDIO, MESSAGE_LIMIT_FILE_AUDIO)
        }

        if (mimeType.match(/video\/*/)) {
            return this.checkLimitFile(file.size, LIMIT_FILE_VIDEO, MESSAGE_LIMIT_FILE_VIDEO)
        }

        if (mimeType.match(/application\/*/)) {
            var type = file.name.substring(file.name.lastIndexOf(".") + 1).toLowerCase();
            if (SCORM.includes(type)) { return this.checkLimitFile(file.size, LIMIT_FILE_SCORM, MESSAGE_LIMIT_FILE_SCORM) }
            else { return this.checkLimitFile(file.size, LIMIT_FILE_DOCUMENT, MESSAGE_LIMIT_FILE_DOCUMENT) }

        }
    }

    private static checkLimitFile(fileSize: number, limitSize: number, message?: string): ConfigurationResult {
        // check MB
        if ((fileSize / 1024 / 1024) > limitSize) {
            return new ConfigurationResult(false, message);
        } else {
            return new ConfigurationResult(true, null);
        }
    }

    public static getZoomLink(baseUrl: string, lessonId: number, contentId: number, zoomMeetingId: number) {
        return `${baseUrl}/online-teaching/${lessonId}/contents/${contentId}/zoomMeetings/${zoomMeetingId}`
    }
    public static BASE_URL = `https://sgdtglmsdemo.cls.vn:5556`
    public static BUTTONSETTINGTEXT = `Cài đặt tài khoản`;
    public static BUTTONLINKTEXT = `Cập nhật liên kết`;
    public static MESSAGE_EXIT_CONFIRM = `Dữ liệu bạn vừa mới cập nhật chắc chắn sẽ không được lưu khi bạn thoát ra ngoài.\r\n
     Bạn có chắc chắn muốn thoát hay không? `;
    public static MESSAGE_INFOR_IMAGE = `Định dạng : .PNG, .JPG, .GIF , .JPEG\r\n Dung lượng tối đa : ${LIMIT_FILE_IMAGE}MB.`;
    public static MESSAGE_INFOR_VIDEO = `Định dạng : .MP4, .MOV, .OGG, .OGV, .MPEG, .WMV, 3GP .AVI \r\n Dung lượng tối đa : ${LIMIT_FILE_VIDEO}MB. `;
    public static MESSAGE_INFOR_SOUND = `Định dạng : .MP3, .AAC, .OGG, .WAV, .AIFF, .FLAC, .M4A \r\n Dung lượng tối đa : ${LIMIT_FILE_AUDIO}MB. `;
    public static MESSAGE_INFOR_DOCCUMENT = `Định dạng : .PPT, .PPTX, .DOC, .DOCX, .XLS, .XLSX, .PDF \r\n Dung lượng tối đa : ${LIMIT_FILE_DOCUMENT}MB. `;
    public static MESSAGE_INFOR_REFERENCE = `Định dạng : .DOC, .DOCX, .XLS, .XLSX, .PDF \r\n Dung lượng tối đa : ${LIMIT_FILE_DOCUMENT}MB. `;
    public static MESSAGE_INFOR_SCORM = `Định dạng : .ZIP \r\n Dung lượng tối đa : ${LIMIT_FILE_SCORM}MB. `;
    public static MESSAGE_CHANGE_ZOOM_ROOM_DETAIL = `Vui lòng nhấn nút <span class="text-primary-color">${Configuration.BUTTONLINKTEXT}</span> để cập nhật tài khoản liên kết mới nhất`;
    public static MESSAGE_DONT_ZOOM_ROOM_DETAIL = `Vui lòng nhấn nút <span class="text-primary-color">${Configuration.BUTTONSETTINGTEXT}</span> để cài đặt tài khoản`;
    public static MESSAGE_FORBIDEN_DETAIL = `Người tạo bài giảng vui lòng cập nhật tài khoản liên kết Zoom`;
    public static FILE_NAME_CREATE_USERS = `them-nguoi-dung-tu-tep-tin.xlsx`;
    public static FILE_NAME_CREATE_DIVISIONS = `them-phong-tu-tep-tin.xlsx`;
    public static FILE_NAME_CREATE_SCHOOLS = `them-truong-tu-tep-tin-`;

}

export class ConfigurationResult {
    status: boolean;
    message?: string
    /**
     * @status : trạng thái check
     * @message : message sau khi check
     */
    constructor(status: boolean, message?: string) {
        this.status = status;
        this.message = message;
    }
}
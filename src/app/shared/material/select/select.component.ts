import { Option } from './../../interface/Option';
import { FormControl } from '@angular/forms';
import { AfterViewInit, Component, Input, OnInit, ViewChild, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ReplaySubject, Subject } from 'rxjs';
import { MatSelect } from '@angular/material/select';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() options: Option[] = [];
  @Input() placeHolder: string;
  @Input() isDisable: boolean = false;
  @Input() defaultOption: Option;
  @Output() outputOptionSelected = new EventEmitter<Option>();
  public selectCtrl: FormControl = new FormControl();
  public selectFilterCtrl: FormControl = new FormControl();
  /** list of options filtered by search keyword */
  public filteredOptions: ReplaySubject<Option[]> = new ReplaySubject<Option[]>(1);
  public optionSelected: Option;
  public selected: number;

  keyWord: string = "";

  public defaultOptionIsValid: boolean;
  @ViewChild('singleSelect', { static: true }) singleSelect: MatSelect;

  /* Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();
  constructor() {
    this.optionSelected = null;
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.options) {
      console.log("this change Value", changes.options);
      this.optionSelected = null;
      this.filteredOptions.next(this.options);
    }

  }

  ngOnInit() {
    if (this.defaultOption != undefined) {
      let option = this.options.find(x => x.id == this.defaultOption.id);
      if (option != undefined) {
        this.defaultOptionIsValid = true;
        this.optionSelected = option;
      }
    }
    // load the initial option list
    this.filteredOptions.next(this.options.slice());
  }

  ngAfterViewInit() {

  }

  ngOnDestroy() {

  }

  /**
   * Sets the initial value after the filteredOptions are loaded initially
   */
  protected filterOptions() {
    if (!this.options) {
      return;
    }
    // get the search keyword
    let search = this.keyWord;
    if (!search) {
      this.filteredOptions.next(this.options.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the options
    this.filteredOptions.next(
      this.options.filter(option => option.name.toLowerCase().indexOf(search) > -1)
    );
  }

  public onChangeOption(id: any) {
    let optionSelected = null;
    optionSelected = this.options.find(o => o.id == id);
    this.optionSelected = optionSelected;
    this.outputOptionSelected.emit(optionSelected);
  }
}
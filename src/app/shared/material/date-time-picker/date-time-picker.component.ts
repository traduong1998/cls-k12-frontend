import { Component, EventEmitter, Input, NgZone, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { ThemePalette } from '@angular/material/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
@Component({
  selector: 'app-date-time-picker',
  templateUrl: './date-time-picker.component.html',
  styleUrls: ['./date-time-picker.component.scss']
})
export class DateTimePickerComponent implements OnInit {
  @ViewChild('picker') picker: any;

  @Input() dateString: string;
  @Input() minDateInput: moment.Moment;
  @Input() maxDateInput: moment.Moment;
  @Output() dateTimeValue = new EventEmitter<Date>();

  public date: Date;
  public disabled = false;
  public showSpinners = true;
  public showSeconds = false;
  public touchUi = false;
  public enableMeridian = false;
  public minDate: moment.Moment;
  public maxDate: moment.Moment;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 1;
  public color: ThemePalette = 'primary';

  public formGroup = new FormGroup({
    date: new FormControl(null, [Validators.required]),
  })
  
  public options = [
    { value: true, label: 'True' },
    { value: false, label: 'False' }
  ];

  public listColors = ['primary', 'accent', 'warn'];

  public stepHours = [1, 2, 3, 4, 5];
  public stepMinutes = [1, 5, 10, 15, 20, 25];
  public stepSeconds = [1, 5, 10, 15, 20, 25];
  constructor() { }

  ngOnInit(): void {
    //this.date = new Date();
    if (this.minDateInput != null) {
      this.minDate = this.minDateInput;
    }
    if (this.maxDateInput != null) {
      this.maxDate = this.maxDateInput;
    }
  }
  closePicker() {
    this.picker.cancel();
  }

  changeDate(date: MatDatepickerInputEvent<Date>) {
    this.dateTimeValue.emit(date.value);
  }
}

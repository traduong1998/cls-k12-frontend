import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, NgControl, ReactiveFormsModule } from '@angular/forms';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon'
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { DomSanitizer } from '@angular/platform-browser';
import { MatBadgeModule } from '@angular/material/badge';
import { SelectComponent } from './select/select.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

import { NgxDropzoneModule } from 'ngx-dropzone';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { DateTimePickerComponent } from './date-time-picker/date-time-picker.component';
import { NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { VerifyAccountDialogComponent } from '../dialog/verify-account-dialog/verify-account-dialog.component';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatButtonModule,
    MatCardModule,
    MatTooltipModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatInputModule,
    MatBadgeModule,
    MatSelectModule,
    CommonModule,
    NgxMatSelectSearchModule,
    MatIconModule,
    MatFormFieldModule,
    NgxDropzoneModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
    MatDatepickerModule,
    MatDividerModule
  ],
  exports: [
    CommonModule,
    MatTableModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCheckboxModule,
    MatIconModule,
    MatCardModule,
    MatTooltipModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatInputModule,
    MatBadgeModule,
    SelectComponent,
    ReactiveFormsModule,
    MatFormFieldModule,
    NgxMatSelectSearchModule,
    DateTimePickerComponent,
    MatDividerModule
  ],

  declarations: [SelectComponent, ConfirmationDialogComponent, VerifyAccountDialogComponent, DateTimePickerComponent]
})
export class MaterialModule {

  constructor(private matIconRegistry: MatIconRegistry, private domSanitizer: DomSanitizer) {
    this.addIconDashboard();

    this.addIconSubject();

    // icon lọc bài giảng
    this.matIconRegistry.addSvgIcon(
      "delete-icon",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/delete-icon.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "dangerous-icon",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/dangerous-icon.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "success-snackbar",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/success-snackbar.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "error-snackbar",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/error-snackbar.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "grade-class-table-icon",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/grade-class-table-icon.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "school-table-icon",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/school-table-icon.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "warnning-black-icon",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/warnning-black-icon.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-icon",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-icon.svg")
    );


    this.matIconRegistry.addSvgIcon(
      "location-icon",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/location-icon.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "mail-icon",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/mail-icon.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "phone-icon",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/phone-icon.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "dot",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/dot.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "detail-report-lesson",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/detail-report-lesson.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "add-circle-outline",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/add_circle_outline.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "star",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/star.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "star-no-background",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/star-no-background.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "camera",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/camera.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "grade-icon",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/grade-icon.svg")
    );

    // icon lọc bài giảng

    this.matIconRegistry.addSvgIcon(
      "orderby",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/orderby.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "filter",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/filter.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "filter-white",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/filter-white.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "filter-black",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/filter-black.svg")
    );

    // icon thêm bài giảng
    this.matIconRegistry.addSvgIcon(
      "add-white",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/add-white.svg")
    );

    // icon thêm bài giảng
    this.matIconRegistry.addSvgIcon(
      "add",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/add.svg")
    );

    // icon nội dung khi hoàn thành
    this.matIconRegistry.addSvgIcon(
      "add-complete",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/add-complete.svg")
    );
    // icon edit
    this.matIconRegistry.addSvgIcon(
      "edit",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/edit.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "edit-complete",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/edit-complete.svg")
    );

    // icon duyệt bài giảng
    this.matIconRegistry.addSvgIcon(
      "approval",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/approval.svg")
    );

    // icon xóa
    this.matIconRegistry.addSvgIcon(
      "delete",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/delete.svg")
    );

    // icon thông tin
    this.matIconRegistry.addSvgIcon(
      "edit-black",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/edit-black.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "copy-black",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/copy-black.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "edit-complete",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/edit-complete.svg")
    );

    // icon qua bước
    this.matIconRegistry.addSvgIcon(
      "next-completed",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/next-completed.svg")
    );

    // icon qua bước
    this.matIconRegistry.addSvgIcon(
      "apporoval",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/apporoval.svg")
    );

    // icon thiết lập
    this.matIconRegistry.addSvgIcon(
      "setting",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/setting.svg")
    );

    // icon thiết lập khi hoàn thành
    this.matIconRegistry.addSvgIcon(
      "setting-completed",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/setting-completed.svg")
    );
    // icon hoàn thành
    this.matIconRegistry.addSvgIcon(
      "finish",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/finish.svg")
    );

    // icon hoàn thành
    this.matIconRegistry.addSvgIcon(
      "finish-completed",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/finish-completed.svg")
    );


    // icon nội dung
    this.matIconRegistry.addSvgIcon(
      "content",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/content.svg")
    );
    // icon nội dung khi hoàn thành
    this.matIconRegistry.addSvgIcon(
      "content-completed",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/content-completed.svg")
    );
    // icon xóa boder black
    this.matIconRegistry.addSvgIcon(
      "deleteboderblack",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/deleteboderblack.svg")
    );

    // icon search
    this.matIconRegistry.addSvgIcon(
      "search",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/search.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "approvedListLesson",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/approvedListLesson.svg")
    );

    // icon display as student
    this.matIconRegistry.addSvgIcon(
      "graduate",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/graduate.svg")
    );

    // icon display as student
    this.matIconRegistry.addSvgIcon(
      "sort",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/sort.svg")
    );

    // icon download
    this.matIconRegistry.addSvgIcon(
      "download",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/download.svg")
    );

    // icon download
    this.matIconRegistry.addSvgIcon(
      "uploadFile",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/uploadFile.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "info",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/info.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "next",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/next.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "next-complete",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/next-complete.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "edit-complete",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/edit-complete.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "setting",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/setting.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "setting-complete",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/setting-complete.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "calendar",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/calendar.svg")
    );

    // icon trash
    this.matIconRegistry.addSvgIcon(
      "trash-white",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/trash-white.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "trash-black",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/trash-black.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "approval",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/approval.svg")
    );
    // icon download
    this.matIconRegistry.addSvgIcon(
      "upload",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/upload.svg")
    );
    // icon display as student
    this.matIconRegistry.addSvgIcon(
      "move-back",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/move-back.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "deleteboderblack",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/deleteboderblack.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "sort",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/sort.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "sort",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/sort.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "content-completed",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/content-completed.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "graduate",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/graduate.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "move-to",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/move-to.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "release-sermons",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/release-sermons.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "info-baned",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/info-baned.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "add-file-document",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/add-file-document.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "trash",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/trash.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "downloadFile",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/downloadFile.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "addFromFile",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/addFromFile.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "pendingPublish",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/pendingPublish.svg")
    );


    this.matIconRegistry.addSvgIcon(
      "person-register",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/person-register.svg")
    );

    //horizontal-header-detail
    this.matIconRegistry.addSvgIcon(
      "horizontal-header-detail-information",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/horizontal-header-detail-information.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "horizontal-header-detail-learninghistory",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/horizontal-header-detail-learninghistory.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "horizontal-header-detail-lecturer",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/horizontal-header-detail-lecturer.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "horizontal-header-detail-logout",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/horizontal-header-detail-logout.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "horizontal-header-detail-manage",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/horizontal-header-detail-manage.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "horizontal-header-detail-mylesson",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/horizontal-header-detail-mylesson.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "horizontal-header-detail-student",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/horizontal-header-detail-student.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "subject-congnghe",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-congnghe.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-dialy",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-dialy.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-gdcd",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-gdcd.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-hoahoc",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-hoahoc.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-congnghe",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-congnghe.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-lichsu",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-lichsu.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-nguvan",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-nguvan.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-sinhhoc",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-sinhhoc.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-tienganh",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-tienganh.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-tinhoc",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-tinhoc.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-toan",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-toan.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-vatly",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-vatly.svg")
    );




    this.matIconRegistry.addSvgIcon(
      "subject-toan-actived",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-toan-actived.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-congnghe-actived",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-congnghe-actived.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-dialy-actived",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-dialy-actived.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-gdcd-actived",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-gdcd-actived.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-hoahoc-actived",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-hoahoc-actived.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-lichsu-actived",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-lichsu-actived.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-nguvan-actived",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-nguvan-actived.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-sinhhoc-actived",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-sinhhoc-actived.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-tienganh-actived",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-tienganh-actived.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-tinhoc-actived",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-tinhoc-actived.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "subject-vatly-actived",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/subject-vatly-actived.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "finish-baned",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/finish-baned.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "setting-baned",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/setting-baned.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "icon-learn-time-list-lesson",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/icon-learn-time-list-lesson.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "icon-publication-date-list-lesson",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/icon-publication-date-list-lesson.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "icon-publication-date-flag-list-lesson",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/icon-publication-date-flag-list-lesson.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "hourglass",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/hourglass.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "next-circle",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/next-circle.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "back-circle",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/back-circle.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "submit-circle",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/submit-circle.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "next-circle-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/next-circle-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "content-finish",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/content-finish.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "back-content-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/back-content-active.svg")
    );
    // icon full màn hình
    this.matIconRegistry.addSvgIcon(
      "fullscreen",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/fullscreen.svg")
    );
    // icon full màn hình
    this.matIconRegistry.addSvgIcon(
      "upload-lesson",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/upload-lesson.svg")
    );
    // icon full màn hình
    this.matIconRegistry.addSvgIcon(
      "accordion",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/accordion.svg")
    );
    // icon full màn hình
    this.matIconRegistry.addSvgIcon(
      "accordion-show",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/accordion-show.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "assignment-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/assignment-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "organize",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/organize.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "organize-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/organize-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "reset",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/reset.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "flag",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/flag.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "eye",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/eye.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "save",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/save.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "save-white",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/save-white.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "info-black",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/info-black.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "exel",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/exel.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "pdf",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/pdf.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "word",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/word.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "download-green",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/download-green.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "success-green",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/success-green.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "fail-red",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/fail-red.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "return",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/return-icon.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "check-outline",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/check-outline-icon.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "eye-outline",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/eye-outline.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "connected-student",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/connected-student.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "disconnected-student",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/disconnected-student.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "not-connected-student",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/not-connected-student.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "violation-warning-student",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/violation-warning-student.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "messenger-icon",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/messenger-icon.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "exam-rule-mobile",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/exam-rule-mobile.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "exit",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/exit.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "reference-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/reference-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "question-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/question-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "chat-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/chat-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "list-tab-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/list-tab-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "list-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/list-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "list-un-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/list-un-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "chat-un-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/chat-un-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "chat-tab-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/chat-tab-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "answer-question-tab-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/answer-question-tab-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "reference-tab-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/reference-tab-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "question-un-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/question-un-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "reference-un-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/reference-un-active.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "total-question",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/total-question.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "total-score",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/total-score.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "teach-online",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/teach-online.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "eyes",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/eyes.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "reply",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/reply.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "view-result",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/view-result.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "view-result-active",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/view-result-active.svg")
    );

    // icon ngân hàng câu hỏi
    this.matIconRegistry.addSvgIcon(
      "setting-search",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/setting_search.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "add-circle-white",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/add-circle-white.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "watch-question",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/watch_question.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "sent-question",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/sent_question.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "sent_approved_question",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/sent_approved_question.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "export_question",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/export_question.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "add_from_file",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/add_from_file.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "download_example_file",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/download_example_file.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "download-template",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/download-template.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "add-school-from-file",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/add-school-from-file.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "check-do-exam",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/check-do-exam.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "review-check-do-exam",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/review-check-do-exam.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "icon-menu-quan-tri-he-thong",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/icon-menu-quan-tri-he-thong.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "icon-detail-learning-path",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/icon-detail-learning-path.svg")
    );

    // icon ngân hàng câu hỏi
    this.matIconRegistry.addSvgIcon(
      "approve_question",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/approve_question.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "refuse_question",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/refuse_question.svg")
    );

    //icon lớp
    this.matIconRegistry.addSvgIcon(
      "remove-student-class",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/remove-student-class.svg")
    );

  }

  private addIconDashboard() {
    this.matIconRegistry.addSvgIcon(
      "co-cau-to-chuc",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/co-cau-to-chuc.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "cai-dat-he-thong",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/cai-dat-he-thong.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "bao-cao-thong-ke",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/bao-cao-thong-ke.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "quan-ly-chu-de",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/quan-ly-chu-de.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "quan-ly-day-hoc",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/quan-ly-day-hoc.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "quan-ly-to-chuc-thi",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/quan-ly-to-chuc-thi.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "question-bank",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/question-bank.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "quy-trinh-dao-tao",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/quy-trinh-dao-tao.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "upload_file_white",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/upload_file_white.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "trash_white",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/trash_white.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "analytic_white",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/analytic_white.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "list-exam-assign-school",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/list-exam-assign-school.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "learner-course",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/learner-course.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "user-info",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/user-info.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "learning-path",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/learning-path.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "history-study",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/history-study.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "learner-exam",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/learner-exam.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "check-primary",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/check-primary.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "book",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/book.svg")
    );
  }

  private addIconSubject() {
    this.matIconRegistry.addSvgIcon(
      "math",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/Math.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "art",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/art.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "biological",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/biological.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "chemistry",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/Chemistry.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "civiceducation",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/civiceducation.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "elective",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/elective.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "English",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/English.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "Geography",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/Geography.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "History",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/History.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "InformationTechnology",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/InformationTechnology.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "Literature",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/Literature.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "morality",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/morality.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "music",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/music.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "NaturalSciences",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/NaturalSciences.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "PhysicalEducation",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/PhysicalEducation.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "physical",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/physical.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "technology",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/technology.svg")
    );

    this.matIconRegistry.addSvgIcon(
      "economic",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon-subject/economic.svg")
    );

    //document-content
    this.matIconRegistry.addSvgIcon(
      "next-page-doc",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/next-page-doc.svg")
    );
    this.matIconRegistry.addSvgIcon(
      "prev-page-doc",
      this.domSanitizer.bypassSecurityTrustResourceUrl("../../../../../assets/images/icon/prev-page-doc.svg")
    );
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})
export class ConfirmationDialogComponent implements OnInit {

  message: string = "Are you sure?"
  confirmButtonText = "Yes"
  cancelButtonText = "Cancel"
  colorButtonSubmit = "primary"
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<ConfirmationDialogComponent>) {

  }
  ngOnInit(): void {
  }

  ngAfterViewInit() {
    if (this.data) {
      setTimeout(() => {
        this.message = this.data.message || this.message;
        if (this.data.buttonText) {
          this.confirmButtonText = this.data.buttonText.ok || this.confirmButtonText;
          this.cancelButtonText = this.data.buttonText.cancel || this.cancelButtonText;
          this.colorButtonSubmit = this.data.buttonText.colorButtonSubmit || 'primary';
        }
      }, 0);
    }
  }

  onConfirmClick(): void {
    this.dialogRef.close(true);
  }

}

import { Injectable } from "@angular/core";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ErrorSnackBarComponent } from "../snack-bar/error-snack-bar/error-snack-bar.component";
import { SuccessSnackBarComponent } from "../snack-bar/success-snack-bar/success-snack-bar.component";

@Injectable()
export class MessageService {
  constructor(private _snackBar: MatSnackBar) {

  }
  public failureCallback(message: string, duration?: number, horizontalPosition?: any, verticalPosition?: any) {
    this._snackBar.openFromComponent(ErrorSnackBarComponent, {
      data: message,
      duration: duration ?? 5000,
      horizontalPosition: horizontalPosition ?? 'center',
      verticalPosition: verticalPosition ?? 'top',
    });
  }
  public susccessCallback(message: string, duration?: number, horizontalPosition?: any, verticalPosition?: any) {
    //debugger
    this._snackBar.openFromComponent(SuccessSnackBarComponent, {
      data: message,
      duration: duration ?? 5000,
      horizontalPosition: horizontalPosition ?? 'center',
      verticalPosition: verticalPosition ?? 'top',
    });
  }
}
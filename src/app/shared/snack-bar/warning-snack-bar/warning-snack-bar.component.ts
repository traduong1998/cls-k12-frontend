import { Component, Inject, OnInit } from '@angular/core';
import { MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { SuccessSnackBarComponent } from '../success-snack-bar/success-snack-bar.component';

@Component({
  selector: 'app-warning-snack-bar',
  templateUrl: './warning-snack-bar.component.html',
  styleUrls: ['./warning-snack-bar.component.scss']
})
export class WarningSnackBarComponent implements OnInit {
   
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any, private _snackRef: MatSnackBarRef<WarningSnackBarComponent>,) { }

  ngOnInit(): void {
  }

  dimiss() {
    this._snackRef.dismiss();
  }
}

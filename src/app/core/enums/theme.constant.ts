/**Các theme hỗ trợ layout */
// export const ThemeConstant: ThemeInfo[] = [
//     { name: 'bootstrap4-light-blue', poster: 'bootstrap4-light-blue' },
//     { name: 'bootstrap4-light-purple', poster: 'bootstrap4-light-purple' }
// ];

export const ThemeConstant = {
    'default': 'default',
};

export interface ThemeInfo {
    name: string;
    poster: string;
}

// export const themeConstant = {
//     'bootstrap4-light-blue': 'bootstrap4-light-blue.svg',
//     'bootstrap4-light-purple': 'bootstrap4-light-purple.svg',
//     'bootstrap4-dark-blue': 'bootstrap4-dark-blue.svg',
//     'bootstrap4-dark-purple': 'bootstrap4-dark-purple.svg',
//     'md-light-indigo': 'md-light-indigo.svg',
//     'md-light-deeppurple': 'md-light-deeppurple.svg',
//     'md-dark-indigo': 'md-dark-indigo.svg',
//     'md-dark-deeppurple': 'md-dark-deeppurple.svg',
//     'mdc-light-indigo': 'md-light-indigo.svg',
//     'mdc-light-deeppurple': 'md-light-deeppurple.svg',
//     'mdc-dark-indigo': 'md-dark-indigo.svg',
//     'mdc-dark-deeppurple': 'md-dark-deeppurple.svg',
//     'saga-blue': 'saga-blue.png',
//     'saga-green': 'saga-green.png',
//     'saga-orange': 'saga-orange.png',
//     'saga-purple': 'saga-purple.png',
//     'vela-blue': 'vela-blue.png',
//     'vela-green': 'vela-green.png',
//     'vela-orange': 'vela-orange.png',
//     'vela-purple': 'vela-purple.png',
//     'arya-blue': 'arya-blue.png',
//     'arya-green': 'arya-green.png',
//     'arya-orange': 'arya-orange.png',
//     'arya-purple': 'arya-purple.png',
//     'nova': 'nova.png',
//     'nova-alt': 'nova-alt.png',
//     'nova-accent': 'nova-accent.png',
//     'nova-vue': 'nova-vue.png',
//     'luna-blue': 'luna-blue.png',
//     'luna-green': 'luna-green.png',
//     'luna-pink': 'luna-pink.png',
//     'luna-amber': 'luna-amber.png',
//     'rhea': 'rhea.png',
//     'fluent-light': 'fluent-light.png',
//     'soho-light': 'soho-light.png',
//     'soho-dark': 'soho-dark.png',
//     'mira': 'mira.jpg',
// };
/**Các layout trong hệ thống */
export const RoleConstant = {
    administrator: 'administrator',
    teacher: 'teacher',
    learner: 'learner'
};
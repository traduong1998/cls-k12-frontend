/**Các layout trong hệ thống */
export enum LayoutConstant {
    'manage' = 'manage',
    'education' = 'education',
    'enterprise' = 'enterprise'
};
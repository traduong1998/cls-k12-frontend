import { DashboardTypes } from "./dashboard-types";

export interface DashboardConfig {
    type: DashboardTypes;
}
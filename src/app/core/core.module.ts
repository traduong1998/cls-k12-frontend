import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { coreInterceptors } from './interceptors/index';
import { I18nModule } from './modules/i18n/i18n/i18n.module';
import { HorizontalMenuItems } from './classes/menu-items/horizontal-menu-items';
import { SpinnerModule } from './modules/spinner/spinner.module';
import { HomeLayoutSharedModule } from './layouts/home-layouts/home-layout-shared.module';
import { MessageService } from '../shared/services/message.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CookieService } from 'ngx-cookie-service';
import { Spinner2Module } from '../shared/modules/spinner/spinner2.module';
@NgModule({
  declarations: [],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    RouterModule,
    HttpClientModule,
    I18nModule,
    SpinnerModule,
    HomeLayoutSharedModule,
    MatSnackBarModule,
    Spinner2Module
  ],
  exports: [],
  providers: [
    CookieService,
    //interceptors
    coreInterceptors,

    HorizontalMenuItems,
    MessageService
  ]
})

/**
 * Chứa singleton service, base layout, những thứ khác mà cần cho ứng dụng lúc startup
 */
export class CoreModule { }

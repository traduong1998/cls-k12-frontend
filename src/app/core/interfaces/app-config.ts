import { DashboardConfig } from "../enums/dashboard-config";

/**Cấu hình app */
export interface AppConfig {
    dark?: boolean;
    language: string;
    unit: {
        portalId: number,
        divisionId?: number,
        schoolId?: number,
        name: string,
        currentSchoolYear: number,
        config:UnitConfig
    };
    dashboard: DashboardConfig;
}
/**Cấu hình upload image,audio, video trong ckeditor */
export interface ConfigUploadCkeditor {
    portalId: number;
    uploadUrl: string;
    token: string;
}

export interface UnitConfig{
    desktopLogo: string,
    mobileLogo: string,
    favicon: string,
    desktopBanner: string,
    mobileBanner: string,
    address: string,
    phoneNumber:string,
    email: string,
    unitName:string
}
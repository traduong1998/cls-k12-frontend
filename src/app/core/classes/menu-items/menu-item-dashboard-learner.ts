import { Menu } from "src/app/core/classes/menu-items/menu-items";

/**Dashboard dành cho người học */
export const MenuItemDashboardLearner: Menu[] = [
    {
        state: '/learner',
        name: 'Tổng quan',
        type: 'link',
        icon: 'horizontal-header-detail-manage',
        useIconSVG: true
    },
    {
        state: '/learner/user-learn',
        name: 'Bài giảng',
        type: 'link',
        icon: 'play_lesson'
    },
    {
        state: '/learner/user-examination',
        name: 'Kỳ thi',
        type: 'link',
        icon: 'learner-exam',
        useIconSVG: true
    },
    {
        state: '/learner/interactive-class',
        name: 'Lớp học tương tác',
        type: 'link',
        icon: 'people',
    },
    {
        state: '/learner/user-learning-path',
        name: 'Lộ trình học tập',
        type: 'link',
        icon: 'learning-path',
        useIconSVG: true
    },
    {
        state: '/learner/manage-report',
        name: 'Lịch sử học tập',
        type: 'link',
        icon: 'history-study',
        useIconSVG: true
    },
    {
        state: '/learner/user-infor',
        name: 'Thông tin cá nhân',
        type: 'link',
        icon: 'user-info',
        useIconSVG: true
    }
];
import { Injectable } from '@angular/core';
import { CLSModules, CLSPermissions, InfoIdentity, LevelManages } from 'cls-k12-sdk-js/src';
import { MenuItemDashboardLearner } from 'src/app/core/classes/menu-items/menu-item-dashboard-learner';
import { DashboardTypes } from '../../enums/dashboard-types';
import { AppConfigService, AuthService } from '../../services';

export interface BadgeItem {
  type: string;
  value: string;
}
export interface Saperator {
  name: string;
  type?: string;
}
export interface SubChildren {
  state: string;
  name: string;
  type?: string;
}
export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
  isOverrideLink?: boolean;
  child?: SubChildren[];

  /**Có quyền */
  hasPermission?: boolean;
  /**Quyền menu item này */
  permissionInfo?: MenuItemPermission;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  useIconSVG?: boolean;
  badge?: BadgeItem[];
  saperator?: Saperator[];
  children?: ChildrenItems[];

  /**Có quyền */
  hasPermission?: boolean;

  /**Quyền menu item này */
  permissionInfo?: MenuItemPermission;
}
export interface MenuItemPermission {
  /**Module */
  module?: CLSModules;
  /**Quyền sử dụng trong module */
  permission?: CLSPermissions;
  /**Phân cấp */
  levelManage?: LevelManages;
}

/**Dashboard quản trị */
const MENUITEMS: Menu[] = [
  {
    state: '/dashboard',
    name: 'Tổng quan',
    type: 'link',
    icon: 'horizontal-header-detail-manage',
    useIconSVG: true,
    hasPermission: true
  },
  {
    state: '/dashboard',
    name: 'Cơ cấu tổ chức',
    type: 'sub',
    icon: 'co-cau-to-chuc',
    useIconSVG: true,
    hasPermission: true,
    children: [
      { state: 'manage-usertypes', name: 'Kiểu người dùng', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.UserType, permission: CLSPermissions.View } },
      { state: 'manage-divisions', name: 'Phòng giáo dục', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Division, permission: CLSPermissions.View } },
      { state: 'manage-schools', name: 'Trường học', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.School, permission: CLSPermissions.View } },
      { state: 'manage-users', name: 'Người dùng', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.User, permission: CLSPermissions.View } },
      { state: 'manage-groupstudents', name: 'Lớp học', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Class, permission: CLSPermissions.View } },
      { state: 'user-group', name: 'Nhóm học sinh', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.GroupStudent, permission: CLSPermissions.View } },
    ]
  },
  {
    state: '/dashboard',
    name: 'Quản lý chủ đề',
    type: 'sub',
    icon: 'quan-ly-chu-de',
    useIconSVG: true,
    hasPermission: true,
    children: [
      { state: 'manage-grades', name: 'Khối lớp', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Grade, permission: CLSPermissions.View } },
      { state: 'manage-subjects', name: 'Môn học', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Subject, permission: CLSPermissions.View } },
      { state: 'manage-curriculums', name: 'Khung chương trình', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Curriculum, permission: CLSPermissions.View } },
    ]
  },
  {
    state: '/dashboard',
    name: 'Quản lý dạy học',
    type: 'sub',
    icon: 'quan-ly-day-hoc',
    useIconSVG: true,
    hasPermission: true,
    children: [
      { state: 'learn/list-lesson', name: 'Khóa học', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Lesson, permission: CLSPermissions.View } },
      { state: 'online-teaching', name: 'Lớp học tương tác', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Lesson, permission: CLSPermissions.View } },
      { state: 'troubleshooting', name: 'Giải đáp thắc mắc', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Lesson, permission: CLSPermissions.View } }
    ]
  },
  {
    state: '/dashboard/examination-manager',
    name: 'Quản lý kỳ thi',
    type: 'sub',
    icon: 'quan-ly-to-chuc-thi',
    useIconSVG: true,
    hasPermission: true,
    children: [
      { state: '', name: 'Kỳ thi', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Exam, permission: CLSPermissions.View } },
      { state: 'organize', name: 'Tổ chức thi', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.OrganizeExam, permission: CLSPermissions.View } },
      { state: '/dashboard/user-examination/list-subject-supervision', name: 'Giám sát thi', type: 'link', isOverrideLink: true, hasPermission: true },
      { state: '/dashboard/user-examination/list-subject-exam', name: 'Chấm điểm thi', type: 'link', isOverrideLink: true, hasPermission: true }

    ]
  },
  {
    state: '/dashboard/question-bank',
    name: 'Ngân hàng câu hỏi',
    type: 'link',
    icon: 'question-bank',
    useIconSVG: true,
    permissionInfo: { module: CLSModules.QuestionBank, permission: CLSPermissions.View }
  },
  {
    state: '/dashboard/learning-path',
    name: 'Quy trình đào tạo',
    type: 'link',
    icon: 'quy-trinh-dao-tao',
    hasPermission: true,
    useIconSVG: true,
    permissionInfo: { module: CLSModules.LearningPath, permission: CLSPermissions.View }
  },
  {
    state: '/dashboard/manage-report',
    name: 'Báo cáo thống kê',
    type: 'sub',
    icon: 'bao-cao-thong-ke',
    useIconSVG: true,
    hasPermission: true,
    children: [
      { state: 'student', name: 'Báo cáo học sinh', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Report, permission: CLSPermissions.View } },
      { state: 'lesson', name: 'Báo cáo bài giảng', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Report, permission: CLSPermissions.View } },
      { state: 'online-classes', name: 'Báo cáo lớp học tương tác', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Report, permission: CLSPermissions.View } },
      { state: 'exam', name: 'Báo cáo kỳ thi', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Report, permission: CLSPermissions.View } },
      { state: 'teacher', name: 'Báo cáo giáo viên', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Report, permission: CLSPermissions.View } },
      { state: 'users-groups-report', name: 'Báo cáo nhóm học sinh', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Report, permission: CLSPermissions.View } },
      // { state: 'grade', name: 'Báo cáo khối, lớp học', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Report, permission: CLSPermissions.View } },
      { state: 'customize', name: 'Báo cáo tùy biến', type: 'link', hasPermission: true, permissionInfo: { module: CLSModules.Report, permission: CLSPermissions.View } }
    ]
  },
  {
    state: '/dashboard/setting-system',
    name: 'Cài đặt hệ thống',
    type: 'link',
    icon: 'cai-dat-he-thong',
    useIconSVG: true,
    hasPermission: true,
    permissionInfo: {
      module: CLSModules.SettingPortal, permission: CLSPermissions.View
    }
  }
];

@Injectable()
export class MenuItems {
  userIdentity: InfoIdentity;
  allMenuItemDashboards: Menu[];

  constructor(private appConfig: AppConfigService, private _authService: AuthService) {
    this.userIdentity = _authService.getTokenInfo();
    this.allMenuItemDashboards = JSON.parse(JSON.stringify(MENUITEMS));

    this.onEventFromAuthService();
  }

  private onEventFromAuthService() {
    this._authService.userIdentity$.subscribe(() => {
      console.log(`sesson subscribe`)
      this.userIdentity = this._authService.getTokenInfo();
      this.allMenuItemDashboards = JSON.parse(JSON.stringify(MENUITEMS));
    });
  }

  /**Danh sách các item. Mặc định ẩn các menu không có quyền */
  getMenuitem(): Menu[] {
    // console.log(`this.userIdentity`, this._authService.getToken());
    //TODO: xử lý binding liên tục ở đây
    if (this.appConfig.getConfig().dashboard.type == DashboardTypes.manage) {
      // JSON.parse(JSON.stringify(MENUITEMS))
      var menuDashboard = this.getMenuCanUsed(this.allMenuItemDashboards);

      return menuDashboard;
    }
    else {
      return MenuItemDashboardLearner;
    }
  }

  private getMenuCanUsed(menuItems: Menu[]) {
    return menuItems.filter(x => {
      //nếu menu parent có quyền thì kiểm tra trước
      let hasPermission = this.hasPermissionMenu(x.permissionInfo);
      if (!hasPermission) {
        return false;
      }

      //check child
      if (x.children) {
        x.children = x.children.filter((c) => {
          let hasPermission = this.hasPermissionMenu(c.permissionInfo);
          // console.log(`check child`, hasPermission);
          if (!hasPermission) {
            return false;
          }

          return true;
        });
      }

      //nếu menu dạng 'sub' có children mới show menu cha
      if (x.type == 'sub') {
        // console.log(`type sub`, x.name, x.children);
        if (!x.children || (x.children && x.children.length == 0)) {
          return false;
        }
      }
      return true;
    });
  }

  private hasPermissionMenu(permissionInfo: MenuItemPermission) {
    if (!permissionInfo) {
      return true;
    }

    let hasPermission = true;
    //kiểm tra phân cấp (nếu có)
    if (permissionInfo.levelManage) {
      hasPermission = this.userIdentity.levelManage == permissionInfo.levelManage;
    }

    //kiểm tra quyền chức năng (nếu có)
    if (permissionInfo.module && permissionInfo.permission) {
      hasPermission = this.userIdentity.hasPermission(permissionInfo.module, permissionInfo.permission);
    }
    return hasPermission;
  }
}
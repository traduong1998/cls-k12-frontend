﻿/**
 * Copyright (c) 2014, CKSource - Frederico Knabben. All rights reserved.
 * Licensed under the terms of the MIT License (see LICENSE.md).
 *
 * Basic sample plugin inserting current date and time into the CKEditor editing area.
 *
 * Created out of the CKEditor Plugin SDK:
 * https://docs.ckeditor.com/ckeditor4/docs/#!/guide/plugin_sdk_intro
 */




// Register the plugin within the editor.
CKEDITOR.plugins.add('InsertVideo', {
    // Register the icons. They must match command names.
    icons: 'insertvideo',
    // The plugin initialization logic goes inside this method.
    init: function (editor) {
        
        var element = editor.element;
        var elementParent = $(element.$).closest('.ck-editor-cls');
        var elementInputVideo = $(elementParent).find('.upload-video');
        var htmlLoading = $(`
                                <div style="position:absolute;top:0px;left:0px;;width: 100%;height: 100%;background-color: rgba(0, 0, 0, 0.4);" class="show-loading-CKEditor">
                                        <div style="position:relative;width: 100%;height: 100%;">
                                             <div style="display:inline-block;position:absolute;top:50%;left:50%;transform: translate(-50%, -50%);color: white;text-align: center;font-weight: bold;" class="info">
                                                <i  class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                                                <span  style="display:inline-block;width: 100%;"  class="text">0%</span>
                                             </div>
                                            
                                        </div>
                                </div>
                              `);
     
        var maDonViSuDung = $(elementParent).data('madonvisudung');
        var urlUploadServerFile = $(elementParent).data('urluploadserverfile');
        function showLoadingCKEditor() {
            $(elementParent).css("position", "relative");
            $(elementParent).append(htmlLoading);
        }
        function hideLoadingCKEditor() {
            $(elementParent).css("position", "inherit");
            var elementLoading = $(elementParent).find('.' + htmlLoading.attr('class'));
            $(elementLoading).remove();
        }
      

        // Define the editor command that inserts a timestamp.
        editor.addCommand('InsertVideo', {

            // Define the function that will be fired when the command is executed.
            exec: function (editor) {
                $(elementInputVideo).trigger('click');
                console.log(editor.lang);
                // Insert the timestamp into the document.
                //editor.insertHtml('The current date and time is: <em>' + now.toString() + '</em>');
            }
        });

        // Create the toolbar button that executes the above command.
        editor.ui.addButton('InsertVideo', {
            label: "Chèn Video",
            command: 'InsertVideo',
            toolbar: 'insert'
        });
      

        $(elementInputVideo).on('change', function (e) {
            var eInput = $(this);
            var files = e.target.files;
            if (window.FormData !== undefined && files.length > 0) {
                var data = new FormData();
                for (var x = 0; x < files.length; x++) {
                    data.append("file" + x, files[x]);
                }

                var sizeInMB = (files[0].size / (1024 * 1024)).toFixed(2);
                if (sizeInMB > 300) {
                    eInput.val('');
                    alertModal("Video không hợp lệ(Kích thước không được quá 300MB)", 'error');
                    return;
                }

                data.append("file", files[0]);
                data.append('LoaiFile', 'Video');
                data.append('MaDonViSuDung', maDonViSuDung);
                data.append('access-token', getCookie('auth-jwt'));
                showLoadingCKEditor();
                $.ajax({
                    type: "POST",
                    url: urlUploadServerFile,
                    contentType: false,
                    processData: false,
                    data: data,
                    xhr: function () {
                        var myXhr = $.ajaxSettings.xhr();
                        myXhr.upload.onprogress = uploadProgress;
                        function uploadProgress(e) {
                            if (e.lengthComputable) {
                                var index = parseInt((e.loaded / e.total) * 100);
                                if (index === 100) {
                                    $(elementParent).find(".show-loading-CKEditor .text").text("Đang convert video");
                                }
                                else {
                                    $(elementParent).find(".show-loading-CKEditor .text").text(index + "%");
                                }
                                console.log(index);
                            }
                        }
                        return myXhr;
                    },
                    success: function (resp) {
                        //progress.width(0 + '%')
                        //progress.html(0 + '%')
                        if (resp.status == "Failed") {
                            alert(resp.message);
                            console.log(resp.message);
                        } else {
                            var url = resp.filePath;
                            editor.insertHtml('<p></p> <video width="400" controls><source src="' + url + '" ></video> <p></p>');
                        }
                        eInput.val('');
                        hideLoadingCKEditor();
                    },
                    error: function (xhr, status, p3, p4) {
                        var err = "Error " + " " + status + " " + p3 + " " + p4;
                        console.log(err);
                        if (xhr.responseText && xhr.responseText[0] == "{")
                            err = JSON.parse(xhr.responseText).Message;
                        console.log(err);
                        eInput.val('');
                        hideLoadingCKEditor();
                    }
                });
            }
            else {
                eInput.val('');
                alertModal("Xin vui lòng chọn lại tệp tin", 'error');
            }
        });
    }
});


﻿/**
 * Copyright (c) 2014, CKSource - Frederico Knabben. All rights reserved.
 * Licensed under the terms of the MIT License (see LICENSE.md).
 *
 * Basic sample plugin inserting current date and time into the CKEditor editing area.
 *
 * Created out of the CKEditor Plugin SDK:
 * https://docs.ckeditor.com/ckeditor4/docs/#!/guide/plugin_sdk_intro
 */



// Register the plugin within the editor.
CKEDITOR.plugins.add('InsertImage', {
    // Register the icons. They must match command names.
    icons: 'insertimage',
    // The plugin initialization logic goes inside this method.
    init: function (editor) {

        var element = editor.element;
        var elementParent = $(element.$).closest('.ck-editor-cls');
        var elementInputPhoto = $(elementParent).find('.upload-photo');

        var htmlLoading = $(`
                                <div style="position:absolute;top:0px;left:0px;;width: 100%;height: 100%;background-color: rgba(0, 0, 0, 0.4);" class="show-loading-CKEditor">
                                        <div style="position:relative;width: 100%;height: 100%;">
                                             <div style="display:inline-block;position:absolute;top:50%;left:50%;transform: translate(-50%, -50%);color: white;text-align: center;font-weight: bold;" class="info">
                                                <i  class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                                                <span  style="display:inline-block;width: 100%;"  class="text">0%</span>
                                             </div>
                                            
                                        </div>
                                </div>
                              `);

        var maDonViSuDung = $(elementParent).data('madonvisudung');
        var urlUploadServerFile = $(elementParent).data('urluploadserverfile');

        function showLoadingCKEditor() {
            $(elementParent).css("position", "relative");
            $(elementParent).append(htmlLoading);
        }
        function hideLoadingCKEditor() {
            $(elementParent).css("position", "inherit");
            var elementLoading = $(elementParent).find('.' + htmlLoading.attr('class'));
            $(elementLoading).remove();
        }

        // Define the editor command that inserts a timestamp.
        editor.addCommand('InsertImage', {

            // Define the function that will be fired when the command is executed.
            exec: function (editor) {
                $(elementInputPhoto).trigger('click');
                console.log(editor.lang);
                // Insert the timestamp into the document.
                //editor.insertHtml('The current date and time is: <em>' + now.toString() + '</em>');
            }
        });

        // Create the toolbar button that executes the above command.
        editor.ui.addButton('InsertImage', {
            label: "Chèn Ảnh",
            command: 'InsertImage',
            toolbar: 'insert'
        });
      

        $(elementInputPhoto).on('change', function (e) {
            var eInput = $(this);
            var files = e.target.files;
            var data = new FormData();
            for (var x = 0; x < files.length; x++) {
                data.append("file" + x, files[x]);
            }
            var fileext = files[0].type.split("/");
            var memphis = files[0].name;
            var depay = files[0].size;

            if (fileext[0] == "image" && memphis.length < 100 && depay > 0 && depay <= 3 * 1024 * 1024) {
                console.log('Là tệp tin hình ảnh');

                if (files.length > 0) {

                    if (window.FormData !== undefined) {
                        data.append("file", files[0]);
                        data.append('LoaiFile', 'Image');
                        data.append('MaDonViSuDung', maDonViSuDung);
                        data.append('access-token', getCookie('auth-jwt'));
                        showLoadingCKEditor();
                        $.ajax({
                            type: "POST",
                            url: urlUploadServerFile,
                            contentType: false,
                            processData: false,
                            data: data,
                            xhr: function () {
                                var myXhr = $.ajaxSettings.xhr();
                                myXhr.upload.onprogress = uploadProgress;
                                function uploadProgress(e) {
                                    if (e.lengthComputable) {
                                        var index = parseInt((e.loaded / e.total) * 100);
                                        if (index === 100) {
                                            $(elementParent).find(".show-loading-CKEditor .text").text("Đang convert ảnh");
                                        }
                                        else {
                                            $(elementParent).find(".show-loading-CKEditor .text").text(index + "%");
                                        }
                                        console.log(index);
                                    }
                                }
                                return myXhr;
                            },
                            success: function (resp) {
                                //progress.width(0 + '%')
                                //progress.html(0 + '%')
                                if (resp.status == "Failed") {
                                    alert(resp.message);
                                    console.log(resp.message);
                                } else {
                                    var url = resp.filePath;
                                    editor.insertHtml('<p></p> <img src="' + url + '" />');
                                }
                                hideLoadingCKEditor();
                                eInput.val('');
                            },
                            error: function (xhr, status, p3, p4) {
                                var err = "Error " + " " + status + " " + p3 + " " + p4;
                                console.log(err);
                                if (xhr.responseText && xhr.responseText[0] == "{")
                                    err = JSON.parse(xhr.responseText).Message;
                                console.log(err);
                                eInput.val('');
                                hideLoadingCKEditor();
                            }
                        });
                    } else {
                        alertModal("Trình duyệt của bạn không hỗ trợ", 'error');
                    }
                }
            }
            else {
                console.log("Lỗi");
                eInput.val('');
                if (fileext[0] != "image") {
                    alertModal("Hình ảnh không hợp lệ", 'error');
                }
                if (memphis.length >= 100) {
                    alertModal("Tên tập tin quá dài.", 'error');
                }

                if (depay <= 0) {
                    alertModal("Hình ảnh không hợp lệ", 'error');
                }
                if (depay > 3 * 1024 * 1024) {
                    alertModal("Hình ảnh không hợp lệ (Kích thước không được quá 3MB)", 'error');
                }
            }

        });
    }
});


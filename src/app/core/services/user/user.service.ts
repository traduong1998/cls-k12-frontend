import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './types/user';
import { UserEndpoint } from './user.endpoint';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private endpoint: UserEndpoint) {
  }
  getUsers(): Observable<User[]> {
    return this.endpoint.getUsers();
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from './types/user';

@Injectable({
  providedIn: 'root'
})
export class UserEndpoint {
  private _baseUrl;

  constructor(private http: HttpClient) {
    this._baseUrl = `${environment.apiBaseUrl}/assets/fakedata`;
  }

  getUsers(): Observable<User[]> {
    return this.http
      .get<any>(`${this._baseUrl}/car-data.json`)
      .pipe(
        delay(2000), // Simulate request delay
        map(res => {
          return <User[]>res.data;
        }),
        map(data => { return data; })
      )
  }
}

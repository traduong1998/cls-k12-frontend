import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { AppConfig } from '../interfaces/app-config';
import { CLS, Logger } from 'cls-k12-sdk-js/src';
import { GlobalEndpoint } from 'cls-k12-sdk-js/src';
import { DashboardTypes } from '../enums/dashboard-types';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from './auth.service';
import { HomeEnpoint } from 'sdk/cls-k12-sdk-js/src/services/home-page/home-enpoint';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  private config: AppConfig = {
    dark: false,
    language: 'vi',
    unit: {
      portalId: undefined,
      name: undefined,
      currentSchoolYear: undefined,
      config: undefined
    },
    dashboard: {
      type: DashboardTypes.learner
    }
  };

  private logger: Logger = Logger.getInstance();

  private globalEndpoint: GlobalEndpoint;

  private homeEnpoint: HomeEnpoint;

  private configUpdate = new Subject<{ config: AppConfig, event: 'all' | 'dashboard' }>();

  public configUpdate$ = this.configUpdate.asObservable();

  private cookieKey = {
    dashboardType: 'dbt'
  };

  constructor(private cookieService: CookieService, private cls: CLS, private auth: AuthService) {
    this.logger.log(`AppConfigService constructor`);
    this.globalEndpoint = new GlobalEndpoint();
    this.homeEnpoint = new HomeEnpoint();
  }

  /**Khởi tạo cấu hình của app */
  init(): Promise<any> {
    return new Promise<void>(async resolve => {
      this.logger.warn('init config app ...');

      if (this.cookieService.get(this.cookieKey.dashboardType))
        this.config.dashboard.type = DashboardTypes[this.cookieService.get(this.cookieKey.dashboardType)];

      //init sdk
      this.cls.init();
      this.cls.setToken(this.auth.getToken());

      try {
        //lấy thông tin đơn vị qua tên mièn
        var unit = await this.globalEndpoint.getUnitInfo({ domain: location.hostname });


        var config = await this.homeEnpoint.getUnitHomeConfig(location.hostname);
        this.logger.warn('unit ', unit);
        this.config.unit = {
          portalId: unit.portalId,
          divisionId: unit.divisionId,
          schoolId: unit.schoolId,
          name: unit.name,
          currentSchoolYear: unit.currentSchoolYear,
          config: config
        };

      }
      catch (error) {
        console.error(`error find unit`);
        //TODO: điều hướng trang không tìm tháy portal

      }

      //set store local
      this.updateConfigToStore();

      resolve();
      this.logger.warn('init config app -> finish');
    });
  }

  /**Thay đổi dashboard  */
  changeDashboardType(dashboardType: DashboardTypes) {
    this.config.dashboard.type = dashboardType;
    this.cookieService.set(this.cookieKey.dashboardType, dashboardType, undefined, '/');

    this.configUpdate.next({
      config: this.config,
      event: 'dashboard'
    });
  }

  updateConfig(config: AppConfig) {
    this.config = { ...this.config, ...config };

    this.configUpdate.next({
      config: config,
      event: 'all'
    });
  }

  /**Get cấu hình app */
  getConfig() {
    return this.config;
  }

  /**Get cấu hình đơn vị */
  getUnitConfig() {
    return this.config?.unit?.config;
  }

  /**Lưu data cần lưu dài hạn vào localstorage hoặc cookie */
  private updateConfigToStore() {
    this.cookieService.set(this.cookieKey.dashboardType, this.config.dashboard.type, undefined, '/');
  }

}

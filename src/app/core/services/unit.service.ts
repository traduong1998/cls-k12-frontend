import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { UnitSetting } from './responses/unit-setting';

@Injectable({
  providedIn: 'root'
})
export class UnitService {
  private _baseUrl;

  constructor(public http: HttpClient) {
    this._baseUrl = `${environment.apiBaseUrl}/assets/fakedata`;
  }

  getUnitSettings(): Observable<UnitSetting> {
    return this.http
      .get<UnitSetting>(`${this._baseUrl}/unit-setting-data.json`)
      .pipe(
        delay(2000)// Simulate request delay
      )
  }
}

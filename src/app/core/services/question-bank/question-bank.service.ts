import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { QuestionResponse } from 'src/app/shared/modules/userexam/QuestionResponse';
import { QuestionBankEndpoint } from './question-bank.endpoint';

@Injectable({
  providedIn: 'root'
})
export class QuestionBankService {
  constructor(private endpoint: QuestionBankEndpoint) {
  }
  getQuestionData(): Observable<QuestionResponse[]> {
    return this.endpoint.getQuestionData();
  }
}

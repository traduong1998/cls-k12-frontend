import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { Question } from 'sdk/cls-k12-sdk-js/src/services/question/models/Question';
import { QuestionExam } from 'src/app/features/examination-manager/interfaces/question-exam';
import { QuestionTest } from 'src/app/features/learn/intefaces/questionTest';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class QuestionFileEndpoint{
    private _baseUrl;
  constructor(private http: HttpClient) {
    this._baseUrl = `${environment.apiBaseUrl}assets/fakedata`;
   }

  getQuestionData(): Observable<QuestionExam[]> {
    return this.http
      .get<any>(`assets/fakedata/data-question-list.json`)
      .pipe(
        map(res => {
          return <QuestionExam[]>res;
        }),
        map(data => { return data; })
      )
  }
}
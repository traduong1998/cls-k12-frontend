import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { QuestionExam } from 'src/app/features/examination-manager/interfaces/question-exam';
import { QuestionFileEndpoint } from './question-file.endpoint';

@Injectable({
  providedIn: 'root'
})
export class QuestionFileService {

  constructor(private endpoint: QuestionFileEndpoint) {
  }
  getQuestionData(): Observable<QuestionExam[]> {
    return this.endpoint.getQuestionData();
  }
}

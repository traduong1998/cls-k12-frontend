import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { UserExamInfo } from 'src/app/shared/modules/userexam/exam-supervision/UserExamInfo';
import { QuestionResponse } from 'src/app/shared/modules/userexam/QuestionResponse';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserExamSupervisionEndpoint{
    private _baseUrl;
  constructor(private http: HttpClient) {
    this._baseUrl = `${environment.apiBaseUrl}/assets/fakedata`;
   }

  getUserExamData(): Observable<UserExamInfo[]> {
    return this.http
      .get<any>(`/assets/fakedata/user-exam-info-data.json`)
      .pipe(
        delay(2000), // Simulate request delay
        map(res => {
          return <UserExamInfo[]>res.UserExamInfor;
        }),
        map(data => { return data; })
      )
  }
}
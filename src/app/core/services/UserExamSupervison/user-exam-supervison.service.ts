import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserExamInfo } from 'src/app/shared/modules/userexam/exam-supervision/UserExamInfo';
import { UserExamSupervisionEndpoint } from './user-exam-suppervision.enpoint';

@Injectable({
  providedIn: 'root'
})
export class UserExamSupervisonService {

  constructor(private endpoint: UserExamSupervisionEndpoint) { }

  getUserExamData(): Observable<UserExamInfo[]> {
    return this.endpoint.getUserExamData();
  }
}

import {
  Component,
  Input,
  OnDestroy,
  Inject,
  ViewEncapsulation
} from '@angular/core';
import {
  Router
} from '@angular/router';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-spinner-partial',
  template: `<div class="spinner-partial-container">
          <div class="template-default">
            <div class="spinner">
              <div class="double-bounce1"></div>
              <div class="double-bounce2"></div>
            </div>
            <div class="content">
              <div >loading {{value}}</div>
            </div>
        </div>
    </div>`,
  encapsulation: ViewEncapsulation.None
})
export class SpinnerPartialComponent implements OnDestroy {
  @Input() config: any = {};
  @Input() value: boolean;

  constructor(
    private router: Router,
    @Inject(DOCUMENT) private document: Document
  ) {

  }

  ngOnDestroy(): void {

  }
}


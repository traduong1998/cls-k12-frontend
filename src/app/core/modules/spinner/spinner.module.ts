import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SpinnerPartialComponent } from './spinner-partial.component';
import { SpinnerComponent } from './spinner.component';

@NgModule({
  declarations: [SpinnerComponent, SpinnerPartialComponent],
  imports: [CommonModule],
  exports: [SpinnerComponent, SpinnerPartialComponent]
})
export class SpinnerModule {

  constructor() {

  }
}
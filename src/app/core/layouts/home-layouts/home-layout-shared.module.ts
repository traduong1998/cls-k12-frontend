import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpinnerModule } from 'src/app/core/modules/spinner/spinner.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { NgxPermissionsModule } from 'ngx-permissions';
import { NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';
import { NgxMatMomentModule } from '@angular-material-components/moment-adapter'
import { ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DemoMaterialModule } from 'src/app/demo-material-module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { Spinner2Module } from 'src/app/shared/modules/spinner/spinner2.module';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    FlexLayoutModule,
    DemoMaterialModule,
    MaterialModule,
    SpinnerModule,
    MatSelectModule,
    MatFormFieldModule,
    NgxMatSelectSearchModule,
    NgxMatNativeDateModule,
    Spinner2Module,
    FormsModule,
    NgxPaginationModule
  ],
  exports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    NgxPermissionsModule,
    FlexLayoutModule,
    DemoMaterialModule,
    SpinnerModule,
    MatDatepickerModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatMomentModule,
    ReactiveFormsModule,
    Spinner2Module,
    FormsModule,
    NgxPaginationModule
  ],
})
/**
 * Share module trang chủ
 */
export class HomeLayoutSharedModule { }

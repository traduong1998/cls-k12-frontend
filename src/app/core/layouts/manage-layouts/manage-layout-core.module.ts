import { NgModule } from '@angular/core';
import { MenuItems } from '../../classes/menu-items/menu-items';
import { ManageLayoutSharedModule } from './manage-layout-shared.module';

@NgModule({
  declarations: [],
  imports: [
    ManageLayoutSharedModule
  ],
  exports: [

  ],
  providers: [
    //menu items
    MenuItems
  ]
})

/**
 * Core module trang quản trị
 */
export class ManageLayoutCoreModule { }

